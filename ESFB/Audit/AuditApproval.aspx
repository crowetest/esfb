﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditApproval.aspx.vb" Inherits="Audit_AuditApproval" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }  
        .sub_hdRow
        {
         background-color:#EBCCD6; height:20px;
         font-family:Arial; color:#B84D4D; font-size:8.5pt;  font-weight:bold;
        }      
        .style1
        {
            width: 100%;
        }
    </style>
    <script language="javascript" type="text/javascript">
     
       function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

          function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }

       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


        function btnApprove_onclick() {
            Update(2);
        }


        function btnReject_onclick() {
            Update(1);

        }
        function Update(Status)
        {
            var GroupID=document.getElementById("<%= cmbAudit.ClientID %>").value;
            var Dtl=document.getElementById("<%= cmbCategory.ClientID %>").value.split("Ø");
            var AuditID=Dtl[0];
            if(GroupID==-1)
            {alert("Select Audit");document.getElementById("<%= cmbAudit.ClientID %>").focus();return false;}
            if(AuditID=="-1")
            {alert("Select Category");document.getElementById("<%= cmbCategory.ClientID %>").focus();return false;}
            var ToData="1Ø"+GroupID+"Ø"+AuditID+"Ø"+Status
            ToServer(ToData, 1);
        }
   function FromServer(arg, context)
    {        
         switch (context) 
         {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);                  
                    if(Data[0]==0) {window.open("AuditApproval.aspx","_self");}
                    break;            
                 case 2:      
                  var Dtl=arg.split("Ø");   
                  var TypeID=Dtl[0];       
                  document.getElementById("<%= hid_dtls.ClientID %>").value=Dtl[1];
                  table_fill(TypeID,document.getElementById("<%= hid_dtls.ClientID %>").value);
                  document.getElementById("RowComments").style.display='';
                  document.getElementById("txtGeneralComments").value=Dtl[2];
                  document.getElementById("txtSeriousComments").value=Dtl[3];                 
                  break;     
                case 3:
                var Dtl=arg.split("¥"); 
                 document.getElementById("<%= txtManagementNote.ClientID %>").value=Dtl[1];
                ComboFill(Dtl[0], "<%= cmbCategory.ClientID %>");
                break;           
                default:
                 break;        
         }            
    }
        function table_fill(Type,arg) 
        {      
        if(arg=="")
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none';
           // document.getElementById("RowNew").style.display='none';
            //document.getElementById("RowComments").style.display='none';
        }
        else
        {
         if(Type==1)
         {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            document.getElementById("RowComments").style.display='';
            document.getElementById("RowNew").style.display='';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'Arial';' align='center'>";
            tab += "<tr >";           
            tab += "<td style='width:5%;text-align:center; '>#</td>";
            tab += "<td style='width:12%;text-align:left;'>Dep. No</td>";
//            tab += "<td style='width:7%;text-align:left; '>Center</td>";
            tab += "<td style='width:7%;text-align:left; '>Product</td>";          
            tab += "<td style='width:15%;text-align:left;'>Customer</td>";
//            tab += "<td style='width:7%;text-align:left;'>Sanction Dt</td>";   
            tab += "<td style='width:10%;text-align:left;'>Dep. Dt</td>";  
            tab += "<td style='width:5%;text-align:right;'>Amount</td>";   
//            tab += "<td style='width:4%;text-align:left;'>To be Verified</td>";  
            tab += "<td style='width:5%;text-align:center; '>Serious</td>";  
            tab += "<td style='width:5%;text-align:left;'>V.Serious</td>";   
            tab += "<td style='width:22%;text-align:left; '>Remarks</td>";  
            tab += "<td style='width:6%;text-align:left;'>Leakage</td>";    
            tab += "<td style='width:4%;text-align:left;'>Spot Close</td>";  
            tab += "<td style='width:4%;text-align:left;'>Staff Invmnt</td>";           
            tab += "</tr>";      
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:254px; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
            if (arg != "") {

                row = arg.split("¥");
                var GroupID=0;
                var SubGroupID=0;
                var ItemID=0;

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if(ItemID != col[5]) 
                    {
                        var heading=col[2]+"&nbsp;&nbsp;/&nbsp;&nbsp;"+col[4]+"&nbsp;&nbsp;/&nbsp;&nbsp;"+col[6];                      
                        tab += "<tr class='sub_hdRow';><td class='sub_hdRow'; colspan=15 >"+ heading + "</td></tr>";
                      
                    }
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_first';>";
                    }
                    i = n + 1;
                    var type="";
                    if(col[8]==1)   
                    type="Yes";     
                    var susp_leakage="";
                    if(col[14]==1)    
                    susp_leakage="Yes";  
                    var SpotClose=""
                    if(col[18]==0)     
                     SpotClose="Yes"
                     var Fraud=""
                    if(col[19]==1)     
                     Fraud="Yes"
                    tab += "<td style='width:5%;text-align:center' >"+ i +"</td>";
                    tab += "<td style='width:12%;text-align:left'>" + col[7] + "</td>"; 
//                    tab += "<td style='width:7%;text-align:left'>" + col[16] + "</td>"; 
                    tab += "<td style='width:7%;text-align:left'>" + col[17] + "</td>";                 
                    tab += "<td style='width:15%;text-align:left'>" + col[10] + "</td>"; 
//                    tab += "<td  align=left style='width:7%;' >" + col[13] + "</td>";  
                    tab += "<td  align=left style='width:10%;' >" + col[11] + "</td>"; 
                    tab += "<td  align=left style='width:5%;text-align:right;' >" + parseFloat(col[12]).toFixed(0)+"&nbsp;&nbsp;" + "</td>"; 
                   // tab += "<td style='width:4%;text-align:center'>"+ type +"</td>"; 
                    tab += "<td style='width:5%;text-align:center'>"+ susp_leakage +"</td>"; 
                    tab += "<td style='width:5%;text-align:center'>"+ Fraud +"</td>"; 
                    tab += "<td style='width:22%;text-align:left'>"+ col[9] +"</td>";   
                    tab += "<td style='width:6%;text-align:right'>"+ parseFloat(col[15]).toFixed(0) +"</td>";   
                    tab += "<td style='width:4%;text-align:center'>"+ SpotClose +"</td>";    
                    if (col[20]==1)      
                        tab += "<td style='width:4%;text-align:center'><a href='Reports/viewStaffInvolvement.aspx?SINo="+btoa(col[21])+"' target=blank>List</a></td>";       
                     else
                        tab += "<td style='width:4%;text-align:center'></td>";         
                    tab += "</tr>";   
                    ItemID=col[5];                 
                }
            }
            tab += "</table></div>";
            }
           else if(Type==2)
           {
              document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            document.getElementById("RowComments").style.display='';
            document.getElementById("RowNew").style.display='';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'Arial';' align='center'>";
            tab += "<tr >";           
            tab += "<td style='width:5%;text-align:center; '>#</td>";
            tab += "<td style='width:9%;text-align:left;'>Loan No</td>";
            tab += "<td style='width:7%;text-align:left; '>Center</td>";
            tab += "<td style='width:4%;text-align:left; '>Product</td>";          
            tab += "<td style='width:15%;text-align:left;'>Customer</td>";
            tab += "<td style='width:7%;text-align:left;'>Sanction Dt</td>";   
            tab += "<td style='width:7%;text-align:left;'>Loan Dt</td>";  
            tab += "<td style='width:5%;text-align:right;'>Amount</td>";   
//            tab += "<td style='width:4%;text-align:left;'>To be Verified</td>";  
            tab += "<td style='width:4%;text-align:center; '>Serious</td>";  
            tab += "<td style='width:5%;text-align:left;'>V.Serious</td>";   
            tab += "<td style='width:18%;text-align:left; '>Remarks</td>";  
            tab += "<td style='width:6%;text-align:left;'>Leakage</td>";    
            tab += "<td style='width:4%;text-align:left;'>Spot Close</td>";  
            tab += "<td style='width:4%;text-align:left;'>Staff Invmnt</td>";           
            tab += "</tr>";      
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:254px; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
            if (arg != "") {

                row = arg.split("¥");
                var GroupID=0;
                var SubGroupID=0;
                var ItemID=0;

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if(ItemID != col[5]) 
                    {
                        var heading=col[2]+"&nbsp;&nbsp;/&nbsp;&nbsp;"+col[4]+"&nbsp;&nbsp;/&nbsp;&nbsp;"+col[6];                      
                        tab += "<tr class='sub_hdRow';><td class='sub_hdRow'; colspan=15 >"+ heading + "</td></tr>";
                      
                    }
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_first';>";
                    }
                    i = n + 1;
                    var type="";
                    if(col[8]==1)   
                    type="Yes";     
                    var susp_leakage="";
                    if(col[14]==1)    
                    susp_leakage="Yes";  
                    var SpotClose=""
                    if(col[18]==0)     
                     SpotClose="Yes"
                     var Fraud=""
                    if(col[19]==1)     
                     Fraud="Yes"
                    tab += "<td style='width:5%;text-align:center' >"+ i +"</td>";
                    tab += "<td style='width:9%;text-align:left'>" + col[7] + "</td>"; 
                    tab += "<td style='width:7%;text-align:left'>" + col[16] + "</td>"; 
                    tab += "<td style='width:4%;text-align:left'>" + col[17] + "</td>";                 
                    tab += "<td style='width:15%;text-align:left'>" + col[10] + "</td>"; 
                    tab += "<td  align=left style='width:7%;' >" + col[13] + "</td>";  
                    tab += "<td  align=left style='width:7%;' >" + col[11] + "</td>"; 
                    tab += "<td  align=left style='width:5%;text-align:right;' >" + parseFloat(col[12]).toFixed(0)+"&nbsp;&nbsp;" + "</td>"; 
                   // tab += "<td style='width:4%;text-align:center'>"+ type +"</td>"; 
                    tab += "<td style='width:4%;text-align:center'>"+ susp_leakage +"</td>"; 
                    tab += "<td style='width:5%;text-align:center'>"+ Fraud +"</td>"; 
                    tab += "<td style='width:18%;text-align:left'>"+ col[9] +"</td>";   
                    tab += "<td style='width:6%;text-align:right'>"+ parseFloat(col[15]).toFixed(0) +"</td>";   
                    tab += "<td style='width:4%;text-align:center'>"+ SpotClose +"</td>";    
                    if (col[20]==1)      
                        tab += "<td style='width:4%;text-align:center'><a href='Reports/viewStaffInvolvement.aspx?SINo="+btoa(col[21])+"' target=blank>List</a></td>";       
                     else
                        tab += "<td style='width:4%;text-align:center'></td>";         
                    tab += "</tr>";   
                    ItemID=col[5];                 
                }
            }
            tab += "</table></div>";
            }
            else
            {
                 var row_bg = 0;
                document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
                document.getElementById("RowComments").style.display='';
                document.getElementById("RowNew").style.display='';
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'Arial';' align='center'>";
                tab += "<tr >";           
                tab += "<td style='width:5%;text-align:center;'>#</td>";
                tab += "<td style='width:13%;text-align:left;'>Group</td>";     
                tab += "<td style='width:16%;text-align:left;'>Sub Group</td>";
                tab += "<td style='width:22%;text-align:left;'>Check List</td>";   
                tab += "<td style='width:4%;text-align:left;'>To be Verified</td>";
                tab += "<td style='width:4%;text-align:left;'>Serious</td>";
                tab += "<td style='width:4%;text-align:left;'>V.Serious</td>";
                tab += "<td style='width:18%;text-align:left;'>Remarks</td>";  
                tab += "<td style='width:6%;text-align:left;'>Leakage</td>";  
                tab += "<td style='width:4%;text-align:left;'>Spot Close</td>"; 
                tab += "<td style='width:4%;text-align:left;'>Staff Invmnt</td>";               
                tab += "</tr>";      
                tab += "</table></div>";  
                tab += "<div id='ScrollDiv' style='width:100%; height:254px; overflow:auto;margin: 0px auto;' class=mainhead>";
                tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
                if (arg != "") {

                row = arg.split("¥");             
                for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");           
               
                 if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
               
                i = n + 1;
                var type="";
                if(col[7]==1)   
                type="Yes";    
                var SuspLeakage=""  
                if(col[9]==1)      
                   SuspLeakage="Yes"
                 var SpotClose=""
                 if(col[11]==0)     
                     SpotClose="Yes"
                 var Fraud=""
                 if(col[12]==1)     
                     Fraud="Yes"
                tab += "<td style='width:5%;text-align:center' >"+ i +"</td>";
                tab += "<td style='width:13%;text-align:left'>" + col[2] + "</td>";                   
                tab += "<td style='width:16%;text-align:left'>" + col[4] + "</td>"; 
                tab += "<td  align=left style='width:22%;' >" + col[6] + "</td>";  
                tab += "<td  align=center style='width:4%; text-align:center' >" + type + "</td>"; 
                tab += "<td  align=center style='width:4%; text-align:center' >" + SuspLeakage + "</td>"; 
                tab += "<td  align=center style='width:4%; text-align:center' >" + Fraud + "</td>";             
                tab += "<td style='width:18%;text-align:left'>"+ col[8] +"</td>";  
                tab += "<td  align=right; style='width:6%;' >" + parseFloat(col[10]).toFixed(0)  + "</td>"; 
                tab += "<td  align=center style='width:4%;text-align:center' >" + SpotClose + "</td>";    
                 if (col[13]==1)      
                    tab += "<td style='width:4%;text-align:center'><a href='Reports/viewStaffInvolvement.aspx?SINo="+btoa(col[14])+"' target=blank>List</a></td>";    
                 else
                     tab += "<td style='width:4%;text-align:center'></td>";                      
                tab += "</tr>";              
                }
                }
                tab += "</table></div>";
            }
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            }
            //--------------------- Clearing Data ------------------------//
        }
        function AuditOnChange()
        {
          ClearCombo("<%= cmbCategory.ClientID %>");
          document.getElementById("RowNew").style.display='none';
          document.getElementById("RowComments").style.display='none';          
          var AuditID=document.getElementById("<%= cmbAudit.ClientID %>").value;
          var ToData = "3Ø" + AuditID;                           
          ToServer(ToData, 3);
        }
        function CategoryOnChange()
        {        
            var AuditID=document.getElementById("<%= cmbCategory.ClientID %>").value;
            var ToData = "2Ø" + AuditID;                           
            ToServer(ToData, 2);
        }

    </script>
</head>
</html>
<asp:HiddenField ID="hid_dtls" runat="server" />
<br />
    <table class="style1" style="width:100%">        
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                Audit</td> 
            <td style="text-align:left; margin:0px auto; width:65%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="89%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td></tr>
        
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                &nbsp;</td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                Category</td> 
            <td style="text-align:left; margin:0px auto; width:65%;">
                <asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="89%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td></tr>
        
      
        
        <tr> 
            <td style="text-align:center; margin:0px auto; width:30%;">
                &nbsp;</td> 
            <td style="text-align:left; margin:0px auto; width:5%;">
                &nbsp;</td> 
            <td style="text-align:left; margin:0px auto; width:65%;">
                &nbsp;</td></tr>
        
      
        
        <tr> 
            <td colspan="3" style="text-align:center; margin:0px auto;">
                &nbsp;</td></tr>
        
        <tr id="RowNew"> 
            <td colspan="3" >
              <asp:Panel ID="pnDisplay" runat="server" Width="90%" style="text-align:center; margin:0px auto;">                           
            </asp:Panel></td>    </tr>

                  <tr id="RowComments" style="display:none;"> 
            <td  colspan="3">
                <table align="center" class="style1" width="90%" style="width:90%; text-align:center; margin:0px auto;">
                    <tr>                        
                        <td style="width:40%; font-family:Arial; font-size:10pt; font-weight:bold;" colSpan="2" >
                            General Comments</td>
                        <td style="width:10%">
                            &nbsp;</td>
                        <td style="width:40%; font-family:Arial; font-size:10pt; font-weight:bold;">
                &nbsp;Serious Comments</td>
                    </tr>
                    <tr>
                      
                        <td style="width:40%" colSpan="2">
                            <textarea id="txtGeneralComments" cols="20" name="S1" rows="3" onkeypress='return TextAreaCheck(event)' 
                                style="width: 100%; resize: none;" readonly="readonly" ></textarea></td>
                        <td style="width:10%">
                            &nbsp;&nbsp;&nbsp;</td>
                        <td style="width:40%">
                            <textarea id="txtSeriousComments" cols="20" name="S2" rows="3"  onkeypress='return TextAreaCheck(event)' 
                                style="width: 100%; resize: none;" readonly="readonly" ></textarea></td>
                    </tr>
                </table>
                      </td> 
         </tr>

           <tr>           
            <td style="text-align:center; margin:0px auto; width:65%; font-family:Arial; font-size:10pt; font-weight:bold;" colspan="3">
                            Management Note</td></tr>

           <tr>           
            <td style="text-align:center; margin:0px auto; width:65%;" colspan="3">
                 <asp:TextBox ID="txtManagementNote" runat="server" Rows="5" 
                     TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' Width="85%" maxlength=5000 ReadOnly="True"  
                    ></asp:TextBox>
               </td></tr>

        <tr>
            <td style="text-align:center;" colspan="3">
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="APPROVE"  onclick="return btnApprove_onclick()"/>&nbsp;
                <input id="btnReject" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="REJECT" onclick="return btnReject_onclick()" />&nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>   
   
</asp:Content>

