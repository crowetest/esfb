﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditIntervalApproval.aspx.vb" Inherits="AuditIntervalApproval" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master"%>
 
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>

      <style>
       .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblBody
        {
            border:9px;background-color:#EEB8A6; height:25px;font-size:10;color:#47476B;
        }
      
          .style1
          {
              width: 30%;
              height: 25px;
          }
      
          .style2
          {
              height: 14px;
          }
                
    </style>
    <br />
    <table align="center" style="width: 90%; margin:0px auto;" >
      <tr>
            <td style="width:30%; text-align:center;" colspan="2">
                <asp:HiddenField ID="hdnDtls" runat="server" />
                <asp:HiddenField ID="hdnValue" runat="server" />
              </td>
        
        </tr>
        <tr>
            <td style="text-align:center;" class="style2" colspan="2">
                Audit Plan For&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="cmbMonth" 
                    class="NormalText" runat="server" 
                        Width="10%">                                     

                    </asp:DropDownList>&nbsp;
                    <asp:DropDownList ID="cmbYear" class="NormalText" runat="server" 
                        Width="5%">                                     

                    </asp:DropDownList>&nbsp;&nbsp;
                <input id="btnView"  style="font-family: cambria; width: 7%; cursor: pointer;" 
                     type="button" value="VIEW" onclick="return btnView_onclick()" /></td>
        </tr>
        <tr>
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnObservDetails" runat="server">
                </asp:Panel>
               </td>
        </tr>
          <tr>
            <td  style="text-align:center; color:red" colspan="2" 
                  class="style1">
               
              </td>
        
        </tr>
        
          <tr>
            <td style="width:30%; text-align:left;">
                &nbsp;</td>
            <td style="width:70%; text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
             <td style="width:30%; text-align:center;" colspan="2">
                <input id="btnSave"  style="font-family: cambria; width: 10%; cursor: pointer;" 
                     type="button" value="SAVE" onclick="return btnSave_onclick()" /><input id="btn"  style="font-family: cambria; width: 10%; cursor: pointer;" 
                     type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
         
        </tr>
        <tr>
            <td style="width:30%; text-align:left;">
                &nbsp;</td>
            <td style="width:70%; text-align:left;">
                &nbsp;</td>
        </tr>
     
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[
     
        function btnExit_onclick() {
         window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }   
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);                
                window.open("AuditIntervalApproval.aspx","_self");
            }
            }        
            
       
        function FillApproveDetails()
        {           
            var arg= document.getElementById("<%= hdnDtls.ClientID %>").value;        
            var Dtl = arg.split("¥");
            var tab = "";           
            tab += "<div  style='width:100%; height:100%;  overflow:auto;   align:left; '  class=mainhead>";
            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
            tab += "<tr class='tblBody'>"; 
            tab += "<td style='width:5%;  text-align:center' >#</td>";          
            tab += "<td style='width:45%;  text-align:left' >Branch</td>";
            tab += "<td style='width:22%;  text-align:left'>Previous Interval</td>";
            tab += "<td style='width:22%; text-align:left'>Interval</td>";          
            tab += "<td style='width:6%;text-align:center' ><input type='checkbox' id='chkSelectAll' onclick='SelectAllOnClick()' /></td>";  
            tab += "</tr>"; 
            tab += "</table>";
            if(arg!="")    
            {      
            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
            var row_bg = 0;
            var i=0;
            for (a = 0; a <= Dtl.length-1; a++) 
            {
            var ObserveDtl = Dtl[a].split("µ");
            i+=1;
            if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_first'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_second'>";
                    }  
            tab += "<td style='width:5%;  text-align:center'>"+ i +"</td>";
            tab += "<td style='width:45%;  text-align:left' >"+ObserveDtl[1]+"</td>";
            tab += "<td style='width:22%;  text-align:left'>"+ ObserveDtl[2] +"</td>";
            tab += "<td style='width:22%;  text-align:left'>"+ ObserveDtl[3] +"</td>";          
            tab += "<td style='width:6%;  text-align:center'><input id='chk"+ i +"'   style='font-family: cambria; width: 45%; cursor: pointer;' type='checkbox' ></td>";  
            tab += "</tr>"; 
            }
            tab += "</table></div>";
            }
            document.getElementById("<%= pnObservDetails.ClientID %>").innerHTML=tab;         
              cursordefault();
        }
        function SelectAllOnClick()
        {
            var arg= document.getElementById("<%= hdnDtls.ClientID %>").value;        
            var Dtl = arg.split("¥");
            var i=0;
            for (a = 0; a <= Dtl.length-1; a++) 
            {          
            i+=1;
            document.getElementById("chk"+i).checked=document.getElementById("chkSelectAll").checked;
         }
        }
  
function btnSave_onclick() {
   var arg= document.getElementById("<%= hdnDtls.ClientID %>").value;        
            var Dtl = arg.split("¥");
            var i=0;
            document.getElementById("<%= hdnValue.ClientID %>").value="";
            for (a = 0; a <= Dtl.length-1; a++) 
            {          
            i+=1;          
            if(document.getElementById("chk"+i).checked==true)
            {
                  var ObserveDtl = Dtl[a].split("µ");
                  if(document.getElementById("<%= hdnValue.ClientID %>").value!="")
                        document.getElementById("<%= hdnValue.ClientID %>").value+="ÿ";
                  document.getElementById("<%= hdnValue.ClientID %>").value+=ObserveDtl[0];
            }
         }
         if(document.getElementById("<%= hdnValue.ClientID %>").value=="")
            {alert("Select atleast one request");return false;}
         var ToData = "1Ø" + document.getElementById("<%= hdnValue.ClientID %>").value;                  
         ToServer(ToData, 1);
        
}

function btnView_onclick() {
    var Yr=document.getElementById("<%= cmbYear.ClientID %>").value;
    var Mnth=parseInt(document.getElementById("<%= cmbMonth.ClientID %>").value);
    var combineDatestr = Yr + "/" + Mnth + "/" + 31;
    var d = new Date(combineDatestr); 
    window.open("Reports/AuditPlan.aspx?Mnth=" + btoa(Mnth) +" &Yr=" + btoa(Yr),"_blank");
}

// ]]>
    </script>

</asp:Content>

