﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditSkip.aspx.vb" Inherits="AuditSkip" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">

<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#3F719A;
          
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #B0C4DE 90%, #F1F3F4 100%);
           
        }
        
        .Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #3F719A 0%, #B8C4C8 0%, #3F719A  100%);
            color:#FFF;
        }   
       
       
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    <script language="javascript" type="text/javascript">
            
        function FromServer(arg, context) {
             if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("AuditSkip.aspx", "_self");              

            }         
            
        }
        
       

         function table_fill() {

            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            var BranchID=0;
            var row_bg = 0;
            var tab = "";
           
            tab += "<div id='ScrollDiv' style='width:70%; height:485px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 

            if (document.getElementById("<%= hid_Dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_Dtls.ClientID %>").value.split("¥");  
               
                for (n = 0; n <= row.length - 1; n++) {                    
                    col = row[n].split("µ");   
                    if(BranchID!=col[3])  
                    {
                       tab+="<tr class=mainhead><td style='width:10%;text-align:left;' >" + col[0] + "</td></tr>";
                    }  
                    BranchID=col[3];                               
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_first>";
                    }
                    i = n + 1;        
                                    
                    tab += "<td style='width:80%;text-align:left'>" + col[1] + "</td>";      
                    tab += "<td style='width:10%;text-align:center' ><input type='checkbox' id='chkSelect" + i + "'  /></td>"; 
                    tab += "</tr>"; 
                }
            }
            tab += "</table></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
              
 }
            
         function FinalStringForSave() {
         
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
                       
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                
                col = row[n].split("µ");
                i = n + 1; 
                        
                if (document.getElementById("chkSelect" + i).checked == 1) {
                   
                    document.getElementById("<%= hid_temp.ClientID %>").value +=  col[2] +"¥";
                }
            }
            return true;
        }    
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnSave_onclick() 
        {
            FinalStringForSave();
            if (document.getElementById("<%= hid_temp.ClientID %>").value =="" ) 
            {
                alert("Select Atleast one ");
                return false;
            }
	        var ToData = "1Ø" + document.getElementById("<%= hid_temp.ClientID %>").value;
            ToServer(ToData, 1);
        }
    </script>
</head>
</html>
<asp:HiddenField ID="hid_Dtls" runat="server" />
<asp:HiddenField ID="hid_temp" runat="server" />
<br />


 <table class="style1" style="width:80%; text-align:right; margin:0px auto;">   
     <tr id="RowNew"> 
            <td colspan="3" >
              <asp:Panel ID="pnDisplay" runat="server" Width="80%" style="text-align:center; margin:0px auto;">                           
            </asp:Panel></td>
     </tr> 
         
     <tr>
        <td style="text-align:center;" colspan="3">
            <br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="SAVE"  onclick="return btnSave_onclick()" onclick="return btnSave_onclick()" />&nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
    </tr>
     
 </table> 

 <br /> <br /> <br />
</asp:Content>