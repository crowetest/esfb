﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="GeneralAuditNew.aspx.vb" Inherits="Audit_GeneralAuditNew"  EnableEventValidation="false"%>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#FFF;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        }        
      
    </style>
   
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var CloseFlag = 0;
        function btnExit_onclick() 
        {
            window.open("AuditStart.aspx", "_self");
        }
         function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

         function FromServer(arg, context) {

         switch (context) {
             case 1:
                 var Data = arg.split("Ø");
                 alert(Data[1]);
                 if (CloseFlag == 0) {
                     CloseFlag = 0; if (Data[0] == 0 || Data[0] == 2) { document.getElementById("<%= cmbCheckList.ClientID %>").value = -1; table_fill(); }
                 }
                 else
                     if (Data[0] == 0 || Data[0] == 2) { window.open("RateABranch.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=3 &TypeID=1", "_self"); }
                     else if (Data[0] == 3) { window.open("RateABranch.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=3 &TypeID=3", "_self"); } 
                 break;
                case 2:
                    ComboFill(arg, "<%= cmbSubGroup.ClientID %>");
                    break;
                case 3:
                    ComboFill(arg, "<%= cmbCheckList.ClientID %>");
                    break;
              
            default:
            break;
        
            }
            
        }

         function table_fill() {           
        if(document.getElementById("<%= hdnAuditDetails.ClientID %>").value=="")
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none';          
        }
        else
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';          
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow-y: scroll; margin: 0px auto;'  class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr >";           
            tab += "<td style='width:5%;text-align:center' >#</td>";
            tab += "<td style='width:44%;text-align:left'>Check List</td>";
            tab += "<td style='width:5%;text-align:center'>Serious</td>";
            tab += "<td style='width:5%;text-align:center'>V.Serious</td>"; 
            tab += "<td style='width:26%;text-align:left'>Remarks</td>";
            tab += "<td style='width:6%;text-align:left'>Leakage</td>";
            tab += "<td style='width:3%;text-align:left'>Spot Close</td>";
            tab += "<td style='width:3%;text-align:left'>Staff Invlmt.</td>";
            tab += "<td style='width:3%;text-align:left'></td>";           
            tab += "</tr>";      
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:277px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
            if (document.getElementById("<%= hdnAuditDetails.ClientID %>").value != "") {

                row = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 2; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
                    i = n + 1;
                             
                     
                    tab += "<td style='width:5%;text-align:center' >" + i + "</td>";                          
                   
                    tab += "<td style='width:44%;text-align:left'>" + col[1] + "</td>";                  
                 
                              
                    if (col[3]=="0")
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + col[0] + "' disabled=true /></td>"; 
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + col[0] + "' checked='checked' disabled=true /></td>";                 
                    if (col[8] == "0")
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + col[0] + "' disabled=true  /></td>";
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + col[0] + "' checked='checked' disabled=true  /></td>";   
                    if(col[4]!="")
                        var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)'  onblur='return RemarksOnBlur(" + col[0] + ")'>" + col[4] + "</textarea>";
                    else
                        var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' type='Text' style='width:99%;'  maxlength='500' onkeypress='return TextAreaCheck(event)'  onblur='return RemarksOnBlur(" + col[0] + ")'  ></textarea>";
                    tab += "<td style='width:26%;text-align:left'>" + txtBox + "</td>";
                    if (col[5] != "")
                        var txtBox1 = "<textarea id='txtLeakage" + col[0] + "' name='txtLeakage" + col[0] + "' style='width:99%;' class='NormalText' onkeypress='NumericCheck(event)' onblur='updateString(" + col[0] + ",4)'  maxlength='7' >" + parseFloat(col[5]).toFixed(0) + "</textarea>";
                    else if (col[2] == "1")
                        var txtBox1 = "<textarea id='txtLeakage" + col[0] + "' name='txtLeakage" + col[0] + "' style='width:99%;' class='NormalText' onkeypress='NumericCheck(event)' disabled=true onblur='updateString(" + col[0] + ",4)' maxlength='7'  ></textarea>";
                    else
                        var txtBox1 = "<textarea id='txtLeakage" + col[0] + "' name='txtLeakage" + col[0] + "' style='width:99%;' class='NormalText' onkeypress='NumericCheck(event)'  onblur='updateString(" + col[0] + ",4)' maxlength='7' ></textarea>";
                    tab += "<td style='width:6%;text-align:left'>" + txtBox1 + "</td>";
                    if (col[6] == "0")
                        tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSpotClose" + col[0] + "' checked='checked'  onclick='updateString(" + col[0] + ",5)' /></td>";
                    else {
                        if (col[2] == "1" || col[8] == "1")
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSpotClose" + col[0] + "' disabled=true  onclick='updateString(" + col[0] + ",5)'  /></td>";
                        else
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSpotClose" + col[0] + "'  onclick='updateString(" + col[0] + ",5)'  /></td>";
                    }
                   
                    if (col[7] == "1")
                        tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkStaff" + col[0] + "' checked='checked'  onclick='updateString(" + col[0] + ",6)' /></td>";
                    else if (col[3] == "1" || col[8] == "1") {                    
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkStaff" + col[0] + "'  onclick='updateString(" + col[0] + ",6)'  /></td>";
                        }
                        else
                            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkStaff" + col[0] + "' disabled=true  onclick='updateString(" + col[0] + ",6)'  /></td>";
                    tab += "<td style='width:3%;text-align:center' onclick='delete_row(" + n + ")'><img  src='../Image/cross.png' style='align:middle;cursor:pointer;' /></td>"
                    tab += "</tr>";                    
                }
            }
            tab += "</table></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            }
            //--------------------- Clearing Data ------------------------//
    }

    function delete_row(a) {
        var data = "";
        var rows = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");
        var len = Math.abs(rows.length - 2);
        for (n = 0; n <= len ; n++) {
            if (a != n) {
                data += rows[n] +"¥";              

            }
        }
        if (data == "¥") {
            document.getElementById("<%= hdnAuditDetails.ClientID %>").value = "";
        }
        else {
            document.getElementById("<%= hdnAuditDetails.ClientID %>").value = data;
        }     
        table_fill();
    }


        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
          function GroupOnChange() {
            var GroupID = document.getElementById("<%= cmbGroup.ClientID %>").value;
            var ToData = "2Ø" + GroupID;              
            ToServer(ToData, 2);

        }
        function SubGroupOnChange()
        {
            var SubGroupID = document.getElementById("<%= cmbSubGroup.ClientID %>").value;
            var ToData = "3Ø" + SubGroupID;                  
            ToServer(ToData, 3);
        }

        function ItemOnChange() {
            var Dtl = document.getElementById("<%= cmbCheckList.ClientID %>").value.split("~")          
                var ItemID = Dtl[0];
                var Serious = Dtl[1];
                var VerySerious = Dtl[2];    
                var ItemName= document.getElementById("<%= cmbCheckList.ClientID %>").options[document.getElementById("<%= cmbCheckList.ClientID %>").selectedIndex].text;           
                if(ItemID!="-1" ) {
                    if (document.getElementById("<%= hdnAuditDetails.ClientID %>").value == "")
                        document.getElementById("<%= hdnAuditDetails.ClientID %>").value = ItemID + "µ" + ItemName + "µ0µ" + Serious + "µµµ1µ0µ" + VerySerious + "¥";
                    else {
                             var row = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");
                             for (n = 0; n <= row.length - 1; n++) {
                                 col = row[n].split("µ");
                                 if (col[0] == ItemID) {
                                     alert("Already Added"); document.getElementById("<%= cmbCheckList.ClientID %>").focus(); return false;
                                 }
                             }
                             document.getElementById("<%= hdnAuditDetails.ClientID %>").value += ItemID + "µ" + ItemName + "µ0µ" + Serious + "µµµ1µ0µ" + VerySerious + "¥";
                         } 
                         table_fill();
                         var elem = document.getElementById('ScrollDiv');
                         elem.scrollTop = elem.scrollHeight;
                }
                else
                {
                    alert("Select a Check list");document.getElementById("<%= cmbCheckList.ClientID %>").focus();
                }
            
            }
         
            
   
        function RemarksOnBlur(val)
        {           
            var textObject=document.getElementById("txtRemarks"+val);
            if (textObject.value == "") {
                alert("Enter Remarks");
                var xxx = "txtRemarks" + val + ".focus();"
                setTimeout(xxx, 1);
                return false;
            }
            else
                updateString(val, 3);
        }
     
    function btnSave_onclick() {      
        var AuditID = document.getElementById("<%= hdnAuditID.ClientID %>").value;      
        var GenComments="";
        var SerComments = "";
        var ManagementNote = ""
        ManagementNote = document.getElementById("<%= txtManagementNote.ClientID %>").value;
        if(document.getElementById("<%= hdnAuditDetails.ClientID %>").value !="")
        { var n = updateValue(); if (n == false) return false; }
        else
        {document.getElementById("<%= hdnValue.ClientID %>").value=""}        
        var CloseAudit=(document.getElementById("chkCloseAudit").checked==true) ? 1 :0;
        if(CloseAudit==1)
        {if(confirm("Are you sure to close this audit?")==0) {return false;} else {
         CloseFlag = 1;
         GenComments=document.getElementById("txtGeneralComments").value;
         SerComments=document.getElementById("txtSeriousComments").value;        
         if(GenComments=="")
         {alert("Enter Comments");document.getElementById("txtGeneralComments").focus();return false;}
         if(SerComments=="")
         {if(confirm("Are you sure there is no serious issues in this audit?")==0){document.getElementById("txtSeriousComments").focus();return false;}}
         }
         }
        if(document.getElementById("<%= hdnValue.ClientID %>").value=="")
        {
          if(confirm("Are you sure to save the Audit as error free?")==0) return false;}
      var ToData = "1Ø" + AuditID + "Ø" + document.getElementById("<%= hdnValue.ClientID %>").value + "Ø" + CloseAudit + "Ø" + GenComments + "Ø" + SerComments + "Ø" + ManagementNote;                  
          ToServer(ToData, 1);
         }
         

    function  updateValue()
    {
        document.getElementById("<%= hdnValue.ClientID %>").value="";
        var i=0;
        var  row = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");
        for (n = 0; n <= row.length - 2; n++) {
            col = row[n].split("µ");          
            if (document.getElementById("txtRemarks" + col[0]).value == "")
            { alert("Enter Remarks"); document.getElementById("txtRemarks" + col[0]).focus(); return false; }
            document.getElementById("<%= hdnValue.ClientID %>").value += "¥" + col[0] + "µ0µ" + document.getElementById("txtRemarks" + col[0]).value + "µ" + ((document.getElementById("chkSuspLeakage" + col[0]).checked == true) ? 1 : 0) + "µ" + Math.abs(document.getElementById("txtLeakage" + col[0]).value) + "µ" + ((document.getElementById("chkSpotClose" + col[0]).checked == true) ? 0 : 1) + "µ" + ((document.getElementById("chkStaff" + col[0]).checked == true) ? 1 : 0) + "µ" + ((document.getElementById("chkFraud" + col[0]).checked == true) ? 1 : 0);
           
        }
    }

    function updateString(val,index) {        
        var i = 0;
        var Newstr = "";
        var row = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");
        for (n = 0; n <= row.length - 2; n++) {
            col = row[n].split("µ");
            if (col[0] == val) {
                if (index == 1) {
                    if (document.getElementById("chkPotential" + col[0]).checked == true)
                        Newstr += col[0] + "µ" + col[1] + "µ" + "1" + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                    else
                        Newstr += col[0] + "µ" + col[1] + "µ" + "0" + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                }
                if (index == 2) {
                    if (document.getElementById("chkSuspLeakage" + col[0]).checked == true)
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + "1" + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                    else
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + "0" + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";
                }
                if (index == 3) {
                    Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + document.getElementById("txtRemarks" + col[0]).value + "µ" + col[5] + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";

                    }
                if (index == 4) {
                    Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + document.getElementById("txtLeakage" + col[0]).value + "µ" + col[6] + "µ" + col[7] + "µ" + col[8] + "¥";

                }
                if (index == 5) {
                    if (document.getElementById("chkSpotClose" + col[0]).checked == true)
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + "0" + "µ" + col[7] + "µ" + col[8] + "¥";
                    else
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + "1" + "µ" + col[7] + "µ" + col[8] + "¥";

                }
                  if (index == 6) {
                    if (document.getElementById("chkStaff" + col[0]).checked == true)
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ1" + "µ" + col[8] + "¥";
                    else
                        Newstr += col[0] + "µ" + col[1] + "µ" + col[2] + "µ" + col[3] + "µ" + col[4] + "µ" + col[5] + "µ" + col[6] + "µ0" + "µ" + col[8] + "¥";

                }
            }
            else
              Newstr+=row[n]+"¥";

        }       
        document.getElementById("<%= hdnAuditDetails.ClientID %>").value = Newstr;
    }

    function viewReport()
    {
       window.open("Reports/viewAuditReport.aspx?AuditID="+ btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value)+" &AuditTypeID="+ document.getElementById("<%= hdnAuditTypeID.ClientID %>").value+" &From=1","_self"); 
       return false;
    }
    function closeOnClick()
    {
        if(document.getElementById("chkCloseAudit").checked==true) {
            var Comments = document.getElementById("<%= hdnComments.ClientID %>").value.split("Ø");
            document.getElementById("GenComm").style.display = ""; document.getElementById("SerComm").style.display = "";
            document.getElementById("txtGeneralComments").value = Comments[0];
            document.getElementById("txtSeriousComments").value = Comments[1];
        }
        else
        {document.getElementById("GenComm").style.display="none";document.getElementById("SerComm").style.display="none";}
    }
    </script>
   </br>
    <table  align="center" style="width: 95%;text-align:right; margin:0px auto;">
        <tr>
            <td style="width:20%; font-weight:bold;" class="mainhead" colspan="2">
                &nbsp;BRANCH: &nbsp;<asp:Label ID="lblBranch" runat="server"></asp:Label></td>
           
            <td style="width:20%; font-weight:bold;" class="mainhead">
                &nbsp;START ON:&nbsp;<asp:Label ID="lblStartDate" runat="server"></asp:Label></td>
            <td style="width:20%; font-weight:bold;" class="mainhead" colspan="2">
                &nbsp;
            <asp:Label ID="lblAuditType" 
                    runat="server"></asp:Label>
            </td>
            <td style="width:20%; text-align:center; font-weight:bold;" class="mainhead">                
                <img id="ViewReport" src="../Image/ViewReport.png" onclick="viewReport()" title="View Report" style="height:20px; width:20px; cursor:pointer;"/>
            </td>
        </tr>
      
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%">
               Main Category</td>
            <td style="width:60%" colspan="4">
              &nbsp;<asp:DropDownList ID="cmbGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                </asp:DropDownList> </td>
        </tr>
        <tr>
            <td style="width:20%">
              </td>
            <td style="width:20%" >
                Sub Category 
            </td>
            <td style="width:60%" colspan="4">
                &nbsp;<asp:DropDownList ID="cmbSubGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:20%">
               </td>
            <td style="width:20%" >
               Check List
            </td>
            <td style="width:40%">
                &nbsp;<asp:DropDownList 
                    ID="cmbCheckList" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="98%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:20%" >
                 <div id="Button" onclick="return ItemOnChange()" style="width:10%; font-family:Courier New ; font-size:13pt; font-weight:bolder;  margin-right:20px;  margin-top:5px;">
                                    <strong>+</strong></div></td>
        </tr>
      
        <tr>
            <td style="width:20%;text-align:center;" colspan="6">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
            
        </tr>
      
       
        <br />
         <tr>
            <td style="width:20%;text-align:center;"colspan="6">
                <input id="chkCloseAudit" title="New" type="checkbox"  onclick="closeOnClick()" 
                    /> Close Audit</td>            
        </tr>
          <tr id="GenComm" style="display:none;">
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                General Comments</td>
            <td style="width:60%" colspan="4">
                <textarea id="txtGeneralComments" cols="20" name="S1" rows="2" maxlength="1000" onkeypress='return TextAreaCheck(event)' 
                    style="width: 95%"></textarea></td>
        </tr>
          <tr id="SerComm" style="display:none;">
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                Serious Comments</td>
            <td style="width:60%" colspan="4">
                 <textarea id="txtSeriousComments" cols="20" name="S1" rows="2"  maxlength="1000" onkeypress='return TextAreaCheck(event)' 
                    style="width: 95%"></textarea></td>
        </tr>
          <tr >
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                Management Note</td>
            <td style="width:60%" colspan="4">
                 <asp:TextBox ID="txtManagementNote" runat="server" Rows="5" 
                     TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Width="95%" maxlength=5000></asp:TextBox>
              </td>
        </tr>
         <tr>
            <td style="width:20%;text-align:center;"colspan="6">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" /> &nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>            
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:80%" colspan="5">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%; height: 21px;">
                <asp:HiddenField ID="hdnAuditID" runat="server" />
            </td>
            <td style="width:80%; height: 21px;" colspan="5">
                <asp:HiddenField ID="hdnAuditDetails" runat="server" />
                </td>
        </tr>
        <tr>
            <td style="width:20%">
                <asp:HiddenField ID="hdnAuditTypeID" runat="server" />
            </td>
            <td style="width:80%" colspan="5">
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:80%" colspan="5">
                <asp:HiddenField ID="hdnComments" runat="server" />
            </td>
        </tr>
    </table>
</head>
</html>
    </table>
    </table>
    </asp:Content>

