﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_DepartmentConfirmation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim SINo As Integer
    Dim ItemID As Integer
    Dim AD As New Audit
    Dim AuditType As Integer
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub Audit_SendToDepartmentConfirmation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 222) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Department Confirmation"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim SqlStr As String = ""
            Me.hdnSINo.Value = SINo.ToString()
            SqlStr = "select a.SINO ,a.REMARKS ,a.LOAN_NO,d.CLIENT_NAME,h.Item_Name,i.center_Name,j.remarks,(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end),isnull(ai.cnt,0),l.Confirmation_ID,k.branch_name+' ('+convert(varchar,k.branch_id)+')'   from AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),AUDIT_USER_LEVELS b,LOAN_MASTER d,AUDIT_OBSERVATION e,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,audit_observation_cycle j ,BRANCH_MASTER k,AUDIT_DEPARTMENTCONFIRMATION_DTL l where  a.LEVEL_ID=b.LEVEL_ID  and a.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.group_ID=g.Group_Id and f.AUDIT_TYPE<>3 and g.Status_id=2 and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and g.BRANCH_ID=k.Branch_ID  and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.sino=l.sino and a.status_ID=6 and l.status_id=1 and l.department_id  IN(SELECT DEPARTMENT_ID FROM AUDIT_CONFIRMATION_DEPARTMENTS WHERE DEPARTMENT_HEAD=" + Session("UserID").ToString() + ")" & _
           "UNION ALL select  a.SINO ,a.REMARKS,''as Loan_NO,'' as Client_Name,c.ITEM_NAME,'' as Center_Name,j.remarks,(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end),isnull(ai.cnt,0),l.Confirmation_ID,k.branch_name+' ('+convert(varchar,k.branch_id)+')'  from dbo.AUDIT_OBSERVATION_DTL a left outer join (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,AUDIT_USER_LEVELS f ,AUDIT_DTL h,Audit_Master i,audit_observation_cycle j,BRANCH_MASTER k,AUDIT_DEPARTMENTCONFIRMATION_DTL l WHERE a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id and i.status_id=2 and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and c.SUB_GROUP_ID=d.SUB_GROUP_ID and i.BRANCH_ID=k.Branch_ID  and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3 and a.LEVEL_ID=f.LEVEL_ID and a.sino=l.sino and a.status_ID=6 and l.status_id=1 and l.department_id IN(SELECT DEPARTMENT_ID FROM AUDIT_CONFIRMATION_DEPARTMENTS WHERE DEPARTMENT_HEAD=" + Session("UserID").ToString() + ")"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim ObservationArray As String = ""
            For Each DR As DataRow In DT.Rows
                If ObservationArray <> "" Then
                    ObservationArray += "ÿ"
                End If
                ObservationArray += DR(0).ToString() + "~" + DR(1).ToString() + "~" + DR(2).ToString() + "~" + DR(3).ToString() + "~" + DR(4).ToString() + "~" + DR(5).ToString() + "~" + DR(6).ToString() + "~" + DR(7).ToString() + "~" + DR(8).ToString() + "~" + DR(9).ToString() + "~" + DR(10).ToString()
            Next
            Me.hdnSINo.Value = ObservationArray
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then
            Dim ObservationDtl As String = Data(1)
            Dim Remarks As String = Data(2)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = ObservationDtl
                Params(1) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(1).Value = Remarks
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = UserID
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDIT_CONFIRMATION_FROM_DEPARTMENT", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If

    End Sub
End Class
