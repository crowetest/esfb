﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_AuditStaffInvolvementClose
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim DB As New MS_SQL.Connect
    Dim AuditID As Integer
    Dim AuditType As Integer
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 76) = False Then
                Server.Transfer("~/AccessDenied.aspx")
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Remove/Close Staff Involvement"

            GN.ComboFill(cmbEmployee, AD.GetEmployeesToCloseStaffInvolvement(CInt(Session("UserID")), CInt(Session("Post_ID"))), 0, 1)
            Me.cmbEmployee.Attributes.Add("onchange", "EmployeeOnChange()")
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim ObservationDtl As String = Data(2).ToString

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar, 1000)
                Params(0).Value = ObservationDtl.Substring(1)
                Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(1).Value = EmpCode
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_Audit_Close_Staff_Involvement", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message
        ElseIf CInt(Data(0)) = 4 Then
            Dim EmpCode As Integer = CInt(Data(1))
            DT = AD.GetObservationDtlsForStaffInvolvementRemoval(EmpCode, CInt(Session("USerID")), CInt(Session("Post_ID")))
            CallBackReturn = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString()
                If n < DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next
        End If
    End Sub
End Class
