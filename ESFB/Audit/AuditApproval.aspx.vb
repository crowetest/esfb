﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_AuditApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 43) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Audit Approval"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                DT = AD.GetAuditForApproval(CInt(Session("UserID")))
                GN.ComboFill(cmbAudit, DT, 0, 1)
            End If
            cmbAudit.Attributes.Add("onchange", "AuditOnChange()")
            cmbCategory.Attributes.Add("onchange", "CategoryOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim GroupID As Integer = CInt(Data(1))
            Dim AuditID As Integer = CInt(Data(2))
            Dim Status As Integer = CInt(Data(3))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@GroupID", SqlDbType.Int)
                Params(0).Value = GroupID
                Params(1) = New SqlParameter("@AuditID", SqlDbType.Int)
                Params(1).Value = AuditID
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(3).Value = Status
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDIT_APPROVAL", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If (CInt(Data(2)) <> 3) Then
                'If (CInt(Data(2)) = 4) Then
                '    CallBackReturn = "4Ø"
                'Else
                '    CallBackReturn = "1Ø"
                'End If
                CallBackReturn = Data(2) + "Ø"
                DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,f.CLIENT_NAME,convert(varchar(11),f.disbursed_Dt,106),f.loan_amt,convert(varchar(11),f.sanctioned_Dt,106),b.Susp_Leakage,b.Fin_Leakage,g.center_name,f.Scheme_ID,b.status_id,b.Fraud,b.Staff_involvement,b.sino from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,center_master g where a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and f.center_id=g.center_id and a.AUDIT_ID=" + Data(1) + " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString() & "µ" & DT.Rows(n)(11).ToString() & "µ" & DT.Rows(n)(12).ToString() & "µ" & DT.Rows(n)(13).ToString() & "µ" & DT.Rows(n)(14).ToString() & "µ" & DT.Rows(n)(15).ToString() & "µ" & DT.Rows(n)(16).ToString() & "µ" & DT.Rows(n)(17).ToString() & "µ" & DT.Rows(n)(18).ToString() & "µ" & DT.Rows(n)(19).ToString() & "µ" & DT.Rows(n)(20).ToString() & "µ" & DT.Rows(n)(21).ToString()
                    If n < DT.Rows.Count - 1 Then
                        CallBackReturn += "¥"
                    End If
                Next
            Else
                CallBackReturn = "3Ø"
                DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME,b.POT_FRAUD,b.REMARKS,b.Susp_Leakage,b.Fin_Leakage,b.status_id,b.Fraud,b.Staff_involvement,b.sino from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e where a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID  and a.AUDIT_ID=" + Data(1) + " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString() & "µ" & DT.Rows(n)(11).ToString() & "µ" & DT.Rows(n)(12).ToString() & "µ" & DT.Rows(n)(13).ToString() & "µ" & DT.Rows(n)(14).ToString()
                    If n < DT.Rows.Count - 1 Then
                        CallBackReturn += "¥"
                    End If
                Next
            End If

                DT = DB.ExecuteDataSet("SELECT General_Comments,Serious_Comments FROM AUDIT_DTL WHERE AUDIT_ID=" + Data(1).ToString()).Tables(0)
                If (DT.Rows.Count > 0) Then
                    CallBackReturn += "Ø" + DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString()
                Else
                    CallBackReturn += "ØØ"
                End If
            ElseIf CInt(Data(0)) = 3 Then
                DT = AD.GetAuditCategory(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "¥" + AD.GetManagementNote_Master(CInt(Data(1)))
            End If
    End Sub
End Class
