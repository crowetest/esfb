﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UploadCheckedSamples.aspx.vb" Inherits="UploadCheckedSamples"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <script src="../../Script/Validations.js" type="text/javascript"></script>
 <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
     
    <br />

<script type="text/javascript" src="../Script/xlsx.full.min.js"></script>
<script type="text/javascript" src="../Script/jszip.js"></script>
	
<script type="text/javascript">
    function Upload() {
        //Reference the FileUpload element.
        var fileUpload = document.getElementById("fileUpload");

        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
         if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
        }
    };
    function ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Create a HTML Table element.
        var table = document.createElement("table");
        table.border = "1";
        table.id = "table1";
        table.style = "width: 40%; text-align:center; margin:0px auto;";
        table.align = "center";

        //Add the header row.
      var row = table.insertRow(-1);

        //Add the header cells.
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "LOAN NO";
        row.appendChild(headerCell);

//       var headerCell = document.createElement("TH");
//       headerCell.innerHTML = "REMARK";
//        row.appendChild(headerCell);

//       var headerCell = document.createElement("TH");
//       headerCell.innerHTML = "CDATE";
        //        row.appendChild(headerCell);

        //Add the data rows from Excel file.
        for (var i = 0; i < excelRows.length; i++) {
            //Add the data row.
            var row = table.insertRow(-1);
          
            //Add the data cells.
            var cell = row.insertCell(-1);
            cell.innerHTML = excelRows[i].LOANNO;

//            cell = row.insertCell(-1);
//            cell.innerHTML = excelRows[i].REMARK;

//            cell = row.insertCell(-1);
//            cell.innerHTML = excelRows[i].CDATE;
        }

        var dvExcel = document.getElementById("dvExcel");
        dvExcel.innerHTML = "";
        dvExcel.appendChild(table);
    };
   

    function btnExit_onclick() {
         window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    function btnSave_onclick() {
        var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
        var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
        var StrLoan = "";
        if (BranchID == -1)
        { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
        if (AuditID == -1)
        { alert("Select Audit"); document.getElementById("<%= cmbAudit.ClientID %>").focus(); return false; }

        var myTab = document.getElementById('table1');
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
            for (var j = 0; j < objCells.length; j++) {
                if ((objCells.item(j).innerHTML) != "LOANNO")
                    StrLoan += "¥" + objCells.item(j).innerHTML;
            }
        }

        var ToData = "2Ø" + BranchID + "Ø" + AuditID + "Ø" + StrLoan;
        ToServer(ToData, 2);
    }
    function BranchOnChange() {
        var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
        if (BranchID == -1)
            ClearCombo("<%= cmbAudit.ClientID %>");
        else {
            var ToData = "1Ø" + BranchID;
            ToServer(ToData, 1);
        }
    }
    function FromServer(arg, context) {
        switch (context) {
            case 1:
                ComboFill(arg, "<%= cmbAudit.ClientID %>");
                break;
            case 2:
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) { window.open("UploadCheckedSamples.aspx", "_self"); }
                break;     
        }
    }

   function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }

    function DisplaySettings() {
        var ReportID = document.getElementById("<%= hdnReportID.ClientID %>").value;
        if (ReportID == 2)
            document.getElementById("rowStatus").style.display = ""
        else
            document.getElementById("rowStatus").style.display = "none"
    }
</script>

   <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                Branch</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Audit</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr id="rowStatus" style="display:none">
            <td style="width:15%;">
                Status</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                    <asp:ListItem Value="1">PENDING</asp:ListItem>                  
                    <asp:ListItem Value="2">CLOSED</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Select File</td>
            <td style="width:85%;">
                <input id="fileUpload" type="file" 
                style="font-family: Cambria; font-size: 10.5pt"  />&nbsp;                   
                <input id="upload" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="UPLOAD"  onclick="Upload()"/>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
           
 
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>

        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
                <asp:HiddenField ID="hdnValue" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>

    <div id="dvExcel" align="center" style="width: 40%; text-align:center; margin:0px auto;"></div>
     
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </asp:Content>
