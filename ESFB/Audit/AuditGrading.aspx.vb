﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class AuditGrading
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 88) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Branch Grading"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//      
            DT = AD.GetGradingSpecifications()
            Dim LastProcessDt As DateTime
            LastProcessDt = CDate(GN.GetParameterValue(10, 3))
            txtPeriodFrom.Text = LastProcessDt.ToString("dd MMM yyyy")
            txtPeriodTo.Text = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            hid_Dtls.Value = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                hid_Dtls.Value += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString()
                If n < DT.Rows.Count - 1 Then
                    hid_Dtls.Value += "¥"
                End If
            Next
            btnProcess.Attributes.Add("onclick", "return ProcessOnClick()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " table_fill();;", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            System.Threading.Thread.Sleep(3000)
            Dim Message As String = Nothing
            Dim DateVal() As String = Me.hid_temp.Value.Split(CChar("~"))
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@FROMDt", SqlDbType.DateTime)
                Params(0).Value = CDate(DateVal(0))
                Params(1) = New SqlParameter("@ToDt", SqlDbType.DateTime)
                Params(1).Value = CDate(DateVal(1))
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DT = DB.ExecuteDataSet("SP_AUDIT_GRADING", Params).Tables(0)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
            End Try
            If ErrorFlag = 0 Then
                Response.Redirect("Reports/viewGradingStatus.aspx")
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
