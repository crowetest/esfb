﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_BranchResponseApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AT As New Audit
    Dim CallBackReturn As String = Nothing
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim SelectedUserID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))

        If RequestID = 1 Then
            Dim SINo As Integer = CInt(Data(1))
            Dim remarks As String = CStr(Data(2))
            Dim StatusID As Integer = CInt(Data(3))
            Dim LevelID As Integer = CInt(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@SINo", SqlDbType.Int)
                Params(0).Value = SINo
                Params(1) = New SqlParameter("@Remarks", SqlDbType.VarChar, 5000)
                Params(1).Value = remarks
                Params(2) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(2).Value = StatusID
                Params(3) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(3).Value = UserID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@Level_ID", SqlDbType.Int)
                Params(6).Value = LevelID
                DB.ExecuteNonQuery("SP_AUDIT_RESPONSE_ATHORIZATION", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 2 Then
            'BRANCH HEAD/AREA HEAD/REGIONAL HEAD
            If DT.Rows.Count > 0 Then
                DTTS = AT.GetObservationForResponseApproval(CInt(Data(1)), CInt(Session("Post_ID")), CInt(Data(2)), CInt(Session("UserID")))
                If (DTTS.Rows.Count > 0) Then
                    For Each DR As DataRow In DTTS.Rows
                        CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                    Next
                Else
                    CallBackReturn = "-1Ñ ---------Select---------ÿ"
                End If
            End If
        ElseIf RequestID = 3 Then
            Dim AuditID As Integer = CInt(Data(3))
            Dim LoanNo As String = Data(2)
            Dim SINo As Integer = CInt(Data(1))
            Dim ItemID = CInt(Data(4))
            DTTS = AT.GetAuditDetailsforResponseApproval(AuditID)
            If DTTS.Rows.Count > 0 Then
                CallBackReturn = CDate(DTTS.Rows(0)(0)).ToString("dd MMM yyyy") + " - " + CDate(DTTS.Rows(0)(1)).ToString("dd MMM yyyy") + "Ñ" + DTTS.Rows(0)(2).ToString()
            Else
                CallBackReturn = "Ñ"
            End If
            If LoanNo = "0" Then
                CallBackReturn += "Ñ0Ñ0Ñ0Ñ0Ñ0Ñ0"
            Else
                DTTS = AT.GetLoanDetailsforResponseApproval(LoanNo)
                If DTTS.Rows.Count > 0 Then
                    CallBackReturn += "Ñ" + DTTS.Rows(0)(0).ToString() + "Ñ" + DTTS.Rows(0)(1).ToString() + "Ñ" + DTTS.Rows(0)(2).ToString() + "Ñ" + DTTS.Rows(0)(3).ToString() + "Ñ" + CDate(DTTS.Rows(0)(4)).ToString("dd MMM yyyy") + "Ñ" + DTTS.Rows(0)(5).ToString()
                Else
                    CallBackReturn += "Ñ0Ñ0Ñ0Ñ0Ñ0Ñ0"
                End If
            End If
            DTTS = AT.GetItemName(ItemID, 0)
            If DTTS.Rows.Count > 0 Then
                CallBackReturn += "Ñ" + DTTS.Rows(0)(0).ToString()
            Else
                CallBackReturn += "Ñ"
            End If
            DTTS = AT.GetAuditObservationCycle(SINo)
            CallBackReturn += "Ø"
            For n As Integer = 0 To DTTS.Rows.Count - 1
                CallBackReturn += CDate(DTTS.Rows(n)(0)).ToString("dd MMM yyyy") + "µ" + DTTS.Rows(n)(1).ToString() + "µ" + DTTS.Rows(n)(2).ToString() + "µ" + DTTS.Rows(n)(3).ToString() + "µ" + DTTS.Rows(n)(4).ToString()
                If n < DTTS.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next
        End If
    End Sub
    Protected Sub Audit_BranchResponseApproval_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If AT.IsAuthorized(CInt(Session("Post_ID"))) Then
                Me.Master.subtitle = "Branch Response Approval/Recommendation"
                Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
                Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
                Dim QueueID As Integer = AT.GetQueueID(CInt(Session("Post_ID")))
                Me.hdnQueueID.Value = QueueID.ToString()
                DT = AT.GetBranchesForResponseApproval(CInt(Session("UserID")), CInt(Session("Post_ID")), QueueID)
                GF.ComboFill(cmbBranch, DT, 0, 1)
                Me.hdnMaxLevelID.Value = AT.GetMaxLevelID(QueueID).ToString()
                Me.hdnMaxPostID.Value = AT.GetMaxPostID(QueueID).ToString()
                Me.hdnPostID.Value = Session("Post_ID").ToString()
                Me.cmbBranch.Attributes.Add("onchange", "return BranchOnchange()")
                Me.cmbObservation.Attributes.Add("onchange", "return ObservationOnchange()")
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Else
                Dim cl_script1 As New System.Text.StringBuilder
                cl_script1.Append("         alert('You are not authorized to view this page');")
                cl_script1.Append("         window.open('../Home.aspx','_self');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "client script", cl_script1.ToString, True)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
