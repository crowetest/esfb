﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Rectification
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 38) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Audit Rectification"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            If Not IsPostBack Then
                DT = AD.GetAuditDetails(CInt(Session("Post_ID")), CInt(Session("UserID")))
                GN.ComboFill(cmbAudit, DT, 0, 1)
            End If
            cmbAudit.Attributes.Add("onchange", "AuditOnChange()")
            cmbCategory.Attributes.Add("onchange", "CategoryOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
           
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@RectData", SqlDbType.VarChar)
                Params(0).Value = Data(1)
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = CInt(Session("UserID"))
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDIT_RECTIFICATION", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim Post_ID As Integer
            Dim StrVal As String
            Post_ID = CInt(Session("Post_ID"))
            If Post_ID = 5 Then 'bm
                StrVal = "and b.LEVEL_ID in(select level_id from audit_user_levels where post_id=" + Session("Post_ID").ToString() + ") and b.STATUS_ID=1"
            ElseIf Post_ID = 6 Then 'am
                StrVal = "and b.LEVEL_ID in(select level_id from audit_user_levels where post_id=" + Session("Post_ID").ToString() + ") And b.STATUS_ID = 1 " ''And b.SUSP_LEAKAGE = 1"
            Else '7 RM
                StrVal = "and b.LEVEL_ID in(select level_id from audit_user_levels where post_id=" + Session("Post_ID").ToString() + ") and b.STATUS_ID=1 and b.FRAUD=1"
            End If
            If (CInt(Data(2)) <> 3) Then
                CallBackReturn = "1Ø"
                DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,f.CLIENT_NAME,convert(varchar(11),isnull(f.disbursed_Dt,f.sanctioned_Dt),106),f.loan_amt,g.Center_Name,b.sino,b.level_id from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,Center_master g where a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and f.Center_ID=g.Center_ID and a.AUDIT_ID=" + Data(1) + "" & StrVal & " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString() & "µ" & DT.Rows(n)(11).ToString() & "µ" & DT.Rows(n)(12).ToString() & "µ" & DT.Rows(n)(13).ToString() & "µ" & DT.Rows(n)(14).ToString() & "µ" & DT.Rows(n)(15).ToString()
                    If n < DT.Rows.Count - 1 Then
                        CallBackReturn += "¥"
                    End If
                Next
            Else
                CallBackReturn = "2Ø"
                DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME,b.Pot_Fraud,b.REMARKS,b.sino,b.level_id from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e where a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID  and a.AUDIT_ID=" + Data(1) + "" & StrVal & " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                For n As Integer = 0 To DT.Rows.Count - 1
                    CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString()
                    If n < DT.Rows.Count - 1 Then
                        CallBackReturn += "¥"
                    End If
                Next
            End If
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetAuditCategory_ForRectify(CInt(Data(1)), CInt(Session("Post_ID")))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next


        End If
    End Sub
End Class
