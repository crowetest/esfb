﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AddMasters.aspx.vb" Inherits="AddMasters" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
     <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
// <![CDATA[
        var TypeID = 1;

        function HeadOnChange() {
            var HeadID = document.getElementById("<%= cmbHead.ClientID %>").value;
            if (HeadID == "-1") {
                document.getElementById("<%= txtHeadName.ClientID %>").value = ""; //document.getElementById("<%= cmbHead.ClientID %>").options[document.getElementById("<%= cmbHead.ClientID %>").selectedIndex].text; 
            }
            else {
                if (TypeID == 1) {
                    var Data = "5Ø1Ø" + HeadID;
                    ToServer(Data, 5);
                }
                else if (TypeID == 2) {
                    var Data = "3Ø2Ø" + HeadID;
                    ToServer(Data, 3);
                }
                else if (TypeID == 3) {
                    var Data = "3Ø3Ø" + HeadID;
                    ToServer(Data, 3);
                }
            }
        }
        function SubGroupOnChange() {
            var HeadID = document.getElementById("<%= cmbSubType.ClientID %>").value;
            if (HeadID == "-1") {
                document.getElementById("<%= txtHeadName.ClientID %>").value = ""; //document.getElementById("<%= cmbHead.ClientID %>").options[document.getElementById("<%= cmbHead.ClientID %>").selectedIndex].text; 

            }
            else {
               
                if (TypeID == 2) {
                    var Data = "5Ø2Ø" + HeadID;
                    ToServer(Data, 5);
                }
                else if (TypeID == 2) {
                    var Data = "6Ø2Ø" + HeadID;
                    ToServer(Data, 6);
                }

                else if (TypeID == 3) {
                    var Data = "6Ø3Ø" + HeadID;
                    ToServer(Data, 6);
                }
            }
        }

        function CheckListOnChange() {
            var HeadID = document.getElementById("<%= cmbCheckList.ClientID %>").value;
            if (HeadID == "-1") {
                document.getElementById("<%= txtHeadName.ClientID %>").value = "";
                document.getElementById("rdVSerious").checked = false;
                document.getElementById("rdSerious").checked = false;
                document.getElementById("rdGeneral").checked = true;
                //document.getElementById("<%= cmbHead.ClientID %>").options[document.getElementById("<%= cmbHead.ClientID %>").selectedIndex].text; 

            }
            else {
                var Data = "5Ø3Ø" + HeadID;
                ToServer(Data, 5);
            }
        }
        function SaveOnClick() {
            var AuditTypeID = document.getElementById("<%= cmbAuditType.ClientID %>").value;
            var GroupID = document.getElementById("<%= cmbHead.ClientID %>").value;
            var SubGroupID = -1;
            if (TypeID > 1) {
                SubGroupID = document.getElementById("<%= cmbSubType.ClientID %>").value;
            }

            var CheckListID = -1;
            if (TypeID > 2)
                CheckListID = document.getElementById("<%= cmbCheckList.ClientID %>").value;
            var SubTypeName = document.getElementById("<%= txtHeadName.ClientID %>").value;
            if (AuditTypeID == -1)
            { alert("Select Audit Type"); document.getElementById("<%= cmbAuditType.ClientID %>").focus(); return false; }
            if (TypeID > 1 && GroupID == -1)
            { alert("Select Group"); document.getElementById("<%= cmbHead.ClientID %>").focus(); return false; }
            if (TypeID > 2 && SubGroupID == -1)
            { alert("Select Sub Group"); document.getElementById("<%= cmbSubType.ClientID %>").focus(); return false; }
            var Sample = 0;
            var Category = 0;
            var MaxMar = 0;
            var Description = "";

            if (TypeID == 2) {
                Sample = document.getElementById("<%= cmbSample.ClientID %>").value;
                Category = document.getElementById("<%= cmbCategory.ClientID %>").value;
                if (Sample == -1)
                { alert("Select Sample"); document.getElementById("<%= cmbSample.ClientID %>").focus(); return false; }
                if (Category == -1)
                { alert("Select Category"); document.getElementById("<%= cmbCategory.ClientID %>").focus(); return false; }
            }
            if (TypeID == 3) {
                MaxMar = document.getElementById("<%= txtMaxMark.ClientID %>").value;
                Description = document.getElementById("<%= txtDescription.ClientID %>").value;

                if (MaxMar == "")
                { alert("Enter Max.Mark"); document.getElementById("<%= txtMaxMark.ClientID %>").focus(); return false; }
                if (Description == "")
                { alert("Enter Description"); document.getElementById("<%= txtDescription.ClientID %>").focus(); return false; }
            }

            if (SubTypeName == "")
            { alert("Enter Name"); document.getElementById("<%= txtHeadName.ClientID %>").focus(); return false; }

            var Flag = 0;
            if ((TypeID == 1 && GroupID == -1) || (TypeID == 2 && SubGroupID == -1) || (TypeID == 3 && CheckListID == -1)) {
                Flag = 1;
            }


            var SPM = 0;
            var Serious = 0;
            var VSerious = 0;
            if (document.getElementById("chkSPM").checked == true)
                SPM = 1;
            if (document.getElementById("rdVSerious").checked == true)
                VSerious = 1;
            else if (document.getElementById("rdSerious").checked == true)
                Serious = 1;
            var Data = "4Ø" + TypeID + "Ø" + GroupID + "Ø" + SubGroupID + "Ø" + CheckListID + "Ø" + SubTypeName + "Ø" + Flag + "Ø" + SPM + "Ø" + Serious + "Ø" + VSerious + "Ø" + AuditTypeID + "Ø" + Sample + "Ø" + Category + "Ø" + MaxMar + "Ø" + Description;
            ToServer(Data, 4);
        }

        function FromServer(arg, context) {
            switch (context) {
                case 1: //  Existing Fill On Change of Type
                    {
                        var Data = arg.split("Ø");
                        ComboFill(Data[0], "<%= cmbHead.ClientID %>");
                        var HeadID = document.getElementById("<%= cmbHead.ClientID %>").value;
                        if (HeadID == -1) {
                            document.getElementById("<%= txtHeadName.ClientID %>").value = "";
                        }
                        break;
                    }
                case 2:
                    {
                        var Data = arg.split("Ø"); //3644ØAnand K B
                        ComboFill(Data[0], "<%= cmbSubType.ClientID %>");
                        break;
                    }
                case 3: //  Existing Fill On Change of Type
                    {
                        var Data = arg.split("Ø");
                        ComboFill(Data[0], "<%= cmbSubType.ClientID %>");
                        break;
                    }
                case 4:
                    {
                        var Data = arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {
                            ClearCombo("<%= cmbHead.ClientID %>", 1);
                            if (document.getElementById("<%= cmbSubType.ClientID %>"))
                                ClearCombo("<%= cmbSubType.ClientID %>", 2);
                            if (document.getElementById("<%= cmbCheckList.ClientID %>"))
                                ClearCombo("<%= cmbCheckList.ClientID %>", 2);
                            document.getElementById("<%= txtHeadName.ClientID %>").value = "";
                            document.getElementById("<%= cmbAuditType.ClientID %>").value = -1;
                            document.getElementById("<%= cmbSample.ClientID %>").value = -1;
                            document.getElementById("<%= cmbCategory.ClientID %>").value = -1;
                            document.getElementById("<%= txtMaxMark.ClientID %>").value = "";
                            document.getElementById("<%= txtDescription.ClientID %>").value = "";


                        }
                        break;
                    }
                case 5:
                    {
                        var Data = arg.split("Ø");
                        if (Data[0] == 1) {
                            document.getElementById("<%= txtHeadName.ClientID %>").value = Data[1];
                        }
                        else if (Data[0] == 2) {
                            document.getElementById("<%= txtHeadName.ClientID %>").value = Data[1];
                            document.getElementById("<%= cmbSample.ClientID %>").value = Data[2];
                            document.getElementById("<%= cmbCategory.ClientID %>").value = Data[3];

                        }

                        else {
                            document.getElementById("<%= txtHeadName.ClientID %>").value = Data[1];
                            document.getElementById("<%= txtMaxMark.ClientID %>").value = Data[5];
                            document.getElementById("<%= txtDescription.ClientID %>").value = Data[6];

                            if (Data[2] == 1)
                                document.getElementById("chkSPM").checked = true;
                            else
                                document.getElementById("chkSPM").checked = false;
                            if (Data[3] == 1) {
                                document.getElementById("rdSerious").checked = true;
                            }
                            else if (Data[4] == 1)
                                document.getElementById("rdVSerious").checked = true;
                            else
                                document.getElementById("rdGeneral").checked = true;


                        }
                        break;
                    }
                case 6:
                    {

                        var Data = arg.split("Ø"); //3644ØAnand K B
                        ComboFill(Data[0], "<%= cmbCheckList.ClientID %>");
                        document.getElementById("<%= cmbSample.ClientID %>").value = Data[1];
                        document.getElementById("<%= cmbCategory.ClientID %>").value = Data[2];

                        break;
                    }
            }
        }


        function btnExit_onclick() {
            window.open('../home.aspx', '_self');
        }

        function GrpOnClick() {
            ClearCombo("<%= cmbHead.ClientID %>", 1);
            ClearCombo("<%= cmbSubType.ClientID %>", 2);
            ClearCombo("<%= cmbCheckList.ClientID %>", 2);
            document.getElementById("<%= cmbSample.ClientID %>").value = -1;


            document.getElementById("<%= txtHeadName.ClientID %>").value = "";
            document.getElementById("<%= cmbAuditType.ClientID %>").value = -1;
            if (document.getElementById("rdGroup").checked == true) {
                document.getElementById("<%= lblHead.ClientID %>").innerHTML = "Group";
                document.getElementById("<%= lblNewHead.ClientID %>").innerHTML = "Group Name";
                document.getElementById("rowSubGroup").style.display = "none";
                document.getElementById("rowSubGroup1").style.display = "none";
                document.getElementById("rowSubGroup2").style.display = "none";
                document.getElementById("rowCheckList").style.display = "none";
                document.getElementById("rowCheckList1").style.display = "none";
                document.getElementById("rowCheckList2").style.display = "none";

                document.getElementById("rowGroup").style.display = "";
                TypeID = 1;
            }
            if (document.getElementById("rdSubGroup").checked == true) {
                document.getElementById("<%= lblHead.ClientID %>").innerHTML = "Group";
                document.getElementById("<%= lblNewHead.ClientID %>").innerHTML = "Sub Group Name";
                document.getElementById("rowSubGroup").style.display = "";
                document.getElementById("rowSubGroup1").style.display = "";
                document.getElementById("rowSubGroup2").style.display = "";

                document.getElementById("rowCheckList").style.display = "none";
                document.getElementById("rowCheckList1").style.display = "none";
                document.getElementById("rowCheckList2").style.display = "none";
                document.getElementById("rowGroup").style.display = "";
                TypeID = 2;
            }
            if (document.getElementById("rdCheckList").checked == true) {
                document.getElementById("<%= lblHead.ClientID %>").innerHTML = "Group";
                document.getElementById("<%= lblNewHead.ClientID %>").innerHTML = "Check List";
                document.getElementById("rowSubGroup").style.display = "";
                document.getElementById("rowSubGroup1").style.display = "";
                document.getElementById("rowSubGroup2").style.display = "";

                document.getElementById("rowGroup").style.display = "";
                document.getElementById("rowCheckList").style.display = "";
                document.getElementById("rowCheckList1").style.display = "";
                document.getElementById("rowCheckList2").style.display = "";
                document.getElementById("rowSPM").style.display = "";
                document.getElementById("rowType").style.display = "";
                TypeID = 3;

            }
        }
        function TypeOnChange() {
            AuditTypeID = document.getElementById("<%= cmbAuditType.ClientID %>").value;
            var Data = "1Ø" + TypeID + "Ø" + AuditTypeID;
            ToServer(Data, 1);

        }
        function ClearCombo(control, Flag) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            if (Flag == 1)
                option1.text = " -----NEW-----";
            else
                option1.text = " -----SELECT-----";
            document.getElementById(control).add(option1);
        }



// ]]>
    </script>

</head>
    <table style="width: 80%;margin: 0px auto">
        <tr>
            <td style="width:33%; text-align: right;font-family:Cambria;">
                &nbsp;</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%; font-family: Cambria; font-size: 12pt;">
                    &nbsp;</td>
        </tr>
        <tr>
            <td style="width:33%; text-align: right;font-family:Cambria;">
                Option</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%; font-family: Cambria; font-size: 10pt;">
                    <input id="rdGroup" name="Grp" type="radio" onclick="return GrpOnClick()" checked="checked" onclick="return rdGroup_onclick()" onclick="return rdGroup_onclick()" />&nbsp;&nbsp;&nbsp;Group&nbsp;&nbsp;&nbsp;<input id="rdSubGroup" name="Grp" 
                        type="radio"  onclick="return GrpOnClick()" />&nbsp;&nbsp;&nbsp;Sub Group&nbsp;&nbsp;&nbsp;<input id="rdCheckList" name="Grp" type="radio"  onclick="return GrpOnClick()" />&nbsp;&nbsp;&nbsp;Check List&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="width:33%; text-align: right;font-family:Cambria;">
                &nbsp;</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%; font-family: Cambria; font-size: 12pt;">
                    &nbsp;</td>
        </tr>
        <tr>
            <td style="width:33%; text-align: right;font-family:Cambria; height: 14px;">
                Audit Type</td>
            <td   style="width:1%; height: 14px;">
                </td>
            <td  style="width:55%; height: 14px;">
                    <asp:DropDownList ID="cmbAuditType" class="NormalText" runat="server" 
                        Width="40%">                                     

                    </asp:DropDownList>
            </td>
        </tr>
        <tr id="rowGroup" >
            <td style="width:33%; text-align: right;font-family:Cambria;">
                <asp:Label ID="lblHead" runat="server" Text="Group"></asp:Label>
            </td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbHead" class="NormalText" runat="server" Width="40%">
                                        </asp:DropDownList>
            </td>
        </tr>
      
        <tr id="rowSubGroup" style="display:none;">
            <td style="width:33%; text-align: right;font-family:Cambria;">
                Sub Group</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbSubType" class="NormalText" runat="server" Width="40%">
                                        </asp:DropDownList>
            </td>
        </tr>
         <tr id="rowSubGroup1" style="display:none;">

            <td style="width:33%; text-align: right;font-family:Cambria;">
                Sample</td>
            <td   style="width:1%;">
                &nbsp;</td>

            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbSample" class="NormalText" runat="server" Width="40%">
                    <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                    <asp:ListItem Value="1">SAMPLE</asp:ListItem>
                    <asp:ListItem Value="2">NON-SAMPLE</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
         <tr id="rowSubGroup2" style="display:none;">
            <td style="width:33%; text-align: right;font-family:Cambria; height: 21px;">
                Category</td>
            <td   style="width:1%; height: 21px;">
                </td>

            <td  style="width:55%; height: 21px;">
                    <asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" 
                        Width="40%">
                        <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                        <asp:ListItem Value="1">BUSINESS-RISK</asp:ListItem>
                        <asp:ListItem Value="2">CONTROL-RISK</asp:ListItem>
                    </asp:DropDownList>
            </td>

        </tr>

        <tr  id="rowCheckList" style="display:none;">
            <td  style="width:33%; text-align: right;font-family:Cambria;">
                Check List</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <asp:DropDownList ID="cmbCheckList" class="NormalText" runat="server" 
                        Width="40%">
                    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td id="col1" style="width:33%; text-align: right;font-family:Cambria;">
                <asp:Label ID="lblNewHead" runat="server" Text="Group Name"></asp:Label>
            </td>
            <td   style="width:1%;">
                </td>
            <td  style="width:55%;">
                    <asp:TextBox ID="txtHeadName" runat="server"  class="NormalText"
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" MaxLength="50"   ></asp:TextBox>
            </td>
        </tr>
        
        <tr id="rowCheckList1" style="display:none;">
            <td style="width:33%; text-align: right;font-family:Cambria;">
                Max.Mark</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                <asp:TextBox ID="txtMaxMark" runat="server" 
                    Width="40%" class="NormalText" style=" font-family:Cambria;font-size:10pt;" MaxLength="4" 
                    onkeypress="return NumericWithDot(this,event);" ></asp:TextBox>
            </td>
        </tr>

        <tr id="rowCheckList2" style="display:none;">
            <td style="width:33%; text-align: right;font-family:Cambria;">
                Description</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                     <asp:TextBox ID="txtDescription" runat="server"  class="NormalText"
                        style=" font-family:Cambria;font-size:10pt;" Width="40%" 
                        MaxLength="50"   ></asp:TextBox>
            </td>
        </tr>

        <tr id="rowSPM" style="display:none">
            <td style="width:33%; text-align: right;font-family:Cambria;">
                SPM 
                Related</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <input id="chkSPM" type="checkbox" /></td>
        </tr>
        <tr id="rowType" style="display:none">
            <td style="width:33%; text-align: right;font-family:Cambria;">
                Type</td>
            <td   style="width:1%;">
                &nbsp;</td>
            <td  style="width:55%;">
                    <input id="rdGeneral" name="Grp1" type="radio" checked="checked" 
                     value="1" />&nbsp;&nbsp;&nbsp;General&nbsp;&nbsp;&nbsp;<input id="rdSerious" name="Grp1" 
                        type="radio"  value="2" />&nbsp;&nbsp;&nbsp;Serious&nbsp;&nbsp;&nbsp;<input 
                    id="rdVSerious" name="Grp1" 
                        type="radio" value="3" />&nbsp;&nbsp;&nbsp;Very Serious&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family:Cambria;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family:Cambria;" colspan="3">
           <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="SAVE"  onclick="return SaveOnClick()" />
                 
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%; height: 25px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnType" runat="server" />
</asp:Content>
