﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"   CodeFile="AuditChecklistApplicability.aspx.vb" Inherits="AuditChecklistApplicability" EnableEventValidation="false"  %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br /><div>
    <table align="center" style="width:50%; margin: 0px auto;">
        <tr>
            <td style="width:30%;text-align:left;">
                Branch</td>
            <td  style="width:40%;">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:30%;">
                </td>
        </tr>
        <tr>
            <td style="width:30%;text-align:left;">
                Audit Type</td>
            <td  style="width:40%;">
                <asp:DropDownList ID="cmbAuditType" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:30%;">
                </td>
        </tr>
 <tr>
            <td style="width:30%;text-align:left;">
                Group</td>
            <td  style="width:40%;">
                <asp:DropDownList ID="cmbGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width:30%;">
                </td>
        </tr>


        <tr>
            <td style="width:30%;"></td>
            <td  style="width:40%;"></td>
            <td style="width:30%;"></td>
        </tr>
    </table>
    <br />
    <br />

    <table align="center" style="width:100%; margin: 0px auto;">
           <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                    <asp:Panel ID="pnAudit" runat="server"></asp:Panel>
                    <asp:HiddenField ID="hid_dtls" runat="server" />
                    <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                    </td>
           </tr>
            <tr style="text-align:center;">
                   
           </tr>
        <tr style="text-align:center;">
                <td  colspan="4" style="text-align:center;">
                    <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;
                    <input id="btnExit" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                
                    <asp:HiddenField 
                        ID="hdnAudit" runat="server" />
                
               </td>
         </tr>
    </table>
    <br />
    <br />
    </div> 
    <script src="../Script/Validations.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../Home.aspx", "_self");
        }
        function BranchGroupOnChange() {
            var Branch_Id = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var Group_Id = document.getElementById("<%= cmbGroup.ClientID %>").value;
          
            if ((Branch_Id != "") && (Group_Id != "")) {
                var ToData = "1Ø" + Branch_Id + "Ø" + Group_Id;
                ToServer(ToData, 1);
            }
        }
        function AuditTypeOnChange() {
            var AuditType_Id = document.getElementById("<%= cmbAuditType.ClientID %>").value;
            if (AuditType_Id != 0) {
                var ToData = "2Ø" + AuditType_Id;
                ToServer(ToData, 2);
            }
        }
        function table_fill() {
            document.getElementById("<%= pnAudit.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center'>Sl No</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>Sub_group_id</td>";
            tab += "<td style='width:10%;text-align:center'>Sub Group</td>";
            tab += "<td style='width:0%;text-align:center;display:none;'>Item_id</td>";
            tab += "<td style='width:20%;text-align:center'>Check List</td>";
            tab += "<td style='width:5%;text-align:center'>Sample Type</td>";
            tab += "<td style='width:5%;text-align:center'>Category</td>";
            tab += "<td style='width:5%;text-align:center'>Max.Mark</td>";
            tab += "<td style='width:10%;text-align:center'>Description</td>";
            tab += "<td style='width:5%;text-align:center'>Applicability</td>";
            tab += "</tr>";

            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }

                    tab += "<td style='width:2%;text-align:center;'>" + n + "</td>";
                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[0] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:0%;text-align:left;display:none;'>" + col[2] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:5;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";

                    var select = "<select id='cmb" + n + "' class='NormalText' name='cmb" + n + "' style='width:100%' >";

                    select += "<option value='-1' selected=true>-Select-</option>";
                    if ((col[8] == 1))
                        select += "<option value='1' selected=true>YES  </option>";
                    else
                        select += "<option value='1'>YES  </option>";
                    if (col[8] == 2)
                        select += "<option value='2' selected=true>NO  </option>";
                    else
                        select += "<option value='2'>NO  </option>";
                    
                 
                    tab += "<td style='width:5%;text-align:left'>" + select + "</td>";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnAudit.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//
        }
        function FromServer(arg, context) {
            switch (context) {

                case 1: // table filling
                    {

                        document.getElementById("<%= hid_dtls.ClientID %>").value = arg;
                        alert(document.getElementById("<%= hid_dtls.ClientID %>").value);
                        table_fill();
                        break;
                    }
                case 2: //  group filling
                    {
                        var Data = arg.split("Ø");
                        ComboFill(arg, "<%= cmbGroup.ClientID %>");
                        break;
                    }
                case 3:
                    {
                        var Data = arg.split("ʘ");
                        alert(Data[1]);
                        if (Data[0] == 0) {
                            window.open("AuditChecklistApplicability.aspx", "_self");
                        }
                        break;

                    }
              
            }
        }
        function btnSave_onclick() {

            var BranchId = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var AuditType = document.getElementById("<%= cmbAuditType.ClientID %>").value;
            var Group = document.getElementById("<%= cmbGroup.ClientID %>").value;


            if (BranchId == -1) {
                alert("Select Branch");
                document.getElementById("<%= cmbBranch.ClientID %>").focus();
                return false;
            }

            if (AuditType == -1) {
                alert("Select Audit Type");
                document.getElementById("<%= cmbAuditType.ClientID %>").focus();
                return false;
            
            }
            if (Group == -1) {
                alert("Select Group");
                document.getElementById("<%= cmbGroup.ClientID %>").focus();
                return false;
            }
            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (document.getElementById("cmb" + (n+1)).value != -1) {
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[4] + "µ" + document.getElementById("cmb" + (n + 1)).value;
                }
                alert(document.getElementById("<%= hid_temp.ClientID %>").value);

            }
            alert(document.getElementById("<%= hid_temp.ClientID %>").value);

            alert("4");
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "") {
                alert("Nothing to save");
                return false;
            }
            var ToData = "3Ø" + BranchId + "Ø" + AuditType + "Ø" + Group + "Ø" + document.getElementById("<%= hid_temp.ClientID %>").value;

            ToServer(ToData, 3);
        }
      
// ]]>
    </script>
</asp:Content>

