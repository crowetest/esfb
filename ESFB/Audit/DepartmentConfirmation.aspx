﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="DepartmentConfirmation.aspx.vb" Inherits="Audit_DepartmentConfirmation" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <table align="center" style="width: 90%; margin: 0px auto;" >
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%">
                Remark&nbsp;<asp:Label ID="lblNumber" runat="server"></asp:Label></td>
            <td colspan="3">
                <asp:TextBox ID="txtRemarks" class="NormalText" runat="server" Width="100%" 
                MaxLength="500" Rows="5" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  ></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center ">
                <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" 
                    onclick="return btnConfirm_onclick()" onclick="return btnConfirm_onclick()" /><input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center ">
                &nbsp;</td>
        </tr>
      
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                <asp:HiddenField ID="hdnSINo" runat="server" />
            </td>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:30%">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function btnConfirm_onclick() {     
            var Dtl = document.getElementById("<%= hdnSINo.ClientID %>").value.split("ÿ");              
            var ObservationDtl=""
            var i = 0;
            for (a = 0; a < Dtl.length; a++) {
                 i+=1;
                var ObserveDtl = Dtl[a].split("~");
                if(document.getElementById("chk"+i).checked==true)
                {
                    if (ObservationDtl != "")
                    { ObservationDtl += "ÿ"; }
                    ObservationDtl += ObserveDtl[9];
                }
            }           
            if(ObservationDtl=="")
            { alert("Select atleast one observation");return false;}
            var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
            if (Remarks == "")
            { alert("Enter Remarks"); document.getElementById(("<%= txtRemarks.ClientID %>")).focus(); return false; }          
            var Data = "1Ø" + ObservationDtl + "Ø" + Remarks ;           
            ToServer(Data, 1);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == "0")
                {window.open("DepartmentConfirmation.aspx","_self");}
            }

        }

        function table_fill() {           
            var Dtl = document.getElementById("<%= hdnSINo.ClientID %>").value.split("ÿ");
            var tab = "";
            tab += "<div  style='width:100%; height:100%;  overflow:auto;   align:left; '  class=mainhead>";
            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
            tab += "<tr class='tblBody'>";
            tab += "<td style='width:5%;  text-align:center' >#</td>";
             tab += "<td style='width:10%;  text-align:center'>Branch Name</td>";
            tab += "<td style='width:10%;  text-align:center' >Ref No</td>";
            tab += "<td style='width:12%;  text-align:center'>Client Name</td>";
            tab += "<td style='width:12%; text-align:center'>Center Name</td>";
            tab += "<td style='width:16%;  text-align:center'>CheckList</td>";
            tab += "<td style='width:10%; text-align:center'>Audit Observation</td>";
            tab += "<td style='width:10%;  text-align:center'>Last Remarks</td>";
            tab += "<td style='width:5%;  text-align:center'>Type</td>";
            tab += "<td style='width:5%;  text-align:center'>Staffs Invld</td>";
            tab += "<td style='width:5%;  text-align:center'>Confirm</td>";    

            tab += "</tr>";
            tab += "</table>";
            if (document.getElementById("<%= hdnSINo.ClientID %>").value != "") {
                tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
                var row_bg = 0;
                var i = 0;
                for (a = 0; a < Dtl.length; a++) {
                    var ObserveDtl = Dtl[a].split("~");
                    i += 1;
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_first'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_second'>";
                    }
                    tab += "<td style='width:5%;  text-align:center'>" + i + "</td>";
                     tab += "<td style='width:10%;  text-align:center'>" + ObserveDtl[10] + "</td>";
                    tab += "<td style='width:10%;  text-align:left' ><a href=Reports/viewLoanDetails.aspx?LoanNo=" + btoa(ObserveDtl[2]) + " target=_blank>" + ObserveDtl[2] + "</a></td>";
                    tab += "<td style='width:12%;  text-align:left'>" + ObserveDtl[3] + "</td>";
                    tab += "<td style='width:12%;  text-align:left'>" + ObserveDtl[5] + "</td>";
                    tab += "<td style='width:16%; text-align:left'>" + ObserveDtl[4] + "</td>";
                    tab += "<td style='width:10%;  text-align:left'>" + ObserveDtl[1] + "</td>";
                    tab += "<td style='width:10%;  text-align:left'>" + ObserveDtl[6] + "&nbsp;<a href =Reports/viewPrevRemarksReport.aspx?AuditID=" + btoa(ObserveDtl[0]) + " target=_blank style='cursor:pointer;'>Previous</a>" + "</td>";
                                                        
                    tab += "<td style='width:5%;  text-align:center'>" + ObserveDtl[7] + "</td>";
                    if (ObserveDtl[8] != "0")
                        tab += "<td style='width:5%;  text-align:center'><a href='Reports/viewStaffInvolvement.aspx?SINo=" + btoa(ObserveDtl[0]) + "' target=blank style='cursor:pointer;'>" + ObserveDtl[8] + "</a></td>";
                    else
                        tab += "<td style='width:5%;  text-align:center'>-</td>";
                    tab += "<td style='width:5%;  text-align:center'><input id='chk" + i + "' onclick='return btnRespond_onclick(" + i + ")'  style='font-family: cambria; width: 45%; cursor: pointer;' type='checkbox' ></td>";                                 
                    tab += "</tr>";
                }
                tab += "</table></div>";
            }           
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
             
        }

        function btnRespond_onclick(value) {
            var Dtl = document.getElementById("<%= hdnSINo.ClientID %>").value.split("ÿ");            
            var i = 0;
            var j = 0;
            var ObserveDtl = Dtl[value].split("~");        
            if (document.getElementById("chk" + value).checked == true) {
                j += 1;              
            }
            for (a = 0; a < Dtl.length; a++) {
                ObserveDtl = Dtl[a].split("~");
                i += 1;

                if (i != value && document.getElementById("chk" + i).checked == true) {
                    j += 1;

                }
            }
            if (j > 0) {                
                var disp = "Observations";
                if (j == 1)
                    disp = "Observation";
                document.getElementById("<%= lblNumber.ClientID %>").innerHTML = "(for " + j + " " + disp + " )";
            }
            
        }
// ]]>
    </script>
</asp:Content>

