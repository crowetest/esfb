﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AuditIntervalApproval
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AD As New Audit
    Dim CallBackReturn As String = Nothing
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then
            Dim RequestDtl As String = Data(1)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(3) As SqlParameter
                Params(0) = New SqlParameter("@RequestDtl", SqlDbType.VarChar, 5000)
                Params(0).Value = RequestDtl.ToString()
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDIT_INTERVAL_APPROVAL", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
    Protected Sub Audit_BranchResponseApproval_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 242) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Audit Interval Approval"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = DB.ExecuteDataSet("select request_id,b.Branch_Name,cast(a.prev_Interval as varchar)+case when a.prev_Interval>1 then ' Months' else 'Month' end as PrevInterval,cast(a.Interval as varchar)+case when a.Interval>1 then ' Months' else 'Month' end as Interval FROM audit_interval_approval a,branch_master b where a.Branch_ID=b.Branch_ID and a.status_id=1").Tables(0)
            hdnDtls.Value = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                hdnDtls.Value += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString()
                If n < DT.Rows.Count - 1 Then
                    hdnDtls.Value += "¥"
                End If
            Next
            Dim Lst As New ListItem
            Lst.Value = CStr(Month(CDate(Session("TraDT"))))
            Lst.Text = CStr(MonthName(Month(CDate(Session("TraDT")))))
            cmbMonth.Items.Add(Lst)
            Dim Lst1 As New ListItem

            Dim TraDt As New DateTime
            TraDt = CDate(Session("TraDt")).AddMonths(1)

            Lst1.Value = CStr(Month(TraDt))
            Lst1.Text = CStr(MonthName(Month(TraDt)))
            cmbMonth.Items.Add(Lst1)

            Dim LstYear As New ListItem
            Dim Mnth As Integer = Month(CDate(Session("TraDt")))
            Dim Yr As Integer = Year(CDate(Session("TraDt")))
            LstYear.Value = Yr.ToString()
            LstYear.Text = Yr.ToString()
            cmbYear.Items.Add(LstYear)
            If Mnth = 12 Then
                Dim LstYear1 As New ListItem
                LstYear1.Value = (Yr + 1).ToString()
                LstYear1.Text = (Yr + 1).ToString()
                cmbYear.Items.Add(LstYear1)
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "FillApproveDetails();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
