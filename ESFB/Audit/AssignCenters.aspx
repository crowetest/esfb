﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AssignCenters.aspx.vb" Inherits="Audit_AssignCenters" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <table align="center" style="width: 65%;text-align:right; margin:0px auto;">
        <tr>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:25%; text-align:left;">
                Audit</td>
            <td style="width:25%; text-align:left;" colspan="3">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="80%" ForeColor="Black">
                </asp:DropDownList> </td>
          
        </tr>
            <tr>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
        </tr>
           <tr  id="row1" style="display:none;">
            <td style="width:25%; text-align:left; height:25px; " colspan="4" class="sub_second">
                <table align="center" style="width: 100%">
                    <tr>
                        <td style="width:10%; text-align:center; font-weight:bold" colspan="10">
                           Selected&nbsp;&nbsp;&nbsp; <asp:Label ID="lblBRSelCount" runat="server" Text="0"></asp:Label>&nbsp;&nbsp;/&nbsp;&nbsp;<asp:Label ID="lblBRCount" runat="server" Text="0"></asp:Label>&nbsp;&nbsp;&nbsp;Record for Branch Audit and&nbsp;&nbsp;&nbsp; <asp:Label ID="lblFLSelCount" runat="server" Text="0"></asp:Label>&nbsp;&nbsp;/&nbsp;&nbsp;<asp:Label ID="lblFLCount" runat="server" Text="0"></asp:Label>&nbsp;&nbsp;&nbsp;Record for 
                            Sangam Audit</td>
                       
                    </tr>
                </table>
            </td>
         
        </tr>
        <tr>
            <td style="width:25%; text-align:left;" colspan="4">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
         
        </tr>  
        <tr>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
            <td style="width:25%; text-align:left;">
                &nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:25%; text-align:center;" colspan="4">
                <asp:HiddenField ID="hid_dtls" runat="server" />
                 <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnTypeID" runat="server" />
                <asp:HiddenField ID="hid_BrRate" runat="server" />
                <asp:HiddenField ID="hid_FldRate" runat="server" />
                <asp:HiddenField ID="hid_BranchCount" runat="server" />
                <asp:HiddenField ID="hid_FldCount" runat="server" />
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" /><input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
         
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

    
        function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function AuditOnChange()
        {
            var AuditID=document.getElementById("<%= cmbAudit.ClientID %>").value;
            if(AuditID!="-1")
            {
                var Dtl=document.getElementById("<%= cmbAudit.ClientID %>").value.split("~") ;
                var ToData = "2Ø" + Dtl[1]+"Ø"+Dtl[0];              
                ToServer(ToData, 2);
            }else{
            document.getElementById("<%= hid_dtls.ClientID %>").value="";
            table_fill();
            }
        }

          function FromServer(arg, context) {

         switch (context) {
             case 1:
                 var Data = arg.split("Ø");
                 alert(Data[1]);
                 if (Data[0] == 0) { window.open("AssignCenters.aspx?TypeID="+ document.getElementById("<%= hdnTypeID.ClientID %>").value, "_self"); }
                 break;
                case 2:
                    var Dtl=arg.split("Ø");
                    document.getElementById("<%= hid_dtls.ClientID %>").value=Dtl[0];
                    document.getElementById("<%= hid_BrRate.ClientID %>").value=Dtl[1];
                    document.getElementById("<%= hid_FldRate.ClientID %>").value=Dtl[2];
                    document.getElementById("<%= hid_BranchCount.ClientID %>").value=Dtl[3];
                    document.getElementById("<%= hid_FldCount.ClientID %>").value=Dtl[4];
                    table_fill() ;
                    break;
               }
            
        }

        function table_fill() 
        {
        if(document.getElementById("<%= hid_dtls.ClientID %>").value=="")
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = 'none'; 
            document.getElementById("row1").style.display = 'none'; 
                
        }
        else
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            if(document.getElementById("<%= hid_BrRate.ClientID %>").value!="-1")
            {        
                document.getElementById("<%= lblBRSelCount.ClientID %>").innerHTML=Math.abs(document.getElementById("<%= hid_BranchCount.ClientID %>").value);
                document.getElementById("<%= lblFLSelCount.ClientID %>").innerHTML=Math.abs(document.getElementById("<%= hid_FldCount.ClientID %>").value);
                document.getElementById("row1").style.display = '';           
            }
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px;' >";           
            tab += "<td style='width:5%;text-align:center' >#</td>";
            tab += "<td style='width:13%;text-align:left'>Center ID</td>";
            tab += "<td style='width:20%;text-align:left'>Center Name</td>";
            tab += "<td style='width:12%;text-align:left'>Formed On</td>";
            tab += "<td style='width:12%;text-align:left'>Meeting Day</td>";     
            tab += "<td style='width:14%;text-align:left'>Branch Audit</br><input type='checkbox' id='chkBASelectAll' style='display:none;' onclick='SelectOnClick(-1,1)' /></td>";
            tab += "<td style='width:14%;text-align:left'>Sangam Audit</br><input type='checkbox' id='chkFASelectAll' style='display:none;'  onclick='SelectOnClick(-1,2)' /></td>";    
            tab += "<td style='width:10%;text-align:left'>Last Visit</td>";    
            tab += "</tr>";      
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:254px; overflow-y: scroll;;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>"; 
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        if(col[6]==1)
                        {
                            tab += "<tr class='sub_first';>";
                        }
                        else
                        {
                            tab += "<tr class='sub_first'; style='background-color:#FAE9E3;'>";
                        }
                    }
                    else {
                        row_bg = 0;
                        if(col[6]==1)
                        {
                            tab += "<tr class='sub_second';>";
                        }
                        else
                        {
                            tab += "<tr class='sub_second'; style='background-color:#FAE9E3;'>";
                        }
                        
                    }
                    i = n + 1;
                    //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time                    
                 
                    tab += "<td style='width:5%;text-align:center'>" + i + "</td>";
                    tab += "<td style='width:13%;text-align:left'>" + col[0] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[1] + "</td>";      
                    tab += "<td style='width:12%;text-align:left'>" + col[3] + "</td>";                 
                    tab += "<td style='width:12%;text-align:left'>" + col[2] + "</td>";
                    if(col[4]==1){
                    tab += "<td style='width:14%;text-align:center' ><input type='checkbox' id='chkSelectBA" + i + "' onclick='SelectOnClick("+ i +",1)' disabled='true' checked='checked'  /></td>"; 
                    }
                    else{
                    tab += "<td style='width:14%;text-align:center' ><input type='checkbox' id='chkSelectBA" + i + "' onclick='SelectOnClick("+ i +",1)'  /></td>"; 
                    }
                     if(col[5]==1){
                    tab += "<td style='width:14%;text-align:center' ><input type='checkbox' id='chkSelectFA" + i + "' onclick='SelectOnClick("+ i +",2)' disabled='true' checked='checked' /></td>"; 
                    }
                    else{
                    tab += "<td style='width:14%;text-align:center' ><input type='checkbox' id='chkSelectFA" + i + "' onclick='SelectOnClick("+ i +",2)' /></td>"; 
                    }    
                     if(col[7]=="01 Jan 2014")
                        tab += "<td style='width:10%;text-align:left' >Not Visited</td>";  
                    else
                        tab += "<td style='width:10%;text-align:left' >"+col[7]+ " </td>";                              
                                      
                    tab += "</tr>";                    
                }
            }
            tab += "</table></div>";
            document.getElementById("<%= lblBRCount.ClientID %>").innerHTML=document.getElementById("<%= hid_BrRate.ClientID %>").value;
            document.getElementById("<%= lblFLCount.ClientID %>").innerHTML=document.getElementById("<%= hid_FldRate.ClientID %>").value;
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            }
            //--------------------- Clearing Data ------------------------//
        }

 function SelectOnClick(val,type)
            {    
                if(val<0)
                {    
                  if (type==1)    
                  {          
                        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");                      
                        if(document.getElementById("chkBASelectAll").checked==false && document.getElementById("chkFASelectAll").checked==true) 
                         {document.getElementById("chkBASelectAll").checked=true; return false;  }                   
                        for (n = 0; n <= row.length - 1; n++) {
                         i = n + 1;
                        col = row[n].split("µ");
                        if (document.getElementById("chkSelectFA"+i).checked==true)
                            document.getElementById("chkSelectBA"+i).checked=true
                        else
                        document.getElementById("chkSelectBA"+i).checked=document.getElementById("chkBASelectAll").checked; 
                                
                       }                   
                     
                  
                   }
                   else  if (type==2)    
                  {          
                        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                        var state=true;
                        if(document.getElementById("chkFASelectAll").checked==true)
                           state=false;
                        for (n = 0; n <= row.length - 1; n++) {
                         i = n + 1;
                        col = row[n].split("µ");
                        if(document.getElementById("chkFASelectAll").checked==true)
                            document.getElementById("chkSelectBA"+i).checked=document.getElementById("chkFASelectAll").checked;   
                        document.getElementById("chkSelectFA"+i).checked=document.getElementById("chkFASelectAll").checked;                      
                       }   
                       if(document.getElementById("chkFASelectAll").checked==true)
                       {
                         document.getElementById("chkBASelectAll").checked= document.getElementById("chkFASelectAll").checked;       
                       }          
                      
                   
                   } 
                }
                else
                {
                 if (type==1)    
                  {    
                  
                     if(document.getElementById("chkSelectFA"+val).checked==true)
                         {document.getElementById("chkSelectBA"+val).checked=true;  return false;} 
                        if (document.getElementById("chkSelectBA"+val).checked==false)
                        {
                            document.getElementById("chkBASelectAll").checked==false;
                           
                        }
                       

                  }
                 else if(type==2)    
                {                                     
                    document.getElementById("chkFASelectAll").checked=false; 
                    if (document.getElementById("chkSelectFA"+val).checked==true)
                    {
                       document.getElementById("chkSelectBA"+val).checked =document.getElementById("chkSelectFA"+val).checked; 
                      
                    }
                   
                }
              }
              var BrSelCount=0;
              var FldSelCount=0;
                 for (n = 0; n <= row.length - 1; n++) {
                         i = n + 1;
                        col = row[n].split("µ");
                        if (document.getElementById("chkSelectFA"+i).checked==true && document.getElementById("chkSelectFA"+i).disabled==false)
                        FldSelCount+=1;
                        if( document.getElementById("chkSelectBA"+i).checked==true  && document.getElementById("chkSelectBA"+i).disabled==false)
                        BrSelCount+=1;
                  }
               BrSelCount+=Math.abs(document.getElementById("<%= hid_BranchCount.ClientID %>").value);
               FldSelCount+=Math.abs(document.getElementById("<%= hid_FldCount.ClientID %>").value);
              document.getElementById("<%= lblBRSelCount.ClientID %>").innerHTML=BrSelCount;
              document.getElementById("<%= lblFLSelCount.ClientID %>").innerHTML=FldSelCount;
            }


function btnSave_onclick() {

   row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
   document.getElementById("<%= hdnValue.ClientID %>").value ="";
     for (n = 0; n <= row.length - 1; n++) {
                         i = n + 1;
                        col = row[n].split("µ");  
                          
                        var status=0                   
                        if(document.getElementById("chkSelectBA"+i).checked==true &&  document.getElementById("chkSelectFA"+i).checked==true)  
                            {
                                if(col[4]==1)
                                {
                                    if (col[5]!=1){
                                        status=3; 
                                    }
                                
                                }
                                else 
                                {
                                    status=2;
                                    if (col[5]!=1)
                                    {
                                        status=1;
                                    }
                                }
                          }
                        else if(document.getElementById("chkSelectBA"+i).checked==true) 
                           { if(col[4]!=1){  
                             status=2;}
                             }
                        else if(document.getElementById("chkSelectFA"+i).checked==true)   
                           { if(col[5]!=1){  
                             status=3;} 
                             }
                        if(status>0)     
                           document.getElementById("<%= hdnValue.ClientID %>").value   +="¥"+ col[0]+"µ"+status;
                       }   
                  
             var Dtl=document.getElementById("<%= cmbAudit.ClientID %>").value.split("~") ;
             var ToData = "1Ø" + Dtl[0]+"Ø"+document.getElementById("<%= hdnValue.ClientID %>").value;              
             ToServer(ToData, 1);
}

// ]]>
    </script>
</asp:Content>

