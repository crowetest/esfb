﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class UpdateMasterData
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
#Region "Page Load & Dispose"
    Protected Sub Election_EditElectionBooth_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 240) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Update Group/SubGroup/CheckList"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim DR As DataRow
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'Dim UserID As Integer = CInt(Session("UserID"))
        Select Case CInt(Data(0))
            Case 1
                Dim ID As Integer = CInt(Data(1))
                CallBackReturn = ""
                If (ID = 1) Then 'Group
                    DT = GF.GetQueryResult("select convert(varchar(10),group_id)+'Æ'+group_name+'Æ'+convert(varchar(10),audit_type) from dbo.AUDIT_CHECK_GROUPS where status_id=1 order by group_name")


                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "¶"
                    DT = GF.GetQueryResult("select type_id,type_name from audit_type where status_id=1 order by 2")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                ElseIf (ID = 2) Then 'SubGroup
                    DT = GF.GetQueryResult("select convert(varchar(10),sub_group_id)+'Æ'+sub_group_name+'Æ'+convert(varchar(10),b.group_id)+'Æ'+b.group_name+'Æ'+d.type_name from dbo.AUDIT_CHECK_SUBGROUPS a,AUDIT_CHECK_GROUPS b,audit_type d" & _
                                           " where a.group_id=b.group_id and  b.audit_type=d.type_id and a.status_id=1 order by sub_group_name")

                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "¶"
                    DT = GF.GetQueryResult("select group_ID,group_Name from AUDIT_CHECK_GROUPS  where status_id=1 order by 2")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                ElseIf (ID = 3) Then 'CheckList
                    DT = GF.GetQueryResult("select convert(varchar(10),item_id)+'Æ'+item_name+'Æ'+convert(varchar(10),a.sub_group_id)+'Æ'+b.group_name+'Æ'+d.type_name+'Æ'+convert(varchar(10),c.SPM_FLAG)+'Æ'+convert(varchar(10),C.SERIOUS_FLAG)+'Æ'+convert(varchar(10),C.VERY_SERIOUS_FLAG) " & _
                                           " from dbo.AUDIT_CHECK_LIST c,dbo.AUDIT_CHECK_SUBGROUPS a,AUDIT_CHECK_GROUPS b,audit_type d where " & _
                                           " c.sub_group_id=a.sub_group_id and a.group_id=b.group_id and b.audit_type=d.type_id and c.status_id=1 order by item_name")


                    If DT.Rows.Count > 0 Then
                        For Each DR In DT.Rows
                            CallBackReturn += CStr(DR(0)) + "ʘ"
                        Next
                    End If
                    CallBackReturn += "¶"
                    DT = GF.GetQueryResult("select sub_group_ID,sub_group_Name from AUDIT_CHECK_SUBGROUPS where status_id=1")
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim ID As Integer = CInt(Data(1))
                Dim ChangeData As String = Data(2)
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Try
                    Dim Params(4) As SqlParameter
                    Params(0) = New SqlParameter("@ChangeID", SqlDbType.Int)
                    Params(0).Value = ID
                    Params(1) = New SqlParameter("@ChangeData", SqlDbType.VarChar)
                    Params(1).Value = ChangeData
                    Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(2).Value = UserID
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 500)
                    Params(4).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_AUDIT_UPDATE_MASTER_DATA", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

                End Try
                CallBackReturn = ErrorFlag.ToString + "ʘ" + Message
        End Select
    End Sub
#End Region
    Private Function DR(ByVal p1 As Integer) As Object
        Throw New NotImplementedException
    End Function
End Class
