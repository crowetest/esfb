﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class QueryResponseTracker
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim PostID As Integer
    Dim UserID As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 216) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))
            UserID = CInt(Session("UserID"))
            Dim DT As New DataTable
            RH.Heading(Session("FirmName"), tb, "QUERY RESPONSE TRACKER", 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim PendingTotal As Integer = 0
            Dim CompletedTotal As Integer = 0
            Dim Total As Integer = 0
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "l", "Region")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "c", "Closed")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "c", "Pending")
            RH.AddColumn(TRHead, TRHead_04, 20, 20, "c", "Total")

            tb.Controls.Add(TRHead)
            Dim SqlStr As String
            If PostID = 2 Then
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e,Audit_Admin_Branches f where d.Branch_id=f.Branch_ID and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and (f.emp_code=" + UserID.ToString() + " )  group by e.Region_ID,e.Region_Name "
            ElseIf PostID = 6 Then
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and  d.branch_id in(select branch_id from brmaster where area_head=" + Session("UserID") + ")  group by e.Region_ID,e.Region_Name "
            ElseIf PostID = 7 Then
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and  d.branch_id in(select branch_id from brmaster where region_head=" + Session("UserID") + ")  group by e.Region_ID,e.Region_Name "
            ElseIf PostID = 8 Then
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and  d.branch_id in(select branch_id from brmaster where zone_head=" + Session("UserID") + ")  group by e.Region_ID,e.Region_Name "
            ElseIf PostID = 3 Then
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and  d.branch_id in(select branch_id from audit_branches where team_lead=" + Session("UserID") + ")  group by e.Region_ID,e.Region_Name "
            ElseIf PostID = 4 Then
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and d.branch_id in(select branch_id from audit_branches where emp_code=" + Session("UserID") + ")  group by e.Region_ID,e.Region_Name "
            Else
                SqlStr = "select e.Region_ID,upper(e.Region_Name)as RegionName,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where  a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2   group by e.Region_ID,e.Region_Name "
            End If
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows
                LineNumber += 1
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid

                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", " <a href='QueryResponseTracker_Area.aspx?RegionID=" + GN.Encrypt(DR(0).ToString()) + "' target='_blank'>" + DR(1).ToString())
                If (CInt(DR(2) > 0)) Then
                    RH.AddColumn(TR3, TR3_02, 20, 20, "c", " <a href='QueryResponseTracker_Observation.aspx?StatusID=0 &RegionID=" + GN.Encrypt(DR(0).ToString()) + " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &GroupID=-1' target='_blank'>" + DR(2).ToString())
                Else
                    RH.AddColumn(TR3, TR3_02, 20, 20, "c", DR(2).ToString())
                End If
                If (CInt(DR(3) > 0)) Then
                    RH.AddColumn(TR3, TR3_03, 20, 20, "c", "  <a href='QueryResponseTracker_Observation.aspx?StatusID=1 &RegionID=" + GN.Encrypt(DR(0).ToString()) + " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &GroupID=-1' target='_blank'>" + DR(3).ToString())
                Else
                    RH.AddColumn(TR3, TR3_03, 20, 20, "c", DR(3).ToString())
                End If
                If CInt(DR(4)) > 0 Then
                    RH.AddColumn(TR3, TR3_04, 20, 20, "c", "  <a href='QueryResponseTracker_Observation.aspx?StatusID=-1 &RegionID=" + GN.Encrypt(DR(0).ToString()) + " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &GroupID=-1' target='_blank'>" + DR(4).ToString())
                Else
                    RH.AddColumn(TR3, TR3_04, 20, 20, "c", DR(4).ToString())
                End If

                PendingTotal += CInt(DR(3))
                CompletedTotal += CInt(DR(2))
                Total += CInt(DR(4))
                tb.Controls.Add(TR3)
            Next
            Dim TREnd As New TableRow
            TREnd.BackColor = Drawing.Color.WhiteSmoke
            Dim TREnd_00, TREnd_01, TREnd_02, TREnd_03, TREnd_04 As New TableCell

            TREnd_00.BorderWidth = "1"
            TREnd_01.BorderWidth = "1"
            TREnd_02.BorderWidth = "1"
            TREnd_03.BorderWidth = "1"
            TREnd_04.BorderWidth = "1"


            TREnd_00.BorderColor = Drawing.Color.Silver
            TREnd_01.BorderColor = Drawing.Color.Silver
            TREnd_02.BorderColor = Drawing.Color.Silver
            TREnd_03.BorderColor = Drawing.Color.Silver
            TREnd_04.BorderColor = Drawing.Color.Silver


            TREnd_00.BorderStyle = BorderStyle.Solid
            TREnd_01.BorderStyle = BorderStyle.Solid
            TREnd_02.BorderStyle = BorderStyle.Solid
            TREnd_03.BorderStyle = BorderStyle.Solid
            TREnd_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TREnd, TREnd_00, 5, 5, "c", "")
            RH.AddColumn(TREnd, TREnd_01, 35, 35, "l", "Total")

            If CompletedTotal > 0 Then
                RH.AddColumn(TREnd, TREnd_02, 20, 20, "c", " <a href='QueryResponseTracker_Observation.aspx?StatusID=0 &RegionID=" & GN.Encrypt("-1") & " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &GroupID=-1' target='_blank'>" + CompletedTotal.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_02, 20, 20, "c", CompletedTotal.ToString())
            End If
            If PendingTotal > 0 Then
                RH.AddColumn(TREnd, TREnd_03, 20, 20, "c", " <a href='QueryResponseTracker_Observation.aspx?StatusID=1 &RegionID=" & GN.Encrypt("-1") & " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &GroupID=-1' target='_blank'>" + PendingTotal.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_03, 20, 20, "c", PendingTotal.ToString())
            End If
            If Total > 0 Then
                RH.AddColumn(TREnd, TREnd_04, 20, 20, "c", " <a href='QueryResponseTracker_Observation.aspx?StatusID=-1 &RegionID=" & GN.Encrypt("-1") & " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &GroupID=-1' target='_blank'>" + Total.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_04, 20, 20, "c", Total.ToString())
            End If

            tb.Controls.Add(TREnd)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
