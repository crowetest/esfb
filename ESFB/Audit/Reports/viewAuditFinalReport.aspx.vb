﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewAuditFinalReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            AuditID = CInt(GF.Decrypt(Request.QueryString.Get("AuditID")))
            hdnReportID.Value = GF.Decrypt(Request.QueryString.Get("ReportID"))
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim DepartmentID As Integer
            Dim AuditTypeID As Integer
            DT1 = DB.ExecuteDataSet("SELECT DEPARTMENT_ID FROM EMP_MASTER WHERE EMP_CODE=" + Session("UserID").ToString()).Tables(0)
            If (DT1.Rows.Count > 0) Then
                DepartmentID = CInt(DT1.Rows(0)(0))
            End If
            DTAudit = DB.ExecuteDataSet("SELECT Group_ID,dbo.udfPropercase(d.Branch_Name),isnull(convert(varchar(11),START_DT,106),'') as StartDt,isnull(convert(varchar(11),END_DT,106),'') as End_dt,B.Emp_Name,E.EMP_NAME AS VERIFIED_BY,convert(varchar(11),a.period_from,106),convert(varchar(11),a.period_to,106) FROM AUDIT_MASTER A,EMP_MASTER B,Branch_master D,EMP_MASTER E WHERE A.EMP_CODE=B.Emp_Code and a.Branch_id=D.branch_ID AND A.VERIFIED_BY=E.EMP_CODE AND A.GROUP_ID=" + AuditID.ToString()).Tables(0)
            DTMASTER = DB.ExecuteDataSet("SELECT AUDIT_id,AUDIT_TYPE,General_Comments,Serious_Comments,B.TYPE_NAME FROM AUDIT_DTL a,AUDIT_TYPE B WHERE a.AUDIT_TYPE=B.TYPE_ID AND a.GROUP_ID=" + AuditID.ToString() + " order by B.Type_ID").Tables(0)
            RH.Heading(Session("FirmName"), tb, "AUDIT OBSERVATION REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim TR0 As New TableRow
            Dim TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07 As New TableCell
            RH.AddColumn(TR0, TR0_01, 10, 10, "l", "Branch")
            RH.AddColumn(TR0, TR0_02, 2, 2, "l", " : ")
            RH.AddColumn(TR0, TR0_03, 25, 25, "l", DTAudit.Rows(0)(1).ToString())
            RH.AddColumn(TR0, TR0_04, 26, 26, "l", "")
            RH.AddColumn(TR0, TR0_05, 10, 10, "l", "Auditor")
            RH.AddColumn(TR0, TR0_06, 2, 2, "l", " : ")
            RH.AddColumn(TR0, TR0_07, 25, 25, "l", DTAudit.Rows(0)(4).ToString())

            tb.Controls.Add(TR0)

            Dim TR1 As New TableRow
            Dim TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07 As New TableCell
            RH.AddColumn(TR1, TR1_01, 10, 10, "l", "Period From")
            RH.AddColumn(TR1, TR1_02, 2, 2, "l", " : ")
            RH.AddColumn(TR1, TR1_03, 25, 25, "l", DTAudit.Rows(0)(6).ToString())
            RH.AddColumn(TR1, TR1_04, 26, 26, "l", "")
            RH.AddColumn(TR1, TR1_05, 10, 10, "l", "Period To")
            RH.AddColumn(TR1, TR1_06, 2, 2, "l", " : ")
            RH.AddColumn(TR1, TR1_07, 25, 25, "l", DTAudit.Rows(0)(7).ToString())

            tb.Controls.Add(TR1)


            Dim TR20 As New TableRow
            Dim TR20_01, TR20_02, TR20_03, TR20_04, TR20_05, TR20_06, TR20_07 As New TableCell
            RH.AddColumn(TR20, TR20_01, 10, 10, "l", "Start Date")
            RH.AddColumn(TR20, TR20_02, 2, 2, "l", " : ")
            RH.AddColumn(TR20, TR20_03, 25, 25, "l", DTAudit.Rows(0)(2).ToString())
            RH.AddColumn(TR20, TR20_04, 26, 26, "l", "")
            RH.AddColumn(TR20, TR20_05, 10, 10, "l", "End Date")
            RH.AddColumn(TR20, TR20_06, 2, 2, "l", " : ")
            RH.AddColumn(TR20, TR20_07, 25, 25, "l", DTAudit.Rows(0)(3).ToString())

            tb.Controls.Add(TR20)

            'RH.DrawLine(tb, 100)
            RH.BlankRow(tb, 15)
            Dim GrossTotalLeakage As Double
            Dim NetTotalLeakage As Double
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            For l As Integer = 0 To DTMASTER.Rows.Count - 1
                Dim GROUPID As Integer = 0
                Dim SUBGROUPID As Integer = 0
                Dim ITEMID As Integer = 0
                AuditTypeID = CInt(DTMASTER.Rows(l)(1))
                Dim TRAuditHead As New TableRow
                Dim TRAuditHead_00 As New TableCell
                RH.AddColumn(TRAuditHead, TRAuditHead_00, 100, 100, "c", DTMASTER.Rows(l)(4).ToString())
                'TRAuditHead_00.BackColor = System.Drawing.Color.BurlyWood
                TRAuditHead_00.BackColor = System.Drawing.Color.Gainsboro
                tb.Controls.Add(TRAuditHead)
                If (AuditTypeID = 1) Then
                    DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,dbo.udfPropercase(f.CLIENT_NAME),isnull(f.disbursed_Dt,f.sanctioned_dt),f.loan_amt,b.Fraud,b.Fin_Leakage,f.scheme_id,b.susp_Leakage,b.SINo from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,center_master g where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and a.Audit_ID  =" + DTMASTER.Rows(l)(0).ToString() + " and f.center_ID=g.Center_ID order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                    For Each DR In DT.Rows

                        LineNumber += 1
                        j += 1
                        If (ITEMID <> CInt(DR(5)) And ITEMID <> 0) Then
                            Dim TR33 As New TableRow
                            TR33.BorderWidth = "1"
                            TR33.BorderStyle = BorderStyle.Solid

                            Dim TR33_00, TR33_01 As New TableCell

                            TR33_00.BorderWidth = "1"
                            TR33_01.BorderWidth = "1"


                            TR33_00.BorderColor = Drawing.Color.Silver
                            TR33_01.BorderColor = Drawing.Color.Silver


                            TR33_00.BorderStyle = BorderStyle.Solid
                            TR33_01.BorderStyle = BorderStyle.Solid

                            TR33_00.ForeColor = Drawing.Color.DarkBlue
                            TR33_01.ForeColor = Drawing.Color.DarkRed

                            RH.AddColumn(TR33, TR33_00, 93, 93, "r", "Total Leakage :")
                            RH.AddColumn(TR33, TR33_01, 7, 7, "r", GrossTotalLeakage)

                            GrossTotalLeakage = 0
                            tb.Controls.Add(TR33)
                        End If
                        If (ITEMID <> CInt(DR(5))) Then
                            I = 1
                            Dim TRItemHead As New TableRow
                            TRItemHead.ForeColor = Drawing.Color.DarkBlue
                            Dim TRItemHead_00 As New TableCell
                            RH.BlankRow(tb, 5)
                            RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "l", "<b><i>" + DR(2).ToString() + " / " + DR(4).ToString() + " / " + DR(6).ToString() + "</i></b>")

                            tb.Controls.Add(TRItemHead)
                            RH.BlankRow(tb, 5)
                            Dim TRHead As New TableRow
                            TRHead.BackColor = Drawing.Color.WhiteSmoke
                            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell

                            TRHead_00.BorderWidth = "1"
                            TRHead_01.BorderWidth = "1"
                            TRHead_02.BorderWidth = "1"
                            TRHead_03.BorderWidth = "1"
                            TRHead_04.BorderWidth = "1"
                            TRHead_05.BorderWidth = "1"
                            TRHead_06.BorderWidth = "1"
                            TRHead_07.BorderWidth = "1"
                            TRHead_08.BorderWidth = "1"

                            TRHead_00.BorderColor = Drawing.Color.Silver
                            TRHead_01.BorderColor = Drawing.Color.Silver
                            TRHead_02.BorderColor = Drawing.Color.Silver
                            TRHead_03.BorderColor = Drawing.Color.Silver
                            TRHead_04.BorderColor = Drawing.Color.Silver
                            TRHead_05.BorderColor = Drawing.Color.Silver
                            TRHead_06.BorderColor = Drawing.Color.Silver
                            TRHead_07.BorderColor = Drawing.Color.Silver
                            TRHead_08.BorderColor = Drawing.Color.Silver

                            TRHead_00.BorderStyle = BorderStyle.Solid
                            TRHead_01.BorderStyle = BorderStyle.Solid
                            TRHead_02.BorderStyle = BorderStyle.Solid
                            TRHead_03.BorderStyle = BorderStyle.Solid
                            TRHead_04.BorderStyle = BorderStyle.Solid
                            TRHead_05.BorderStyle = BorderStyle.Solid
                            TRHead_06.BorderStyle = BorderStyle.Solid
                            TRHead_07.BorderStyle = BorderStyle.Solid
                            TRHead_08.BorderColor = Drawing.Color.Silver

                            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                            RH.AddColumn(TRHead, TRHead_01, 13, 13, "l", "Dep. No")
                            RH.AddColumn(TRHead, TRHead_02, 5, 5, "l", "Product")
                            RH.AddColumn(TRHead, TRHead_03, 22, 22, "l", "Customer Name")
                            RH.AddColumn(TRHead, TRHead_04, 12, 12, "l", "Dep. Date")
                            RH.AddColumn(TRHead, TRHead_05, 8, 8, "r", "Amount")
                            RH.AddColumn(TRHead, TRHead_06, 5, 5, "c", "Type")
                            RH.AddColumn(TRHead, TRHead_07, 23, 23, "l", "Remarks")
                            RH.AddColumn(TRHead, TRHead_08, 7, 7, "l", "Est.Amt. Involved")


                            tb.Controls.Add(TRHead)
                            'RH.BlankRow(tb, 3)
                        End If

                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid

                        Dim Type As String = ""
                        If (CInt(DR(13)) = 1) Then
                            Type = "VS"
                        ElseIf (CInt(DR(16)) = 1) Then
                            Type = "S"
                        End If
                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"
                        TR3_08.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver
                        TR3_08.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid
                        TR3_08.BorderStyle = BorderStyle.Solid


                        RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString())
                        RH.AddColumn(TR3, TR3_01, 13, 13, "l", DR(7))
                        RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(15))
                        RH.AddColumn(TR3, TR3_03, 22, 22, "l", DR(10))
                        If Not IsDBNull(DR(11)) Then
                            RH.AddColumn(TR3, TR3_04, 12, 12, "l", CDate(DR(11)).ToString("dd/MM/yyyy"))
                        Else
                            RH.AddColumn(TR3, TR3_04, 12, 12, "l", "")
                        End If
                        RH.AddColumn(TR3, TR3_05, 8, 8, "r", CDbl(DR(12)).ToString())
                        RH.AddColumn(TR3, TR3_06, 5, 5, "c", Type)
                        RH.AddColumn(TR3, TR3_07, 23, 23, "l", DR(9).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(17)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                        RH.AddColumn(TR3, TR3_08, 7, 7, "r", CDbl(DR(14)).ToString("0"))
                        tb.Controls.Add(TR3)
                        I = I + 1
                        GROUPID = CInt(DR(1))
                        SUBGROUPID = CInt(DR(3))
                        ITEMID = CInt(DR(5))
                        GrossTotalLeakage = GrossTotalLeakage + CInt(DR(14))
                        NetTotalLeakage = NetTotalLeakage + CInt(DR(14))
                    Next
                    Dim TR55 As New TableRow
                    TR55.BorderWidth = "1"
                    TR55.BorderStyle = BorderStyle.Solid

                    Dim TR55_00, TR55_01 As New TableCell

                    TR55_00.BorderWidth = "1"
                    TR55_01.BorderWidth = "1"


                    TR55_00.BorderColor = Drawing.Color.Silver
                    TR55_01.BorderColor = Drawing.Color.Silver


                    TR55_00.BorderStyle = BorderStyle.Solid
                    TR55_01.BorderStyle = BorderStyle.Solid


                    TR55_00.ForeColor = Drawing.Color.DarkBlue
                    TR55_01.ForeColor = Drawing.Color.DarkRed

                    RH.AddColumn(TR55, TR55_00, 93, 93, "r", "Total Leakage :")
                    RH.AddColumn(TR55, TR55_01, 7, 7, "r", GrossTotalLeakage)

                    GrossTotalLeakage = 0
                    tb.Controls.Add(TR55)
                ElseIf (AuditTypeID = 2) Then
                    DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,dbo.udfPropercase(f.CLIENT_NAME),isnull(f.disbursed_Dt,f.sanctioned_dt),f.loan_amt,b.Fraud,b.Fin_Leakage,dbo.udfPropercase(g.Center_Name),b.susp_Leakage,b.SINo from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,center_master g where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and a.Audit_ID  =" + DTMASTER.Rows(l)(0).ToString() + " and f.center_ID=g.Center_ID order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                    For Each DR In DT.Rows

                        LineNumber += 1
                        j += 1
                        If (ITEMID <> CInt(DR(5)) And ITEMID <> 0) Then
                            Dim TR33 As New TableRow
                            TR33.BorderWidth = "1"
                            TR33.BorderStyle = BorderStyle.Solid

                            Dim TR33_00, TR33_01 As New TableCell

                            TR33_00.BorderWidth = "1"
                            TR33_01.BorderWidth = "1"


                            TR33_00.BorderColor = Drawing.Color.Silver
                            TR33_01.BorderColor = Drawing.Color.Silver


                            TR33_00.BorderStyle = BorderStyle.Solid
                            TR33_01.BorderStyle = BorderStyle.Solid

                            TR33_00.ForeColor = Drawing.Color.DarkBlue
                            TR33_01.ForeColor = Drawing.Color.DarkRed

                            RH.AddColumn(TR33, TR33_00, 93, 93, "r", "Total Leakage :")
                            RH.AddColumn(TR33, TR33_01, 7, 7, "r", GrossTotalLeakage)

                            GrossTotalLeakage = 0
                            tb.Controls.Add(TR33)
                        End If
                        If (ITEMID <> CInt(DR(5))) Then
                            I = 1
                            Dim TRItemHead As New TableRow
                            TRItemHead.ForeColor = Drawing.Color.DarkBlue
                            Dim TRItemHead_00 As New TableCell
                            RH.BlankRow(tb, 5)
                            RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "l", "<b><i>" + DR(2).ToString() + " / " + DR(4).ToString() + " / " + DR(6).ToString() + "</i></b>")

                            tb.Controls.Add(TRItemHead)
                            RH.BlankRow(tb, 5)
                            Dim TRHead As New TableRow
                            TRHead.BackColor = Drawing.Color.WhiteSmoke
                            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell

                            TRHead_00.BorderWidth = "1"
                            TRHead_01.BorderWidth = "1"
                            TRHead_02.BorderWidth = "1"
                            TRHead_03.BorderWidth = "1"
                            TRHead_04.BorderWidth = "1"
                            TRHead_05.BorderWidth = "1"
                            TRHead_06.BorderWidth = "1"
                            TRHead_07.BorderWidth = "1"
                            TRHead_08.BorderWidth = "1"

                            TRHead_00.BorderColor = Drawing.Color.Silver
                            TRHead_01.BorderColor = Drawing.Color.Silver
                            TRHead_02.BorderColor = Drawing.Color.Silver
                            TRHead_03.BorderColor = Drawing.Color.Silver
                            TRHead_04.BorderColor = Drawing.Color.Silver
                            TRHead_05.BorderColor = Drawing.Color.Silver
                            TRHead_06.BorderColor = Drawing.Color.Silver
                            TRHead_07.BorderColor = Drawing.Color.Silver
                            TRHead_08.BorderColor = Drawing.Color.Silver

                            TRHead_00.BorderStyle = BorderStyle.Solid
                            TRHead_01.BorderStyle = BorderStyle.Solid
                            TRHead_02.BorderStyle = BorderStyle.Solid
                            TRHead_03.BorderStyle = BorderStyle.Solid
                            TRHead_04.BorderStyle = BorderStyle.Solid
                            TRHead_05.BorderStyle = BorderStyle.Solid
                            TRHead_06.BorderStyle = BorderStyle.Solid
                            TRHead_07.BorderStyle = BorderStyle.Solid
                            TRHead_08.BorderColor = Drawing.Color.Silver

                            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                            RH.AddColumn(TRHead, TRHead_01, 13, 13, "l", "Loan No")
                            RH.AddColumn(TRHead, TRHead_02, 5, 5, "l", "Center")
                            RH.AddColumn(TRHead, TRHead_03, 22, 22, "l", "Customer Name")
                            RH.AddColumn(TRHead, TRHead_04, 12, 12, "l", "Loan Date")
                            RH.AddColumn(TRHead, TRHead_05, 8, 8, "r", "Amount")
                            RH.AddColumn(TRHead, TRHead_06, 5, 5, "c", "Type")
                            RH.AddColumn(TRHead, TRHead_07, 23, 23, "l", "Remarks")
                            RH.AddColumn(TRHead, TRHead_08, 7, 7, "l", "Est.Amt. Involved")


                            tb.Controls.Add(TRHead)
                            'RH.BlankRow(tb, 3)
                        End If

                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid

                        Dim Type As String = ""
                        If (CInt(DR(13)) = 1) Then
                            Type = "VS"
                        ElseIf (CInt(DR(16)) = 1) Then
                            Type = "S"
                        End If
                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"
                        TR3_05.BorderWidth = "1"
                        TR3_06.BorderWidth = "1"
                        TR3_07.BorderWidth = "1"
                        TR3_08.BorderWidth = "1"

                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver
                        TR3_05.BorderColor = Drawing.Color.Silver
                        TR3_06.BorderColor = Drawing.Color.Silver
                        TR3_07.BorderColor = Drawing.Color.Silver
                        TR3_08.BorderColor = Drawing.Color.Silver

                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid
                        TR3_05.BorderStyle = BorderStyle.Solid
                        TR3_06.BorderStyle = BorderStyle.Solid
                        TR3_07.BorderStyle = BorderStyle.Solid
                        TR3_08.BorderStyle = BorderStyle.Solid


                        RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString())
                        RH.AddColumn(TR3, TR3_01, 13, 13, "l", DR(7))
                        RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(15))
                        RH.AddColumn(TR3, TR3_03, 22, 22, "l", DR(10))
                        If Not IsDBNull(DR(11)) Then
                            RH.AddColumn(TR3, TR3_04, 12, 12, "l", CDate(DR(11)).ToString("dd/MM/yyyy"))
                        Else
                            RH.AddColumn(TR3, TR3_04, 12, 12, "l", "")
                        End If
                        RH.AddColumn(TR3, TR3_05, 8, 8, "r", CDbl(DR(12)).ToString())
                        RH.AddColumn(TR3, TR3_06, 5, 5, "c", Type)
                        RH.AddColumn(TR3, TR3_07, 23, 23, "l", DR(9).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(17)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                        RH.AddColumn(TR3, TR3_08, 7, 7, "r", CDbl(DR(14)).ToString("0"))
                        tb.Controls.Add(TR3)
                        I = I + 1
                        GROUPID = CInt(DR(1))
                        SUBGROUPID = CInt(DR(3))
                        ITEMID = CInt(DR(5))
                        GrossTotalLeakage = GrossTotalLeakage + CInt(DR(14))
                        NetTotalLeakage = NetTotalLeakage + CInt(DR(14))
                    Next
                    Dim TR55 As New TableRow
                    TR55.BorderWidth = "1"
                    TR55.BorderStyle = BorderStyle.Solid

                    Dim TR55_00, TR55_01 As New TableCell

                    TR55_00.BorderWidth = "1"
                    TR55_01.BorderWidth = "1"


                    TR55_00.BorderColor = Drawing.Color.Silver
                    TR55_01.BorderColor = Drawing.Color.Silver


                    TR55_00.BorderStyle = BorderStyle.Solid
                    TR55_01.BorderStyle = BorderStyle.Solid


                    TR55_00.ForeColor = Drawing.Color.DarkBlue
                    TR55_01.ForeColor = Drawing.Color.DarkRed

                    RH.AddColumn(TR55, TR55_00, 93, 93, "r", "Total Leakage :")
                    RH.AddColumn(TR55, TR55_01, 7, 7, "r", GrossTotalLeakage)

                    GrossTotalLeakage = 0
                    tb.Controls.Add(TR55)

                ElseIf AuditTypeID = 3 Then
                    DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME,b.POT_FRAUD ,b.REMARKS,b.FIN_LEAKAGE,b.SUSP_LEAKAGE,b.Fraud,b.SIno from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID  and a.Audit_ID = " + DTMASTER.Rows(l)(0).ToString() + " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                    GrossTotalLeakage = 0
                    For Each DR In DT.Rows
                        LineNumber += 1
                        j += 1
                        If (SUBGROUPID <> CInt(DR(3)) And SUBGROUPID <> 0) Then
                            Dim TR33 As New TableRow
                            TR33.BorderWidth = "1"
                            TR33.BorderStyle = BorderStyle.Solid

                            Dim TR33_00, TR33_01 As New TableCell

                            TR33_00.BorderWidth = "1"
                            TR33_01.BorderWidth = "1"


                            TR33_00.BorderColor = Drawing.Color.Silver
                            TR33_01.BorderColor = Drawing.Color.Silver


                            TR33_00.BorderStyle = BorderStyle.Solid
                            TR33_01.BorderStyle = BorderStyle.Solid

                            TR33_00.ForeColor = Drawing.Color.DarkBlue
                            TR33_01.ForeColor = Drawing.Color.DarkRed

                            RH.AddColumn(TR33, TR33_00, 93, 93, "r", "Total Leakage :")
                            RH.AddColumn(TR33, TR33_01, 7, 7, "r", GrossTotalLeakage)

                            GrossTotalLeakage = 0
                            tb.Controls.Add(TR33)
                        End If

                        If (SUBGROUPID <> CInt(DR(3))) Then
                            I = 1
                            Dim TRItemHead As New TableRow
                            TRItemHead.ForeColor = Drawing.Color.DarkBlue
                            Dim TRItemHead_00 As New TableCell
                            RH.BlankRow(tb, 5)
                            RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "l", "<b><i>" + DR(2).ToString() + " / " + DR(4).ToString() + "</i></b>")

                            tb.Controls.Add(TRItemHead)
                            RH.BlankRow(tb, 5)
                            Dim TRHead As New TableRow
                            TRHead.BackColor = Drawing.Color.WhiteSmoke
                            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

                            TRHead_00.BorderWidth = "1"
                            TRHead_01.BorderWidth = "1"
                            TRHead_02.BorderWidth = "1"
                            TRHead_03.BorderWidth = "1"
                            TRHead_04.BorderWidth = "1"


                            TRHead_00.BorderColor = Drawing.Color.Silver
                            TRHead_01.BorderColor = Drawing.Color.Silver
                            TRHead_02.BorderColor = Drawing.Color.Silver
                            TRHead_03.BorderColor = Drawing.Color.Silver
                            TRHead_04.BorderColor = Drawing.Color.Silver


                            TRHead_00.BorderStyle = BorderStyle.Solid
                            TRHead_01.BorderStyle = BorderStyle.Solid
                            TRHead_02.BorderStyle = BorderStyle.Solid
                            TRHead_03.BorderStyle = BorderStyle.Solid
                            TRHead_04.BorderStyle = BorderStyle.Solid


                            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                            RH.AddColumn(TRHead, TRHead_01, 40, 40, "l", "Check List")
                            RH.AddColumn(TRHead, TRHead_02, 7, 7, "c", "Type")
                            RH.AddColumn(TRHead, TRHead_03, 41, 41, "l", "Remarks")
                            RH.AddColumn(TRHead, TRHead_04, 7, 7, "r", "Est.Amt. Involved")
                            tb.Controls.Add(TRHead)
                            'RH.BlankRow(tb, 3)
                        End If
                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid

                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell

                        TR3_00.BorderWidth = "1"
                        TR3_01.BorderWidth = "1"
                        TR3_02.BorderWidth = "1"
                        TR3_03.BorderWidth = "1"
                        TR3_04.BorderWidth = "1"


                        TR3_00.BorderColor = Drawing.Color.Silver
                        TR3_01.BorderColor = Drawing.Color.Silver
                        TR3_02.BorderColor = Drawing.Color.Silver
                        TR3_03.BorderColor = Drawing.Color.Silver
                        TR3_04.BorderColor = Drawing.Color.Silver


                        TR3_00.BorderStyle = BorderStyle.Solid
                        TR3_01.BorderStyle = BorderStyle.Solid
                        TR3_02.BorderStyle = BorderStyle.Solid
                        TR3_03.BorderStyle = BorderStyle.Solid
                        TR3_04.BorderStyle = BorderStyle.Solid

                        Dim Type As String = ""

                        If DR(11) = 1 Then
                            Type = "VS"
                        ElseIf DR(10) = 1 Then

                            Type = "S"

                        End If

                        RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString())
                        RH.AddColumn(TR3, TR3_01, 40, 40, "l", DR(6))
                        RH.AddColumn(TR3, TR3_02, 7, 7, "c", Type)
                        RH.AddColumn(TR3, TR3_03, 41, 41, "l", DR(8).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(12)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                        RH.AddColumn(TR3, TR3_04, 7, 7, "r", CDbl(DR(9)))

                        tb.Controls.Add(TR3)
                        I = I + 1
                        GROUPID = CInt(DR(1))
                        SUBGROUPID = CInt(DR(3))
                        ITEMID = CInt(DR(5))
                        GrossTotalLeakage += CDbl(DR(9))
                        NetTotalLeakage += CDbl(DR(9))
                    Next
                    Dim TR55 As New TableRow
                    TR55.BorderWidth = "1"
                    TR55.BorderStyle = BorderStyle.Solid

                    Dim TR55_00, TR55_01 As New TableCell

                    TR55_00.BorderWidth = "1"
                    TR55_01.BorderWidth = "1"


                    TR55_00.BorderColor = Drawing.Color.Silver
                    TR55_01.BorderColor = Drawing.Color.Silver


                    TR55_00.BorderStyle = BorderStyle.Solid
                    TR55_01.BorderStyle = BorderStyle.Solid


                    TR55_00.ForeColor = Drawing.Color.DarkBlue
                    TR55_01.ForeColor = Drawing.Color.DarkRed

                    RH.AddColumn(TR55, TR55_00, 93, 93, "r", "Total  :")
                    RH.AddColumn(TR55, TR55_01, 7, 7, "r", GrossTotalLeakage)

                    GrossTotalLeakage = 0
                    tb.Controls.Add(TR55)
                ElseIf AuditTypeID = 4 Then
                    DT = DB.ExecuteDataSet("select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,dbo.udfPropercase(f.CLIENT_NAME),isnull(f.disbursed_Dt,f.sanctioned_dt),f.loan_amt,b.Fraud,b.Fin_Leakage,dbo.udfPropercase(g.Center_Name),b.susp_Leakage,b.SINo from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,center_master g where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and a.Audit_ID  =" + DTMASTER.Rows(l)(0).ToString() + " and f.center_ID=g.Center_ID order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                    For Each DR In DT.Rows
                        LineNumber += 1
                        j += 1
                        If (ITEMID <> CInt(DR(5)) And ITEMID <> 0) Then
                            Dim TR33 As New TableRow
                            TR33.BorderWidth = "1"
                            TR33.BorderStyle = BorderStyle.Solid

                            Dim TR33_00, TR33_01 As New TableCell

                            TR33_00.BorderWidth = "1"
                            TR33_01.BorderWidth = "1"


                            TR33_00.BorderColor = Drawing.Color.Silver
                            TR33_01.BorderColor = Drawing.Color.Silver


                            TR33_00.BorderStyle = BorderStyle.Solid
                            TR33_01.BorderStyle = BorderStyle.Solid

                            TR33_00.ForeColor = Drawing.Color.DarkBlue
                            TR33_01.ForeColor = Drawing.Color.DarkRed

                            RH.AddColumn(TR33, TR33_00, 93, 93, "r", "Total Leakage :")
                            RH.AddColumn(TR33, TR33_01, 7, 7, "r", GrossTotalLeakage)

                            GrossTotalLeakage = 0
                            tb.Controls.Add(TR33)
                        End If
                        If (ITEMID <> CInt(DR(5))) Then
                            I = 1
                            Dim TRItemHead As New TableRow
                            TRItemHead.ForeColor = Drawing.Color.DarkBlue
                            Dim TRItemHead_00 As New TableCell
                            RH.BlankRow(tb, 5)
                            RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "l", "<b><i>" + DR(2).ToString() + " / " + DR(4).ToString() + " / " + DR(6).ToString() + "</i></b>")

                            tb.Controls.Add(TRItemHead)
                            RH.BlankRow(tb, 5)
                            Dim TRHead As New TableRow
                            TRHead.BackColor = Drawing.Color.WhiteSmoke
                            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell
                            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
                            RH.InsertColumn(TRHead, TRHead_02, 20, 0, "Center")
                            RH.InsertColumn(TRHead, TRHead_03, 5, 2, "Type")
                            RH.InsertColumn(TRHead, TRHead_04, 63, 0, "Remarks")
                            RH.InsertColumn(TRHead, TRHead_05, 7, 0, "Est.Amt. Involved")


                            tb.Controls.Add(TRHead)
                            'RH.BlankRow(tb, 3)
                        End If

                        Dim TR3 As New TableRow
                        TR3.BorderWidth = "1"
                        TR3.BorderStyle = BorderStyle.Solid

                        Dim Type As String = ""
                        If (CInt(DR(13)) = 1) Then
                            Type = "VS"
                        ElseIf (CInt(DR(16)) = 1) Then
                            Type = "S"
                        End If
                        Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell
                        RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                        RH.InsertColumn(TR3, TR3_02, 20, 0, DR(15))
                        RH.InsertColumn(TR3, TR3_03, 5, 2, Type)
                        RH.InsertColumn(TR3, TR3_04, 63, 0, DR(9).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(17)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                        RH.InsertColumn(TR3, TR3_05, 7, 1, CDbl(DR(14)).ToString("0"))
                        tb.Controls.Add(TR3)
                        I = I + 1
                        GROUPID = CInt(DR(1))
                        SUBGROUPID = CInt(DR(3))
                        ITEMID = CInt(DR(5))
                        GrossTotalLeakage = GrossTotalLeakage + CInt(DR(14))
                        NetTotalLeakage = NetTotalLeakage + CInt(DR(14))
                    Next
                    Dim TR55 As New TableRow
                    TR55.BorderWidth = "1"
                    TR55.BorderStyle = BorderStyle.Solid

                    Dim TR55_00, TR55_01 As New TableCell

                    TR55_00.BorderWidth = "1"
                    TR55_01.BorderWidth = "1"


                    TR55_00.BorderColor = Drawing.Color.Silver
                    TR55_01.BorderColor = Drawing.Color.Silver


                    TR55_00.BorderStyle = BorderStyle.Solid
                    TR55_01.BorderStyle = BorderStyle.Solid


                    TR55_00.ForeColor = Drawing.Color.DarkBlue
                    TR55_01.ForeColor = Drawing.Color.DarkRed

                    RH.AddColumn(TR55, TR55_00, 93, 93, "r", "Total Leakage :")
                    RH.AddColumn(TR55, TR55_01, 7, 7, "r", GrossTotalLeakage)

                    GrossTotalLeakage = 0
                    tb.Controls.Add(TR55)
                End If
                Dim TR2 As New TableRow
                RH.BlankRow(tb, 20)
                Dim TR21 As New TableRow
                Dim TR21_00 As New TableCell
                TR21_00.Style.Add("text-align", "justify")
                RH.AddColumn(TR21, TR21_00, 100, 100, "l", "<b>General Comments</b> : " + DTMASTER.Rows(l)(2).ToString())
                tb.Controls.Add(TR21)
                If DepartmentID = 14 Or DepartmentID = 16 Then
                    RH.BlankRow(tb, 5)
                    Dim TRSC As New TableRow
                    Dim TRSC_00 As New TableCell
                    TRSC_00.Style.Add("text-align", "justify")
                    RH.AddColumn(TRSC, TRSC_00, 100, 100, "l", "<b>Serious Comments</b> : " + DTMASTER.Rows(l)(3).ToString())
                    tb.Controls.Add(TRSC)
                End If
                RH.BlankRow(tb, 20)
            Next


            RH.BlankRow(tb, 5)
            Dim TR44 As New TableRow
            TR44.BorderWidth = "1"
            TR44.BorderStyle = BorderStyle.Solid

            Dim TR44_00, TR44_01 As New TableCell


            TR44_00.Font.Bold = True
            TR44_01.Font.Bold = True

            TR44_00.ForeColor = Drawing.Color.DarkBlue
            TR44_01.ForeColor = Drawing.Color.DarkRed

            RH.AddColumn(TR44, TR44_00, 93, 93, "r", "Net Estimate Amount Involved :")
            RH.AddColumn(TR44, TR44_01, 7, 7, "r", NetTotalLeakage)


            tb.Controls.Add(TR44)

            If DTAudit.Rows(0)(4).ToString() <> DTAudit.Rows(0)(5).ToString() Then
                Dim TR_Prep As New TableRow
                Dim TR_Prep_00, TR_Prep_01 As New TableCell
                ' TR_Prep.ForeColor = Drawing.Color.Silver
                RH.AddColumn(TR_Prep, TR_Prep_00, 50, 50, "l", " Prepared By ")
                RH.AddColumn(TR_Prep, TR_Prep_01, 50, 50, "r", " Screened By ")
                tb.Controls.Add(TR_Prep)
                RH.BlankRow(tb, 5)

                Dim TR_Prep1 As New TableRow
                Dim TR_Prep1_00, TR_Prep1_01 As New TableCell
                ' TR_Prep1.ForeColor = Drawing.Color.Silver
                RH.AddColumn(TR_Prep1, TR_Prep1_00, 50, 50, "l", DTAudit.Rows(0)(4).ToString())
                RH.AddColumn(TR_Prep1, TR_Prep1_01, 50, 50, "r", DTAudit.Rows(0)(5).ToString())
                tb.Controls.Add(TR_Prep1)
                RH.BlankRow(tb, 5)
            Else
                Dim TR_Prep As New TableRow
                Dim TR_Prep_00 As New TableCell
                ' TR_Prep.ForeColor = Drawing.Color.Silver
                RH.AddColumn(TR_Prep, TR_Prep_00, 100, 100, "l", " Prepared By ")

                tb.Controls.Add(TR_Prep)
                RH.BlankRow(tb, 5)

                Dim TR_Prep1 As New TableRow
                Dim TR_Prep1_00 As New TableCell
                'TR_Prep1.ForeColor = Drawing.Color.Silver
                RH.AddColumn(TR_Prep1, TR_Prep1_00, 100, 100, "l", DTAudit.Rows(0)(4).ToString())
                tb.Controls.Add(TR_Prep1)
                RH.BlankRow(tb, 5)
            End If


            Dim TR_Decl As New TableRow
            Dim TR_Decl_00 As New TableCell
            'TR_Decl.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_Decl, TR_Decl_00, 100, 100, "c", "<b> This is a system generated Report and does not require signature </b>")
            tb.Controls.Add(TR_Decl)
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
