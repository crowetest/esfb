﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewStaffInvolvementAbstract
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim SINO As Integer = 1
    Dim GN As New GeneralFunctions
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim CloseFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim AD As New Audit
    Dim RegionID As Integer = -1
    Dim AreaID As Integer = -1
    Dim BranchID As Integer = -1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            If Not Request.QueryString.Get("RegionID") Is Nothing Then
                RegionID = GN.Decrypt(Request.QueryString.Get("RegionID"))
            End If
            If Not Request.QueryString.Get("AreaID") Is Nothing Then
                AreaID = GN.Decrypt(Request.QueryString.Get("AreaID"))
            End If
            If Not Request.QueryString.Get("BranchID") Is Nothing Then
                BranchID = GN.Decrypt(Request.QueryString.Get("BranchID"))
            End If
            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))

            CloseFlag = Request.QueryString.Get("CloseFlag")
            PostID = CInt(Session("Post_ID"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If


            Dim SqlStr As String = ""
            Dim SqlTail As String = ""
            Dim SqlObserv As String = "SELECT x.EMP_CODE,x.Emp_Name , x.Branch_Name,  Label = STUFF((SELECT '^' +   item_name     FROM AUDIT_STAFF_INVOLVEMENT a,EMP_MASTER  b,BrMaster c,AUDIT_OBSERVATION_DTL d,AUDIT_OBSERVATION e,AUDIT_CHECK_LIST f,audit_master g,audit_dtl h,brmaster i WHERE a.EMP_CODE=b.Emp_Code and b.branch_id=i.branch_id and g.Branch_ID=c.Branch_ID and e.audit_id=h.audit_id and h.group_id=g.group_id   and  g.Status_ID=2 and a.SINO=d.SINO and d.OBSERVATION_ID=e.OBSERVATION_ID and e.ITEM_ID=f.ITEM_ID  "
            SqlStr = "select A.EMP_CODE,b.Emp_Name  , i.Branch_Name,sum(isnull( amount,0)) as FL,sum(case when isnull(intentionally,0)=1 then Amount else 0 end) as StaffInvolvement,COUNT(d.sino)as Observations from AUDIT_STAFF_INVOLVEMENT a,audit_observation_dtl d,audit_observation e,audit_dtl f,audit_master g,EMP_MASTER  b,BrMaster c,brmaster i where a.EMP_CODE=b.Emp_Code and b.branch_id=i.branch_id  and  g.Branch_ID=c.Branch_ID and a.SINO=d.SINO and d.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.GROUP_ID=g.GROUP_ID and g.STATUS_ID=2"
            SqlTail = " and DATEADD(day, DATEDIFF(day, 0, a.Tra_DT), 0) between ' " + FromDt + "'  and  '" + ToDt + "'"
            Dim SubHead As String = ""
            If RegionID > 0 Then
                SqlTail += " and c.region_id=" + RegionID.ToString()
                SubHead = " From " + FromDt.ToString("dd/MMM/yyyy") + " To " + ToDt.ToString("dd/MMM/yyyy") + " " + GN.GetRegion_Name(RegionID) + " Region"
            ElseIf AreaID > 0 Then
                SqlTail += " and c.area_id=" + AreaID.ToString()
                SubHead = " From " + FromDt.ToString("dd/MMM/yyyy") + " To " + ToDt.ToString("dd/MMM/yyyy") + " " + GN.GetArea_Name(AreaID) + " Area"
            ElseIf BranchID > 0 Then
                SqlTail += " and c.Branch_id=" + BranchID.ToString()
                SubHead = " From " + FromDt.ToString("dd/MMM/yyyy") + " To " + ToDt.ToString("dd/MMM/yyyy") + " " + GN.GetBranch_Name(BranchID) + " Branch"
            End If
            If (Month > 0) Then
                SqlTail += " and month(g.period_to)=" + Month.ToString() + " and year(g.period_to)=" + year.ToString() + "  "
            End If

            If CloseFlag = 0 Then
                SqlStr += " and a.status_id in(0,9)"
            ElseIf CloseFlag = 1 Then
                SqlStr += " and a.status_id=1"
            End If

            SqlStr += SqlTail
            SqlStr += "  group by a.EMP_CODE,b.Emp_Name , i.Branch_Name"
            SqlObserv += SqlTail
            SqlObserv += " and a.emp_code = x.EMP_CODE group by ITEM_NAME   FOR XML PATH('')), 1, 1, '')"
            Dim SQL As String = "WITH x AS ( " + SqlStr + ")" + SqlObserv + " ,x.FL,x.StaffInvolvement,x.Observations   FROM x ORDER BY X.BRANCH_NAME,X.EMP_NAME"
            DT = DB.ExecuteDataSet(SQL).Tables(0)

            Dim tb As New Table
            tb.Attributes.Add("width", "100%")
            tb.Style.Add("table-layout", "fixed")
            RH.Heading(Session("FirmName"), tb, "Staffs Involvement Abstract" + SubHead, 100)

            Dim RowBG As Integer = 0
            Dim TRHead As New TableRow
            TRHead.Style.Add("Font-weight", "bold")
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_02, 20, 0, "Staff Name")
            RH.InsertColumn(TRHead, TRHead_03, 20, 0, "Current Branch")
            RH.InsertColumn(TRHead, TRHead_04, 10, 1, "No Of Observations")
            RH.InsertColumn(TRHead, TRHead_05, 25, 2, "CheckList")
            RH.InsertColumn(TRHead, TRHead_06, 10, 2, "Fin.Leakage")
            RH.InsertColumn(TRHead, TRHead_07, 10, 2, "Staff Involvement")
            tb.Controls.Add(TRHead)


            Dim DR As DataRow
            Dim Cnt As Double = 0
            Dim Tot1 As Double = 0
            Dim Tot2 As Double = 0
            Dim Tot3 As Integer = 0
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, SINO.ToString())
                RH.InsertColumn(TR3, TR3_02, 20, 0, DR(1).ToString() + " (" + DR(0).ToString() + ")")
                RH.InsertColumn(TR3, TR3_03, 20, 0, DR(2).ToString())
                RH.InsertColumn(TR3, TR3_04, 10, 1, DR(6).ToString())
                RH.InsertColumn(TR3, TR3_05, 25, 0, DR(3).ToString().Replace("^", "<br /><br />"))
                RH.InsertColumn(TR3, TR3_06, 10, 1, CDbl(DR(4)).ToString("0.00"))
                If CDbl(DR(5)) > 0 Then
                    RH.InsertColumn(TR3, TR3_07, 10, 1, CDbl(DR(5)).ToString("0.00"))
                Else
                    RH.InsertColumn(TR3, TR3_07, 10, 1, CDbl(DR(5)).ToString("-"))
                End If

                tb.Controls.Add(TR3)
                SINO += 1
                ' Cnt += DR(4)
                Tot1 += DR(4)
                Tot2 += DR(5)
                Tot3 += DR(6)
            Next

            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            RH.InsertColumn(TRFooter, TRFooter_00, 45, 2, "Total")
            RH.InsertColumn(TRFooter, TRFooter_01, 10, 1, Tot3.ToString())
            RH.InsertColumn(TRFooter, TRFooter_02, 25, 2, "")
            RH.InsertColumn(TRFooter, TRFooter_03, 10, 1, Tot1.ToString("0.00"))
            RH.InsertColumn(TRFooter, TRFooter_04, 10, 1, Tot2.ToString("0.00"))
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
