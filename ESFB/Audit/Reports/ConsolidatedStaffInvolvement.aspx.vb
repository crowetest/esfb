﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_Reports_ConsolidatedStaffInvolvement
    Inherits System.Web.UI.Page
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 77) = False And GN.FormAccess(CInt(Session("UserID")), 72) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            ReportID = CInt(Request.QueryString.Get("RptID"))
            Me.hdnReportID.Value = ReportID.ToString()
            Me.hdnPostID.Value = Session("Post_ID")
            Dim PostID As Integer = CInt(Session("Post_ID"))
            If (PostID = 6) Then
                LocationID = GN.GetArea_ID(CInt(Session("UserID")))
            ElseIf (PostID = 7) Then
                LocationID = GN.GetRegion_ID(CInt(Session("UserID")))
            ElseIf (PostID = 5) Then
                LocationID = GN.GetBranch_ID(CInt(Session("UserID")))
            End If
            Me.txtFrom_CalendarExtender.SelectedDate = CDate("01/May/2014")
            Me.txtTo_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            Me.hdnLocationID.Value = LocationID.ToString()
            Me.Master.subtitle = "Consolidated Staff Involvement Report"
            GN.ComboFill(cmbAudit, AD.GetAuditMonthsALL(), 0, 1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "Initialize();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
