﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewAuditReportCons
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim tbHead As New Table
    Dim TypeID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim SPMFlag As Integer
    Dim FromDt As String
    Dim ToDt As String
    Dim SpotClose As String = ""
    Dim ObsFlag As Integer = 0
    Dim SummaryFlag As Integer = 0
    Dim GN As New GeneralFunctions
    Dim DS As New DataSet
    Dim StrSubHead As String = ""
    Dim StrExcelHead As String = ""
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            FromDt = Format(CDate(Request.QueryString.Get("FromDt")).ToString("dd/MMM/yyyy"))
            ToDt = Format(CDate(Request.QueryString.Get("ToDt")).ToString("dd/MMM/yyyy"))

            TypeID = CInt(GF.Decrypt(Request.QueryString.Get("TypeID")))
            RegionID = CInt(GF.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GF.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))

            SPMFlag = Request.QueryString.Get("SPMFlag")
            ObsFlag = Request.QueryString.Get("ObsFlag")
            SummaryFlag = Request.QueryString.Get("SummaryFlag")

            Dim DTObservation As New DataTable
            Dim DTSummary As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            tb.Attributes.Add("width", "100%")
            tbHead.Attributes.Add("width", "100%")
            Dim hdStr As String



            If TypeID = 1 Then
                StrSubHead = "( Ended From :" + FromDt.ToString() + " - Ended To :" + ToDt.ToString() + ")"
            Else
                StrSubHead = "( Period From :" + FromDt.ToString() + " - Period To :" + ToDt.ToString() + ")"
            End If
            If BranchID > 0 Then
                If SPMFlag = 0 Then
                    hdStr = "AUDIT REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH " + StrSubHead
                Else
                    hdStr = "SPM QUERIES REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH" + StrSubHead
                End If
            ElseIf AreaID > 0 Then
                If SPMFlag = 0 Then
                    hdStr = "AUDIT REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA" + StrSubHead
                Else
                    hdStr = "SPM QUERIES REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA" + StrSubHead
                End If
            ElseIf RegionID > 0 Then
                If SPMFlag = 0 Then
                    hdStr = "AUDIT REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION" + StrSubHead
                Else
                    hdStr = "SPM QUERIES REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION" + StrSubHead
                End If

            Else
                If SPMFlag = 0 Then
                    hdStr = "CONSOLIDATED AUDIT REPORT" + StrSubHead
                Else
                    hdStr = "CONSOLIDATED SPM QUERIES REPORT" + StrSubHead
                End If

            End If
            StrExcelHead = hdStr
            RH.Heading(Session("FirmName"), tbHead, hdStr, 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow

            Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TypeID", TypeID),
                                                                      New System.Data.SqlClient.SqlParameter("@FromDT", CDate(FromDt)),
                                                                      New System.Data.SqlClient.SqlParameter("@ToDT", CDate(ToDt)),
                                                                      New System.Data.SqlClient.SqlParameter("@RegionID", RegionID),
                                                                      New System.Data.SqlClient.SqlParameter("@AreaID", AreaID),
                                                                      New System.Data.SqlClient.SqlParameter("@BranchID", BranchID),
                                                                      New System.Data.SqlClient.SqlParameter("@SPMFlag", SPMFlag),
                                                                      New System.Data.SqlClient.SqlParameter("@ObsFlag", ObsFlag),
                                                                      New System.Data.SqlClient.SqlParameter("@SummaryFlag", SummaryFlag)}
            DS = DB.ExecuteDataSetLongTime("SP_AUDIT_GET_BRIEFOVERVIEW", Parameters)
            DTAudit = DS.Tables(0)
            Dim TR0 As New TableRow
            Dim TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07 As New TableCell
            RH.InsertColumn(TR0, TR0_01, 3, 0, "<b>Branches</b>")
            RH.InsertColumn(TR0, TR0_02, 2, 0, " : ")
            Dim sss As String = "<a href='viewAuditsAndBranch.aspx?RptID=1 &FromDt=" + FromDt.ToString() + " &ToDt=" + ToDt.ToString() + " &RegionID=" + GF.Encrypt(RegionID.ToString()) + " &AreaID=" + GF.Encrypt(AreaID.ToString()) + " &BranchID=" + GF.Encrypt(BranchID.ToString()) + " &TypeID=" + TypeID.ToString() + " &SPMFlag=" + SPMFlag.ToString() + "' style='text-align:right;' target='_blank'><b>" + DTAudit.Rows(0)(0).ToString() + "</b>"
            RH.InsertColumn(TR0, TR0_03, 81, 0, sss)
            RH.InsertColumn(TR0, TR0_05, 10, 0, "<b>Audits</b>")
            RH.InsertColumn(TR0, TR0_06, 2, 0, " : ")
            sss = "<a href='viewAuditsAndBranch.aspx?RptID=2 &FromDt=" + FromDt.ToString() + " &ToDt=" + ToDt.ToString() + " &RegionID=" + GF.Encrypt(RegionID.ToString()) + " &AreaID=" + GF.Encrypt(AreaID.ToString()) + " &BranchID=" + GF.Encrypt(BranchID.ToString()) + " &TypeID=" + TypeID.ToString() + " &SPMFlag=" + SPMFlag.ToString() + "' style='text-align:right;' target='_blank'><b>" + DTAudit.Rows(0)(1).ToString() + "</b>"
            RH.InsertColumn(TR0, TR0_07, 3, 1, sss)
            TR0.Font.Name = "cambria"
            TR0_01.BorderStyle = BorderStyle.None
            TR0_02.BorderStyle = BorderStyle.None
            TR0_03.BorderStyle = BorderStyle.None
            TR0_04.BorderStyle = BorderStyle.None
            TR0_05.BorderStyle = BorderStyle.None
            TR0_06.BorderStyle = BorderStyle.None
            TR0_07.BorderStyle = BorderStyle.None

            tbHead.Controls.Add(TR0)

            DTObservation = DS.Tables(2)
            If SummaryFlag = 1 Then
                DTSummary = DS.Tables(3)


            End If


            Dim TR01 As New TableRow
            Dim TR01_01, TR01_02, TR01_03, TR01_05, TR01_06, TR01_07 As New TableCell
            RH.InsertColumn(TR01, TR01_01, 3, 0, "<b>Observations</b>")
            RH.InsertColumn(TR01, TR01_02, 2, 0, " : ")
            RH.InsertColumn(TR01, TR01_03, 81, 0, "<b>" + DTObservation.Rows(0).Item(0).ToString() + "</b>")
            RH.InsertColumn(TR01, TR01_05, 10, 0, "<b>Spote Close</b>")
            RH.InsertColumn(TR01, TR01_06, 2, 0, " : ")
            If SummaryFlag = 1 Then
                RH.InsertColumn(TR01, TR01_07, 3, 1, "<b>" + DTSummary.Rows(0)(18).ToString() + "</b>")
            Else
                RH.InsertColumn(TR01, TR01_07, 3, 1, "<b></b>")
            End If

            TR01.Font.Name = "cambria"
            TR01_01.BorderStyle = BorderStyle.None
            TR01_02.BorderStyle = BorderStyle.None
            TR01_03.BorderStyle = BorderStyle.None
            TR01_05.BorderStyle = BorderStyle.None
            TR01_06.BorderStyle = BorderStyle.None
            TR01_07.BorderStyle = BorderStyle.None

            tbHead.Controls.Add(TR01)

            If SummaryFlag = 1 Then
                RH.BlankRow(tb, 5)

                Dim TR1 As New TableRow
                Dim TR1_01 As New TableCell
                RH.InsertColumn(TR1, TR1_01, 100, 2, "BRIEF OVERVIEW")
                TR1.BackColor = Drawing.Color.WhiteSmoke
                'TR1.ForeColor = Drawing.Color.White
                TR1.Font.Name = "cambria"
                TR1.Font.Bold = True


                tb.Controls.Add(TR1)

                Dim TR2 As New TableRow
                Dim TR2_01, TR2_02, TR2_03, TR2_04, TR2_05, TR2_06 As New TableCell
                RH.InsertColumn(TR2, TR2_01, 3, 0, " ")
                RH.InsertColumn(TR2, TR2_02, 46, 0, " ")
                RH.InsertColumn(TR2, TR2_03, 16, 2, "Sangam")
                RH.InsertColumn(TR2, TR2_04, 16, 2, "Client")
                RH.InsertColumn(TR2, TR2_05, 16, 2, "Accounts")
                RH.InsertColumn(TR2, TR2_06, 3, 0, " ")
                TR2.BackColor = Drawing.Color.WhiteSmoke
                TR2.Font.Name = "cambria"
                TR2.Font.Bold = True

                tb.Controls.Add(TR2)

                Dim TR22 As New TableRow
                Dim TR22_01, TR22_02, TR22_03, TR22_04, TR22_05, TR22_06, TR22_07, TR22_08, TR22_09 As New TableCell
                RH.InsertColumn(TR22, TR22_01, 3, 0, " ")
                RH.InsertColumn(TR22, TR22_02, 46, 0, " ")
                RH.InsertColumn(TR22, TR22_03, 8, 2, "Count")
                RH.InsertColumn(TR22, TR22_04, 8, 2, "%")
                RH.InsertColumn(TR22, TR22_05, 8, 2, "Count")
                RH.InsertColumn(TR22, TR22_06, 8, 2, "%")
                RH.InsertColumn(TR22, TR22_07, 8, 2, "Count")
                RH.InsertColumn(TR22, TR22_08, 8, 2, "%")
                RH.InsertColumn(TR22, TR22_09, 3, 0, " ")
                TR22.BackColor = Drawing.Color.WhiteSmoke
                TR22.Font.Name = "cambria"
                TR22.Font.Bold = True

                tb.Controls.Add(TR22)


                Dim TR33 As New TableRow
                Dim TR33_01, TR33_02, TR33_03, TR33_04, TR33_05, TR33_06, TR33_07, TR33_08, TR33_09 As New TableCell
                RH.InsertColumn(TR33, TR33_01, 3, 0, " ")
                RH.InsertColumn(TR33, TR33_02, 46, 0, "Total Portfolio")
                TR33.Font.Name = "cambria"
                TR33.Font.Bold = True
                TR33_01.BackColor = Drawing.Color.WhiteSmoke
                TR33_02.BackColor = Drawing.Color.WhiteSmoke
                TR33_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR33, TR33_03, 8, 2, DTSummary.Rows(0)(21))
                RH.InsertColumn(TR33, TR33_04, 8, 2, "")
                RH.InsertColumn(TR33, TR33_05, 8, 2, DTSummary.Rows(0)(33))
                RH.InsertColumn(TR33, TR33_06, 8, 2, "")
                RH.InsertColumn(TR33, TR33_07, 8, 2, DTSummary.Rows(0)(22))
                RH.InsertColumn(TR33, TR33_08, 8, 2, "")
                RH.InsertColumn(TR33, TR33_09, 3, 0, " ")


                tb.Controls.Add(TR33)





                Dim TR3 As New TableRow
                Dim TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell
                RH.InsertColumn(TR3, TR3_01, 3, 0, " ")
                RH.InsertColumn(TR3, TR3_02, 46, 0, "Verified Disbursement Documents During the Period")
                TR3.Font.Name = "cambria"
                TR3.Font.Bold = True
                TR3_01.BackColor = Drawing.Color.WhiteSmoke
                TR3_02.BackColor = Drawing.Color.WhiteSmoke
                TR3_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR3, TR3_03, 8, 2, DTSummary.Rows(0)(0))
                RH.InsertColumn(TR3, TR3_04, 8, 2, ((CInt(DTSummary.Rows(0)(0)) * 100) / CInt(DTSummary.Rows(0)(21))).ToString("0.00"))
                RH.InsertColumn(TR3, TR3_05, 8, 2, DTSummary.Rows(0)(23))
                RH.InsertColumn(TR3, TR3_06, 8, 2, ((CInt(DTSummary.Rows(0)(23)) * 100) / CInt(DTSummary.Rows(0)(33))).ToString("0.00"))
                RH.InsertColumn(TR3, TR3_07, 8, 2, DTSummary.Rows(0)(1))
                RH.InsertColumn(TR3, TR3_08, 8, 2, ((CInt(DTSummary.Rows(0)(1)) * 100) / CInt(DTSummary.Rows(0)(22))).ToString("0.00"))
                RH.InsertColumn(TR3, TR3_09, 3, 0, " ")


                tb.Controls.Add(TR3)

                Dim TR4 As New TableRow
                Dim TR4_01, TR4_02, TR4_03, TR4_04, TR4_05, TR4_06, TR4_07, TR4_08, TR4_09 As New TableCell
                RH.InsertColumn(TR4, TR4_01, 3, 0, " ")
                RH.InsertColumn(TR4, TR4_02, 46, 0, "Visited Sangams During the Period")
                TR4.Font.Name = "cambria"
                TR4.Font.Bold = True
                TR4_01.BackColor = Drawing.Color.WhiteSmoke
                TR4_02.BackColor = Drawing.Color.WhiteSmoke
                TR4_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR4, TR4_03, 8, 2, DTSummary.Rows(0)(2))
                RH.InsertColumn(TR4, TR4_04, 8, 2, ((CInt(DTSummary.Rows(0)(2)) * 100) / CInt(DTSummary.Rows(0)(21))).ToString("0.00"))
                RH.InsertColumn(TR4, TR4_05, 8, 2, DTSummary.Rows(0)(24))
                RH.InsertColumn(TR4, TR4_06, 8, 2, ((CInt(DTSummary.Rows(0)(24)) * 100) / CInt(DTSummary.Rows(0)(33))).ToString("0.00"))
                RH.InsertColumn(TR4, TR4_07, 8, 2, DTSummary.Rows(0)(3))
                RH.InsertColumn(TR4, TR4_08, 8, 2, ((CInt(DTSummary.Rows(0)(3)) * 100) / CInt(DTSummary.Rows(0)(22))).ToString("0.00"))
                RH.InsertColumn(TR4, TR4_09, 3, 0, " ")


                tb.Controls.Add(TR4)

                Dim TR5 As New TableRow
                Dim TR5_01, TR5_02, TR5_03, TR5_04, TR5_05, TR5_06, TR5_07, TR5_08, TR5_09 As New TableCell
                RH.InsertColumn(TR5, TR5_01, 3, 0, " ")
                RH.InsertColumn(TR5, TR5_02, 46, 0, "Verified Samples(Combined) During the Period ")
                TR5.Font.Name = "cambria"
                TR5.Font.Bold = True
                TR5_01.BackColor = Drawing.Color.WhiteSmoke
                TR5_02.BackColor = Drawing.Color.WhiteSmoke
                TR5_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR5, TR5_03, 8, 2, DTSummary.Rows(0)(4))
                RH.InsertColumn(TR5, TR5_04, 8, 2, ((CInt(DTSummary.Rows(0)(4)) * 100) / CInt(DTSummary.Rows(0)(21))).ToString("0.00"))
                RH.InsertColumn(TR5, TR5_05, 8, 2, DTSummary.Rows(0)(25))
                RH.InsertColumn(TR5, TR5_06, 8, 2, ((CInt(DTSummary.Rows(0)(25)) * 100) / CInt(DTSummary.Rows(0)(33))).ToString("0.00"))
                RH.InsertColumn(TR5, TR5_07, 8, 2, DTSummary.Rows(0)(5))
                RH.InsertColumn(TR5, TR5_08, 8, 2, ((CInt(DTSummary.Rows(0)(5)) * 100) / CInt(DTSummary.Rows(0)(22))).ToString("0.00"))
                RH.InsertColumn(TR5, TR5_09, 3, 0, " ")


                tb.Controls.Add(TR5)

                Dim TR6 As New TableRow
                Dim TR6_01, TR6_02, TR6_03, TR6_04, TR6_05, TR6_06, TR6_07, TR6_08, TR6_09 As New TableCell
                RH.InsertColumn(TR6, TR6_01, 3, 0, " ")
                RH.InsertColumn(TR6, TR6_02, 46, 0, "Number of Very Serious Observations During the Period")
                TR6.Font.Name = "cambria"
                TR6.Font.Bold = True
                TR6_01.BackColor = Drawing.Color.WhiteSmoke
                TR6_02.BackColor = Drawing.Color.WhiteSmoke
                TR6_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR6, TR6_03, 8, 2, DTSummary.Rows(0)(6))
                RH.InsertColumn(TR6, TR6_04, 8, 2, ((CInt(DTSummary.Rows(0)(6)) * 100) / CInt(DTSummary.Rows(0)(4))).ToString("0.00"))
                RH.InsertColumn(TR6, TR6_05, 8, 2, DTSummary.Rows(0)(26))
                RH.InsertColumn(TR6, TR6_06, 8, 2, ((CInt(DTSummary.Rows(0)(26)) * 100) / CInt(DTSummary.Rows(0)(25))).ToString("0.00"))
                RH.InsertColumn(TR6, TR6_07, 8, 2, DTSummary.Rows(0)(7))
                RH.InsertColumn(TR6, TR6_08, 8, 2, ((CInt(DTSummary.Rows(0)(7)) * 100) / CInt(DTSummary.Rows(0)(5))).ToString("0.00"))
                RH.InsertColumn(TR6, TR6_09, 3, 0, " ")


                tb.Controls.Add(TR6)

                Dim TR7 As New TableRow
                Dim TR7_01, TR7_02, TR7_03, TR7_04, TR7_05, TR7_06, TR7_07, TR7_08, TR7_09 As New TableCell
                RH.InsertColumn(TR7, TR7_01, 3, 0, " ")
                RH.InsertColumn(TR7, TR7_02, 46, 0, "Number of Serious Observations During the Period")
                TR7.Font.Name = "cambria"
                TR7.Font.Bold = True
                TR7_01.BackColor = Drawing.Color.WhiteSmoke
                TR7_02.BackColor = Drawing.Color.WhiteSmoke
                TR7_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR7, TR7_03, 8, 2, DTSummary.Rows(0)(8))
                RH.InsertColumn(TR7, TR7_04, 8, 2, ((CInt(DTSummary.Rows(0)(8)) * 100) / CInt(DTSummary.Rows(0)(4))).ToString("0.00"))
                RH.InsertColumn(TR7, TR7_05, 8, 2, DTSummary.Rows(0)(27))
                RH.InsertColumn(TR7, TR7_06, 8, 2, ((CInt(DTSummary.Rows(0)(27)) * 100) / CInt(DTSummary.Rows(0)(25))).ToString("0.00"))
                RH.InsertColumn(TR7, TR7_07, 8, 2, DTSummary.Rows(0)(9))
                RH.InsertColumn(TR7, TR7_08, 8, 2, ((CInt(DTSummary.Rows(0)(9)) * 100) / CInt(DTSummary.Rows(0)(5))).ToString("0.00"))
                RH.InsertColumn(TR7, TR7_09, 3, 0, " ")


                tb.Controls.Add(TR7)

                Dim TR8 As New TableRow
                Dim TR8_01, TR8_02, TR8_03, TR8_04, TR8_05, TR8_06, TR8_07, TR8_08, TR8_09 As New TableCell
                RH.InsertColumn(TR8, TR8_01, 3, 0, " ")
                RH.InsertColumn(TR8, TR8_02, 46, 0, "Number of General Observations During the Period")
                TR8.Font.Name = "cambria"
                TR8.Font.Bold = True
                TR8_01.BackColor = Drawing.Color.WhiteSmoke
                TR8_02.BackColor = Drawing.Color.WhiteSmoke
                TR8_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR8, TR8_03, 8, 2, DTSummary.Rows(0)(10))
                RH.InsertColumn(TR8, TR8_04, 8, 2, ((CInt(DTSummary.Rows(0)(10)) * 100) / CInt(DTSummary.Rows(0)(4))).ToString("0.00"))
                RH.InsertColumn(TR8, TR8_05, 8, 2, DTSummary.Rows(0)(28))
                RH.InsertColumn(TR8, TR8_06, 8, 2, ((CInt(DTSummary.Rows(0)(28)) * 100) / CInt(DTSummary.Rows(0)(25))).ToString("0.00"))
                RH.InsertColumn(TR8, TR8_07, 8, 2, DTSummary.Rows(0)(11))
                RH.InsertColumn(TR8, TR8_08, 8, 2, ((CInt(DTSummary.Rows(0)(11)) * 100) / CInt(DTSummary.Rows(0)(5))).ToString("0.00"))
                RH.InsertColumn(TR8, TR8_09, 3, 0, " ")

                tb.Controls.Add(TR8)

                Dim TR14 As New TableRow
                Dim TR14_01, TR14_02, TR14_03, TR14_04, TR14_05, TR14_06, TR14_07, TR14_08, TR14_09 As New TableCell
                RH.InsertColumn(TR14, TR14_01, 3, 0, " ")
                RH.InsertColumn(TR14, TR14_02, 46, 0, "Total Number of Observations During the Period")
                TR14.Font.Name = "cambria"
                TR14.Font.Bold = True
                TR14_01.BackColor = Drawing.Color.WhiteSmoke
                TR14_02.BackColor = Drawing.Color.WhiteSmoke
                TR14_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR14, TR14_03, 8, 2, DTSummary.Rows(0)(19))
                RH.InsertColumn(TR14, TR14_04, 8, 2, ((CInt(DTSummary.Rows(0)(19)) * 100) / CInt(DTSummary.Rows(0)(4))).ToString("0.00"))
                RH.InsertColumn(TR14, TR14_05, 8, 2, DTSummary.Rows(0)(32))
                RH.InsertColumn(TR14, TR14_06, 8, 2, ((CInt(DTSummary.Rows(0)(32)) * 100) / CInt(DTSummary.Rows(0)(25))).ToString("0.00"))
                RH.InsertColumn(TR14, TR14_07, 8, 2, DTSummary.Rows(0)(20))
                RH.InsertColumn(TR14, TR14_08, 8, 2, ((CInt(DTSummary.Rows(0)(20)) * 100) / CInt(DTSummary.Rows(0)(5))).ToString("0.00"))
                RH.InsertColumn(TR14, TR14_09, 3, 0, " ")

                tb.Controls.Add(TR14)

                Dim TR9 As New TableRow
                Dim TR9_01, TR9_02, TR9_03, TR9_04, TR9_05, TR9_06, TR9_07, TR9_08, TR9_09 As New TableCell
                RH.InsertColumn(TR9, TR9_01, 3, 0, " ")
                RH.InsertColumn(TR9, TR9_02, 46, 0, "Highest Observation(Count)  During the Period")
                TR9.Font.Name = "cambria"
                TR9.Font.Bold = True
                TR9_01.BackColor = Drawing.Color.WhiteSmoke
                TR9_02.BackColor = Drawing.Color.WhiteSmoke
                TR9_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR9, TR9_03, 8, 2, DTSummary.Rows(0)(12))
                RH.InsertColumn(TR9, TR9_04, 8, 2, ((CInt(DTSummary.Rows(0)(12)) * 100) / CInt(DTSummary.Rows(0)(19))).ToString("0.00"))
                RH.InsertColumn(TR9, TR9_05, 8, 2, DTSummary.Rows(0)(29))
                RH.InsertColumn(TR9, TR9_06, 8, 2, ((CInt(DTSummary.Rows(0)(29)) * 100) / CInt(DTSummary.Rows(0)(32))).ToString("0.00"))
                RH.InsertColumn(TR9, TR9_07, 8, 2, DTSummary.Rows(0)(15))
                RH.InsertColumn(TR9, TR9_08, 8, 2, ((CInt(DTSummary.Rows(0)(15)) * 100) / CInt(DTSummary.Rows(0)(20))).ToString("0.00"))
                RH.InsertColumn(TR9, TR9_09, 3, 0, " ")

                tb.Controls.Add(TR9)


                Dim TR10 As New TableRow
                Dim TR10_01, TR10_02, TR10_03, TR10_04, TR10_05, TR10_06, TR10_07, TR10_08, TR10_09 As New TableCell
                RH.InsertColumn(TR10, TR10_01, 3, 0, " ")
                RH.InsertColumn(TR10, TR10_02, 46, 0, "Lowest Observations(Count)  During the Period")
                TR10.Font.Name = "cambria"
                TR10.Font.Bold = True
                TR10_01.BackColor = Drawing.Color.WhiteSmoke
                TR10_02.BackColor = Drawing.Color.WhiteSmoke
                TR10_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR10, TR10_03, 8, 2, DTSummary.Rows(0)(13))
                RH.InsertColumn(TR10, TR10_04, 8, 2, ((CInt(DTSummary.Rows(0)(13)) * 100) / CInt(DTSummary.Rows(0)(19))).ToString("0.00"))
                RH.InsertColumn(TR10, TR10_05, 8, 2, DTSummary.Rows(0)(30))
                RH.InsertColumn(TR10, TR10_06, 8, 2, ((CInt(DTSummary.Rows(0)(30)) * 100) / CInt(DTSummary.Rows(0)(32))).ToString("0.00"))
                RH.InsertColumn(TR10, TR10_07, 8, 2, DTSummary.Rows(0)(16))
                RH.InsertColumn(TR10, TR10_08, 8, 2, ((CInt(DTSummary.Rows(0)(16)) * 100) / CInt(DTSummary.Rows(0)(20))).ToString("0.00"))
                RH.InsertColumn(TR10, TR10_09, 3, 0, " ")

                tb.Controls.Add(TR10)

                Dim TR11 As New TableRow
                Dim TR11_01, TR11_02, TR11_03, TR11_04, TR11_05, TR11_06, TR11_07, TR11_08, TR11_09 As New TableCell
                RH.InsertColumn(TR11, TR11_01, 3, 0, " ")
                RH.InsertColumn(TR11, TR11_02, 46, 0, "Average Observations(Count)  During the Period")
                TR11.Font.Name = "cambria"
                TR11.Font.Bold = True
                TR11_01.BackColor = Drawing.Color.WhiteSmoke
                TR11_02.BackColor = Drawing.Color.WhiteSmoke
                TR11_09.BackColor = Drawing.Color.WhiteSmoke
                RH.InsertColumn(TR11, TR11_03, 8, 2, DTSummary.Rows(0)(14))
                RH.InsertColumn(TR11, TR11_04, 8, 2, ((CInt(DTSummary.Rows(0)(14)) * 100) / CInt(DTSummary.Rows(0)(19))).ToString("0.00"))
                RH.InsertColumn(TR11, TR11_05, 8, 2, DTSummary.Rows(0)(31))
                RH.InsertColumn(TR11, TR11_06, 8, 2, ((CInt(DTSummary.Rows(0)(31)) * 100) / CInt(DTSummary.Rows(0)(32))).ToString("0.00"))
                RH.InsertColumn(TR11, TR11_07, 8, 2, DTSummary.Rows(0)(17))
                RH.InsertColumn(TR11, TR11_08, 8, 2, ((CInt(DTSummary.Rows(0)(17)) * 100) / CInt(DTSummary.Rows(0)(20))).ToString("0.00"))
                RH.InsertColumn(TR11, TR11_09, 3, 0, " ")

                tb.Controls.Add(TR11)



                Dim TR13 As New TableRow
                Dim TR13_01 As New TableCell
                RH.InsertColumn(TR13, TR13_01, 100, 2, "&nbsp;")
                TR13.BackColor = Drawing.Color.WhiteSmoke
                TR13.Font.Name = "cambria"
                TR13.Font.Bold = True


                tb.Controls.Add(TR13)
            End If
            '--------------------------------

            'RH.DrawLine(tb, 100)
            RH.BlankRow(tb, 5)
            Dim TotObservations As Double
            Dim I As Integer = 0
            Dim TRAuditHead As New TableRow
            Dim TRAuditHead_00 As New TableCell
            'TRAuditHead_00.BackColor = System.Drawing.Color.BurlyWood
            TRAuditHead_00.BackColor = System.Drawing.Color.Gainsboro
            tb.Controls.Add(TRAuditHead)

            DT = DS.Tables(1)
            TotObservations = 0
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 70, 0, "Check List")
            RH.InsertColumn(TRHead, TRHead_02, 10, 2, "Type")
            RH.InsertColumn(TRHead, TRHead_03, 15, 2, "Status")

            tb.Controls.Add(TRHead)
            I = 1

            For Each DR In DT.Rows
                Dim TR33 As New TableRow
                TR33.BorderWidth = "1"
                TR33.BorderStyle = BorderStyle.Solid

                Dim TR33_00, TR33_01, TR33_02, TR33_03, TR33_04, TR33_05 As New TableCell

                RH.InsertColumn(TR33, TR33_00, 5, 2, I.ToString())
                RH.InsertColumn(TR33, TR33_01, 70, 0, DR(5).ToString())
                RH.InsertColumn(TR33, TR33_02, 10, 2, DR(7).ToString())
                If (CInt(DR(6)) = 0) Then
                    RH.InsertColumn(TR33, TR33_03, 15, 2, "-")
                Else
                    Dim str As String = "<a href='viewAuditReportCons_1.aspx?ItemID=" + DR(4).ToString() + " &FromDt=" + FromDt.ToString() + " &ToDt=" + ToDt.ToString() + " &RegionID=" + GF.Encrypt(RegionID.ToString()) + " &AreaID=" + GF.Encrypt(AreaID.ToString()) + " &BranchID=" + GF.Encrypt(BranchID.ToString()) + " &TypeID=" + TypeID.ToString() + " &AuditTypeID=" + DR(8).ToString() + " &SPMFlag=" + SPMFlag.ToString() + "' style='text-align:right;' target='_blank'>" + DR(6).ToString()
                    RH.InsertColumn(TR33, TR33_03, 15, 2, str)
                End If

                tb.Controls.Add(TR33)
                I = I + 1
                TotObservations += CInt(DR(6))
            Next
            Dim TR55 As New TableRow
            TR55.BorderWidth = "1"
            TR55.BorderStyle = BorderStyle.Solid

            Dim TR55_00, TR55_01 As New TableCell

            TR55_00.BorderWidth = "1"
            TR55_01.BorderWidth = "1"


            TR55_00.BorderColor = Drawing.Color.Silver
            TR55_01.BorderColor = Drawing.Color.Silver


            TR55_00.BorderStyle = BorderStyle.Solid
            TR55_01.BorderStyle = BorderStyle.Solid


            TR55_00.ForeColor = Drawing.Color.DarkBlue
            TR55_01.ForeColor = Drawing.Color.DarkRed

            RH.InsertColumn(TR55, TR55_00, 85, 1, "Total :")
            RH.InsertColumn(TR55, TR55_01, 15, 2, TotObservations)

            tb.Controls.Add(TR55)


            Dim TR_Prep1 As New TableRow
            Dim TR_Prep1_00 As New TableCell
            'TR_Prep1.ForeColor = Drawing.Color.Silver
            'RH.InsertColumn(TR_Prep1, TR_Prep1_00, 100, 100, "l", DTAudit.Rows(0)(4).ToString())
            tb.Controls.Add(TR_Prep1)
            RH.BlankRow(tb, 5)
            Dim TR_Decl As New TableRow
            Dim TR_Decl_00 As New TableCell
            'TR_Decl.ForeColor = Drawing.Color.Silver
            RH.InsertColumn(TR_Decl, TR_Decl_00, 100, 2, "<b> This is a system generated Report and does not require signature </b>")
            tb.Controls.Add(TR_Decl)
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.InsertColumn(TR_End, TR_End_00, 100, 2, "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tbHead)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim sw As New StringWriter()
        'Dim hw As New HtmlTextWriter(sw)
        'pnDisplay.RenderControl(hw)

        'Dim sr As New StringReader(sw.ToString())
        'Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        'Dim htmlparser As New HTMLWorker(pdfDoc)
        'PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        'pdfDoc.Open()
        'htmlparser.Parse(sr)
        'pdfDoc.Close()
        'Response.Write(pdfDoc)
        'Response.[End]()

        Try


            WebTools.ExporttoExcel(DS.Tables(1), StrExcelHead)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
