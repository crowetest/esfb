﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class AgingBucket
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 68) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ReportID = CInt(Request.QueryString.Get("RptID"))
            Me.hdnReportID.Value = ReportID.ToString()
            Me.hdnPostID.Value = Session("Post_ID")
            Dim PostID As Integer = CInt(Session("Post_ID"))
            txtFrom.Text = Format(Date.Now, "dd MMM yyyy")
            If PostID = 8 Then
                DT = DB.ExecuteDataSet("select Branch_id,area_id,region_id from brmaster where zone_head= " & CInt(Session("UserID")) & "").Tables(0)
            ElseIf PostID = 7 Then
                DT = DB.ExecuteDataSet("select Branch_id,area_id,region_id from brmaster where region_head= " & CInt(Session("UserID")) & "").Tables(0)
            ElseIf PostID = 6 Then
                DT = DB.ExecuteDataSet("select Branch_id,area_id,region_id from brmaster where area_head= " & CInt(Session("UserID")) & "").Tables(0)
            ElseIf PostID = 5 Then
                DT = DB.ExecuteDataSet("select Branch_id,area_id,region_id from brmaster where branch_head= " & CInt(Session("UserID")) & "").Tables(0)
            Else
            End If

            If DT.Rows.Count > 0 Then
                Me.hdnBranch.Value = DT.Rows(0)(0)
                Me.hdnArea.Value = DT.Rows(0)(1)
                Me.hdnRegion.Value = DT.Rows(0)(2)
            Else
                Me.hdnBranch.Value = 0
                Me.hdnArea.Value = 0
                Me.hdnRegion.Value = 0
            End If

            GN.ComboFill(cmbAudit, AD.GetAuditForAgingReport(PostID, CInt(Session("UserID"))), 0, 1)
            cmbDaysType.SelectedIndex = 1
            cmbDaysType.Enabled = False
            Me.Master.subtitle = "Audit Aging Report"

            Me.cmbType.Attributes.Add("onchange", "return TypeOnChange()")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub


    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            DT = GN.GetArea(CInt(Data(1)), 2)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
        If CInt(Data(0)) = 2 Then
            DT = GN.GetBranch(CInt(Data(1)), 2)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
End Class
