﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewStaffInvolvementReport_Observation
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim SINO As Integer = 1
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim StatusID As Integer = CInt(Request.QueryString.Get("StatusID"))
            Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
            Dim AuditID As Integer = CInt(GN.Decrypt(Request.QueryString.Get("AuditID")))
            Dim RegionID As Integer = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            Dim AreaID As Integer = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            Dim BranchID As Integer = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            Dim EmpCode As Integer = CInt(GN.Decrypt(Request.QueryString.Get("EmpCode")))
            Dim EmpName As String
            EmpName = GN.GetEmpName(EmpCode).Rows(0)(0)
            Dim BranchName = ""
            If BranchID > 0 Then
                BranchName = GN.GetBranch_Name(BranchID) + " Branch "
            End If
            Dim Status As String = ""
            If StatusID = 0 Then
                Status = "Status : Closed"
            ElseIf StatusID = 1 Then
                Status = "Status : Pending"
            End If

            Dim tb As New Table
            tb.Attributes.Add("width", "100%")
            tb.Style.Add("table-layout", "fixed")
            RH.Heading(Session("FirmName"), tb, "Financial Leakage Settlement-Recovery From " + FromDt.ToString("dd/MMM/yyyy") + " To " + ToDt.ToString("dd/MMM/yyyy") + " " + BranchName + EmpName + " Employee " + Status, 100)

            Dim RowBG As Integer = 0
            Dim TRHead As New TableRow
            TRHead.Style.Add("Font-weight", "bold")
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_01, 10, 0, "Branch Name")
            RH.InsertColumn(TRHead, TRHead_02, 5, 2, "Audit")
            RH.InsertColumn(TRHead, TRHead_03, 10, 0, "Observation")
            RH.InsertColumn(TRHead, TRHead_04, 8, 0, "Ref. No")
            RH.InsertColumn(TRHead, TRHead_05, 15, 0, "Remarks")
            RH.InsertColumn(TRHead, TRHead_06, 7, 0, "Status")
            RH.InsertColumn(TRHead, TRHead_07, 5, 2, "Intention- ally")
            RH.InsertColumn(TRHead, TRHead_08, 5, 1, "Amount")
            RH.InsertColumn(TRHead, TRHead_09, 25, 2, "Recovery Status")
            RH.InsertColumn(TRHead, TRHead_10, 5, 1, "Balance")
            tb.Controls.Add(TRHead)

            Dim TRHead1 As New TableRow
            TRHead1.Style.Add("Font-weight", "bold")
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10, TRHead1_11, TRHead1_12, TRHead1_13, TRHead1_14 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "")
            RH.InsertColumn(TRHead1, TRHead1_01, 10, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_02, 5, 2, "")
            RH.InsertColumn(TRHead1, TRHead1_03, 10, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_05, 15, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_06, 7, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_07, 5, 2, "")
            RH.InsertColumn(TRHead1, TRHead1_08, 5, 1, "")
            RH.InsertColumn(TRHead1, TRHead1_09, 5, 0, "Recovery") '1
            RH.InsertColumn(TRHead1, TRHead1_10, 5, 0, "Recti- fication") '2
            RH.InsertColumn(TRHead1, TRHead1_11, 5, 0, "HR Action") '3
            RH.InsertColumn(TRHead1, TRHead1_12, 5, 0, "Legal Initiation") '4
            RH.InsertColumn(TRHead1, TRHead1_13, 5, 0, "Write-Off") '5
            RH.InsertColumn(TRHead1, TRHead1_14, 5, 1, "")
            tb.Controls.Add(TRHead1)

            Dim SQL As String = ""
            If AuditID > 0 Then
                SQL = " AND  g.AUDIT_ID = " + AuditID.ToString() + " "
            End If
            If EmpCode > 0 Then
                SQL += " and a.EMP_CODE=" + EmpCode.ToString()
            End If
            If BranchID > 0 Then
                SQL += " and c.branch_id = " + BranchID.ToString() + ""
            End If
            If StatusID = -1 Then
                DT = DB.ExecuteDataSet("select e.SINO,c.Branch_Name,h.PERIOD_TO ,i.ITEM_NAME,e.loan_no,e.REMARKS,isnull(a.Amount,0) Total_amount,isnull((d.Recover),0) Recover,isnull((d.Rectification),0) Rectification,isnull((d.Hraction),0) Hraction,isnull((d.Legal),0) Legal,isnull((d.Writeoff),0) Writeoff,(isnull(a.Amount,0)-isnull(a.refund_amount,0)) Balance,case  when a.intentionally  =1 then 'Yes' else '-' end  as intentionally,r.display_status from emp_master b,BrMaster c,AUDIT_OBSERVATION_DTL e,audit_observation f,AUDIT_DTL g,AUDIT_MASTER h,AUDIT_OBSERVATION_STATUS_MASTER r,AUDIT_CHECK_LIST i,AUDIT_STAFF_INVOLVEMENT a left outer join ( select emp_code,SLNO,sum(case when typeid = 1 then amount else 0 end) as Recover, sum(case when typeid = 2 then amount else 0 end) as Writeoff,sum(case when typeid = 3 then amount else 0 end) as Rectification, sum(case when typeid = 4 then amount else 0 end) as Legal,sum(case when typeid = 5 then amount else 0 end) as Hraction from  AUDIT_STAFF_INVOLVEMENT_RECOVERY_DTL group by EMP_CODE,SLNO)d on(d.SLNO=a.SINO and d.EMP_CODE=a.EMP_CODE )  where a.EMP_CODE = b.Emp_Code and h.Branch_ID = c.Branch_ID and a.SINO = e.SINO and e.OBSERVATION_ID = f.OBSERVATION_ID  and f.AUDIT_ID = g.AUDIT_ID and g.GROUP_ID = h.GROUP_ID and e.status_id=r.status_id  and h.STATUS_ID = 2 and f.item_id=i.item_id  and DATEADD(day, DATEDIFF(day, 0, a.Tra_DT), 0) between '" + FromDt.ToString() + "' and  '" + ToDt.ToString() + "'    " + SQL.ToString() + "").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select e.SINO,c.Branch_Name,h.PERIOD_TO ,i.ITEM_NAME,e.loan_no,e.REMARKS,isnull(a.Amount,0) Total_amount,isnull((d.Recover),0) Recover,isnull((d.Rectification),0) Rectification,isnull((d.Hraction),0) Hraction,isnull((d.Legal),0) Legal,isnull((d.Writeoff),0) Writeoff,(isnull(a.Amount,0)-isnull(a.refund_amount,0)) Balance,case  when a.intentionally  =1 then 'Yes' else '-' end  as intentionally,r.display_status from emp_master b,BrMaster c,AUDIT_OBSERVATION_DTL e,audit_observation f,AUDIT_DTL g,AUDIT_MASTER h,AUDIT_OBSERVATION_STATUS_MASTER r,AUDIT_CHECK_LIST i,AUDIT_STAFF_INVOLVEMENT a left outer join ( select emp_code,SLNO,sum(case when typeid = 1 then amount else 0 end) as Recover, sum(case when typeid = 2 then amount else 0 end) as Writeoff,sum(case when typeid = 3 then amount else 0 end) as Rectification, sum(case when typeid = 4 then amount else 0 end) as Legal,sum(case when typeid = 5 then amount else 0 end) as Hraction from  AUDIT_STAFF_INVOLVEMENT_RECOVERY_DTL group by EMP_CODE,SLNO)d on(d.SLNO=a.SINO and d.EMP_CODE=a.EMP_CODE )  where a.EMP_CODE = b.Emp_Code and h.Branch_ID = c.Branch_ID and a.SINO = e.SINO and e.OBSERVATION_ID = f.OBSERVATION_ID  and f.AUDIT_ID = g.AUDIT_ID and g.GROUP_ID = h.GROUP_ID  and e.status_id=r.status_id and a.Status_ID = " + StatusID.ToString() + "  and h.STATUS_ID = 2 and f.item_id=i.item_id  and DATEADD(day, DATEDIFF(day, 0, a.Tra_DT), 0) between '" + FromDt.ToString() + "' and  '" + ToDt.ToString() + "'    " + SQL.ToString() + " ").Tables(0)
            End If

            Dim DR As DataRow
            Dim Tot1 As Double = 0
            Dim Tot2 As Double = 0
            Dim Tot3 As Double = 0
            Dim Tot4 As Double = 0
            Dim Tot5 As Double = 0
            Dim Tot6 As Double = 0
            Dim Tot7 As Double = 0
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, SINO.ToString())
                RH.InsertColumn(TR3, TR3_01, 10, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_02, 5, 2, Format(DR(2), "MMM-yyyy").ToString())
                RH.InsertColumn(TR3, TR3_03, 10, 0, DR(3).ToString())
                RH.InsertColumn(TR3, TR3_04, 8, 0, DR(4).ToString())
                RH.InsertColumn(TR3, TR3_05, 15, 0, DR(5).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                RH.InsertColumn(TR3, TR3_06, 7, 0, DR(14).ToString())
                RH.InsertColumn(TR3, TR3_07, 5, 2, DR(13).ToString())
                RH.InsertColumn(TR3, TR3_08, 5, 1, DR(6).ToString())
                If (DR(7) > 0) Then
                    RH.InsertColumn(TR3, TR3_09, 5, 1, "<a href='ViewStaffInvolvementReport_RecoveryDtl.aspx?TypeID=1 &SINo=" + GN.Encrypt(DR(0).ToString()) + "&EmpCode=" + GN.Encrypt(EmpCode.ToString()) + "' target=_blank>" + DR(7).ToString() + "</a>")
                Else
                    RH.InsertColumn(TR3, TR3_09, 5, 1, "-")
                End If
                If (DR(8) > 0) Then
                    RH.InsertColumn(TR3, TR3_10, 5, 1, "<a href='ViewStaffInvolvementReport_RecoveryDtl.aspx?TypeID=3  &SINo=" + GN.Encrypt(DR(0).ToString()) + "&EmpCode=" + GN.Encrypt(EmpCode.ToString()) + "' target=_blank>" + DR(8).ToString() + "</a>")
                Else
                    RH.InsertColumn(TR3, TR3_10, 5, 1, "-")
                End If
                If (DR(9) > 0) Then
                    RH.InsertColumn(TR3, TR3_11, 5, 1, "<a href='ViewStaffInvolvementReport_RecoveryDtl.aspx?TypeID=5 &SINo=" + GN.Encrypt(DR(0).ToString()) + "&EmpCode=" + GN.Encrypt(EmpCode.ToString()) + "' target=_blank>" + DR(9).ToString() + "</a>")
                Else
                    RH.InsertColumn(TR3, TR3_11, 5, 1, "-")
                End If
                If (DR(10) > 0) Then
                    RH.InsertColumn(TR3, TR3_12, 5, 1, "<a href='ViewStaffInvolvementReport_RecoveryDtl.aspx?TypeID=4 &SINo=" + GN.Encrypt(DR(0).ToString()) + "&EmpCode=" + GN.Encrypt(EmpCode.ToString()) + "' target=_blank>" + DR(10).ToString() + "</a>")
                Else
                    RH.InsertColumn(TR3, TR3_12, 5, 1, "-")
                End If
                If (DR(11) > 0) Then
                    RH.InsertColumn(TR3, TR3_13, 5, 1, "<a href='ViewStaffInvolvementReport_RecoveryDtl.aspx?TypeID=2 &SINo=" + GN.Encrypt(DR(0).ToString()) + "&EmpCode=" + GN.Encrypt(EmpCode.ToString()) + "' target=_blank>" + DR(11).ToString() + "</a>")
                Else
                    RH.InsertColumn(TR3, TR3_13, 5, 1, "-")
                End If
                RH.InsertColumn(TR3, TR3_14, 5, 1, DR(12).ToString())

                tb.Controls.Add(TR3)
                SINO += 1
                Tot1 += DR(6)
                Tot2 += DR(7)
                Tot3 += DR(8)
                Tot4 += DR(9)
                Tot5 += DR(10)
                Tot6 += DR(11)
                Tot7 += DR(12)
            Next

            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06, TRFooter_07, TRFooter_08 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            RH.InsertColumn(TRFooter, TRFooter_00, 5, 2, "")
            RH.InsertColumn(TRFooter, TRFooter_01, 60, 0, "TOTAL")
            RH.InsertColumn(TRFooter, TRFooter_02, 5, 1, Tot1.ToString("0.00"))
            If (Tot2 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_03, 5, 1, Tot2.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_03, 5, 1, "-")
            End If
            If (Tot3 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_04, 5, 1, Tot3.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_04, 5, 1, "-")
            End If
            If (Tot4 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_05, 5, 1, Tot4.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_05, 5, 1, "-")
            End If
            If (Tot5 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_06, 5, 1, Tot5.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_06, 5, 1, "-")
            End If
            If (Tot6 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_07, 5, 1, Tot6.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_07, 5, 1, "-")
            End If
            RH.InsertColumn(TRFooter, TRFooter_08, 5, 1, Tot7.ToString("0.00"))

            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
