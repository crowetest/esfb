﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewObservationPendingBucketReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 48) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable



            RH.Heading(Session("FirmName"), tb, "OBSERVATION LAG-BUCKET WISE REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")


            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)


            tb.Controls.Add(TRItemHead)
            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 55, 55, "l", "Item Name")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "0 - 10 Days")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "11 - 20 Days")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "c", "21 - 30 Days")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "Above 30")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            DT = DB.ExecuteDataSet("SELECT SUM(Bucket10)as Bucket10,SUM(Bucket20)as Bucket20,SUM(Bucket30)as Bucket30,SUM(Bucket30Above)as Bucket30Above,item_name FROM ( " & _
                " SELECT CASE WHEN daycount between 0 and 10 then CNT ELSE 0 END as Bucket10,CASE WHEN daycount between 11 and 20 then CNT  ELSE 0 END as Bucket20  " & _
                " ,CASE WHEN daycount between 21 and 30 then CNT  ELSE 0 END as Bucket30,CASE WHEN daycount >30 then CNT  ELSE 0 END as Bucket30Above,item_name from (  " & _
                " SELECT COUNT(daycount) as CNT,item_name,daycount from (select DATEDIFF(dd,a.updated_dt,DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)) as daycount,b.ITEM_ID,e.ITEM_NAME  " & _
                " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,dbo.AUDIT_CHECK_LIST e where a.OBSERVATION_ID=b.OBSERVATION_ID and  " & _
                " b.AUDIT_ID = c.AUDIT_ID And b.ITEM_ID = e.ITEM_ID And c.GROUP_ID = d.Group_ID And a.STATUS_ID > 0 and d.status_id=2 )AA GROUP BY item_name,daycount  " & _
                " )BB )CC GROUP BY item_name").Tables(0)
            Dim Tot_Buckert1 As Integer = 0
            Dim Tot_Buckert2 As Integer = 0
            Dim Tot_Buckert3 As Integer = 0
            Dim Tot_Buckert4 As Integer = 0
            For Each DR In DT.Rows
                j += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 55, 55, "l", DR(4))
                RH.AddColumn(TR3, TR3_02, 10, 10, "c", DR(0))
                RH.AddColumn(TR3, TR3_03, 10, 10, "c", DR(1))
                RH.AddColumn(TR3, TR3_04, 10, 10, "c", DR(2))
                RH.AddColumn(TR3, TR3_05, 10, 10, "c", DR(3))
                tb.Controls.Add(TR3)
                I = I + 1
                Tot_Buckert1 += DR(0)
                Tot_Buckert2 += DR(1)
                Tot_Buckert3 += DR(2)
                Tot_Buckert4 += DR(3)
            Next


            Dim TRTOT As New TableRow
            TRTOT.BorderWidth = "1"
            TRTOT.BorderStyle = BorderStyle.Solid
            TRTOT.Style.Add("font-weight", "bold")
            Dim TRTOT_00, TRTOT_01, TRTOT_02, TRTOT_03, TRTOT_04, TRTOT_05 As New TableCell

            TRTOT_00.BorderWidth = "1"
            TRTOT_01.BorderWidth = "1"
            TRTOT_02.BorderWidth = "1"
            TRTOT_03.BorderWidth = "1"
            TRTOT_04.BorderWidth = "1"
            TRTOT_05.BorderWidth = "1"

            TRTOT_00.BorderColor = Drawing.Color.Silver
            TRTOT_01.BorderColor = Drawing.Color.Silver
            TRTOT_02.BorderColor = Drawing.Color.Silver
            TRTOT_03.BorderColor = Drawing.Color.Silver
            TRTOT_04.BorderColor = Drawing.Color.Silver
            TRTOT_05.BorderColor = Drawing.Color.Silver

            TRTOT_00.BorderStyle = BorderStyle.Solid
            TRTOT_01.BorderStyle = BorderStyle.Solid
            TRTOT_02.BorderStyle = BorderStyle.Solid
            TRTOT_03.BorderStyle = BorderStyle.Solid
            TRTOT_04.BorderStyle = BorderStyle.Solid
            TRTOT_05.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRTOT, TRTOT_00, 5, 5, "c", "")
            RH.AddColumn(TRTOT, TRTOT_01, 55, 55, "l", "Total")
            RH.AddColumn(TRTOT, TRTOT_02, 10, 10, "c", Tot_Buckert1.ToString())
            RH.AddColumn(TRTOT, TRTOT_03, 10, 10, "c", Tot_Buckert2.ToString())
            RH.AddColumn(TRTOT, TRTOT_04, 10, 10, "c", Tot_Buckert3.ToString())
            RH.AddColumn(TRTOT, TRTOT_05, 10, 10, "c", Tot_Buckert4.ToString())
            tb.Controls.Add(TRTOT)
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
