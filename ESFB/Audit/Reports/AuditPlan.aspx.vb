﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_AuditPlan
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim PrdTo As New DateTime
            Dim Mnth = CInt(GF.Decrypt(Request.QueryString.Get("Mnth")))
            Dim YR = CInt(GF.Decrypt(Request.QueryString.Get("Yr")))
            Dim daysIn As Integer = System.DateTime.DaysInMonth(YR, Mnth)
            PrdTo = New DateTime(YR, Mnth, daysIn)

            Dim DT As New DataTable
            RH.Heading(Session("FirmName"), tb, "AUDIT PLAN FOR " + PrdTo.ToString("dd MMM yyyy"), 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim PendingTotal As Integer = 0
            Dim ProgressTotal As Integer = 0
            Dim CompletedTotal As Integer = 0
            Dim Total As Integer = 0
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 25, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_02, 25, 0, "Auditor")
            RH.InsertColumn(TRHead, TRHead_03, 25, 0, "Team Lead")
            RH.InsertColumn(TRHead, TRHead_04, 5, 0, "Interval (Months)")
            RH.InsertColumn(TRHead, TRHead_05, 15, 0, "Last Audit")

            tb.Controls.Add(TRHead)

            DT = DB.ExecuteDataSet("SELECT a.BRANCH_ID,d.Branch_Name,b.Emp_Name as Auditor,c.Emp_Name as TeamLead ,AUDIT_INTERVAL ,LAST_AUDIT_DATE  FROM AUDIT_BRANCHES a,EMP_MASTER b,EMP_MASTER c ,branch_master d WHERE a.branch_id=d.Branch_ID  and a.EMP_CODE=b.Emp_Code and a.TEAM_LEAD=c.Emp_Code  and  (LAST_AUDIT_ASSIGN_DT IS NULL OR LAST_AUDIT_ASSIGN_DT < CONVERT(VARCHAR(10),GETDATE(),111) ) AND DATEADD(MM,AUDIT_INTERVAL,LAST_AUDIT_DATE )<=DATEADD(day, DATEDIFF(day, 0, '" + PrdTo.ToString("dd/MMM/yyyy") + "'), 0) and a.branch_id>100  ORDER BY a.BRANCH_ID ").Tables(0)
            For Each DR In DT.Rows
                LineNumber += 1
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell
                I = I + 1
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString().ToString())
                RH.InsertColumn(TR3, TR3_01, 25, 0, DR(1).ToString().ToUpper())
                RH.InsertColumn(TR3, TR3_02, 25, 0, DR(2).ToString().ToUpper())
                RH.InsertColumn(TR3, TR3_03, 25, 0, DR(3).ToString().ToUpper())
                RH.InsertColumn(TR3, TR3_04, 5, 2, DR(4).ToString().ToUpper())
                RH.InsertColumn(TR3, TR3_05, 15, 0, CDate(DR(5)).ToString("dd MMM yyyy"))
                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 25)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
