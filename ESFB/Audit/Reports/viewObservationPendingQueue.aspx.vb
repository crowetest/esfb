﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewObservationPendingQueue
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim AuditTypeID As Integer
    Dim From As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 63) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))
            RH.Heading(Session("FirmName"), tb, "VERIFICATION PENDING QUEUE", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = AD.ObservationPendingQueue(PostID)
            Dim TRHead As New TableRow
            Dim colWidth As Integer = 100 - ((DT.Columns.Count - 2) * 13)
            Dim TRHead_00 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_00.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRHead, TRHead_00, colWidth, colWidth, "l", "Level")
            For i = 2 To DT.Columns.Count - 1
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_01 As New TableCell
                TRHead_01.BorderWidth = "1"
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_01, 13, 13, "c", DT.Columns(i).ColumnName)
            Next
            Dim wdth As Integer = 0
            Dim aln As String
            Dim Tot As Integer
            Dim dispText As String
            Dim total((DT.Columns.Count - 2)) As Integer
            tb.Controls.Add(TRHead)
            For Each DR In DT.Rows
                Dim TR01 As New TableRow
                For i = 1 To DT.Columns.Count - 1
                    TR01.BackColor = Drawing.Color.WhiteSmoke
                    Dim TR01_01 As New TableCell
                    TR01_01.BorderWidth = "1"
                    TR01_01.BorderColor = Drawing.Color.Silver
                    TR01_01.BorderStyle = BorderStyle.Solid
                    Tot = 0
                    If i = 1 Then
                        wdth = colWidth
                        aln = "l"
                        dispText = DR(i).ToString()
                    Else
                        wdth = 13
                        aln = "c"
                        If (IsDBNull(total(i - 2))) Then
                            total(i - 2) = 0
                        End If
                        If (Not IsDBNull(DR(i))) Then
                            dispText = "<a href='viewObservationPendingQueue_1.aspx?LevelID=" + DR(0).ToString() + " &Role=" + DT.Columns(i).ColumnName + "'>" + DR(i).ToString()
                            Tot = CInt(DR(i))
                        Else
                            dispText = "-"
                        End If
                        total(i - 2) += Tot
                    End If
                    RH.AddColumn(TR01, TR01_01, wdth, wdth, aln, dispText)
                Next
                tb.Controls.Add(TR01)
            Next
            Dim TRHead_End As New TableRow
            TRHead_End.Style.Add("Font-Weight", "Bold")
            Dim TRHead_End_00 As New TableCell
            TRHead_End_00.BorderWidth = "1"
            TRHead_End_00.BorderColor = Drawing.Color.Silver
            TRHead_End_00.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRHead_End, TRHead_End_00, colWidth, colWidth, "l", "Total")
            For i = 2 To DT.Columns.Count - 1
                TRHead_End.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_End_01 As New TableCell
                TRHead_End_01.BorderWidth = "1"
                TRHead_End_01.BorderColor = Drawing.Color.Silver
                TRHead_End_01.BorderStyle = BorderStyle.Solid
                If (total(i - 2) = 0) Then
                    dispText = "-"
                Else
                    dispText = total(i - 2)
                End If
                RH.AddColumn(TRHead_End, TRHead_End_01, 13, 13, "c", dispText)
            Next
            tb.Controls.Add(TRHead_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
