﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewVisitedAssignSangamsConsolidated
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AssingDt As String
    Dim From As Integer
    Dim RptID As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim SubHD = ""
            Dim DR As DataRow
            AssingDt = ""
            FromDt = DateTime.Now.Date
            ToDt = DateTime.Now.Date

            RptID = Request.QueryString.Get("RptID")
            If (RptID = 1) Then
                AssingDt = GF.Decrypt(Request.QueryString.Get("AuditID"))
                Dim AssignDtl() As String
                AssignDtl = AssingDt.Split("~")
                tb.Attributes.Add("width", "100%")
                RH.Heading(Session("FirmName"), tb, "Assign Sangam List of " + MonthName(CInt(AssignDtl(0))) + "-" + AssignDtl(1) + " Audit", 100)
                Dim RowBG As Integer = 0

                SubHD = MonthName(CInt(AssignDtl(0))) + "-" + AssignDtl(1) + ""
                DT = DB.ExecuteDataSet("select p.branch_id,p.branch_name,p.emp_name,sum(p.fldCount)as FieldAuditCount,max(fldAuditID)as FieldAuditID from (select c.BRANCH_ID,d.Branch_Name,e.Emp_Name, COUNT(distinct b.CENTER_ID) as fldCount,max(c.AUDIT_ID) as fldAuditID from AUDIT_ACCOUNTS a,LOAN_MASTER b,AUDIT_DTL  c,BRANCH_MASTER d,EMP_MASTER e,Audit_Master f where a.LOAN_NO=b.LOAN_NO and a.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID and c.EMP_CODE=e.Emp_Code and c.group_id=f.group_ID and month(f.period_to) =" + AssignDtl(0) + " and year(f.period_to)=" + AssignDtl(1) + " and c.audit_type in(2,4) group by c.BRANCH_ID,d.Branch_Name,e.Emp_Name,c.AUDIT_TYPE) p group by p.branch_id,p.branch_name,p.emp_name order by 2").Tables(0)
            ElseIf RptID = 2 Then
                FromDt = CDate(Request.QueryString.Get("FromDt"))
                ToDt = CDate(Request.QueryString.Get("ToDt"))
                SubHD = FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy") + ""
                RH.Heading(Session("FirmName"), tb, "Assign Sangam List of Audits Ended during  " + FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy"), 100)
                DT = DB.ExecuteDataSet("select p.branch_id,p.branch_name,p.emp_name,sum(p.fldCount)as FieldAuditCount,0 from (select c.BRANCH_ID,d.Branch_Name,e.Emp_Name, COUNT(distinct b.CENTER_ID) as fldCount,0 as FieldAuditID from AUDIT_ACCOUNTS a,LOAN_MASTER b,AUDIT_DTL  c,BRANCH_MASTER d,EMP_MASTER e,Audit_Master f where a.LOAN_NO=b.LOAN_NO and a.AUDIT_ID=c.AUDIT_ID and c.BRANCH_ID=d.Branch_ID and c.EMP_CODE=e.Emp_Code and c.group_id=f.group_ID and f.End_dt between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' and c.audit_type in(2,4) group by c.BRANCH_ID,d.Branch_Name,e.Emp_Name) p group by p.branch_id,p.branch_name,p.emp_name order by 2").Tables(0)
            End If

            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 30, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_02, 35, 0, "Auditor")
            RH.InsertColumn(TRHead, TRHead_03, 30, 2, "No of Centers Assigned")
            Dim I As Integer = 1
            tb.Controls.Add(TRHead)

            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                RH.InsertColumn(TR3, TR3_01, 30, 0, DR(1))
                RH.InsertColumn(TR3, TR3_02, 35, 0, DR(2))
                RH.InsertColumn(TR3, TR3_03, 30, 2, " <a href='ViewVisitedAssignCenters.aspx?AuditID=" + GF.Encrypt(DR(4)) + " &AssignDate=" + AssingDt.ToString() + " &SubHD=Assign Centers - " + DR(1).ToString() + "-" + SubHD.ToString() + " &RptID=" + RptID.ToString() + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + " &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &BranchID=" + GF.Encrypt(DR(0)) + "' target='blank'/>" + DR(3).ToString())

                tb.Controls.Add(TR3)
                I = I + 1

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
