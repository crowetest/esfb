﻿Imports System.Data
Partial Class EmergencyAuditReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 363) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim Postid As Integer = CInt(Session("Post_ID"))
            Dim EmpCode As Integer = CInt(Session("UserID"))
            Me.Master.subtitle = "Emergency Audit Details"
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
