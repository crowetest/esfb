﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementLoanWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim AD As New Audit
    Dim BranchID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim ItemID As Integer
    Dim EmpCode As Integer
    Dim TypeID As Integer
    Dim GN As New GeneralFunctions
    Dim CloseFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            ItemID = CInt(Request.QueryString.Get("ItemID"))
            EmpCode = CInt(Request.QueryString.Get("EmpCode"))
            TypeID = CInt(Request.QueryString.Get("TypeID"))
            CloseFlag = Request.QueryString.Get("CloseFlag")
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim SubHD As String = "Report Parameters:"
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
                SubHD += " Period " + MonthName(Month) + "-" + year.ToString()
            End If

            If BranchID > 0 Then
                SubHD += " Branch - " + GN.GetBranch_Name(BranchID)
            ElseIf AreaID > 0 Then
                SubHD += " Area - " + GN.GetArea_Name(AreaID)
            ElseIf RegionID > 0 Then
                SubHD += " Region - " + GN.GetRegion_Name(RegionID)
            Else
                SubHD += " Consolidated"
            End If
            If ItemID > 0 Then
                Dim DTItem As New DataTable
                DTItem = AD.GetItemName(ItemID, 0)
                SubHD += " Check List - " + DTItem.Rows(0)(0).ToString()
            End If
            If TypeID = 1 Then
                SubHD += " Type - Very Serious"
            ElseIf TypeID = 2 Then
                SubHD += " Type - Serious"
            ElseIf TypeID = 3 Then
                SubHD += " Type - General"
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable

            RH.Heading(Session("FirmName"), tb, "Staff Involvement Report " + Status, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            RH.SubHeading(tb, 100, "l", SubHD)

            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim BranchName As String = ""
            Dim ItemName As String = ""

            'RH.BlankRow(tb, 3)
            Dim TotObservation As Integer = 0
            Dim TotStaff As Integer = 0
            Dim TotLeakage As Integer = 0
            Dim first As Integer = 0
            Dim CheckTotal As Integer = 0
            DT = AD.GetStaffInvolvementLoan(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, EmpCode, CloseFlag, TypeID, FromDt, ToDt)
            For Each DR In DT.Rows

                If BranchName <> DR(6) Or ItemName <> DR(5) Then
                    If BranchName <> DR(6) Then
                        Dim trBranch As New TableRow
                        Dim trBranch_00 As New TableCell
                        trBranch.Style.Add("background-color", "LightSteelblue")
                        RH.AddColumn(trBranch, trBranch_00, 100, 100, "c", DR(6))

                        tb.Controls.Add(trBranch)
                        j = 0
                    End If
                    If ItemName <> DR(5) Then
                        If first <> 0 Then
                            Dim TRCHeckFooter As New TableRow
                            TRCHeckFooter.BackColor = Drawing.Color.WhiteSmoke
                            Dim TRCHeckFooter_00, TRCHeckFooter_01, TRCHeckFooter_02, TRCHeckFooter_03, TRCHeckFooter_04, TRCHeckFooter_05, TRCHeckFooter_06 As New TableCell
                            TRCHeckFooter.Style.Add("Font-weight", "bold")
                            TRCHeckFooter_00.BorderWidth = "1"
                            TRCHeckFooter_01.BorderWidth = "1"
                            TRCHeckFooter_02.BorderWidth = "1"
                            TRCHeckFooter_03.BorderWidth = "1"
                            TRCHeckFooter_04.BorderWidth = "1"
                            TRCHeckFooter_05.BorderWidth = "1"
                            TRCHeckFooter_06.BorderWidth = "1"

                            TRCHeckFooter_00.BorderColor = Drawing.Color.Silver
                            TRCHeckFooter_01.BorderColor = Drawing.Color.Silver
                            TRCHeckFooter_02.BorderColor = Drawing.Color.Silver
                            TRCHeckFooter_03.BorderColor = Drawing.Color.Silver
                            TRCHeckFooter_04.BorderColor = Drawing.Color.Silver
                            TRCHeckFooter_05.BorderColor = Drawing.Color.Silver
                            TRCHeckFooter_06.BorderColor = Drawing.Color.Silver

                            TRCHeckFooter_00.BorderStyle = BorderStyle.Solid
                            TRCHeckFooter_01.BorderStyle = BorderStyle.Solid
                            TRCHeckFooter_02.BorderStyle = BorderStyle.Solid
                            TRCHeckFooter_03.BorderStyle = BorderStyle.Solid
                            TRCHeckFooter_04.BorderStyle = BorderStyle.Solid
                            TRCHeckFooter_05.BorderStyle = BorderStyle.Solid
                            TRCHeckFooter_06.BorderStyle = BorderStyle.Solid

                            RH.AddColumn(TRCHeckFooter, TRCHeckFooter_00, 5, 5, "c", "")
                            RH.AddColumn(TRCHeckFooter, TRCHeckFooter_01, 15, 15, "l", "Total")
                            RH.AddColumn(TRCHeckFooter, TRCHeckFooter_02, 40, 40, "l", "")
                            RH.AddColumn(TRCHeckFooter, TRCHeckFooter_03, 10, 10, "c", "")
                            If TotLeakage > 0 Then
                                RH.AddColumn(TRCHeckFooter, TRCHeckFooter_04, 10, 10, "r", CheckTotal.ToString("#,##,###.00"))
                            Else
                                RH.AddColumn(TRCHeckFooter, TRCHeckFooter_04, 10, 10, "r", "-")
                            End If

                            RH.AddColumn(TRCHeckFooter, TRCHeckFooter_05, 10, 10, "c", "")
                            RH.AddColumn(TRCHeckFooter, TRCHeckFooter_06, 10, 10, "c", "")
                            tb.Controls.Add(TRCHeckFooter)
                        End If
                        first += 1
                        RH.BlankRow(tb, 5)
                        Dim trItem As New TableRow
                        Dim trItem_00 As New TableCell
                        trItem.Style.Add("background-color", "lightsilver")
                        CheckTotal = 0
                        RH.AddColumn(trItem, trItem_00, 100, 100, "l", "<strong><i>Check List : " + DR(5) + "</i></strong>")
                        tb.Controls.Add(trItem)
                        j = 0
                        RH.BlankRow(tb, 5)
                    End If
                    Dim TRHead As New TableRow
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

                    TRHead_00.BorderWidth = "1"
                    TRHead_01.BorderWidth = "1"
                    TRHead_02.BorderWidth = "1"
                    TRHead_03.BorderWidth = "1"
                    TRHead_04.BorderWidth = "1"
                    TRHead_05.BorderWidth = "1"
                    TRHead_06.BorderWidth = "1"

                    TRHead_00.BorderColor = Drawing.Color.Silver
                    TRHead_01.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_03.BorderColor = Drawing.Color.Silver
                    TRHead_04.BorderColor = Drawing.Color.Silver
                    TRHead_05.BorderColor = Drawing.Color.Silver
                    TRHead_06.BorderColor = Drawing.Color.Silver

                    TRHead_00.BorderStyle = BorderStyle.Solid
                    TRHead_01.BorderStyle = BorderStyle.Solid
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    TRHead_03.BorderStyle = BorderStyle.Solid
                    TRHead_04.BorderStyle = BorderStyle.Solid
                    TRHead_05.BorderStyle = BorderStyle.Solid
                    TRHead_06.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
                    RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Ref. No")
                    RH.AddColumn(TRHead, TRHead_02, 40, 40, "l", "Last Remarks")
                    RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Status")
                    RH.AddColumn(TRHead, TRHead_04, 10, 10, "c", "Est. Amt. Involved")
                    RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "No of Staffs")
                    RH.AddColumn(TRHead, TRHead_06, 10, 10, "c", "Enter Date")
                    tb.Controls.Add(TRHead)
                End If
                ItemName = DR(5)
                BranchName = DR(6)
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 15, 15, "l", "<a href='viewLoanDetails.aspx?LoanNo=" + GN.Encrypt(DR(0)) + "' style='text-align:right;' target='_blank'>" + DR(0) + "</a>")
                RH.AddColumn(TR3, TR3_02, 40, 40, "l", DR(7).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(4)) + "' style='text-align:right;' target='_blank' >Previous</a>")
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(1).ToString())
                If DR(2) = 0 Then
                    RH.AddColumn(TR3, TR3_04, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_04, 10, 10, "c", CDbl(DR(2)).ToString("#,##,###.00"))
                End If
                If DR(3) = 0 Then
                    RH.AddColumn(TR3, TR3_05, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_05, 10, 10, "c", "<a href='viewStaffInvolvementEmpWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt("0") + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt("0") + " &SINo=" + DR(4).ToString() + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='blank'>" + DR(3).ToString() + "</a>")
                End If
                RH.AddColumn(TR3, TR3_06, 10, 10, "c", CDate(DR(8)).ToString("dd/MMM/yyyy"))
                tb.Controls.Add(TR3)
                TotLeakage += DR(2)
                CheckTotal += DR(2)
                I += 1
            Next

            Dim TRTotCHeckFooter As New TableRow
            TRTotCHeckFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRTotCHeckFooter_00, TRTotCHeckFooter_01, TRTotCHeckFooter_02, TRTotCHeckFooter_03, TRTotCHeckFooter_04, TRTotCHeckFooter_05, TRTotCHeckFooter_06 As New TableCell
            TRTotCHeckFooter.Style.Add("Font-weight", "bold")
            TRTotCHeckFooter_00.BorderWidth = "1"
            TRTotCHeckFooter_01.BorderWidth = "1"
            TRTotCHeckFooter_02.BorderWidth = "1"
            TRTotCHeckFooter_03.BorderWidth = "1"
            TRTotCHeckFooter_04.BorderWidth = "1"
            TRTotCHeckFooter_05.BorderWidth = "1"
            TRTotCHeckFooter_06.BorderWidth = "1"

            TRTotCHeckFooter_00.BorderColor = Drawing.Color.Silver
            TRTotCHeckFooter_01.BorderColor = Drawing.Color.Silver
            TRTotCHeckFooter_02.BorderColor = Drawing.Color.Silver
            TRTotCHeckFooter_03.BorderColor = Drawing.Color.Silver
            TRTotCHeckFooter_04.BorderColor = Drawing.Color.Silver
            TRTotCHeckFooter_05.BorderColor = Drawing.Color.Silver
            TRTotCHeckFooter_06.BorderColor = Drawing.Color.Silver

            TRTotCHeckFooter_00.BorderStyle = BorderStyle.Solid
            TRTotCHeckFooter_01.BorderStyle = BorderStyle.Solid
            TRTotCHeckFooter_02.BorderStyle = BorderStyle.Solid
            TRTotCHeckFooter_03.BorderStyle = BorderStyle.Solid
            TRTotCHeckFooter_04.BorderStyle = BorderStyle.Solid
            TRTotCHeckFooter_05.BorderStyle = BorderStyle.Solid
            TRTotCHeckFooter_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_01, 15, 15, "l", "Total")
            RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_02, 40, 40, "l", "")
            RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_03, 10, 10, "c", "")
            If TotLeakage > 0 Then
                RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_04, 10, 10, "r", CheckTotal.ToString("#,##,###.00"))
            Else
                RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_04, 10, 10, "r", "-")
            End If

            RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_05, 10, 10, "c", "")
            RH.AddColumn(TRTotCHeckFooter, TRTotCHeckFooter_06, 10, 10, "c", "")
            tb.Controls.Add(TRTotCHeckFooter)

            RH.BlankRow(tb, 5)

            TotStaff = AD.GetTotalStaffInvolved(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, EmpCode, CloseFlag, FromDt, ToDt)
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"
            TRFooter_05.BorderWidth = "1"
            TRFooter_06.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver
            TRFooter_05.BorderColor = Drawing.Color.Silver
            TRFooter_06.BorderColor = Drawing.Color.Silver

            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid
            TRFooter_05.BorderStyle = BorderStyle.Solid
            TRFooter_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", I.ToString())
            RH.AddColumn(TRFooter, TRFooter_01, 15, 15, "l", "Grand Total")
            RH.AddColumn(TRFooter, TRFooter_02, 40, 40, "l", "")
            RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "c", "")
            If TotLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "r", TotLeakage.ToString("#,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "r", "-")
            End If

            RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "c", TotStaff.ToString())
            RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "c", "")
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
