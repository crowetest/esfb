﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewConsolidatedAudit
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            AuditDtl = Request.QueryString.Get("AuditID")
            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If

            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim SubHD As String = ""
            RH.Heading(Session("FirmName"), tb, "Consolidated Audit Report", 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")



            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            'TRHead_05.BorderWidth = "1"
            'TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            'TRHead_05.BorderColor = Drawing.Color.Silver
            'TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            'TRHead_05.BorderStyle = BorderStyle.Solid
            'TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "l", "Region")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "c", "Planned")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "c", "Pending")
            RH.AddColumn(TRHead, TRHead_04, 20, 20, "c", "Completed")

            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            Dim TotFraud As Integer = 0
            Dim TotSusLeakage As Integer = 0
            Dim TotGeneral As Integer = 0
            Dim GrandTotal As Integer = 0
            Dim TotLeakage As Double = 0
            DT = AD.GetConsolidatedAudit(Month, year, PostID, CInt(Session("UserID")))
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", "<a href='ViewConsolidatedAuditReportAreaWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &RegionID=" + GF.Encrypt(DR(0).ToString()) + " &AuditID=" + GF.Encrypt(AuditDtl) + "'>" + DR(1) + "</a>")
                If DR(2) = 0 Then
                    RH.AddColumn(TR3, TR3_02, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_02, 10, 10, "c", DR(2).ToString())
                End If
                If DR(3) = 0 Then
                    RH.AddColumn(TR3, TR3_03, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_03, 10, 10, "c", DR(3).ToString())
                End If
                If DR(4) = 0 Then
                    RH.AddColumn(TR3, TR3_04, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_04, 10, 10, "c", DR(4).ToString())
                End If
                If DR(5) = 0 Then
                    RH.AddColumn(TR3, TR3_05, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_05, 10, 10, "c", DR(5).ToString())
                End If
                If DR(6) = 0 Then
                    RH.AddColumn(TR3, TR3_06, 10, 10, "r", "-")
                Else
                    RH.AddColumn(TR3, TR3_06, 10, 10, "r", CDbl(DR(6)).ToString("0.00"))
                End If
                tb.Controls.Add(TR3)
                TotFraud += DR(2)
                TotSusLeakage += DR(3)
                TotGeneral += DR(4)
                GrandTotal += DR(5)
                TotLeakage += DR(6)
            Next

            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"
            TRFooter_05.BorderWidth = "1"
            TRFooter_06.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver
            TRFooter_05.BorderColor = Drawing.Color.Silver
            TRFooter_06.BorderColor = Drawing.Color.Silver

            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid
            TRFooter_05.BorderStyle = BorderStyle.Solid
            TRFooter_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 35, 35, "l", "Total")
            RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", "<a href='ViewConsolidatedAuditReport_tot_Gen.aspx?StatusID=1 &ReportID=" + hdnReportID.Value + " &RegionID=" + GF.Encrypt("-1") + " &AuditID=" + GF.Encrypt(AuditDtl) + " '>" + TotFraud.ToString() + "</a>")
            RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "c", "<a href='ViewConsolidatedAuditReport_tot_Gen.aspx?StatusID=2 &ReportID=" + hdnReportID.Value + " &RegionID=" + GF.Encrypt("-1") + " &AuditID=" + GF.Encrypt(AuditDtl) + " '>" + TotSusLeakage.ToString() + "</a>")
            RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "c", "<a href='ViewConsolidatedAuditReport_tot_Gen.aspx?StatusID=3 &ReportID=" + hdnReportID.Value + " &RegionID=" + GF.Encrypt("-1") + " &AuditID=" + GF.Encrypt(AuditDtl) + " '>" + TotGeneral.ToString() + "</a>")
            RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "c", "<a href='ViewConsolidatedAuditReport_tot_Gen.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &RegionID=" + GF.Encrypt("-1") + " &AuditID=" + GF.Encrypt(AuditDtl) + " '>" + GrandTotal.ToString() + "</a>")
            RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "r", "<a href='ViewConsolidatedAuditReport_tot_Gen.aspx?StatusID=4 &ReportID=" + hdnReportID.Value + " &RegionID=" + GF.Encrypt("-1") + " &AuditID=" + GF.Encrypt(AuditDtl) + " '>" + TotLeakage.ToString("0.00") + "</a>")
            tb.Controls.Add(TRFooter)


            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
