﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewManasgementNote
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim Month As Integer
    Dim Year As Integer
    Dim BranchID As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Month = CInt(Request.QueryString.Get("MonthID"))
            Year = CInt(Request.QueryString.Get("YearID"))

            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            Dim DT As New DataTable
            DT = AD.GetManagementNoteReport(Month, Year, BranchID)
            Dim SubHD As String = "Management Notes "
            If Month > 0 And Year > 0 Then
                SubHD += " Period: " + MonthName(Month) + "-" + Year.ToString()
            End If

            If BranchID > 0 Then
                SubHD += " " + GN.GetBranch_Name(BranchID) + " Branch"
            End If

            RH.Heading(Session("FirmName"), tb, SubHD, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim i As Integer = 0
            For Each DR In DT.Rows

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                TR3.Style.Add("background-color", "LightSteelblue")
                Dim TR3_00 As New TableCell
                TR3_00.BorderWidth = "1"
                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_00.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TR3, TR3_00, 100, 100, "l", DR(1) + "  " + CDate(DR(2)).ToString("MMM-yyyy"))
                tb.Controls.Add(TR3)

                Dim TR4 As New TableRow
                TR4.BorderWidth = "1"
                TR4.BorderStyle = BorderStyle.Solid
                Dim TR4_00 As New TableCell
                TR4_00.BorderWidth = "1"
                TR4_00.BorderColor = Drawing.Color.Silver
                TR4_00.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR4, TR4_00, 100, 100, "l", DR(3).ToString().Replace(vbCr, "").Replace(vbLf, vbCrLf).Replace(Environment.NewLine, "<br />") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='viewAuditFinalReport.aspx?ReportID=" + GN.Encrypt("1") + "&AuditID=" + GN.Encrypt(DR(0)) + "' style='text-align:right;' target='_blank'>View Audit Report</a>")
                tb.Controls.Add(TR4)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
        Return
    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
        Return
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
