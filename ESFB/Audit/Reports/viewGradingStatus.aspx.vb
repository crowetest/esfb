﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewGradingStatus
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim MonthDesc As String
    Dim subHead As String = ""
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            RH.Heading(Session("FirmName"), tb, "BRANCH GRADING", 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 65, 65, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Total Mark")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "Grade")

            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 5)      
            DT = DB.ExecuteDataSet("SELECT A.BRANCH_ID,B.Branch_Name,A.TOTAL_MARKS,C.Grade  FROM AUDIT_BRANCH_GRADE_MASTER A,BRANCH_MASTER B,AUDIT_GRADE_MASTER C WHERE A.BRANCH_ID=B.Branch_ID AND A.GRADE_NO=C.GRADE_NO").Tables(0)

            Dim Total As Integer = 0
            For Each DR In DT.Rows


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid


                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 65, 65, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 15, 15, "c", "<a href='viewBranchGradeStatusDtl_Individual.aspx?BranchID=" + GN.Encrypt(DR(0).ToString()) + "' target='blank'>" + CDbl(DR(2)).ToString("##0.00"))
                RH.AddColumn(TR3, TR3_03, 15, 15, "c", DR(3).ToString())

                tb.Controls.Add(TR3)
                Total += DR(2)
            Next
            Dim TrEnd As New TableRow
            TrEnd.BackColor = Drawing.Color.WhiteSmoke
            Dim TrEnd_00, TrEnd_01, TrEnd_02, TrEnd_03 As New TableCell

            TrEnd_00.BorderWidth = "1"
            TrEnd_01.BorderWidth = "1"
            TrEnd_02.BorderWidth = "1"
            TrEnd_03.BorderWidth = "1"


            TrEnd_00.BorderColor = Drawing.Color.Silver
            TrEnd_01.BorderColor = Drawing.Color.Silver
            TrEnd_02.BorderColor = Drawing.Color.Silver
            TrEnd_03.BorderColor = Drawing.Color.Silver


            TrEnd_00.BorderStyle = BorderStyle.Solid
            TrEnd_01.BorderStyle = BorderStyle.Solid
            TrEnd_02.BorderStyle = BorderStyle.Solid
            TrEnd_03.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TrEnd, TrEnd_00, 5, 5, "c", "")
            RH.AddColumn(TrEnd, TrEnd_01, 65, 65, "l", "AVERAGE")
            RH.AddColumn(TrEnd, TrEnd_02, 15, 15, "c", "<a href='viewBranchGradeStatusDtl.aspx?BranchID=" + GN.Encrypt("-1") + "'>" + (Total / I).ToString("####") + "</a>")
            RH.AddColumn(TrEnd, TrEnd_03, 15, 15, "l", "")
            tb.Controls.Add(TrEnd)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
