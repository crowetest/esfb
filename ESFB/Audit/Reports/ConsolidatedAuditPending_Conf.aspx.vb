﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ConsolidatedAuditPending_Conf
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim BranchID As Integer
    Dim StatusID As Integer
    Dim UserID As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim PostID As Integer
    Dim GroupID As Integer
    Dim AreaID As Integer
    Dim RegionID As Integer
    Dim TerritoryID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            StatusID = CInt(Request.QueryString.Get("StatusID"))
            If Not IsNothing(Request.QueryString.Get("BranchID")) Then
                BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            End If
            If Not IsNothing(Request.QueryString.Get("GroupID")) Then
                GroupID = CInt(GN.Decrypt(Request.QueryString.Get("GroupID")))
            End If
            If Not IsNothing(Request.QueryString.Get("AreaID")) Then
                AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            End If
            If Not IsNothing(Request.QueryString.Get("RegionID")) Then
                RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            End If
            If Not IsNothing(Request.QueryString.Get("ZoneID")) Then
                TerritoryID = CInt(GN.Decrypt(Request.QueryString.Get("ZoneID")))
            End If
            PostID = CInt(Session("Post_ID"))
            UserID = CInt(Session("UserID"))
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim BranchName As String = GN.GetBranch_Name(BranchID)
            Dim subHD As String = ""
            Dim SqlStr As String = ""
            Dim SqlStr_1 As String = ""
            Dim SqlStr_2 As String = ""
            If StatusID = 6 Then
                subHD = "Send for Confirmation " + BranchName + " Branch"
                SqlStr = "select e.department_id,e.department_Name,COUNT(a.sino) as count from audit_observation_dtl a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_CONFIRMATION_DEPARTMENTS  e,AUDIT_DEPARTMENTCONFIRMATION_DTL f,brmaster g where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and f.department_id=e.Department_ID  and d.STATUS_ID=2 and d.branch_id=g.branch_id and a.SINO=f.sino and f.status_id=1 and a.STATUS_ID=6  "
                SqlStr_2 = " group by  e.department_id,e.department_Name"
            End If
            If BranchID > 0 Then
                SqlStr_1 += "  and g.branch_id=" & BranchID.ToString() & ""
            ElseIf AreaID > 0 Then
                SqlStr_1 += "  and g.area_id=" & AreaID.ToString() & ""
            ElseIf RegionID > 0 Then
                SqlStr_1 += "  and g.region_id=" & RegionID.ToString() & ""
            ElseIf TerritoryID > 0 Then
                SqlStr_1 += "  and g.zone_id=" & TerritoryID.ToString() & ""
            End If
            If GroupID > 0 Then
                SqlStr_1 += "  and d.group_id= " + GroupID.ToString() + " "
            End If

            SqlStr = SqlStr + SqlStr_1 + SqlStr_2
            RH.Heading(Session("FirmName"), tb, subHD, 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "#")
            RH.AddColumn(TRHead, TRHead_01, 60, 60, "l", "Department")
            RH.AddColumn(TRHead, TRHead_02, 35, 35, "c", "Count")

            tb.Controls.Add(TRHead)

            Dim i As Integer = 0
            Dim Total As Integer
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows
                i += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 60, 60, "l", DR(1))
                RH.AddColumn(TR3, TR3_02, 35, 35, "c", CInt(DR(2)).ToString("#,##,###"))

                tb.Controls.Add(TR3)
                Total += CInt(DR(2))
            Next
            Dim TR_End As New TableRow
            Dim TR_End_00, TR_End_01, TR_End_02 As New TableCell
            TR_End_00.BorderWidth = "1"
            TR_End_01.BorderWidth = "1"
            TR_End_02.BorderWidth = "1"

            TR_End_00.BorderColor = Drawing.Color.Silver
            TR_End_01.BorderColor = Drawing.Color.Silver
            TR_End_02.BorderColor = Drawing.Color.Silver

            TR_End_00.BorderStyle = BorderStyle.Solid
            TR_End_01.BorderStyle = BorderStyle.Solid
            TR_End_02.BorderStyle = BorderStyle.Solid

            TR_End.Style.Add("Font-Weight", "Bold")
            RH.AddColumn(TR_End, TR_End_00, 5, 5, "l", "")
            RH.AddColumn(TR_End, TR_End_01, 60, 60, "l", "Total")
            RH.AddColumn(TR_End, TR_End_02, 35, 35, "c", Total.ToString("#,##,###"))
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
