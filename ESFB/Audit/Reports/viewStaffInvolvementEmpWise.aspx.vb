﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementEmpWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim AD As New Audit
    Dim BranchID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim ItemID As Integer
    Dim SINO As Integer
    Dim CloseFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim StrSubHead As String
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            ItemID = CInt(Request.QueryString.Get("ItemID"))
            SINO = CInt(Request.QueryString.Get("SINO"))
            CloseFlag = CInt(Request.QueryString.Get("CloseFlag"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim SubHD As String = "Report Parameters:"
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
                SubHD += " Period " + MonthName(Month) + "-" + year.ToString()
            End If

            If BranchID > 0 Then
                SubHD += " Branch - " + GN.GetBranch_Name(BranchID)
            ElseIf AreaID > 0 Then
                SubHD += " Area - " + GN.GetArea_Name(AreaID)
            ElseIf RegionID > 0 Then
                SubHD += " Region - " + GN.GetRegion_Name(RegionID)
            Else
                SubHD += " Consolidated"
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If
            If ItemID > 0 Then
                Dim DTItem As New DataTable
                DTItem = AD.GetItemName(ItemID, 0)
                SubHD += " Check List - " + DTItem.Rows(0)(0).ToString()
            End If

            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable

            RH.Heading(Session("FirmName"), tb, "Staff Involvement Report " + Status, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            RH.SubHeading(tb, 100, "l", SubHD)
            StrSubHead = "Staff Involvement Report " + Status + "(" + SubHD + ")"
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim BranchName As String = ""
            Dim ItemName As String = ""

            'RH.BlankRow(tb, 3)
            Dim TotObservation As Integer = 0
            Dim TotStaff As Integer = 0
            Dim TotLeakage As Integer = 0
            Dim TotFraudObservation As Integer = 0
            Dim TotSeriousObservation As Integer = 0
            Dim TotGeneralObservation As Integer = 0
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Last Updated on")
            RH.AddColumn(TRHead, TRHead_02, 5, 5, "l", "Emp Code")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Name")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Region")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Area")
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Designation")
            RH.AddColumn(TRHead, TRHead_08, 5, 5, "r", "Est. Amt. Involved")
            RH.AddColumn(TRHead, TRHead_09, 5, 5, "c", "No of Observations")
            RH.AddColumn(TRHead, TRHead_10, 3, 3, "c", "V.Serious")
            RH.AddColumn(TRHead, TRHead_11, 3, 3, "c", "Serious")
            RH.AddColumn(TRHead, TRHead_12, 3, 3, "c", "General")
            RH.AddColumn(TRHead, TRHead_13, 3, 3, "c", "Audits")
            RH.AddColumn(TRHead, TRHead_14, 8, 8, "c", "Audit Periods")

            tb.Controls.Add(TRHead)
            DT = AD.GetStaffInvolvementStaff(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 10, 10, "l", CDate(DR(10)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(0))
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_04, 10, 10, "l", DR(14).ToString())
                RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(13).ToString())

                RH.AddColumn(TR3, TR3_06, 10, 10, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_07, 10, 10, "l", DR(3).ToString())
                If DR(5) = 0 Then
                    RH.AddColumn(TR3, TR3_08, 5, 5, "r", "-")
                Else
                    RH.AddColumn(TR3, TR3_08, 5, 5, "r", CDbl(DR(5)).ToString("#,##,###.00"))
                End If
                If DR(4) = 0 Then
                    RH.AddColumn(TR3, TR3_09, 5, 5, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_09, 5, 5, "c", "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=" + ItemID.ToString() + " &BranchID=" + GN.Encrypt(DR(11)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &EmpCode=" + DR(0).ToString() + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + " &TypeID=-1 &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='blank'>" + DR(4).ToString() + "</a>")
                End If
                If DR(7) = 0 Then
                    RH.AddColumn(TR3, TR3_10, 3, 3, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_10, 3, 3, "c", "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=" + ItemID.ToString() + " &BranchID=" + GN.Encrypt(DR(11)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &EmpCode=" + DR(0).ToString() + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + " &TypeID=1 &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='blank'>" + DR(7).ToString() + "</a>")
                End If
                If DR(8) = 0 Then
                    RH.AddColumn(TR3, TR3_11, 3, 3, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_11, 3, 3, "c", "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=" + ItemID.ToString() + " &BranchID=" + GN.Encrypt(DR(11)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &EmpCode=" + DR(0).ToString() + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + " &TypeID=2 &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='blank'>" + DR(8).ToString() + "</a>")
                End If
                If DR(9) = 0 Then
                    RH.AddColumn(TR3, TR3_12, 3, 3, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_12, 3, 3, "c", "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=" + ItemID.ToString() + " &BranchID=" + GN.Encrypt(DR(11)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &EmpCode=" + DR(0).ToString() + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + " &TypeID=3 &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='blank'>" + DR(9).ToString() + "</a>")
                End If
                RH.AddColumn(TR3, TR3_13, 3, 3, "c", "<a href='viewStaffInvolvementAudits.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=" + ItemID.ToString() + " &BranchID=" + GN.Encrypt(DR(11)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &EmpCode=" + DR(0).ToString() + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + " &TypeID=3 &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='blank'>" + DR(6).ToString() + "</a>")
                RH.AddColumn(TR3, TR3_14, 8, 8, "c", DR(12).ToString())
                tb.Controls.Add(TR3)
                I += 1
            Next
            TotObservation = AD.GetTotalStaffInvolvedObservations(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            TotFraudObservation = AD.GetTotalStaffInvolvedObservations_Fraud(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            TotSeriousObservation = AD.GetTotalStaffInvolvedObservations_Serious(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            TotGeneralObservation = AD.GetTotalStaffInvolvedObservations_General(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            TotLeakage = AD.GetTotalLeakageStaffInvolvedObservations(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06, TRFooter_07, TRFooter_08, TRFooter_09, TRFooter_10, TRFooter_11, TRFooter_12, TRFooter_13, TRFooter_14 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"
            TRFooter_05.BorderWidth = "1"
            TRFooter_06.BorderWidth = "1"
            TRFooter_07.BorderWidth = "1"
            TRFooter_08.BorderWidth = "1"
            TRFooter_09.BorderWidth = "1"
            TRFooter_10.BorderWidth = "1"
            TRFooter_11.BorderWidth = "1"
            TRFooter_12.BorderWidth = "1"
            TRFooter_13.BorderWidth = "1"
            TRFooter_14.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver
            TRFooter_05.BorderColor = Drawing.Color.Silver
            TRFooter_06.BorderColor = Drawing.Color.Silver
            TRFooter_07.BorderColor = Drawing.Color.Silver
            TRFooter_08.BorderColor = Drawing.Color.Silver
            TRFooter_09.BorderColor = Drawing.Color.Silver
            TRFooter_10.BorderColor = Drawing.Color.Silver
            TRFooter_11.BorderColor = Drawing.Color.Silver
            TRFooter_12.BorderColor = Drawing.Color.Silver
            TRFooter_13.BorderColor = Drawing.Color.Silver
            TRFooter_14.BorderColor = Drawing.Color.Silver

            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid
            TRFooter_05.BorderStyle = BorderStyle.Solid
            TRFooter_06.BorderStyle = BorderStyle.Solid
            TRFooter_07.BorderStyle = BorderStyle.Solid
            TRFooter_08.BorderStyle = BorderStyle.Solid
            TRFooter_09.BorderStyle = BorderStyle.Solid
            TRFooter_10.BorderStyle = BorderStyle.Solid
            TRFooter_11.BorderStyle = BorderStyle.Solid
            TRFooter_12.BorderStyle = BorderStyle.Solid
            TRFooter_13.BorderStyle = BorderStyle.Solid
            TRFooter_14.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", I.ToString())
            RH.AddColumn(TRFooter, TRFooter_01, 10, 10, "c", "Total")
            RH.AddColumn(TRFooter, TRFooter_02, 5, 5, "l", "")
            RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "l", "") '
            RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "l", "")
            RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "l", "")
            RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "c", "")
            RH.AddColumn(TRFooter, TRFooter_07, 10, 10, "c", "")
            If TotLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_08, 5, 5, "r", TotLeakage.ToString("#,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_08, 5, 5, "r", "")
            End If
            RH.AddColumn(TRFooter, TRFooter_09, 5, 5, "c", TotObservation.ToString())
            RH.AddColumn(TRFooter, TRFooter_10, 3, 3, "c", TotFraudObservation.ToString())
            RH.AddColumn(TRFooter, TRFooter_11, 3, 3, "c", TotSeriousObservation.ToString())
            RH.AddColumn(TRFooter, TRFooter_12, 3, 3, "c", TotGeneralObservation.ToString())
            RH.AddColumn(TRFooter, TRFooter_13, 3, 3, "r", "")
            RH.AddColumn(TRFooter, TRFooter_14, 8, 8, "r", "")
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            WebTools.ExporttoExcel(DT, StrSubHead)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
