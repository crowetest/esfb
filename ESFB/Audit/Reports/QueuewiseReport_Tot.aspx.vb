﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class QueuewiseReport_Tot
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim LevelID As Integer
    Dim StatusID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim UserID As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            StatusID = CInt(Request.QueryString.Get("StatusID"))

            LevelID = CInt(GN.Decrypt(Request.QueryString.Get("LevelID")))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim LevelName As String = AD.GetLevelName(LevelID)
            Dim subHD As String = ""
            Dim SqlStr As String = ""
            Dim SqlStrSub As String = ""
            Dim SqlStrSub_1 As String = ""
            PostID = CInt(Session("Post_ID"))
            UserID = CInt(Session("UserID"))
            If (PostID = 5) Then
                SqlStrSub = " and l.Branch_Head=" + UserID.ToString()
            ElseIf PostID = 6 Then
                SqlStrSub = " and l.area_Head=" + UserID.ToString()
            ElseIf PostID = 7 Then
                SqlStrSub = " and l.region_Head=" + UserID.ToString()
            ElseIf PostID = 8 Then
                SqlStrSub = " and l.zone_Head=" + UserID.ToString()
            ElseIf PostID = 3 Then
                SqlStrSub = " and l.branch_id in(select branch_ID from audit_Branches where team_lead=" + UserID.ToString() + ")"
            ElseIf PostID = 4 Then
                SqlStrSub = " and l.branch_id in(select branch_ID from audit_Branches where emp_code=" + UserID.ToString() + ")"
            End If

            If StatusID = 1 Then
                subHD = "Original Response Pending - " + LevelName + " Level "
                SqlStr = "select a.sino,a.loan_no,isnull(d.end_dt,''),i.remarks,isnull((case when a.status_id=1 then (case when (j.cumilative_days-a.operation_days)<0 then 0 else (j.cumilative_days-a.operation_days) end ) else 0 end),0) as PendingDays,g.level_name,a.fin_leakage,a.fraud,a.susp_leakage,k.level_name,l.Branch_Name,f.ITEM_NAME,a.remarks  from audit_observation_dtl a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_CHECK_LIST f,audit_user_levels g,audit_observation_cycle i,audit_escalation_matrix_dtl j,audit_user_levels k,BrMaster l where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and b.item_id=f.item_id and a.level_id=g.level_id  and a.order_id=i.order_id and a.level_upto=j.level_ID and a.level_Upto=k.level_id and a.matrix_id=j.matrix_id and d.STATUS_ID=2 and a.STATUS_ID=1 and a.LEVEL_ID=" + LevelID.ToString() + " and a.Response_Flag=0 and a.ROLE_ID=1 and d.branch_id=l.branch_id "
            ElseIf StatusID = 2 Then
                subHD = "Further Response Pending - " + LevelName + " Level "
                SqlStr = "select a.sino,a.loan_no,isnull(d.end_dt,''),i.remarks,isnull((case when a.status_id=1 then (case when (j.cumilative_days-a.operation_days)<0 then 0 else (j.cumilative_days-a.operation_days) end ) else 0 end),0) as PendingDays,g.level_name,a.fin_leakage,a.fraud,a.susp_leakage,k.level_name,l.Branch_Name,f.ITEM_NAME,a.remarks  from audit_observation_dtl a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_CHECK_LIST f,audit_user_levels g,audit_observation_cycle i,audit_escalation_matrix_dtl j,audit_user_levels k,BrMaster l where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and b.item_id=f.item_id and a.level_id=g.level_id  and a.order_id=i.order_id and a.level_upto=j.level_ID and a.level_Upto=k.level_id and a.matrix_id=j.matrix_id and d.STATUS_ID=2 and a.STATUS_ID=1 and a.LEVEL_ID=" + LevelID.ToString() + " and a.Response_Flag>0 and a.ROLE_ID=1 and d.branch_id=l.branch_id "
            ElseIf StatusID = 3 Then
                subHD = "Response Authorization Pending - " + LevelName + " Level "
                SqlStr = "select a.sino,a.loan_no,isnull(d.end_dt,''),i.remarks,isnull((case when a.status_id=1 then (case when (j.cumilative_days-a.operation_days)<0 then 0 else (j.cumilative_days-a.operation_days) end ) else 0 end),0) as PendingDays,g.level_name,a.fin_leakage,a.fraud,a.susp_leakage,k.level_name,l.Branch_Name,f.ITEM_NAME,a.remarks  from audit_observation_dtl a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_CHECK_LIST f,audit_user_levels g,audit_observation_cycle i,audit_escalation_matrix_dtl j,audit_user_levels k,BrMaster l where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and b.item_id=f.item_id and a.level_id=g.level_id  and a.order_id=i.order_id and a.level_upto=j.level_ID and a.level_Upto=k.level_id and a.matrix_id=j.matrix_id and d.STATUS_ID=2 and a.STATUS_ID=1 and a.LEVEL_ID=" + LevelID.ToString() + " and a.ROLE_ID=4 and d.branch_id=l.branch_id "
            ElseIf StatusID = 4 Then
                subHD = "Response Closure Pending - " + LevelName + " Level "
                SqlStr = "select a.sino,a.loan_no,isnull(d.end_dt,''),i.remarks,isnull((case when a.status_id=1 then (case when (j.cumilative_days-a.operation_days)<0 then 0 else (j.cumilative_days-a.operation_days) end ) else 0 end),0) as PendingDays,g.level_name,a.fin_leakage,a.fraud,a.susp_leakage,k.level_name,l.Branch_Name,f.ITEM_NAME,a.remarks  from audit_observation_dtl a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_CHECK_LIST f,audit_user_levels g,audit_observation_cycle i,audit_escalation_matrix_dtl j,audit_user_levels k,BrMaster l where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and b.item_id=f.item_id and a.level_id=g.level_id  and a.order_id=i.order_id and a.level_upto=j.level_ID and a.level_Upto=k.level_id and a.matrix_id=j.matrix_id and d.STATUS_ID=2 and a.STATUS_ID=5 and a.LEVEL_ID=" + LevelID.ToString() + "  and d.branch_id=l.branch_id "
            ElseIf StatusID = 5 Then
                subHD = "Response Authorization Escalated to - " + LevelName + " Level "
                SqlStr = "select a.sino,a.loan_no,isnull(d.end_dt,''),i.remarks,isnull((case when a.status_id=1 then (case when (j.cumilative_days-a.operation_days)<0 then 0 else (j.cumilative_days-a.operation_days) end ) else 0 end),0) as PendingDays,g.level_name,a.fin_leakage,a.fraud,a.susp_leakage,k.level_name,l.Branch_Name,f.ITEM_NAME,a.remarks  from audit_observation_dtl a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,AUDIT_CHECK_LIST f,audit_user_levels g,audit_observation_cycle i,audit_escalation_matrix_dtl j,audit_user_levels k,BrMaster l where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and b.item_id=f.item_id and a.level_id=g.level_id  and a.order_id=i.order_id and a.level_upto=j.level_ID and a.level_Upto=k.level_id and a.matrix_id=j.matrix_id and d.STATUS_ID=2 and a.STATUS_ID=1 and " & LevelID.ToString() & ">a.Level_id and " & LevelID.ToString() & "<=a.Level_Upto  and a.level_id<a.level_upto  and d.branch_id=l.branch_id "
            End If

            If BranchID > 0 Then
                SqlStrSub_1 = " and l.branch_id=" + BranchID.ToString()
            ElseIf AreaID > 0 Then
                SqlStrSub_1 = " and l.area_id=" + AreaID.ToString()
            ElseIf RegionID > 0 Then
                SqlStrSub_1 = " and l.region_id=" + RegionID.ToString()
            End If

            SqlStr = SqlStr + SqlStrSub_1 + SqlStrSub + " order by l.Branch_Name,f.ITEM_NAME ,a.sino"
            RH.Heading(Session("FirmName"), tb, subHD, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            'RH.BlankRow(tb, 3)
            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim Total As Double

            Dim BranchName As String = ""
            Dim ItemName As String = ""
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows

                i += 1

                If BranchName <> DR(10) Then
                    Dim trBranch As New TableRow
                    Dim trBranch_00 As New TableCell
                    trBranch.Style.Add("background-color", "LightSteelblue")
                    RH.AddColumn(trBranch, trBranch_00, 100, 100, "c", DR(10))
                    tb.Controls.Add(trBranch)
                    j = 0
                End If

                If ItemName <> DR(11) Then
                    RH.BlankRow(tb, 5)
                    Dim trItem As New TableRow
                    Dim trItem_00 As New TableCell
                    trItem.Style.Add("background-color", "lightsilver")
                    RH.AddColumn(trItem, trItem_00, 100, 100, "l", "<strong><i>Check List : " + DR(11) + "</i></strong>")
                    tb.Controls.Add(trItem)
                    j = 0
                    RH.BlankRow(tb, 5)
                    Dim TRHead_0 As New TableRow
                    TRHead_0.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_0_00, TRHead_0_01, TRHead_0_02, TRHead_0_03, TRHead_0_04, TRHead_0_05, TRHead_0_06, TRHead_0_07 As New TableCell

                    TRHead_0_00.BorderWidth = "1"
                    TRHead_0_01.BorderWidth = "1"
                    TRHead_0_02.BorderWidth = "1"
                    TRHead_0_03.BorderWidth = "1"
                    TRHead_0_04.BorderWidth = "1"
                    TRHead_0_05.BorderWidth = "1"
                    TRHead_0_06.BorderWidth = "1"
                    TRHead_0_07.BorderWidth = "1"

                    TRHead_0_00.BorderColor = Drawing.Color.Silver
                    TRHead_0_01.BorderColor = Drawing.Color.Silver
                    TRHead_0_02.BorderColor = Drawing.Color.Silver
                    TRHead_0_03.BorderColor = Drawing.Color.Silver
                    TRHead_0_04.BorderColor = Drawing.Color.Silver
                    TRHead_0_05.BorderColor = Drawing.Color.Silver
                    TRHead_0_06.BorderColor = Drawing.Color.Silver
                    TRHead_0_07.BorderColor = Drawing.Color.Silver

                    TRHead_0_00.BorderStyle = BorderStyle.Solid
                    TRHead_0_01.BorderStyle = BorderStyle.Solid
                    TRHead_0_02.BorderStyle = BorderStyle.Solid
                    TRHead_0_03.BorderStyle = BorderStyle.Solid
                    TRHead_0_04.BorderStyle = BorderStyle.Solid
                    TRHead_0_05.BorderStyle = BorderStyle.Solid
                    TRHead_0_06.BorderColor = Drawing.Color.Silver
                    TRHead_0_07.BorderColor = Drawing.Color.Silver

                    RH.AddColumn(TRHead_0, TRHead_0_00, 5, 5, "l", "#")
                    RH.AddColumn(TRHead_0, TRHead_0_01, 10, 10, "l", "Loan_no")
                    RH.AddColumn(TRHead_0, TRHead_0_02, 10, 10, "l", "Audit End On")
                    RH.AddColumn(TRHead_0, TRHead_0_03, 25, 25, "c", "Current Level ")
                    RH.AddColumn(TRHead_0, TRHead_0_04, 20, 20, "c", "Audit Observation")
                    RH.AddColumn(TRHead_0, TRHead_0_05, 20, 20, "c", "Last Remarks")
                    RH.AddColumn(TRHead_0, TRHead_0_06, 5, 5, "c", "Type")
                    RH.AddColumn(TRHead_0, TRHead_0_07, 5, 5, "c", "Est.Amt. Involved")


                    tb.Controls.Add(TRHead_0)


                    Dim TRHead As New TableRow
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09 As New TableCell

                    TRHead_00.BorderWidth = "1"
                    TRHead_01.BorderWidth = "1"
                    TRHead_02.BorderWidth = "1"
                    TRHead_03.BorderWidth = "1"
                    TRHead_04.BorderWidth = "1"
                    TRHead_05.BorderWidth = "1"
                    TRHead_06.BorderWidth = "1"
                    TRHead_07.BorderWidth = "1"
                    TRHead_08.BorderWidth = "1"
                    TRHead_09.BorderWidth = "1"

                    TRHead_00.BorderColor = Drawing.Color.Silver
                    TRHead_01.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_03.BorderColor = Drawing.Color.Silver
                    TRHead_04.BorderColor = Drawing.Color.Silver
                    TRHead_05.BorderColor = Drawing.Color.Silver
                    TRHead_06.BorderColor = Drawing.Color.Silver
                    TRHead_07.BorderColor = Drawing.Color.Silver
                    TRHead_08.BorderColor = Drawing.Color.Silver
                    TRHead_09.BorderColor = Drawing.Color.Silver

                    TRHead_00.BorderStyle = BorderStyle.Solid
                    TRHead_01.BorderStyle = BorderStyle.Solid
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    TRHead_03.BorderStyle = BorderStyle.Solid
                    TRHead_04.BorderStyle = BorderStyle.Solid
                    TRHead_05.BorderStyle = BorderStyle.Solid
                    TRHead_06.BorderStyle = BorderStyle.Solid
                    TRHead_07.BorderStyle = BorderStyle.Solid
                    TRHead_08.BorderStyle = BorderStyle.Solid
                    TRHead_09.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "")
                    RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "")
                    RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "")
                    RH.AddColumn(TRHead, TRHead_03, 8, 8, "c", "Response")
                    RH.AddColumn(TRHead, TRHead_04, 8, 8, "c", "Approve")
                    RH.AddColumn(TRHead, TRHead_05, 9, 9, "c", "Days to furth. esc.")
                    RH.AddColumn(TRHead, TRHead_06, 20, 20, "l", "")
                    RH.AddColumn(TRHead, TRHead_07, 20, 20, "l", "")
                    RH.AddColumn(TRHead, TRHead_08, 5, 5, "c", "")
                    RH.AddColumn(TRHead, TRHead_09, 5, 5, "c", "")
                    tb.Controls.Add(TRHead)
                End If
                j += 1
                BranchName = DR(10)
                ItemName = DR(11)
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid

                Dim Type As String = ""
                If (CInt(DR(7)) > 0) Then
                    Type = "VS"
                    '  TR3.Style.Add("background-color", "#EBCCD6")
                ElseIf (CInt(DR(8)) > 0) Then
                    Type = "S"
                    '  TR3.Style.Add("background-color", "LightSteelblue")
                End If

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                If (DR(1) <> "") Then
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", "<a href='viewLoanDetails.aspx?LoanNo=" + GN.Encrypt(DR(1).ToString()) + "' style='text-align:right;' target='_blank'>" + DR(1) + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", "")
                End If

                RH.AddColumn(TR3, TR3_02, 10, 10, "l", CDate(DR(2)).ToString("dd/MM/yyyy"))
                RH.AddColumn(TR3, TR3_03, 8, 8, "c", DR(5).ToString())
                RH.AddColumn(TR3, TR3_04, 8, 8, "c", DR(9).ToString())
                If (CInt(DR(4)) < 0) Then
                    TR3_05.Style.Add("Color", "Red")
                Else
                    TR3_05.Style.Add("Color", "black")
                End If
                If DR(9) = "CMD" Then
                    RH.AddColumn(TR3, TR3_05, 9, 9, "c", "")
                Else
                    RH.AddColumn(TR3, TR3_05, 9, 9, "c", DR(4))
                End If
                RH.AddColumn(TR3, TR3_06, 20, 20, "l", DR(12).ToString())
                RH.AddColumn(TR3, TR3_07, 20, 20, "l", DR(3).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank' >Previous</a>")

                RH.AddColumn(TR3, TR3_08, 5, 5, "c", Type)

                If (CDbl(DR(6)) > 0) Then
                    RH.AddColumn(TR3, TR3_09, 5, 5, "r", CDbl(DR(6)).ToString("#,##,###.00"))
                Else
                    RH.AddColumn(TR3, TR3_09, 5, 5, "r", "-")
                End If



                tb.Controls.Add(TR3)
                Total += CDbl(DR(6))

            Next

            Dim TR_End As New TableRow
            Dim TR_End_00, TR_End_01, TR_End_02 As New TableCell
            TR_End_00.BorderWidth = "1"
            TR_End_01.BorderWidth = "1"
            TR_End_02.BorderWidth = "1"


            TR_End_00.BorderColor = Drawing.Color.Silver
            TR_End_01.BorderColor = Drawing.Color.Silver
            TR_End_02.BorderColor = Drawing.Color.Silver



            TR_End_00.BorderStyle = BorderStyle.Solid
            TR_End_01.BorderStyle = BorderStyle.Solid
            TR_End_02.BorderStyle = BorderStyle.Solid


            TR_End.Style.Add("Font-Weight", "Bold")
            RH.AddColumn(TR_End, TR_End_00, 5, 5, "c", i.ToString())
            RH.AddColumn(TR_End, TR_End_01, 85, 85, "c", "Total")
            If Total > 0 Then
                RH.AddColumn(TR_End, TR_End_02, 15, 15, "r", Total.ToString("#,##,###.00"))
            Else
                RH.AddColumn(TR_End, TR_End_02, 15, 15, "r", "-")
            End If

            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
