﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewBranchGradeStatusDtl_Individual
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim BranchID As Integer
    Dim AuditTypeID As Integer
    Dim From As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            Dim BranchName As String = GN.GetBranch_Name(BranchID)
            RH.Heading(Session("FirmName"), tb, "GRADING REPORT - " + BranchName.ToUpper() + " BRANCH", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = DB.ExecuteDataSet("SELECT A.GRADING_PERIOD_FROM,A.GRADING_PERIOD_TO,A.Total_Marks,B.Grade  FROM AUDIT_BRANCH_GRADE_MASTER A,Audit_Grade_Master B WHERE A.Grade_No=B.Grade_No AND A.Branch_ID=" + BranchID.ToString()).Tables(0)
            If DT.Rows.Count > 0 Then
                Dim TR00 As New TableRow
                TR00.BackColor = Drawing.Color.LightSlateGray
                TR00.ForeColor = Drawing.Color.White
                TR00.Height = "25"
                TR00.Font.Bold = True
                Dim TR00_01 As New TableCell
                RH.AddColumn(TR00, TR00_01, 100, 100, "c", "SUMMARY")
                tb.Controls.Add(TR00)
                Dim TR0 As New TableRow
                'TR0.BackColor = Drawing.Color.LightSlateGray
                'TR0.ForeColor = Drawing.Color.White
                TR0.Font.Bold = True
                TR0.Height = "25"
                Dim TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07 As New TableCell
                RH.AddColumn(TR0, TR0_01, 20, 20, "l", "Grading Period From")
                RH.AddColumn(TR0, TR0_02, 2, 2, "l", " : ")
                RH.AddColumn(TR0, TR0_03, 25, 25, "l", CDate(DT.Rows(0)(0)).ToString("dd MMM yyyy"))
                RH.AddColumn(TR0, TR0_04, 6, 6, "l", "")
                RH.AddColumn(TR0, TR0_05, 20, 20, "l", "Grading Period To")
                RH.AddColumn(TR0, TR0_06, 2, 2, "l", " : ")
                RH.AddColumn(TR0, TR0_07, 25, 25, "l", CDate(DT.Rows(0)(1)).ToString("dd MMM yyyy"))

                tb.Controls.Add(TR0)

                Dim TR1 As New TableRow
                'TR1.BackColor = Drawing.Color.LightSlateGray
                'TR1.ForeColor = Drawing.Color.White
                TR1.Height = "25"
                TR1.Font.Bold = True
                Dim TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07 As New TableCell
                RH.AddColumn(TR1, TR1_01, 20, 20, "l", "Total Marks")
                RH.AddColumn(TR1, TR1_02, 2, 2, "l", " : ")
                RH.AddColumn(TR1, TR1_03, 25, 25, "l", DT.Rows(0)(2).ToString())
                RH.AddColumn(TR1, TR1_04, 6, 6, "l", "")
                RH.AddColumn(TR1, TR1_05, 20, 20, "l", "Grade")
                RH.AddColumn(TR1, TR1_06, 2, 2, "l", " : ")
                RH.AddColumn(TR1, TR1_07, 25, 25, "l", DT.Rows(0)(3).ToString())

                tb.Controls.Add(TR1)
                ' RH.DrawLine(tb, 100)
                RH.BlankRow(tb, 5)
                Dim TR000 As New TableRow
                TR000.BackColor = Drawing.Color.LightSlateGray
                TR000.ForeColor = Drawing.Color.White
                TR000.Height = "25"
                TR000.Font.Bold = True
                Dim TR000_01 As New TableCell
                RH.AddColumn(TR000, TR000_01, 100, 100, "c", "DETAILS")
                tb.Controls.Add(TR000)
                RH.BlankRow(tb, 3)
                DT = DB.ExecuteDataSet("select b.Specification_ID,c.GRADING_FACTOR,(case when c.PERCENTAGE_LIMIT >0 then  c.DESCRIPTION_perc else c.description_number end)as Descr,(case when c.PERCENTAGE_LIMIT >0 then  'Percent.' else 'Number' end)as TypeDescr,case when c.PERC_CRITERIA in(1,4) then total_observations else total_sampling end as samplling,b.NoOf_Observations,(case when c.PERCENTAGE_LIMIT >0 then b.Specification_Percentage else b.noof_observations end),(case when c.PERCENTAGE_LIMIT >0 then c.PERCENTAGE_LIMIT else c.number_Limit end)as Limit,c.MARKS_AWARDED ,b.Marks ,d.Category_Description ,d.Category_ID   from AUDIT_BRANCH_GRADE_MASTER a, AUDIT_BRANCH_GRADE_DTL b,AUDIT_GRADING_SPECIFICATIONS c,AUDIT_GRADING_CATEGORY d where a.Grade_ID=b.Grade_ID and b.Specification_ID=c.SPECIFICATION_ID and c.CATEGORY_ID=d.Category_ID  and a.Branch_ID=" + BranchID.ToString() + " order by d.Category_ID,c.specification_id ").Tables(0)
                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.LightGray
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell
                TRHead_00.BorderWidth = "1"
                TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_00.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "SI No")
                TRHead_01.BorderWidth = "1"
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Grading Factor")
                TRHead_02.BorderWidth = "1"
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_02, 34, 34, "l", "Description")
                TRHead_03.BorderWidth = "1"
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_03, 5, 5, "c", "Criteria Used")
                TRHead_04.BorderWidth = "1"
                TRHead_04.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_04, 6, 6, "c", "Sample")
                TRHead_05.BorderWidth = "1"
                TRHead_05.BorderColor = Drawing.Color.Silver
                TRHead_05.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_05, 6, 6, "c", "Observation")
                TRHead_06.BorderWidth = "1"
                TRHead_06.BorderColor = Drawing.Color.Silver
                TRHead_06.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_06, 12, 12, "c", "% /Number")
                TRHead_07.BorderWidth = "1"
                TRHead_07.BorderColor = Drawing.Color.Silver
                TRHead_07.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_07, 12, 12, "c", "Marks")
                tb.Controls.Add(TRHead)

                Dim TRHead1 As New TableRow
                TRHead1.BackColor = Drawing.Color.LightGray
                Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09 As New TableCell
                TRHead1_00.BorderWidth = "1"
                TRHead1_00.BorderColor = Drawing.Color.Silver
                TRHead1_00.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_00, 5, 5, "c", "")
                TRHead1_01.BorderWidth = "1"
                TRHead1_01.BorderColor = Drawing.Color.Silver
                TRHead1_01.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_01, 15, 15, "l", "")
                TRHead1_02.BorderWidth = "1"
                TRHead1_02.BorderColor = Drawing.Color.Silver
                TRHead1_02.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_02, 34, 34, "l", "")
                TRHead1_03.BorderWidth = "1"
                TRHead1_03.BorderColor = Drawing.Color.Silver
                TRHead1_03.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_03, 5, 5, "l", "")
                TRHead1_04.BorderWidth = "1"
                TRHead1_04.BorderColor = Drawing.Color.Silver
                TRHead1_04.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_04, 6, 6, "l", "")
                TRHead1_05.BorderWidth = "1"
                TRHead1_05.BorderColor = Drawing.Color.Silver
                TRHead1_05.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_05, 6, 6, "l", "")
                TRHead1_06.BorderWidth = "1"
                TRHead1_06.BorderColor = Drawing.Color.Silver
                TRHead1_06.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_06, 6, 6, "c", "Actual")
                TRHead1_07.BorderWidth = "1"
                TRHead1_07.BorderColor = Drawing.Color.Silver
                TRHead1_07.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_07, 6, 6, "c", "Max")
                TRHead1_08.BorderWidth = "1"
                TRHead1_08.BorderColor = Drawing.Color.Silver
                TRHead1_08.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_08, 6, 6, "c", "Max")
                TRHead1_09.BorderWidth = "1"
                TRHead1_09.BorderColor = Drawing.Color.Silver
                TRHead1_09.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead1, TRHead1_09, 6, 6, "c", "Scored")
                tb.Controls.Add(TRHead1)
                Dim I As Integer = 0
                Dim TotalMark As Double = 0
                Dim TotalMarks_Scored As Double = 0
                Dim TotalMarks_deducted As Double = 0
                Dim CategoryID As Integer = 0
                Dim TotalFlag As Integer = 0
                For Each DR In DT.Rows
                    I += 1
                    If DR(11) = 5 And TotalFlag = 0 Then
                        TotalFlag = 1
                        Dim TRSubHD_Tot As New TableRow
                        TRSubHD_Tot.BackColor = Drawing.Color.LightGray
                        ' TRSubHD_Tot.ForeColor = Drawing.Color.White
                        TRSubHD_Tot.Font.Bold = True
                        TRSubHD_Tot.Height = "25"
                        Dim TRSubHD_Tot_00, TRSubHD_Tot_01, TRSubHD_Tot_02, TRSubHD_Tot_03, TRSubHD_Tot_04, TRSubHD_Tot_05, TRSubHD_Tot_06, TRSubHD_Tot_07, TRSubHD_Tot_08, TRSubHD_Tot_09 As New TableCell
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_00, 5, 5, "l", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_01, 15, 15, "l", "Total")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_02, 34, 34, "l", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_03, 5, 5, "l", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_04, 6, 6, "c", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_05, 6, 6, "c", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_06, 6, 6, "c", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_07, 6, 6, "c", "")
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_08, 6, 6, "c", TotalMark)
                        RH.AddColumn(TRSubHD_Tot, TRSubHD_Tot_09, 6, 6, "c", TotalMarks_Scored.ToString("##0.00"))
                        tb.Controls.Add(TRSubHD_Tot)
                        RH.BlankRow(tb, 5)
                    End If
                    If CategoryID <> DR(11) Then
                        Dim TRSubHD As New TableRow
                        TRSubHD.BackColor = Drawing.Color.Silver
                        TRSubHD.Font.Bold = True
                        Dim TRSubHD_00 As New TableCell
                        RH.AddColumn(TRSubHD, TRSubHD_00, 100, 100, "l", DR(10))
                        tb.Controls.Add(TRSubHD)
                    End If
                    CategoryID = CInt(DR(11))


                    Dim TR01 As New TableRow
                    Dim TR01_00, TR01_01, TR01_02, TR01_03, TR01_04, TR01_05, TR01_06, TR01_07, TR01_08, TR01_09 As New TableCell

                    'TR01.BackColor = Drawing.Color.WhiteSmoke
                    TR01_00.BorderWidth = "1"
                    TR01_00.BorderColor = Drawing.Color.Silver
                    TR01_00.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_00, 5, 5, "c", I)
                    TR01_01.BorderWidth = "1"
                    TR01_01.BorderColor = Drawing.Color.Silver
                    TR01_01.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_01, 15, 15, "l", DR(1))
                    TR01_02.BorderWidth = "1"
                    TR01_02.BorderColor = Drawing.Color.Silver
                    TR01_02.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_02, 34, 34, "l", DR(2))
                    TR01_03.BorderWidth = "1"
                    TR01_03.BorderColor = Drawing.Color.Silver
                    TR01_03.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_03, 5, 5, "c", DR(3))
                    TR01_04.BorderWidth = "1"
                    TR01_04.BorderColor = Drawing.Color.Silver
                    TR01_04.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_04, 6, 6, "c", DR(4))
                    TR01_05.BorderWidth = "1"
                    TR01_05.BorderColor = Drawing.Color.Silver
                    TR01_05.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_05, 6, 6, "c", DR(5))
                    TR01_06.BorderWidth = "1"
                    TR01_06.BorderColor = Drawing.Color.Silver
                    TR01_06.BorderStyle = BorderStyle.Solid
                    If DR(6) > 0 Then
                        RH.AddColumn(TR01, TR01_06, 6, 6, "c", CDbl(DR(6)).ToString("###"))
                    Else
                        RH.AddColumn(TR01, TR01_06, 6, 6, "c", CDbl(DR(6)).ToString("0"))
                    End If

                    TR01_07.BorderWidth = "1"
                    TR01_07.BorderColor = Drawing.Color.Silver
                    TR01_07.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_07, 6, 6, "c", DR(7))


                    TR01_08.BorderWidth = "1"
                    TR01_08.BorderColor = Drawing.Color.Silver
                    TR01_08.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_08, 6, 6, "c", DR(8))
                    TR01_09.BorderWidth = "1"
                    TR01_09.BorderColor = Drawing.Color.Silver
                    TR01_09.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TR01, TR01_09, 6, 6, "c", Math.Abs(CDbl(DR(9))).ToString("##0.00"))
                    tb.Controls.Add(TR01)
                    TotalMark += DR(8)
                    If CategoryID = 5 Then
                        TotalMarks_deducted += DR(9)
                    Else
                        TotalMarks_Scored += DR(9)
                    End If

                Next
                Dim TRHead_End As New TableRow
                TRHead_End.BackColor = Drawing.Color.LightGray
                ' TRHead_End.ForeColor = Drawing.Color.White
                TRHead_End.Font.Bold = True
                TRHead_End.Height = "25"
                Dim TRHead_End_00, TRHead_End_01, TRHead_End_02, TRHead_End_03, TRHead_End_04, TRHead_End_05, TRHead_End_06, TRHead_End_07, TRHead_End_08, TRHead_End_09 As New TableCell
                RH.AddColumn(TRHead_End, TRHead_End_00, 5, 5, "l", "")
                RH.AddColumn(TRHead_End, TRHead_End_01, 15, 15, "l", "Total Deducted")
                RH.AddColumn(TRHead_End, TRHead_End_02, 34, 34, "l", "")
                RH.AddColumn(TRHead_End, TRHead_End_03, 5, 5, "l", "")
                RH.AddColumn(TRHead_End, TRHead_End_04, 6, 6, "c", "")
                RH.AddColumn(TRHead_End, TRHead_End_05, 6, 6, "c", "")
                RH.AddColumn(TRHead_End, TRHead_End_06, 6, 6, "c", "")
                RH.AddColumn(TRHead_End, TRHead_End_07, 6, 6, "c", "")
                RH.AddColumn(TRHead_End, TRHead_End_08, 6, 6, "c", "")
                RH.AddColumn(TRHead_End, TRHead_End_09, 6, 6, "c", Math.Abs(TotalMarks_deducted).ToString("##0.00"))
                tb.Controls.Add(TRHead_End)
                RH.BlankRow(tb, 5)

                Dim TRGTotal As New TableRow
                TRGTotal.BackColor = Drawing.Color.LightSlateGray
                TRGTotal.ForeColor = Drawing.Color.White
                TRGTotal.Font.Bold = True
                TRGTotal.Height = "25"
                Dim TRGTotal_00, TRGTotal_01, TRGTotal_02, TRGTotal_03, TRGTotal_04, TRGTotal_05, TRGTotal_06, TRGTotal_07, TRGTotal_08, TRGTotal_09 As New TableCell
                RH.AddColumn(TRGTotal, TRGTotal_00, 5, 5, "l", "")
                RH.AddColumn(TRGTotal, TRGTotal_01, 15, 15, "l", "Grand Total")
                RH.AddColumn(TRGTotal, TRGTotal_02, 34, 34, "l", "")
                RH.AddColumn(TRGTotal, TRGTotal_03, 5, 5, "l", "")
                RH.AddColumn(TRGTotal, TRGTotal_04, 6, 6, "c", "")
                RH.AddColumn(TRGTotal, TRGTotal_05, 6, 6, "c", "")
                RH.AddColumn(TRGTotal, TRGTotal_06, 6, 6, "c", "")
                RH.AddColumn(TRGTotal, TRGTotal_07, 6, 6, "c", "")
                RH.AddColumn(TRGTotal, TRGTotal_08, 6, 6, "c", "")
                RH.AddColumn(TRGTotal, TRGTotal_09, 6, 6, "c", (TotalMarks_Scored + TotalMarks_deducted).ToString("##0.00"))
                tb.Controls.Add(TRGTotal)
                RH.BlankRow(tb, 5)
                'Dim TRHead_End As New TableRow
                'Dim TRHead_End_00, TRHead_End_01, TRHead_End_02, TRHead_End_03, TRHead_End_04, TRHead_End_05, TRHead_End_06, TRHead_End_07, TRHead_End_08 As New TableCell
                'TRHead_End_00.BorderWidth = "1"
                'TRHead_End_00.BorderColor = Drawing.Color.Silver
                'TRHead_End_00.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_00, 5, 5, "c", "")
                'TRHead_End_01.BorderWidth = "1"
                'TRHead_End_01.BorderColor = Drawing.Color.Silver
                'TRHead_End_01.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_01, 15, 15, "l", "TOTAL")
                'TRHead_End_02.BorderWidth = "1"
                'TRHead_End_02.BorderColor = Drawing.Color.Silver
                'TRHead_End_02.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_02, 20, 20, "l", "")
                'TRHead_End_03.BorderWidth = "1"
                'TRHead_End_03.BorderColor = Drawing.Color.Silver
                'TRHead_End_03.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_03, 5, 5, "l", "")
                'TRHead_End_04.BorderWidth = "1"
                'TRHead_End_04.BorderColor = Drawing.Color.Silver
                'TRHead_End_04.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_04, 3, 3, "l", "")
                'TRHead_End_05.BorderWidth = "1"
                'TRHead_End_05.BorderColor = Drawing.Color.Silver
                'TRHead_End_05.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_05, 13, 13, "l", "")
                'TRHead_End_06.BorderWidth = "1"
                'TRHead_End_06.BorderColor = Drawing.Color.Silver
                'TRHead_End_06.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_06, 13, 13, "l", "")
                'TRHead_End_07.BorderWidth = "1"
                'TRHead_End_07.BorderColor = Drawing.Color.Silver
                'TRHead_End_07.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_07, 13, 13, "l", "")
                'TRHead_End_08.BorderWidth = "1"
                'TRHead_End_08.BorderColor = Drawing.Color.Silver
                'TRHead_End_08.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_08, 13, 13, "l", Total)
                'Dim TRHead_End As New TableRow
                'TRHead_End.Style.Add("Font-Weight", "Bold")
                'Dim TRHead_End_00 As New TableCell
                'TRHead_End_00.BorderWidth = "1"
                'TRHead_End_00.BorderColor = Drawing.Color.Silver
                'TRHead_End_00.BorderStyle = BorderStyle.Solid
                'RH.AddColumn(TRHead_End, TRHead_End_00, colWidth, colWidth, "l", "Total")
                'For i = 2 To DT.Columns.Count - 1
                '    TRHead_End.BackColor = Drawing.Color.WhiteSmoke
                '    Dim TRHead_End_01 As New TableCell
                '    TRHead_End_01.BorderWidth = "1"
                '    TRHead_End_01.BorderColor = Drawing.Color.Silver
                '    TRHead_End_01.BorderStyle = BorderStyle.Solid
                '    If (total(i - 2) = 0) Then
                '        dispText = "-"
                '    Else
                '        dispText = total(i - 2)
                '    End If
                '    RH.AddColumn(TRHead_End, TRHead_End_01, 13, 13, "c", dispText)
                'Next
                'tb.Controls.Add(TRHead_End)
                RH.BlankRow(tb, 20)
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
