﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewObservationPendingQueue_3
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim LevelID As Integer
    Dim BranchID As Integer
    Dim ItemID As Integer
    Dim Role As String
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))
            LevelID = CInt(Request.QueryString.Get("LevelID"))
            Role = Request.QueryString.Get("Role")

            BranchID = GN.Decrypt(Request.QueryString.Get("BranchID"))

            ItemID = CInt(Request.QueryString.Get("ItemID"))
            Dim ItemName As String = ""
            Dim LevelName As String = ""
            LevelName = AD.GetLevelName(LevelID)
            DT = AD.GetItemName(ItemID, 0)
            If DT.Rows.Count > 0 Then
                ItemName = DT.Rows(0)(0).ToString()
            End If
            Me.hdnLevelID.Value = LevelID.ToString()
            Me.hdnRole.Value = Role
            Dim BranchName As String
            BranchName = GN.GetBranch_Name(BranchID)
            RH.Heading(Session("FirmName"), tb, Role.ToUpper() + " PENDING QUEUE " + LevelName + " Level", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            RH.SubHeading(tb, 100, "l", "Branch : " + BranchName)
            RH.SubHeading(tb, 100, "l", "Check List : " + ItemName)
            RH.BlankRow(tb, 5)
            DT = DB.ExecuteDataSet("select a.SINO,a.LOAN_NO,ISNULL(h.DISBURSED_DT,h.SANCTIONED_DT ),h.LOAN_AMT,i.CENTER_NAME ,h.CLIENT_NAME   ,f.REMARKS,g.DESCRIPTION  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL d,AUDIT_OBSERVATION_STATUS_MASTER c,AUDIT_OBSERVATION_CYCLE f,AUDIT_UPDATE_MASTER g,LOAN_MASTER h,CENTER_MASTER i where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=d.AUDIT_ID  and a.LOAN_NO=h.LOAN_NO and h.CENTER_ID=i.CENTER_ID   and a.role_id=c.Status_ID and a.STATUS_ID>0 and a.LAST_UPDATED_DT is not null and  a.LEVEL_ID=" + LevelID.ToString() + " and c.Description='" + Role + "'  and d.BRANCH_ID=" + BranchID.ToString() + " and b.Item_ID=" + ItemID.ToString() + "  and a.ORDER_ID=f.ORDER_ID and f.UPDATE_ID=g.UPDATE_ID ").Tables(0)
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 15, 15, "l", "Ref. No")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "Customer")
            RH.AddColumn(TRHead, TRHead_03, 8, 8, "l", "Date")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Center")
            RH.AddColumn(TRHead, TRHead_05, 7, 7, "r", "Amount")
            RH.AddColumn(TRHead, TRHead_06, 35, 35, "l", "Remarks")
            RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Status")
            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            For Each DR In DT.Rows
                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02, TR01_03, TR01_04, TR01_05, TR01_06, TR01_07 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"
                TR01_03.BorderWidth = "1"
                TR01_04.BorderWidth = "1"
                TR01_05.BorderWidth = "1"
                TR01_06.BorderWidth = "1"
                TR01_07.BorderWidth = "1"

                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver
                TR01_03.BorderColor = Drawing.Color.Silver
                TR01_04.BorderColor = Drawing.Color.Silver
                TR01_05.BorderColor = Drawing.Color.Silver
                TR01_06.BorderColor = Drawing.Color.Silver
                TR01_07.BorderColor = Drawing.Color.Silver

                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid
                TR01_03.BorderStyle = BorderStyle.Solid
                TR01_04.BorderStyle = BorderStyle.Solid
                TR01_05.BorderStyle = BorderStyle.Solid
                TR01_06.BorderStyle = BorderStyle.Solid
                TR01_07.BorderStyle = BorderStyle.Solid

                I = I + 1
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR01, TR01_01, 15, 15, "l", DR(1))
                RH.AddColumn(TR01, TR01_02, 10, 10, "l", DR(5))
                RH.AddColumn(TR01, TR01_03, 8, 8, "l", CDate(DR(2)).ToString("dd/MM/yy"))
                RH.AddColumn(TR01, TR01_04, 10, 10, "l", DR(4))
                RH.AddColumn(TR01, TR01_05, 7, 7, "r", CDbl(DR(3)).ToString("####,##,###"))
                RH.AddColumn(TR01, TR01_06, 35, 35, "l", DR(6) + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(0)) + "' style='text-align:right;' target='_blank' >Previous</a>")
                RH.AddColumn(TR01, TR01_07, 10, 10, "l", DR(7))
                tb.Controls.Add(TR01)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
