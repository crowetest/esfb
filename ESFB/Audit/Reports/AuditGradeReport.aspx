﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditGradeReport.aspx.vb" Inherits="Audit_Reports_AuditGradeReport" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                Type</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <input id="chkMark" type="checkbox" />Show Mark</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
            if (TypeID == -1)
            { alert("Select Type"); document.getElementById("<%= cmbType.ClientID %>").focus(); return false; }
            var RptID = 0;
            if (document.getElementById("chkMark").checked == true)
                RptID = 1;          
            window.open("viewGradeReport.aspx?TypeID=" + btoa(TypeID) + " &RptID=" + btoa(RptID), "_self");        
            
        }
      
// ]]>
    </script>
</asp:Content>

