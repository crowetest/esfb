﻿
Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf


Partial Class Reports_ViewVisitedSangamsConsolidated
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AssingDt As String
    Dim From As Integer
    Dim RptID As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim SubHD As String = ""
    Dim DTExcel As New DataTable
    Dim AssignDtl() As String
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            Dim Postid As Integer = CInt(Session("Post_ID"))
            Dim EmpCode As Integer = CInt(Session("UserID"))

            Dim DR As DataRow
            AssingDt = ""
            FromDt = DateTime.Now.Date
            ToDt = DateTime.Now.Date
            Dim StrVal As String = ""


            RptID = Request.QueryString.Get("RptID")
            If (RptID = 1) Then
                AssingDt = GF.Decrypt(Request.QueryString.Get("AuditID"))

                AssignDtl = AssingDt.Split("~")
                tb.Attributes.Add("width", "100%")
                RH.Heading(Session("FirmName"), tb, "Visited Sangam List of " + MonthName(CInt(AssignDtl(0))) + "-" + AssignDtl(1) + " Audit", 100)
                Dim RowBG As Integer = 0

                SubHD = MonthName(CInt(AssignDtl(0))) + "-" + AssignDtl(1) + ""

                If Postid = 2 Then 'Admin

                    StrVal = " and a.branch_id in (select distinct branch_id  from (select  branch_id from Audit_Admin_Branches b where  b.type_id=1  " & _
                        " union all select  branch_id from Audit_Admin_Branches b where  b.type_id=1  )AA )"
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Admin_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.type_id=1 and (b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
                ElseIf Postid = 3 Then 'Team Lead
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
                    StrVal = " and a.branch_id in (select distinct branch_id  from (select  branch_id from Audit_Branches b where   (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " )  " & _
                        " union all select  branch_id from Audit_Branches_his b where   (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ) )AA )"
                    'DT = DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit Union All select distinct cast(MONTH(a.Period_to) as varchar(2))+'~'+cast(year(a.Period_to) as varchar(4)), datename(MONTH,a.Period_to) +'-'+CAST(YEAR(a.Period_to) AS VARCHAR(4)) from Audit_Master a,Audit_Branches b where a.Branch_id=b.Branch_id and (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ) and a.status_id<>9").Tables(0)
                ElseIf Postid = 4 Then 'Auditor
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.emp_code=" + EmpCode.ToString() + " )").Tables(0)
                    StrVal = " and  c.emp_code=" & EmpCode.ToString & " and a.branch_id in ( select distinct branch_id from (select  branch_id from Audit_Branches b where   ( b.emp_code=" + EmpCode.ToString() + " ) " & _
                        " union all select  branch_id from Audit_Branches_his b where   ( b.emp_code=" + EmpCode.ToString() + " ) )AA )"
                ElseIf Postid = 22 Then 'Junior Auditor
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Admin_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.type_id=0 and (b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
                    StrVal = " and  c.emp_code=" & EmpCode.ToString & " and a.branch_id in (select distinct branch_id  from (select  branch_id from Audit_Admin_Branches b where  b.type_id=0  " & _
                      " union all select  branch_id from Audit_Admin_Branches b where  b.type_id=0  )AA )"

                    'StrVal = " and a.branch_id in ( select distinct branch_id from Audit_Admin_Branches b where  b.type_id=0  and ( b.emp_code=" + EmpCode.ToString() + " ) )"
                Else
                    StrVal = ""
                End If


                'DT = DB.ExecuteDataSet("select COUNT(c.GROUP_ID) as Count,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,c.visited_id,a.group_id,f.branch_name,f.branch_id,c.emp_code from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                '                      " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and month(a.period_to) =" + AssignDtl(0) + " and year(a.period_to)=" + AssignDtl(1) + "  " & StrVal.ToString & "  group by c.visited_id,a.group_id,f.branch_name,f.branch_id ,d.emp_name,c.emp_code").Tables(0)
                'DTExcel = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY c.visited_id,a.period_to )) AS 'Sl No',f.branch_name,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,COUNT(c.GROUP_ID) as Count from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                '                       " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and month(a.period_to) =" + AssignDtl(0) + " and year(a.period_to)=" + AssignDtl(1) + " " & StrVal.ToString & " group by c.visited_id,a.group_id,f.branch_name,f.branch_id ,d.emp_name,c.emp_code,a.period_to ").Tables(0)

                DT = DB.ExecuteDataSet(" select staff,SUM(BrCount),SUM(SCount),emp_code from ( select c.emp_code,COUNT(c.GROUP_ID) as SCount,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,count(distinct f.Branch_ID) as BrCount from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d   " & _
                            " where a.branch_id = f.branch_id And a.GROUP_ID = b.AUDIT_GROUP_ID And b.VISITED_ID = c.VISITED_ID And c.emp_code = d.emp_code and month(a.period_to) =" + AssignDtl(0) + " and year(a.period_to)=" + AssignDtl(1) + "  " & StrVal.ToString & "  " & _
                            " group by c.visited_id,d.emp_name,c.emp_code)XX   group by staff,emp_code").Tables(0)
                DTExcel = DB.ExecuteDataSet(" select convert(varchar,ROW_NUMBER() OVER(ORDER BY staff)) AS 'Sl No',staff,SUM(BrCount),SUM(SCount) from ( select COUNT(c.GROUP_ID) as SCount,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,count(distinct f.Branch_ID) as BrCount   " & _
                            "  from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                                       " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and month(a.period_to) =" + AssignDtl(0) + " and year(a.period_to)=" + AssignDtl(1) + " " & StrVal.ToString & " group by c.visited_id,d.emp_name,c.emp_code)XX   group by staff ").Tables(0)

            ElseIf RptID = 2 Then
                FromDt = CDate(Request.QueryString.Get("FromDt"))
                ToDt = CDate(Request.QueryString.Get("ToDt"))
                tb.Attributes.Add("width", "100%")
                SubHD = FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy") + ""
                RH.Heading(Session("FirmName"), tb, "Visited Sangam List of during  " + FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy"), 100)

                If Postid = 2 Then 'Admin

                    StrVal = " and a.branch_id in (select distinct AA.branch_id  from (select  branch_id from Audit_Admin_Branches b where  b.type_id=1 and assigned_on <= '" + ToDt.ToString("dd/MMM/yyyy") + "' " & _
                        " union all select  branch_id from Audit_Admin_Branches b where  b.type_id=1 and assigned_on <= '" + ToDt.ToString("dd/MMM/yyyy") + "' )AA,(select * from audit_admin_branches where emp_code=" & EmpCode.ToString & " and TYPE_ID=1) g where aa.branch_id=g.branch_id  )"
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Admin_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.type_id=1 and (b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
                ElseIf Postid = 3 Then 'Team Lead
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
                    StrVal = " and a.branch_id in (select distinct branch_id  from (select  branch_id from Audit_Branches b where   (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ) and  assigned_on <='" + ToDt.ToString("dd/MMM/yyyy") + "' " & _
                        " union all select  branch_id from Audit_Branches_his b where   (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ) and  assigned_on<= '" + ToDt.ToString("dd/MMM/yyyy") + "')AA )"
                    'DT = DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit Union All select distinct cast(MONTH(a.Period_to) as varchar(2))+'~'+cast(year(a.Period_to) as varchar(4)), datename(MONTH,a.Period_to) +'-'+CAST(YEAR(a.Period_to) AS VARCHAR(4)) from Audit_Master a,Audit_Branches b where a.Branch_id=b.Branch_id and (b.Team_lead=" + EmpCode.ToString() + "  or b.emp_code=" + EmpCode.ToString() + " ) and a.status_id<>9").Tables(0)
                ElseIf Postid = 4 Then 'Auditor
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Branches b where a.Branch_id=b.Branch_id and a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.emp_code=" + EmpCode.ToString() + " )").Tables(0)
                    StrVal = " and  c.emp_code=" & EmpCode.ToString & " and a.branch_id in ( select distinct branch_id from (select  branch_id from Audit_Branches b where   ( b.emp_code=" + EmpCode.ToString() + " ) and assigned_on <= '" + ToDt.ToString("dd/MMM/yyyy") + "'" & _
                        " union all select  branch_id from Audit_Branches_his b where   ( b.emp_code=" + EmpCode.ToString() + " ) and assigned_on <= '" + ToDt.ToString("dd/MMM/yyyy") + "')AA )"
                ElseIf Postid = 22 Then 'Junior Auditor
                    'Return DB.ExecuteDataSet("select -1 as BranchID,' ------Select------' as BranchName Union all SELECT Branch_ID,Branch_Name From BRANCH_MASTER h where EXISTS (SELECT a.Branch_ID from AUDIT_MASTER a,Audit_Admin_Branches b where a.Branch_id=b.Branch_ID and  a.branch_ID=h.Branch_ID and a.STATUS_ID=1 and b.type_id=0 and (b.emp_code=" + EmpCode.ToString() + " ))").Tables(0)
                    StrVal = " and  c.emp_code=" & EmpCode.ToString & " and a.branch_id in (select distinct branch_id  from (select  branch_id from Audit_Admin_Branches b where  b.type_id=0 and assigned_on <= '" + ToDt.ToString("dd/MMM/yyyy") + "' " & _
                      " union all select  branch_id from Audit_Admin_Branches b where  b.type_id=0 and assigned_on <='" + ToDt.ToString("dd/MMM/yyyy") + "' )AA )"

                    'StrVal = " and a.branch_id in ( select distinct branch_id from Audit_Admin_Branches b where  b.type_id=0  and ( b.emp_code=" + EmpCode.ToString() + " ) )"
                Else
                    StrVal = ""
                End If


                'DT = DB.ExecuteDataSet("select COUNT(c.GROUP_ID) as Count,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,c.visited_id,a.group_id,f.branch_name + '  (' + convert(varchar,datename(MONTH,month(a.PERIOD_TO)))+'-'+convert(varchar,year(a.period_to))+')',f.branch_id,c.emp_code from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                '                      " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and DATEADD(day, DATEDIFF(day, 0, c.visited_dt), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'  " & StrVal.ToString & "  group by c.visited_id,a.group_id,f.branch_name,f.branch_id ,d.emp_name,c.emp_code,a.period_to order by c.visited_id,a.period_to").Tables(0)
                'DTExcel = DB.ExecuteDataSet("select convert(varchar,ROW_NUMBER() OVER(ORDER BY c.visited_id,a.period_to )) AS 'Sl No',f.branch_name,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,COUNT(c.GROUP_ID) as Count  from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                '                      " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and DATEADD(day, DATEDIFF(day, 0, c.visited_dt), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'  " & StrVal.ToString & "  group by c.visited_id,a.group_id,f.branch_name,f.branch_id ,d.emp_name,c.emp_code,a.period_to order by c.visited_id,a.period_to").Tables(0)

                DT = DB.ExecuteDataSet(" select staff,SUM(BrCount),SUM(SCount),emp_code from ( select c.emp_code,COUNT(c.GROUP_ID) as SCount,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,count(distinct f.Branch_ID) as BrCount from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d   " & _
                            " where a.branch_id = f.branch_id And a.GROUP_ID = b.AUDIT_GROUP_ID And b.VISITED_ID = c.VISITED_ID And c.emp_code = d.emp_code and DATEADD(day, DATEDIFF(day, 0, c.visited_dt), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'  " & StrVal.ToString & "  " & _
                            " group by c.visited_id,d.emp_name,c.emp_code)XX  group by staff,emp_code ").Tables(0)
                DTExcel = DB.ExecuteDataSet(" select convert(varchar,ROW_NUMBER() OVER(ORDER BY staff)) AS 'Sl No',staff,SUM(BrCount),SUM(SCount) from ( select COUNT(c.GROUP_ID) as SCount,d.emp_name + '(' + convert(varchar,c.emp_code) + ')' as Staff,count(distinct f.Branch_ID) as BrCount from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d   " & _
                                " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and DATEADD(day, DATEDIFF(day, 0, c.visited_dt), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' " & StrVal.ToString & " group by c.visited_id,d.emp_name,c.emp_code)XX   group by staff ").Tables(0)
            End If

            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 30, 0, "Staff")
            RH.InsertColumn(TRHead, TRHead_02, 35, 2, "BranchCount")
            RH.InsertColumn(TRHead, TRHead_03, 30, 2, "Sangams")
            Dim I As Integer = 1
            tb.Controls.Add(TRHead)

            Dim Total As Integer = 0

            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                RH.InsertColumn(TR3, TR3_01, 30, 0, DR(0).ToString())
                RH.InsertColumn(TR3, TR3_02, 35, 2, " <a href='ViewVisitedBranchDetails.aspx?AssignDate=" + AssingDt.ToString() + " &SubHD=Visited Centers - " + DR(0).ToString() + "-" + SubHD.ToString() + " &RptID=" + RptID.ToString() + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + " &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &Emp_Code=" + DR(3).ToString() + "' target='_blank'/>" + DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 30, 2, " <a href='ViewVisitedCenters.aspx?AuditID=" + GF.Encrypt("-1") + " &AssignDate=" + AssingDt.ToString() + " &SubHD=Visited Centers - " + DR(0).ToString() + "-" + SubHD.ToString() + " &RptID=" + RptID.ToString() + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + " &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &BranchID=" + GF.Encrypt("-1") + " &Emp_Code=" + DR(3).ToString() + "' target='_blank'/>" + DR(2).ToString())

                tb.Controls.Add(TR3)
                Total = Total + CInt(DR(2))
                I = I + 1

            Next
            Dim TR4 As New TableRow
            TR4.BorderWidth = "1"
            TR4.BorderStyle = BorderStyle.Solid

            Dim TR4_02, TR4_03 As New TableCell

            RH.InsertColumn(TR4, TR4_02, 70, 1, "<b>Total Centers</b>")
            RH.InsertColumn(TR4, TR4_03, 30, 2, Total.ToString())

            tb.Controls.Add(TR4)
            RH.BlankRow(tb, 20)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try



    End Sub

    Protected Sub cmd_Excel_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel_Export.Click
        
        Try
            ''Response.Clear()
            ''Response.Buffer = True
            ''Response.ContentType = "application/vnd.ms-excel"
            ''Response.AddHeader("content-disposition", "attachment;filename=Report.xls")
            ''Response.Charset = ""
            ''Me.EnableViewState = False

            ''Dim sw As New System.IO.StringWriter()
            ''Dim htw As New System.Web.UI.HtmlTextWriter(sw)

            ''pnDisplay.RenderControl(htw)

            ''Response.Write(sw.ToString())
            ''Response.[End]()
            Dim HeaderText As String
            If (RptID = 1) Then
                HeaderText = "Visited Sangam List of " + MonthName(CInt(AssignDtl(0))) + "-" + AssignDtl(1) + " Audit"
            Else
                HeaderText = "Visited Sangam List of during  " + FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy")
            End If


            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
