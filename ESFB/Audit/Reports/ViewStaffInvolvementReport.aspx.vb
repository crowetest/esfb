﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementEmpWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim SINO As Integer = 1
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim tb As New Table
            tb.Attributes.Add("width", "100%")
            tb.Style.Add("table-layout", "fixed")
            RH.Heading(Session("FirmName"), tb, "Financial Leakage Settlement-Recovery", 100)

            Dim RowBG As Integer = 0
            Dim TRHead As New TableRow
            TRHead.Style.Add("Font-weight", "bold")
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_01, 10, 0, "Region")
            RH.InsertColumn(TRHead, TRHead_02, 10, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_03, 17, 0, "Staff Name")
            RH.InsertColumn(TRHead, TRHead_04, 8, 2, "Amount")
            RH.InsertColumn(TRHead, TRHead_05, 42, 2, "Recovery Status")
            RH.InsertColumn(TRHead, TRHead_06, 8, 2, "Balance")
            tb.Controls.Add(TRHead)

            Dim TRHead1 As New TableRow
            TRHead1.Style.Add("Font-weight", "bold")
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08, TRHead1_09, TRHead1_10 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "")
            RH.InsertColumn(TRHead1, TRHead1_01, 10, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_02, 10, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_03, 17, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_04, 8, 2, "")
            'RH.InsertColumn(TRHead1, TRHead1_05, 8, 2, "Recovery") '1
            'RH.InsertColumn(TRHead1, TRHead1_06, 8, 2, "Write-Off") '2
            'RH.InsertColumn(TRHead1, TRHead1_07, 10, 2, "Legal Initiation") '3
            'RH.InsertColumn(TRHead1, TRHead1_08, 8, 2, "Rectification") '4
            'RH.InsertColumn(TRHead1, TRHead1_09, 8, 2, "HR Action") '5
            RH.InsertColumn(TRHead1, TRHead1_05, 8, 2, "Recovery") '1
            RH.InsertColumn(TRHead1, TRHead1_06, 8, 2, "Rectification") '2
            RH.InsertColumn(TRHead1, TRHead1_07, 10, 2, "HR Action") '3
            RH.InsertColumn(TRHead1, TRHead1_08, 8, 2, "Legal Initiation") '4
            RH.InsertColumn(TRHead1, TRHead1_09, 8, 2, "Write-Off") '5
            RH.InsertColumn(TRHead1, TRHead1_10, 8, 2, "")
            tb.Controls.Add(TRHead1)

            DT = DB.ExecuteDataSet("select c.Region_Name,c.Branch_Name,b.Emp_Code,b.Emp_Name,SUM(isnull(a.Amount,0)) Total_amount,isnull(sum(d.Recover),0) ,isnull(sum(d.Writeoff),0),isnull(sum(d.Legal),0),isnull(sum(d.Rectification),0),isnull(sum(d.Hraction),0),sum(isnull(a.Amount,0)-isnull(a.refund_amount,0)) from emp_master b,BrMaster c,AUDIT_STAFF_INVOLVEMENT a left outer join ( select emp_code,SLNO,sum(case when typeid = 1 then amount else 0 end) as Recover,sum(case when typeid = 2 then amount else 0 end) as Writeoff,sum(case when typeid = 3 then amount else 0 end) as Rectification,sum(case when typeid = 4 then amount else 0 end) as Legal,sum(case when typeid = 5 then amount else 0 end) as Hraction from AUDIT_STAFF_INVOLVEMENT_RECOVERY_DTL group by EMP_CODE,SLNO)d on(d.SLNO=a.SINO and d.EMP_CODE=a.EMP_CODE ) where a.EMP_CODE = b.Emp_Code and b.Branch_ID = c.Branch_ID and (isnull(a.Amount,0) - isnull(a.Refund_Amount,0))>0 and a.SINO in(select SINO from AUDIT_OBSERVATION_DTL where OBSERVATION_ID in(select OBSERVATION_ID from audit_observation where AUDIT_ID in(select AUDIT_ID from AUDIT_DTL where group_id in(select GROUP_ID from AUDIT_MASTER where status_id=2))) )  group by c.Region_Name,c.Branch_Name,b.Emp_Code,b.Emp_Name order by c.region_name,c.branch_name,b.emp_code").Tables(0)
            Dim DR As DataRow
            Dim Tot1 As Double = 0
            Dim Tot2 As Double = 0
            Dim Tot3 As Double = 0
            Dim Tot4 As Double = 0
            Dim Tot5 As Double = 0
            Dim Tot6 As Double = 0
            Dim Tot7 As Double = 0
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, SINO.ToString())
                RH.InsertColumn(TR3, TR3_01, 10, 0, DR(0).ToString())
                RH.InsertColumn(TR3, TR3_02, 10, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 17, 0, DR(3).ToString() + "(" + DR(2).ToString() + ")")
                RH.InsertColumn(TR3, TR3_04, 8, 2, DR(4).ToString())
                RH.InsertColumn(TR3, TR3_05, 8, 2, DR(5).ToString())
                RH.InsertColumn(TR3, TR3_06, 8, 2, DR(8).ToString())
                RH.InsertColumn(TR3, TR3_07, 10, 2, DR(9).ToString())
                RH.InsertColumn(TR3, TR3_08, 8, 2, DR(7).ToString())
                RH.InsertColumn(TR3, TR3_09, 8, 2, DR(6).ToString())
                RH.InsertColumn(TR3, TR3_10, 8, 2, DR(10).ToString())
                tb.Controls.Add(TR3)
                SINO += 1
                Tot1 += DR(4)
                Tot2 += DR(5)
                Tot3 += DR(6)
                Tot4 += DR(7)
                Tot5 += DR(8)
                Tot6 += DR(9)
                Tot7 += DR(10)
            Next

            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06, TRFooter_07 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            RH.InsertColumn(TRFooter, TRFooter_00, 42, 2, "Total")
            RH.InsertColumn(TRFooter, TRFooter_01, 8, 2, Tot1.ToString())
            RH.InsertColumn(TRFooter, TRFooter_02, 8, 2, Tot2.ToString())
            RH.InsertColumn(TRFooter, TRFooter_03, 8, 2, Tot3.ToString())
            RH.InsertColumn(TRFooter, TRFooter_04, 10, 2, Tot4.ToString())
            RH.InsertColumn(TRFooter, TRFooter_05, 8, 2, Tot5.ToString())
            RH.InsertColumn(TRFooter, TRFooter_06, 8, 2, Tot6.ToString())
            RH.InsertColumn(TRFooter, TRFooter_07, 8, 2, Tot7.ToString())
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
