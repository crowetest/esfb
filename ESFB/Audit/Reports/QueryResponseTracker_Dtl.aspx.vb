﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class QueryResponseTracker_Dtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim GroupID As Integer = -1
    Dim ItemID As Integer = -1
    Dim StatusID As Integer = -1
    Dim GN As New GeneralFunctions
    Dim PostID As Integer
    Dim UserID As Integer
    Dim SqlStr As String
    Dim TypeID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            PostID = CInt(Session("Post_ID"))
            UserID = CInt(Session("UserID"))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            TypeID = CInt(Request.QueryString.Get("TypeID"))
            If Not IsNothing(Request.QueryString.Get("GroupID")) Then
                GroupID = CInt(Request.QueryString.Get("GroupID"))
            End If
            If Not IsNothing(Request.QueryString.Get("ItemID")) Then
                ItemID = CInt(Request.QueryString.Get("ItemID"))
            End If
            If Not IsNothing(Request.QueryString.Get("StatusID")) Then
                StatusID = CInt(Request.QueryString.Get("StatusID"))
            End If
            Dim SubHead As String = ""
            Dim SubHead1 As String = ""
            If BranchID > 0 Then
                SubHead = " - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
            ElseIf AreaID > 0 Then
                SubHead = " - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
            ElseIf RegionID > 0 Then
                SubHead = " - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
            End If
            If GroupID > 0 Then
                Dim AuditDescr As String = ""
                DT = DB.ExecuteDataSet("select DATENAME(month,Period_to)+'/'+CAST(YEAR(Period_to)as varchar(4)) from audit_master where group_id=" + GroupID.ToString()).Tables(0)
                If DT.Rows.Count > 0 Then
                    AuditDescr = DT.Rows(0)(0).ToString()
                End If
                SubHead += "  " + AuditDescr.ToUpper() + " AUDIT"
            End If
            If ItemID > 0 Then
                Dim ItemDescr As String = ""
                DT = DB.ExecuteDataSet("select a.Item_Name from audit_Check_List a,audit_observation b where a.item_id=b.item_id and  b.observation_ID=" + ItemID.ToString()).Tables(0)
                If DT.Rows.Count > 0 Then
                    ItemDescr = DT.Rows(0)(0).ToString()
                End If
                SubHead1 = " Check List - " + ItemDescr.ToUpper() + ""
            End If
            If StatusID > 0 Then
                SubHead += " STATUS - PENDING"
            ElseIf StatusID = 0 Then
                SubHead += " STATUS - CLOSED"
            End If
            RH.Heading(Session("FirmName"), tb, "QUERY RESPONSE TRACKER " + SubHead.ToUpper(), 100)
            RH.BlankRow(tb, 5)
            RH.SubHeading(tb, 100, "l", SubHead1)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim PendingTotal As Integer = 0
            Dim CompletedTotal As Integer = 0
            Dim Total As Integer = 0


            If TypeID = 3 Then
                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

                TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"


                TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderColor = Drawing.Color.Silver


                TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid


                RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                RH.AddColumn(TRHead, TRHead_01, 30, 30, "l", "Check List")
                RH.AddColumn(TRHead, TRHead_02, 35, 35, "l", "Last Remark")
                RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "Current Level")
                RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Status")

                tb.Controls.Add(TRHead)

                Dim SqlStr As String = "select a.sino,j.ITEM_ID,j.ITEM_NAME,a.REMARKS ,(case when a.STATUS_ID>0 then g.LEVEL_NAME else '' end  )as CurrentLevel ,(case when a.STATUS_ID=0 then 'Closed' else 'Pending' end)as Stat from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e,AUDIT_USER_LEVELS g,AUDIT_CHECK_GROUPS h,AUDIT_CHECK_SUBGROUPS i,AUDIT_CHECK_LIST j   where a.OBSERVATION_ID=b.OBSERVATION_ID and b.ITEM_ID =j.ITEM_ID  and j.SUB_GROUP_ID=i.SUB_GROUP_ID and i.GROUP_ID=h.GROUP_ID  and a.LEVEL_ID=g.LEVEL_ID  and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and b.Observation_ID=" + ItemID.ToString() + ""

                If StatusID = 0 Then
                    SqlStr += " and a.status_id=0"
                ElseIf StatusID > 0 Then
                    SqlStr += " and a.status_id>0"
                End If
                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                For Each DR In DT.Rows

                    LineNumber += 1
                    j += 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid


                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"


                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid

                    I = I + 1
                    RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                    RH.AddColumn(TR3, TR3_01, 30, 30, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_02, 35, 35, "l", DR(3).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                    RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(5).ToString())
                    tb.Controls.Add(TR3)
                Next

                RH.BlankRow(tb, 25)
            Else
                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

                TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"
                TRHead_05.BorderWidth = "1"

                TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_02.BorderColor = Drawing.Color.Silver
                TRHead_03.BorderColor = Drawing.Color.Silver
                TRHead_04.BorderColor = Drawing.Color.Silver
                TRHead_05.BorderColor = Drawing.Color.Silver

                TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid
                TRHead_05.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Ref No")
                RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Name")
                RH.AddColumn(TRHead, TRHead_03, 35, 35, "l", "Last Remark")
                RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Current Level")
                RH.AddColumn(TRHead, TRHead_05, 15, 15, "l", "Status")

                tb.Controls.Add(TRHead)

                Dim SqlStr As String = "select a.sino, f.LOAN_NO ,f.CLIENT_NAME,a.REMARKS ,(case when a.STATUS_ID>0 then g.LEVEL_NAME else '' end  )as CurrentLevel ,(case when a.STATUS_ID=0 then 'Closed' else 'Pending' end)as Stat from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e,LOAN_MASTER f,AUDIT_USER_LEVELS g   where a.OBSERVATION_ID=b.OBSERVATION_ID and a.LOAN_NO =f.LOAN_NO and a.LEVEL_ID=g.LEVEL_ID  and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  and b.Observation_ID=" + ItemID.ToString() + ""

                If StatusID = 0 Then
                    SqlStr += " and a.status_id=0"
                ElseIf StatusID > 0 Then
                    SqlStr += " and a.status_id>0"
                End If
                DT = DB.ExecuteDataSet(SqlStr).Tables(0)
                For Each DR In DT.Rows

                    LineNumber += 1
                    j += 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid


                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid

                    I = I + 1
                    RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", "<a href=viewLoanDetails.aspx?LoanNo=" + GN.Encrypt(DR(1).ToString()) + " target=_blank>" + DR(1).ToString() + "</a>")
                    RH.AddColumn(TR3, TR3_02, 20, 20, "l", DR(2).ToString())
                    RH.AddColumn(TR3, TR3_03, 35, 35, "l", DR(3).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                    RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(4).ToString())
                    RH.AddColumn(TR3, TR3_05, 15, 15, "l", DR(5).ToString())
                    tb.Controls.Add(TR3)
                Next

                RH.BlankRow(tb, 25)

            End If

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
