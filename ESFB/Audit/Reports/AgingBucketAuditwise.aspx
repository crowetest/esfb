﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AgingBucketAuditwise.aspx.vb" Inherits="AgingBucketAuditwise" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server"> 
<br />
 <table align="center" style="width:50%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                Audit</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Type</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ALL</asp:ListItem>
                    <asp:ListItem Value="1">Pending</asp:ListItem>
                    <asp:ListItem Value="2">Closed</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Criteria</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbDaysType" class="NormalText" runat="server" Font-Names="Cambria" 
                  Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    <asp:ListItem Value="1">Total Days</asp:ListItem>
                    <asp:ListItem Value="2">Queue Days</asp:ListItem>
                    <asp:ListItem Value="3">Operation Days</asp:ListItem>
                    <asp:ListItem Value="4">Audit Days</asp:ListItem>
                </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="txtDays" style="width: 10%" type="text" />&nbsp;&nbsp;Days</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
           <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
            </td>
        </tr></table>
   
    <script language="javascript" type="text/javascript">
// <![CDATA[
        function TypeOnChange() {
            if (document.getElementById("<%= cmbType.ClientID %>").value == 1) {
                document.getElementById("<%= cmbDaysType.ClientID %>").value = -1;
                document.getElementById("<%= cmbDaysType.ClientID %>").disabled = false;
            }
            else {
                document.getElementById("<%= cmbDaysType.ClientID %>").value = 1;
                document.getElementById("<%= cmbDaysType.ClientID %>").disabled = true;
              
            }
        }
        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var Audit = document.getElementById("<%= cmbAudit.ClientID %>").value;              
            var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
            if (document.getElementById("<%= cmbDaysType.ClientID %>").value == -1)
            { alert("Select Criteria"); document.getElementById("<%= cmbDaysType.ClientID %>").focus(); return false; }
            var Type = document.getElementById("<%= cmbType.ClientID %>").value;
            var DayType = document.getElementById("<%= cmbDaysType.ClientID %>").value;
            if (document.getElementById("txtDays").value == "") {
                alert("Enter Days"); document.getElementById("txtDays").focus(); return false;
            }
            var Days = document.getElementById("txtDays").value;
            var OptGreat = 1;         
            window.open("viewBucketAuditWiseReport.aspx?Type=" + btoa(Type) + " &DayType=" + DayType + " &Days=" + Days + " &OptGreat=" + OptGreat + " &AuditID=" + btoa(Audit), "_blank");

        }
        function FromServer(arg, context) {
            switch (context) {
                case 1:
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) { window.open("AgingBucketAuditwise.aspx", "_self"); }
                    break;
                
                default:
                    break;
            }
        }
  
     

       

// ]]>
    </script>
</asp:Content>

