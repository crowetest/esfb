﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewAuditAbstractReportDetail
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim AuditID As String
    Dim CloseFlag As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim SubHD As String
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim SearchID As Integer
    Dim Rtype As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            AuditID = Request.QueryString.Get("AuditID")

            CloseFlag = Request.QueryString.Get("CloseFlag")
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            SearchID = CInt(Request.QueryString.Get("SubGroupID"))
            Rtype = CInt(Request.QueryString.Get("RType"))
            Dim AuditDtl() As String
            AuditDtl = AuditID.Split("~")
            Dim MonthID As Integer
            Dim YearID As Integer
            If AuditDtl(0) = -1 Then
                If (BranchID > 0) Then
                    SubHD = " AUDIT ABSTRACT REPORT " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                ElseIf AreaID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                ElseIf RegionID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                Else
                    SubHD = " AUDIT ABSTRACT REPORT "
                End If
            Else
                MonthID = CInt(AuditDtl(0))
                YearID = CInt(AuditDtl(1))
                If (BranchID > 0) Then
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                ElseIf AreaID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                ElseIf RegionID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                Else
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString()
                End If
            End If
            RH.Heading(Session("FirmName"), tb, SubHD, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = AD.GetAbstractReportDetail(MonthID, YearID, RegionID, AreaID, BranchID, 1, CloseFlag, FromDt, ToDt, Rtype, SearchID, 0, 0, CInt(Session("UserID")))
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 45, 45, "l", "Check List")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "V.Serious")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Serious")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "c", "General")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "Total")
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "c", "Est.Amt. Involved")

            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            Dim AuditType As Integer = 0
            Dim TotSuspFraud As Integer = 0
            Dim TotSuspLeakage As Integer = 0
            Dim TotGeneral As Integer = 0
            Dim TotLeakage As Double = 0

            Dim GrandTotal As Integer = 0
            Dim GrandSuspFraud As Integer = 0
            Dim GrandSuspLeakage As Integer = 0
            Dim GrandGeneral As Integer = 0
            Dim GrandLeakage As Double = 0

            For Each DR In DT.Rows
                If AuditType <> CInt(DR(7)) Then
                    If (I > 0) Then
                        Dim TRTotal As New TableRow
                        Dim TRTotal_00, TRTotal_01, TRTotal_02, TRTotal_03, TRTotal_04, TRTotal_05, TRTotal_06 As New TableCell
                        TRTotal.Style.Add("Font-Weight", "Bold")

                        TRTotal_00.BorderWidth = "1"
                        TRTotal_01.BorderWidth = "1"
                        TRTotal_02.BorderWidth = "1"
                        TRTotal_03.BorderWidth = "1"
                        TRTotal_04.BorderWidth = "1"
                        TRTotal_05.BorderWidth = "1"
                        TRTotal_06.BorderWidth = "1"

                        TRTotal_00.BorderColor = Drawing.Color.Silver
                        TRTotal_01.BorderColor = Drawing.Color.Silver
                        TRTotal_02.BorderColor = Drawing.Color.Silver
                        TRTotal_03.BorderColor = Drawing.Color.Silver
                        TRTotal_04.BorderColor = Drawing.Color.Silver
                        TRTotal_05.BorderColor = Drawing.Color.Silver
                        TRTotal_06.BorderColor = Drawing.Color.Silver

                        TRTotal_00.BorderStyle = BorderStyle.Solid
                        TRTotal_01.BorderStyle = BorderStyle.Solid
                        TRTotal_02.BorderStyle = BorderStyle.Solid
                        TRTotal_03.BorderStyle = BorderStyle.Solid
                        TRTotal_04.BorderStyle = BorderStyle.Solid
                        TRTotal_05.BorderStyle = BorderStyle.Solid
                        TRTotal_06.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TRTotal, TRTotal_00, 5, 5, "c", "")
                        RH.AddColumn(TRTotal, TRTotal_01, 45, 45, "r", "Total&nbsp;&nbsp;")
                        If (TotSuspFraud > 0) Then
                            RH.AddColumn(TRTotal, TRTotal_02, 10, 10, "c", TotSuspFraud)
                        Else
                            RH.AddColumn(TRTotal, TRTotal_02, 10, 10, "c", "-")
                        End If
                        If (TotSuspLeakage > 0) Then
                            RH.AddColumn(TRTotal, TRTotal_03, 10, 10, "c", TotSuspLeakage)
                        Else
                            RH.AddColumn(TRTotal, TRTotal_03, 10, 10, "c", "-")
                        End If
                        If (TotGeneral > 0) Then
                            RH.AddColumn(TRTotal, TRTotal_04, 10, 10, "c", TotGeneral)
                        Else
                            RH.AddColumn(TRTotal, TRTotal_04, 10, 10, "c", "-")
                        End If
                        If (TotGeneral > 0) Then
                            RH.AddColumn(TRTotal, TRTotal_05, 10, 10, "c", Total)
                        Else
                            RH.AddColumn(TRTotal, TRTotal_05, 10, 10, "c", "-")
                        End If
                        If (TotLeakage > 0) Then
                            RH.AddColumn(TRTotal, TRTotal_06, 10, 10, "c", TotLeakage.ToString("####,##,###.00"))
                        Else
                            RH.AddColumn(TRTotal, TRTotal_06, 10, 10, "c", "-")
                        End If

                        tb.Controls.Add(TRTotal)
                        TotSuspFraud = 0
                        TotSuspLeakage = 0
                        TotGeneral = 0
                        Total = 0
                        TotLeakage = 0
                    End If
                    Dim TRSUBHD As New TableRow
                    Dim TRSUBHD_00 As New TableCell
                    TRSUBHD.Style.Add("Font-Weight", "Bold")
                    TRSUBHD_00.BorderWidth = "1"
                    TRSUBHD_00.BorderColor = Drawing.Color.Silver
                    TRSUBHD_00.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TRSUBHD, TRSUBHD_00, 100, 100, "c", DR(8))
                    TRSUBHD.Style.Add("background-color", "silver")
                    tb.Controls.Add(TRSUBHD)
                End If
                AuditType = CInt(DR(7))
                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02, TR01_03, TR01_04, TR01_05, TR01_06 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"
                TR01_03.BorderWidth = "1"
                TR01_04.BorderWidth = "1"
                TR01_05.BorderWidth = "1"
                TR01_06.BorderWidth = "1"

                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver
                TR01_03.BorderColor = Drawing.Color.Silver
                TR01_04.BorderColor = Drawing.Color.Silver
                TR01_05.BorderColor = Drawing.Color.Silver
                TR01_06.BorderColor = Drawing.Color.Silver

                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid
                TR01_03.BorderStyle = BorderStyle.Solid
                TR01_04.BorderStyle = BorderStyle.Solid
                TR01_05.BorderStyle = BorderStyle.Solid
                TR01_06.BorderStyle = BorderStyle.Solid

                I = I + 1
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR01, TR01_01, 45, 45, "l", DR(1))
                If (CInt(DR(2)) > 0) Then
                    If AuditType <> 3 Then
                        Dim str As String = "<a href='viewAuditAbstractReport_1.aspx?StatusID=1 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(2).ToString()
                        RH.AddColumn(TR01, TR01_02, 10, 10, "c", str)
                    Else
                        Dim str As String = "<a href='viewAuditAbstractReport_2.aspx?StatusID=1 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(2).ToString()
                        RH.AddColumn(TR01, TR01_02, 10, 10, "c", str)
                    End If
                Else
                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", "-")
                End If
                If (CInt(DR(3)) > 0) Then
                    If AuditType <> 3 Then
                        Dim str As String = "<a href='viewAuditAbstractReport_1.aspx?StatusID=2 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(3).ToString()
                        RH.AddColumn(TR01, TR01_03, 10, 10, "c", str)
                    Else
                        Dim str As String = "<a href='viewAuditAbstractReport_2.aspx?StatusID=2 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(3).ToString()
                        RH.AddColumn(TR01, TR01_03, 10, 10, "c", str)
                    End If
                Else
                    RH.AddColumn(TR01, TR01_03, 10, 10, "c", "-")
                End If

                If (CInt(DR(4)) > 0) Then
                    If AuditType <> 3 Then
                        Dim str As String = "<a href='viewAuditAbstractReport_1.aspx?StatusID=3 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(4).ToString()
                        RH.AddColumn(TR01, TR01_04, 10, 10, "c", str)
                    Else
                        Dim str As String = "<a href='viewAuditAbstractReport_2.aspx?StatusID=3 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(4).ToString()
                        RH.AddColumn(TR01, TR01_04, 10, 10, "c", str)
                    End If
                Else
                    RH.AddColumn(TR01, TR01_04, 10, 10, "c", "-")
                End If

                If (CInt(DR(5)) > 0) Then
                    If AuditType <> 3 Then
                        Dim str As String = "<a href='viewAuditAbstractReport_1.aspx?StatusID=0 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(5).ToString()
                        RH.AddColumn(TR01, TR01_05, 10, 10, "c", str)
                    Else
                        Dim str As String = "<a href='viewAuditAbstractReport_2.aspx?StatusID=0 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(5).ToString()
                        RH.AddColumn(TR01, TR01_05, 10, 10, "c", str)
                    End If
                Else
                    RH.AddColumn(TR01, TR01_05, 10, 10, "c", "-")
                End If

                If (CInt(DR(6)) > 0) Then
                    If AuditType <> 3 Then
                        Dim str As String = "<a href='viewAuditAbstractReport_1.aspx?StatusID=4 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + CDbl(DR(6)).ToString("####,##,###.00")
                        RH.AddColumn(TR01, TR01_06, 10, 10, "c", str)
                    Else
                        Dim str As String = "<a href='viewAuditAbstractReport_2.aspx?StatusID=4 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "  &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + CDbl(DR(6)).ToString("####,##,###.00")
                        RH.AddColumn(TR01, TR01_06, 10, 10, "c", str)
                    End If
                Else
                    RH.AddColumn(TR01, TR01_06, 10, 10, "c", "-")
                End If

                tb.Controls.Add(TR01)
                TotSuspFraud += CInt(DR(2))
                GrandSuspFraud += CInt(DR(2))
                TotSuspLeakage += CInt(DR(3))
                GrandSuspLeakage += CInt(DR(3))
                TotGeneral += CInt(DR(4))
                GrandGeneral += CInt(DR(4))
                Total += CInt(DR(5))
                GrandTotal += CInt(DR(5))
                TotLeakage += CDbl(DR(6))
                GrandLeakage += CDbl(DR(6))

            Next
            Dim TRTot As New TableRow
            Dim TRTot_00, TRTot_01, TRTot_02, TRTot_03, TRTot_04, TRTot_05, TRTot_06 As New TableCell
            TRTot.Style.Add("Font-Weight", "Bold")

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"
            TRTot_03.BorderWidth = "1"
            TRTot_04.BorderWidth = "1"
            TRTot_05.BorderWidth = "1"
            TRTot_06.BorderWidth = "1"

            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver
            TRTot_03.BorderColor = Drawing.Color.Silver
            TRTot_04.BorderColor = Drawing.Color.Silver
            TRTot_05.BorderColor = Drawing.Color.Silver
            TRTot_06.BorderColor = Drawing.Color.Silver

            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid
            TRTot_03.BorderStyle = BorderStyle.Solid
            TRTot_04.BorderStyle = BorderStyle.Solid
            TRTot_05.BorderStyle = BorderStyle.Solid
            TRTot_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRTot, TRTot_00, 5, 5, "c", "")
            RH.AddColumn(TRTot, TRTot_01, 45, 45, "r", "Total&nbsp;&nbsp;")
            If (TotSuspFraud > 0) Then
                RH.AddColumn(TRTot, TRTot_02, 10, 10, "c", TotSuspFraud)
            Else
                RH.AddColumn(TRTot, TRTot_02, 10, 10, "c", "-")
            End If
            If (TotSuspLeakage > 0) Then
                RH.AddColumn(TRTot, TRTot_03, 10, 10, "c", TotSuspLeakage)
            Else
                RH.AddColumn(TRTot, TRTot_03, 10, 10, "c", "-")
            End If
            If (TotGeneral > 0) Then
                RH.AddColumn(TRTot, TRTot_04, 10, 10, "c", TotGeneral)
            Else
                RH.AddColumn(TRTot, TRTot_04, 10, 10, "c", "-")
            End If
            If (TotGeneral > 0) Then
                RH.AddColumn(TRTot, TRTot_05, 10, 10, "c", Total)
            Else
                RH.AddColumn(TRTot, TRTot_05, 10, 10, "c", "-")
            End If
            If (TotLeakage > 0) Then
                RH.AddColumn(TRTot, TRTot_06, 10, 10, "c", TotLeakage.ToString("####,##,###.00"))
            Else
                RH.AddColumn(TRTot, TRTot_06, 10, 10, "c", "-")
            End If
            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)
            Dim TRFooter As New TableRow
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06 As New TableCell
            TRFooter.Style.Add("Font-Weight", "Bold")

            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"
            TRFooter_05.BorderWidth = "1"
            TRFooter_06.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver
            TRFooter_05.BorderColor = Drawing.Color.Silver
            TRFooter_06.BorderColor = Drawing.Color.Silver

            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid
            TRFooter_05.BorderStyle = BorderStyle.Solid
            TRFooter_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 45, 45, "r", "Grand Total&nbsp;&nbsp;")
            If GrandSuspFraud > 0 Then
                RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", GrandSuspFraud)
            Else
                RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", "-")
            End If

            If GrandSuspLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "c", GrandSuspLeakage)
            Else
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "c", "-")
            End If

            If GrandGeneral > 0 Then
                RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "c", GrandGeneral)
            Else
                RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "c", "-")
            End If

            If GrandTotal > 0 Then
                RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "c", GrandTotal)
            Else
                RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "c", "-")
            End If

            If GrandSuspLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "c", GrandLeakage.ToString("####,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "c", "-")
            End If

            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
