﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewMisleadingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim AuditID As String
    Dim CloseFlag As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim SubHD As String
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            AuditID = GN.Decrypt(Request.QueryString.Get("AuditID"))

            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim AuditDtl() As String
            AuditDtl = AuditID.Split("~")
            Dim MonthID As Integer
            Dim YearID As Integer
            If AuditDtl(0) = -1 Then
                If (BranchID > 0) Then
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                ElseIf AreaID > 0 Then
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                ElseIf RegionID > 0 Then
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                Else
                    SubHD = " OBSERVATION CONFIRMATION REPORT "
                End If
            Else
                MonthID = CInt(AuditDtl(0))
                YearID = CInt(AuditDtl(1))
                If (BranchID > 0) Then
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                ElseIf AreaID > 0 Then
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                ElseIf RegionID > 0 Then
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                Else
                    SubHD = " OBSERVATION CONFIRMATION REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString()
                End If
            End If
            RH.Heading(Session("FirmName"), tb, SubHD, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable

            DT = AD.GetObservationConfirmationDetail(MonthID, YearID, RegionID, AreaID, BranchID, 0, FromDt, ToDt, 0, 0, CInt(Session("UserID")))
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            'TRHead_04.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            'TRHead_04.BorderColor = Drawing.Color.Silver



            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            'TRHead_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 65, 65, "l", "Region Name")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "Satisfied")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Misleading")
            'RH.AddColumn(TRHead, TRHead_04, 10, 10, "c", "Pending")


            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            Dim AuditType As Integer = 0
            Dim TotSatisfied As Integer = 0
            Dim TotMisleading As Integer = 0


            Dim GrandTotal As Integer = 0
            Dim GrandSatisfied As Integer = 0
            Dim GrandMisleading As Integer = 0


            For Each DR In DT.Rows


                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02, TR01_03, TR01_04, TR01_05, TR01_06 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"
                TR01_03.BorderWidth = "1"
                'TR01_04.BorderWidth = "1"


                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver
                TR01_03.BorderColor = Drawing.Color.Silver

                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid
                TR01_03.BorderStyle = BorderStyle.Solid

                I = I + 1
                Dim Str As String
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                Str = "<a href='viewMisleadingReport_area.aspx?StatusID=0 &DetailFlag=0 &AuditID=" + GN.Encrypt(AuditID.ToString) + " &RegionID=" + GN.Encrypt(DR(0)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(1).ToString()
                RH.AddColumn(TR01, TR01_01, 65, 65, "l", Str)
                If (CInt(DR(2)) > 0) Then
                    Str = "<a href='viewMisleadingDetails.aspx?StatusID=0 &DetailFlag=1 &AuditID=" + GN.Encrypt(AuditID.ToString) + " &RegionID=" + GN.Encrypt(DR(0)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(2).ToString()
                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", Str)
                Else
                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", "-")
                End If
                If (CInt(DR(3)) > 0) Then
                    Str = "<a href='viewMisleadingDetails.aspx?StatusID=1 &DetailFlag=1 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(DR(0)) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + DR(3).ToString()
                    RH.AddColumn(TR01, TR01_03, 10, 10, "c", Str)
                Else
                    RH.AddColumn(TR01, TR01_03, 10, 10, "c", "-")
                End If
                tb.Controls.Add(TR01)
                TotSatisfied += CInt(DR(2))
                GrandSatisfied += CInt(DR(2))
                TotMisleading += CInt(DR(3))
                GrandMisleading += CInt(DR(3))
                Total += CInt(DR(4))
                GrandTotal += CInt(DR(4))

            Next
            Dim TRTot As New TableRow
            Dim TRTot_00, TRTot_01, TRTot_02, TRTot_03, TRTot_04, TRTot_05, TRTot_06 As New TableCell
            TRTot.Style.Add("Font-Weight", "Bold")

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"
            TRTot_03.BorderWidth = "1"
            'TRTot_04.BorderWidth = "1"


            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver
            TRTot_03.BorderColor = Drawing.Color.Silver
            'TRTot_04.BorderColor = Drawing.Color.Silver


            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid
            TRTot_03.BorderStyle = BorderStyle.Solid
            'TRTot_04.BorderStyle = BorderStyle.Solid


            Dim Str1 As String

            RH.AddColumn(TRTot, TRTot_00, 5, 5, "c", "")
            RH.AddColumn(TRTot, TRTot_01, 65, 65, "r", "Total&nbsp;&nbsp;")
            If (TotSatisfied > 0) Then
                Str1 = "<a href='viewMisleadingDetails.aspx?StatusID=0 &DetailFlag=1 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + TotSatisfied.ToString()
                RH.AddColumn(TRTot, TRTot_02, 10, 10, "c", Str1)
            Else
                RH.AddColumn(TRTot, TRTot_02, 10, 10, "c", "-")
            End If
            If (TotMisleading > 0) Then
                Str1 = "<a href='viewMisleadingDetails.aspx?StatusID=1 &DetailFlag=1 &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' style='text-align:right;' target='_blank'>" + TotMisleading.ToString()
                RH.AddColumn(TRTot, TRTot_03, 10, 10, "c", Str1)
            Else
                RH.AddColumn(TRTot, TRTot_03, 10, 10, "c", "-")
            End If

            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
