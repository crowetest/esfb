﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewConsolidatedAuditReportLoanWise_Gen
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim CloseFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            AuditDtl = Request.QueryString.Get("AuditID")
            Dim ObservationID As Integer = CInt(Request.QueryString.Get("ObservationID"))
            Dim StatusID As Integer = CInt(Request.QueryString.Get("StatusID"))
            Me.hdnBranchID.Value = Request.QueryString.Get("BranchID")
            Dim ReportID As Integer = CInt(Request.QueryString.Get("ReportID"))
            CloseFlag = CInt(Request.QueryString.Get("CloseFlag"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Me.hdnReportID.Value = ReportID.ToString()
            Me.hdnAuditID.Value = AuditDtl
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim BranchName As String
            Dim ItemName As String = ""
            DT = AD.GetItemName(0, ObservationID)
            If DT.Rows.Count > 0 Then
                ItemName = DT.Rows(0)(0)
            End If
            BranchName = GN.GetBranch_Name(CInt(Me.hdnBranchID.Value))
            RH.Heading(Session("FirmName"), tb, "Observation List  " + BranchName + " Branch", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim SubHD As String = ""
            If (CloseFlag = 1) Then
                SubHD = "Status - Pending Operations"
            ElseIf CloseFlag = 5 Then
                SubHD = "Status - Pending Auditors"
            ElseIf CloseFlag = 2 Then
                SubHD = "Status - Pending"
            ElseIf CloseFlag = 0 Then
                SubHD = "Status - Closed"
            End If
            RH.SubHeading(tb, 100, "l", SubHD)
            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No")
            RH.AddColumn(TRHead, TRHead_01, 45, 45, "l", "CheckList")
            RH.AddColumn(TRHead, TRHead_02, 40, 40, "l", "Last Remarks")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Est.Amt. Involved")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)

            Dim TotLeakage As Double = 0

            DT = AD.GetConsolidatedAuditReportLoanWise_Gen(ObservationID, StatusID, CloseFlag)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 45, 45, "r", ItemName)
                RH.AddColumn(TR3, TR3_02, 40, 40, "l", DR(0).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(2)) + "' style='text-align:right;' target='_blank' >Previous Remarks</a>")
                If DR(1) = 0 Then
                    RH.AddColumn(TR3, TR3_03, 10, 10, "r", "-")
                Else
                    RH.AddColumn(TR3, TR3_03, 10, 10, "r", CDbl(DR(1)).ToString("0.00"))
                End If
                tb.Controls.Add(TR3)
                TotLeakage += CDbl(DR(1))
            Next
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver


            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 45, 45, "l", "Total")
            RH.AddColumn(TRFooter, TRFooter_02, 40, 40, "c", "")
            If TotLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "r", TotLeakage.ToString("###,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "r", "-")
            End If

            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 20)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
