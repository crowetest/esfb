﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewAuditSummaryReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim AD As New Audit
    Dim AsOn As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim Type As Integer = CInt(Request.QueryString.Get("Type"))
            Dim DayType As Integer = CInt(Request.QueryString.Get("DayType"))
            Dim Days As Integer = CInt(Request.QueryString.Get("Days"))
            Dim OptGreat As Integer = CInt(Request.QueryString.Get("OptGreat"))
            Dim head As String = CStr(Request.QueryString.Get("head"))
            Dim DetailSearch As Integer = CInt(Request.QueryString.Get("DetailSearch"))
            AsOn = CStr(Request.QueryString.Get("AsOn"))

            Dim REGID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("REGID")))
            Dim BRANCHID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("BRANCHID")))
            Dim AuditID As String = CStr(GF.Decrypt(Request.QueryString.Get("AuditID")))

            Dim DTHead As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0



            RH.Heading(Session("FirmName"), tb, "AUDITWISE DETAILED AGING REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            If REGID > 0 Then
                DTAudit = DB.ExecuteDataSet("SELECT UPPER(REGION_name) FROM REGION_MASTER where REGION_id=" + REGID.ToString()).Tables(0)



                Dim TRItemHead2 As New TableRow
                TRItemHead2.ForeColor = Drawing.Color.DarkBlue
                Dim TRItemHead2_00 As New TableCell

                RH.AddColumn(TRItemHead2, TRItemHead2_00, 100, 100, "l", "<b><i>REGION  :-     " & DTAudit.Rows(0)(0).ToString() & " </i></b>")

                tb.Controls.Add(TRItemHead2)
            End If

            If head <> "" Then

                Dim TRItemHead3 As New TableRow
                TRItemHead3.ForeColor = Drawing.Color.DarkBlue
                Dim TRItemHead3_00 As New TableCell

                RH.AddColumn(TRItemHead3, TRItemHead3_00, 100, 100, "l", "<b><i>Aging Bucket  :-     " & head.ToString() & " </i></b>")

                tb.Controls.Add(TRItemHead3)


                Dim TRItemHead As New TableRow
                TRItemHead.ForeColor = Drawing.Color.DarkBlue
                Dim TRItemHead_00 As New TableCell
                RH.BlankRow(tb, 5)

            End If
            '-----------------
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03 As New TableCell

            TRHead1_00.BorderWidth = "1"
            TRHead1_01.BorderWidth = "1"
            TRHead1_02.BorderWidth = "1"
            TRHead1_03.BorderWidth = "1"

            TRHead1_00.BorderColor = Drawing.Color.Silver
            TRHead1_01.BorderColor = Drawing.Color.Silver
            TRHead1_02.BorderColor = Drawing.Color.Silver
            TRHead1_03.BorderColor = Drawing.Color.Silver

            TRHead1_00.BorderStyle = BorderStyle.Solid
            TRHead1_01.BorderStyle = BorderStyle.Solid
            TRHead1_02.BorderStyle = BorderStyle.Solid
            TRHead1_03.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead1, TRHead1_00, 10, 10, "l", "")
            RH.AddColumn(TRHead1, TRHead1_01, 20, 20, "l", "")
            RH.AddColumn(TRHead1, TRHead1_02, 50, 50, "l", "")
            RH.AddColumn(TRHead1, TRHead1_03, 20, 20, "c", "<b>No Of Pending Observations</b>")
            tb.Controls.Add(TRHead1)

            '-----------------
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Branch")
            'DTHead = AD.AGING_AUDITWISE_REPORT(Type, "B", DayType, OptGreat, Days, AREAID, 0, 1, 0, AuditID)
            'If DTHead.Rows.Count > 0 Then
            RH.AddColumn(TRHead, TRHead_02, 50, 50, "l", "Audit")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "c", "Total Count")
            'RH.AddColumn(TRHead, TRHead_03, 12, 12, "l", DTHead.Rows(0).Item(1))
            'RH.AddColumn(TRHead, TRHead_04, 12, 12, "l", DTHead.Rows(0).Item(2))
            'RH.AddColumn(TRHead, TRHead_05, 12, 12, "l", DTHead.Rows(0).Item(3))
            'RH.AddColumn(TRHead, TRHead_06, 12, 12, "l", DTHead.Rows(0).Item(4))
            'RH.AddColumn(TRHead, TRHead_07, 12, 12, "c", "<b>Total</b>")
            'End If


            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)

            DT = AD.AGING_AUDITWISE_REPORT(Type, "TT", DayType, OptGreat, Days, REGID, DetailSearch, 0, BRANCHID, AuditID, CInt(Session("UserID")), CInt(Session("Post_ID")), , AsOn)
            Dim Total1 As Integer = 0

            For Each DR In DT.Rows

                j += 1

                I = 1

                Dim h_Total As Integer = 0
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid


                TR3_03.BackColor = Drawing.Color.WhiteSmoke

                Total1 += DR(3)

                '"<a href='SRStatusDetail.aspx?StatusID=" + DR(2).ToString() + " &Status=" + DR(0) + "'>" + DR(0) + "</a>"
                RH.AddColumn(TR3, TR3_00, 10, 10, "l", DR(0))
                RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(4))
                RH.AddColumn(TR3, TR3_02, 50, 50, "l", DR(2))
                Dim val As String
                val = IIf(DR(3) > 0, "<a href='ViewDetailSummaryBucketAuditWiseReport.aspx?AsOn=" + AsOn + " &AuditID=" + GF.Encrypt(AuditID.ToString) + "&GROUPID=" + CInt(DR(1)).ToString() + " &head=" + head.ToString + "&branch_id=" + GF.Encrypt(DR(5).ToString()) + "&OBSERVATIONID=0 &Type=" + Type.ToString() + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + " &OptGreat=" + OptGreat.ToString() + " &DetailSearch=" + DetailSearch.ToString + "' target='_blank'>" + DR(3).ToString + "</a>", DR(3).ToString)
                RH.AddColumn(TR3, TR3_03, 20, 20, "c", val)

                tb.Controls.Add(TR3)
                I = I + 1

            Next


            Dim TRTot As New TableRow
            TRTot.BorderWidth = "1"
            TRTot.BorderStyle = BorderStyle.Solid
            TRTot.Style.Add("font-weight", "bold")
            TRTot.Style.Add("background-color", "whitesmoke")
            Dim TRTot_00, TRTot_01, TRTot_02, TRTot_03 As New TableCell

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"
            TRTot_03.BorderWidth = "1"


            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver
            TRTot_03.BorderColor = Drawing.Color.Silver


            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid
            TRTot_03.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRTot, TRTot_00, 10, 10, "l", "")
            RH.AddColumn(TRTot, TRTot_01, 20, 20, "l", "")
            RH.AddColumn(TRTot, TRTot_02, 50, 50, "r", "Total")
            RH.AddColumn(TRTot, TRTot_03, 20, 20, "c", Total1.ToString())


            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)
            Dim TR_Note As New TableRow
            Dim TR_Note_00 As New TableCell
            ' TR_Note.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_Note, TR_Note_00, 100, 100, "l", "* Both Days Inclusive")
            tb.Controls.Add(TR_Note)
            RH.BlankRow(tb, 5)
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
