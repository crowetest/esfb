﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewVisitedBranchDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim MonthDesc As String
    Dim subHead As String = ""
    Dim FromDt As Date
    Dim ToDt As Date
    Dim RptID As Integer
    Dim BranchID As Integer
    Dim Emp_Code As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            MonthDesc = Request.QueryString.Get("AssignDate")
            FromDt = Request.QueryString.Get("FromDt")
            ToDt = Request.QueryString.Get("ToDt")
            RptID = CInt(Request.QueryString.Get("RptID"))

            Emp_Code = CInt(Request.QueryString.Get("Emp_Code"))
            Dim AssignDtl() As String = MonthDesc.Split("~")

            hdnAssingDt.Value = MonthDesc.ToString()
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            subHead = Request.QueryString.Get("SubHD")
            RH.Heading(Session("FirmName"), tb, subHead, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 15, 0, "Branch ID")
            RH.InsertColumn(TRHead, TRHead_02, 55, 0, "Branch Name")
            RH.InsertColumn(TRHead, TRHead_03, 25, 2, "Sangam Count")


            tb.Controls.Add(TRHead)
            RH.BlankRow(tb, 5)
            If RptID = 1 Then


                'DT = DB.ExecuteDataSet("select distinct d.CENTER_ID,d.CENTER_NAME,DATENAME(DW,d.MEETING_DAY-2)as MeetingDay,c.visited_dt from AUDIT_master a,audit_visited_sangam_master b," & _
                '            " audit_visited_sangam_dtl c, CENTER_MASTER  d where a.group_id=b.audit_group_id and b.visited_id=c.visited_id and c.group_id=d.center_id and a.group_ID=" + AuditID.ToString() + " and c.emp_code=" & Emp_Code.ToString() & "").Tables(0)
                DT = DB.ExecuteDataSet("select COUNT(c.GROUP_ID) as Count,c.visited_id,a.group_id,f.branch_name,f.branch_id from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                                     " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and month(a.period_to) =" + AssignDtl(0) + " and year(a.period_to)=" + AssignDtl(1) + " and c.emp_code=" & Emp_Code.ToString() & "   group by c.visited_id,a.group_id,f.branch_name,f.branch_id").Tables(0)
            Else

                'DT = DB.ExecuteDataSet("select distinct d.CENTER_ID,d.CENTER_NAME,DATENAME(DW,d.MEETING_DAY-2)as MeetingDay,c.visited_dt from AUDIT_master a,audit_visited_sangam_master b," & _
                '                       " audit_visited_sangam_dtl c, CENTER_MASTER  d where a.group_id=b.audit_group_id and b.visited_id=c.visited_id and c.group_id=d.center_id and a.group_ID=" + AuditID.ToString() + "  and c.emp_code=" & Emp_Code.ToString() & " and DATEADD(day, DATEDIFF(day, 0, c.visited_dt), 0) between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'").Tables(0)
                DT = DB.ExecuteDataSet("select COUNT(c.GROUP_ID) as Count,c.visited_id,a.group_id,f.branch_name,f.branch_id from AUDIT_MASTER a,branch_master f,AUDIT_VISITED_SANGAM_MASTER b,AUDIT_VISITED_SANGAM_DTL c,emp_master d " & _
                                     " where a.branch_id=f.branch_id and a.GROUP_ID=b.AUDIT_GROUP_ID and b.VISITED_ID=c.VISITED_ID and c.emp_code=d.emp_code and DATEADD(day, DATEDIFF(day, 0, c.visited_dt), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'  and c.emp_code=" & Emp_Code.ToString() & "   group by c.visited_id,a.group_id,f.branch_name,f.branch_id").Tables(0)
            End If



            For Each DR In DT.Rows


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell
                I = I + 1
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString().ToString())
                RH.InsertColumn(TR3, TR3_01, 15, 0, DR(4).ToString())
                RH.InsertColumn(TR3, TR3_02, 55, 0, DR(3).ToString())
                RH.InsertColumn(TR3, TR3_03, 25, 2, " <a href='ViewVisitedCenters.aspx?AuditID=" + GF.Encrypt(DR(2)) + " &AssignDate=" + MonthDesc.ToString() + " &SubHD=Branch - " + DR(3).ToString() + "(" + DR(4).ToString() + ")" + "   " + subHead.ToString() + " &RptID=" + RptID.ToString() + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + " &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &BranchID=" + GF.Encrypt(DR(4)) + " &Emp_Code=" + Emp_Code.ToString() + "' target='_blank'/>" + DR(0).ToString())


                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
