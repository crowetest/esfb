﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewObservationPendingQueue_1
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim LevelID As Integer
    Dim Role As String
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))
            LevelID = CInt(Request.QueryString.Get("LevelID"))
            Role = Request.QueryString.Get("Role")
            Dim LevelName As String = ""
            LevelName = AD.GetLevelName(LevelID)
            RH.Heading(Session("FirmName"), tb, Role.ToUpper() + " PENDING QUEUE " + LevelName + " Level", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = DB.ExecuteDataSet("select d.BRANCH_ID,e.Branch_Name,a.role_id ,COUNT(a.LEVEL_ID) as Cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL d,BRANCH_MASTER e,AUDIT_OBSERVATION_STATUS_MASTER c where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=d.AUDIT_ID and d.BRANCH_ID=e.branch_id and a.role_id=c.Status_ID and a.STATUS_ID>0 and a.LAST_UPDATED_DT is not null and a.LEVEL_ID=" + LevelID.ToString() + " and c.Description='" + Role + "' group by d.BRANCH_ID,e.Branch_Name,a.role_id").Tables(0)
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 60, 60, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 35, 35, "c", "Count")

            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            For Each DR In DT.Rows
                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"

                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver

                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid
                I = I + 1
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR01, TR01_01, 60, 60, "l", DR(1))

                Dim str As String = "<a href='viewObservationPendingQueue_2.aspx?LevelID=" + LevelID.ToString() + " &Role=" + Role + " &BranchID=" + GF.Encrypt(DR(0)) + "'>" + DR(3).ToString()
                RH.AddColumn(TR01, TR01_02, 35, 35, "c", str)
                tb.Controls.Add(TR01)
                Total += CInt(DR(3))
            Next
            Dim TRHead_End As New TableRow
            TRHead_End.Style.Add("Font-Weight", "Bold")
            Dim TRHead_End_00, TRHead_End_01 As New TableCell
            TRHead_End_00.BorderWidth = "1"
            TRHead_End_01.BorderWidth = "1"


            TRHead_End_00.BorderColor = Drawing.Color.Silver
            TRHead_End_01.BorderColor = Drawing.Color.Silver


            TRHead_End_00.BorderStyle = BorderStyle.Solid
            TRHead_End_01.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead_End, TRHead_End_00, 65, 65, "c", "Total")
            RH.AddColumn(TRHead_End, TRHead_End_01, 35, 35, "c", Total)

            tb.Controls.Add(TRHead_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
