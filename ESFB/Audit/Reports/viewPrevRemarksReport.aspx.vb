﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewPrevRemarksReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim From As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            AuditID = CInt(GF.Decrypt(Request.QueryString.Get("AuditID")))
            hdnAuditID.Value = AuditID.ToString()
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            tb.Attributes.Add("width", "100%")

            DT = DB.ExecuteDataSet("select c.Emp_Code,c.Emp_Name,case when a.update_id=20 then e.department_name else b.level_name end ,a.remarks,a.TRA_DT,d.description from AUDIT_OBSERVATION_CYCLE a, " & _
                                   " AUDIT_USER_LEVELS b,EMP_MASTER c,audit_update_master d,department_master e " & _
                                   " where a.level_id=b.level_id  " & _
            " and a.user_id=c.Emp_Code and a.update_id=d.update_id and c.department_id=e.department_id and a.sino=" + AuditID.ToString() + "  order by a.order_ID").Tables(0)

            RH.Heading(Session("FirmName"), tb, "AUDIT OBSERVATION -PREVIOUS REMARKS REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim I As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No")
            RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "Emp.Code")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Name")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "Post")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "l", "Date")
            RH.AddColumn(TRHead, TRHead_05, 25, 25, "l", "Remarks")
            RH.AddColumn(TRHead, TRHead_06, 15, 15, "l", "Status")
            tb.Controls.Add(TRHead)
            Dim LoanDate As String
            For Each DR In DT.Rows
                I = I + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid


                If Not IsDBNull(DR(4)) Then
                    LoanDate = CDate(DR(4)).ToString("dd/MM/yyyy")
                Else
                    LoanDate = ""
                End If
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString())
                If (CInt(DR(0)) = 0) Then
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", "")
                    RH.AddColumn(TR3, TR3_02, 20, 20, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_01, 10, 10, "l", DR(0))
                    RH.AddColumn(TR3, TR3_02, 20, 20, "l", DR(1))
                End If
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2))
                RH.AddColumn(TR3, TR3_04, 10, 10, "l", LoanDate)
                RH.AddColumn(TR3, TR3_05, 25, 25, "l", DR(3))
                RH.AddColumn(TR3, TR3_06, 15, 15, "l", DR(5))


                tb.Controls.Add(TR3)

            Next




            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


End Class
