﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewAuditsAndBranch
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim TypeID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim SPMFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim SpotClose As String = ""
    Dim GN As New GeneralFunctions
    Dim RptID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RptID = CInt(Request.QueryString.Get("RptID"))
            TypeID = CInt(Request.QueryString.Get("TypeID"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            SPMFlag = Request.QueryString.Get("SPMFlag")

            RegionID = GN.Decrypt(Request.QueryString.Get("RegionID"))
            AreaID = GN.Decrypt(Request.QueryString.Get("AreaID"))
            BranchID = GN.Decrypt(Request.QueryString.Get("BranchID"))

            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            tb.Attributes.Add("width", "100%")
            Dim hdStr As String
            If TypeID = 1 Then
                If RptID = 1 Then
                    hdStr = "AUDITED BRANCHES DURING THE PERIOD " + FromDt.ToString("dd MMM yyyy") + " AND " + ToDt.ToString("dd MMM yyyy")
                Else
                    hdStr = "AUDITS ENDED DURING THE PERIOD " + FromDt.ToString("dd MMM yyyy") + " AND " + ToDt.ToString("dd MMM yyyy")
                End If
            Else
                If RptID = 1 Then
                    hdStr = "COMPLETED BRANCHES PLANNED DURING THE PERIOD " + FromDt.ToString("dd MMM yyyy") + " AND " + ToDt.ToString("dd MMM yyyy")
                Else
                    hdStr = "AUDITS DURING THE PERIOD " + FromDt.ToString("dd MMM yyyy") + " AND " + ToDt.ToString("dd MMM yyyy")
                End If
            End If



            If BranchID > 0 Then
                hdStr += " " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
            ElseIf AreaID > 0 Then
                hdStr += " " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
            ElseIf RegionID > 0 Then
                hdStr += " " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
            End If
            If SPMFlag <> 0 Then
                hdStr += " (SPM RELATED)"
            End If

            RH.Heading(Session("FirmName"), tb, hdStr, 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim sqlStr As String
            Dim sqlStr1 As String = ""
            If RptID = 1 Then
                If BranchID > 0 Then
                    sqlStr1 += " and i.branch_id=" + BranchID.ToString()
                ElseIf AreaID > 0 Then
                    sqlStr1 += " and i.area_id=" + AreaID.ToString()
                ElseIf RegionID > 0 Then
                    sqlStr1 += " and i.region_id=" + RegionID.ToString()
                End If
                If TypeID = 1 Then
                    sqlStr1 += " and DATEADD(day, DATEDIFF(day, 0, j.END_DT), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'"
                Else
                    sqlStr1 += " and j.period_to between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'"
                End If
                If SPMFlag = 1 Then
                    sqlStr1 += " and e.SPM_Flag=1"
                End If

                sqlStr = "select i.branch_id,I.Branch_Name,COUNT(DISTINCT J.GROUP_ID)AS NoOfAudits,convert(varchar,DATENAME(month,MAX(DATEADD(day, DATEDIFF(day, 0, j.period_to), 0))))+'-'+ convert(varchar,year(MAX(DATEADD(day, DATEDIFF(day, 0, j.period_to), 0)))),nm.Resources " & _
                    " from AUDIT_OBSERVATION a , AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c " & _
                    " ,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,AUDIT_DTL h,BrMaster i,audit_Master j " & _
                    " left join (SELECT distinct   r.branch_id, Resources =  STUFF( (SELECT ','+ar.Name FROM (select i.branch_id,I.Branch_Name, " & _
                    " convert(varchar,DATENAME(month,PERIOD_TO))+'-'+ convert(varchar,year(PERIOD_TO)) as Name from AUDIT_MASTER j ,BrMaster i " & _
                    " where   j.BRANCH_ID=i.Branch_ID and j.status_id in (0,9) and SKIP_FLAG<>1 ) ar " & _
                    " WHERE ar.BRANCH_ID = r.BRANCH_ID  " & _
                    " FOR XML PATH('')), 1, 1, '') " & _
                    " FROM " & _
                    " ( select i.branch_id,I.Branch_Name,j.GROUP_ID   from AUDIT_MASTER j ,BrMaster i " & _
                    " where   j.BRANCH_ID=i.Branch_ID and j.status_id in (0,9) and SKIP_FLAG<>1 ) r)nm on j.branch_id=nm.branch_id " & _
                    " where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID " & _
                    " and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID  and a.AUDIT_ID=h.AUDIT_ID and h.BRANCH_ID=i.Branch_ID  and h.group_id=j.group_id and j.status_id=2 " & sqlStr1.ToString & " "

                sqlStr += " group by i.branch_id,I.Branch_Name,nm.Resources order  by I.Branch_Name"
                DT = DB.ExecuteDataSet(sqlStr).Tables(0)
                Dim TRHead As New TableRow
                Dim I As Integer
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell
                RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
                RH.InsertColumn(TRHead, TRHead_01, 35, 0, "Branch")
                RH.InsertColumn(TRHead, TRHead_02, 10, 2, "No Of Audits")
                RH.InsertColumn(TRHead, TRHead_03, 10, 2, "Review Period Completed")
                RH.InsertColumn(TRHead, TRHead_04, 40, 2, "Audits Due")
                tb.Controls.Add(TRHead)
                I = 1
                For Each DR In DT.Rows
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell

                    RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                    RH.InsertColumn(TR3, TR3_01, 35, 0, DR(1).ToString())
                    RH.InsertColumn(TR3, TR3_02, 10, 2, DR(2))
                    RH.InsertColumn(TR3, TR3_03, 10, 0, DR(3).ToString())
                    RH.InsertColumn(TR3, TR3_04, 40, 0, DR(4).ToString())
                    tb.Controls.Add(TR3)
                    I = I + 1
                Next
                RH.BlankRow(tb, 20)
                pnDisplay.Controls.Add(tb)
            Else
                sqlStr = "select distinct  i.branch_id,I.Branch_Name,I.Branch_Name+'-'+ datename(MONTH,j.period_to) +'-'+CAST(YEAR(j.period_to) AS VARCHAR(4)) ,j.PERIOD_FROM,j.PERIOD_TO,j.START_DT ,j.END_DT,j.GROUP_ID   from AUDIT_OBSERVATION a , AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,AUDIT_DTL h,BrMaster i,audit_Master j where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and a.AUDIT_ID=h.AUDIT_ID and h.BRANCH_ID=i.Branch_ID  and h.group_id=j.group_id and j.status_id=2  "
                If BranchID > 0 Then
                    sqlStr += " and i.branch_id=" + BranchID.ToString()
                ElseIf AreaID > 0 Then
                    sqlStr += " and i.area_id=" + AreaID.ToString()
                ElseIf RegionID > 0 Then
                    sqlStr += " and i.region_id=" + RegionID.ToString()
                End If
                If TypeID = 1 Then
                    sqlStr += " and DATEADD(day, DATEDIFF(day, 0, j.END_DT), 0) between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                Else
                    sqlStr += " and j.period_to between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                End If
                If SPMFlag = 1 Then
                    sqlStr += " and e.SPM_Flag=1"
                End If
                sqlStr += "  order by i.branch_name,j.GROUP_ID "
                DT = DB.ExecuteDataSet(sqlStr).Tables(0)
                Dim TRHead As New TableRow
                Dim I As Integer
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell
                RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
                RH.InsertColumn(TRHead, TRHead_01, 47, 0, "Audit")
                RH.InsertColumn(TRHead, TRHead_02, 12, 0, "Period From")
                RH.InsertColumn(TRHead, TRHead_03, 12, 0, "Period To")
                RH.InsertColumn(TRHead, TRHead_04, 12, 0, "Started On")
                RH.InsertColumn(TRHead, TRHead_05, 12, 0, "Ended On")

                tb.Controls.Add(TRHead)
                I = 1
                For Each DR In DT.Rows
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                    RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                    RH.InsertColumn(TR3, TR3_01, 47, 0, DR(2).ToString())
                    RH.InsertColumn(TR3, TR3_02, 12, 2, CDate(DR(3)).ToString("dd/MMM/yyyy"))
                    RH.InsertColumn(TR3, TR3_03, 12, 2, CDate(DR(4)).ToString("dd/MMM/yyyy"))
                    RH.InsertColumn(TR3, TR3_04, 12, 2, CDate(DR(5)).ToString("dd/MMM/yyyy"))
                    RH.InsertColumn(TR3, TR3_05, 12, 2, CDate(DR(6)).ToString("dd/MMM/yyyy"))
                    tb.Controls.Add(TR3)
                    I = I + 1
                Next
                RH.BlankRow(tb, 20)
                pnDisplay.Controls.Add(tb)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
