﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewConsolidatedAuditReportLoanWise_Gen
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim CloseFlag As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim DB As New MS_SQL.Connect
    Dim Rtype As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim StatusID As Integer = CInt(Request.QueryString.Get("StatusID"))

            AuditDtl = GF.Decrypt(Request.QueryString.Get("AuditID"))
            'Me.hdnBranchID.Value = GF.Decrypt(Request.QueryString.Get("BranchID"))
            If Not IsNothing(Request.QueryString.Get("RegionID")) Then
                RegionID = CInt(GF.Decrypt(Request.QueryString.Get("RegionID")))
            End If
            If Not IsNothing(Request.QueryString.Get("AreaID")) Then
                AreaID = CInt(GF.Decrypt(Request.QueryString.Get("AreaID")))
            End If
            If Not IsNothing(Request.QueryString.Get("BranchID")) Then
                BranchID = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))
            End If

            Dim ReportID As Integer = CInt(Request.QueryString.Get("ReportID"))
            CloseFlag = CInt(Request.QueryString.Get("CloseFlag"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Rtype = CInt(Request.QueryString.Get("RTypeID"))
            Me.hdnReportID.Value = ReportID.ToString()
            Me.hdnAuditID.Value = AuditDtl
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If

            Dim StrQry As String = ""
            Dim StrDateQry As String = ""
            Dim StrJoinQry As String = ""
            Dim SqlStr As String = ""
            Dim SqlStrSub As String = ""
            Dim SqlStrSub_1 As String = ""
            If StatusID = 2 Then
                SqlStr += " and  DATEADD(day, DATEDIFF(day, 0, k.END_DT), 0) <= '" + ToDt + "' and (k.REPORT_CLOSED_DT >'" + ToDt + "' OR k.REPORT_CLOSED_DT IS NULL) "
            ElseIf StatusID = 1 Then
                SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b, AUDIT_DTL c,AUDIT_MASTER d " & _
                    " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <='" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=" + StatusID.ToString() + ")  group by d.GROUP_ID) mm  where k.group_id=mm.group_id and mm.cnt>0)"
            ElseIf StatusID = 5 Then
                SqlStr += " and exists (select mm.group_id,mm.cnt from (select d.GROUP_ID,COUNT(*)as cnt from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d " & _
                    " where DATEADD(day, DATEDIFF(day, 0, d.END_DT), 0) <= '" + ToDt + "' and   ( DATEADD(day, DATEDIFF(day, 0, D.REPORT_CLOSED_DT), 0) >'" + ToDt + "' OR D.REPORT_CLOSED_DT IS NULL) and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and a.STATUS_ID in (select Observation_Status from AUDIT_OBSERVATION_STATUS_DTL where STATUS_ID=5) and d.GROUP_ID not in(select group_id from audit_dtl where audit_id in(select AUDIT_ID from AUDIT_OBSERVATION where OBSERVATION_ID in(select OBSERVATION_ID from AUDIT_OBSERVATION_DTL where STATUS_ID=1)))  group by d.GROUP_ID) mm  where k.group_id=mm.group_id and mm.cnt>0)"
            ElseIf StatusID = 0 Then
                SqlStr += " and DATEADD(day, DATEDIFF(day, 0, k.REPORT_CLOSED_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "'"
            Else
                SqlStr += " and DATEADD(day, DATEDIFF(day, 0, k.END_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' "
            End If

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim SubHD As String = ""
            Dim HDStr As String = ""
            If (CloseFlag = 1) Then
                SubHD = "Status - Pending Operations (As on -" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 5 Then
                SubHD = "Status - Pending Auditors (As on -" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 2 Then
                SubHD = "Status - Pending (As on -" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 0 Then
                SubHD = "Status - Closed (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            Else
                SubHD = "Status -All (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            End If

            If Month > 0 And year > 0 Then
                SqlStrSub_1 += " and month(k.period_to)=" + Month.ToString() + " and year(k.period_to)=" + year.ToString() + ""
                HDStr += MonthName(Month) + " " + year.ToString()
            Else
                HDStr += "Consolidated "
            End If
            HDStr += " Audit Observations "
            Dim Location As String = ""
            If BranchID > 0 Then
                SqlStrSub_1 += " and h.branch_id=" + BranchID.ToString()
                Location = GN.GetBranch_Name(BranchID)
                HDStr += Location + " Branch"
            ElseIf AreaID > 0 Then
                SqlStrSub_1 += " and h.area_id=" + AreaID.ToString()
                Location = GN.GetArea_Name(AreaID)
                HDStr += Location + " Area"
            ElseIf RegionID > 0 Then
                SqlStrSub_1 += " and h.region_id=" + RegionID.ToString()
                Location = GN.GetRegion_Name(RegionID)
                HDStr += Location + " Region"
            End If
            If PostID = 8 Then
                SqlStrSub_1 += " and h.zone_Head=" + Session("UserID").ToString()

            End If

            PostID = CInt(Session("Post_ID"))
            Dim TotLeakage As Double = 0
            Dim BranchName As String = ""
            Dim ItemName As String = ""
            If StatusID = 1 Then
                SqlStr = "select e.remarks,a.fin_Leakage,a.SINo,a.loan_no,h.branch_name,f.Item_name,l.client_name,m.center_name,l.loan_amt  " & _
                    " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k, " & _
                    " loan_master l,center_master m   where  " & _
                    "  a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id " & _
                    " and g.group_id=k.group_id  and a.Fraud=1 and  k.status_id=2 and a.loan_no=l.loan_no and l.center_id=m.center_id and g.audit_type in(1,2) and " & _
                    " a.last_updated_dt is not null " + SqlStrSub_1 + " " + SqlStrSub + " union all select e.remarks,a.fin_Leakage,a.SINo,a.loan_no,h.branch_name,f.Item_name ,'','',0 from " & _
                    " AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k   where " & _
                    " a.OBSERVATION_ID=b.OBSERVATION_ID  and " & _
                    " a.order_id = e.Order_ID And b.item_id = f.item_id And b.audit_id = g.audit_id And g.branch_id = h.branch_id And g.group_id = k.group_id   And a.Fraud = 1 And " & _
                    " k.status_id=2 and a.last_updated_dt is not null " + SqlStrSub_1 + " " + SqlStrSub + " and g.audit_type=3 "
                HDStr += " Type - Fraud"
            ElseIf StatusID = 2 Then
                SqlStr = "select e.remarks,a.fin_Leakage,a.SINo ,a.loan_no,h.branch_name,f.Item_name,l.client_name,m.center_name,l.loan_amt  from AUDIT_OBSERVATION_DTL a  " & _
                    " ,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k,loan_master l,center_master m   where  " & _
                    "     a.OBSERVATION_ID=b.OBSERVATION_ID  and  " & _
                    " a.order_id=e.Order_ID  and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and  k.status_id=2  " & _
                    " and a.loan_no=l.loan_no and l.center_id=m.center_id and g.audit_type in(1,2) and  a.last_updated_dt is not null and a.Susp_Leakage=1  " & _
                    " " + SqlStrSub_1 + " " + SqlStrSub + "  union all select e.remarks,a.fin_Leakage,a.SINo ,a.loan_no,h.branch_name,f.Item_name ,'','',0   " & _
                    " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k   where  " & _
                    "     a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  " & _
                    " and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and k.status_id=2 and  " & _
                    " a.last_updated_dt is not null and a.Susp_Leakage=1 " + SqlStrSub_1 + " " + SqlStrSub + "  and g.audit_type=3"
                HDStr += " Type - Serious"
            ElseIf StatusID = 3 Then
                SqlStr = "select e.remarks,a.fin_Leakage,a.SINo ,a.loan_no,h.branch_name,f.Item_name,l.client_name,m.center_name,l.loan_amt  from AUDIT_OBSERVATION_DTL a " & _
                    " ,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k,loan_master l,center_master m   where  " & _
                    "     a.OBSERVATION_ID=b.OBSERVATION_ID  and  " & _
                    " a.order_id=e.Order_ID and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and  k.status_id=2   " & _
                    " and a.loan_no=l.loan_no and l.center_id=m.center_id and g.audit_type in(1,2) and a.last_updated_dt is not null and a.Fraud=0 and a.Susp_Leakage=0  " & _
                    " " + SqlStrSub_1 + " " + SqlStrSub + "  union all select e.remarks,a.fin_Leakage,a.SINo ,a.loan_no,h.branch_name,f.Item_name ,'','',0   " & _
                    " from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k   where  " & _
                    "     a.OBSERVATION_ID=b.OBSERVATION_ID  and  " & _
                    " a.order_id=e.Order_ID and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and  k.status_id=2  " & _
                    " and a.last_updated_dt is not null and a.Fraud=0 and a.Susp_Leakage=0  " + SqlStrSub_1 + " " + SqlStrSub + "  and g.audit_type=3"
                HDStr += " Type - General"
            ElseIf StatusID = 4 Then
                SqlStr = "select e.remarks,a.fin_Leakage ,a.SINo,a.loan_no,h.branch_name,f.Item_name,l.client_name,m.center_name,l.loan_amt  from AUDIT_OBSERVATION_DTL a " & _
                    " ,AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k,loan_master l,center_master m   where  " & _
                    "     a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  " & _
                    " and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and  k.status_id=2  and a.loan_no=l.loan_no  " & _
                    " and l.center_id=m.center_id   and g.audit_type in(1,2) and a.last_updated_dt is not null and a.Fin_Leakage>0 " + SqlStrSub_1 + " " + SqlStrSub + "  " & _
                    "   union all  " & _
                    " select e.remarks,a.fin_Leakage ,a.SINo,a.loan_no,h.branch_name,f.Item_name ,'','',0  from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,audit_observation_Cycle e  " & _
                    ",AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k   where    " & _
                    "  a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and  " & _
                    " g.group_id=k.group_id   and  k.status_id=2 and a.last_updated_dt is not null and a.Fin_Leakage>0 " + SqlStrSub_1 + " " + SqlStrSub + "   and g.audit_type=3  "

                HDStr += " With Financial Leakage"
            ElseIf StatusID = 0 Then
                SqlStr = "select e.remarks,a.fin_Leakage,a.SINo,a.loan_no,h.branch_name,f.Item_name,l.client_name,m.center_name,l.loan_amt   from AUDIT_OBSERVATION_DTL a, " & _
                    " AUDIT_OBSERVATION b,audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k,loan_master l,center_master m  where  " & _
                    "     a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  " & _
                    " and b.item_id=f.item_id and b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and  k.status_id=2  and a.loan_no=l.loan_no  " & _
                    " and l.center_id=m.center_id and g.audit_type in(1,2) and a.last_updated_dt is not null " + SqlStrSub_1 + " " + SqlStrSub + "  " & _
                    "  union all  " & _
                    " select e.remarks,a.fin_Leakage,a.SINo,a.loan_no,h.branch_name,f.Item_name ,'','',0   from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,  " & _
                    " audit_observation_Cycle e,AUDIT_CHECK_LIST f,Audit_dtl g,brmaster h,audit_master k   where   " & _
                    "   a.OBSERVATION_ID=b.OBSERVATION_ID  and a.order_id=e.Order_ID  and b.item_id=f.item_id and  " & _
                    " b.audit_id=g.audit_id and g.branch_id=h.branch_id and g.group_id=k.group_id   and  k.status_id=2 and a.last_updated_dt is not null  " & _
                    " " + SqlStrSub_1 + " " + SqlStrSub + "  and g.audit_type=3 "
            End If
            DT = DB.ExecuteDataSet(SqlStr + " order by 5,6").Tables(0)
            RH.Heading(Session("FirmName"), tb, HDStr, 100)
            RH.SubHeading(tb, 100, "l", SubHD)
            For Each DR In DT.Rows
                If (BranchName <> DR(4) Or ItemName <> DR(5)) Then
                    If BranchName <> DR(4) Then
                        RH.BlankRow(tb, 5)
                        Dim trBranch As New TableRow
                        Dim trBranch_00 As New TableCell
                        trBranch.Style.Add("background-color", "LightSteelblue")
                        RH.AddColumn(trBranch, trBranch_00, 100, 100, "c", DR(4))
                        tb.Controls.Add(trBranch)
                        j = 0
                    End If
                    RH.BlankRow(tb, 5)
                    Dim trItem As New TableRow
                    Dim trItem_00 As New TableCell
                    trItem.Style.Add("background-color", "lightsilver")
                    RH.AddColumn(trItem, trItem_00, 100, 100, "l", "<strong><i>Check List : " + DR(5) + "</i></strong>")
                    tb.Controls.Add(trItem)
                    Dim TRHead As New TableRow
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

                    TRHead_00.BorderWidth = "1"
                    TRHead_01.BorderWidth = "1"
                    TRHead_02.BorderWidth = "1"
                    TRHead_03.BorderWidth = "1"
                    TRHead_04.BorderWidth = "1"
                    TRHead_05.BorderWidth = "1"
                    TRHead_06.BorderWidth = "1"

                    TRHead_00.BorderColor = Drawing.Color.Silver
                    TRHead_01.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_03.BorderColor = Drawing.Color.Silver
                    TRHead_04.BorderColor = Drawing.Color.Silver
                    TRHead_05.BorderColor = Drawing.Color.Silver
                    TRHead_06.BorderColor = Drawing.Color.Silver

                    TRHead_00.BorderStyle = BorderStyle.Solid
                    TRHead_01.BorderStyle = BorderStyle.Solid
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    TRHead_03.BorderStyle = BorderStyle.Solid
                    TRHead_04.BorderStyle = BorderStyle.Solid
                    TRHead_05.BorderStyle = BorderStyle.Solid
                    TRHead_06.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No")
                    RH.AddColumn(TRHead, TRHead_01, 12, 12, "l", "Ref. No")
                    RH.AddColumn(TRHead, TRHead_02, 12, 12, "l", "Client Name")
                    RH.AddColumn(TRHead, TRHead_03, 12, 12, "l", "Center")
                    RH.AddColumn(TRHead, TRHead_04, 8, 8, "l", "Loan Amt")
                    RH.AddColumn(TRHead, TRHead_05, 41, 41, "l", "Last Remarks")
                    RH.AddColumn(TRHead, TRHead_06, 10, 10, "c", "Est.Amt. Involved")
                    tb.Controls.Add(TRHead)
                End If


                BranchName = DR(4)
                ItemName = DR(5)
                j += 1
                I += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())

                RH.AddColumn(TR3, TR3_01, 12, 12, "l", DR(3))
                RH.AddColumn(TR3, TR3_02, 12, 12, "l", DR(6))
                RH.AddColumn(TR3, TR3_03, 12, 12, "l", DR(7))
                If (DR(8) = 0) Then
                    RH.AddColumn(TR3, TR3_04, 8, 8, "l", "")
                Else
                    RH.AddColumn(TR3, TR3_04, 8, 8, "l", CDbl(DR(8)).ToString("#,##,##,###"))
                End If
                RH.AddColumn(TR3, TR3_05, 41, 41, "l", DR(0).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(2)) + "' style='text-align:right;' target='_blank' >Previous Remarks</a>")
                If DR(1) = 0 Then
                    RH.AddColumn(TR3, TR3_06, 10, 10, "r", "-")
                Else
                    RH.AddColumn(TR3, TR3_06, 10, 10, "r", CDbl(DR(1)).ToString("0.00"))
                End If
                tb.Controls.Add(TR3)
                TotLeakage += CDbl(DR(1))
            Next
            RH.BlankRow(tb, 5)
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"


            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver


            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", I.ToString())
            RH.AddColumn(TRFooter, TRFooter_01, 45, 45, "l", "Total")
            RH.AddColumn(TRFooter, TRFooter_02, 40, 40, "c", "")
            If TotLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "r", TotLeakage.ToString("###,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "r", "-")
            End If

            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 20)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
