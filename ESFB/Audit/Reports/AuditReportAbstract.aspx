﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditReportAbstract.aspx.vb" Inherits="Audit_Reports_AuditReportAbstract" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
 <table align="center" style="width:40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                  <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Period</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Region</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbRegion" class="NormalText" runat="server" Font-Names="Cambria" 
                  Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Area</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbArea" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Branch</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Status</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                    <asp:ListItem Value="2">PENDING</asp:ListItem>
                    <asp:ListItem Value="1">PENDING OPERATIONS</asp:ListItem>
                    <asp:ListItem Value="5">PENDING AUDITORS</asp:ListItem>
                    <asp:ListItem Value="0">CLOSED</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px;">
                From</td>
            <td  style="width:35%; height: 18px;">
            <asp:TextBox ID="txtFrom" class="NormalText" runat="server" Width="75%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtFrom" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
            <td  style="width:15%; height: 18px;">
                To</td>
            <td  style="width:35%; height: 18px;">
            <asp:TextBox ID="txtTo" class="NormalText" runat="server" Width="75%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEToDate" runat="server" 
                    TargetControlID="txtTo" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
            </td>
        </tr></table>
   
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
     

        function FromServer(arg, context) {

            switch (context) {               
                case 1:
                    ComboFill(arg, "<%= cmbArea.ClientID %>");
                    break;
                case 2:
                    ComboFill(arg, "<%= cmbBranch.ClientID %>");
                    break;
            }

        }

        function RegionOnChange() {
            var RegionID = document.getElementById("<%= cmbRegion.ClientID %>").value;
            if (RegionID != -1) {
                var Data = "1Ø" + RegionID
                ToServer(Data, 1);
            }
            else {
                ClearCombo("<%= cmbArea.ClientID %>");
                ClearCombo("<%= cmbBranch.ClientID %>");
            }
        }
        function AreaOnChange() {
            var AreaID = document.getElementById("<%= cmbArea.ClientID %>").value;
             if (AreaID != -1) {
                var Data = "2Ø" + AreaID
                ToServer(Data, 2);
            }
            else {                
                ClearCombo("<%= cmbBranch.ClientID %>");
            }
        }
        function btnView_onclick() {           
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;           
//            if (AuditID == -1)
//            { alert("Select Audit"); document.getElementById("<%= cmbAudit.ClientID %>").focus(); return false; }
            var RegionID = document.getElementById("<%= cmbRegion.ClientID %>").value;
            var AreaID = document.getElementById("<%= cmbArea.ClientID %>").value;
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var StatusID = document.getElementById("<%= cmbStatus.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFrom.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtTo.ClientID %>").value;
            
            window.open("viewAuditAbstractReport.aspx?AuditID=" + AuditID + " &RegionID=" + btoa(RegionID) + " &AreaID=" + btoa(AreaID) + " &BranchID=" + btoa(BranchID) + "&StatusID=" + StatusID +" &FromDt="+ FromDt + " &ToDt="+ ToDt + "", "_self");
            
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = "ALL";
            document.getElementById(control).add(option1);
        }

       

// ]]>
    </script>
</asp:Content>

