﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class AuditReportAbstractDetail
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim LocationID As Integer = 0
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GN.FormAccess(CInt(Session("UserID")), 215) = False Then
            Response.redirect("~/AccessDenied.aspx", False)
            Return
        End If
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        ReportID = CInt(Request.QueryString.Get("RptID"))
        Me.hdnReportID.Value = ReportID.ToString()
        Me.hdnPostID.Value = Session("Post_ID")
        Dim PostID As Integer = CInt(Session("Post_ID"))
        If (PostID = 6) Then
            LocationID = GN.GetArea_ID(CInt(Session("UserID")))
        ElseIf (PostID = 7) Then
            LocationID = GN.GetRegion_ID(CInt(Session("UserID")))
        End If
        Me.hdnLocationID.Value = LocationID.ToString()
        Me.Master.subtitle = "Consolidated Audit Report Abstract"
        GN.ComboFill(cmbAudit, AD.GetAuditMonthsALL(), 0, 1)
        GN.ComboFill(cmbRegion, GN.GetRegion(2, CInt(Session("Post_ID")), CInt(Session("UserID"))), 0, 1)
        If PostID = 5 Then
            DT = DB.ExecuteDataSet("select area_id,area_name from area_master where area_id in(select area_id from branch_master where branch_id=" + Session("BranchID") + ")").Tables(0)
            GN.ComboFill(cmbArea, DT, 0, 1)
            DT = DB.ExecuteDataSet("select branch_id,branch_name from brmaster where branch_id=" + Session("BranchID")).Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
        ElseIf PostID = 6 Then
            DT = DB.ExecuteDataSet("select area_id,area_name from area_master where area_head=" + Session("UserID") + "").Tables(0)
            GN.ComboFill(cmbArea, DT, 0, 1)
            DT = DB.ExecuteDataSet("select -1 as Branch_id,' ALL' as Branch_Name union all select branch_id,branch_name from brmaster where area_id in(select area_id from area_master where area_head=" + Session("UserID") + ")").Tables(0)
            GN.ComboFill(cmbBranch, DT, 0, 1)
        ElseIf PostID = 7 Then
            DT = DB.ExecuteDataSet("select -1 as AreaID,' ALL' as AreaName union all select area_id,area_name from area_master where region_id in (select region_ID from region_master where region_Head=" + Session("UserID") + ")").Tables(0)
            GN.ComboFill(cmbArea, DT, 0, 1)
        End If
        Me.cmbRegion.SelectedIndex = 0
        Me.CEFromDate.SelectedDate = CDate("01/May/2014")
        CEToDate.SelectedDate = CDate(Session("TraDt"))
        Me.
        cmbRegion.Attributes.Add("onchange", "RegionOnChange()")
        cmbArea.Attributes.Add("onchange", "AreaOnChange()")
    End Sub


    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            DT = GN.GetArea(CInt(Data(1)), 2)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
        If CInt(Data(0)) = 2 Then
            DT = GN.GetBranch(CInt(Data(1)), 2)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
End Class
