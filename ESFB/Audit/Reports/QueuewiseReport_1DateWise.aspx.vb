﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class QueuewiseReport_1DateWise
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim LevelID As Integer
    Dim StatusID As Integer
    Dim UserID As Integer
    Dim TRA_DT As DateTime
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim PostID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            StatusID = CInt(Request.QueryString.Get("StatusID"))

            LevelID = CInt(GN.Decrypt(Request.QueryString.Get("LevelID")))

            PostID = CInt(Session("Post_ID"))
            UserID = CInt(Session("UserID"))
            TRA_DT = CDate(Request.QueryString.Get("TRA_DT"))
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim LevelName As String = AD.GetLevelName(LevelID)
            Dim PostName As String = GN.GetEmp_Post_Name(PostID)
            Dim subHD As String = ""
            Dim SqlStr As String = ""
            Dim SqlStr_1 As String = ""
            Dim SqlStr_2 As String = ""
            If StatusID = 1 Then
                subHD = "Original Response Pending - " + LevelName + " Level"
                SqlStr = "select e.Region_ID,e.Region_Name,COUNT(sino) as count from AUDIT_OBSERVATION_DTL_DATEWISE a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where DATEADD(day, DATEDIFF(day, 0, a.TRA_DT ), 0)='" + TRA_DT.ToString("dd/MMM/yyyy") + "' and DATEADD(day, DATEDIFF(day, 0, d.END_DT ), 0)<='" + TRA_DT.ToString("dd/MMM/yyyy") + "'and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 and a.STATUS_ID=1 and a.LEVEL_ID=" & LevelID.ToString() & " and a.Response_Flag=0 and a.ROLE_ID=1 "
                SqlStr_2 = " group by  e.Region_ID,e.Region_Name"
            ElseIf StatusID = 2 Then
                subHD = "Further Response Pending - " + LevelName + " Level"
                SqlStr = "select e.Region_ID,e.Region_Name,COUNT(sino) as count from AUDIT_OBSERVATION_DTL_DATEWISE a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where DATEADD(day, DATEDIFF(day, 0, a.TRA_DT ), 0)='" + TRA_DT.ToString("dd/MMM/yyyy") + "' and DATEADD(day, DATEDIFF(day, 0, d.END_DT ), 0)<='" + TRA_DT.ToString("dd/MMM/yyyy") + "'and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 and a.STATUS_ID=1 and a.LEVEL_ID=" & LevelID.ToString() & " and a.Response_Flag>0 and a.ROLE_ID=1 "
                SqlStr_2 = " group by  e.Region_ID,e.Region_Name"
            ElseIf StatusID = 3 Then
                subHD = "Response Authorization Pending - " + LevelName + " Level"
                SqlStr = "select e.Region_ID,e.Region_Name,COUNT(sino) as count from AUDIT_OBSERVATION_DTL_DATEWISE a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where DATEADD(day, DATEDIFF(day, 0, a.TRA_DT ), 0)='" + TRA_DT.ToString("dd/MMM/yyyy") + "' and DATEADD(day, DATEDIFF(day, 0, d.END_DT ), 0)<='" + TRA_DT.ToString("dd/MMM/yyyy") + "'and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 and a.STATUS_ID=1 and a.LEVEL_ID=" & LevelID.ToString() & "  and a.ROLE_ID=4 "
                SqlStr_2 = " group by  e.Region_ID,e.Region_Name"
            ElseIf StatusID = 4 Then
                subHD = "Response Closure Pending - " + LevelName + " Level"
                SqlStr = "select e.Region_ID,e.Region_Name,COUNT(sino) as count from AUDIT_OBSERVATION_DTL_DATEWISE a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where DATEADD(day, DATEDIFF(day, 0, a.TRA_DT ), 0)='" + TRA_DT.ToString("dd/MMM/yyyy") + "' and DATEADD(day, DATEDIFF(day, 0, d.END_DT ), 0)<='" + TRA_DT.ToString("dd/MMM/yyyy") + "'and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 and a.STATUS_ID=5 and a.LEVEL_ID=" & LevelID.ToString() & "   "
                SqlStr_2 = " group by  e.Region_ID,e.Region_Name"
            ElseIf StatusID = 5 Then
                subHD = "Response Authorization Escalated to - " + LevelName + " Level"
                SqlStr = "select e.Region_ID,e.Region_Name,COUNT(sino) as count from AUDIT_OBSERVATION_DTL_DATEWISE a,audit_observation b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e where DATEADD(day, DATEDIFF(day, 0, a.TRA_DT ), 0)='" + TRA_DT.ToString("dd/MMM/yyyy") + "' and DATEADD(day, DATEDIFF(day, 0, d.END_DT ), 0)<='" + TRA_DT.ToString("dd/MMM/yyyy") + "'and a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID and d.STATUS_ID=2 and a.STATUS_ID=1 and " & LevelID.ToString() & ">a.Level_id and " & LevelID.ToString() & "<=a.Level_Upto and a.level_id<a.level_upto "
                SqlStr_2 = " group by  e.Region_ID,e.Region_Name"
            End If
            If (PostID = 5) Then
                SqlStr_1 = " and e.Branch_Head=" + UserID.ToString()
            ElseIf PostID = 6 Then
                SqlStr_1 = " and e.area_Head=" + UserID.ToString()
            ElseIf PostID = 7 Then
                SqlStr_1 = " and e.region_Head=" + UserID.ToString()
            ElseIf PostID = 8 Then
                SqlStr_1 = " and e.zone_Head=" + UserID.ToString()
            ElseIf PostID = 3 Then
                SqlStr_1 = " and e.branch_id in(select branch_ID from audit_Branches where team_lead=" + UserID.ToString() + ")"
            ElseIf PostID = 4 Then
                SqlStr_1 = " and e.branch_id in(select branch_ID from audit_Branches where emp_code=" + UserID.ToString() + ")"
            End If

            SqlStr = SqlStr + SqlStr_1 + SqlStr_2
            RH.Heading(Session("FirmName"), tb, subHD, 100)
            RH.SubHeading(tb, 100, "l", "Report Level - " + PostName)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")





            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid



            RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "#")
            RH.AddColumn(TRHead, TRHead_01, 60, 60, "l", "Region")
            RH.AddColumn(TRHead, TRHead_02, 35, 35, "c", "Count")

            tb.Controls.Add(TRHead)

            'RH.BlankRow(tb, 3)
            Dim i As Integer = 0
            Dim Total As Integer


            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows

                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 60, 60, "l", DR(1))
                RH.AddColumn(TR3, TR3_02, 35, 35, "c", "<a href='QueuewiseReport_2DateWise.aspx?StatusID=" + StatusID.ToString() + " &LevelID=" + GN.Encrypt(LevelID.ToString()) + " &RegionID=" + GN.Encrypt(DR(0).ToString()) + "&TRA_DT=" + TRA_DT.ToString() + "' style='text-align:right;' target='_blank'>" + CInt(DR(2)).ToString("#,##,###") + "</a>")

                tb.Controls.Add(TR3)
                Total += CInt(DR(2))
            Next

            Dim TR_End As New TableRow
            Dim TR_End_00, TR_End_01, TR_End_02 As New TableCell
            TR_End_00.BorderWidth = "1"
            TR_End_01.BorderWidth = "1"
            TR_End_02.BorderWidth = "1"

            TR_End_00.BorderColor = Drawing.Color.Silver
            TR_End_01.BorderColor = Drawing.Color.Silver
            TR_End_02.BorderColor = Drawing.Color.Silver



            TR_End_00.BorderStyle = BorderStyle.Solid
            TR_End_01.BorderStyle = BorderStyle.Solid
            TR_End_02.BorderStyle = BorderStyle.Solid


            TR_End.Style.Add("Font-Weight", "Bold")
            RH.AddColumn(TR_End, TR_End_00, 5, 5, "l", "")
            RH.AddColumn(TR_End, TR_End_01, 60, 60, "l", "Total")
            RH.AddColumn(TR_End, TR_End_02, 35, 35, "c", "<a href='QueuewiseReport_TotDateWise.aspx?StatusID=" + StatusID.ToString() + " &LevelID=" + GN.Encrypt(LevelID.ToString()) + " &RegionID=" & GN.Encrypt("-1") & " &AreaID=" & GN.Encrypt("-1") & " &BranchID=" & GN.Encrypt("-1") & " &TRA_DT=" + TRA_DT.ToString() + "'style='text-align:right;' target='_blank'>" + Total.ToString("#,##,###") + "</a>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
