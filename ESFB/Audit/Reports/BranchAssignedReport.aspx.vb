﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class BranchAssignedReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 64) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))
            Dim PostName As String = ""
            PostName = GN.GetEmp_Post_Name(PostID)
            RH.Heading(Session("FirmName"), tb, "BRANCH ASSIGNED REPORT", 100)
            RH.SubHeading(tb, 100, "l", "Report Level - " + PostName)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver



            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid



            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "SI No")
            RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Team Manager")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "l", "Team Lead")
            RH.AddColumn(TRHead, TRHead_04, 25, 25, "l", "Auditor")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "Screening")
            tb.Controls.Add(TRHead)

            'RH.BlankRow(tb, 3)
            Dim i, j As Integer
            Dim Org, Auth, Auth_Esc, Further, ForClos As Integer
            Org = 0
            Auth = 0
            Auth_Esc = 0
            Further = 0
            ForClos = 0
            Dim SqlStr As String = ""
            If PostID = 5 Then
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+ cast(a.team_lead as varchar(6)) +')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')' as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  and d.branch_id in(select branch_id from brmaster where branch_head=" + Session("UserID") + ")  order by g.emp_name,b.Emp_Name,c.Emp_Name,d.Branch_Name "
            ElseIf PostID = 6 Then
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+cast(a.team_lead as varchar(6))+')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')'  as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  and d.branch_id in(select branch_id from brmaster where area_head=" + Session("UserID") + ")  order by g.emp_name,b.Emp_Name,c.Emp_Name,d.Branch_Name "
            ElseIf PostID = 7 Then
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+cast(a.team_lead as varchar(6))+')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')'  as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  and d.branch_id in(select branch_id from brmaster where region_head=" + Session("UserID") + ")  order by g.emp_name,b.Emp_Name,c.Emp_Name,d.Branch_Name "
            ElseIf PostID = 8 Then
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+cast(a.team_lead as varchar(6))+')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')'  as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  and d.branch_id in(select branch_id from brmaster where zone_head=" + Session("UserID") + ")  order by g.emp_name,b.Emp_Name,c.Emp_Name,d.Branch_Name "
            ElseIf PostID = 3 Then
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+cast(a.team_lead as varchar(6))+')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')'  as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  and d.branch_id in(select branch_id from audit_branches where team_lead=" + Session("UserID") + ")  order by g.emp_name,b.Emp_Name,c.Emp_Name,d.Branch_Name "
            ElseIf PostID = 4 Then
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+cast(a.team_lead as varchar(6))+')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')'  as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  and d.branch_id in(select branch_id from audit_branches where emp_code=" + Session("UserID") + ")  order by g.emp_name,b.Emp_Name,c.Emp_Name,d.Branch_Name "
            Else
                SqlStr = "select a.BRANCH_ID,upper(d.Branch_Name) as Branch_Name,ISNULL(b.Emp_Name+' ('+cast(a.team_lead as varchar(6))+')','Not Assigned') as TeamLead,c.Emp_Name+' ('+cast(a.emp_code as varchar(6))+')'  as auditor,(case when e.NEED_APPROVAL=1 then 'Yes' else 'No' end) as screening, g.Emp_Name+' ('+cast(f.emp_code as varchar(6))+')' as TeamManager  from AUDIT_BRANCHES a left outer join EMP_MASTER b on(a.TEAM_LEAD=b.Emp_Code),EMP_MASTER c,BRANCH_MASTER d left join (select * from AUDIT_ADMIN_BRANCHES where TYPE_ID=1) f ON d.BRANCH_ID=f.Branch_ID LEFT JOIN EMP_MASTER g ON f.EMP_CODE=g.Emp_Code,AUDIT_EMP_MASTER e where a.EMP_CODE=c.Emp_Code and a.BRANCH_ID=d.Branch_ID and a.EMP_CODE=e.EMP_CODE  order by g.emp_name,b.Emp_Name,c.Emp_Name ,d.Branch_Name"
            End If
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows

                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", i.ToString())

                RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_02, 20, 20, "l", DR(5).ToString())
                If (DR(2).ToString() = "Not Assigned") Then
                    TR3_02.Style.Add("color", "red")
                Else
                    TR3_02.Style.Add("color", "black")
                End If
                RH.AddColumn(TR3, TR3_03, 20, 20, "l", DR(2).ToString())

                RH.AddColumn(TR3, TR3_04, 25, 25, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 10, 10, "c", DR(4).ToString())
                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String

            HeaderText = "Assigned Branches Report"

            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
