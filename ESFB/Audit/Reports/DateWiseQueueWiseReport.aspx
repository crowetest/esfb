﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="DateWiseQueueWiseReport.aspx.vb" Inherits="DateWiseQueueWiseReport" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                Date</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbDate" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">As on Date</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"/>&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        function btnView_onclick() 
        {
            var TRA_DT = document.getElementById("<%= cmbDate.ClientID %>").value;
            if (TRA_DT != '-1')
                window.open("QueuewiseReportDateWise.aspx?TRA_DT=" + TRA_DT, "_blank")
            else
                window.open("QueuewiseReport.aspx", "_blank")
        }
        function btnExit_onclick() 
        {
            window.open("../../Home.aspx", "_self");
        }
    </script>
</asp:Content>

