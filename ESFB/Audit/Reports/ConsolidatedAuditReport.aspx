﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ConsolidatedAuditReport.aspx.vb" Inherits="Audit_Reports_ConsolidatedAuditReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
                    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                    </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 35%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:21%;">
                Period</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="96%" ForeColor="Black" Height="16px">
                    <asp:ListItem Value="-1"> ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
      
        <tr>
            <td style="width:21%;">
                Type</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="96%" ForeColor="Black" Height="19px">
                    <asp:ListItem Value="2">REPORT WISE</asp:ListItem>
                    <asp:ListItem Value="1">OBSERVATION WISE</asp:ListItem>                                       
                </asp:DropDownList></td>
        </tr>
          <tr>
            <td style="width:21%;">
                Status</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="96%" ForeColor="Black" Height="18px">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                    <asp:ListItem Value="2">PENDING</asp:ListItem>
                    <asp:ListItem Value="1">PENDING OPERATIONS</asp:ListItem>
                    <asp:ListItem Value="5">PENDING AUDITORS</asp:ListItem>
                    <asp:ListItem Value="0">CLOSED</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
          <tr id="from">
            <td style="width:21%;">
                Ended From&nbsp;</td>
            <td  style="width:85%;">
                <asp:TextBox ID="txtFrom" runat="server" Height="16px" Width="155px"></asp:TextBox>
                <asp:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtFrom" Format="dd MMM yyyy">
                </asp:CalendarExtender>
              </td>
        </tr>
          <tr>
            <td style="width:21%;">
                Ended To</td>
            <td  style="width:85%;">
                <asp:TextBox ID="txtTo" runat="server" Height="16px" Width="155px"></asp:TextBox>
                <asp:CalendarExtender ID="txtTo_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtTo" Format="dd MMM yyyy">
                </asp:CalendarExtender>
              </td>
        </tr>
        <tr id ="ChkObsClose" style="display:none;">
            <td style="height: 19px;" colspan="2">
                <asp:CheckBox ID="ChkObs_Close" runat="server" />&nbsp;&nbsp;Based On Observation Close Date
            </td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:21%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:21%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:21%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:21%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }
        function StatusOnChange() {
//            document.getElementById("<%= ChkObs_Close.ClientID %>").checked = false;
            if (document.getElementById("<%= cmbStatus.ClientID %>").value == "1" || document.getElementById("<%= cmbStatus.ClientID %>").value == "2" || document.getElementById("<%= cmbStatus.ClientID %>").value == "5") {
                document.getElementById("from").style.display = 'none';
            }
            else {
                document.getElementById("from").style.display = '';
            }
        }

        function btnView_onclick() {
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            var StatusID = document.getElementById("<%= cmbStatus.ClientID %>").value;
            var FromDt = document.getElementById("<%= txtFrom.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtTo.ClientID %>").value;
            var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
            var RTypeID = (document.getElementById("<%= ChkObs_Close.ClientID %>").checked==true ? 1 : 2);
            
            if (TypeID == 1) {
                if (document.getElementById("<%= hdnPostID.ClientID %>").value == "7") {
                    window.open("ViewConsolidatedAuditReportAreaWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
                else if (document.getElementById("<%= hdnPostID.ClientID %>").value == "6") {
                    window.open("ViewConsolidatedAuditReportBranchWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=btoa(0) &AreaID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
                else if (document.getElementById("<%= hdnPostID.ClientID %>").value == "5") {
                    window.open("ViewConsolidatedAuditReportObservationWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=btoa(0) &AreaID=btoa(0) &BranchID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
                else {
                    window.open("viewConsolidatedAuditReport.aspx?AuditID=" + btoa(AuditID) + " &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
            }
            else if (TypeID == 2) {
                if (document.getElementById("<%= hdnPostID.ClientID %>").value == "7") {
                    window.open("ViewConsolidatedReportWiseAreaWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
                else if (document.getElementById("<%= hdnPostID.ClientID %>").value == "6") {
                    window.open("ViewConsolidatedReportWiseBranchWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=btoa(0) &AreaID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
                else if (document.getElementById("<%= hdnPostID.ClientID %>").value == "5") {
                    window.open("ViewConsolidatedReportWiseBranchWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=btoa(0) &AreaID=btoa(0) &BranchID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
                else {
                    window.open("ViewConsolidatedReportWiseAuditReport.aspx?AuditID=" + btoa(AuditID) + " &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &Rtype=" + RTypeID, "_self");
                }
            }
        }

    

// ]]>
    </script>
</asp:Content>

