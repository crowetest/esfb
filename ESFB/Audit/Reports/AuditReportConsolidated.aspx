﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditReportConsolidated.aspx.vb" Inherits="Audit_Reports_AuditReportConsolidated" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
 <table align="center" style="width:40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                  <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Type</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="1">AUDIT END DATE</asp:ListItem>
                    <asp:ListItem Value="2">AUDIT PERIOD</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px;">
                From</td>
            <td  style="width:35%; height: 18px;">
            <asp:TextBox ID="txtFrom" class="NormalText" runat="server" Width="75%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtFrom" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
            <td  style="width:15%; height: 18px;">
                To</td>
            <td  style="width:35%; height: 18px;">
            <asp:TextBox ID="txtTo" class="NormalText" runat="server" Width="75%" 
                MaxLength="100" Height="20px" ReadOnly="true"></asp:TextBox>
             <asp:CalendarExtender ID="CEToDate" runat="server" 
                    TargetControlID="txtTo" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px;">
                Region</td>
            <td  style="height: 18px;" colspan="3">
                <asp:DropDownList ID="cmbRegion" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black"><asp:ListItem Value="-1">ALL</asp:ListItem>
                 </asp:DropDownList>
                </td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px;">
                Area</td>
            <td  style="height: 18px;" colspan="3">
                <asp:DropDownList ID="cmbArea" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black"><asp:ListItem Value="-1">ALL</asp:ListItem>
                  </asp:DropDownList>
                </td>
        </tr>
        <tr>
            <td style="width:15%; height: 12px;">
                Branch</td>
            <td  style="height: 12px;" colspan="3">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black"><asp:ListItem Value="-1">ALL</asp:ListItem>

                </asp:DropDownList>
                </td>
        </tr>
        <tr>
            
            <td  style="height: 18px;" colspan="4">
                 <input id="chkSPM" type="checkbox" /> &nbsp;SPM Related Only&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="chkObserv" type="checkbox" />&nbsp;Checklist With Observations Only&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="chkSummary" type="checkbox" />&nbsp; Include Summary</td>
        </tr>
           <tr>
            <td style="width:15%; height: 19px; text-align:center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()"  />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
            </td>
        </tr></table>
   
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function FromServer(arg, context) {

            switch (context) {
                case 1:
                    ComboFill(arg, "<%= cmbArea.ClientID %>");
                    break;
                case 2:
                    ComboFill(arg, "<%= cmbBranch.ClientID %>");
                    break;
            }

        }

        function RegionOnChange() {
            var RegionID = document.getElementById("<%= cmbRegion.ClientID %>").value;
            if (RegionID != -1) {
                var Data = "1Ø" + RegionID
                ToServer(Data, 1);
            }
            else {
                ClearCombo("<%= cmbArea.ClientID %>");
                ClearCombo("<%= cmbBranch.ClientID %>");
            }
        }
        function AreaOnChange() {
            var AreaID = document.getElementById("<%= cmbArea.ClientID %>").value;
            if (AreaID != -1) {
                var Data = "2Ø" + AreaID
                ToServer(Data, 2);
            }
            else {
                ClearCombo("<%= cmbBranch.ClientID %>");
            }
        }
   
       function btnView_onclick() {
           var TypeID = document.getElementById("<%= cmbType.ClientID %>").value;
           var RegionID = document.getElementById("<%= cmbRegion.ClientID %>").value;
           var AreaID = document.getElementById("<%= cmbArea.ClientID %>").value;
           var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;          
            var FromDt = document.getElementById("<%= txtFrom.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtTo.ClientID %>").value;
            var SPMFlag = 0;
            var ObsFlag = 0;
            var SummaryFlag = 0;
            if (document.getElementById("chkSPM").checked == true)
                SPMFlag = 1;
            if (document.getElementById("chkObserv").checked == true)
                ObsFlag = 1;
            if (document.getElementById("chkSummary").checked == true)
                SummaryFlag = 1;
            window.open("viewAuditReportCons.aspx?TypeID=" + btoa(TypeID) + " &RegionID=" + btoa(RegionID) + " &AreaID=" + btoa(AreaID) + " &BranchID=" + btoa(BranchID) + " &FromDt=" + FromDt + " &ToDt=" + ToDt + " &SPMFlag=" + SPMFlag + " &ObsFlag=" + ObsFlag + " &SummaryFlag=" + SummaryFlag, "_blank");
            
        }

        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = "ALL";
            document.getElementById(control).add(option1);
        }

// ]]>
    </script>
</asp:Content>

