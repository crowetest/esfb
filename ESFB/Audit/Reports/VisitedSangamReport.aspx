﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="VisitedSangamReport.aspx.vb" Inherits="Audit_Reports_VisitedSangamReport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr  style="display:none;" >
            <td colspan="4" style="text-align:center;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center;">
                <input id="rdAudit" type="radio" checked="checked" 
                    onclick="return OptionOnClick()" name="rr" />&nbsp;Audit&nbsp;&nbsp;<input 
                    id="rdDateRange" type="radio"  onclick="return OptionOnClick()"
                    name="rr" />&nbsp;Date Range&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center;">
                &nbsp;</td>
        </tr>
        <tr id="rowAudit"  >
            <td style="width:15%;">
                Audit</td>
            <td  style="width:85%;" colspan="3">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr id="rowDate"  style="display:none;">
            <td style="width:15%;">
                From</td>
            <td  style="width:35%;">
                <asp:TextBox ID="txtFromDt" runat="server" class="NormalText" Width="90%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CE1" runat="server" TargetControlID="txtFromDt" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
            <td  style="width:15%;">
                To</td>
            <td  style="width:35%;">
                <asp:TextBox ID="txtToDt" runat="server" class="NormalText" Width="90%"  
                    ReadOnly="true"></asp:TextBox> 
                <asp:CalendarExtender ID="CE2" runat="server" 
                    TargetControlID="txtToDt" Format="dd MMM yyyy"></asp:CalendarExtender>
                </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="4">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;" colspan="3">
                &nbsp;</td>
        </tr>
    </table>
    <script src="../../Script/Validations.js" type="text/javascript"></script>   
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var RptID = 1;
            if (document.getElementById("rdDateRange").checked == true)
                RptID = 2; 
            if (RptID == 1) {
                var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
                if (AuditID == "-1")
                { alert("Select Audit"); document.getElementById("<%= cmbAudit.ClientID %>").focus(); return false; }
                window.open("ViewVisitedSangamsConsolidated.aspx?RptID=1 &AuditID=" + btoa(AuditID) + "", "_self");
            }
            else {
                var FromDt=document.getElementById("<%= txtFromDt.ClientID %>").value;
                var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;               
                if (FromDt == "")
                {alert("Select From Date");return false;}
                if (ToDt == "")
                {alert("Select To Date");return false; }
                var DiffDays = getDateDiff(FromDt, ToDt, "days");
                if(DiffDays<0)
                {alert("Check Dates");return false;}
                window.open("ViewVisitedSangamsConsolidated.aspx?RptID=2 &FromDt=" + FromDt + " &ToDt="+ToDt, "_self");
            }
        }

        function OptionOnClick() {
            if (document.getElementById("rdAudit").checked == true) {
                document.getElementById("rowAudit").style.display = "";
                document.getElementById("rowDate").style.display = "none";
            }
            else {
                document.getElementById("rowAudit").style.display = "none";
                document.getElementById("rowDate").style.display = "";
            }
        }
    
// ]]>
    </script>
</asp:Content>

