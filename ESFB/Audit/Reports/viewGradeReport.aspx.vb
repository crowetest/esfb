﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewGradeReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim AuditTypeID As Integer
    Dim TypeID As Integer
    Dim RptID As Integer
    Dim From As Integer
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))

            TypeID = CInt(GF.Decrypt(Request.QueryString.Get("TypeID")))
            RptID = CInt(GF.Decrypt(Request.QueryString.Get("RptID")))
            Dim Substr As String = ""
            If TypeID = 1 Then
                Substr = "MONTHLY"
            ElseIf TypeID = 3 Then
                Substr = "QUARTERLY"
            ElseIf TypeID = 6 Then
                Substr = "HALF YEARLY"
            ElseIf TypeID = 12 Then
                Substr = "YEARLY"
            ElseIf TypeID = 15 Then
                Substr = "MANUAL"
            End If
            RH.Heading(Session("FirmName"), tb, Substr + " GRADING REPORT ", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = AD.GradingReport(TypeID, PostID, CInt(Session("UserID")), RptID)
            If Not IsNothing(DT) Then
                Dim TRHead As New TableRow
                Dim colWidth As Integer = 95 - ((DT.Columns.Count - 2) * 5)
                Dim TRHead_00, TRHead_01 As New TableCell
                TRHead_00.BorderWidth = "1"
                TRHead_00.BorderColor = Drawing.Color.Silver
                TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderWidth = "1"
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "SI No")
                RH.AddColumn(TRHead, TRHead_01, colWidth, colWidth, "l", "Branch")
                For i = 2 To DT.Columns.Count - 1
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_02 As New TableCell
                    TRHead_02.BorderWidth = "1"
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    If TypeID = 15 Then
                        RH.AddColumn(TRHead, TRHead_02, 5, 5, "c", CDate(DT.Columns(i).ColumnName).ToString("dd MMM yyyy"))
                    Else
                        RH.AddColumn(TRHead, TRHead_02, 5, 5, "c", DateAdd(DateInterval.Day, -1, CDate(DT.Columns(i).ColumnName)).ToString("MMM yyyy"))
                    End If
                Next
                Dim wdth As Integer = 0
                Dim aln As String
                Dim dispText As String
                tb.Controls.Add(TRHead)
                Dim J As Integer = 0
                For Each DR In DT.Rows
                    Dim TR01 As New TableRow
                    Dim TR01_SINo, TR01_Branch As New TableCell
                    TR01_SINo.BorderWidth = "1"
                    TR01_SINo.BorderColor = Drawing.Color.Silver
                    TR01_SINo.BorderStyle = BorderStyle.Solid
                    TR01_Branch.BorderWidth = "1"
                    TR01_Branch.BorderColor = Drawing.Color.Silver
                    TR01_Branch.BorderStyle = BorderStyle.Solid
                    J += 1
                    RH.AddColumn(TR01, TR01_SINo, 5, 5, "c", J.ToString())
                    RH.AddColumn(TR01, TR01_Branch, colWidth, colWidth, "l", DR(1))
                    For i = 2 To DT.Columns.Count - 1
                        TR01.BackColor = Drawing.Color.WhiteSmoke
                        Dim TR01_01 As New TableCell
                        TR01_01.BorderWidth = "1"
                        TR01_01.BorderColor = Drawing.Color.Silver
                        TR01_01.BorderStyle = BorderStyle.Solid
                        wdth = 5
                        aln = "c"
                        If (Not IsDBNull(DR(i))) Then
                            If RptID = 1 Then
                                If (CDbl(DR(i)) = 0) Then
                                    dispText = "-"
                                Else
                                    dispText = "<a href='viewGradeStatusDtl_Individual.aspx?BranchID=" + GF.Encrypt(DR(0)) + " &TraDt=" + DT.Columns(i).ColumnName + " &TypeID=" + TypeID.ToString() + "' target='blank'>" + CDbl(DR(i)).ToString("##0")
                                End If
                            Else
                                dispText = "<a href='viewGradeStatusDtl_Individual.aspx?BranchID=" + GF.Encrypt(DR(0)) + " &TraDt=" + DT.Columns(i).ColumnName + " &TypeID=" + TypeID.ToString() + "' target='blank'>" + DR(i).ToString()
                            End If
                        Else
                            dispText = "-"
                        End If
                        RH.AddColumn(TR01, TR01_01, wdth, wdth, aln, dispText)
                    Next
                    tb.Controls.Add(TR01)
                Next
            End If
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
