﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewBranchGradeStatusDtl
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim BranchID As Integer
    Dim AuditTypeID As Integer
    Dim From As Integer
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            BranchID = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))

            RH.Heading(Session("FirmName"), tb, "BRANCH GRADING STATUS DETAIL", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = AD.GradingStatusDtl(BranchID)
            Dim TRHead As New TableRow
            Dim colWidth As Integer = 92 - ((DT.Columns.Count - 2) * 8)
            Dim TRHead_00 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_00.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRHead, TRHead_00, colWidth, colWidth, "l", "Branch")
            For i = 2 To DT.Columns.Count - 1
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_01 As New TableCell
                TRHead_01.BorderWidth = "1"
                TRHead_01.BorderColor = Drawing.Color.Silver
                TRHead_01.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TRHead, TRHead_01, 8, 8, "c", DT.Columns(i).ColumnName)
            Next
            Dim TRHead_Total As New TableCell
            TRHead_Total.BorderWidth = "1"
            TRHead_Total.BorderColor = Drawing.Color.Silver
            TRHead_Total.BorderStyle = BorderStyle.Solid
            RH.AddColumn(TRHead, TRHead_Total, 8, 8, "c", "Total")
            Dim wdth As Integer = 0
            Dim aln As String
            Dim Tot As Integer
            Dim dispText As String
            Dim total As Integer
            tb.Controls.Add(TRHead)
            For Each DR In DT.Rows
                Dim TR01 As New TableRow
                total = 0
                For i = 1 To DT.Columns.Count - 1
                    Dim TR01_01 As New TableCell
                    TR01_01.BorderWidth = "1"
                    TR01_01.BorderColor = Drawing.Color.Silver
                    TR01_01.BorderStyle = BorderStyle.Solid
                    Tot = 0
                    If i = 1 Then
                        wdth = colWidth
                        aln = "l"
                        dispText = DR(i).ToString()
                    Else
                        wdth = 8
                        aln = "c"
                        If (DR(i) > 0) Then
                            dispText = DR(i).ToString()
                            Tot = CInt(DR(i))
                        Else
                            dispText = "-"
                        End If
                        total += CInt(DR(i))
                    End If
                    RH.AddColumn(TR01, TR01_01, wdth, wdth, aln, dispText)
                Next
                Dim TR01_Total As New TableCell
                TR01_Total.BorderWidth = "1"
                TR01_Total.BorderColor = Drawing.Color.Silver
                TR01_Total.BorderStyle = BorderStyle.Solid
                RH.AddColumn(TR01, TR01_Total, 8, 8, "c", total)
                tb.Controls.Add(TR01)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
