﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementBranchWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim AD As New Audit
    Dim AreaID As Integer
    Dim CloseFlag As Integer
    Dim GN As New GeneralFunctions
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))

            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))
            CloseFlag = Request.QueryString.Get("CloseFlag")
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If
            Dim Area_Name As String = GN.GetArea_Name(AreaID)
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim SubHD As String = ""
            RH.Heading(Session("FirmName"), tb, "Staff Involvement Report" + Area_Name + " Area " + Status, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")



            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_01, 35, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_02, 15, 2, "No Of Cases")
            RH.InsertColumn(TRHead, TRHead_03, 15, 2, "No Of Observations")
            RH.InsertColumn(TRHead, TRHead_04, 15, 2, "No of Staffs")
            RH.InsertColumn(TRHead, TRHead_05, 15, 1, "Est. Amt. Involved")

            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            Dim TotObservation As Integer = 0
            Dim TotStaff As Integer = 0
            Dim TotLeakage As Integer = 0
            Dim TotCases As Integer = 0
            DT = AD.GetStaffInvolvementBranch(Month, year, PostID, CInt(Session("UserID")), AreaID, CloseFlag, FromDt, ToDt)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, j.ToString())
                RH.InsertColumn(TR3, TR3_01, 35, 0, "<a href='viewStaffInvolvementObservationsWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &BranchID=" + GN.Encrypt(DR(0)) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='_blank'>" + DR(1) + "</a>")
                If DR(5) = 0 Then
                    RH.InsertColumn(TR3, TR3_02, 15, 2, "-")
                Else
                    RH.InsertColumn(TR3, TR3_02, 15, 2, DR(5).ToString())
                End If
                If DR(2) = 0 Then
                    RH.InsertColumn(TR3, TR3_03, 15, 2, "-")
                Else
                    RH.InsertColumn(TR3, TR3_03, 15, 2, DR(2).ToString())
                End If
                If DR(3) = 0 Then
                    RH.InsertColumn(TR3, TR3_04, 15, 2, "-")
                Else
                    RH.InsertColumn(TR3, TR3_04, 15, 2, DR(3).ToString())
                End If
                If DR(4) = 0 Then
                    RH.InsertColumn(TR3, TR3_05, 15, 1, "-")
                Else
                    RH.InsertColumn(TR3, TR3_05, 15, 1, CDbl(DR(4)).ToString("#,##,###.00"))
                End If



                tb.Controls.Add(TR3)
                TotObservation += DR(2)
                TotLeakage += DR(4)

            Next
            TotStaff = AD.GetTotalStaffInvolved(Month, year, PostID, CInt(Session("UserID")), 0, AreaID, 0, 0, 0, CloseFlag, FromDt, ToDt)
            TotCases = AD.GetTotalCasesInvolved(Month, year, PostID, CInt(Session("UserID")), 0, AreaID, 0, 0, 0, CloseFlag, FromDt, ToDt)
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")

            RH.InsertColumn(TRFooter, TRFooter_00, 5, 2, "")
            RH.InsertColumn(TRFooter, TRFooter_01, 35, 0, "Total")
            If TotCases > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_02, 15, 2, "<a href='viewStaffInvolvementChecklisWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt("0") + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='_blank'>" + TotCases.ToString() + "</a>")
                ' RH.InsertColumn(TRFooter, TRFooter_02, 15, 2, TotCases.ToString())
            Else
                RH.InsertColumn(TRFooter, TRFooter_02, 15, 2, "")
            End If

            If TotObservation > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_03, 15, 2, "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt("0") + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='_blank'>" + TotObservation.ToString() + "</a>")
            Else
                RH.InsertColumn(TRFooter, TRFooter_03, 15, 2, "")
            End If

            If TotStaff > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_04, 15, 2, "<a href='viewStaffInvolvementEmpWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt("0") + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='_blank'>" + TotStaff.ToString() + "</a>")
            Else
                RH.InsertColumn(TRFooter, TRFooter_04, 15, 2, "")
            End If
            If TotLeakage > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_05, 15, 1, TotLeakage.ToString("#,##,###.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_05, 15, 1, "")
            End If

            tb.Controls.Add(TRFooter)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
