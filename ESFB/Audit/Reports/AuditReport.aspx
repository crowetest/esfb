﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditReport.aspx.vb" Inherits="Audit_Reports_AuditReport" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:15%;">
                Branch</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                Audit</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr id="rowStatus" style="display:none">
            <td style="width:15%;">
                Status</td>
            <td  style="width:85%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                    <asp:ListItem Value="1">PENDING</asp:ListItem>                  
                    <asp:ListItem Value="2">CLOSED</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%; height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            if (BranchID == -1)
            { alert("Select Branch"); document.getElementById("<%= cmbBranch.ClientID %>").focus(); return false; }
            if (AuditID == -1)
            { alert("Select Audit"); document.getElementById("<%= cmbAudit.ClientID %>").focus(); return false; }
            var ReportID = document.getElementById("<%= hdnReportID.ClientID %>").value;
            if (ReportID == 1)
                window.open("viewAuditFinalReport.aspx?AuditID=" + btoa(AuditID) + " &ReportID=" + btoa(document.getElementById("<%= hdnReportID.ClientID %>").value), "_self");
            else if (ReportID == 2) {
                var StatusID = document.getElementById("<%= cmbStatus.ClientID %>").value;
                window.open("viewComplianceStatusReport.aspx?AuditID=" + btoa(AuditID) + " &ReportID=" + btoa(document.getElementById("<%= hdnReportID.ClientID %>").value) + " &StatusID=" + StatusID, "_self");
            }
            else if (ReportID == 3)
                window.open("viewAuditFinalReportExport.aspx?AuditID=" + btoa(AuditID) + " &ReportID=" + btoa(document.getElementById("<%= hdnReportID.ClientID %>").value), "_self");
        }
        function BranchOnChange() {
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            if (BranchID == -1)
                ClearCombo("<%= cmbAudit.ClientID %>");
            else {
                var ToData = "1Ø" + BranchID;
                ToServer(ToData, 1);
            }
        }
        function FromServer(arg, context) {

            switch (context) {
                case 1:
                    ComboFill(arg, "<%= cmbAudit.ClientID %>");
                    break;
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

        function DisplaySettings() {
            var ReportID = document.getElementById("<%= hdnReportID.ClientID %>").value;
            if (ReportID == 2)
                document.getElementById("rowStatus").style.display = ""
            else
                document.getElementById("rowStatus").style.display = "none"
        }
    </script>
</asp:Content>

