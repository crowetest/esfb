﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewVisitedAssignCenters
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim MonthDesc As String
    Dim subHead As String = ""
    Dim FromDt As Date
    Dim ToDt As Date
    Dim RptID As Integer
    Dim BranchID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AuditID = CInt(GF.Decrypt(Request.QueryString.Get("AuditID")))
            BranchID = CInt(GF.Decrypt(Request.QueryString.Get("BranchID")))

            MonthDesc = Request.QueryString.Get("AssignDate")
            FromDt = Request.QueryString.Get("FromDt")
            ToDt = Request.QueryString.Get("ToDt")
            RptID = CInt(Request.QueryString.Get("RptID"))

            Dim AssignDtl() As String = MonthDesc.Split("~")

            hdnAssingDt.Value = MonthDesc.ToString()
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            subHead = Request.QueryString.Get("SubHD")
            RH.Heading(Session("FirmName"), tb, subHead, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 20, 0, "Center ID")
            RH.InsertColumn(TRHead, TRHead_02, 40, 0, "Center Name")
            RH.InsertColumn(TRHead, TRHead_03, 35, 0, "Meeting Day")

            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 5)    
            If RptID = 1 Then
                DT = DB.ExecuteDataSet("select distinct b.CENTER_ID,c.CENTER_NAME,DATENAME(DW,c.MEETING_DAY-2)as MeetingDay from AUDIT_ACCOUNTS a,LOAN_MASTER b,CENTER_MASTER  c where a.LOAN_NO=b.LOAN_NO and b.CENTER_ID=c.CENTER_ID and a.AUDIT_ID=" + AuditID.ToString()).Tables(0)
            Else
                DT = DB.ExecuteDataSet("select distinct b.CENTER_ID,c.CENTER_NAME,DATENAME(DW,c.MEETING_DAY-2)as MeetingDay from AUDIT_ACCOUNTS a,LOAN_MASTER b,CENTER_MASTER  c,audit_dtl d,audit_master e where a.LOAN_NO=b.LOAN_NO and b.CENTER_ID=c.CENTER_ID and a.audit_id=d.audit_id and d.group_id=e.group_id and d.audit_type in(2,4) and e.Branch_ID=" + BranchID.ToString() + " and e.end_dt between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'").Tables(0)
            End If



            For Each DR In DT.Rows


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell
                I = I + 1
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString().ToString())
                RH.InsertColumn(TR3, TR3_01, 20, 0, DR(0).ToString())
                RH.InsertColumn(TR3, TR3_02, 40, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 35, 0, DR(2).ToString())

                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
