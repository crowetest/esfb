﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewmonthwiseReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim DayType As Integer = CInt(Request.QueryString.Get("DayType"))
            Dim Days As Integer = CInt(Request.QueryString.Get("Days"))
            Dim OptGreat As Integer = CInt(Request.QueryString.Get("OptGreat"))
            Dim Type As Integer = CInt(GF.Decrypt(Request.QueryString.Get("Type")))

            Dim AuditID As String = CStr(GF.Decrypt(Request.QueryString.Get("AuditID")))

            Dim DTHead As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable

            RH.Heading(Session("FirmName"), tb, "REGION WISE AGING REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)


            tb.Controls.Add(TRItemHead)
            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 10, 10, "l", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 18, 18, "l", "Region")
            DTHead = AD.AGINGREPORT(Type, "R", DayType, OptGreat, Days, 0, 0, 1, 0, AuditID, CInt(Session("UserID")), CInt(Session("Post_ID")))
            If DTHead.Rows.Count > 0 Then
                RH.AddColumn(TRHead, TRHead_02, 12, 12, "l", DTHead.Rows(0).Item(0))
                RH.AddColumn(TRHead, TRHead_03, 12, 12, "l", DTHead.Rows(0).Item(1))
                RH.AddColumn(TRHead, TRHead_04, 12, 12, "l", DTHead.Rows(0).Item(2))
                RH.AddColumn(TRHead, TRHead_05, 12, 12, "l", DTHead.Rows(0).Item(3))
                RH.AddColumn(TRHead, TRHead_06, 12, 12, "l", DTHead.Rows(0).Item(4))
                RH.AddColumn(TRHead, TRHead_07, 12, 12, "c", "<b>Total</b>")
            End If


            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            DT = AD.AGINGREPORT(Type, "R", DayType, OptGreat, Days, 0, 0, 0, 0, AuditID, CInt(Session("UserID")), CInt(Session("Post_ID")))
            Dim Val As String
            Dim Total1 As Integer = 0
            Dim Total2 As Integer = 0
            Dim Total3 As Integer = 0
            Dim Total4 As Integer = 0
            Dim Total5 As Integer = 0
            Dim GrandTotal As Integer = 0
            For Each DR In DT.Rows

                j += 1

                I = 1

                Dim h_Total As Integer = 0

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_07.BackColor = Drawing.Color.WhiteSmoke
                Total1 += DR(3)
                Total2 += DR(4)
                Total3 += DR(5)
                Total4 += DR(6)
                Total5 += DR(7)
                h_Total += DR(3) + DR(4) + DR(5) + DR(6) + DR(7)
                GrandTotal += h_Total
                RH.AddColumn(TR3, TR3_00, 10, 10, "l", DR(0))
                RH.AddColumn(TR3, TR3_01, 18, 18, "l", "<a href='ViewAreaBucketwiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + " &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + DR(2).ToString + "</a>")
                Val = IIf(DR(3) > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(0).ToString + "  &Total=0 &DetailSearch=1 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + DR(3).ToString + "</a>", DR(3))
                RH.AddColumn(TR3, TR3_02, 12, 12, "c", Val)
                Val = IIf(DR(4) > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(1).ToString + "  &Total=0 &DetailSearch=2 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + DR(4).ToString + "</a>", DR(4))
                RH.AddColumn(TR3, TR3_03, 12, 12, "c", Val)
                Val = IIf(DR(5) > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(2).ToString + "  &Total=0 &DetailSearch=3 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + DR(5).ToString + "</a>", DR(5))
                RH.AddColumn(TR3, TR3_04, 12, 12, "c", Val)
                Val = IIf(DR(6) > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(3).ToString + "  &Total=0 &DetailSearch=4 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + DR(6).ToString + "</a>", DR(6))
                RH.AddColumn(TR3, TR3_05, 12, 12, "c", Val)
                Val = IIf(DR(7) > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(4).ToString + "  &Total=0 &DetailSearch=5 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + DR(7).ToString + "</a>", DR(7))
                RH.AddColumn(TR3, TR3_06, 12, 12, "c", Val)
                RH.AddColumn(TR3, TR3_07, 12, 12, "c", "<a href='ViewAreaBucketAuditWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" + GF.Encrypt(DR(1)) + "&Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + " &OptGreat=" + OptGreat.ToString() + "' target='_blank'><b>" + h_Total.ToString() + "</b></a>")
                tb.Controls.Add(TR3)
                I = I + 1
            Next
            Dim Val1 As String
            Dim TRTot As New TableRow
            TRTot.BorderWidth = "1"
            TRTot.BorderStyle = BorderStyle.Solid
            TRTot.Style.Add("font-weight", "bold")
            TRTot.Style.Add("background-color", "whitesmoke")
            Dim TRTot_00, TRTot_01, TRTot_02, TRTot_03, TRTot_04, TRTot_05, TRTot_06, TRTot_07 As New TableCell

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"
            TRTot_03.BorderWidth = "1"
            TRTot_04.BorderWidth = "1"
            TRTot_05.BorderWidth = "1"
            TRTot_06.BorderWidth = "1"
            TRTot_07.BorderWidth = "1"

            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver
            TRTot_03.BorderColor = Drawing.Color.Silver
            TRTot_04.BorderColor = Drawing.Color.Silver
            TRTot_05.BorderColor = Drawing.Color.Silver
            TRTot_06.BorderColor = Drawing.Color.Silver
            TRTot_07.BorderColor = Drawing.Color.Silver

            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid
            TRTot_03.BorderStyle = BorderStyle.Solid
            TRTot_04.BorderStyle = BorderStyle.Solid
            TRTot_05.BorderStyle = BorderStyle.Solid
            TRTot_06.BorderStyle = BorderStyle.Solid
            TRTot_07.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRTot, TRTot_00, 10, 10, "l", "")
            RH.AddColumn(TRTot, TRTot_01, 18, 18, "l", "Total")
            Val1 = IIf(Total1 > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" & GF.Encrypt("0") & " &Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(0).ToString + " &Total=1 &DetailSearch=1 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + Total1.ToString() + "</a>", Total1.ToString())
            RH.AddColumn(TRTot, TRTot_02, 12, 12, "c", Val1)
            Val1 = IIf(Total2 > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" & GF.Encrypt("0") & " &Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(1).ToString + " &Total=1 &DetailSearch=2 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + Total2.ToString() + "</a>", Total2.ToString())
            RH.AddColumn(TRTot, TRTot_03, 12, 12, "c", Val1)
            Val1 = IIf(Total3 > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" & GF.Encrypt("0") & " &Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(2).ToString + " &Total=1 &DetailSearch=3 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + Total3.ToString() + "</a>", Total3.ToString())
            RH.AddColumn(TRTot, TRTot_04, 12, 12, "c", Val1)
            Val1 = IIf(Total4 > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" & GF.Encrypt("0") & " &Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(3).ToString + " &Total=1 &DetailSearch=1 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + Total4.ToString() + "</a>", Total4.ToString())
            RH.AddColumn(TRTot, TRTot_05, 12, 12, "c", Val1)
            Val1 = IIf(Total5 > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" & GF.Encrypt("0") & " &Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=" + Days.ToString() + "&head=" + DTHead.Rows(0).Item(4).ToString + " &Total=1 &DetailSearch=5 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + Total5.ToString() + "</a>", Total5.ToString())
            RH.AddColumn(TRTot, TRTot_06, 12, 12, "c", Val1)
            Val1 = IIf(GrandTotal > 0, "<a href='ViewAreaSummaryBucketWiseReport.aspx?AuditID=" + GF.Encrypt(AuditID.ToString) + "&REGID=" & GF.Encrypt("0") & " &Type=" + GF.Encrypt(Type.ToString()) + " &DayType=" + DayType.ToString() + " &Days=0 &head=0 &Total=1 &DetailSearch=0 ' &OptGreat=" + OptGreat.ToString() + "' target='_blank'>" + GrandTotal.ToString() + "</a>", GrandTotal.ToString())
            RH.AddColumn(TRTot, TRTot_07, 12, 12, "c", Val1)
            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)

            Dim TR_Note As New TableRow
            Dim TR_Note_00 As New TableCell
            RH.AddColumn(TR_Note, TR_Note_00, 100, 100, "l", "* Both Days Inclusive")
            tb.Controls.Add(TR_Note)
            RH.BlankRow(tb, 5)
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
