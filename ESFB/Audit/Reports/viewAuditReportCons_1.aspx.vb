﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewAuditReportCons_1
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim ItemID As Integer
    Dim TypeID As Integer
    Dim AuditTypeID As Integer
    Dim AD As New Audit
    Dim SPMFlag As Integer = 0
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            TypeID = CInt(Request.QueryString.Get("TypeID"))
            ItemID = CInt(Request.QueryString.Get("ItemID"))
            AuditTypeID = CInt(Request.QueryString.Get("AuditTypeID"))
            SPMFlag = CInt(Request.QueryString.Get("SPMFlag"))
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            If AuditTypeID = 1 Or AuditTypeID = 2 Then
                '                                  0           1       2           3               4               5           6           7           8           9           10                                          11                        12                        13          14        15                      16                17            18        
                Dim sqlStr As String = "select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,dbo.udfPropercase(f.CLIENT_NAME),isnull(f.disbursed_Dt,f.sanctioned_dt),f.loan_amt,b.Fraud,b.Fin_Leakage,dbo.udfPropercase(g.Center_Name),b.susp_Leakage,b.SINo,j.branch_name+' '+ datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4)) from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,center_master g,audit_dtl h,audit_master i,brmaster j where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and f.center_ID=g.Center_ID and a.audit_id=h.audit_id and h.group_id=i.group_id and i.branch_id=j.branch_id and i.status_id=2 and a.ITEM_ID=" + ItemID.ToString()
                If BranchID > 0 Then
                    sqlStr += " and j.branch_id=" + BranchID.ToString()
                ElseIf AreaID > 0 Then
                    sqlStr += " and j.area_id=" + AreaID.ToString()
                ElseIf RegionID > 0 Then
                    sqlStr += " and j.region_id=" + RegionID.ToString()
                End If
                If TypeID = 1 Then
                    sqlStr += " and i.end_dt between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                Else
                    sqlStr += " and i.period_to between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                End If
                sqlStr += " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID"
                DT = DB.ExecuteDataSet(sqlStr).Tables(0)
                Dim hdStr As String
                If BranchID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                    End If

                ElseIf AreaID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                    End If

                ElseIf RegionID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                    End If

                Else
                    If SPMFlag = 0 Then
                        hdStr = "CONSOLIDATED AUDIT OBSERVATION REPORT"
                    Else
                        hdStr = "CONSOLIDATED SPM QUERIES REPORT"
                    End If

                End If
                RH.Heading(Session("FirmName"), tb, hdStr, 100)
                DT1 = AD.GetItemName(ItemID, 0)

                Dim sqlStr1 As String
                sqlStr1 = "select count(distinct i.branch_id)as NoOfBranches,count(distinct h.group_id)as NoOfAudits from AUDIT_OBSERVATION a , AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,AUDIT_DTL h,BrMaster i,audit_Master j where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and a.AUDIT_ID=h.AUDIT_ID and h.BRANCH_ID=i.Branch_ID  and h.group_id=j.group_id and j.status_id=2 "
                If BranchID > 0 Then
                    sqlStr1 += " and i.branch_id=" + BranchID.ToString()
                ElseIf AreaID > 0 Then
                    sqlStr1 += " and i.area_id=" + AreaID.ToString()
                ElseIf RegionID > 0 Then
                    sqlStr1 += " and i.region_id=" + RegionID.ToString()
                End If
                If TypeID = 1 Then
                    sqlStr1 += " and DATEADD(day, DATEDIFF(day, 0, j.END_DT), 0) between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                Else
                    sqlStr1 += " and j.period_to between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                End If
                DTAudit = DB.ExecuteDataSet(sqlStr1).Tables(0)
                Dim TR0 As New TableRow
                Dim TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07 As New TableCell
                RH.AddColumn(TR0, TR0_01, 10, 10, "l", "Branches")
                RH.AddColumn(TR0, TR0_02, 2, 2, "l", " : ")
                RH.AddColumn(TR0, TR0_03, 25, 25, "l", DTAudit.Rows(0)(0).ToString())
                RH.AddColumn(TR0, TR0_04, 26, 26, "l", "")
                RH.AddColumn(TR0, TR0_05, 10, 10, "l", "Audits")
                RH.AddColumn(TR0, TR0_06, 2, 2, "l", " : ")
                RH.AddColumn(TR0, TR0_07, 25, 25, "l", DTAudit.Rows(0)(1).ToString())

                tb.Controls.Add(TR0)

                Dim TR1 As New TableRow
                Dim TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07 As New TableCell
                If TypeID = 1 Then
                    RH.AddColumn(TR1, TR1_01, 10, 10, "l", "Ended From")
                Else
                    RH.AddColumn(TR1, TR1_01, 10, 10, "l", "Period From")
                End If
                RH.AddColumn(TR1, TR1_02, 2, 2, "l", " : ")
                RH.AddColumn(TR1, TR1_03, 25, 25, "l", FromDt.ToString("dd MMM yyyy"))
                RH.AddColumn(TR1, TR1_04, 26, 26, "l", "")
                If TypeID = 1 Then
                    RH.AddColumn(TR1, TR1_05, 10, 10, "l", "Ended To")
                Else
                    RH.AddColumn(TR1, TR1_05, 10, 10, "l", "Period To")
                End If

                RH.AddColumn(TR1, TR1_06, 2, 2, "l", " : ")
                RH.AddColumn(TR1, TR1_07, 25, 25, "l", ToDt.ToString("dd MMM yyyy"))

                tb.Controls.Add(TR1)
                RH.SubHeading(tb, 100, "l", "Check List :" + DT1.Rows(0)(0).ToString())

                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell
                RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
                RH.InsertColumn(TRHead, TRHead_01, 12, 0, "Branch/ Audit")
                RH.InsertColumn(TRHead, TRHead_02, 10, 0, "Ref. No")
                RH.InsertColumn(TRHead, TRHead_03, 12, 0, "Center")
                RH.InsertColumn(TRHead, TRHead_04, 15, 0, "Customer Name")
                RH.InsertColumn(TRHead, TRHead_05, 10, 0, "Eff. Date")
                RH.InsertColumn(TRHead, TRHead_06, 7, 1, "Amount")
                RH.InsertColumn(TRHead, TRHead_07, 22, 0, "Remarks")
                RH.InsertColumn(TRHead, TRHead_08, 7, 0, "Est.Amt. Involved")
                tb.Controls.Add(TRHead)
                Dim i As Integer
                i = 1
                For Each DR In DT.Rows
                    Dim TR3 As New TableRow
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell
                    RH.InsertColumn(TR3, TR3_00, 5, 2, i.ToString())
                    RH.InsertColumn(TR3, TR3_01, 12, 0, DR(18).ToString.ToUpper())
                    RH.InsertColumn(TR3, TR3_02, 10, 0, DR(7))
                    Dim Type As String = ""
                    RH.InsertColumn(TR3, TR3_03, 12, 0, DR(15))
                    RH.InsertColumn(TR3, TR3_04, 15, 0, DR(10))
                    RH.InsertColumn(TR3, TR3_05, 10, 0, CDate(DR(11)).ToString("dd/MM/yyyy"))
                    RH.InsertColumn(TR3, TR3_06, 7, 1, CDbl(DR(12)).ToString())
                    RH.InsertColumn(TR3, TR3_07, 22, 0, DR(9).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(17)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                    RH.InsertColumn(TR3, TR3_08, 7, 1, CDbl(DR(14)).ToString("0"))
                    tb.Controls.Add(TR3)
                    i = i + 1
                Next
                Dim TR55 As New TableRow
                TR55.BorderWidth = "1"
                TR55.BorderStyle = BorderStyle.Solid

                Dim TR55_00, TR55_01 As New TableCell
                TR55_00.BorderWidth = "1"
                TR55_01.BorderWidth = "1"
                TR55_00.BorderColor = Drawing.Color.Silver
                TR55_01.BorderColor = Drawing.Color.Silver
                TR55_00.BorderStyle = BorderStyle.Solid
                TR55_01.BorderStyle = BorderStyle.Solid
                TR55_00.ForeColor = Drawing.Color.DarkBlue
                TR55_01.ForeColor = Drawing.Color.DarkRed
                RH.AddColumn(TR55, TR55_00, 80, 80, "r", "Total Leakage :")
                RH.AddColumn(TR55, TR55_01, 20, 20, "r", 0)
                tb.Controls.Add(TR55)
            ElseIf AuditTypeID = 3 Then
                Dim sqlStr As String = "select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, '',b.POT_FRAUD,b.REMARKS,'','','',b.Fraud,b.Fin_Leakage,'',b.susp_Leakage,b.SINo,j.branch_name+' '+ datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4)) from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,audit_dtl h,audit_master i,brmaster j where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID  and a.audit_id=h.audit_id and h.group_id=i.group_id and i.branch_id=j.branch_id and i.status_id=2 and a.ITEM_ID=" + ItemID.ToString()
                If BranchID > 0 Then
                    sqlStr += " and j.branch_id=" + BranchID.ToString()
                ElseIf AreaID > 0 Then
                    sqlStr += " and j.area_id=" + AreaID.ToString()
                ElseIf RegionID > 0 Then
                    sqlStr += " and j.region_id=" + RegionID.ToString()
                End If
                If TypeID = 1 Then
                    sqlStr += " and i.end_dt between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                Else
                    sqlStr += " and i.period_to between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                End If
                sqlStr += " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID"
                DT = DB.ExecuteDataSet(sqlStr).Tables(0)
                Dim hdStr As String
                If BranchID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                    End If

                ElseIf AreaID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                    End If

                ElseIf RegionID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                    End If

                Else
                    If SPMFlag = 0 Then
                        hdStr = "CONSOLIDATED AUDIT OBSERVATION REPORT"
                    Else
                        hdStr = "CONSOLIDATED SPM QUERIES REPORT"
                    End If

                End If
                RH.Heading(Session("FirmName"), tb, hdStr, 100)
                DT1 = AD.GetItemName(ItemID, 0)
                RH.SubHeading(tb, 100, "l", "Check List :" + DT1.Rows(0)(0).ToString())
                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell
                RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
                RH.InsertColumn(TRHead, TRHead_01, 20, 0, "Branch/Audit")
                RH.InsertColumn(TRHead, TRHead_02, 65, 0, "Remarks")
                RH.InsertColumn(TRHead, TRHead_03, 10, 0, "Est.Amt. Involved")
                tb.Controls.Add(TRHead)
                Dim i As Integer
                i = 1
                For Each DR In DT.Rows
                    Dim TR3 As New TableRow
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell
                    RH.InsertColumn(TR3, TR3_00, 5, 2, i.ToString())
                    RH.InsertColumn(TR3, TR3_01, 20, 0, DR(18).ToString().ToUpper())
                    RH.InsertColumn(TR3, TR3_02, 65, 0, DR(9).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(17)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                    RH.InsertColumn(TR3, TR3_03, 10, 1, CDbl(DR(14)).ToString("0"))
                    tb.Controls.Add(TR3)
                    i = i + 1
                Next
                Dim TR55 As New TableRow
                TR55.BorderWidth = "1"
                TR55.BorderStyle = BorderStyle.Solid

                Dim TR55_00, TR55_01 As New TableCell
                TR55_00.BorderWidth = "1"
                TR55_01.BorderWidth = "1"
                TR55_00.BorderColor = Drawing.Color.Silver
                TR55_01.BorderColor = Drawing.Color.Silver
                TR55_00.BorderStyle = BorderStyle.Solid
                TR55_01.BorderStyle = BorderStyle.Solid
                TR55_00.ForeColor = Drawing.Color.DarkBlue
                TR55_01.ForeColor = Drawing.Color.DarkRed
                RH.AddColumn(TR55, TR55_00, 90, 90, "r", "Total Leakage :")
                RH.AddColumn(TR55, TR55_01, 10, 10, "r", 0)
                tb.Controls.Add(TR55)
            ElseIf AuditTypeID = 4 Then
                Dim sqlStr As String = "select a.AUDIT_ID,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME, b.LOAN_NO,b.POT_FRAUD,b.REMARKS,dbo.udfPropercase(f.CLIENT_NAME),isnull(f.disbursed_Dt,f.sanctioned_dt),f.loan_amt,b.Fraud,b.Fin_Leakage,dbo.udfPropercase(g.Center_Name),b.susp_Leakage,b.SINo,j.branch_name+' '+ datename(MONTH,i.period_to) +'-'+CAST(YEAR(i.period_to) AS VARCHAR(4)) from AUDIT_OBSERVATION a, AUDIT_OBSERVATION_DTL b,AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e,LOAN_MASTER f,center_master g,audit_dtl h,audit_master i,brmaster j where  a.OBSERVATION_ID=b.OBSERVATION_ID and a.ITEM_ID=e.ITEM_ID and e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and b.LOAN_NO=f.Loan_no and f.center_ID=g.Center_ID and a.audit_id=h.audit_id and h.group_id=i.group_id and i.branch_id=j.branch_id and i.status_id=2 and a.ITEM_ID=" + ItemID.ToString()
                If BranchID > 0 Then
                    sqlStr += " and j.branch_id=" + BranchID.ToString()
                ElseIf AreaID > 0 Then
                    sqlStr += " and j.area_id=" + AreaID.ToString()
                ElseIf RegionID > 0 Then
                    sqlStr += " and j.region_id=" + RegionID.ToString()
                End If
                If TypeID = 1 Then
                    sqlStr += " and i.end_dt between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                Else
                    sqlStr += " and i.period_to between '" + FromDt.ToString("dd MMM yyyy") + "' and '" + ToDt.ToString("dd MMM yyyy") + "'"
                End If
                sqlStr += " order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID"
                DT = DB.ExecuteDataSet(sqlStr).Tables(0)
                Dim hdStr As String
                If BranchID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                    End If

                ElseIf AreaID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                    End If

                ElseIf RegionID > 0 Then
                    If SPMFlag = 0 Then
                        hdStr = "AUDIT OBSERVATION REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                    Else
                        hdStr = "SPM QUERIES REPORT - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                    End If

                Else
                    If SPMFlag = 0 Then
                        hdStr = "CONSOLIDATED AUDIT OBSERVATION REPORT"
                    Else
                        hdStr = "CONSOLIDATED SPM QUERIES REPORT"
                    End If

                End If
                RH.Heading(Session("FirmName"), tb, hdStr, 100)
                DT1 = AD.GetItemName(ItemID, 0)
                RH.SubHeading(tb, 100, "l", "Check List :" + DT1.Rows(0)(0).ToString())
                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell
                RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
                RH.InsertColumn(TRHead, TRHead_01, 15, 0, "Branch/Audit")
                RH.InsertColumn(TRHead, TRHead_02, 25, 0, "Center")
                RH.InsertColumn(TRHead, TRHead_03, 45, 0, "Remarks")
                RH.InsertColumn(TRHead, TRHead_04, 10, 0, "Est.Amt. Involved")
                tb.Controls.Add(TRHead)
                Dim i As Integer
                i = 1
                For Each DR In DT.Rows
                    Dim TR3 As New TableRow
                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell
                    RH.InsertColumn(TR3, TR3_00, 5, 2, i.ToString())
                    RH.InsertColumn(TR3, TR3_01, 15, 0, DR(18).ToString().ToUpper())
                    RH.InsertColumn(TR3, TR3_02, 25, 0, DR(15))
                    RH.InsertColumn(TR3, TR3_03, 45, 0, DR(9).ToString() + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(17)) + "' style='text-align:right;' target='_blank' >ViewAll</a>")
                    RH.InsertColumn(TR3, TR3_04, 10, 1, CDbl(DR(14)).ToString("0"))
                    tb.Controls.Add(TR3)
                    i = i + 1
                Next
                Dim TR55 As New TableRow
                TR55.BorderWidth = "1"
                TR55.BorderStyle = BorderStyle.Solid

                Dim TR55_00, TR55_01 As New TableCell
                TR55_00.BorderWidth = "1"
                TR55_01.BorderWidth = "1"
                TR55_00.BorderColor = Drawing.Color.Silver
                TR55_01.BorderColor = Drawing.Color.Silver
                TR55_00.BorderStyle = BorderStyle.Solid
                TR55_01.BorderStyle = BorderStyle.Solid
                TR55_00.ForeColor = Drawing.Color.DarkBlue
                TR55_01.ForeColor = Drawing.Color.DarkRed
                RH.AddColumn(TR55, TR55_00, 90, 90, "r", "Total Leakage :")
                RH.AddColumn(TR55, TR55_01, 10, 10, "r", 0)
                tb.Controls.Add(TR55)

            End If

            Dim TR_Decl As New TableRow
            Dim TR_Decl_00 As New TableCell
            'TR_Decl.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_Decl, TR_Decl_00, 100, 100, "c", "<b> This is a system generated Report and does not require signature </b>")
            tb.Controls.Add(TR_Decl)
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
