﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewObservationPendingDragAreaReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim AreaID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("AreaID")))
            Dim RegionID As Integer = CInt(GF.Encrypt(Request.QueryString.Get("REGID")))
            hid_Region.Value = RegionID
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable



            RH.Heading(Session("FirmName"), tb, "OBSERVATION PENDING REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            DTAudit = DB.ExecuteDataSet("SELECT UPPER(area_name) FROM AREA_MASTER where area_id=" + AreaID.ToString()).Tables(0)



            Dim TRItemHead2 As New TableRow
            TRItemHead2.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead2_00 As New TableCell

            RH.AddColumn(TRItemHead2, TRItemHead2_00, 100, 100, "l", "<b><i>AREA  :-     " & DTAudit.Rows(0)(0).ToString() & " </i></b>")

            tb.Controls.Add(TRItemHead2)

            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)


            tb.Controls.Add(TRItemHead)
            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 15, 15, "l", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 70, 70, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Count")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            DT = DB.ExecuteDataSet(" select ROW_NUMBER() OVER (ORDER BY Branch_Name) AS Row,f.Branch_ID  ,f.Branch_Name   as RPT_DESCR,  COUNT(e.OBSERVATION_ID) as Count from AUDIT_DTL a, " & _
                     " dbo.AUDIT_TYPE b, dbo.AUDIT_OBSERVATION C ,dbo.AUDIT_CHECK_LIST D,dbo.AUDIT_OBSERVATION_DTL E,  BRANCH_MASTER F,AREA_MASTER g,Audit_master i   " & _
                     " WHERE(a.group_ID = I.group_ID And a.AUDIT_TYPE = b.TYPE_ID And I.status_id = 2 And e.status_id = 1 And C.ITEM_ID = D.item_id  " & _
                     " And C.OBSERVATION_ID = e.OBSERVATION_ID) and a.AUDIT_ID=c.AUDIT_ID and  a.BRANCH_ID=f.BRANCH_ID and f.Area_ID=g.Area_ID  " & _
                     " and g.Area_ID =" & AreaID.ToString & " group by f.Branch_ID ,f.Branch_Name ").Tables(0)
            For Each DR In DT.Rows

                j += 1

                I = 1


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 15, 15, "l", DR(0))
                RH.AddColumn(TR3, TR3_01, 70, 70, "l", DR(2))
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", "<a href='ViewObservationPendingDragBranchReport.aspx?BranchID=" + GF.Encrypt(DR(1)) + "&AreaID=" + GF.Encrypt(AreaID.ToString()) + "&REGID=" + GF.Encrypt(RegionID.ToString()) + "'>" + DR(3).ToString + "</a>")
                tb.Controls.Add(TR3)
                I = I + 1
            Next
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
