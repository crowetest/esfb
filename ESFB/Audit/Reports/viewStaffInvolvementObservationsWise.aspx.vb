﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementObservationsWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim AD As New Audit
    Dim BranchID As Integer
    Dim GN As New GeneralFunctions
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim CloseFlag As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))
            CloseFlag = Request.QueryString.Get("CloseFlag")
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If
            Dim Branch_Name As String = GN.GetBranch_Name(BranchID)
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim SubHD As String = ""
            RH.Heading(Session("FirmName"), tb, "Staff Involvement Report" + Branch_Name + " Branch " + Status, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")



            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            'TRHead_05.BorderWidth = "1"
            'TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            'TRHead_05.BorderColor = Drawing.Color.Silver
            'TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            'TRHead_05.BorderStyle = BorderStyle.Solid
            'TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "l", "Observation")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "c", "No Of Findings")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "c", "No of Staffs")
            RH.AddColumn(TRHead, TRHead_04, 20, 20, "c", "Est. Amt. Involved")

            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            Dim TotObservation As Integer = 0
            Dim TotStaff As Integer = 0
            Dim TotLeakage As Integer = 0

            DT = AD.GetStaffInvolvementObservation(Month, year, PostID, CInt(Session("UserID")), BranchID, CloseFlag, FromDt, ToDt)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                'TR3_05.BorderWidth = "1"
                'TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=" + DR(0).ToString() + "&RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt("0") + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &AuditType=" + DR(5).ToString() + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='blank'>" + DR(1) + "</a>")
                If DR(2) = 0 Then
                    RH.AddColumn(TR3, TR3_02, 20, 20, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_02, 20, 20, "c", DR(2).ToString())
                End If
                If DR(3) = 0 Then
                    RH.AddColumn(TR3, TR3_03, 20, 20, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_03, 20, 20, "c", DR(3).ToString())
                End If
                If DR(4) = 0 Then
                    RH.AddColumn(TR3, TR3_04, 20, 20, "r", "-")
                Else
                    RH.AddColumn(TR3, TR3_04, 20, 20, "r", CDbl(DR(4)).ToString("#,##,###.00"))
                End If



                tb.Controls.Add(TR3)
                TotObservation += DR(2)
                TotLeakage += DR(4)
            Next

            TotStaff = AD.GetTotalStaffInvolved(Month, year, PostID, CInt(Session("UserID")), BranchID, 0, 0, 0, 0, CloseFlag, FromDt, ToDt)
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"
            'TRFooter_05.BorderWidth = "1"
            'TRFooter_06.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver

            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 35, 35, "l", "Total")
            If TotObservation > 0 Then
                RH.AddColumn(TRFooter, TRFooter_02, 20, 20, "c", "<a href='viewStaffInvolvementLoanWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &RegionID=" + GN.Encrypt("0") + "&AreaID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='blank'>" + TotObservation.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_02, 20, 20, "c", "")
            End If

            If TotStaff > 0 Then
                RH.AddColumn(TRFooter, TRFooter_03, 20, 20, "c", "<a href='viewStaffInvolvementEmpWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='blank'>" + TotStaff.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_03, 20, 20, "c", "")
            End If
            If TotLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_04, 20, 20, "r", TotLeakage.ToString("#,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_04, 20, 20, "r", "")
            End If
            tb.Controls.Add(TRFooter)


            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
