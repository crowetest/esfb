﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_AuditPendingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            RH.Heading(Session("FirmName"), tb, "AUDIT LAG REPORT", 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim PendingTotal As Integer = 0
            Dim ProgressTotal As Integer = 0
            Dim CompletedTotal As Integer = 0
            Dim Total As Integer = 0
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 27, 27, "l", "Month")
            RH.AddColumn(TRHead, TRHead_02, 17, 17, "c", "Planned")
            RH.AddColumn(TRHead, TRHead_03, 17, 17, "c", "Pending")
            RH.AddColumn(TRHead, TRHead_04, 17, 17, "c", "In Progress")
            RH.AddColumn(TRHead, TRHead_05, 17, 17, "c", "Completed")

            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 5)

            'DT = DB.ExecuteDataSet("select DATENAME(month,Period_to)+'/'+CAST(YEAR(Period_to)as varchar(4)) as AuditMonth ,sum(case when Status_ID in(0,9) then 1 else 0 end) as Pending,sum(case when Status_ID in(1,3) then 1 else 0 end) as InProgress,sum(case when Status_ID=2 then 1 else 0 end) as Completed,count(*) as planned from AUDIT_MASTER where skip_flag=0  group by Period_To  order by Period_To ").Tables(0)

            Dim Str, StrWhere As String
            Dim Post_Id, User_Id As Integer
            Post_Id = CInt(Session("Post_ID"))
            User_Id = CInt(Session("UserID"))
            If Post_Id = 1 Or Post_Id = 2 Then 'Audit Head/TM
                StrWhere = " And 1 = 1 "
                'ElseIf Post_Id = 2 Then 'TM
                '    StrWhere = " And TM =" + User_Id.ToString()
            ElseIf Post_Id = 3 Then 'TL
                StrWhere = " And TL =" + User_Id.ToString()
            ElseIf Post_Id = 4 Then 'Auditor
                StrWhere = " And Auditor =" + User_Id.ToString()
            End If
            Str = "select DATENAME(month,Period_to)+'/'+CAST(YEAR(Period_to)as varchar(4)) as AuditMonth, Sum(Pending), Sum(InProgress), Sum(Completed), Sum(Planned) from (" +
                " select A.branch_id, A.Branch_name, B.Emp_code as TM, C.Team_Lead as TL, C.Emp_code as Auditor from branch_master A " +
                " left join AUDIT_ADMIN_BRANCHES B on A.Branch_id = B.Branch_Id" +
                " left join AUDIT_BRANCHES c on A.Branch_id = c.Branch_Id " +
                " where branch_type in ( 0 ,3) ) A left join " +
                " (select branch_id, Period_to  ,sum(case when Status_ID in(0,9) then 1 else 0 end) as Pending,sum(case when Status_ID in(1,3) then 1 else 0 end) as InProgress,sum(case when Status_ID=2 then 1 else 0 end) as Completed,count(*) as planned " +
                " from AUDIT_MASTER where skip_flag=0   " +
                " group by Period_To,branch_id " +
                " ) B on A.Branch_id = B.Branch_Id " +
                " where(B.branch_id Is Not null) " + StrWhere +
                " group by Period_to " +
                " order by Period_to "
            DT = DB.ExecuteDataSet(Str).Tables(0)
            For Each DR In DT.Rows

                LineNumber += 1
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 27, 27, "l", DR(0).ToString())
                If (CInt(DR(4) > 0)) Then
                    RH.AddColumn(TR3, TR3_02, 17, 17, "c", " <a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("-1") & " &MonthDesc=" + DR(0) + " &SubHD=Audit Pending Report - " + DR(0) + " &UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + DR(4).ToString())
                Else
                    RH.AddColumn(TR3, TR3_02, 17, 17, "c", DR(4).ToString())
                End If
                If (CInt(DR(1) > 0)) Then
                    RH.AddColumn(TR3, TR3_03, 17, 17, "c", " <a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("0") & " &MonthDesc=" + DR(0) + " &SubHD=Audit Pending Report - " + DR(0) + " &UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + DR(1).ToString())
                Else
                    RH.AddColumn(TR3, TR3_03, 17, 17, "c", DR(1).ToString())
                End If
                If CInt(DR(2)) > 0 Then
                    RH.AddColumn(TR3, TR3_04, 17, 17, "c", " <a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("1") & " &MonthDesc=" + DR(0) + " &SubHD=Audit In Progress Report - " + DR(0) + " &UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + DR(2).ToString())
                Else
                    RH.AddColumn(TR3, TR3_04, 17, 17, "c", DR(2).ToString())
                End If

                If CInt(DR(3)) > 0 Then
                    RH.AddColumn(TR3, TR3_05, 17, 17, "c", " <a href='AuditCompletionReport.aspx?StatusID= " & GF.Encrypt("2") & " &MonthDesc=" + DR(0) + " &SubHD=Audit Completion Report - " + DR(0) + " &UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + DR(3).ToString())
                Else
                    RH.AddColumn(TR3, TR3_05, 17, 17, "c", DR(3).ToString())
                End If

                PendingTotal += CInt(DR(1))
                ProgressTotal += CInt(DR(2))
                CompletedTotal += CInt(DR(3))
                Total += CInt(DR(4))
                tb.Controls.Add(TR3)
            Next
            Dim TREnd As New TableRow
            TREnd.BackColor = Drawing.Color.WhiteSmoke
            Dim TREnd_00, TREnd_01, TREnd_02, TREnd_03, TREnd_04, TREnd_05 As New TableCell

            TREnd_00.BorderWidth = "1"
            TREnd_01.BorderWidth = "1"
            TREnd_02.BorderWidth = "1"
            TREnd_03.BorderWidth = "1"
            TREnd_04.BorderWidth = "1"
            TREnd_05.BorderWidth = "1"

            TREnd_00.BorderColor = Drawing.Color.Silver
            TREnd_01.BorderColor = Drawing.Color.Silver
            TREnd_02.BorderColor = Drawing.Color.Silver
            TREnd_03.BorderColor = Drawing.Color.Silver
            TREnd_04.BorderColor = Drawing.Color.Silver
            TREnd_05.BorderColor = Drawing.Color.Silver

            TREnd_00.BorderStyle = BorderStyle.Solid
            TREnd_01.BorderStyle = BorderStyle.Solid
            TREnd_02.BorderStyle = BorderStyle.Solid
            TREnd_03.BorderStyle = BorderStyle.Solid
            TREnd_04.BorderStyle = BorderStyle.Solid
            TREnd_05.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TREnd, TREnd_00, 5, 5, "c", "")
            RH.AddColumn(TREnd, TREnd_01, 27, 27, "l", "Total")

            If Total > 0 Then
                RH.AddColumn(TREnd, TREnd_02, 17, 17, "c", "<a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("-1") & " &SubHD=Audit Pending Report  &MonthDesc=&UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + Total.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_02, 17, 17, "c", PendingTotal.ToString())
            End If
            If PendingTotal > 0 Then
                RH.AddColumn(TREnd, TREnd_03, 17, 17, "c", "<a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("0") & " &SubHD=Audit Pending Report  &MonthDesc=&UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + PendingTotal.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_03, 17, 17, "c", Total.ToString())
            End If
            If ProgressTotal > 0 Then
                RH.AddColumn(TREnd, TREnd_04, 17, 17, "c", "<a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("1") & " &SubHD=Audit In Progress Report &MonthDesc=&UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + ProgressTotal.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_04, 17, 17, "c", ProgressTotal.ToString())
            End If
            If CompletedTotal > 0 Then
                RH.AddColumn(TREnd, TREnd_05, 17, 17, "c", "<a href='AuditPendingDtlReport.aspx?StatusID= " & GF.Encrypt("2") & " &SubHD=Audit Completion Report &MonthDesc=&UserId=" + User_Id.ToString() + " &PostId=" + Post_Id.ToString() + " '>" + CompletedTotal.ToString())
            Else
                RH.AddColumn(TREnd, TREnd_05, 17, 17, "c", CompletedTotal.ToString())
            End If


            tb.Controls.Add(TREnd)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
