﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewObservationPendingQueue_2
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim LevelID As Integer
    Dim BranchID As Integer
    Dim Role As String
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            PostID = CInt(Session("Post_ID"))
            LevelID = CInt(Request.QueryString.Get("LevelID"))
            Role = Request.QueryString.Get("Role")
            BranchID = GN.Decrypt(Request.QueryString.Get("BranchID"))
            Me.hdnLevelID.Value = LevelID.ToString()
            Me.hdnRole.Value = Role
            Dim LevelName As String = ""
            LevelName = AD.GetLevelName(LevelID)
            Dim BranchName As String
            BranchName = GN.GetBranch_Name(BranchID)
            RH.Heading(Session("FirmName"), tb, Role.ToUpper() + " PENDING QUEUE " + LevelName + " Level", 100)
            RH.SubHeading(tb, 100, "l", "Branch : " + BranchName)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = DB.ExecuteDataSet("select b.item_id, e.ITEM_NAME ,count(*)as cnt ,d.Audit_Type,h.Type_name from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL d,AUDIT_CHECK_LIST e,AUDIT_OBSERVATION_STATUS_MASTER c,AUDIT_OBSERVATION_CYCLE f,AUDIT_UPDATE_MASTER g,Audit_Type h where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=d.AUDIT_ID and b.ITEM_ID =e.ITEM_ID  and a.role_id=c.Status_ID and d.Audit_type=h.TYPE_ID and a.STATUS_ID>0 and a.LAST_UPDATED_DT is not null and a.LEVEL_ID=" + LevelID.ToString() + " and c.Description='" + Role + "'  and d.BRANCH_ID=" + BranchID.ToString() + " and a.ORDER_ID=f.ORDER_ID and f.UPDATE_ID=g.UPDATE_ID group by b.item_id,e.ITEM_NAME,d.Audit_Type,h.Type_name order by d.Audit_Type,e.item_Name").Tables(0)
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 85, 85, "l", "Check List")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "Count")


            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            Dim AuditType As Integer = 0
            For Each DR In DT.Rows
                If AuditType <> CInt(DR(3)) Then
                    Dim TRSUBHD As New TableRow
                    Dim TRSUBHD_00 As New TableCell
                    TRSUBHD_00.BorderWidth = "1"
                    TRSUBHD_00.BorderColor = Drawing.Color.Silver
                    TRSUBHD_00.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TRSUBHD, TRSUBHD_00, 100, 100, "c", DR(4))
                    TRSUBHD.Style.Add("background-color", "silver")
                    tb.Controls.Add(TRSUBHD)
                End If
                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"


                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver


                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid


                I = I + 1
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR01, TR01_01, 85, 85, "l", DR(1))
                If AuditType <> 3 Then
                    Dim str As String = "<a href='viewObservationPendingQueue_3.aspx?LevelID=" + LevelID.ToString() + " &Role=" + Role + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &ItemID=" + DR(0).ToString() + "'>" + DR(2).ToString()
                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", str)
                Else
                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", DR(2))
                End If

                tb.Controls.Add(TR01)
                Total += CInt(DR(2))
                AuditType = CInt(DR(3))
            Next
            Dim TRFooter As New TableRow
            Dim TRFooter_00, TRFooter_01, TRFooter_02 As New TableCell
            TRFooter.Style.Add("Font-Weight", "Bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"


            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver


            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 85, 85, "r", "Total&nbsp;&nbsp;")
            RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", Total.ToString())
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
