﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ViewConsolidatedAuditReportObservationWise.aspx.vb" Inherits="ViewConsolidatedAuditReportObservationWise" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<body style="background-color:Black;">
 <script language="javascript" type="text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() {
         window.open("viewConsolidatedAuditReportBranchWise.aspx?StatusID=" + document.getElementById("<%= hdnStatusID.ClientID %>").value  + "&AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &RegionID=" + bota(document.getElementById("<%= hdnReportID.ClientID %>").value) + " &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &AreaID=" + btoa(document.getElementById("<%= hdnAreaID.ClientID %>").value), "_self");
         return false;
     }
     </script>
    <form id="form1" runat="server">    
    <div style="width:70%; background-color:white; min-height:750px; margin:0px auto;">
        <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">
           <asp:HiddenField ID="hdnReportID" runat="server" />
            <asp:HiddenField ID="hdnAuditID" runat="server" />
             <asp:HiddenField ID="hdnAreaID" runat="server" />
             <asp:HiddenField ID="hdnRegionID" runat="server" />
             <asp:HiddenField ID="hdnStatusID" runat="server" />
            <asp:ImageButton ID="cmd_Back" 
                style="text-align:left ;float:left; padding-top:5px;" runat="server" 
                Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/back.png" 
                ToolTip="Back" Visible="False"/>
            <asp:ImageButton ID="cmd_Print" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            &nbsp;&nbsp;<asp:ImageButton ID="cmd_Export" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Click to Export"/>
        </div>
        <br style="background-color:white"/>
        <div style="text-align:center;margin:0px auto; width:95%; background-color:white; font-family:Cambria">   
            <asp:Panel ID="pnDisplay" runat="server" Width="100%">
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
</asp:Content>