﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_AuditPendingDtlReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim StatusID As Integer
    Dim User_Id As Integer
    Dim Post_Id As Integer

    Dim MonthDesc As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            StatusID = CInt(GF.Decrypt(Request.QueryString.Get("StatusID")))
            MonthDesc = Request.QueryString.Get("MonthDesc")
            User_Id = Request.QueryString.Get("UserId")
            Post_Id = Request.QueryString.Get("PostId")
            Dim SubHead As String = ""
            SubHead = Request.QueryString.Get("SubHD")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            RH.Heading(Session("FirmName"), tb, SubHead, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "l", "Auditor")
            If StatusID = 0 Then
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Assigned On")
            ElseIf StatusID = 1 Then
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Started On")
            ElseIf StatusID = 2 Then
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Completed On")
            Else
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "Assigned On")
            End If
            RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Branch Audit")
            RH.AddColumn(TRHead, TRHead_05, 15, 15, "l", "Field Audit")
            RH.AddColumn(TRHead, TRHead_06, 15, 15, "l", "General Audit")
            tb.Controls.Add(TRHead)


            'RH.BlankRow(tb, 5)
            Dim Str As String
            If MonthDesc <> "" Then
                If StatusID < 0 Then
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),A.Assign_dt,106)  as  Assign_dt ,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code AND DATENAME(month,A.Period_to)+'/'+CAST(YEAR(A.Period_to)as varchar(4))='" + MonthDesc + "' and a.skip_flag=0 GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,a.Assign_dt"
                ElseIf StatusID = 0 Then
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),A.Assign_dt,106)  as  Assign_dt ,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code AND A.Status_ID in(0,9) AND DATENAME(month,A.Period_to)+'/'+CAST(YEAR(A.Period_to)as varchar(4))='" + MonthDesc + "' and a.skip_flag=0 GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,a.Assign_dt"
                ElseIf StatusID = 1 Then
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),A.Start_Dt,106)  as  Assign_dt,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code AND A.Status_ID in(1,3) AND DATENAME(month,A.Period_to)+'/'+CAST(YEAR(A.Period_to)as varchar(4))='" + MonthDesc + "' and a.skip_flag=0 GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,a.Start_Dt"
                Else
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),max(isnull(A.End_Dt,a.Start_Dt)),106) ,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code AND A.Status_ID=" + StatusID.ToString() + " AND DATENAME(month,A.Period_to)+'/'+CAST(YEAR(A.Period_to)as varchar(4))='" + MonthDesc + "' and a.skip_flag=0 GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,A.Assign_dt"
                End If
            Else
                If StatusID < 0 Then
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),A.Assign_dt,106)  as  Assign_dt ,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.skip_flag=0 GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,a.Assign_dt"
                ElseIf StatusID = 0 Then
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),A.Assign_dt,106)  as  Assign_dt,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.skip_flag=0 AND A.Status_ID in(0,9) GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,A.Assign_dt"
                ElseIf StatusID = 1 Then
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),A.Start_Dt,106)  as  Assign_dt,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.skip_flag=0 AND A.Status_ID in(1,3) GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,A.Start_Dt"
                Else
                    Str = "select C.Branch_Id, C.Branch_Name,D.Emp_Name,convert(varchar(11),max(isnull(A.End_Dt,a.Start_Dt)),106)  as  Assign_dt,MIN(CASE WHEN B.AUDIT_TYPE=1 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS BA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=2 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID in(1,3) THEN 'Progress' ELSE 'Complete' END)END) AS FA_STATUS ,MIN(CASE WHEN B.AUDIT_TYPE=3 THEN(CASE WHEN B.STATUS_ID in(0,9) THEN 'Pending' WHEN B.STATUS_ID=1 THEN 'Progress' ELSE 'Complete' END)END) AS GA_STATUS  from AUDIT_MASTER A,AUDIT_DTL B,BRANCH_MASTER C,EMP_MASTER D WHERE A.Group_ID=B.GROUP_ID AND A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code AND A.Status_ID=" + StatusID.ToString() + " and a.skip_flag=0 GROUP BY C.Branch_Id, C.Branch_Name,D.Emp_Name,A.Assign_dt"
                End If
            End If
            Str = "select B.* from (select A.branch_id, A.Branch_name, B.Emp_code as TM, C.Team_Lead as TL, C.Emp_code as Auditor from branch_master A " +
                  "  left join AUDIT_ADMIN_BRANCHES B on A.Branch_id = B.Branch_Id " +
                  "  left join AUDIT_BRANCHES c on A.Branch_id = c.Branch_Id  " +
                  "  where branch_type in ( 0 ,3) ) A left join  " +
                  "  (" + Str + " ) B on A.Branch_id = B.Branch_Id " +
                  "  where B.branch_id is not null "

            Dim StrWhere As String = ""
          
            If Post_Id = 1 Or Post_Id = 2 Then 'Audit Head/TM
                StrWhere = " And 1 = 1 "
                'ElseIf Post_Id = 2 Then 'TM
                '    StrWhere = " And TM =" + User_Id.ToString()
            ElseIf Post_Id = 3 Then 'TL
                StrWhere = " And TL =" + User_Id.ToString()
            ElseIf Post_Id = 4 Then 'Auditor
                StrWhere = " And Auditor =" + User_Id.ToString()
            End If
            Str = Str + StrWhere
            DT = DB.ExecuteDataSet(Str).Tables(0)
            For Each DR In DT.Rows
                LineNumber += 1
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 20, 20, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 20, 20, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 15, 15, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 15, 15, "l", DR(5).ToString())
                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
