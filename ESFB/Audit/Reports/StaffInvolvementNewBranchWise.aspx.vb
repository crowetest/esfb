﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class StaffInvolvementNewBranchWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim CloseFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim AreaID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            AreaID = GN.Decrypt(Request.QueryString.Get("AreaID"))
            CloseFlag = Request.QueryString.Get("CloseFlag")
            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim SubHD As String = ""
            Dim Area_Name As String = GN.GetArea_Name(AreaID)
            RH.Heading(Session("FirmName"), tb, "FI & SI Abstract  From " + FromDt.ToString("dd/MMM/yyyy") + " To " + ToDt.ToString("dd/MMM/yyyy") + " " + Area_Name + " Area " + Status, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_01, 45, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_02, 10, 1, "Financial Leakage")
            RH.InsertColumn(TRHead, TRHead_03, 10, 1, "Staff Involvement")
            RH.InsertColumn(TRHead, TRHead_04, 10, 1, "No Of Observations")
            RH.InsertColumn(TRHead, TRHead_05, 10, 1, "No of CheckLists")
            RH.InsertColumn(TRHead, TRHead_06, 10, 1, "No of Staffs")

            tb.Controls.Add(TRHead)

            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            'RH.BlankRow(tb, 3)

            Dim TotStaff As Double = 0
            Dim TotLeakage As Double = 0
            Dim TotNoOfStaff As Double = 0
            Dim TotNoOfCheckList As Double = 0
            Dim TotNoOfObs As Double = 0

            Dim SqlStr As String = "select c.Branch_ID, c.Branch_Name,sum(isnull( amount,0)) as FL,sum(case when isnull(intentionally,0)=1 then Amount else 0 end) as StaffInvolvement,COUNT(distinct a.emp_code)as NoOFStaff,COUNT(distinct e.ITEM_ID )as NoOFCheckList,COUNT(*) as NoOfObservations from AUDIT_STAFF_INVOLVEMENT a,audit_observation_dtl d,audit_observation e,audit_dtl f,audit_master g,EMP_MASTER  b,BrMaster c where a.EMP_CODE=b.Emp_Code and g.Branch_ID=c.Branch_ID and a.SINO=d.SINO and d.OBSERVATION_ID=e.OBSERVATION_ID and e.AUDIT_ID=f.AUDIT_ID and f.GROUP_ID=g.GROUP_ID and g.STATUS_ID=2 and DATEADD(day, DATEDIFF(day, 0, a.Tra_DT), 0) between '" + FromDt + "'  and  '" + ToDt + "' and c.area_ID=" + AreaID.ToString() + "  "
            If (Month > 0) Then
                SqlStr += " and month(g.period_to)=" + Month.ToString() + " and year(g.period_to)=" + year.ToString() + "  "
            End If

            If CloseFlag = 0 Then
                SqlStr += " and a.status_id in(0,9)"
            ElseIf CloseFlag = 1 Then
                SqlStr += " and a.status_id=1"
            End If

            Dim sqlTail As String = "    group by  c.Branch_ID, c.Branch_Name order by c.Branch_Name"
            SqlStr += sqlTail
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            ' AD.GetConsolidatedStaffInvolvement(Month, year, PostID, CInt(Session("UserID")), CloseFlag, FromDt, ToDt)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell



                RH.InsertColumn(TR3, TR3_00, 5, 2, j.ToString())
                'RH.InsertColumn(TR3, TR3_01, 45, 0, "<a href=ViewStaffInvolvementAbstract.aspx?ReportID=" + hdnReportID.Value + " &BranchID=" + GN.Encrypt(DR(0)) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='_blank'>" + DR(1) + "  </a>")
                RH.InsertColumn(TR3, TR3_01, 45, 0, "<a href='ViewStaffInvolvementAbstract.aspx?ReportID=" + hdnReportID.Value + " &BranchID=" + GN.Encrypt(DR(0)) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + "' target='_blank'>" + DR(1).ToString().ToUpper() + "  </a>")
                If DR(2) = 0 Then
                    RH.InsertColumn(TR3, TR3_02, 10, 1, "-")
                Else
                    RH.InsertColumn(TR3, TR3_02, 10, 1, DR(2).ToString())
                End If
                If DR(3) = 0 Then
                    RH.InsertColumn(TR3, TR3_03, 10, 1, "-")
                Else
                    RH.InsertColumn(TR3, TR3_03, 10, 1, DR(3).ToString())
                End If
                If DR(6) = 0 Then
                    RH.InsertColumn(TR3, TR3_04, 10, 1, "-")
                Else
                    RH.InsertColumn(TR3, TR3_04, 10, 1, CDbl(DR(6)).ToString())
                End If
                If DR(5) = 0 Then
                    RH.InsertColumn(TR3, TR3_05, 10, 1, "-")
                Else
                    RH.InsertColumn(TR3, TR3_05, 10, 1, CDbl(DR(5)).ToString())
                End If
                If DR(4) = 0 Then
                    RH.InsertColumn(TR3, TR3_06, 10, 1, "-")
                Else
                    RH.InsertColumn(TR3, TR3_06, 10, 1, DR(4).ToString())
                End If
                tb.Controls.Add(TR3)
                TotLeakage += DR(2)
                TotStaff += DR(3)
                TotNoOfStaff += DR(4)
                TotNoOfCheckList += DR(5)
                TotNoOfObs += DR(6)

                tb.Controls.Add(TR3)
                TotLeakage += DR(2)
                TotStaff += DR(3)
            Next
            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")

            RH.InsertColumn(TRFooter, TRFooter_00, 5, 2, "")
            RH.InsertColumn(TRFooter, TRFooter_01, 45, 0, "<a href='viewStaffInvolvementAbstract.aspx?ReportID=" + hdnReportID.Value + " &BranchID=" + GN.Encrypt("0") + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target=_blank>TOTAL</a>")
            If TotLeakage > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_02, 10, 1, TotLeakage.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_02, 10, 1, "")
            End If
            If TotStaff > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_03, 10, 1, TotStaff.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_03, 10, 1, "")
            End If
            If TotNoOfObs > 0 Then
                'RH.InsertColumn(TRFooter, TRFooter_04, 10, 1, "<a href='viewStaffInvolvementEmpWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &ItemID=0 &BranchID=" + GN.Encrypt("0") + " &RegionID=" + GN.Encrypt("0") + " &AreaID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " ' target='_blank'>" + TotNoOfStaff.ToString() + "</a>")
                RH.InsertColumn(TRFooter, TRFooter_04, 10, 1, TotNoOfObs.ToString()) 'TotNoOfStaff.ToString())
            Else
                RH.InsertColumn(TRFooter, TRFooter_04, 10, 1, "")
            End If
            If TotNoOfCheckList > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_05, 10, 1, "") ' TotNoOfCheckList.ToString())
            Else
                RH.InsertColumn(TRFooter, TRFooter_05, 10, 1, "")
            End If
            If TotNoOfStaff > 0 Then
                RH.InsertColumn(TRFooter, TRFooter_06, 10, 1, "")
            Else
                RH.InsertColumn(TRFooter, TRFooter_06, 10, 1, "")
            End If
            tb.Controls.Add(TRFooter)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
