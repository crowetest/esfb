﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewAuditAbstractReport_Base
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim AuditID As String
    Dim CloseFlag As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim SubHD As String
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim BranchFlag As Integer
    Dim TypeID As Integer
    Dim ItemID As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            AuditID = Request.QueryString.Get("AuditID")

            CloseFlag = Request.QueryString.Get("CloseFlag")
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            BranchFlag = Request.QueryString.Get("BranchFlag")
            TypeID = Request.QueryString.Get("TypeID")
            ItemID = Request.QueryString.Get("ItemID")
            Dim AuditDtl() As String
            AuditDtl = AuditID.Split("~")
            Dim MonthID As Integer
            Dim YearID As Integer
            If AuditDtl(0) = -1 Then
                If (BranchID > 0) Then
                    SubHD = " AUDIT ABSTRACT REPORT " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                ElseIf AreaID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                ElseIf RegionID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                Else
                    SubHD = " AUDIT ABSTRACT REPORT "
                End If
            Else
                MonthID = CInt(AuditDtl(0))
                YearID = CInt(AuditDtl(1))
                If (BranchID > 0) Then
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
                ElseIf AreaID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
                ElseIf RegionID > 0 Then
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
                Else
                    SubHD = " AUDIT ABSTRACT REPORT " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString()
                End If
            End If
            If TypeID = 1 Then
                SubHD = SubHD + "TYPE-VERY SERIOUS"
            ElseIf TypeID = 2 Then
                SubHD = SubHD + "TYPE-SERIOUS"
            ElseIf TypeID = 3 Then
                SubHD = SubHD + "TYPE-GENERAL"
            ElseIf TypeID = 4 Then
                SubHD = SubHD + "TYPE-AMOUNT INVOLVED"
            Else
                SubHD = SubHD + "TYPE-TOTAL"
            End If
            RH.Heading(Session("FirmName"), tb, SubHD, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim DT As New DataTable
            DT = AD.GetAbstractReport(MonthID, YearID, RegionID, AreaID, BranchID, 4, CloseFlag, FromDt, ToDt, TypeID, ItemID, CInt(Session("UserID")))
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver



            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 85, 85, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "Count")


            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            Dim AuditType As Integer = 0
            Dim GrandTotal As Integer = 0
            For Each DR In DT.Rows
                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"
                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver


                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid


                I = I + 1
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR01, TR01_01, 85, 85, "l", DR(1))

                If (CInt(DR(2)) > 0) Then

                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", DR(2).ToString)

                Else
                    RH.AddColumn(TR01, TR01_02, 10, 10, "c", "-")
                End If

                tb.Controls.Add(TR01)

                Total += CInt(DR(2))
                GrandTotal += CInt(DR(2))


            Next
            Dim TRTot As New TableRow
            Dim TRTot_00, TRTot_01, TRTot_02 As New TableCell
            TRTot.Style.Add("Font-Weight", "Bold")

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"


            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver


            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRTot, TRTot_00, 5, 5, "c", "")
            RH.AddColumn(TRTot, TRTot_01, 85, 85, "r", "Total&nbsp;&nbsp;")

            RH.AddColumn(TRTot, TRTot_02, 10, 10, "c", Total)

            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)
            Dim TRFooter As New TableRow
            Dim TRFooter_00, TRFooter_01, TRFooter_02 As New TableCell
            TRFooter.Style.Add("Font-Weight", "Bold")

            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"


            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver


            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 85, 85, "r", "Grand Total&nbsp;&nbsp;")
            If GrandTotal > 0 Then
                RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", GrandTotal)
            Else
                RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", "-")
            End If

            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
