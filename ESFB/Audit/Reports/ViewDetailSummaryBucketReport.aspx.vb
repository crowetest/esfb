﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewDetailSummaryBucketReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Close.Attributes.Add("onclick", "return Exitform()")
            Dim Type As Integer = CInt(Request.QueryString.Get("Type"))
            Dim DayType As Integer = CInt(Request.QueryString.Get("DayType"))
            Dim Days As Integer = CInt(Request.QueryString.Get("Days"))
            Dim OptGreat As Integer = CInt(Request.QueryString.Get("OptGreat"))
            Dim OBSERVATIONID As Integer = CInt(Request.QueryString.Get("OBSERVATIONID"))
            Dim DetailSearch As Integer = CInt(Request.QueryString.Get("DetailSearch"))
            Dim head As String = CStr(Request.QueryString.Get("head"))
            Dim GROUPID As Integer = CInt(Request.QueryString.Get("GROUPID"))

            Dim BRANCHID As Integer = CInt(GF.Decrypt(Request.QueryString.Get("branch_id")))
            Dim AuditID As String = CStr(GF.Decrypt(Request.QueryString.Get("AuditID")))

            Dim DTHead As New DataTable
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable

            RH.Heading(Session("FirmName"), tb, "DETAILED AGING REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            DTAudit = DB.ExecuteDataSet("SELECT UPPER(branch_name) FROM BRANCH_MASTER where branch_id=" + BRANCHID.ToString()).Tables(0)



            Dim TRItemHead2 As New TableRow
            TRItemHead2.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead2_00 As New TableCell

            RH.AddColumn(TRItemHead2, TRItemHead2_00, 100, 100, "l", "<b><i>BRANCH  :-     " & DTAudit.Rows(0)(0).ToString() & " </i></b>")

            tb.Controls.Add(TRItemHead2)

            Dim TRItemHead3 As New TableRow
            TRItemHead3.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead3_00 As New TableCell

            RH.AddColumn(TRItemHead3, TRItemHead3_00, 100, 100, "l", "<b><i>Aging Bucket  :-     " & head.ToString() & " </i></b>")

            tb.Controls.Add(TRItemHead3)
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)


            tb.Controls.Add(TRItemHead)
            RH.BlankRow(tb, 5)

            Dim TypeName As String
            DT = AD.AGINGREPORT(Type, "DD", DayType, OptGreat, Days, OBSERVATIONID, DetailSearch, 0, BRANCHID, AuditID, CInt(Session("UserID")), CInt(Session("Post_ID")), GROUPID)
            For Each DR In DT.Rows


                j += 1
                If TypeName <> DR(15) Then

                    Dim TRItemHead4 As New TableRow
                    TRItemHead4.ForeColor = Drawing.Color.DarkBlue
                    Dim TRItemHead4_00 As New TableCell

                    RH.AddColumn(TRItemHead4, TRItemHead4_00, 100, 100, "l", "<b><i>Check List  :-     " & DR(6).ToString() & " </i></b>")

                    tb.Controls.Add(TRItemHead4)


                    Dim TRItemHead1 As New TableRow
                    TRItemHead1.ForeColor = Drawing.Color.DarkBlue
                    TRItemHead1.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRItemHead1_00 As New TableCell
                    RH.BlankRow(tb, 5)
                    RH.AddColumn(TRItemHead1, TRItemHead1_00, 100, 100, "c", "<b><i>" & DR(15) & " </i></b>")

                    tb.Controls.Add(TRItemHead1)
                    RH.BlankRow(tb, 5)


                    Dim TRHead As New TableRow
                    TRHead.BackColor = Drawing.Color.WhiteSmoke
                    Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07 As New TableCell

                    TRHead_00.BorderWidth = "1"
                    TRHead_01.BorderWidth = "1"
                    TRHead_02.BorderWidth = "1"
                    TRHead_03.BorderWidth = "1"
                    TRHead_04.BorderWidth = "1"
                    TRHead_05.BorderWidth = "1"
                    TRHead_06.BorderWidth = "1"
                    TRHead_07.BorderWidth = "1"



                    TRHead_00.BorderColor = Drawing.Color.Silver
                    TRHead_01.BorderColor = Drawing.Color.Silver
                    TRHead_02.BorderColor = Drawing.Color.Silver
                    TRHead_03.BorderColor = Drawing.Color.Silver
                    TRHead_04.BorderColor = Drawing.Color.Silver
                    TRHead_05.BorderColor = Drawing.Color.Silver
                    TRHead_06.BorderColor = Drawing.Color.Silver
                    TRHead_07.BorderColor = Drawing.Color.Silver


                    TRHead_00.BorderStyle = BorderStyle.Solid
                    TRHead_01.BorderStyle = BorderStyle.Solid
                    TRHead_02.BorderStyle = BorderStyle.Solid
                    TRHead_03.BorderStyle = BorderStyle.Solid
                    TRHead_04.BorderStyle = BorderStyle.Solid
                    TRHead_05.BorderStyle = BorderStyle.Solid
                    TRHead_06.BorderStyle = BorderStyle.Solid
                    TRHead_07.BorderStyle = BorderStyle.Solid
                    RH.AddColumn(TRHead, TRHead_00, 6, 6, "l", "Sl NO")
                    RH.AddColumn(TRHead, TRHead_01, 10, 10, "l", "REF. NO")
                    RH.AddColumn(TRHead, TRHead_02, 21, 21, "l", "CUSTOMER")
                    RH.AddColumn(TRHead, TRHead_03, 12, 12, "l", "SANCTION DT")
                    RH.AddColumn(TRHead, TRHead_04, 12, 12, "l", "EFF. DT")
                    RH.AddColumn(TRHead, TRHead_05, 9, 9, "l", "AMOUNT")
                    RH.AddColumn(TRHead, TRHead_06, 30, 30, "l", "REMARKS")

                    tb.Controls.Add(TRHead)
                End If

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 6, 6, "c", j.ToString)
                RH.AddColumn(TR3, TR3_01, 10, 10, "l", "<a href='viewLoanDetails.aspx?LoanNo=" + GF.Encrypt(DR(7)) + "' style='text-align:right;' target='_blank'>" + DR(7) + "</a>")
                RH.AddColumn(TR3, TR3_02, 21, 21, "l", DR(10))
                RH.AddColumn(TR3, TR3_03, 12, 12, "l", DR(13))
                RH.AddColumn(TR3, TR3_04, 12, 12, "l", DR(11))
                RH.AddColumn(TR3, TR3_05, 9, 9, "l", DR(12))
                RH.AddColumn(TR3, TR3_06, 30, 30, "l", DR(9) + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(14)) + "' style='text-align:right;' target='_blank' >Previous</a>")
                tb.Controls.Add(TR3)
                TypeName = DR(15)
            Next
            DT = AD.AGINGREPORT(Type, "D4", DayType, OptGreat, Days, OBSERVATIONID, DetailSearch, 0, BRANCHID, AuditID, CInt(Session("UserID")), CInt(Session("Post_ID")), GROUPID)
            If DT Is Nothing Then
            Else
                For Each DR In DT.Rows
                    j += 1
                    If TypeName <> DR(10) Then
                        Dim TRItemHead1 As New TableRow
                        TRItemHead1.ForeColor = Drawing.Color.DarkBlue
                        TRItemHead1.BackColor = Drawing.Color.WhiteSmoke
                        Dim TRItemHead1_00 As New TableCell
                        RH.BlankRow(tb, 5)
                        RH.AddColumn(TRItemHead1, TRItemHead1_00, 100, 100, "c", "<b><i>" & DR(10) & " </i></b>")

                        tb.Controls.Add(TRItemHead1)


                        Dim TRItemHead4 As New TableRow
                        TRItemHead4.ForeColor = Drawing.Color.DarkBlue
                        Dim TRItemHead4_00 As New TableCell


                        tb.Controls.Add(TRItemHead4)
                        RH.BlankRow(tb, 5)
                        Dim TRHead As New TableRow
                        TRHead.BackColor = Drawing.Color.WhiteSmoke
                        Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell

                        TRHead_00.BorderWidth = "1"
                        TRHead_01.BorderWidth = "1"
                        TRHead_02.BorderWidth = "1"




                        TRHead_00.BorderColor = Drawing.Color.Silver
                        TRHead_01.BorderColor = Drawing.Color.Silver
                        TRHead_02.BorderColor = Drawing.Color.Silver


                        TRHead_00.BorderStyle = BorderStyle.Solid
                        TRHead_01.BorderStyle = BorderStyle.Solid
                        TRHead_02.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No")
                        RH.AddColumn(TRHead, TRHead_01, 45, 45, "l", "CHECK LIST")
                        RH.AddColumn(TRHead, TRHead_02, 50, 50, "l", "REMARKS")



                        tb.Controls.Add(TRHead)
                    End If

                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid

                    RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString)
                    RH.AddColumn(TR3, TR3_01, 45, 45, "l", DR(6))
                    RH.AddColumn(TR3, TR3_02, 50, 50, "l", DR(8) + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GF.Encrypt(DR(9)) + "' style='text-align:right;' target='_blank' >Previous</a>")
                    tb.Controls.Add(TR3)
                    TypeName = DR(10)
                Next
            End If
            RH.BlankRow(tb, 5)
            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub


    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
