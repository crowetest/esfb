﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewConsolidatedReportWiseAreaWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim CloseFlag As Integer
    Dim PostID As Integer
    Dim RegionID As Integer
    Dim AD As New Audit
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim RType As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))

            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))
            CloseFlag = CInt(Request.QueryString.Get("CloseFlag"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            RType = CInt(Request.QueryString.Get("RType"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If
            Dim HDStr As String = " "
            If RegionID > 0 Then
                HDStr += GN.GetRegion_Name(RegionID) + " Region "
            End If
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim SubHD As String = ""
            RH.Heading(Session("FirmName"), tb, "Consolidated Audit Report " + HDStr, 100)
            If (CloseFlag = 1) Then
                SubHD = "Status - Pending Operations (As on -" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 5 Then
                SubHD = "Status - Pending Auditors (As on -" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 2 Then
                SubHD = "Status - Pending (As on -" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 0 Then
                SubHD = "Status - Closed (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            Else
                SubHD = "Status -All (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            End If
            RH.SubHeading(tb, 100, "l", SubHD)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")



            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "l", "Area")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "c", "V.Serious Reports")
            RH.AddColumn(TRHead, TRHead_03, 10, 10, "c", "Serious Reports")
            RH.AddColumn(TRHead, TRHead_04, 10, 10, "c", "Both S&F")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "General Reports")
            RH.AddColumn(TRHead, TRHead_06, 10, 10, "r", "Total Reports")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            Dim TotFraud As Integer = 0
            Dim TotSusLeakage As Integer = 0
            Dim TotGeneral As Integer = 0
            Dim TotTotal As Double = 0
            Dim TotalBoth As Integer = 0
            DT = AD.AreaWiseAuditReportReportWise(Month, year, CloseFlag, PostID, CInt(Session("UserID")), RegionID, FromDt, ToDt, RType)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", "<a href='ViewConsolidatedReportWiseBranchWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &AreaID=" + GN.Encrypt(DR(0).ToString()) + " &RegionID=" + GN.Encrypt("0") + " &BranchID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "' target=blank>" + DR(1) + "</a>")
                If DR(3) = 0 Then
                    RH.AddColumn(TR3, TR3_02, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_02, 10, 10, "c", "<a href='ViewConsolidatedReportWiseAuditWise.aspx?StatusID=1 &ReportID=" + hdnReportID.Value + "&AreaID=" + GN.Encrypt(DR(0).ToString()) + " &CloseFlag=" + CloseFlag.ToString() + " &RegionID=" + GN.Encrypt("0") + " &BranchID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "'>" + DR(3).ToString() + "</a>")
                End If
                If DR(4) = 0 Then
                    RH.AddColumn(TR3, TR3_03, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_03, 10, 10, "c", "<a href='ViewConsolidatedReportWiseAuditWise.aspx?StatusID=2 &ReportID=" + hdnReportID.Value + "&AreaID=" + GN.Encrypt(DR(0).ToString()) + " &CloseFlag=" + CloseFlag.ToString() + " &RegionID=" + GN.Encrypt("0") + " &BranchID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "'>" + DR(4).ToString() + "</a>")
                End If
                If DR(5) = 0 Then
                    RH.AddColumn(TR3, TR3_04, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_04, 10, 10, "c", "<a href='ViewConsolidatedReportWiseAuditWise.aspx?StatusID=3 &ReportID=" + hdnReportID.Value + "&AreaID=" + GN.Encrypt(DR(0).ToString()) + " &CloseFlag=" + CloseFlag.ToString() + " &RegionID=" + GN.Encrypt("0") + " &BranchID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "'>" + DR(5).ToString() + "</a>")
                End If
                If DR(6) = 0 Then
                    RH.AddColumn(TR3, TR3_05, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_05, 10, 10, "c", "<a href='ViewConsolidatedReportWiseAuditWise.aspx?StatusID=4 &ReportID=" + hdnReportID.Value + "&AreaID" + GN.Encrypt(DR(0).ToString()) + " &CloseFlag=" + CloseFlag.ToString() + " &RegionID=" + GN.Encrypt("0") + " &BranchID=" + GN.Encrypt("0") + " &AuditID=" + GN.Encrypt(AuditDtl) + " &FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "'>" + DR(6).ToString() + "</a>")
                End If
                If DR(2) = 0 Then
                    RH.AddColumn(TR3, TR3_06, 10, 10, "c", "-")
                Else
                    RH.AddColumn(TR3, TR3_06, 10, 10, "c", CDbl(DR(2)).ToString())
                End If

                tb.Controls.Add(TR3)
                TotFraud += DR(3)
                TotSusLeakage += DR(4)
                TotalBoth += DR(5)
                TotGeneral += DR(6)
                TotTotal += DR(2)
            Next


            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"
            TRFooter_05.BorderWidth = "1"
            TRFooter_06.BorderWidth = "1"

            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver
            TRFooter_05.BorderColor = Drawing.Color.Silver
            TRFooter_06.BorderColor = Drawing.Color.Silver

            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid
            TRFooter_05.BorderStyle = BorderStyle.Solid
            TRFooter_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 35, 35, "l", "Total")
            If TotFraud > 0 Then
                RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", "<a href='ViewConsolidatedReportWiseBranchWise.aspx?StatusID=1 &ReportID=" + hdnReportID.Value + " &AreaID=" + GN.Encrypt("-1") + "&BranchID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + "&AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "' target=blank>" + TotFraud.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_02, 10, 10, "c", "-")
            End If
            If TotSusLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "c", "<a href='ViewConsolidatedReportWiseBranchWise.aspx?StatusID=2 &ReportID=" + hdnReportID.Value + " &AreaID=" + GN.Encrypt("-1") + "&BranchID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "' target=blank>" + TotSusLeakage.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_03, 10, 10, "c", "-")
            End If
            If TotalBoth > 0 Then
                RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "c", "<a href='ViewConsolidatedReportWiseBranchWise.aspx?StatusID=3 &ReportID=" + hdnReportID.Value + " &AreaID=" + GN.Encrypt("-1") + "&BranchID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "' target=blank>" + TotalBoth.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_04, 10, 10, "c", "-")
            End If
            If TotGeneral > 0 Then
                RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "c", "<a href='ViewConsolidatedReportWiseBranchWise.aspx?StatusID=4 &ReportID=" + hdnReportID.Value + " &AreaID=" + GN.Encrypt("-1") + "&BranchID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "' target=blank>" + TotGeneral.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "c", "-")
            End If
            If TotTotal > 0 Then
                RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "c", "<a href='ViewConsolidatedReportWiseBranchWise.aspx?StatusID=0 &ReportID=" + hdnReportID.Value + " &AreaID=" + GN.Encrypt("-1") + "&BranchID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AuditID=" + GN.Encrypt(AuditDtl) + " &CloseFlag=" + CloseFlag.ToString() + "&FromDt=" + FromDt.ToString("dd/MMM/yyyy") + "  &ToDt=" + ToDt.ToString("dd/MMM/yyyy") + " &RTypeID=" + RType.ToString() + "' target=blank>" + TotTotal.ToString() + "</a>")
            Else
                RH.AddColumn(TRFooter, TRFooter_06, 10, 10, "c", "-")
            End If

            tb.Controls.Add(TRFooter)


            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
