﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ConsolidatedStaffInvolvement.aspx.vb" Inherits="Audit_Reports_ConsolidatedStaffInvolvement" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
    </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:22%;">
                Period</td>
            <td  style="width:75%;">
                <asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ALL</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr id="rowStatus" style="display:none">
            <td style="width:22%;">
                Status</td>
            <td  style="width:75%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                    <asp:ListItem Value="-1">ALL</asp:ListItem>
                    <asp:ListItem Value="1">PENDING</asp:ListItem>           
                    <asp:ListItem Value="0">CLOSED</asp:ListItem>
                    <asp:ListItem Value="9">REMOVE</asp:ListItem>
                </asp:DropDownList></td>
        </tr>

        <tr>
            <td style="width:22%; height: 18px;">
                From Date</td>
            <td  style="width:29%; height: 18px;">
                <asp:TextBox ID="txtFrom" runat="server" Height="18px" Width="254px"></asp:TextBox>
                <asp:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtFrom" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                
                </td>
        </tr>

        <tr>
            <td style="width:22%; height: 18px;">
                To Date</td>
            <td  style="width:29%; height: 18px;">
                <asp:TextBox ID="txtTo" runat="server" Height="16px" Width="255px"></asp:TextBox>
                <asp:CalendarExtender ID="txtTo_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtTo" Format="dd MMM yyyy">
                </asp:CalendarExtender>
                
                </td>
        </tr>
        <tr>
            <td style="height: 18px; text-align:center;" colspan="2">
                <input id="btnView" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" /></td>
       
        </tr>
        <tr>
            <td style="width:22%;">
                &nbsp;</td>
            <td  style="width:75%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:22%;">
                &nbsp;</td>
            <td  style="width:75%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:22%;">
                &nbsp;</td>
            <td  style="width:75%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:22%;">
                &nbsp;</td>
            <td  style="width:75%;">
                <asp:HiddenField ID="hdnPostID" runat="server" />
                 <asp:HiddenField ID="hdnLocationID" runat="server" />
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnExit_onclick() {
            window.open("../../Home.aspx", "_self");
        }

        function btnView_onclick() {
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            var StatusID = 1;
            var FromDt = document.getElementById("<%= txtFrom.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtTo.ClientID %>").value;
            if (document.getElementById("<%= hdnReportID.ClientID %>").value==1) {               
                StatusID = document.getElementById("<%= cmbStatus.ClientID %>").value;
            }
            if (document.getElementById("<%= hdnPostID.ClientID %>").value == "7") {
                window.open("viewStaffInvolvementAreaWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID +" &FromDt="+ FromDt + " &ToDt="+ ToDt, "_blank");
            }
            else if (document.getElementById("<%= hdnPostID.ClientID %>").value == "6") {
                window.open("viewStaffInvolvementBranchWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=btoa(0) &AreaID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt, "_blank");
            }
            else if (document.getElementById("<%= hdnPostID.ClientID %>").value == "5") {
                window.open("viewStaffInvolvementObservationsWise.aspx?StatusID=1 &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &RegionID=btoa(0) &AreaID=btoa(0) &BranchID=" + btoa(document.getElementById("<%= hdnLocationID.ClientID %>").value) + " &AuditID=" + btoa(AuditID) + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt, "_blank");
            }
            else {
                window.open("viewStaffInvolvementRegionWise.aspx?AuditID=" + btoa(AuditID) + " &ReportID=" + document.getElementById("<%= hdnReportID.ClientID %>").value + " &CloseFlag=" + StatusID + " &FromDt=" + FromDt + " &ToDt=" + ToDt, "_blank");
            }
        }

        function Initialize() {
            if (document.getElementById("<%= hdnReportID.ClientID %>").value == 1) {
                document.getElementById("rowStatus").style.display = "";
            }
            else {
                document.getElementById("<%= cmbStatus.ClientID %>").value = 1;
                document.getElementById("rowStatus").style.display = "none";
            }
        }

// ]]>
    </script>
</asp:Content>

