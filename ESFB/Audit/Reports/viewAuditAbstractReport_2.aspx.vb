﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewAuditAbstractReport_2
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim AuditID As String
    Dim StatusID As Integer
    Dim ItemID As Integer
    Dim CloseFlag As Integer
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Dim SubHD As String
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            AuditID = Request.QueryString.Get("AuditID")

            StatusID = Request.QueryString.Get("StatusID")
            CloseFlag = Request.QueryString.Get("CloseFlag")
            ItemID = Request.QueryString.Get("ItemID")
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim StatDescr As String
            If StatusID = 1 Then
                StatDescr = "TYPE-VERY SERIOUS"
            ElseIf StatusID = 2 Then
                StatDescr = "TYPE-SERIOUS"
            ElseIf StatusID = 3 Then
                StatDescr = "TYPE-GENERAL"
            ElseIf StatusID = 4 Then
                StatDescr = "TYPE-AMOUNT INVOLVED"
            Else
                StatDescr = ""
            End If
            Dim AuditDtl() As String
            AuditDtl = AuditID.Split("~")
            Dim MonthID As Integer = 0
            Dim YearID As Integer = 0
            If AuditDtl(0) = -1 Then
                If (BranchID > 0) Then
                    SubHD = " AUDIT OBSERVATIONS " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH " + StatDescr
                ElseIf AreaID > 0 Then
                    SubHD = " AUDIT OBSERVATIONS " + GN.GetArea_Name(AreaID).ToUpper() + " AREA " + StatDescr
                ElseIf RegionID > 0 Then
                    SubHD = " AUDIT OBSERVATIONS " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION " + StatDescr
                Else
                    SubHD = " AUDIT OBSERVATIONS " + StatDescr
                End If
            Else
                MonthID = CInt(AuditDtl(0))
                YearID = CInt(AuditDtl(1))
                If (BranchID > 0) Then
                    SubHD = " AUDIT OBSERVATIONS " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH " + StatDescr
                ElseIf AreaID > 0 Then
                    SubHD = " AUDIT OBSERVATIONS " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetArea_Name(AreaID).ToUpper() + " AREA " + StatDescr
                ElseIf RegionID > 0 Then
                    SubHD = " AUDIT OBSERVATIONS " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION " + StatDescr
                Else
                    SubHD = " AUDIT OBSERVATIONS " + MonthName(MonthID).ToUpper() + " - " + YearID.ToString() + " " + StatDescr
                End If
            End If


            Dim ItemName As String = ""
            DT = AD.GetItemName(ItemID, 0)
            If DT.Rows.Count > 0 Then
                ItemName = DT.Rows(0)(0)
            End If
            RH.Heading(Session("FirmName"), tb, SubHD, 100)
            RH.SubHeading(tb, 100, "l", "Check List : " + ItemName)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow

            DT = AD.GetAbstractReport(MonthID, YearID, RegionID, AreaID, BranchID, 3, CloseFlag, FromDt, ToDt, StatusID, ItemID, CInt(Session("UserID")))
            Dim TRHead As New TableRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 20, 20, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 5, 5, "c", "Type")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "c", "Est.Amt. Involved")
            RH.AddColumn(TRHead, TRHead_04, 55, 55, "c", "Remarks")

            tb.Controls.Add(TRHead)
            Dim I As Integer = 0
            Dim Total As Integer = 0
            Dim TotLeakage As Double = 0
            Dim BrCode As Integer = 0
            Dim GrandTotal As Integer = 0
            Dim GrandLeakage As Double = 0
            Dim j As Integer = 0
            For Each DR In DT.Rows
                Dim TR01 As New TableRow
                Dim TR01_00, TR01_01, TR01_02, TR01_03, TR01_04 As New TableCell

                TR01_00.BorderWidth = "1"
                TR01_01.BorderWidth = "1"
                TR01_02.BorderWidth = "1"
                TR01_03.BorderWidth = "1"
                TR01_04.BorderWidth = "1"


                TR01_00.BorderColor = Drawing.Color.Silver
                TR01_01.BorderColor = Drawing.Color.Silver
                TR01_02.BorderColor = Drawing.Color.Silver
                TR01_03.BorderColor = Drawing.Color.Silver
                TR01_04.BorderColor = Drawing.Color.Silver


                TR01_00.BorderStyle = BorderStyle.Solid
                TR01_01.BorderStyle = BorderStyle.Solid
                TR01_02.BorderStyle = BorderStyle.Solid
                TR01_03.BorderStyle = BorderStyle.Solid
                TR01_04.BorderStyle = BorderStyle.Solid


                I = I + 1
                j = j + 1
                Dim Type As String = ""
                If (CInt(DR(2)) = 1) Then
                    Type = "VS"
                ElseIf CInt(DR(3)) = 1 Then
                    Type = "S"
                End If
                RH.AddColumn(TR01, TR01_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR01, TR01_01, 20, 20, "l", DR(1))
                RH.AddColumn(TR01, TR01_02, 5, 5, "l", Type)
                If CDbl(DR(4)) > 0 Then
                    RH.AddColumn(TR01, TR01_03, 15, 15, "r", CDbl(DR(4)).ToString("##,##,##,###.00"))
                Else
                    RH.AddColumn(TR01, TR01_03, 15, 15, "r", "-")
                End If
                RH.AddColumn(TR01, TR01_04, 55, 55, "l", DR(6) + "&nbsp;" + "<a href='viewPrevRemarksReport.aspx?AuditID=" + GN.Encrypt(DR(5).ToString()) + "' style='text-align:right;' target='_blank' >Previous</a>")

                tb.Controls.Add(TR01)


                TotLeakage += CDbl(DR(4))
                GrandLeakage += CDbl(DR(4))
                BrCode = CInt(DR(0))
            Next
            Dim TRTot As New TableRow
            Dim TRTot_00, TRTot_01, TRTot_02, TRTot_03, TRTot_04 As New TableCell
            TRTot.Style.Add("Font-Weight", "Bold")

            TRTot_00.BorderWidth = "1"
            TRTot_01.BorderWidth = "1"
            TRTot_02.BorderWidth = "1"
            TRTot_03.BorderWidth = "1"
            TRTot_04.BorderWidth = "1"


            TRTot_00.BorderColor = Drawing.Color.Silver
            TRTot_01.BorderColor = Drawing.Color.Silver
            TRTot_02.BorderColor = Drawing.Color.Silver
            TRTot_03.BorderColor = Drawing.Color.Silver
            TRTot_04.BorderColor = Drawing.Color.Silver


            TRTot_00.BorderStyle = BorderStyle.Solid
            TRTot_01.BorderStyle = BorderStyle.Solid
            TRTot_02.BorderStyle = BorderStyle.Solid
            TRTot_03.BorderStyle = BorderStyle.Solid
            TRTot_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRTot, TRTot_00, 5, 5, "c", "")
            RH.AddColumn(TRTot, TRTot_01, 20, 20, "r", "Total&nbsp;&nbsp;")
            RH.AddColumn(TRTot, TRTot_02, 5, 5, "c", "")

            If TotLeakage > 0 Then
                RH.AddColumn(TRTot, TRTot_03, 15, 15, "r", TotLeakage.ToString("###,##,##,###.00"))
            Else
                RH.AddColumn(TRTot, TRTot_03, 15, 15, "r", "-")
            End If
            RH.AddColumn(TRTot, TRTot_04, 55, 55, "c", "")
            tb.Controls.Add(TRTot)
            RH.BlankRow(tb, 5)
            Dim TRFooter As New TableRow
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04 As New TableCell
            TRFooter.Style.Add("Font-Weight", "Bold")

            TRFooter_00.BorderWidth = "1"
            TRFooter_01.BorderWidth = "1"
            TRFooter_02.BorderWidth = "1"
            TRFooter_03.BorderWidth = "1"
            TRFooter_04.BorderWidth = "1"


            TRFooter_00.BorderColor = Drawing.Color.Silver
            TRFooter_01.BorderColor = Drawing.Color.Silver
            TRFooter_02.BorderColor = Drawing.Color.Silver
            TRFooter_03.BorderColor = Drawing.Color.Silver
            TRFooter_04.BorderColor = Drawing.Color.Silver


            TRFooter_00.BorderStyle = BorderStyle.Solid
            TRFooter_01.BorderStyle = BorderStyle.Solid
            TRFooter_02.BorderStyle = BorderStyle.Solid
            TRFooter_03.BorderStyle = BorderStyle.Solid
            TRFooter_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRFooter, TRFooter_00, 5, 5, "c", "")
            RH.AddColumn(TRFooter, TRFooter_01, 20, 20, "r", "Grand Total&nbsp;&nbsp;")
            RH.AddColumn(TRFooter, TRFooter_02, 5, 5, "c", "")

            If GrandLeakage > 0 Then
                RH.AddColumn(TRFooter, TRFooter_03, 15, 15, "r", GrandLeakage.ToString("###,##,##,###.00"))
            Else
                RH.AddColumn(TRFooter, TRFooter_03, 15, 15, "r", "-")
            End If

            RH.AddColumn(TRFooter, TRFooter_04, 55, 55, "c", "")
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub

End Class
