﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class QueryResponseTracker_Observation
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim GroupID As Integer = -1
    Dim StatusID As Integer = -1
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable

            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            If Not IsNothing(Request.QueryString.Get("GroupID")) Then
                GroupID = CInt(Request.QueryString.Get("GroupID"))
            End If
            If Not IsNothing(Request.QueryString.Get("StatusID")) Then
                StatusID = CInt(Request.QueryString.Get("StatusID"))
            End If
            Dim SubHead As String = ""
            If BranchID > 0 Then
                SubHead = " - " + GN.GetBranch_Name(BranchID).ToUpper() + " BRANCH"
            ElseIf AreaID > 0 Then
                SubHead = " - " + GN.GetArea_Name(AreaID).ToUpper() + " AREA"
            ElseIf RegionID > 0 Then
                SubHead = " - " + GN.GetRegion_Name(RegionID).ToUpper() + " REGION"
            End If
            If GroupID > 0 Then
                Dim AuditDescr As String = ""
                DT = DB.ExecuteDataSet("select DATENAME(month,Period_to)+'/'+CAST(YEAR(Period_to)as varchar(4)) from audit_master where group_id=" + GroupID.ToString()).Tables(0)
                If DT.Rows.Count > 0 Then
                    AuditDescr = DT.Rows(0)(0).ToString()
                End If
                SubHead += "  " + AuditDescr.ToUpper() + " AUDIT"
            End If
            If StatusID > 0 Then
                SubHead += " STATUS - PENDING"
            ElseIf StatusID = 0 Then
                SubHead += " STATUS - CLOSED"
            End If
            RH.Heading(Session("FirmName"), tb, "QUERY RESPONSE TRACKER " + SubHead.ToUpper(), 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim PendingTotal As Integer = 0
            Dim CompletedTotal As Integer = 0
            Dim Total As Integer = 0
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "l", "Check List")
            RH.AddColumn(TRHead, TRHead_02, 20, 20, "c", "Closed")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "c", "Pending")
            RH.AddColumn(TRHead, TRHead_04, 20, 20, "c", "Total")


            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 5)
            'Dim SqlStr As String = "select b.observation_ID,f.item_name,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total ,c.Audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e,audit_check_List f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.item_id=f.item_id  and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2 and a.SINO in(select SINO from AUDIT_OBSERVATION_CYCLE where USER_ID=" + Session("UserID").ToString() + ") "

            Dim SqlStr As String = "select b.observation_ID,f.item_name,sum(case when a.STATUS_ID=0 then 1 else 0 end)as Closed,sum(case when a.STATUS_ID>0 then 1 else 0 end)as Pending,count(sino) as Total ,c.Audit_Type from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,BrMaster e,audit_check_List f where a.OBSERVATION_ID=b.OBSERVATION_ID and b.item_id=f.item_id  and b.AUDIT_ID =c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.BRANCH_ID=e.Branch_ID  and d.STATUS_ID=2  "
            If RegionID > 0 Then
                SqlStr += "and e.Region_ID=" + RegionID.ToString()
            End If
            If AreaID > 0 Then
                SqlStr += " and e.Area_ID=" + AreaID.ToString()
            End If
            If BranchID > 0 Then
                SqlStr += " and e.branch_id=" + BranchID.ToString()
            End If
            If GroupID > 0 Then
                SqlStr += " and d.group_id=" + GroupID.ToString()
            End If
            If StatusID > 0 Then
                SqlStr += " and a.status_id>0"
            ElseIf StatusID = 0 Then
                SqlStr += " and a.status_id=0"
            End If
            SqlStr += " group by b.observation_ID  ,f.item_name,c.Audit_Type"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows

                LineNumber += 1
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid

                I = I + 1
                RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString().ToString())
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", " <a href='QueryResponseTracker_Dtl.aspx?TypeID=" + DR(5).ToString() + " &StatusID=-1 &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &GroupID=" + GroupID.ToString() + " &ItemID=" + DR(0).ToString() + " '>" + DR(1).ToString())
                If (CInt(DR(2) > 0)) Then
                    RH.AddColumn(TR3, TR3_02, 20, 20, "c", " <a href='QueryResponseTracker_Dtl.aspx?TypeID=" + DR(5).ToString() + " &StatusID=0 &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &GroupID=" + GroupID.ToString() + " &ItemID=" + DR(0).ToString() + " '>" + DR(2).ToString())
                Else
                    RH.AddColumn(TR3, TR3_02, 20, 20, "c", DR(2).ToString())
                End If
                If (CInt(DR(3) > 0)) Then
                    RH.AddColumn(TR3, TR3_03, 20, 20, "c", "  <a href='QueryResponseTracker_Dtl.aspx?TypeID=" + DR(5).ToString() + " &StatusID=1 &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &GroupID=" + GroupID.ToString() + " &ItemID=" + DR(0).ToString() + "'>" + DR(3).ToString())
                Else
                    RH.AddColumn(TR3, TR3_03, 20, 20, "c", DR(3).ToString())
                End If
                If CInt(DR(4)) > 0 Then
                    RH.AddColumn(TR3, TR3_04, 20, 20, "c", "  <a href='QueryResponseTracker_Dtl.aspx?TypeID=-1 &StatusID=-1 &RegionID=" + GN.Encrypt(RegionID.ToString()) + " &AreaID=" + GN.Encrypt(AreaID.ToString()) + " &BranchID=" + GN.Encrypt(BranchID.ToString()) + " &GroupID=" + GroupID.ToString() + " &ItemID=" + DR(0).ToString() + "'>" + DR(4).ToString())
                Else
                    RH.AddColumn(TR3, TR3_04, 20, 20, "c", DR(4).ToString())
                End If

                PendingTotal += CInt(DR(3))
                CompletedTotal += CInt(DR(2))
                Total += CInt(DR(4))
                tb.Controls.Add(TR3)
            Next
            Dim TREnd As New TableRow
            TREnd.BackColor = Drawing.Color.WhiteSmoke
            Dim TREnd_00, TREnd_01, TREnd_02, TREnd_03, TREnd_04 As New TableCell

            TREnd_00.BorderWidth = "1"
            TREnd_01.BorderWidth = "1"
            TREnd_02.BorderWidth = "1"
            TREnd_03.BorderWidth = "1"
            TREnd_04.BorderWidth = "1"


            TREnd_00.BorderColor = Drawing.Color.Silver
            TREnd_01.BorderColor = Drawing.Color.Silver
            TREnd_02.BorderColor = Drawing.Color.Silver
            TREnd_03.BorderColor = Drawing.Color.Silver
            TREnd_04.BorderColor = Drawing.Color.Silver

            TREnd_00.BorderStyle = BorderStyle.Solid
            TREnd_01.BorderStyle = BorderStyle.Solid
            TREnd_02.BorderStyle = BorderStyle.Solid
            TREnd_03.BorderStyle = BorderStyle.Solid
            TREnd_04.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TREnd, TREnd_00, 5, 5, "c", "")
            RH.AddColumn(TREnd, TREnd_01, 35, 35, "l", "Total")
            RH.AddColumn(TREnd, TREnd_02, 20, 20, "c", CompletedTotal.ToString())
            RH.AddColumn(TREnd, TREnd_03, 20, 20, "c", PendingTotal.ToString())
            RH.AddColumn(TREnd, TREnd_04, 20, 20, "c", Total.ToString())
            tb.Controls.Add(TREnd)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        pnDisplay.RenderControl(hw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        Response.Write(pdfDoc)
        Response.[End]()
    End Sub
End Class
