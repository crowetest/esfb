﻿Imports System.Data
Partial Class Audit_Reports_AuditGradeReport
    Inherits System.Web.UI.Page
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 351) = False And GN.FormAccess(CInt(Session("UserID")), 89) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Grading Report"
            DT = DB.ExecuteDataSet("select count(*) from audit_user_levels where queue_id=2 and post_id=" + Session("Post_ID") + "").Tables(0)
            If CInt(DT.Rows(0)(0)) = 0 Then
                GN.ComboFill(cmbType, AD.GetGradeType(), 0, 1)
            Else
                GN.ComboFill(cmbType, AD.GetGradeTypeAll(), 0, 1)
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
