﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class QueuewiseReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            PostID = CInt(Session("Post_ID"))
            Dim PostName As String = ""
            PostName = GN.GetEmp_Post_Name(PostID)
            RH.Heading(Session("FirmName"), tb, "QUEUE WISE REPORT", 100)
            RH.SubHeading(tb, 100, "l", "Report Level - " + PostName)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 25, 25, "l", "Level")
            RH.AddColumn(TRHead, TRHead_01, 12, 12, "c", "Original")
            RH.AddColumn(TRHead, TRHead_02, 12, 12, "c", "Further")
            RH.AddColumn(TRHead, TRHead_03, 26, 26, "c", "Authorization")
            RH.AddColumn(TRHead, TRHead_04, 12, 12, "c", "For Closure")
            RH.AddColumn(TRHead, TRHead_05, 12, 12, "c", "Send For")
            tb.Controls.Add(TRHead)
            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06 As New TableCell

            TRHead1_00.BorderWidth = "1"
            TRHead1_01.BorderWidth = "1"
            TRHead1_02.BorderWidth = "1"
            TRHead1_03.BorderWidth = "1"
            TRHead1_04.BorderWidth = "1"
            TRHead1_05.BorderWidth = "1"
            TRHead1_06.BorderWidth = "1"

            TRHead1_00.BorderColor = Drawing.Color.Silver
            TRHead1_01.BorderColor = Drawing.Color.Silver
            TRHead1_02.BorderColor = Drawing.Color.Silver
            TRHead1_03.BorderColor = Drawing.Color.Silver
            TRHead1_04.BorderColor = Drawing.Color.Silver
            TRHead1_05.BorderColor = Drawing.Color.Silver
            TRHead1_06.BorderColor = Drawing.Color.Silver

            TRHead1_00.BorderStyle = BorderStyle.Solid
            TRHead1_01.BorderStyle = BorderStyle.Solid
            TRHead1_02.BorderStyle = BorderStyle.Solid
            TRHead1_03.BorderStyle = BorderStyle.Solid
            TRHead1_04.BorderStyle = BorderStyle.Solid
            TRHead1_05.BorderStyle = BorderStyle.Solid
            TRHead1_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead1, TRHead1_00, 25, 25, "l", "")
            RH.AddColumn(TRHead1, TRHead1_01, 12, 12, "c", "Response")
            RH.AddColumn(TRHead1, TRHead1_02, 12, 12, "c", "Response")
            RH.AddColumn(TRHead1, TRHead1_03, 13, 13, "c", "In Queue")
            RH.AddColumn(TRHead1, TRHead1_04, 13, 13, "c", "Potential")
            RH.AddColumn(TRHead1, TRHead1_05, 12, 12, "c", "")
            RH.AddColumn(TRHead1, TRHead1_06, 12, 12, "c", "Confirmation")

            tb.Controls.Add(TRHead1)
            'RH.BlankRow(tb, 3)
            Dim i, j As Integer
            Dim Org, Auth, Auth_Esc, Further, ForClos, ForConf As Integer
            Org = 0
            Auth = 0
            Auth_Esc = 0
            Further = 0
            ForClos = 0
            ForConf = 0
            Dim SqlStr As String = ""
            If PostID = 5 Then
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and d.STATUS_ID=2 and d.branch_id in(select branch_id from branch_master where branch_head=" + Session("UserID") + ") group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                    "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and e.LEVEL_ID > a.LEVEL_ID  and e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO  and d.branch_id in(select branch_id from branch_master where branch_head=" + Session("UserID") + ")  group by  e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            ElseIf PostID = 6 Then
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and d.branch_id in(select branch_id from brmaster where area_head=" + Session("UserID") + ") group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                              "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and e.LEVEL_ID > a.LEVEL_ID   and  e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO and d.branch_id in(select branch_id from brmaster where area_head=" + Session("UserID") + ")  group by  e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            ElseIf PostID = 7 Then
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and d.STATUS_ID=2 and d.branch_id in(select branch_id from brmaster where region_head=" + Session("UserID") + ") group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                              "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and e.LEVEL_ID > a.LEVEL_ID   and  e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO  and d.branch_id in(select branch_id from brmaster where region_head=" + Session("UserID") + ")  group by  e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            ElseIf PostID = 8 Then
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and d.STATUS_ID=2 and d.branch_id in(select branch_id from brmaster where Zone_head=" + Session("UserID") + ") group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                              "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and e.LEVEL_ID > a.LEVEL_ID   and  e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO and d.branch_id in(select branch_id from brmaster where Zone_head=" + Session("UserID") + ")  group by  e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            ElseIf PostID = 3 Then
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and d.branch_id in(select branch_id from audit_branches where team_lead=" + Session("UserID") + ") group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                              "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and e.LEVEL_ID > a.LEVEL_ID and  e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO and d.branch_id in(select branch_id from audit_branches where team_lead=" + Session("UserID") + ")  group by  e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            ElseIf PostID = 4 Then
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2 and d.branch_id in(select branch_id from audit_branches where emp_code=" + Session("UserID") + ") group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                              "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and e.LEVEL_ID > a.LEVEL_ID and  e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO and d.branch_id in(select branch_id from audit_branches where emp_code=" + Session("UserID") + ")  group by  e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            Else
                SqlStr = "select aaa.*,isnull(bbb.further,0)  as Escalated from  (select aa.LEVEL_ID,aa.LEVEL_NAME,ISNULL(bb.foreClosure,0) as ForeClosure,isnull(bb.original_response,0) as Original,ISNULL(bb.furtherResponse ,0) as further,ISNULL(bb.o_Authorization,0) as Auth,ISNULL(bb.Confirmation_Pending ,0) as Conf from audit_user_levels aa left outer join  (select a.LEVEL_ID  ,sum(case when a.STATUS_ID=5 then 1 else 0 end) as foreClosure,sum(case when a.STATUS_ID=1 and a.Response_Flag=0 and a.ROLE_ID=1 then 1 else 0 end) as original_response,SUM(case when a.STATUS_ID=1 and a.Response_Flag>0 and a.ROLE_ID=1 then 1 else 0 end) as furtherResponse,SUM(case when a.STATUS_ID=1 and a.ROLE_ID=4 then 1 else 0 end) as o_Authorization,sum(case when a.STATUS_ID=6 then 1 else 0 end) as Confirmation_Pending from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID and d.STATUS_ID=2  group by a.LEVEL_ID) bb on (aa.LEVEL_ID=bb.LEVEL_ID ) where aa.LEVEL_ID >0) aaa left outer join " & _
                                             "(select e.LEVEL_id ,count(SINo) as further from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION b,AUDIT_DTL c,AUDIT_MASTER d,Audit_User_levels e where a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=c.AUDIT_ID and c.GROUP_ID=d.GROUP_ID  and e.LEVEL_ID > a.LEVEL_ID and  e.LEVEL_ID <= a.LEVEL_UPTO  and d.STATUS_ID=2 and e.LEVEL_ID in (10,11) and a.STATUS_ID=1 and a.LEVEL_ID<a.LEVEL_UPTO  group by e.LEVEL_id) bbb on (aaa.LEVEL_ID=bbb.LEVEL_ID )"
            End If
            SqlStr += " order by aaa.level_id"
            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            For Each DR In DT.Rows
                j += 1
                i = 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 25, 25, "l", DR(1))
                If (DR(3) > 0) Then
                    RH.AddColumn(TR3, TR3_01, 12, 12, "c", "<a href='QueuewiseReport_1.aspx?StatusID=1 &LevelID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + CInt(DR(3)).ToString("#,##,###") + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_01, 12, 12, "c", "-")
                End If
                If (DR(4) > 0) Then
                    RH.AddColumn(TR3, TR3_02, 12, 12, "c", "<a href='QueuewiseReport_1.aspx?StatusID=2 &LevelID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + CInt(DR(4)).ToString("#,##,###") + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_02, 12, 12, "c", "-")
                End If
                If (DR(5) > 0) Then
                    RH.AddColumn(TR3, TR3_03, 13, 13, "c", "<a href='QueuewiseReport_1.aspx?StatusID=3 &LevelID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + CInt(DR(5)).ToString("#,##,###") + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_03, 13, 13, "c", "-")
                End If
                If (DR(7) > 0) Then
                    RH.AddColumn(TR3, TR3_04, 13, 13, "c", "<a href='QueuewiseReport_1.aspx?StatusID=5 &LevelID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + CInt(DR(7)).ToString("#,##,###") + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_04, 13, 13, "c", "-")
                End If
                If (DR(2) > 0) Then
                    RH.AddColumn(TR3, TR3_05, 12, 12, "c", "<a href='QueuewiseReport_1.aspx?StatusID=4 &LevelID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + CInt(DR(2)).ToString("#,##,###") + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_05, 12, 12, "c", "-")
                End If
                If (DR(6) > 0) Then
                    RH.AddColumn(TR3, TR3_06, 12, 12, "c", "<a href='QueuewiseReport_Conf.aspx?StatusID=6 &LevelID=" + GN.Encrypt(DR(0).ToString()) + "' style='text-align:right;' target='_blank'>" + CInt(DR(6)).ToString("#,##,###") + "</a>")
                Else
                    RH.AddColumn(TR3, TR3_06, 12, 12, "c", "-")
                End If
                tb.Controls.Add(TR3)
                i = i + 1
                Org += CInt(DR(3))
                Further += CInt(DR(4))
                Auth += CInt(DR(5))
                Auth_Esc += CInt(DR(7))
                ForClos += CInt(DR(2))
                ForConf += CInt(DR(6))
            Next



            Dim TR_End As New TableRow
            Dim TR_End_00, TR_End_01, TR_End_02, TR_End_03, TR_End_04, TR_End_05, TR_End_06 As New TableCell
            TR_End_00.BorderWidth = "1"
            TR_End_01.BorderWidth = "1"
            TR_End_02.BorderWidth = "1"
            TR_End_03.BorderWidth = "1"
            TR_End_04.BorderWidth = "1"
            TR_End_05.BorderWidth = "1"
            TR_End_06.BorderWidth = "1"

            TR_End_00.BorderColor = Drawing.Color.Silver
            TR_End_01.BorderColor = Drawing.Color.Silver
            TR_End_02.BorderColor = Drawing.Color.Silver
            TR_End_03.BorderColor = Drawing.Color.Silver
            TR_End_04.BorderColor = Drawing.Color.Silver
            TR_End_05.BorderColor = Drawing.Color.Silver
            TR_End_06.BorderColor = Drawing.Color.Silver

            TR_End_00.BorderStyle = BorderStyle.Solid
            TR_End_01.BorderStyle = BorderStyle.Solid
            TR_End_02.BorderStyle = BorderStyle.Solid
            TR_End_03.BorderStyle = BorderStyle.Solid
            TR_End_04.BorderStyle = BorderStyle.Solid
            TR_End_05.BorderStyle = BorderStyle.Solid
            TR_End_06.BorderStyle = BorderStyle.Solid

            TR_End.Style.Add("Font-Weight", "Bold")
            RH.AddColumn(TR_End, TR_End_00, 25, 25, "l", "Total")
            If (Org > 0) Then
                RH.AddColumn(TR_End, TR_End_01, 12, 12, "c", Org)
            Else
                RH.AddColumn(TR_End, TR_End_01, 12, 12, "c", "-")
            End If
            If (Further > 0) Then
                RH.AddColumn(TR_End, TR_End_02, 12, 12, "c", Further)
            Else
                RH.AddColumn(TR_End, TR_End_02, 12, 12, "c", "-")
            End If
            If (Auth > 0) Then
                RH.AddColumn(TR_End, TR_End_03, 13, 13, "c", Auth)
            Else
                RH.AddColumn(TR_End, TR_End_03, 13, 13, "c", "-")
            End If
            If (Auth_Esc > 0) Then
                RH.AddColumn(TR_End, TR_End_04, 13, 13, "c", Auth_Esc)
            Else
                RH.AddColumn(TR_End, TR_End_04, 13, 13, "c", "-")
            End If
            If (ForClos > 0) Then
                RH.AddColumn(TR_End, TR_End_05, 12, 12, "c", ForClos)
            Else
                RH.AddColumn(TR_End, TR_End_05, 12, 12, "c", "-")
            End If
            If (ForConf > 0) Then
                RH.AddColumn(TR_End, TR_End_06, 12, 12, "c", ForConf)
            Else
                RH.AddColumn(TR_End, TR_End_06, 12, 12, "c", "-")
            End If

            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click
    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
