﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewEmergencyAudits
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AssingDt As String
    Dim From As Integer
    Dim RptID As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim SubHD As String = ""
    Dim DTExcel As New DataTable
    Dim AssignDtl() As String
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            Dim Postid As Integer = CInt(Session("Post_ID"))
            Dim EmpCode As Integer = CInt(Session("UserID"))

            Dim DR As DataRow
            AssingDt = ""
            FromDt = DateTime.Now.Date
            ToDt = DateTime.Now.Date
            Dim StrVal As String = ""


            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            tb.Attributes.Add("width", "100%")
            SubHD = FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy") + ""
            RH.Heading(Session("FirmName"), tb, "Emergency Audit List of during  " + FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy"), 100)

            DT = DB.ExecuteDataSet("select distinct a.branch_id,b.branch_name, nm.Resources " & _
                    " from AUDIT_MASTER a,branch_master b  left join (SELECT distinct   r.branch_id, Resources =  STUFF( (SELECT ','+ar.Name FROM  " & _
                    " (select i.branch_id,I.Branch_Name,  convert(varchar,DATENAME(month,PERIOD_TO))+'-'+ convert(varchar,year(PERIOD_TO)) as Name  " & _
                    " from AUDIT_MASTER j ,BrMaster i  where   j.BRANCH_ID=i.Branch_ID  " & _
                    " and DATEADD(day, DATEDIFF(day, 0, PERIOD_TO), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' and emergency_flag=1 ) ar  WHERE ar.BRANCH_ID " & _
                    " = r.BRANCH_ID   FOR XML PATH('')), 1, 1, '')  FROM  ( select i.branch_id,I.Branch_Name,j.GROUP_ID   from AUDIT_MASTER j ,BrMaster i " & _
                    " where j.BRANCH_ID = I.Branch_ID And DateAdd(Day, DateDiff(Day, 0, PERIOD_TO), 0)  between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' " & _
                    " and emergency_flag=1) r)nm on b.branch_id=nm.branch_id    where a.branch_id = b.branch_id " & _
                    " And DateAdd(Day, DateDiff(Day, 0, a.PERIOD_TO), 0)   between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'  and emergency_flag=1 order by b.branch_name").Tables(0)



            DTExcel = DB.ExecuteDataSet("select distinct  a.branch_id as BranchID,b.branch_name as Branch,nm.Resources as Periods " & _
                    " from AUDIT_MASTER a,branch_master b  left join (SELECT distinct   r.branch_id, Resources =  STUFF( (SELECT ','+ar.Name FROM  " & _
                    " (select i.branch_id,I.Branch_Name,  convert(varchar,DATENAME(month,PERIOD_TO))+'-'+ convert(varchar,year(PERIOD_TO)) as Name  " & _
                    " from AUDIT_MASTER j ,BrMaster i  where   j.BRANCH_ID=i.Branch_ID  " & _
                    " and DATEADD(day, DATEDIFF(day, 0, PERIOD_TO), 0) between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' and emergency_flag=1 ) ar  WHERE ar.BRANCH_ID " & _
                    " = r.BRANCH_ID   FOR XML PATH('')), 1, 1, '')  FROM  ( select i.branch_id,I.Branch_Name,j.GROUP_ID   from AUDIT_MASTER j ,BrMaster i " & _
                    " where j.BRANCH_ID = I.Branch_ID And DateAdd(Day, DateDiff(Day, 0, PERIOD_TO), 0)  between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "' " & _
                    " and emergency_flag=1) r)nm on b.branch_id=nm.branch_id    where a.branch_id = b.branch_id " & _
                    " And DateAdd(Day, DateDiff(Day, 0, a.PERIOD_TO), 0)   between '" + FromDt.ToString("dd/MMM/yyyy") + "' and '" + ToDt.ToString("dd/MMM/yyyy") + "'  and emergency_flag=1 order by b.branch_name").Tables(0)


            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 10, 0, "BranchID")
            RH.InsertColumn(TRHead, TRHead_02, 35, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_03, 50, 0, "Periods")
            Dim I As Integer = 1
            tb.Controls.Add(TRHead)

            Dim Total As Integer = 0

            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                RH.InsertColumn(TR3, TR3_01, 10, 0, DR(0).ToString())
                RH.InsertColumn(TR3, TR3_02, 35, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 50, 0, DR(2).ToString())

                tb.Controls.Add(TR3)
                'Total = Total + CInt(DR(0))
                I = I + 1

            Next
            Dim TR4 As New TableRow
            TR4.BorderWidth = "1"
            TR4.BorderStyle = BorderStyle.Solid

            'Dim TR4_02, TR4_03 As New TableCell

            'RH.InsertColumn(TR4, TR4_02, 70, 1, "<b>Total Centers</b>")
            'RH.InsertColumn(TR4, TR4_03, 30, 2, Total.ToString())

            'tb.Controls.Add(TR4)
            'RH.BlankRow(tb, 20)

            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try



    End Sub

    Protected Sub cmd_Excel_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel_Export.Click


        ''Response.Clear()
        ''Response.Buffer = True
        ''Response.ContentType = "application/vnd.ms-excel"
        ''Response.AddHeader("content-disposition", "attachment;filename=Report.xls")
        ''Response.Charset = ""
        ''Me.EnableViewState = False

        ''Dim sw As New System.IO.StringWriter()
        ''Dim htw As New System.Web.UI.HtmlTextWriter(sw)

        ''pnDisplay.RenderControl(htw)

        ''Response.Write(sw.ToString())
        ''Response.[End]()
        Dim HeaderText As String

        HeaderText = "Skipped Audit List of during  " + FromDt.ToString("dd MMM yyyy") + "-" + ToDt.ToString("dd MMM yyyy")


        WebTools.ExporttoExcel(DTExcel, HeaderText)

    End Sub
End Class
