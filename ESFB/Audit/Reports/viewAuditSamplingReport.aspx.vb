﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewAuditSamplingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim AuditTypeID As Integer
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            AuditID = CInt(GF.Decrypt(Request.QueryString.Get("AuditID")))
            AuditTypeID = CInt(Request.QueryString.Get("AuditTypeID"))
            hdnAuditID.Value = AuditID.ToString()
            hdnAuditTypeID.Value = AuditTypeID.ToString()

            Dim DTAudit As New DataTable
            DTAudit = DB.ExecuteDataSet("SELECT AUDIT_ID,d.Branch_Name,isnull(convert(varchar(11),START_DT,106),'') as StartDt,isnull(convert(varchar(11),END_DT,106),'') as End_dt,B.Emp_Name,C.TYPE_NAME AS AUDIT_TYPE,(CASE WHEN (A.STATUS_ID=0) THEN 'Yet to Start' when a.status_id=1 then 'In Progress' else 'Closed' end) as Status FROM AUDIT_DTL A,EMP_MASTER B,AUDIT_TYPE C,Branch_master D WHERE A.AUDIT_TYPE=C.TYPE_ID AND A.EMP_CODE=B.Emp_Code and a.Branch_id=D.branch_ID AND A.AUDIT_ID=" + AuditID.ToString()).Tables(0)

            RH.Heading(Session("FirmName"), tb, "AUDIT SAMPLING REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim TR0 As New TableRow
            Dim TR0_01, TR0_02, TR0_03 As New TableCell
            RH.AddColumn(TR0, TR0_01, 10, 10, "l", "Branch")
            RH.AddColumn(TR0, TR0_02, 2, 2, "l", " : ")
            RH.AddColumn(TR0, TR0_03, 88, 88, "l", DTAudit.Rows(0)(1).ToString())



            tb.Controls.Add(TR0)


            Dim TR2 As New TableRow
            Dim TR2_01, TR2_02, TR2_03 As New TableCell
            RH.AddColumn(TR2, TR2_01, 10, 10, "l", "Auditor")
            RH.AddColumn(TR2, TR2_02, 2, 2, "l", " : ")
            RH.AddColumn(TR2, TR2_03, 88, 88, "l", DTAudit.Rows(0)(4).ToString())

            tb.Controls.Add(TR2)



            'RH.DrawLine(tb, 100)
            RH.BlankRow(tb, 15)

            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0
            Dim GROUPID As Integer = 0
            Dim SUBGROUPID As Integer = 0
            Dim ITEMID As Integer = 0
            Dim TRAuditHead As New TableRow
            Dim TRAuditHead_00 As New TableCell
            RH.AddColumn(TRAuditHead, TRAuditHead_00, 100, 100, "c", DTAudit.Rows(0)(5).ToString())
            'TRAuditHead_00.BackColor = System.Drawing.Color.BurlyWood
            TRAuditHead_00.BackColor = System.Drawing.Color.Gainsboro
            tb.Controls.Add(TRAuditHead)
            If (AuditTypeID <> 3) Then
                DT = DB.ExecuteDataSet("select e.center_name,c.center_id,convert(numeric(16,0),c.loan_amt), a.LOAN_NO,c.Client_ID,dbo.udfpropercase(c.CLIENT_NAME),c.Scheme_ID,convert(varchar(11),isnull(c.Disbursed_dt,c.SANCTIONED_DT),106)   from audit_accounts a,AUDIT_DTL b,LOAN_MASTER c,CENTER_MASTER e " & _
                    "where a.AUDIT_ID=b.AUDIT_ID and a.LOAN_NO=c.Loan_No and b.AUDIT_TYPE<>3 and c.center_id=e.center_id and b.audit_id=" + AuditID.ToString() + " order by c.center_id,c.CLIENT_NAME,a.LOAN_NO").Tables(0)
                For Each DR In DT.Rows

                    LineNumber += 1
                    j += 1
                    If (ITEMID <> CInt(DR(1))) Then
                        I = 1
                        Dim TRItemHead As New TableRow
                        TRItemHead.ForeColor = Drawing.Color.DarkBlue
                        Dim TRItemHead_00 As New TableCell
                        RH.BlankRow(tb, 5)
                        RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "l", "<b><i>Center Name : " + DR(0).ToString() + " (" + DR(1).ToString() + ") </i></b>")

                        tb.Controls.Add(TRItemHead)
                        RH.BlankRow(tb, 5)
                        Dim TRHead As New TableRow
                        TRHead.BackColor = Drawing.Color.WhiteSmoke
                        Dim TRHead_00, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_01, TRHead_06 As New TableCell

                        TRHead_00.BorderWidth = "1"
                        TRHead_01.BorderWidth = "1"
                        TRHead_02.BorderWidth = "1"
                        TRHead_03.BorderWidth = "1"
                        TRHead_04.BorderWidth = "1"
                        TRHead_05.BorderWidth = "1"
                        TRHead_06.BorderWidth = "1"

                        TRHead_00.BorderColor = Drawing.Color.Silver
                        TRHead_01.BorderColor = Drawing.Color.Silver
                        TRHead_02.BorderColor = Drawing.Color.Silver
                        TRHead_03.BorderColor = Drawing.Color.Silver
                        TRHead_04.BorderColor = Drawing.Color.Silver
                        TRHead_05.BorderColor = Drawing.Color.Silver
                        TRHead_06.BorderColor = Drawing.Color.Silver

                        TRHead_00.BorderStyle = BorderStyle.Solid
                        TRHead_01.BorderStyle = BorderStyle.Solid
                        TRHead_02.BorderStyle = BorderStyle.Solid
                        TRHead_03.BorderStyle = BorderStyle.Solid
                        TRHead_04.BorderStyle = BorderStyle.Solid
                        TRHead_05.BorderStyle = BorderStyle.Solid
                        TRHead_06.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                        RH.AddColumn(TRHead, TRHead_02, 18, 18, "l", "Ref. No")
                        RH.AddColumn(TRHead, TRHead_06, 13, 13, "l", "Date")
                        RH.AddColumn(TRHead, TRHead_03, 15, 15, "l", "Client ID")
                        RH.AddColumn(TRHead, TRHead_04, 29, 29, "l", "Name")
                        RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Product")
                        RH.AddColumn(TRHead, TRHead_01, 10, 10, "r", "Loan Amt")
                        tb.Controls.Add(TRHead)
                        'RH.BlankRow(tb, 3)
                    End If

                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim Type As String = ""

                    Dim TR3_00, TR3_02, TR3_03, TR3_04, TR3_05, TR3_01, TR3_06 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"


                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver


                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid


                    RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString())
                    RH.AddColumn(TR3, TR3_02, 18, 18, "l", DR(3))
                    RH.AddColumn(TR3, TR3_06, 13, 13, "l", DR(7))
                    RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(4))
                    RH.AddColumn(TR3, TR3_04, 29, 29, "l", DR(5))
                    RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(6))
                    RH.AddColumn(TR3, TR3_01, 10, 10, "r", DR(2))

                    tb.Controls.Add(TR3)
                    I = I + 1

                    ITEMID = CInt(DR(1))

                Next
            ElseIf AuditTypeID = 3 Then
                DT = DB.ExecuteDataSet("select 1,c.GROUP_ID,c.GROUP_NAME,d.SUB_GROUP_ID,d.SUB_GROUP_NAME,e.ITEM_ID,e.ITEM_NAME from AUDIT_CHECK_GROUPS c,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_LIST e where  e.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=c.GROUP_ID and c.audit_type=3  order by c.GROUP_ID,d.SUB_GROUP_ID,e.ITEM_ID").Tables(0)
                For Each DR In DT.Rows

                    LineNumber += 1
                    j += 1
                    If (SUBGROUPID <> CInt(DR(3))) Then
                        I = 1
                        Dim TRItemHead As New TableRow
                        TRItemHead.ForeColor = Drawing.Color.DarkBlue
                        Dim TRItemHead_00 As New TableCell
                        RH.BlankRow(tb, 5)
                        RH.AddColumn(TRItemHead, TRItemHead_00, 100, 100, "l", "<b><i>" + DR(2).ToString() + " / " + DR(4).ToString() + "</i></b>")

                        tb.Controls.Add(TRItemHead)
                        RH.BlankRow(tb, 5)
                        Dim TRHead As New TableRow
                        TRHead.BackColor = Drawing.Color.WhiteSmoke
                        Dim TRHead_00, TRHead_01 As New TableCell

                        TRHead_00.BorderWidth = "1"
                        TRHead_01.BorderWidth = "1"



                        TRHead_00.BorderColor = Drawing.Color.Silver
                        TRHead_01.BorderColor = Drawing.Color.Silver


                        TRHead_00.BorderStyle = BorderStyle.Solid
                        TRHead_01.BorderStyle = BorderStyle.Solid


                        RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "#")
                        RH.AddColumn(TRHead, TRHead_01, 95, 95, "l", "Check List")

                        tb.Controls.Add(TRHead)
                        'RH.BlankRow(tb, 3)
                    End If
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01 As New TableCell

                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"



                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver



                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid



                    RH.AddColumn(TR3, TR3_00, 5, 5, "c", I.ToString())
                    RH.AddColumn(TR3, TR3_01, 95, 95, "l", DR(6))


                    tb.Controls.Add(TR3)
                    I = I + 1
                    GROUPID = CInt(DR(1))
                    SUBGROUPID = CInt(DR(3))
                    ITEMID = CInt(DR(5))
                Next
            End If
            RH.BlankRow(tb, 20)

            Dim TR_Decl As New TableRow
            Dim TR_Decl_00 As New TableCell
            'TR_Decl.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_Decl, TR_Decl_00, 100, 100, "c", "<b> This is a system generated Report and does not require signature </b>")
            tb.Controls.Add(TR_Decl)
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            If hdnAuditTypeID.Value = "1" Then
                HeaderText = "BranchAuditAccounts"
            ElseIf hdnAuditTypeID.Value = "2" Then
                HeaderText = "FieldAuditAccounts"
            Else
                HeaderText = "GeneralAuditAccounts"
            End If
            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
