﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewMisleadObservationStatusReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim GroupID As Integer
    Dim MonthDesc As String
    Dim subHead As String
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GroupID = CInt(GF.Decrypt(Request.QueryString.Get("GroupID")))
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            subHead = "Observation Confirmation Status"
            RH.Heading(Session("FirmName"), tb, subHead, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell

            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "#")
            RH.InsertColumn(TRHead, TRHead_01, 55, 0, "Item Name")
            RH.InsertColumn(TRHead, TRHead_02, 20, 0, "Total Count")
            RH.InsertColumn(TRHead, TRHead_03, 20, 0, "Updated Count")

            tb.Controls.Add(TRHead)
            RH.BlankRow(tb, 5)

            DT = DB.ExecuteDataSet("select a.item_id,f.item_name,SUM(CONFIRMATION_Flag) as updated,count(b.SINO) as TotalCount from AUDIT_OBSERVATION a,AUDIT_OBSERVATION_DTL b,AUDIT_DTL c,(select *  from audit_observation_cycle where  " & _
            " SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17 )) j, " & _
            " dbo.AUDIT_CHECK_LIST  f   ,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e where a.OBSERVATION_ID=b.OBSERVATION_ID " & _
            " and a.AUDIT_ID=c.AUDIT_ID and  b.order_ID=j.order_ID and a.item_id =f.ITEM_ID and f.SUB_GROUP_ID=d.SUB_GROUP_ID and " & _
            " d.GROUP_ID = e.GROUP_ID " & _
            " and c.GROUP_ID=" + GroupID.ToString() + " and b.status_id=0  group by a.item_id,f.item_name").Tables(0)



            For Each DR In DT.Rows


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04 As New TableCell
                I = I + 1
                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString().ToString())
                RH.InsertColumn(TR3, TR3_01, 55, 0, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_02, 20, 0, DR(3).ToString())
                RH.InsertColumn(TR3, TR3_03, 20, 0, DR(2).ToString())


                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
