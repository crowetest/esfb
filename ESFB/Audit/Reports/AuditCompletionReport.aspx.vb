﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_AuditCompletionReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim StatusID As Integer
    Dim MonthDesc As String
    Dim GF As New GeneralFunctions
    Dim User_Id As Integer
    Dim Post_Id As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            StatusID = CInt(GF.Decrypt(Request.QueryString.Get("StatusID")))
            MonthDesc = Request.QueryString.Get("MonthDesc")
            User_Id = Request.QueryString.Get("UserId")
            Post_Id = Request.QueryString.Get("PostId")
            Dim SubHead As String = ""
            SubHead = Request.QueryString.Get("SubHD")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim DT As New DataTable
            RH.Heading(Session("FirmName"), tb, SubHead, 100)
            RH.BlankRow(tb, 5)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, WholeHelper.ClsRepCtrl.Allign.Centre, "#")
            RH.InsertColumn(TRHead, TRHead_01, 20, WholeHelper.ClsRepCtrl.Allign.Left, "Branch")
            RH.InsertColumn(TRHead, TRHead_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, "Auditor")
            RH.InsertColumn(TRHead, TRHead_03, 10, WholeHelper.ClsRepCtrl.Allign.Left, "Completed On")
            RH.InsertColumn(TRHead, TRHead_04, 15, WholeHelper.ClsRepCtrl.Allign.Left, "Period From")
            RH.InsertColumn(TRHead, TRHead_05, 15, WholeHelper.ClsRepCtrl.Allign.Left, "Period To")
            RH.InsertColumn(TRHead, TRHead_06, 15, WholeHelper.ClsRepCtrl.Allign.Left, "Team Lead")
            tb.Controls.Add(TRHead)
            'If MonthDesc <> "" Then
            '    DT = DB.ExecuteDataSet("select C.Branch_Name,D.Emp_Name,a.END_DT,a.PERIOD_FROM ,a.PERIOD_TO ,f.Emp_Name as TeamLead  from AUDIT_MASTER A,BRANCH_MASTER C,EMP_MASTER D,AUDIT_BRANCHES e,EMP_MASTER f WHERE  A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.BRANCH_ID=e.BRANCH_ID and e.TEAM_LEAD=f.Emp_Code AND A.Status_ID=" + StatusID.ToString() + " AND DATENAME(month,A.Period_to)+'/'+CAST(YEAR(A.Period_to)as varchar(4))='" + MonthDesc + "' and a.skip_flag=0 ").Tables(0)
            'Else
            '    DT = DB.ExecuteDataSet("select C.Branch_Name,D.Emp_Name,a.END_DT,a.PERIOD_FROM ,a.PERIOD_TO ,f.Emp_Name as TeamLead  from AUDIT_MASTER A,BRANCH_MASTER C,EMP_MASTER D,AUDIT_BRANCHES e,EMP_MASTER f WHERE  A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.BRANCH_ID=e.BRANCH_ID and e.TEAM_LEAD=f.Emp_Code AND A.Status_ID=" + StatusID.ToString() + " and a.skip_flag=0 ").Tables(0)
            'End If
            Dim Str, StrWhere As String
            If MonthDesc <> "" Then
                Str = "select C.Branch_Name,D.Emp_Name,a.END_DT,a.PERIOD_FROM ,a.PERIOD_TO ,f.Emp_Name as TeamLead  from AUDIT_MASTER A,BRANCH_MASTER C,EMP_MASTER D,AUDIT_BRANCHES e,EMP_MASTER f, AUDIT_ADMIN_BRANCHES g, EMP_MASTER h  WHERE  A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.BRANCH_ID=e.BRANCH_ID and e.TEAM_LEAD=f.Emp_Code and a.BRANCH_ID=g.BRANCH_ID and  g.EMP_CODE=h.Emp_Code  AND A.Status_ID=" + StatusID.ToString() + " AND DATENAME(month,A.Period_to)+'/'+CAST(YEAR(A.Period_to)as varchar(4))='" + MonthDesc + "' and a.skip_flag=0 "
            Else
                Str = "select C.Branch_Name,D.Emp_Name,a.END_DT,a.PERIOD_FROM ,a.PERIOD_TO ,f.Emp_Name as TeamLead  from AUDIT_MASTER A,BRANCH_MASTER C,EMP_MASTER D,AUDIT_BRANCHES e,EMP_MASTER f, AUDIT_ADMIN_BRANCHES g, EMP_MASTER h  WHERE  A.Branch_ID=C.Branch_ID AND A.Emp_Code=D.Emp_Code and a.BRANCH_ID=e.BRANCH_ID and e.TEAM_LEAD=f.Emp_Code and a.BRANCH_ID=g.BRANCH_ID and  g.EMP_CODE=h.Emp_Code  AND A.Status_ID=" + StatusID.ToString() + " and a.skip_flag=0  "
            End If
            If Post_Id = 1 Or Post_Id = 2 Then 'Audit Head/TM
                StrWhere = " And 1 = 1 "
                'ElseIf Post_Id = 2 Then 'TM
                '    StrWhere = " And g.EMP_CODE =" + User_Id.ToString()
            ElseIf Post_Id = 3 Then 'TL
                StrWhere = " And e.TEAM_LEAD =" + User_Id.ToString()
            ElseIf Post_Id = 4 Then 'Auditor
                StrWhere = " And A.Emp_Code =" + User_Id.ToString()
            End If

            DT = DB.ExecuteDataSet(Str + StrWhere).Tables(0)

            For Each DR In DT.Rows

                LineNumber += 1
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid


                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06 As New TableCell
                I = I + 1
                RH.InsertColumn(TR3, TR3_00, 5, WholeHelper.ClsRepCtrl.Allign.Centre, I.ToString().ToString())
                RH.InsertColumn(TR3, TR3_01, 20, WholeHelper.ClsRepCtrl.Allign.Left, DR(0).ToString())
                RH.InsertColumn(TR3, TR3_02, 20, WholeHelper.ClsRepCtrl.Allign.Left, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 10, WholeHelper.ClsRepCtrl.Allign.Left, CDate(DR(2)).ToString("dd MMM yyyy"))
                RH.InsertColumn(TR3, TR3_04, 15, WholeHelper.ClsRepCtrl.Allign.Left, CDate(DR(3)).ToString("dd MMM yyyy"))
                RH.InsertColumn(TR3, TR3_05, 15, WholeHelper.ClsRepCtrl.Allign.Left, CDate(DR(4)).ToString("dd MMM yyyy"))
                RH.InsertColumn(TR3, TR3_06, 15, WholeHelper.ClsRepCtrl.Allign.Left, DR(5).ToString())
                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 20)
            'RH.DrawLine(tb, 100)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
