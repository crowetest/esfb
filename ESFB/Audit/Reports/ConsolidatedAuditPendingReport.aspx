﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="ConsolidatedAuditPendingReport.aspx.vb" Inherits="ConsolidatedAuditPendingReport" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#E0E0E0;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            color:#036;
        }        
        .style1
        {
            width: 100%;
        }
        .NormalText
{
    font-family:Cambria;
    font-size:10pt;
    text-transform:uppercase;
}
.NormalText:focus
{
    border-style:solid;
    border-width:thin;    
    border-color:#CFEBFD;
}
    </style>
   
</head>
<body style="background-color:Black;">

 <script language="javascript" type="text/javascript">
     function OnLoad() 
     {
         if (document.getElementById("<%= hdnPostID.ClientID %>").value == 5) {
             document.getElementById("<%= BranchView.ClientID %>").style.display = '';
             document.getElementById("<%= Refresh.ClientID %>").style.display = 'none';
             document.getElementById("<%= Branch.ClientID %>").style.display = 'none';
             document.getElementById("<%= Area.ClientID %>").style.display = 'none';
             document.getElementById("<%= Region.ClientID %>").style.display = 'none';
             document.getElementById("<%= Territory.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlTerritory.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlRegion.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlArea.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlBranch.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlView.ClientID %>").style.display = 'none';
         }
         else if (document.getElementById("<%= hdnPostID.ClientID %>").value == 6) {
             document.getElementById("<%= BranchView.ClientID %>").style.display = 'none';
             document.getElementById("<%= Refresh.ClientID %>").style.display = '';
             document.getElementById("<%= Branch.ClientID %>").style.display = '';
             document.getElementById("<%= Area.ClientID %>").style.display = '';
             document.getElementById("<%= Region.ClientID %>").style.display = 'none';
             document.getElementById("<%= Territory.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlTerritory.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlRegion.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlArea.ClientID %>").style.display = '';
             document.getElementById("<%= ddlBranch.ClientID %>").style.display = '';
             document.getElementById("<%= ddlView.ClientID %>").style.display = 'none';
         }
         else if (document.getElementById("<%= hdnPostID.ClientID %>").value == 7) {
             document.getElementById("<%= BranchView.ClientID %>").style.display = 'none';
             document.getElementById("<%= Refresh.ClientID %>").style.display = '';
             document.getElementById("<%= Branch.ClientID %>").style.display = '';
             document.getElementById("<%= Area.ClientID %>").style.display = '';
             document.getElementById("<%= Region.ClientID %>").style.display = '';
             document.getElementById("<%= Territory.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlTerritory.ClientID %>").style.display = 'none';
             document.getElementById("<%= ddlRegion.ClientID %>").style.display = '';
             document.getElementById("<%= ddlArea.ClientID %>").style.display = '';
             document.getElementById("<%= ddlBranch.ClientID %>").style.display = '';
             document.getElementById("<%= ddlView.ClientID %>").style.display = 'none';
         }
         else  {
             document.getElementById("<%= BranchView.ClientID %>").style.display = 'none';
             document.getElementById("<%= Branch.ClientID %>").style.display = '';
             document.getElementById("<%= Area.ClientID %>").style.display = '';
             document.getElementById("<%= Region.ClientID %>").style.display = '';
             document.getElementById("<%= Territory.ClientID %>").style.display = '';
             document.getElementById("<%= ddlTerritory.ClientID %>").style.display = '';
             document.getElementById("<%= ddlRegion.ClientID %>").style.display = '';
             document.getElementById("<%= ddlArea.ClientID %>").style.display = '';
             document.getElementById("<%= ddlBranch.ClientID %>").style.display = '';
             document.getElementById("<%= ddlView.ClientID %>").style.display = 'none';
         }
         
     }

//     function GenerateOnclick() {
//         document.getElementById("<%= cmbStatus.ClientID %>").disabled = true;
//         return true
//     }

//     function ChangeImage() {
//         document.getElementById("<%= cmbStatus.ClientID %>").disabled = true;
//         var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;
//         if (ID == 1) {
//             document.getElementById("<%= Branch.ClientID %>").src = "../../Image/Branch_sel.gif";
//             document.getElementById("<%= Area.ClientID %>").src = "../../Image/Area.gif";
//             document.getElementById("<%= Region.ClientID %>").src = "../../Image/Region.gif";
//             document.getElementById("<%= Territory.ClientID %>").src = "../../Image/Territory.gif"; 
//             document.getElementById("<%= hdnRefresh.ClientID %>").value = 1;
//         }
//         else if (ID == 2) {
//             
//             document.getElementById("<%= Branch.ClientID %>").src = "../../Image/Branch.gif";
//             document.getElementById("<%= Area.ClientID %>").src = "../../Image/Area_sel.gif";
//             document.getElementById("<%= Region.ClientID %>").src = "../../Image/Region.gif";
//             document.getElementById("<%= Territory.ClientID %>").src = "../../Image/Territory.gif";
//             document.getElementById("<%= hdnRefresh.ClientID %>").value = 1;
//         }
//         else if (ID == 3) {
//             
//             document.getElementById("<%= Branch.ClientID %>").src = "../../Image/Branch.gif";
//             document.getElementById("<%= Area.ClientID %>").src = "../../Image/Area.gif";
//             document.getElementById("<%= Region.ClientID %>").src = "../../Image/Region_sel.gif";
//             document.getElementById("<%= Territory.ClientID %>").src = "../../Image/Territory.gif";
//             document.getElementById("<%= hdnRefresh.ClientID %>").value = 1;
//         }
//         else if (ID == 4) {
//             
//             document.getElementById("<%= Branch.ClientID %>").src = "../../Image/Branch.gif";
//             document.getElementById("<%= Area.ClientID %>").src = "../../Image/Area.gif";
//             document.getElementById("<%= Region.ClientID %>").src = "../../Image/Region.gif";
//             document.getElementById("<%= Territory.ClientID %>").src = "../../Image/Territory_sel.gif";
//             document.getElementById("<%= hdnRefresh.ClientID %>").value = 1;
//         }
//         return true;
//         }

     function PrintPanel() 
     {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () 
         {
             printWindow.print();
         }, 500);
         return false;
     }
   
     function btnExit_onclick() {
         window.open('../../home.aspx', '_self');
         return false;
     }
     </script>
    
     <form id="form1" runat="server"> 
       
    <div style="width:100%; background-color:white; min-height:750px; margin:0px auto;">
    <div>
     <br /> 
    <table  style="width:95%;margin: 0px auto;font-family:Cambria;"  >
    <tr style="background-color:#EAE7E8;height:20px;">
    <td style="text-align: center;font-size:13pt;" colspan="4"><asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager><b>Audit Pending Report</b></td>
    </tr>
    <tr id="Tr1" style="background-color:#EAE7E8;">
    <td style="width:25%; text-align: center;" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="cmd_Export" align="right" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Click to Export"/>
        <asp:ImageButton ID="cmd_Print" align="right" style="text-align:right ;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            </td>
    
    </tr>
    <tr style="background-color:#EAE7E8;">
    <td style="width:15%; text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
    <td style="width:25%;">
                <asp:DropDownList ID="cmbStatus" class="NormalText" 
                    style="text-align: left;" runat="server" Font-Names="Cambria" 
                Width="95%" ForeColor="Black" AutoPostBack="True">
                    <asp:ListItem value="1">PENDING</asp:ListItem>
                    <asp:ListItem value="0">ALL</asp:ListItem>
                    </asp:DropDownList> 
                </td>
    <td style="width:45%; text-align: right;">Standard Days for Closure of Audit Report</td>
    <td style="width:15%; text-align:center;">
                <asp:TextBox ID="txtClosureDays" runat="server" class="NormalText" 
                    style="text-align: left;color:red;font-size:13pt;font-weight:bold;" Font-Names="Cambria" 
                Width="40%"  ReadOnly="true">30</asp:TextBox>
                </td>
    </tr>
    <tr id="rowDate" style="background-color:#EAE7E8;">
    <td style="width:25%; text-align: center;" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    
    </tr>
    <tr style="background-color:#EAE7E8;">
    <td style="width:15%; text-align: left;" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="Branch" ImageUrl="../../Image/Branch.gif"   runat="server" style="cursor:hand;cursor:pointer;width:150px" />
    <asp:ImageButton ID="BranchView" ImageUrl="../../Image/BranchView.gif" runat="server"  style="cursor:hand;cursor:pointer;width:150px" />
    <asp:ImageButton ID="Area" ImageUrl="../../Image/Area.gif" runat="server"  style="cursor:hand;cursor:pointer;width:150px" />
    <asp:ImageButton ID="Region" ImageUrl="../../Image/Region.gif" runat="server"  style="cursor:hand;cursor:pointer;width:150px"  />
    <asp:ImageButton ID="Territory" ImageUrl="../../Image/Territory.gif" runat="server"  style="cursor:hand;cursor:pointer;width:150px" />
    <asp:ImageButton ID="Refresh" ImageUrl="../../Image/Refresh.gif" runat="server"    style="cursor:hand;cursor:pointer;width:150px" />
    <asp:ImageButton ID="Exit" ImageUrl="../../Image/Exit.gif" runat="server"   style="cursor:hand;cursor:pointer;width:150px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </td>
    </tr>
    <tr style="background-color:#EAE7E8;">
    <td style="text-align: left;" colspan="4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlBranch" runat="server" class="NormalText" 
    AutoPostBack="true" > </asp:DropDownList>
    <asp:DropDownList ID="ddlView" runat="server"  class="NormalText" 
    AutoPostBack="true"  > </asp:DropDownList>
    <asp:DropDownList ID="ddlArea" runat="server"  class="NormalText" 
    AutoPostBack="true" > </asp:DropDownList>
    <asp:DropDownList ID="ddlRegion" runat="server"  class="NormalText" 
    AutoPostBack="true" > </asp:DropDownList>
    <asp:DropDownList ID="ddlTerritory" runat="server" class="NormalText"
    AutoPostBack="true" > </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td></tr>
    <tr><td colspan="4">
        <asp:Panel ID="Panel1" runat="server">
            <table align="center" class="style1">
                <tr>
                    <td style="width:4%">
                        &nbsp;</td>
                    <td style="width:6%">
                        &nbsp;</td>
                    <td style="width:6%">
                        &nbsp;</td>
                    <td style="width:6%">
                        &nbsp;</td>
                    <td style="width:6%">
                        &nbsp;</td>
                    <td style="width:6%">
                        &nbsp;</td>
                    <td style="width:64%">
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
        </td></tr>
    <tr><td colspan="4"><asp:Panel ID="pnDisplay"  style="text-align:center;"  runat="server" Width="100%">
            </asp:Panel></td></tr>
    </table>
     
    </div>
        <asp:HiddenField ID="hdnPostID" runat="server" />
        <asp:HiddenField ID="hdnRefresh" runat="server" />
       <br style="background-color:white"/>
        <div style="text-align:center;margin:0px auto; width:95%; background-color:white; font-family:Cambria">
        <table style="width:100%;">
        <tr>
        <td style="width:20%;"></td>
        <td style="width:80%;">
         
        </td>
        </tr>
        </table>           
        </div>
    </div>
    </form>
</body>
</html>
</asp:Content>