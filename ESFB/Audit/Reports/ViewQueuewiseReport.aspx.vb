﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewQueuewiseReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditID As Integer
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim statusID As Integer = CInt(Request.QueryString.Get("ID"))
            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable



            RH.Heading(Session("FirmName"), tb, "QUEUE WISE REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            DTAudit = DB.ExecuteDataSet("SELECT UPPER(description) FROM AUDIT_OBSERVATION_status_master where status_id=" + statusID.ToString()).Tables(0)

            Dim Typename As String
            Dim TR0 As New TableRow

            Dim TRItemHead2 As New TableRow
            TRItemHead2.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead2_00 As New TableCell

            RH.AddColumn(TRItemHead2, TRItemHead2_00, 100, 100, "l", "<b><i>Type  :-     " & DTAudit.Rows(0)(0).ToString() & " </i></b>")

            tb.Controls.Add(TRItemHead2)


            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim TRItemHead As New TableRow
            TRItemHead.ForeColor = Drawing.Color.DarkBlue
            Dim TRItemHead_00 As New TableCell
            RH.BlankRow(tb, 5)


            tb.Controls.Add(TRItemHead)
            RH.BlankRow(tb, 5)
            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"



            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver


            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead, TRHead_00, 15, 15, "l", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 70, 70, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 15, 15, "l", "Count")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            DT = DB.ExecuteDataSet("select f.Branch_Name,f.Branch_ID,COUNT(a.status_ID) as count,b.status_id, b.description  as RPT_DESCR from AUDIT_OBSERVATION_DTL a,AUDIT_OBSERVATION_status_master b " & _
                    " ,AUDIT_OBSERVATION c,AUDIT_DTL d,AUDIT_MASTER e,BRANCH_MASTER f where a.STATUS_ID=b.status_id and a.OBSERVATION_ID=c.OBSERVATION_ID and c.AUDIT_ID=d.AUDIT_ID " & _
                   " and d.GROUP_ID=e.GROUP_ID and e.BRANCH_ID=f.Branch_ID  and a.STATUS_ID>0  and e.STATUS_ID=2 and b.STATUS_ID=" & statusID & " " & _
                   " group by b.status_id,b.description,f.Branch_Name,f.Branch_ID order by 2 desc").Tables(0)
            For Each DR In DT.Rows

                j += 1

                I = 1


                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"


                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver


                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 15, 15, "l", j.ToString())
                RH.AddColumn(TR3, TR3_01, 70, 70, "l", "<a href='ViewQueuewiseDetailedReport.aspx?StatusID=" + CInt(DR(3)).ToString() + " &BranchID=" + GF.Encrypt(DR(1)) + "'>" + DR(0) + "</a>")
                RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(2))

                tb.Controls.Add(TR3)
                I = I + 1
            Next
            RH.BlankRow(tb, 5)

            Dim TR_End As New TableRow
            Dim TR_End_00 As New TableCell
            TR_End.ForeColor = Drawing.Color.Silver
            RH.AddColumn(TR_End, TR_End_00, 100, 100, "c", "<b>*  *  *  End of Document  *  *  *</b>")
            tb.Controls.Add(TR_End)
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
