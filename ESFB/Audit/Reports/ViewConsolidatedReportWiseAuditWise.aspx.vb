﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewConsolidatedReportWiseAuditWise
    Inherits System.Web.UI.Page

    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim CloseFlag As Integer
    Dim PostID As Integer
    Dim AreaID As Integer
    Dim BranchID As Integer
    Dim RegionID As Integer
    Dim AD As New Audit
    Dim StatusID As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim RType As Integer
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))
            CloseFlag = CInt(Request.QueryString.Get("CloseFlag"))

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            StatusID = CInt(Request.QueryString.Get("StatusID"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            RType = CInt(Request.QueryString.Get("RTypeID"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
            End If
            Dim HDStr As String = ""

            If StatusID = 1 Then
                HDStr += " With Very Serious Observations"
            ElseIf StatusID = 2 Then
                HDStr += " With Serious Observations"
            ElseIf StatusID = 3 Then
                HDStr += " With Both Very serious & Serious Observations"
            ElseIf StatusID = 4 Then
                HDStr += " With General Observations only"
            End If

            If RegionID > 0 Then
                HDStr += GN.GetRegion_Name(RegionID) + " Region"
            ElseIf AreaID > 0 Then
                HDStr += GN.GetArea_Name(AreaID) + " Area"
            ElseIf BranchID > 0 Then
                HDStr += GN.GetBranch_Name(BranchID) + " Branch"
            End If

            Dim DT As New DataTable
            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable
            Dim SubHD As String = ""
            RH.Heading(Session("FirmName"), tb, "Audit Report " + HDStr, 100)
            If (CloseFlag = 1) Then
                SubHD = "Status - Pending Operations (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 5 Then
                SubHD = "Status - Pending Auditors (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 2 Then
                SubHD = "Status - Pending  (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            ElseIf CloseFlag = 0 Then
                SubHD = "Status - Closed (" + Format(FromDt, "dd/MMM/yyyy") + "-" + Format(ToDt, "dd/MMM/yyyy") + ")"
            End If
            RH.SubHeading(tb, 100, "l", SubHD)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")



            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            'TRHead_06.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            'TRHead_06.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            'TRHead_06.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead, TRHead_00, 5, 5, "c", "Sl No.")
            RH.AddColumn(TRHead, TRHead_01, 35, 35, "l", "Branch")
            RH.AddColumn(TRHead, TRHead_02, 10, 10, "l", "Audit")
            RH.AddColumn(TRHead, TRHead_03, 20, 20, "c", "Audit Period From")
            RH.AddColumn(TRHead, TRHead_04, 20, 20, "c", "Audit Period To")
            RH.AddColumn(TRHead, TRHead_05, 10, 10, "c", "")
            tb.Controls.Add(TRHead)
            'RH.BlankRow(tb, 3)
            Dim TotFraud As Integer = 0
            Dim TotSusLeakage As Integer = 0
            Dim TotGeneral As Integer = 0
            Dim TotTotal As Double = 0
            Dim TotalBoth As Integer = 0
            DT = AD.AuditWiseAuditReportReportWise(Month, year, CloseFlag, PostID, CInt(Session("UserID")), RegionID, AreaID, BranchID, StatusID, FromDt, ToDt, RType)
            For Each DR In DT.Rows
                j += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                'TR3_06.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                'TR3_06.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                'TR3_06.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 5, 5, "c", j.ToString())
                RH.AddColumn(TR3, TR3_01, 35, 35, "l", DR(1))
                RH.AddColumn(TR3, TR3_02, 10, 10, "l", CDate(DR(3)).ToString("MMM yyyy"))
                RH.AddColumn(TR3, TR3_03, 20, 20, "c", CDate(DR(2)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_04, 20, 20, "c", CDate(DR(3)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_05, 10, 10, "c", "<a href='viewComplianceStatusReport.aspx?AuditID=" + GN.Encrypt(DR(0).ToString()) + " &ReportID=" + GN.Encrypt("1") + "' target=blank>View Report</a>")
                tb.Controls.Add(TR3)
            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub
    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
