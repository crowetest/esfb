﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ConsolidatedAuditPendingReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim UserID As Integer
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim PostID As Integer
    Dim T As Integer
    Dim WebTools As New WebApp.Tools
    Dim DTTerritory As New DataTable
    Dim DTRegion As New DataTable
    Dim DTArea As New DataTable
    Dim DTBranch As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 269) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            UserID = CInt(Session("UserID"))
            Me.hdnPostID.Value = Session("Post_ID")
            PostID = CInt(Session("Post_ID"))
            Dim Type As Integer = CInt(Me.cmbStatus.SelectedValue)
            If Not IsPostBack Then
                DTTerritory = GN.GetTerritoryBasedOnPost(PostID, UserID)
                DTRegion = GN.GetRegionBasedOnPost(PostID, UserID)
                DTArea = GN.GetAreaBasedOnPost(PostID, UserID)
                DTBranch = GN.GetBranchBasedOnPost(PostID, UserID)
                ViewState("ID") = 1
                Me.hdnRefresh.Value = 0
                GN.ComboFill(ddlTerritory, DTTerritory, 0, 1)
                GN.ComboFill(ddlRegion, DTRegion, 0, 1)
                GN.ComboFill(ddlArea, DTArea, 0, 1)
                GN.ComboFill(ddlBranch, DTBranch, 0, 1)
                T = 1
                Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@ReportType", Type), New System.Data.SqlClient.SqlParameter("@PostID", PostID), New System.Data.SqlClient.SqlParameter("@UserID", UserID)}
                DT = DB.ExecuteDataSet("SP_AUDIT_CLOSURE_PENDING", Parameters).Tables(0)
                ViewState("myDataSet") = DT
                Filter()
            Else
                DT = DirectCast(ViewState("myDataSet"), DataTable)
            End If
            ddlTerritory.Width = 150
            ddlRegion.Width = 150
            ddlArea.Width = 150
            ddlBranch.Width = 150
            'Me.Branch.Attributes.Add("OnClientClick", "return GenerateOnclick()")
            'Me.Area.Attributes.Add("OnClientClick", "return GenerateOnclick()")
            'Me.Region.Attributes.Add("OnClientClick", "return GenerateOnclick()")
            'Me.Territory.Attributes.Add("OnClientClick", "return GenerateOnclick()")
            'Me.Refresh.Attributes.Add("onclick", "return btnRefresh_onclick()")
            Me.Exit.Attributes.Add("onclick", "return btnExit_onclick()")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

    Public Sub GenerateReport(ByVal ID As Integer)
        Try
            pnDisplay.Controls.Clear()
            Dim Type As Integer = CInt(Me.cmbStatus.SelectedValue)
            Me.hdnPostID.Value = Session("Post_ID")
            PostID = Me.hdnPostID.Value
            Dim Selection As String = ""
            Dim PriorityFilter As String = ""
            Dim StatusFilter As String = ""
            Dim DateFilter As String = ""
            Dim GroupString As String = ""
            Dim CategoryFilter As String = ""
            Dim Territory As Integer = 0
            Dim Area As Integer = 0
            Dim Region As Integer = 0
            Dim Branch As Integer = 0

            Dim TotalPoints As Integer = 0
            Dim TotalPendingOpn As Integer = 0
            Dim TotalPendingAudit As Integer = 0
            Dim TotalConf As Integer = 0

            Dim TerritoryTotalPoints As Integer = 0
            Dim TerritoryPendingOpn As Integer = 0
            Dim TerritoryPendingAudit As Integer = 0
            Dim TerritoryConf As Integer = 0
            Dim RegionTotalPoints As Integer = 0
            Dim RegionPendingOpn As Integer = 0
            Dim RegionPendingAudit As Integer = 0
            Dim RegionConf As Integer = 0
            Dim AreaTotalPoints As Integer = 0
            Dim AreaPendingOpn As Integer = 0
            Dim AreaPendingAudit As Integer = 0
            Dim AreaConf As Integer = 0
            Dim BranchTotalPoints As Integer = 0
            Dim BranchPendingOpn As Integer = 0
            Dim BranchPendingAudit As Integer = 0
            Dim BranchConf As Integer = 0

            Dim TotCnt As Integer = DT.Columns.Count
            Dim Count As Integer = TotCnt - 16
            Dim SubPerc As Integer = (Count / TotCnt) * 100
            Dim BalPerc As Integer = 100 - SubPerc
            Dim tb As New Table
            RH.Heading("", tb, " AUDITWISE PENDING SUMMARY", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            If DT.Rows.Count > 0 Then

                Dim TRHead As New TableRow
                TRHead.BackColor = Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10 As New TableCell

                RH.InsertColumn(TRHead, TRHead_00, BalPerc * 6 / 100, 2, "#")
                RH.InsertColumn(TRHead, TRHead_01, BalPerc * 10 / 100, 0, "Territory")
                RH.InsertColumn(TRHead, TRHead_02, BalPerc * 10 / 100, 0, "Region")
                RH.InsertColumn(TRHead, TRHead_03, BalPerc * 10 / 100, 0, "Area")
                RH.InsertColumn(TRHead, TRHead_04, BalPerc * 12 / 100, 0, "Branch")
                RH.InsertColumn(TRHead, TRHead_05, BalPerc * 10 / 100, 0, "ReportDate")
                RH.InsertColumn(TRHead, TRHead_06, BalPerc * 10 / 100, 0, "Available Days For Closure")
                RH.InsertColumn(TRHead, TRHead_07, BalPerc * 10 / 100, 0, "Total Queries")
                RH.InsertColumn(TRHead, TRHead_08, BalPerc * 6 / 100, 0, "Pending Queries")
                Dim val As Integer = 10
                Dim DynamicPerc As Integer = SubPerc / Count
                Dim Balance As Integer = SubPerc - (DynamicPerc * Count)
                Dim j As Integer = 0
                Dim CheckCnt As Integer = 15
                Dim Slno As Integer = 0
                Dim TotSIno As Integer = 0
                For j = 1 To Count
                    Dim tCell As New TableCell()
                    val = val + 1
                    tCell.Text = "TRHead_" & val

                    CheckCnt = CheckCnt + 1
                    If j = Count Then
                        RH.InsertColumn(TRHead, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, DT.Columns(CheckCnt).ColumnName)

                    Else

                        RH.InsertColumn(TRHead, tCell, DynamicPerc, 2, DT.Columns(CheckCnt).ColumnName)
                    End If


                Next

                RH.InsertColumn(TRHead, TRHead_09, BalPerc * 10 / 100, 0, "Confirmation")
                RH.InsertColumn(TRHead, TRHead_10, BalPerc * 6 / 100, 0, "Auditor")
                tb.Controls.Add(TRHead)
                Dim PendingPoints As Integer = 0
                '---------------------------------5
                Dim TotalVal(Count) As String
                Dim TerritoryVal(Count) As String
                Dim RegionVal(Count) As String
                Dim AreaVal(Count) As String
                Dim BranchVal(Count) As String
                For Each DR In DT.Rows
                    Dim Tr3 As New TableRow


                    '------------------------------------------------------------------------------------------Total
                    If ID = 1 Then
                        If DR(7) <> Branch And Branch <> 0 Then
                            Slno = 0
                            Dim Tr4 As New TableRow
                            Tr4.BackColor = Drawing.Color.Silver

                            Dim Tr4_00, Tr4_01, Tr4_02, Tr4_03, Tr4_04, Tr4_05, Tr4_06, Tr4_07, Tr4_08, Tr4_09, Tr4_10 As New TableCell
                            RH.InsertColumn(Tr4, Tr4_00, BalPerc * 6 / 100, 2, "")
                            RH.InsertColumn(Tr4, Tr4_01, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr4, Tr4_02, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr4, Tr4_03, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr4, Tr4_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Branch Total</b>")
                            TotalPoints = TotalPoints + BranchTotalPoints
                            TotalPendingOpn = TotalPendingOpn + BranchPendingOpn
                            TotalPendingAudit = TotalPendingAudit + BranchPendingAudit
                            TotalConf = TotalConf + BranchConf

                            RH.InsertColumn(Tr4, Tr4_05, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr4, Tr4_06, BalPerc * 10 / 100, 2, "")
                            RH.InsertColumn(Tr4, Tr4_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchTotalPoints.ToString + "</b>")
                            RH.InsertColumn(Tr4, Tr4_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchPendingOpn.ToString + "</b>")
                            val = 10
                            CheckCnt = 15
                            For j = 1 To Count
                                Dim tCell As New TableCell()
                                val = val + 1
                                tCell.Text = "Tr4_" & val
                                CheckCnt = CheckCnt + 1
                                If j = Count Then
                                    RH.InsertColumn(Tr4, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + BranchVal(j).ToString + "</b>")

                                Else

                                    RH.InsertColumn(Tr4, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + BranchVal(j).ToString + "</b>")
                                End If
                                Dim val1 As Integer = TotalVal(j)
                                Dim val2 As Integer = BranchVal(j)
                                'TotalVal(j) = val1 + val2

                                BranchVal(j) = 0
                            Next
                            If BranchConf = 0 Then
                                RH.InsertColumn(Tr4, Tr4_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchConf.ToString + "</b>")
                            Else
                                RH.InsertColumn(Tr4, Tr4_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'> <a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt(Branch.ToString()) + "&GroupID=" + GN.Encrypt("-1") + "&AreaID=" + GN.Encrypt("-1") + "&RegionID=" + GN.Encrypt("-1") + "&ZoneID=" + GN.Encrypt("-1") + "'  target='_blank'>" + BranchConf.ToString() + "</a></b>")
                            End If

                            RH.InsertColumn(Tr4, Tr4_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchPendingAudit.ToString + "</b>")
                            tb.Controls.Add(Tr4)
                            BranchTotalPoints = 0
                            BranchPendingOpn = 0
                            BranchPendingAudit = 0
                            BranchConf = 0
                        End If
                    ElseIf ID = 2 Then
                        If DR(5) <> Area And Area <> 0 Then
                            Slno = 0
                            Dim Tr5 As New TableRow
                            Tr5.BackColor = Drawing.Color.LightSteelBlue
                            Dim Tr5_00, Tr5_01, Tr5_02, Tr5_03, Tr5_04, Tr5_05, Tr5_06, Tr5_07, Tr5_08, Tr5_09, Tr5_10 As New TableCell
                            RH.InsertColumn(Tr5, Tr5_00, BalPerc * 6 / 100, 2, "")
                            RH.InsertColumn(Tr5, Tr5_01, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr5, Tr5_02, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr5, Tr5_03, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr5, Tr5_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Area Total</b>")

                            TotalPoints = TotalPoints + AreaTotalPoints
                            TotalPendingOpn = TotalPendingOpn + AreaPendingOpn
                            TotalPendingAudit = TotalPendingAudit + AreaPendingAudit
                            TotalConf = TotalConf + AreaConf

                            RH.InsertColumn(Tr5, Tr5_05, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr5, Tr5_06, BalPerc * 10 / 100, 2, "")
                            RH.InsertColumn(Tr5, Tr5_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaTotalPoints.ToString + "</b>")
                            RH.InsertColumn(Tr5, Tr5_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaPendingOpn.ToString + "</b>")
                            val = 10
                            CheckCnt = 15
                            For j = 1 To Count
                                Dim tCell As New TableCell()
                                val = val + 1
                                tCell.Text = "Tr5_" & val
                                CheckCnt = CheckCnt + 1
                                If j = Count Then
                                    RH.InsertColumn(Tr5, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + AreaVal(j).ToString + "</b>")

                                Else

                                    RH.InsertColumn(Tr5, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + AreaVal(j).ToString + "</b>")
                                End If
                                Dim val1 As Integer = TotalVal(j)
                                Dim val2 As Integer = AreaVal(j)
                                'TotalVal(j) = val1 + val2
                                AreaVal(j) = 0
                            Next
                            If AreaConf = 0 Then
                                RH.InsertColumn(Tr5, Tr5_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaConf.ToString + "</b>")
                            Else
                                RH.InsertColumn(Tr5, Tr5_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + "&GroupID=" + GN.Encrypt("-1") + "&AreaID=" + GN.Encrypt(Area.ToString()) + "&RegionID=" + GN.Encrypt("-1") + "&ZoneID=" + GN.Encrypt("-1") + "'  target='_blank'>" + AreaConf.ToString + "</a></b>")
                            End If


                            RH.InsertColumn(Tr5, Tr5_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaPendingAudit.ToString + "</b>")
                            tb.Controls.Add(Tr5)
                            AreaTotalPoints = 0
                            AreaPendingOpn = 0
                            AreaPendingAudit = 0
                            AreaConf = 0
                        End If
                    ElseIf ID = 3 Then
                        If DR(3) <> Region And Region <> 0 Then
                            Slno = 0
                            Dim Tr6 As New TableRow
                            Tr6.BackColor = Drawing.Color.Turquoise
                            Dim Tr6_00, Tr6_01, Tr6_02, Tr6_03, Tr6_04, Tr6_05, Tr6_06, Tr6_07, Tr6_08, Tr6_09, Tr6_10 As New TableCell
                            RH.InsertColumn(Tr6, Tr6_00, BalPerc * 6 / 100, 2, "")
                            RH.InsertColumn(Tr6, Tr6_01, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr6, Tr6_02, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr6, Tr6_03, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr6, Tr6_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Region Total</b>")

                            TotalPoints = TotalPoints + RegionTotalPoints
                            TotalPendingOpn = TotalPendingOpn + RegionPendingOpn
                            TotalPendingAudit = TotalPendingAudit + RegionPendingAudit
                            TotalConf = TotalConf + RegionConf

                            RH.InsertColumn(Tr6, Tr6_05, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr6, Tr6_06, BalPerc * 10 / 100, 2, "")
                            RH.InsertColumn(Tr6, Tr6_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionTotalPoints.ToString + "</b>")
                            RH.InsertColumn(Tr6, Tr6_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionPendingOpn.ToString + "</b>")
                            val = 10
                            CheckCnt = 15
                            For j = 1 To Count
                                Dim tCell As New TableCell()
                                val = val + 1
                                tCell.Text = "Tr6_" & val
                                CheckCnt = CheckCnt + 1
                                If j = Count Then
                                    RH.InsertColumn(Tr6, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + RegionVal(j).ToString + "</b>")

                                Else

                                    RH.InsertColumn(Tr6, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + RegionVal(j).ToString + "</b>")
                                End If
                                Dim val1 As Integer = TotalVal(j)
                                Dim val2 As Integer = RegionVal(j)
                                'TotalVal(j) = val1 + val2
                                RegionVal(j) = 0
                            Next
                            If RegionConf = 0 Then
                                RH.InsertColumn(Tr6, Tr6_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionConf.ToString + "</b>")
                            Else
                                RH.InsertColumn(Tr6, Tr6_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + "&GroupID=" + GN.Encrypt("-1") + "&AreaID=" + GN.Encrypt("-1") + "&RegionID=" + GN.Encrypt(Region.ToString()) + "&ZoneID=" + GN.Encrypt("-1") + "'  target='_blank'>" + RegionConf.ToString + "</a></b>")
                            End If
                            RH.InsertColumn(Tr6, Tr6_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionPendingAudit.ToString + "</b>")
                            tb.Controls.Add(Tr6)
                            RegionTotalPoints = 0
                            RegionPendingOpn = 0
                            RegionPendingAudit = 0
                            RegionConf = 0
                        End If
                    ElseIf ID = 4 Then
                        If DR(2) <> Territory And Territory <> 0 Then
                            Slno = 0
                            Dim Tr7 As New TableRow
                            Tr7.BackColor = Drawing.Color.LightYellow
                            Dim Tr7_00, Tr7_01, Tr7_02, Tr7_03, Tr7_04, Tr7_05, Tr7_06, Tr7_07, Tr7_08, Tr7_09, Tr7_10 As New TableCell
                            RH.InsertColumn(Tr7, Tr7_00, BalPerc * 6 / 100, 2, "")
                            RH.InsertColumn(Tr7, Tr7_01, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr7, Tr7_02, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr7, Tr7_03, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr7, Tr7_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Territory Total</b>")

                            TotalPoints = TotalPoints + TerritoryTotalPoints
                            TotalPendingOpn = TotalPendingOpn + TerritoryPendingOpn
                            TotalPendingAudit = TotalPendingAudit + TerritoryPendingAudit
                            TotalConf = TotalConf + TerritoryConf

                            RH.InsertColumn(Tr7, Tr7_05, BalPerc * 10 / 100, 0, "")
                            RH.InsertColumn(Tr7, Tr7_06, BalPerc * 10 / 100, 2, "")
                            RH.InsertColumn(Tr7, Tr7_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryTotalPoints.ToString + "</b>")
                            RH.InsertColumn(Tr7, Tr7_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryPendingOpn.ToString + "</b>")
                            val = 10
                            CheckCnt = 15
                            For j = 1 To Count
                                Dim tCell As New TableCell()
                                val = val + 1
                                tCell.Text = "Tr7_" & val
                                CheckCnt = CheckCnt + 1
                                If j = Count Then
                                    RH.InsertColumn(Tr7, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryVal(j).ToString + "</b>")

                                Else

                                    RH.InsertColumn(Tr7, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryVal(j).ToString + "</b>")
                                End If
                                Dim val1 As Integer = TotalVal(j)
                                Dim val2 As Integer = TerritoryVal(j)
                                'TotalVal(j) = val1 + val2
                                TerritoryVal(j) = 0
                            Next
                            If TerritoryConf = 0 Then
                                RH.InsertColumn(Tr7, Tr7_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryConf.ToString + "</b>")
                            Else
                                RH.InsertColumn(Tr7, Tr7_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + "&GroupID=" + GN.Encrypt("-1") + " &AreaID=" + GN.Encrypt("-1") + "&RegionID=" + GN.Encrypt("-1") + "&ZoneID=" + GN.Encrypt(Territory.ToString()) + "'  target='_blank'>" + TerritoryConf.ToString + "</a></b>")
                            End If

                            RH.InsertColumn(Tr7, Tr7_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryPendingAudit.ToString + "</b>")
                            tb.Controls.Add(Tr7)
                            TerritoryTotalPoints = 0
                            TerritoryPendingOpn = 0
                            TerritoryPendingAudit = 0
                            TerritoryConf = 0

                        End If
                    End If
                    '------------------------------------------------------------------------------------------Total

                    TerritoryTotalPoints = TerritoryTotalPoints + IIf(DR(10).ToString = "", 0, DR(10))
                    TerritoryPendingOpn = TerritoryPendingOpn + IIf(DR(13).ToString = "", 0, DR(13))
                    TerritoryPendingAudit = TerritoryPendingAudit + IIf(DR(14).ToString = "", 0, DR(14))
                    TerritoryConf = TerritoryConf + IIf(DR(11).ToString = "", 0, DR(11))

                    RegionTotalPoints = RegionTotalPoints + IIf(DR(10).ToString = "", 0, DR(10))
                    RegionPendingOpn = RegionPendingOpn + IIf(DR(13).ToString = "", 0, DR(13))
                    RegionPendingAudit = RegionPendingAudit + IIf(DR(14).ToString = "", 0, DR(14))
                    RegionConf = RegionConf + IIf(DR(11).ToString = "", 0, DR(11))

                    AreaTotalPoints = AreaTotalPoints + IIf(DR(10).ToString = "", 0, DR(10))
                    AreaPendingOpn = AreaPendingOpn + IIf(DR(13).ToString = "", 0, DR(13))
                    AreaPendingAudit = AreaPendingAudit + IIf(DR(14).ToString = "", 0, DR(14))
                    AreaConf = AreaConf + IIf(DR(11).ToString = "", 0, DR(11))

                    BranchTotalPoints = BranchTotalPoints + IIf(DR(10).ToString = "", 0, DR(10))
                    BranchPendingOpn = BranchPendingOpn + IIf(DR(13).ToString = "", 0, DR(13))
                    BranchPendingAudit = BranchPendingAudit + IIf(DR(14).ToString = "", 0, DR(14))
                    BranchConf = BranchConf + IIf(DR(11).ToString = "", 0, DR(11))

                    Slno = Slno + 1
                    TotSIno += 1
                    Dim Tr3_00, Tr3_01, Tr3_02, Tr3_03, Tr3_04, Tr3_05, Tr3_06, Tr3_07, Tr3_08, Tr3_09, Tr3_10 As New TableCell
                    RH.InsertColumn(Tr3, Tr3_00, BalPerc * 6 / 100, 2, Slno)
                    RH.InsertColumn(Tr3, Tr3_01, BalPerc * 10 / 100, 0, DR(1))
                    RH.InsertColumn(Tr3, Tr3_02, BalPerc * 10 / 100, 0, DR(4))
                    RH.InsertColumn(Tr3, Tr3_03, BalPerc * 10 / 100, 0, DR(6))
                    RH.InsertColumn(Tr3, Tr3_04, BalPerc * 12 / 100, 0, DR(8))
                    RH.InsertColumn(Tr3, Tr3_05, BalPerc * 10 / 100, 0, DR(9))
                    Dim TotPending = 0
                    TotPending = DR(13) + DR(14) + DR(11)
                    If TotPending > 0 Then
                        RH.InsertColumn(Tr3, Tr3_06, BalPerc * 10 / 100, 2, IIf(IIf(DR(12).ToString = "", 0, DR(12)) < 0, "<b style='color:red;'>" + DR(12).ToString + "</b>", DR(12).ToString))
                    Else
                        RH.InsertColumn(Tr3, Tr3_06, BalPerc * 10 / 100, 2, "<b style='color:green;'>Closed</b>")
                    End If
                    RH.InsertColumn(Tr3, Tr3_07, BalPerc * 10 / 100, 2, DR(10))
                    RH.InsertColumn(Tr3, Tr3_08, BalPerc * 6 / 100, 2, IIf(DR(14).ToString = "", "", DR(13)))
                    'PendingPoints =
                    val = 10

                    j = 0
                    CheckCnt = 15

                    For j = 1 To Count
                        Dim tCell As New TableCell()
                        val = val + 1
                        tCell.Text = "Tr3_" & val
                        CheckCnt = CheckCnt + 1
                        If j = Count Then
                            RH.InsertColumn(Tr3, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt)))

                        Else

                            RH.InsertColumn(Tr3, tCell, DynamicPerc, 2, IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt)))
                        End If
                        TerritoryVal(j) = TerritoryVal(j) + IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt))
                        RegionVal(j) = RegionVal(j) + IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt))
                        AreaVal(j) = AreaVal(j) + IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt))
                        BranchVal(j) = BranchVal(j) + IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt))
                        TotalVal(j) = TotalVal(j) + IIf(IsDBNull(DR(CheckCnt)), 0, DR(CheckCnt))
                    Next

                    If IsDBNull(DR(11)) Or DR(11) = 0 Then
                        RH.InsertColumn(Tr3, Tr3_09, BalPerc * 10 / 100, 2, "0")
                    Else
                        RH.InsertColumn(Tr3, Tr3_09, BalPerc * 10 / 100, 2, "<a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt(DR(7).ToString()) + " &GroupID=" + GN.Encrypt(DR(0).ToString()) + "'  target='_blank'>" + DR(11).ToString() + "</a>")
                    End If
                    RH.InsertColumn(Tr3, Tr3_10, BalPerc * 6 / 100, 2, IIf(DR(13).ToString = "", "", DR(14)))
                    tb.Controls.Add(Tr3)

                    Territory = DR(2)
                    Region = DR(3)
                    Area = DR(5)
                    Branch = DR(7)
                Next
                '---------------------------Last Total
                If ID = 1 Then
                    If Branch <> 0 Then

                        Dim Tr4 As New TableRow
                        Tr4.BackColor = Drawing.Color.Silver

                        Dim Tr4_00, Tr4_01, Tr4_02, Tr4_03, Tr4_04, Tr4_05, Tr4_06, Tr4_07, Tr4_08, Tr4_09, Tr4_10 As New TableCell
                        RH.InsertColumn(Tr4, Tr4_00, BalPerc * 6 / 100, 2, "")
                        RH.InsertColumn(Tr4, Tr4_01, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr4, Tr4_02, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr4, Tr4_03, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr4, Tr4_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Branch Total</b>")

                        TotalPoints = TotalPoints + BranchTotalPoints
                        TotalPendingOpn = TotalPendingOpn + BranchPendingOpn
                        TotalPendingAudit = TotalPendingAudit + BranchPendingAudit
                        TotalConf = TotalConf + BranchConf

                        RH.InsertColumn(Tr4, Tr4_05, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr4, Tr4_06, BalPerc * 10 / 100, 2, "")
                        RH.InsertColumn(Tr4, Tr4_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchTotalPoints.ToString + "</b>")
                        RH.InsertColumn(Tr4, Tr4_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchPendingOpn.ToString + "</b>")
                        val = 10
                        CheckCnt = 15
                        For j = 1 To Count
                            Dim tCell As New TableCell()
                            val = val + 1
                            tCell.Text = "Tr4_" & val
                            CheckCnt = CheckCnt + 1
                            If j = Count Then
                                RH.InsertColumn(Tr4, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + BranchVal(j).ToString + "</b>")

                            Else

                                RH.InsertColumn(Tr4, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + BranchVal(j).ToString + "</b>")
                            End If
                            Dim val1 As Integer = TotalVal(j)
                            Dim val2 As Integer = BranchVal(j)
                            'TotalVal(j) = val1 + val2
                            BranchVal(j) = 0
                        Next
                        If BranchConf = 0 Then
                            RH.InsertColumn(Tr4, Tr4_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchConf.ToString + "</b>")
                        Else
                            RH.InsertColumn(Tr4, Tr4_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'> <a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt(Branch.ToString()) + "&GroupID=" + GN.Encrypt("-1") + "&AreaID=" + GN.Encrypt("-1") + "&RegionID=" + GN.Encrypt("-1") + "&ZoneID=" + GN.Encrypt("-1") + "'  target='_blank'>" + BranchConf.ToString() + "</a></b>")
                        End If
                        RH.InsertColumn(Tr4, Tr4_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + BranchPendingAudit.ToString + "</b>")
                        tb.Controls.Add(Tr4)
                        BranchTotalPoints = 0
                        BranchPendingOpn = 0
                        BranchPendingAudit = 0
                        BranchConf = 0
                    End If
                ElseIf ID = 2 Then
                    If Area <> 0 Then
                        Dim Tr5 As New TableRow
                        Tr5.BackColor = Drawing.Color.LightSteelBlue
                        Dim Tr5_00, Tr5_01, Tr5_02, Tr5_03, Tr5_04, Tr5_05, Tr5_06, Tr5_07, Tr5_08, Tr5_09, Tr5_10 As New TableCell
                        RH.InsertColumn(Tr5, Tr5_00, BalPerc * 6 / 100, 2, "")
                        RH.InsertColumn(Tr5, Tr5_01, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr5, Tr5_02, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr5, Tr5_03, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr5, Tr5_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Area Total</b>")

                        TotalPoints = TotalPoints + AreaTotalPoints
                        TotalPendingOpn = TotalPendingOpn + AreaPendingOpn
                        TotalPendingAudit = TotalPendingAudit + AreaPendingAudit
                        TotalConf = TotalConf + AreaConf

                        RH.InsertColumn(Tr5, Tr5_05, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr5, Tr5_06, BalPerc * 10 / 100, 2, "")
                        RH.InsertColumn(Tr5, Tr5_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaTotalPoints.ToString + "</b>")
                        RH.InsertColumn(Tr5, Tr5_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaPendingOpn.ToString + "</b>")
                        val = 10
                        CheckCnt = 15
                        For j = 1 To Count
                            Dim tCell As New TableCell()
                            val = val + 1
                            tCell.Text = "Tr5_" & val
                            CheckCnt = CheckCnt + 1
                            If j = Count Then
                                RH.InsertColumn(Tr5, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + AreaVal(j).ToString + "</b>")

                            Else

                                RH.InsertColumn(Tr5, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + AreaVal(j).ToString + "</b>")
                            End If
                            Dim val1 As Integer = TotalVal(j)
                            Dim val2 As Integer = AreaVal(j)
                            'TotalVal(j) = val1 + val2
                            AreaVal(j) = 0
                        Next

                        If AreaConf = 0 Then
                            RH.InsertColumn(Tr5, Tr5_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaConf.ToString + "</b>")
                        Else
                            RH.InsertColumn(Tr5, Tr5_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + " &GroupID=" + GN.Encrypt("-1") + "&AreaID=" + GN.Encrypt(Area.ToString()) + "&RegionID=" + GN.Encrypt("-1") + "&ZoneID=" + GN.Encrypt("-1") + "' target='_blank'>" + +AreaConf.ToString + "</a></b>")
                        End If
                        RH.InsertColumn(Tr5, Tr5_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + AreaPendingAudit.ToString + "</b>")
                        tb.Controls.Add(Tr5)
                        AreaTotalPoints = 0
                        AreaPendingOpn = 0
                        AreaPendingAudit = 0
                        AreaConf = 0
                    End If
                ElseIf ID = 3 Then
                    If Region <> 0 Then

                        Dim Tr6 As New TableRow
                        Tr6.BackColor = Drawing.Color.Turquoise
                        Dim Tr6_00, Tr6_01, Tr6_02, Tr6_03, Tr6_04, Tr6_05, Tr6_06, Tr6_07, Tr6_08, Tr6_09, Tr6_10 As New TableCell
                        RH.InsertColumn(Tr6, Tr6_00, BalPerc * 6 / 100, 2, "")
                        RH.InsertColumn(Tr6, Tr6_01, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr6, Tr6_02, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr6, Tr6_03, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr6, Tr6_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Region Total</b>")

                        TotalPoints = TotalPoints + RegionTotalPoints
                        TotalPendingOpn = TotalPendingOpn + RegionPendingOpn
                        TotalPendingAudit = TotalPendingAudit + RegionPendingAudit
                        TotalConf = TotalConf + RegionConf

                        RH.InsertColumn(Tr6, Tr6_05, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr6, Tr6_06, BalPerc * 10 / 100, 2, "")
                        RH.InsertColumn(Tr6, Tr6_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionTotalPoints.ToString + "</b>")
                        RH.InsertColumn(Tr6, Tr6_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionPendingOpn.ToString + "</b>")
                        val = 10
                        CheckCnt = 15
                        For j = 1 To Count
                            Dim tCell As New TableCell()
                            val = val + 1
                            tCell.Text = "Tr6_" & val
                            CheckCnt = CheckCnt + 1
                            If j = Count Then
                                RH.InsertColumn(Tr6, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + RegionVal(j).ToString + "</b>")

                            Else

                                RH.InsertColumn(Tr6, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + RegionVal(j).ToString + "</b>")
                            End If
                            Dim val1 As Integer = TotalVal(j)
                            Dim val2 As Integer = RegionVal(j)
                            'TotalVal(j) = val1 + val2
                            RegionVal(j) = 0
                        Next
                        If RegionConf = 0 Then
                            RH.InsertColumn(Tr6, Tr6_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionConf.ToString + "</b>")
                        Else
                            RH.InsertColumn(Tr6, Tr6_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + " &GroupID=" + GN.Encrypt("-1") + " &AreaID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt(Region.ToString()) + " &ZoneID=" + GN.Encrypt("-1") + "'  target='_blank'>" + RegionConf.ToString + "</a></b>")
                        End If

                        RH.InsertColumn(Tr6, Tr6_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + RegionPendingAudit.ToString + "</b>")
                        tb.Controls.Add(Tr6)
                        RegionTotalPoints = 0
                        RegionPendingOpn = 0
                        RegionPendingAudit = 0
                        RegionConf = 0
                    End If
                ElseIf ID = 4 Then
                    If Territory <> 0 Then

                        Dim Tr7 As New TableRow
                        Tr7.BackColor = Drawing.Color.LightYellow
                        Dim Tr7_00, Tr7_01, Tr7_02, Tr7_03, Tr7_04, Tr7_05, Tr7_06, Tr7_07, Tr7_08, Tr7_09, Tr7_10 As New TableCell
                        RH.InsertColumn(Tr7, Tr7_00, BalPerc * 6 / 100, 2, "")
                        RH.InsertColumn(Tr7, Tr7_01, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr7, Tr7_02, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr7, Tr7_03, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr7, Tr7_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:13pt;'>Territory Total</b>")

                        TotalPoints = TotalPoints + TerritoryTotalPoints
                        TotalPendingOpn = TotalPendingOpn + TerritoryPendingOpn
                        TotalPendingAudit = TotalPendingAudit + TerritoryPendingAudit
                        TotalConf = TotalConf + TerritoryConf

                        RH.InsertColumn(Tr7, Tr7_05, BalPerc * 10 / 100, 0, "")
                        RH.InsertColumn(Tr7, Tr7_06, BalPerc * 10 / 100, 2, "")
                        RH.InsertColumn(Tr7, Tr7_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryTotalPoints.ToString + "</b>")
                        RH.InsertColumn(Tr7, Tr7_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryPendingOpn.ToString + "</b>")
                        val = 10
                        CheckCnt = 15
                        For j = 1 To Count
                            Dim tCell As New TableCell()
                            val = val + 1
                            tCell.Text = "Tr7_" & val
                            CheckCnt = CheckCnt + 1
                            If j = Count Then
                                RH.InsertColumn(Tr7, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryVal(j).ToString + "</b>")

                            Else

                                RH.InsertColumn(Tr7, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryVal(j).ToString + "</b>")
                            End If
                            Dim val1 As Integer = TotalVal(j)
                            Dim val2 As Integer = TerritoryVal(j)
                            'TotalVal(j) = val1 + val2
                            TerritoryVal(j) = 0
                        Next
                        If TerritoryConf = 0 Then
                            RH.InsertColumn(Tr7, Tr7_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryConf.ToString + "</b>")
                        Else
                            RH.InsertColumn(Tr7, Tr7_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + " &GroupID=" + GN.Encrypt("-1") + " &AreaID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt("-1") + " &ZoneID=" + GN.Encrypt(Territory.ToString()) + "'  target='_blank'>" + TerritoryConf.ToString + "</a></b>")
                        End If
                        RH.InsertColumn(Tr7, Tr7_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:12pt;'>" + TerritoryPendingAudit.ToString + "</b>")
                        tb.Controls.Add(Tr7)
                        TerritoryTotalPoints = 0
                        TerritoryPendingOpn = 0
                        TerritoryPendingAudit = 0
                        TerritoryConf = 0

                    End If
                End If
                Dim Tr8 As New TableRow
                Tr8.BackColor = Drawing.Color.Bisque
                Dim Tr8_00, Tr8_01, Tr8_02, Tr8_03, Tr8_04, Tr8_05, Tr8_06, Tr8_07, Tr8_08, Tr8_09, Tr8_10 As New TableCell
                RH.InsertColumn(Tr8, Tr8_00, BalPerc * 6 / 100, 2, TotSIno)
                RH.InsertColumn(Tr8, Tr8_01, BalPerc * 10 / 100, 0, "")
                RH.InsertColumn(Tr8, Tr8_02, BalPerc * 10 / 100, 0, "")
                RH.InsertColumn(Tr8, Tr8_03, BalPerc * 10 / 100, 0, "")
                RH.InsertColumn(Tr8, Tr8_04, BalPerc * 12 / 100, 0, "<b style='color:brown;font-size:14pt;'>Grand Total</b>")



                RH.InsertColumn(Tr8, Tr8_05, BalPerc * 10 / 100, 0, "")
                RH.InsertColumn(Tr8, Tr8_06, BalPerc * 10 / 100, 2, "")
                RH.InsertColumn(Tr8, Tr8_07, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:13pt;'>" + TotalPoints.ToString + "</b>")
                RH.InsertColumn(Tr8, Tr8_08, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:13pt;'>" + TotalPendingOpn.ToString + "</b>")
                val = 10
                CheckCnt = 15
                For j = 1 To Count
                    Dim tCell As New TableCell()
                    val = val + 1
                    tCell.Text = "Tr8_" & val
                    CheckCnt = CheckCnt + 1
                    If j = Count Then
                        RH.InsertColumn(Tr8, tCell, IIf(Balance > 0, DynamicPerc + Balance, DynamicPerc), 2, "<b style='color:brown;font-size:13pt;'>" + TotalVal(j).ToString + "</b>")

                    Else
                        RH.InsertColumn(Tr8, tCell, DynamicPerc, 2, "<b style='color:brown;font-size:13pt;'>" + TotalVal(j).ToString + "</b>")
                    End If
                    TotalVal(j) = 0
                Next
                If TotalConf = 0 Then
                    RH.InsertColumn(Tr8, Tr8_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:13pt;'>" + TotalConf.ToString + "</b>")
                Else
                    RH.InsertColumn(Tr8, Tr8_09, BalPerc * 10 / 100, 2, "<b style='color:brown;font-size:12pt;'><a href='ConsolidatedAuditPending_Conf.aspx?StatusID=6 &BranchID=" + GN.Encrypt("-1") + " &GroupID=" + GN.Encrypt("-1") + " &AreaID=" + GN.Encrypt("-1") + " &RegionID=" + GN.Encrypt("-1") + " &ZoneID=" + GN.Encrypt("-1") + "'  target='_blank'>" + TotalConf.ToString + "</a></b>")
                End If

                RH.InsertColumn(Tr8, Tr8_10, BalPerc * 6 / 100, 2, "<b style='color:brown;font-size:13pt;'>" + TotalPendingAudit.ToString + "</b>")
                tb.Controls.Add(Tr8)
                '------------------------------------------------------------------------------------------Total
            End If
            RH.BlankRow(tb, 4)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

    Protected Sub Branch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Branch.Click
        Branch.ImageUrl = "../../Image/Branch_sel.gif"
        Area.ImageUrl = "../../Image/Area.gif"
        Region.ImageUrl = "../../Image/Region.gif"
        Territory.ImageUrl = "../../Image/Territory.gif"
        cmbStatus.Enabled = False
        ViewState("ID") = 1
        Filter()

    End Sub

    Protected Sub BranchView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BranchView.Click
        ViewState("ID") = 1
        Filter()
    End Sub

    Protected Sub Area_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Area.Click
        Branch.ImageUrl = "../../Image/Branch.gif"
        Area.ImageUrl = "../../Image/Area_sel.gif"
        Region.ImageUrl = "../../Image/Region.gif"
        Territory.ImageUrl = "../../Image/Territory.gif"
        hdnRefresh.Value = 1
        cmbStatus.Enabled = False
        ViewState("ID") = 2
        Filter()
    End Sub

    Protected Sub Region_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Region.Click
        Branch.ImageUrl = "../../Image/Branch.gif"
        Area.ImageUrl = "../../Image/Area.gif"
        Region.ImageUrl = "../../Image/Region_sel.gif"
        Territory.ImageUrl = "../../Image/Territory.gif"
        hdnRefresh.Value = 1
        cmbStatus.Enabled = False
        ViewState("ID") = 3
        Filter()
    End Sub

    Protected Sub Territory_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Territory.Click
        Branch.ImageUrl = "../../Image/Branch.gif"
        Area.ImageUrl = "../../Image/Area.gif"
        Region.ImageUrl = "../../Image/Region.gif"
        Territory.ImageUrl = "../../Image/Territory_sel.gif"
        hdnRefresh.Value = 1
        cmbStatus.Enabled = False
        ViewState("ID") = 4
        Filter()
    End Sub

    
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Dim DTExcel As New DataTable
            Dim DTTemp As New DataTable
            Dim FilterText As String = ""
            If ddlBranch.SelectedValue > 0 Then
                FilterText = "Branch_ID=" + ddlBranch.SelectedValue.ToString()
            ElseIf ddlArea.SelectedValue > 0 Then
                FilterText = "Area_ID=" + ddlArea.SelectedValue.ToString()
            ElseIf ddlRegion.SelectedValue > 0 Then
                FilterText = "Region_ID=" + ddlRegion.SelectedValue.ToString()
            ElseIf ddlTerritory.SelectedValue > 0 Then
                FilterText = "Zone_ID=" + ddlTerritory.SelectedValue.ToString()
            End If
            DTTemp = DirectCast(ViewState("myDataSet"), DataTable)
            Dim displayView = New DataView(DTTemp, FilterText, "Zone_Id,region_id,area_id,branch_id,group_id", DataViewRowState.OriginalRows)
            DTExcel = displayView.ToTable(False, "Territory", "Region", "Area", "Branch", "Audit", "PendingDays", "Total Queries", "Pending Queries", "MBO", "QCO", "MANAGER MICRO BANKING", "HQ-SM MICROBANKING", "HEAD MICRO BANKING", "Confirmation", "Auditor")
            Dim HeaderText As String
            HeaderText = "Monitoring Report"
            WebTools.ExporttoExcel(DTExcel, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

    Protected Sub ddlTerritory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerritory.SelectedIndexChanged
        ddlBranch.SelectedValue = "-1"
        ddlArea.SelectedValue = "-1"
        ddlRegion.SelectedValue = "-1"
        Filter()
    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion.SelectedIndexChanged
        ddlBranch.SelectedValue = "-1"
        ddlArea.SelectedValue = "-1"
        ddlTerritory.SelectedValue = "-1"
        Filter()
    End Sub

    Private Sub Filter()
        Dim DTTemp As New DataTable
        Dim FilterText As String = ""
        Branch.Enabled = True
        Area.Enabled = True
        Region.Enabled = True
        Territory.Enabled = True
        Branch.Style.Add("cursor", "pointer")
        Area.Style.Add("cursor", "pointer")
        Region.Style.Add("cursor", "pointer")
        Territory.Style.Add("cursor", "pointer")
        If ddlBranch.SelectedValue > 0 Then
            FilterText = "Branch_ID=" + ddlBranch.SelectedValue.ToString()
            Area.Enabled = False
            Region.Enabled = False
            Territory.Enabled = False
            Area.Style.Add("cursor", "default")
            Region.Style.Add("cursor", "default")
            Territory.Style.Add("cursor", "default")
        ElseIf ddlArea.SelectedValue > 0 Then
            Region.Enabled = False
            Territory.Enabled = False
            Region.Style.Add("cursor", "default")
            Territory.Style.Add("cursor", "default")
            FilterText = "Area_ID=" + ddlArea.SelectedValue.ToString()
        ElseIf ddlRegion.SelectedValue > 0 Then
            Territory.Enabled = False
            Territory.Style.Add("cursor", "default")
            FilterText = "Region_ID=" + ddlRegion.SelectedValue.ToString()
        ElseIf ddlTerritory.SelectedValue > 0 Then
            FilterText = "Zone_ID=" + ddlTerritory.SelectedValue.ToString()
        End If
        DTTemp = DirectCast(ViewState("myDataSet"), DataTable)
        Dim displayView = New DataView(DTTemp, FilterText, "Zone_Id,region_id,area_id,branch_id,group_id", DataViewRowState.OriginalRows)
        DT = displayView.ToTable()
        GenerateReport(CInt(ViewState("ID")))
    End Sub

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlArea.SelectedIndexChanged
        ddlBranch.SelectedValue = "-1"
        ddlRegion.SelectedValue = "-1"
        ddlTerritory.SelectedValue = "-1"
        Filter()
    End Sub

    Protected Sub ddlBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBranch.SelectedIndexChanged
        ddlArea.SelectedValue = "-1"
        ddlRegion.SelectedValue = "-1"
        ddlTerritory.SelectedValue = "-1"
        Filter()
    End Sub

    Protected Sub Refresh_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Refresh.Click
        cmbStatus.Enabled = True
        hdnRefresh.Value = 0
        Branch.ImageUrl = "../../Image/Branch.gif"
        Area.ImageUrl = "../../Image/Area.gif"
        Region.ImageUrl = "../../Image/Region.gif"
        Territory.ImageUrl = "../../Image/Territory.gif"
    End Sub

    Protected Sub cmbStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbStatus.SelectedIndexChanged
        Dim Type As Integer = CInt(Me.cmbStatus.SelectedValue)
        Dim Parameters As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@ReportType", Type), New System.Data.SqlClient.SqlParameter("@PostID", PostID), New System.Data.SqlClient.SqlParameter("@UserID", UserID)}
        DT = DB.ExecuteDataSet("SP_AUDIT_CLOSURE_PENDING", Parameters).Tables(0)
        ViewState("myDataSet") = DT
        Filter()
    End Sub
End Class
