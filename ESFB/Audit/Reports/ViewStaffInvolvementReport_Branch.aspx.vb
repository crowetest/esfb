﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementEmpWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim SINO As Integer = 1
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim StatusID As Integer = CInt(Request.QueryString.Get("StatusID"))
            Dim FromDt As Date = CDate(Request.QueryString.Get("FromDt"))
            Dim ToDt As Date = CDate(Request.QueryString.Get("ToDt"))
            Dim AuditID As Integer = CInt(GN.Decrypt(Request.QueryString.Get("AuditID")))

            Dim RegionID As Integer = -1
            Dim AreaID As Integer = -1
            Dim BranchID As Integer = -1
            If Not Request.QueryString.Get("RegionID") Is Nothing Then
                RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            End If
            If Not Request.QueryString.Get("AreaID") Is Nothing Then
                AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            End If
            If Not Request.QueryString.Get("BranchID") Is Nothing Then
                BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))
            End If
            Dim AreaName As String = GN.GetArea_Name(AreaID)
            Dim Status As String = ""
            If StatusID = 0 Then
                Status = "Status : Closed"
            ElseIf StatusID = 1 Then
                Status = "Status :Pending"
            End If
            Dim tb As New Table
            tb.Attributes.Add("width", "100%")
            tb.Style.Add("table-layout", "fixed")
            RH.Heading(Session("FirmName"), tb, "Financial Leakage Settlement-Recovery From " + FromDt.ToString("dd/MMM/yyyy") + " To " + ToDt.ToString("dd/MMM/yyyy") + " " + AreaName + " Area " + Status, 100)

            Dim RowBG As Integer = 0
            Dim TRHead As New TableRow
            TRHead.Style.Add("Font-weight", "bold")
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_01, 15, 0, "Branch")
            RH.InsertColumn(TRHead, TRHead_02, 15, 1, "Amount")
            RH.InsertColumn(TRHead, TRHead_03, 50, 2, "Recovery Status")
            RH.InsertColumn(TRHead, TRHead_04, 15, 1, "Balance")
            tb.Controls.Add(TRHead)

            Dim TRHead1 As New TableRow
            TRHead1.Style.Add("Font-weight", "bold")
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_00, TRHead1_01, TRHead1_02, TRHead1_03, TRHead1_04, TRHead1_05, TRHead1_06, TRHead1_07, TRHead1_08 As New TableCell
            RH.InsertColumn(TRHead1, TRHead1_00, 5, 2, "")
            RH.InsertColumn(TRHead1, TRHead1_01, 15, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_02, 15, 0, "")
            RH.InsertColumn(TRHead1, TRHead1_03, 10, 2, "Recovery") '1
            RH.InsertColumn(TRHead1, TRHead1_04, 10, 2, "Rectification") '2
            RH.InsertColumn(TRHead1, TRHead1_05, 10, 2, "HR Action") '3
            RH.InsertColumn(TRHead1, TRHead1_06, 10, 2, "Legal Initiation") '4
            RH.InsertColumn(TRHead1, TRHead1_07, 10, 2, "Write-Off") '5
            RH.InsertColumn(TRHead1, TRHead1_08, 15, 2, "")
            tb.Controls.Add(TRHead1)

            Dim SQL As String = ""
            If AuditID > 0 Then
                SQL = " AND  g.AUDIT_ID = " + AuditID.ToString() + " "
            End If
            If StatusID = -1 Then
                DT = DB.ExecuteDataSet("select c.branch_ID,c.branch_Name,SUM(isnull(a.Amount,0)) Total_amount,isnull(sum(d.Recover),0) Recover,isnull(sum(d.Rectification),0) Rectification,isnull(sum(d.Hraction),0) Hraction,isnull(sum(d.Legal),0) Legal,isnull(sum(d.Writeoff),0) Writeoff,sum(isnull(a.Amount,0)-isnull(a.refund_amount,0)) Balance from emp_master b,BrMaster c,AUDIT_OBSERVATION_DTL e,audit_observation f,AUDIT_DTL g,AUDIT_MASTER h,AUDIT_STAFF_INVOLVEMENT a left outer join ( select emp_code,SLNO,sum(case when typeid = 1 then amount else 0 end) as Recover,sum(case when typeid = 2 then amount else 0 end) as Writeoff,sum(case when typeid = 3 then amount else 0 end) as Rectification,sum(case when typeid = 4 then amount else 0 end) as Legal,sum(case when typeid = 5 then amount else 0 end) as Hraction from AUDIT_STAFF_INVOLVEMENT_RECOVERY_DTL group by EMP_CODE,SLNO)d on(d.SLNO=a.SINO and d.EMP_CODE=a.EMP_CODE ) where a.EMP_CODE = b.Emp_Code and h.Branch_ID = c.Branch_ID and a.SINO = e.SINO and e.OBSERVATION_ID = f.OBSERVATION_ID and f.AUDIT_ID = g.AUDIT_ID and g.GROUP_ID = h.GROUP_ID and h.STATUS_ID = 2 and DATEADD(day, DATEDIFF(day, 0, a.Tra_DT), 0) between '" + FromDt.ToString() + "' and  '" + ToDt.ToString() + "'  and c.area_id = " + AreaID.ToString() + " " + SQL.ToString() + " group by c.branch_ID,c.branch_Name order by c.branch_name").Tables(0)
            Else
                DT = DB.ExecuteDataSet("select c.branch_ID,c.branch_Name,SUM(isnull(a.Amount,0)) Total_amount,isnull(sum(d.Recover),0) Recover,isnull(sum(d.Rectification),0) Rectification,isnull(sum(d.Hraction),0) Hraction,isnull(sum(d.Legal),0) Legal,isnull(sum(d.Writeoff),0) Writeoff,sum(isnull(a.Amount,0)-isnull(a.refund_amount,0)) Balance from emp_master b,BrMaster c,AUDIT_OBSERVATION_DTL e,audit_observation f,AUDIT_DTL g,AUDIT_MASTER h,AUDIT_STAFF_INVOLVEMENT a left outer join ( select emp_code,SLNO,sum(case when typeid = 1 then amount else 0 end) as Recover,sum(case when typeid = 2 then amount else 0 end) as Writeoff,sum(case when typeid = 3 then amount else 0 end) as Rectification,sum(case when typeid = 4 then amount else 0 end) as Legal,sum(case when typeid = 5 then amount else 0 end) as Hraction from AUDIT_STAFF_INVOLVEMENT_RECOVERY_DTL group by EMP_CODE,SLNO)d on(d.SLNO=a.SINO and d.EMP_CODE=a.EMP_CODE ) where a.EMP_CODE = b.Emp_Code and h.Branch_ID = c.Branch_ID and a.SINO = e.SINO and e.OBSERVATION_ID = f.OBSERVATION_ID and f.AUDIT_ID = g.AUDIT_ID and g.GROUP_ID = h.GROUP_ID  and a.STATUS_ID = " + StatusID.ToString() + " and h.STATUS_ID = 2  and DATEADD(day, DATEDIFF(day, 0, a.Tra_DT), 0) between '" + FromDt.ToString() + "' and  '" + ToDt.ToString() + "'  and c.area_id = " + AreaID.ToString() + "  " + SQL.ToString() + " group by c.branch_ID,c.branch_Name order by c.branch_name").Tables(0)
            End If


            Dim DR As DataRow
            Dim Tot1 As Double = 0
            Dim Tot2 As Double = 0
            Dim Tot3 As Double = 0
            Dim Tot4 As Double = 0
            Dim Tot5 As Double = 0
            Dim Tot6 As Double = 0
            Dim Tot7 As Double = 0
            For Each DR In DT.Rows
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell
                RH.InsertColumn(TR3, TR3_00, 5, 2, SINO.ToString())
                RH.InsertColumn(TR3, TR3_01, 15, 0, "<a href='ViewStaffInvolvementReport_Emp.aspx?StatusID=" + StatusID.ToString() + "&RegionID=" + GN.Encrypt(RegionID.ToString()) + "&AreaID=" + GN.Encrypt(AreaID.ToString()) + "&BranchID=" + GN.Encrypt(DR(0).ToString()) + " &AuditID=" + GN.Encrypt(AuditID.ToString()) + " &FromDt=" + FromDt.ToString() + " &ToDt=" + ToDt.ToString() + "' target=_blank>" + DR(1).ToString() + "</a>")
                If (DR(2) > 0) Then
                    RH.InsertColumn(TR3, TR3_02, 15, 1, DR(2).ToString())
                Else
                    RH.InsertColumn(TR3, TR3_02, 15, 1, "-")
                End If

                If (DR(3) > 0) Then
                    RH.InsertColumn(TR3, TR3_03, 10, 1, DR(3).ToString())
                Else
                    RH.InsertColumn(TR3, TR3_03, 10, 1, "-")
                End If
                If (DR(4) > 0) Then
                    RH.InsertColumn(TR3, TR3_04, 10, 1, DR(4).ToString())
                Else
                    RH.InsertColumn(TR3, TR3_04, 10, 1, "-")
                End If
                If (DR(5) > 0) Then
                    RH.InsertColumn(TR3, TR3_05, 10, 1, DR(5).ToString())
                Else
                    RH.InsertColumn(TR3, TR3_05, 10, 1, "-")
                End If
                If (DR(6) > 0) Then
                    RH.InsertColumn(TR3, TR3_06, 10, 1, DR(6).ToString())
                Else
                    RH.InsertColumn(TR3, TR3_06, 10, 1, "-")
                End If
                If (DR(7) > 0) Then
                    RH.InsertColumn(TR3, TR3_07, 10, 1, DR(7).ToString())
                Else
                    RH.InsertColumn(TR3, TR3_07, 10, 1, "-")
                End If
                RH.InsertColumn(TR3, TR3_08, 15, 1, DR(8).ToString())
                tb.Controls.Add(TR3)
                SINO += 1
                Tot1 += DR(2)
                Tot2 += DR(3)
                Tot3 += DR(4)
                Tot4 += DR(5)
                Tot5 += DR(6)
                Tot6 += DR(7)
                Tot7 += DR(8)
            Next

            Dim TRFooter As New TableRow
            TRFooter.BackColor = Drawing.Color.WhiteSmoke
            Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05, TRFooter_06, TRFooter_07, TRFooter_08 As New TableCell
            TRFooter.Style.Add("Font-weight", "bold")
            RH.InsertColumn(TRFooter, TRFooter_00, 5, 2, "")
            RH.InsertColumn(TRFooter, TRFooter_01, 15, 0, "TOTAL")
            If (Tot1 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_02, 15, 1, Tot1.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_02, 15, 1, "-")
            End If
            If (Tot2 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_03, 10, 1, Tot2.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_03, 10, 1, "-")
            End If
            If (Tot3 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_04, 10, 1, Tot3.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_04, 10, 1, "-")
            End If
            If (Tot4 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_05, 10, 1, Tot4.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_05, 10, 1, "-")
            End If
            If (Tot5 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_06, 10, 1, Tot5.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_06, 10, 1, "-")
            End If
            If (Tot6 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_07, 10, 1, Tot6.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_07, 10, 1, "-")
            End If
            If (Tot7 > 0) Then
                RH.InsertColumn(TRFooter, TRFooter_08, 15, 1, Tot7.ToString("0.00"))
            Else
                RH.InsertColumn(TRFooter, TRFooter_08, 15, 1, "-")
            End If
            tb.Controls.Add(TRFooter)
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
