﻿Imports System.Data

Partial Class Audit_Reports_VisitedSangamReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 59) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim Postid As Integer = CInt(Session("Post_ID"))
            Dim EmpCode As Integer = CInt(Session("UserID"))
            Me.Master.subtitle = "Visited Center List"

            DT = DB.ExecuteDataSet("select '-1' as AuditID,' ------Select------' as Audit Union All " & _
                                    " select distinct cast(MONTH(a.Period_to) as varchar(2))+'~'+cast(year(a.Period_to) as varchar(4)), datename(MONTH,a.Period_to) +'-'+ " & _
                                    " CAST(YEAR(a.Period_to) AS VARCHAR(4)) from Audit_Master a,AUDIT_VISITED_SANGAM_MASTER b where  a.status_id<>9 and a.GROUP_ID=b.AUDIT_GROUP_ID").Tables(0)

            GN.ComboFill(cmbAudit, DT, 0, 1)
            'GN.ComboFill(cmbAudit, DT, 0, 1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
