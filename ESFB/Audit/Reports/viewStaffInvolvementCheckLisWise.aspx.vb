﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewStaffInvolvementCheckLisWise
    Inherits System.Web.UI.Page
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim AuditDtl As String
    Dim PostID As Integer
    Dim AD As New Audit
    Dim BranchID As Integer
    Dim RegionID As Integer
    Dim AreaID As Integer
    Dim ItemID As Integer
    Dim SINO As Integer
    Dim CloseFlag As Integer
    Dim FromDt As DateTime
    Dim ToDt As DateTime
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim StrSubHead As String
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Me.hdnReportID.Value = Request.QueryString.Get("ReportID")
            PostID = CInt(Session("Post_ID"))

            AuditDtl = GN.Decrypt(Request.QueryString.Get("AuditID"))
            RegionID = CInt(GN.Decrypt(Request.QueryString.Get("RegionID")))
            AreaID = CInt(GN.Decrypt(Request.QueryString.Get("AreaID")))
            BranchID = CInt(GN.Decrypt(Request.QueryString.Get("BranchID")))

            ItemID = CInt(Request.QueryString.Get("ItemID"))
            SINO = CInt(Request.QueryString.Get("SINO"))
            CloseFlag = CInt(Request.QueryString.Get("CloseFlag"))
            FromDt = CDate(Request.QueryString.Get("FromDt"))
            ToDt = CDate(Request.QueryString.Get("ToDt"))
            Dim Month As Integer = -1
            Dim year As Integer = -1
            Dim SubHD As String = "Report Parameters:"
            Dim AuditArr() As String = AuditDtl.Split("~")
            If (AuditArr(0) <> -1) Then
                Month = CInt(AuditArr(0))
                year = CInt(AuditArr(1))
                SubHD += " Period " + MonthName(Month) + "-" + year.ToString()
            End If

            If BranchID > 0 Then
                SubHD += " Branch - " + GN.GetBranch_Name(BranchID)
            ElseIf AreaID > 0 Then
                SubHD += " Area - " + GN.GetArea_Name(AreaID)
            ElseIf RegionID > 0 Then
                SubHD += " Region - " + GN.GetRegion_Name(RegionID)
            Else
                SubHD += " Consolidated"
            End If
            Dim Status As String = ""
            If CloseFlag = 0 Then
                Status = "Status : Closed"
            ElseIf CloseFlag = 1 Then
                Status = "Status : Pending"
            End If
            If ItemID > 0 Then
                Dim DTItem As New DataTable
                DTItem = AD.GetItemName(ItemID, 0)
                SubHD += " Check List - " + DTItem.Rows(0)(0).ToString()
            End If

            Dim DTAudit As New DataTable
            Dim DT1 As New DataTable
            Dim DTMASTER As New DataTable

            RH.Heading(Session("FirmName"), tb, "Staff Involvement Report " + Status, 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            RH.SubHeading(tb, 100, "l", SubHD)
            StrSubHead = "Staff Involvement Report " + Status + "(" + SubHD + ")"
            Dim I As Integer = 0
            Dim LineNumber As Integer = 8
            Dim j As Integer = 0

            Dim BranchName As String = ""
            Dim ItemName As String = ""

            'RH.BlankRow(tb, 3)
            Dim TotObservation As Integer = 0
            Dim TotStaff As Integer = 0
            Dim TotLeakage As Integer = 0

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05 As New TableCell
            RH.InsertColumn(TRHead, TRHead_00, 5, 2, "Sl No.")
            RH.InsertColumn(TRHead, TRHead_01, 65, 0, "Check List")
            RH.InsertColumn(TRHead, TRHead_02, 15, 2, "No of Observations")
            RH.InsertColumn(TRHead, TRHead_03, 15, 2, "No of Employees")
            ' RH.InsertColumn(TRHead, TRHead_04, 15, 1, "Est. Amt. Involved")
            ' RH.InsertColumn(TRHead, TRHead_05, 5, 2, "Audits")


            tb.Controls.Add(TRHead)

            DT = AD.GetStaffInvolvementCases(Month, year, PostID, CInt(Session("UserID")), BranchID, AreaID, RegionID, ItemID, SINO, CloseFlag, FromDt, ToDt)
            For Each DR In DT.Rows
                I += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05 As New TableCell

                RH.InsertColumn(TR3, TR3_00, 5, 2, I.ToString())
                RH.InsertColumn(TR3, TR3_01, 65, 0, DR(0).ToString())
                RH.InsertColumn(TR3, TR3_02, 15, 2, DR(1).ToString())
                RH.InsertColumn(TR3, TR3_03, 15, 2, DR(2).ToString())
                ' RH.InsertColumn(TR3, TR3_04, 15, 1, DR(3).ToString())
                ' RH.InsertColumn(TR3, TR3_05, 5, 2, DR(4).ToString())
                tb.Controls.Add(TR3)
                TotObservation += DR(1)
                TotStaff += DR(2)
                TotLeakage += DR(3)
            Next

            'Dim TRFooter As New TableRow
            'TRFooter.BackColor = Drawing.Color.WhiteSmoke
            'Dim TRFooter_00, TRFooter_01, TRFooter_02, TRFooter_03, TRFooter_04, TRFooter_05 As New TableCell
            'TRFooter.Style.Add("Font-weight", "bold")


            'RH.InsertColumn(TRFooter, TRFooter_00, 5, 2, "")
            'RH.InsertColumn(TRFooter, TRFooter_01, 50, 2, "Total")
            'RH.InsertColumn(TRFooter, TRFooter_02, 15, 2, TotObservation.ToString())
            'RH.InsertColumn(TRFooter, TRFooter_03, 15, 2, TotStaff.ToString()) '
            'RH.InsertColumn(TRFooter, TRFooter_04, 15, 1, TotLeakage.ToString("0.00"))
            '' RH.AddColumn(TRFooter, TRFooter_05, 10, 10, "l", "")

            'tb.Controls.Add(TRFooter)

            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            WebTools.ExporttoExcel(DT, StrSubHead)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
