﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PreviousAuditConfirmation
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim GF As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim dt As New DataTable
    Dim AUD As New Audit
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 335) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Audit Observation Confirmation"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            hid_Dtls.Value = "ÿ~~~~~~~~~~~~~~"
            hid_temp.Value = Nothing

            dt = DB.ExecuteDataSet("select '-1' as ID,'----------Select----------' union all select DISTINCT D.BRANCH_ID,b.Branch_Name from AUDIT_DTL a,AUDIT_BRANCHES k,BRANCH_MASTER b,AUDIT_TYPE c,AUDIT_MASTER d LEFT JOIN AUDIT_OBS_CONFIRMATION G " & _
                    " ON D.GROUP_ID=G.GROUP_ID,AUDIT_OBSERVATION e,(select * from AUDIT_OBSERVATION_DTL where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17 ))  f  WHERE  a.branch_id=b.Branch_ID AND a.branch_id=K.Branch_ID and a.GROUP_ID=d.GROUP_ID and a.AUDIT_TYPE=c.TYPE_ID and a.BRANCH_ID= " & _
                    " b.Branch_ID and a.AUDIT_ID=e.AUDIT_ID and e.OBSERVATION_ID=f.OBSERVATION_ID and f.STATUS_ID =0 and f.confirmation_flag=0 AND G.CLOSED_DATE IS NULL " & _
                    " AND k.EMP_CODE=" & CInt(Session("UserID")).ToString() & " order by 2,1").Tables(0)
            GF.ComboFill(cmbBranch, dt, 0, 1)
            Me.cmbBranch.Attributes.Add("onchange", "return BranchOnchange()")
            Me.cmbAudit.Attributes.Add("onchange", "return AuditOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))

        Dim UserID As Integer = CInt(Session("UserID"))

        If RequestID = 1 Then

            Dim Datas As String = CStr(Data(1))
            Dim BranchID As Integer = CInt(Data(2))
            Dim ClosedFlag As Integer = CInt(Data(3))
            Dim GroupID As Integer = CInt(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(6) As SqlParameter

                Params(0) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar)
                Params(0).Value = Datas
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = CInt(Session("UserID"))
                Params(2) = New SqlParameter("@CloseFlag", SqlDbType.Int)
                Params(2).Value = ClosedFlag
                Params(3) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(3).Value = BranchID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@Group_ID", SqlDbType.Int)
                Params(6).Value = GroupID
                DB.ExecuteNonQuery("SP_AUDIT_OBS_CONFIRMATION", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 2 Then
            Dim BranchID As Integer = CInt(Data(1))
            dt = DB.ExecuteDataSet("select DISTINCT d.Group_ID ,datename(MONTH,d.period_to) +'-'+CAST(YEAR(d.period_to) AS VARCHAR(4)) from AUDIT_DTL a,BRANCH_MASTER b,AUDIT_TYPE c,AUDIT_MASTER d LEFT JOIN AUDIT_OBS_CONFIRMATION G ON D.GROUP_ID=G.GROUP_ID,AUDIT_OBSERVATION e,(select * from AUDIT_OBSERVATION_DTL where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17 )) f WHERE  " & _
                   " a.GROUP_ID=d.GROUP_ID and a.AUDIT_TYPE=c.TYPE_ID and a.BRANCH_ID=b.Branch_ID and a.AUDIT_ID=e.AUDIT_ID and e.OBSERVATION_ID=f.OBSERVATION_ID and f.STATUS_ID =0 and f.confirmation_flag=0 and d.SKIP_FLAG=0 AND G.CLOSED_DATE IS NULL  and d.branch_id=" & BranchID.ToString() & "  order by 2,1").Tables(0)
            Dim StrAudit As String = ""
            If dt.Rows.Count > 0 Then
                StrAudit = "Ñ"

            End If

            For n As Integer = 0 To dt.Rows.Count - 1
                StrAudit += dt.Rows(n)(0).ToString & "ÿ" & dt.Rows(n)(1).ToString
                If n < dt.Rows.Count - 1 Then
                    StrAudit += "Ñ"
                End If
            Next
            CallBackReturn = StrAudit.ToString
        ElseIf RequestID = 3 Then
            Dim GroupID As Integer = CInt(Data(1))

            dt = DB.ExecuteDataSet("select distinct '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+  " & _
                " cast(g.Group_ID as varchar(6))+'~'+a.LOAN_NO+'~'+cast(e.Item_ID as varchar(6))+'~'+d.CLIENT_NAME+'~'+h.Item_Name+'~'+i.center_Name+'~'+  " & _
                " j.remarks+'~'+(case when a.fraud=1 then 'VS' when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~'+  " & _
                " convert(varchar(11),isnull(d.DISBURSED_DT,d.SANCTIONED_DT ),103)+'~'+cast(d.LOAN_AMT as varchar)  from AUDIT_OBSERVATION_DTL a left outer join  " & _
                " (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),LOAN_MASTER d,AUDIT_OBSERVATION e  " & _
                " ,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j,audit_branches b    " & _
                " where( a.OBSERVATION_ID = e.OBSERVATION_ID And e.AUDIT_ID = f.AUDIT_ID And f.group_ID = g.Group_Id and f.AUDIT_TYPE<>3 And g.Status_id not in (0,9))  " & _
                " and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.SINO=j.sino and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.confirmation_flag=0 and a.status_id=0  " & _
                " and g.Group_ID= " & GroupID.ToString & "  " & _
                " union all  " & _
                " select  distinct '0~'+cast(a.SINO as varchar(8))+'~'+cast(a.LEVEL_ID as varchar(2))+'~'+cast(a.LEVEL_UPTO as varchar(2))+'~'+a.REMARKS+'~'+  " & _
                " cast(i.Group_ID as varchar(6))+'~'+'~'+cast(b.Item_ID as varchar(6))+'~~'+c.ITEM_NAME+'~~'+j.remarks+'~'+(case when a.fraud=1 then 'VS'  " & _
                " when a.Susp_Leakage=1 then 'S' else 'G' end)+'~'+cast(isnull(ai.cnt,0) as varchar)+'~~' from dbo.AUDIT_OBSERVATION_DTL a left outer join   " & _
                " (select sino,count(sino)as cnt from audit_staff_involvement group by sino)ai on(a.sino=ai.sino),dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c  " & _
                " ,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,audit_branches f ,AUDIT_DTL h,Audit_Master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j WHERE   " & _
                " a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id And i.Status_id not in (0,9) and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and a.SINO=j.sino " & _
                " and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3   and a.confirmation_flag=0 and a.status_id=0  " & _
                " and i.Group_ID= " & GroupID.ToString & " ").Tables(0)
            If (dt.Rows.Count > 0) Then
                For Each DR As DataRow In dt.Rows
                    CallBackReturn += "ÿ" + DR(0).ToString()
                Next
            Else
                CallBackReturn = "ÿ~~~~~~~~~~~~~~"
            End If
            CallBackReturn += "Ø"
            dt = DB.ExecuteDataSet("select distinct ITEM_ID,name from ( " & _
                " select e.Item_ID ,CAST(count(distinct a.SINO) as varchar)+' - '+h.Item_Name as name   from AUDIT_OBSERVATION_DTL a ,LOAN_MASTER d,AUDIT_OBSERVATION e  " & _
                " ,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j,audit_branches b     " & _
                " where( a.OBSERVATION_ID = e.OBSERVATION_ID And e.AUDIT_ID = f.AUDIT_ID And f.group_ID = g.Group_Id and f.AUDIT_TYPE<>3 And g.Status_id not in (0,9))   " & _
                " and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.SINO=j.sino and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id  and a.confirmation_flag=0 and a.status_id=0   " & _
                " and g.Group_ID= " & GroupID.ToString & " group by e.item_id,h.Item_Name    " & _
                " union all   " & _
                " select  b.Item_ID ,CAST(count(distinct a.SINO) as varchar)+' - '+c.Item_Name as name  from dbo.AUDIT_OBSERVATION_DTL a,dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c  " & _
                " ,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,audit_branches f ,AUDIT_DTL h,Audit_Master i,(select *  from audit_observation_cycle where  " & _
                " SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j WHERE    " & _
                " a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id And i.Status_id not in (0,9)  " & _
                " and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID  and a.SINO=j.sino " & _
                 " and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3    and a.confirmation_flag=0 and a.status_id=0   " & _
                 " and i.Group_ID=" & GroupID.ToString & " group by b.item_id,c.Item_Name  " & _
                 " )AA").Tables(0)
            For Each DR As DataRow In dt.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            '--------------Total count
            CallBackReturn += "Ø"
            dt = DB.ExecuteDataSet("select isnull(SUM(isnull(cnt,0)),0) from ( " & _
                " select count(distinct a.sino) as cnt  from AUDIT_OBSERVATION_DTL a ,LOAN_MASTER d,AUDIT_OBSERVATION e  " & _
                " ,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j,audit_branches b " & _
                " where( a.OBSERVATION_ID = e.OBSERVATION_ID And e.AUDIT_ID = f.AUDIT_ID And f.group_ID = g.Group_Id and f.AUDIT_TYPE<>3 And g.Status_id not in (0,9))  " & _
                " and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.SINO=j.sino and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and a.status_id=0  and g.Group_ID= " & GroupID.ToString & "     " & _
                " union all " & _
                " select  count(distinct a.sino) as cnt from dbo.AUDIT_OBSERVATION_DTL a ,dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c  " & _
                " ,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,audit_branches f ,AUDIT_DTL h,Audit_Master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j WHERE   " & _
                " a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id And i.Status_id not in (0,9) and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and a.SINO=j.sino " & _
                " and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3     " & _
                " and a.status_id=0 and i.Group_ID=" & GroupID.ToString & "   )AA").Tables(0)
            Dim Totalcount As Integer = 0
            If CInt(dt.Rows(0)(0)) > 0 Then
                Totalcount = CInt(dt.Rows(0)(0))
            Else
                Totalcount = CInt(0)
            End If
            Dim TotalCnt As Integer = 0
            dt = DB.ExecuteDataSet("select percentage from audit_parameters where sino=1").Tables(0)
            TotalCnt = CInt(dt.Rows(0)(0))
            Totalcount = CInt(Totalcount * TotalCnt / 100)
            CallBackReturn += CStr(IIf(IsDBNull(Totalcount), 0, Totalcount))
            '--------------Saved count
            CallBackReturn += "Ø"
            dt = DB.ExecuteDataSet("select isnull(SUM(isnull(cnt,0)),0) from ( " & _
                " select count(distinct a.sino) as cnt  from AUDIT_OBSERVATION_DTL a ,LOAN_MASTER d,AUDIT_OBSERVATION e  " & _
                " ,AUDIT_DTL f,AUDIT_MASTER g,dbo.AUDIT_CHECK_LIST h,center_master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j,audit_branches b " & _
                " where( a.OBSERVATION_ID = e.OBSERVATION_ID And e.AUDIT_ID = f.AUDIT_ID And f.group_ID = g.Group_Id and f.AUDIT_TYPE<>3 And g.Status_id not in (0,9))  " & _
                " and e.Item_ID=h.item_ID and a.order_ID=j.order_ID and a.SINO=j.sino and a.LOAN_NO=d.LOAN_NO and d.center_id=i.center_id and confirmation_flag=1 and a.status_id=0  and g.Group_ID= " & GroupID.ToString & "     " & _
                " union all " & _
                " select  count(distinct a.sino) as cnt from dbo.AUDIT_OBSERVATION_DTL a,dbo.AUDIT_OBSERVATION b ,dbo.AUDIT_CHECK_LIST  c  " & _
                " ,AUDIT_CHECK_SUBGROUPS d,AUDIT_CHECK_GROUPS e,audit_branches f ,AUDIT_DTL h,Audit_Master i,(select *  from audit_observation_cycle where SINO  not in (select distinct sino from audit_observation_cycle where UPDATE_ID=17  )) j WHERE   " & _
                " a.OBSERVATION_ID=b.OBSERVATION_ID and b.AUDIT_ID=h.AUDIT_ID and h.group_id=i.group_id And i.Status_id not in (0,9) and b.ITEM_ID =c.item_id and a.order_ID=j.order_ID and a.SINO=j.sino " & _
                " and c.SUB_GROUP_ID=d.SUB_GROUP_ID and d.GROUP_ID=e.GROUP_ID and e.AUDIT_TYPE=3  and confirmation_flag=1  and a.status_id=0  " & _
                " and i.Group_ID=" & GroupID.ToString & "   )AA").Tables(0)
            If CInt(dt.Rows(0)(0)) > 0 Then
                CallBackReturn += dt.Rows(0)(0).ToString
            Else
                CallBackReturn += CStr(0)
            End If
        End If



    End Sub
#End Region
End Class
