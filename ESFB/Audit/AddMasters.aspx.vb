﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AddMasters
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Disposed"
    Protected Sub General_AddMasters_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 244) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Master Maintenance"
            If Not IsPostBack Then
                DT = GF.GetQueryResult("select -1,'---Select---' AuditType union all  select Type_ID,Type_Name from audit_Type where status_id=1 order by 2")
                GF.ComboFill(cmbAuditType, DT, 0, 1)
                DT = GF.GetQueryResult("select -1,'---NEW---' Group_Name ")
                GF.ComboFill(cmbHead, DT, 0, 1)
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.cmbHead.Attributes.Add("onchange", "return HeadOnChange()")
            Me.cmbAuditType.Attributes.Add("onchange", "return TypeOnChange()")
            Me.cmbSubType.Attributes.Add("onchange", "return SubGroupOnChange()")
            Me.cmbCheckList.Attributes.Add("onchange", "return CheckListOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub General_AddMasters_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim SQL As String = ""
        Dim SQL1 As String = ""

        Select Case CInt(Data(0))
            Case 1
                Dim TypeID As Integer = CInt(Data(1))
                Dim AuditTypeID As Integer = CInt(Data(2))
                If TypeID = 1 Then
                    SQL = "select -1,'---NEW---' Group_Name union all  select Group_ID,Group_Name from Audit_Check_Groups where audit_type=" + AuditTypeID.ToString() + " and status_id=1 order by 2"
                Else
                    SQL = "select -1,'---SELECT---' Group_Name union all  select Group_ID,Group_Name from Audit_Check_Groups where audit_type=" + AuditTypeID.ToString() + " and status_id=1 order by 2"
                End If
                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 2
                Dim GroupID As Integer = CInt(Data(1))
                SQL = "select -1,'---Select---' Group_Name union all  select SUB_GROUP_ID,Sub_Group_Name from Audit_Check_SubGroups where GROUP_ID=" + GroupID.ToString() + " and status_id=1 order by 2"
                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 3
                Dim TypeID As Integer = CInt(Data(1))
                Dim GroupID As Integer = CInt(Data(2))
                If TypeID = 2 Then
                    SQL = "select -1,'---NEW---' SubGroup_Name union all  select Sub_Group_ID,Sub_Group_Name from Audit_Check_SubGroups where Group_ID=" + GroupID.ToString() + " and status_id=1 order by 2"
                Else
                    SQL = "select -1,'---Select---' Group_Name union all  select Sub_Group_ID,Sub_Group_Name from Audit_Check_SubGroups where Group_ID=" + GroupID.ToString() + " and status_id=1 order by 2"
                End If

                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
            Case 4
                '"4Ø" + CategoryID + "Ø" + TypeID + "Ø" + SubTypeID + "Ø" + SubTypeName + "Ø" + HeadCode + "Ø" + Flag  SPM + "Ø" + Serious + "Ø" + VSerious + "Ø" + AuditTypeID + "Ø" + Sample + "Ø" + Category + "Ø" + MaxMar + "Ø" + Description;
                Dim TypeID As Integer = CInt(Data(1)) 'Region/Area/Territory
                Dim GroupID As Integer = CInt(Data(2))
                Dim SubGroupID As Integer = CInt(Data(3))
                Dim CheckListID As Integer = CInt(Data(4))
                Dim SubTypeName As String = CStr(Data(5))
                Dim Flag As Integer = CInt(Data(6))
                Dim SPMFlag As Integer = CInt(Data(7))
                Dim SeriousFlag As Integer = CInt(Data(8))
                Dim VSeriousFlag As Integer = CInt(Data(9))
                Dim AuditTypeID As Integer = CInt(Data(10))
                Dim Sample As Integer = CInt(Data(11))
                Dim Category As Integer = CInt(Data(12))
                Dim Max_Mark As Double = CDbl(Data(13))
                Dim Description As String = CStr(Data(14))

                Dim ErrorFlag As Integer = 0
                Dim Message As String = Nothing
                Try
                    Dim Params(16) As SqlParameter
                    Params(0) = New SqlParameter("@TypeID", SqlDbType.Int)
                    Params(0).Value = TypeID
                    Params(1) = New SqlParameter("@GroupID", SqlDbType.Int)
                    Params(1).Value = GroupID
                    Params(2) = New SqlParameter("@SubGroupID", SqlDbType.Int)
                    Params(2).Value = SubGroupID
                    Params(3) = New SqlParameter("@SubTypeName", SqlDbType.VarChar, 50)
                    Params(3).Value = SubTypeName
                    Params(4) = New SqlParameter("@CheckListID", SqlDbType.Int)
                    Params(4).Value = CheckListID
                    Params(5) = New SqlParameter("@Flag", SqlDbType.Int)
                    Params(5).Value = Flag
                    Params(6) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(6).Value = CInt(Session("UserID"))
                    Params(7) = New SqlParameter("@SPMFLAG", SqlDbType.Int)
                    Params(7).Value = SPMFlag
                    Params(8) = New SqlParameter("@SERIOUSFLAG", SqlDbType.Int)
                    Params(8).Value = SeriousFlag
                    Params(9) = New SqlParameter("@VSERIOUSFLAG", SqlDbType.Int)
                    Params(9).Value = VSeriousFlag
                    Params(10) = New SqlParameter("@AuditType", SqlDbType.Int)
                    Params(10).Value = AuditTypeID
                    Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(11).Direction = ParameterDirection.Output
                    Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(12).Direction = ParameterDirection.Output
                    Params(13) = New SqlParameter("@SAMPLE", SqlDbType.Int)
                    Params(13).Value = Sample
                    Params(14) = New SqlParameter("@CATEGORY", SqlDbType.Int)
                    Params(14).Value = Category
                    Params(15) = New SqlParameter("@MAX_MARK", SqlDbType.Decimal)
                    Params(15).Value = Max_Mark
                    Params(15).Precision = 10
                    Params(15).Scale = 2
                    Params(16) = New SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 1000)
                    Params(16).Value = Description

                    DB.ExecuteNonQuery("SP_AUDIT_MASTER_MAINTENANCE", Params)
                    Message = CStr(Params(12).Value)
                    ErrorFlag = CInt(Params(11).Value)
                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

                End Try
                CallBackReturn = ErrorFlag.ToString() + "ʘ" + Message.ToString()
            Case 5
                Dim HeadID As Integer = CInt(Data(2))
                Dim TypeID As Integer = CInt(Data(1))
                If TypeID = 1 Then
                    SQL = "select Group_Name,0,0 from Audit_Check_Groups a where a.Group_ID= " & HeadID.ToString() & ""
                ElseIf TypeID = 2 Then
                    SQL = "select Sub_Group_Name , isnull(Sample_type,-1) , isnull(Category,-1) from Audit_Check_SubGroups a where a.Sub_Group_ID= " & HeadID.ToString() & ""
                Else
                    SQL = "select Item_Name,spm_Flag,serious_Flag,Very_Serious_Flag,Max_Mark, Description from Audit_Check_List a where a.Item_ID= " & HeadID.ToString() & ""
                End If

                DT = GF.GetQueryResult(SQL)
                If TypeID <> 3 Then
                    If DT.Rows.Count > 0 Then
                        CallBackReturn = TypeID.ToString() + "Ø" + DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString()
                    End If
                Else
                    If DT.Rows.Count > 0 Then
                        CallBackReturn = TypeID.ToString() + "Ø" + DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString()
                    End If
                End If
             
            Case 6
                Dim TypeID As Integer = CInt(Data(1))
                Dim SubGroupID As Integer = CInt(Data(2))
                If TypeID = 3 Then
                    SQL = "select -1,'---NEW---' ItemName union all  select Item_ID,Item_Name from Audit_Check_List where Sub_Group_ID=" + SubGroupID.ToString() + " and status_id=1 order by 2"
                Else
                    SQL = "select -1,'---SELECT---' Group_Name union all select Item_ID,Item_Name from Audit_Check_List where Sub_Group_ID=" + SubGroupID.ToString() + " and status_id=1 order by 2"
                End If
                DT = GF.GetQueryResult(SQL)
                If DT.Rows.Count > 0 Then
                    For Each DR As DataRow In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                If TypeID = 3 Then
                    SQL = "select  isnull(Sample_type,-1) , isnull(Category,-1) from Audit_Check_SubGroups a where a.Sub_Group_ID= " & SubGroupID.ToString() + " and status_id=1 order by 2"
                    DT = GF.GetQueryResult(SQL)
                    If DT.Rows.Count > 0 Then
                        CallBackReturn += "Ø" + DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString()
                    End If

                End If
        End Select
    End Sub
#End Region
End Class
