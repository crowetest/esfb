﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Audit.aspx.vb" Inherits="Audit_Audit"  EnableEventValidation="false"%>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#FFF;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
      
        .style1
        {
            width: 20%;
            height: 21px;
        }
        .style2
        {
            width: 80%;
            height: 21px;
        }
      
    </style>
   
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var CloseFlag = 0;
           function btnExit_onclick() 
        {
            window.open("AuditStart.aspx", "_self");
        }
         function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }

         function FromServer(arg, context) {

         switch (context) {
             case 1:
                 var Data = arg.split("Ø");
                 alert(Data[1]);
                 if (CloseFlag == 0) {
                     CloseFlag = 0; if (Data[0] == 0 || Data[0] == 2) { document.getElementById("<%= cmbCheckList.ClientID %>").value = -1; document.getElementById("<%= hdnAuditDetails.ClientID %>").value = ""; table_fill(""); }

                 }
                 else {
                     if (document.getElementById("<%= hdnAuditTypeID.ClientID %>").value == 1) {
                         if (Data[0] == 0 || Data[0] == 2) { window.open("RateABranch.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=1 &TypeID=1", "_self"); }
                         else if (Data[0] == 3) { window.open("RateABranch.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=1 &TypeID=3", "_self"); }
                     }
                     else {
                         if (Data[0] == 0 || Data[0] == 2) { window.open("AuditStart.aspx", "_self"); }
                         else if (Data[0] == 3) { window.open("AuditStaffInvolvement.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=1 ", "_self"); }
                     }
                 }
                 break;
                case 2:
                    ComboFill(arg, "<%= cmbSubGroup.ClientID %>");
                    break;
                case 3:
                    ComboFill(arg, "<%= cmbCheckList.ClientID %>");
                    break;
                case 4:
                    var Data = arg.split("Ø");
                    document.getElementById("<%= hdnAuditDetails.ClientID %>").value = Data[0];
                    document.getElementById("<%= hdnItemType.ClientID %>").value = Data[1];
                   
                    table_fill(Data[0], Data[1]);
                    break;
                case 5:
                    if (arg == "1") {
                        alert("Please Check the Deposit No");
                    }
                    else if (arg == "") {
                        document.getElementById("txtCustomerName").value = "";
                        document.getElementById("txtLoanDate").value = "";
                        //document.getElementById("txtSanctionDt").value = "";
                        document.getElementById("txtLoanAmount").value = "";
                        document.getElementById("txtClientID").value = "";
                        document.getElementById("txtScheme").value = "";
                        document.getElementById("txtRemarks").value = "";
                        document.getElementById("txtLeakage").value = "";
                        document.getElementById("txtCustomerName").disabled = false;
                        document.getElementById("txtLoanDate").disabled = false;
                        //document.getElementById("txtSanctionDt").disabled = false;
                        document.getElementById("txtLoanAmount").disabled = false;
                        document.getElementById("txtClientID").disabled = false;
                        document.getElementById("txtScheme").disabled = false;
                        document.getElementById("txtRemarks").disabled = false;
                        document.getElementById("txtLeakage").disabled = false;
                        document.getElementById("txtClientID").focus();
                    }
                    else {
                        var Dtl = arg.split("µ");
                        document.getElementById("txtCustomerName").value = Dtl[0];
                        document.getElementById("txtLoanDate").value = Dtl[1];
                        // document.getElementById("txtSanctionDt").value = Dtl[3];
                        document.getElementById("txtLoanAmount").value = parseFloat(Dtl[2]).toFixed(2);
                        document.getElementById("txtClientID").value = Dtl[4];
                        document.getElementById("txtScheme").value = Dtl[5];
                        document.getElementById("txtRemarks").value = "";
                        document.getElementById("txtLeakage").value = "";
                        document.getElementById("txtCustomerName").disabled = true;
                        document.getElementById("txtLoanDate").disabled = true;
                        // document.getElementById("txtSanctionDt").disabled = true;
                        document.getElementById("txtLoanAmount").disabled = true;
                        document.getElementById("txtClientID").disabled = true;
                        document.getElementById("txtScheme").disabled = true;
                        document.getElementById("txtRemarks").disabled = false;
                        document.getElementById("txtLeakage").disabled = false;
                        document.getElementById("txtRemarks").focus();
                    }
                    if (document.getElementById("chkFraud").checked != true)
                        document.getElementById("chkSpotClose").disabled = false;
                    else
                        document.getElementById("chkSpotClose").disabled = true;
                    document.getElementById("chkStaff").disabled = false;
                    break;
            default:
            break;
        
            }
            
        }

        function table_fill(arg,Flag) 
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; margin: 0px auto;'  class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr >";          
            tab += "<td style='width:9%;text-align:left'>Dep. No</td>";
            tab += "<td style='width:7%;text-align:left'>Client ID</td>";
            tab += "<td style='width:4%;text-align:left'>Product</td>";     
            tab += "<td style='width:15%;text-align:left'>Customer Name</td>";
            //tab += "<td style='width:6%;text-align:left'>Sanction Dt</td>";
            tab += "<td style='width:12%;text-align:left'>Dep.Date</td>";  
            tab += "<td style='width:4%;text-align:left'>Amount</td>";
           // tab += "<td style='width:5%;text-align:center'>To be Verified</td>";
            tab += "<td style='width:5%;text-align:center'>Serious</td>";
            tab += "<td style='width:5%;text-align:center'>Very Serious</td>";   
            tab += "<td style='width:20%;text-align:left'>Remarks</td>";
            tab += "<td style='width:6%;text-align:left'>Leakage</td>";
            tab += "<td style='width:5%;text-align:left'>Spot Close</td>";
            tab += "<td style='width:5%;text-align:left'>Staff Invlmt.</td>";
            tab += "<td style='width:3%;text-align:center' >Select</td>";       
            tab += "</tr>";      
            tab += "</table></div>";
            tab += "<div id='ScrollDiv' style='width:100%; height:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
           
            if (arg != "") {             
                Flag = document.getElementById("<%= hdnItemType.ClientID %>").value;              
                row = arg.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
                    i = n + 1;
                    //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time                                     
                    tab += "<td style='width:9%;text-align:left'>" + col[1] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[13] + "</td>";                  
                    tab += "<td style='width:15%;text-align:left'>" + col[5].toString().toUpperCase() + "</td>"; 
                   // tab += "<td  align=left style='width:6%;' >" + col[9] + "</td>";  
                    tab += "<td  align=left style='width:12%;' >" + col[7] + "</td>"; 
                    tab += "<td  align=left style='width:4%;text-align:right;' >" + parseFloat(col[8]) + "</td>"; 
//                    if(col[3]=="0" && col[6]==0)                
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkPotential" + i + "' disabled=true onclick='chkPotentialOnClick(" + i + ")' /></td>"; 
//                    else if (col[3]=="0" && col[6]>0)
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkPotential" + i + "' onclick='chkPotentialOnClick(" + i + ")' /></td>"; 
//                    else
//                      tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkPotential" + i + "' checked='checked' onclick='chkPotentialOnClick(" + i + ")' /></td>";   
                    if (Flag == "S")
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "'  disabled=true checked='checked' onclick='chkSuspLeakageOnClick(" + i + ")' /></td>";
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "' disabled=true onclick='chkSuspLeakageOnClick(" + i + ")' /></td>"; 
//                    if(col[11]=="0" && col[6]==0)                
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "' disabled=true onclick='chkSuspLeakageOnClick(" + i + ")' /></td>"; 
//                    else if (col[11]=="0" && col[6]>0)
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "' onclick='chkSuspLeakageOnClick(" + i + ")' /></td>"; 
//                    else
//                      tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSuspLeakage" + i + "' checked='checked' onclick='chkSuspLeakageOnClick(" + i + ")' /></td>";

                    if (Flag == "V")
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "'  disabled=true checked='checked' onclick='chkFraudOnClick(" + i + ")' /></td>";
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "' disabled=true onclick='chkFraudOnClick(" + i + ")' /></td>";
//                    if (document.getElementById("<%= hdnAuditTypeID.ClientID %>").value == "1")
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "' disabled=true onclick='chkFraudOnClick(" + i + ")' /></td>";
//                    else if (col[14] == "0" && col[6] == 0)
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "' disabled=true onclick='chkFraudOnClick(" + i + ")' /></td>";
//                    else if (col[14] == "0" && col[6] > 0)
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "' onclick='chkFraudOnClick(" + i + ")' /></td>";
//                    else
//                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkFraud" + i + "' checked='checked' onclick='chkFraudOnClick(" + i + ")' /></td>";
                   
                    if(col[6]!="0")
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='300' onkeypress='return TextAreaCheck(event)'  onblur='return RemarksOnBlur(" + i + ")'>" + col[4] + "</textarea>";
                    else
                        var txtBox = "<textarea id='txtRemarks" + i + "' name='txtRemarks" + i + "' type='Text' style='width:99%;'  maxlength='300' onkeypress='return TextAreaCheck(event)'  onblur='return RemarksOnBlur(" + i + ")'  disabled=true ></textarea>";
                    tab += "<td style='width:20%;text-align:left'>" + txtBox + "</td>";
                    if (col[3] == "0" && col[6] != "0")
                        var txtBox1 = "<textarea id='txtLeakage" + i + "' name='txtLeakage" + i + "' style='width:99%;' class='NormalText' onkeypress='NumericCheck(event)'  maxlength='6' >" + parseFloat(col[12]).toFixed(0)+"</textarea>";
                    else
                        var txtBox1 = "<textarea id='txtLeakage" + i + "' name='txtLeakage" + i + "'  style='width:99%;' class='NormalText'  disabled=true onkeypress='NumericCheck(event)'  maxlength='6' ></textarea>";
                    tab += "<td style='width:6%;text-align:left'>" + txtBox1 + "</td>";
                    if (col[6] == "0" || Flag == "V" )
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSpotClose" + i + "' disabled=true  /></td>";
                    else if (col[15] == 0)
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSpotClose" + i + "'    checked='checked'  /></td>";
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkSpotClose" + i + "'    /></td>";                   
                    if (col[6] == "0")
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "'   /></td>";
                    else if (col[16] == 1)
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "'   checked='checked'  /></td>";
                    else
                        tab += "<td style='width:5%;text-align:center'><input type='checkbox' id='chkStaff" + i + "'     /></td>";                   
                    tab += "<td style='width:3%;text-align:center' ><img src='../image/cross.png' title='Delete' onclick='return deleteOnClick("+ n +")' style='cursor:pointer' /></td>";
                    tab += "</tr>";
                }

            }
            //----------------------------New Row-----------------------------------

            if (row_bg == 0) {
                row_bg = 1;
                tab += "<tr class='sub_first';>";
            }
            else {
                row_bg = 0;
                tab += "<tr class='sub_second';>";
            }
            tab += "<td style='width:9%;text-align:left'><textarea id='txtLoanNo' type='number' class='NormalText' style='width:99%; resize: none; ' maxlength='15' onpaste='CheckIfNumber(event)' onkeypress='NumericCheck(event)'  onchange='LoanNoOnChange()'></textarea></td>";
//            tab += "<td style='width:9%;text-align:left'><textarea id='txtLoanNo'  class='NormalText' style='width:99%; resize: none; ' maxlength='15' onchange='LoanNoOnChange()' ></textarea></td>";
            tab += "<td style='width:7%;text-align:left'><textarea id='txtClientID'  disabled='true' type='text' style='width:99%; resize: none;' maxlength='15' onkeypress='return TextAreaCheck(event)' ></textarea></td>";
            var Select = CreateSchemeSelect();
            tab += "<td style='width:4%;text-align:left'>" + Select  + "</td>";
            tab += "<td style='width:15%;text-align:left'><textarea id='txtCustomerName'  disabled='true'   style='width:99%; resize: none;' onkeypress='return TextAreaCheck(event)'></textarea></td>";
           // tab += "<td  align=left style='width:6%;' ><textarea id='txtSanctionDt'  disabled='true' style='width:99%; resize: none;' maxlength='10' onkeypress='return NumericCheck(event)' onkeyup='DateField(this)' placeholder='mm/dd/yyyy' ></textarea></td>";
            tab += "<td  align=left style='width:12%;' ><textarea id='txtLoanDate'  disabled='true' style='width:99%; resize: none;'  maxlength='10' onkeypress='return NumericCheck(event)' onkeyup='DateField(this)' placeholder='mm/dd/yyyy'></textarea></td>";
            tab += "<td  align=left style='width:4%;text-align:right;' ><textarea id='txtLoanAmount'  disabled='true' style='width:99%; resize: none;' onkeypress='return NumericCheck(event)' maxlength='8'></textarea></td>";
            if(Flag=="S")
                tab += "<td style='width:5%;text-align:center'><input id='chkSuspLeakage' checked=true ; type='checkbox' disabled='true' onclick='chkSuspLeakageOnClick('')'/></td>";
            else
                tab += "<td style='width:5%;text-align:center'><input id='chkSuspLeakage'  type='checkbox' disabled='true' onclick='chkSuspLeakageOnClick('')'/></td>";
            if (Flag == "V")
                tab += "<td style='width:5%;text-align:center'><input id='chkFraud' checked=true; type='checkbox' disabled='true' onclick='chkFraudOnClick('')'/></td>";
            else
                tab += "<td style='width:5%;text-align:center'><input id='chkFraud'  type='checkbox' disabled='true' onclick='chkFraudOnClick('')'/></td>";
            tab += "<td style='width:20%;text-align:left'><textarea id='txtRemarks'  disabled='true' class='NormalText'  style='width:99%;  ' onkeypress='return TextAreaCheck(event)' ></textarea></td>";
            tab += "<td style='width:6%;text-align:left'><textarea id='txtLeakage'  disabled='true' class='NormalText'  style='width:99%;' onkeypress='NumericCheck(event)' maxlength='6'></textarea></td>";
            tab += "<td style='width:5%;text-align:left'><input id='chkSpotClose'  type='checkbox' disabled='true' style='width:90%; text-align:right;' /></td>";
            tab += "<td style='width:5%;text-align:left'> <input id='chkStaff'  type='checkbox' disabled='true' style='width:90%; text-align:right;' />   </td>";
            tab += "<td style='width:3%;text-align:center' > <div id='Button' onclick='return AddOnClick()' style='width:90%; font-family:Courier New ; font-size:13pt; font-weight:bolder; '><strong>+</strong></div></td>";
            tab += "</tr>";
            tab += "</table></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            document.getElementById("txtLoanNo").focus();
        }
        function deleteOnClick(ID) {
            if (confirm("Are you sure to Delete the record?") == true) {
                var arg = document.getElementById("<%= hdnAuditDetails.ClientID %>").value;
                row = arg.split("¥");
                var NewStr = "";
                for (n = 1; n <= row.length - 1; n++) {
                    if (ID != n)
                        NewStr += "¥" + row[n];
                }
                document.getElementById("<%= hdnAuditDetails.ClientID %>").value = NewStr;
                table_fill(document.getElementById("<%= hdnAuditDetails.ClientID %>").value, document.getElementById("<%= hdnItemType.ClientID %>").value);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
          function GroupOnChange() {
            var GroupID = document.getElementById("<%= cmbGroup.ClientID %>").value;
            var ToData = "2Ø" + GroupID;              
            ToServer(ToData, 2);

        }
        function SubGroupOnChange()
        {
            var SubGroupID = document.getElementById("<%= cmbSubGroup.ClientID %>").value;
            var ToData = "3Ø" + SubGroupID;                  
            ToServer(ToData, 3);
        }

        function ItemOnChange()
            {
                var ItemID = document.getElementById("<%= cmbCheckList.ClientID %>").value;
                if(ItemID!="-1")
                {
                    var ToData = "4Ø" + ItemID + "Ø" + document.getElementById("<%= hdnAuditID.ClientID %>").value;
                    ToServer(ToData, 4);

                }
                else
                {
                    document.getElementById("<%= hdnAuditDetails.ClientID %>").value = "";
                    table_fill(document.getElementById("<%= hdnAuditDetails.ClientID %>").value,"V");
                }
            }
            function NewOnClick()
            {           
                document.getElementById("txtLoanNo").value="";               
                document.getElementById("txtRemarks").value="";
                document.getElementById("txtLeakage").value = "";
                document.getElementById("txtCustomerName").value = "";
                document.getElementById("txtLoanDate").value = "";
                //document.getElementById("txtSanctionDt").value = "";
                document.getElementById("txtLoanAmount").value = "";
                document.getElementById("txtClientID").value = "";
                document.getElementById("txtScheme").value = "";
                document.getElementById("txtCustomerName").disabled = false;
                document.getElementById("txtLoanDate").disabled = false;
                //document.getElementById("txtSanctionDt").disabled = false;
                document.getElementById("txtLoanAmount").disabled = false;
                document.getElementById("txtClientID").disabled = false;
                document.getElementById("txtScheme").disabled = false;
                document.getElementById("txtLoanNo").focus();       
               
            }
            function SelectOnClick(val)
            {     
                if(document.getElementById("chkSelect"+val).checked==true)    
                {    
                    document.getElementById("txtRemarks"+val).disabled =false;
                    document.getElementById("txtLeakage" + val).disabled = false;
                    if (document.getElementById("chkFraud" + val).checked == false)
                        document.getElementById("chkSpotClose" + val).disabled = false;
                }
                 else
                {
                    document.getElementById("txtRemarks"+val).value="";                    
                    document.getElementById("txtRemarks"+val).disabled =true; 
                    document.getElementById("txtLeakage" + val).value = "";
                    document.getElementById("txtLeakage" + val).disabled = true ;
                    document.getElementById("chkSpotClose" + val).disabled = true;
                    document.getElementById("chkStaff" + val).disabled = true;                       
                    document.getElementById("chkFraud" + val).disabled = true;  
                }
            }

        function LoanNoOnChange()
        {
           
            var LoanNo=document.getElementById("txtLoanNo").value;
            document.getElementById("txtCustomerName").value="";
            document.getElementById("txtLoanDate").value="";
           // document.getElementById("txtSanctionDt").value="";
            document.getElementById("txtLoanAmount").value="";
            document.getElementById("txtClientID").value="";
            var row = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");
                for (n = 0; n <= row.length - 1; n++) {
                   var  col = row[n].split("µ");
                    if(LoanNo==col[1])
                    {alert("Dep No Already Assigned"); document.getElementById("txtLoanNo").select();return false;}
                    }
            
            var ToData = "5Ø" + LoanNo;                  
            ToServer(ToData, 5);
        }
        function AddOnClick()
        {
            var customerName = document.getElementById("txtCustomerName").value;
            var Scheme = document.getElementById("txtScheme").value;
            if (Scheme == "")
            { alert("Select Product"); document.getElementById("txtScheme").focus(); return false; }
            var LoanDate = "";
            if (document.getElementById("txtLoanDate").value != "")
                LoanDate = document.getElementById("txtLoanDate").value;
            if (isValidDate(LoanDate) == false)
            { alert("Check Date"); document.getElementById("txtLoanDate").focus(); return false; }
            var SanctionDate = LoanDate;
//            if (document.getElementById("txtSanctionDt").value != "")
//                SanctionDate=document.getElementById("txtSanctionDt").value;
            var LoanAmount = 0;
            if (isNumber(document.getElementById("txtLoanAmount").value) == true)
                LoanAmount = document.getElementById("txtLoanAmount").value; 
            var ClientID=  document.getElementById("txtClientID").value;      
            var PotentialFraud=0;
            var Fraud = 0;
            var SuspLeakage = 0;
            var SpotClose = 1;
            var StaffInvolvement = 0;
            if(document.getElementById("chkSuspLeakage").checked==true)
                SuspLeakage = 1;
            if (document.getElementById("chkFraud").checked == true)
                Fraud = 1;
            if (document.getElementById("chkSpotClose").checked == true)
                SpotClose = 0;
            if (document.getElementById("chkStaff").checked == true)
                StaffInvolvement = 1;
            var Remarks=document.getElementById("txtRemarks").value;
            var FinLeakage = 0;           
            if (document.getElementById("txtLeakage").value != "")
                FinLeakage = document.getElementById("txtLeakage").value;         
            if(document.getElementById("txtLoanNo").value == "")
            { alert("Enter Loan/Deposit No"); document.getElementById("txtLoanNo").focus(); return false; }      
            if(Remarks=="")
            { alert("Enter Remarks"); document.getElementById("txtRemarks").focus(); return false; }
           
            document.getElementById("<%= hdnAuditDetails.ClientID %>").value += "¥" + document.getElementById("<%= hdnAuditID.ClientID %>").value + "µ" + document.getElementById("txtLoanNo").value + "µ" + document.getElementById("<%= cmbCheckList.ClientID %>").value + "µ" + PotentialFraud + "µ" + Remarks + "µ" + customerName + "µ1µ" + LoanDate + "µ" + LoanAmount + "µ" + SanctionDate + "µ" + ClientID + "µ" + SuspLeakage + "µ" + FinLeakage + "µ" + Scheme + "µ" + Fraud + "µ" + SpotClose + "µ" + StaffInvolvement;
            table_fill(document.getElementById("<%= hdnAuditDetails.ClientID %>").value, "V");
          
           
        }
        function RemarksOnBlur(val)
        {           
            var textObject=document.getElementById("txtRemarks"+val);          
            if(textObject.value == "") 
            { 
                alert("Enter Remarks");
                var xxx="txtRemarks"+ val +".focus();"                
                setTimeout(xxx ,1);                
                return false;
            }   
        }
        function chkSuspLeakageOnClick(val)
        {
            document.getElementById("chkFraud" + val).checked = false;
            document.getElementById("txtLeakage" + val).disabled = false;
            if (document.getElementById("chkSuspLeakage" + val).checked == true) 
                document.getElementById("chkStaff" + val).disabled = false;
            else {
                document.getElementById("chkSpotClose" + val).disabled = false;
                document.getElementById("chkSpotClose" + val).checked = false;
                document.getElementById("chkStaff" + val).disabled = true;
                document.getElementById("chkStaff" + val).checked = false;
            }
            document.getElementById("chkFraud" + val).focus();
        }
        function chkFraudOnClick(val) {
            document.getElementById("chkSuspLeakage" + val).checked = false;
            document.getElementById("txtLeakage" + val).disabled = false;
            if (document.getElementById("chkFraud" + val).checked == true) {
                document.getElementById("chkSpotClose" + val).disabled = true;
                document.getElementById("chkSpotClose" + val).checked = false;
                document.getElementById("chkStaff" + val).disabled = false;
            }
            else {
                document.getElementById("chkSpotClose" + val).disabled = false;
                document.getElementById("chkSpotClose" + val).checked = false;
                document.getElementById("chkStaff" + val).disabled = true;
                document.getElementById("chkStaff" + val).checked = false;
            }
            document.getElementById("txtRemarks" + val).focus();
        }
        function btnSave_onclick() {
         var ItemID = document.getElementById("<%= cmbCheckList.ClientID %>").value;
         if (ItemID != -1) {
             var Scheme = document.getElementById("txtScheme").value;
             var LoanNo = document.getElementById("txtLoanNo").value;
             var remarks = document.getElementById("txtRemarks").value;
             if (Scheme != "" && LoanNo != "" && remarks != "")
             { var xxx = AddOnClick(); if (xxx == false) return false; }
         }
        var AuditID = document.getElementById("<%= hdnAuditID.ClientID %>").value;
        var GenComments="";
        var SerComments = "";
        var ManagementNote = ""       
        ManagementNote = document.getElementById("<%= txtManagementNote.ClientID %>").value;        
        if(ItemID!=-1)
        { var n = updateValue(); if (n == false) return false; }
        else
        {document.getElementById("<%= hdnValue.ClientID %>").value=""}
      
        var CloseAudit=(document.getElementById("chkCloseAudit").checked==true) ? 1 :0;
        if (CloseAudit == 1) {
//        if (document.getElementById("chkAccept").checked == false) {
//            alert("Please accept the declaration."); document.getElementById("chkAccept").focus(); return false;
//        }
        if(confirm("Are you sure to close this audit?")==0) {return false;} else {
         CloseFlag = 1;
         GenComments=document.getElementById("txtGeneralComments").value;
         SerComments = document.getElementById("txtSeriousComments").value;        
         if(GenComments=="")
         {alert("Enter Comments");document.getElementById("txtGeneralComments").focus();return false;}
         if(SerComments=="")
         {if(confirm("Are you sure there is no serious issues in this audit?")==0){document.getElementById("txtSeriousComments").focus();return false;}}
         }
         }
        if (ItemID > 0 && document.getElementById("<%= hdnValue.ClientID %>").value == "")
        {
          if(confirm("Are you sure to save the check list as error free?")==0) return false;}
          var ToData = "1Ø" + AuditID + "Ø" + ItemID + "Ø" + document.getElementById("<%= hdnValue.ClientID %>").value + "Ø" + CloseAudit + "Ø" + GenComments + "Ø" + SerComments + "Ø" + ManagementNote;                  
          ToServer(ToData, 1);
         }
         

    function  updateValue()
    {
        document.getElementById("<%= hdnValue.ClientID %>").value="";
        var i=0;
        var  row = document.getElementById("<%= hdnAuditDetails.ClientID %>").value.split("¥");
        for (n = 1; n <= row.length - 1; n++) {
            col = row[n].split("µ");
            i = n + 1;
            var StaffInv = (document.getElementById("chkStaff" + i).checked == true) ? 1 : 0;
            var LeakageAmount = Math.abs(document.getElementById("txtLeakage" + i).value);
            if (LeakageAmount > 0)
                StaffInv = 1;
            document.getElementById("<%= hdnValue.ClientID %>").value += "¥" + col[1] + "µ0µ" + document.getElementById("txtRemarks" + i).value + "µ" + col[5] + "µ" + col[7] + "µ" + col[8] + "µ" + col[9] + "µ" + col[10] + "µ" + col[11] + "µ" + LeakageAmount + "µ" + col[13] + "µ" + col[14] + "µ" + ((document.getElementById("chkSpotClose" + i).checked == true) ? 0 : 1) + "µ" + StaffInv;
          
        }
    }
    function viewReport()
    {
       window.open("Reports/viewAuditReport.aspx?AuditID="+btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value)+" &AuditTypeID="+ document.getElementById("<%= hdnAuditTypeID.ClientID %>").value+" &From=1","_self"); 
       return false;
    }
    function closeOnClick()
    {
        if (document.getElementById("chkCloseAudit").checked == true) { 
            var Comments = document.getElementById("<%= hdnComments.ClientID %>").value.split("Ø");
            document.getElementById("GenComm").style.display = ""; document.getElementById("SerComm").style.display = "";           
            document.getElementById("txtGeneralComments").value = Comments[0];
            document.getElementById("txtSeriousComments").value = Comments[1];
//            document.getElementById("Decl1").style.display = "";
//            document.getElementById("Decl2").style.display = "";
//            var Declaration = ""
//            if (document.getElementById("<%= hdnAuditTypeID.ClientID %>").value == "1") {
//                var Dtl = document.getElementById("<%= hdnCenterCount.ClientID %>").value.split("~");
//                Declaration = "I hereby declare that all the disbursements during the audit period (<span style='color:red;'><b>" + Dtl[0] + " centers </b></span>) has been checked. "
//                if (Dtl[1] > 0) {
//                    Declaration += "</br> Also checked disbursements of <span style='color:red;'><b>" + Dtl[1] + "</b></span>  centers.";
//                }
//            }
//            else {
//                var Dtl = document.getElementById("<%= hdnCenterCount.ClientID %>").value.split("~");
//                Declaration = "I hereby declare that <span style='color:red;'><b>" + Dtl[0] + "/" + Dtl[2] + "</b></span> new centers and <span style='color:red;'><b>" + Dtl[1] + "/" + Dtl[3] + "</b></span> exisitng centers were visited during this audit.";
//            }
//            Declaration += "&nbsp;&nbsp;&nbsp;<input id='chkAccept' type='checkbox'/>&nbsp;Accept"
//            document.getElementById("<%= pnDeclaration.ClientID %>").innerHTML = Declaration;
        }
        else {
            document.getElementById("GenComm").style.display = "none"; document.getElementById("SerComm").style.display = "none";
            document.getElementById("Decl1").style.display = "none";
            document.getElementById("Decl2").style.display = "none";
        }
    }

    function DateField(control)
    {
        var v = control.value;
        if (v.match(/^\d{2}$/) !== null) {
            control.value = v + '/';
        } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
            control.value = v + '/';
        }
    }
    function CreateSchemeSelect() {       
            var select1 = "<select id='txtScheme' class='NormalText' name='txtScheme' style='width:100%;height:50px;' disabled=true;   >";      
            var rows = document.getElementById("<%= hdnScheme.ClientID %>").value.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");              
                    select1 += "<option value='" + cols[0] + "' >" + cols[1] + "</option>";
            }
        
        select1 += "</select>"
        return select1;
    }

    function isValidDate(value) {
        var dateWrapper = new Date(value);
        return !isNaN(dateWrapper.getDate());
    }



    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    </script>
   </br>
    <table  align="center" style="width: 95%;text-align:right; margin:0px auto;">
        <tr>
            <td style="width:20%; font-weight:bold;" class="mainhead" colspan="2">
                &nbsp;BRANCH: &nbsp;<asp:Label ID="lblBranch" runat="server"></asp:Label></td>
           
            <td style="width:20%; font-weight:bold;" class="mainhead">
                &nbsp;START ON:&nbsp;<asp:Label ID="lblStartDate" runat="server"></asp:Label></td>
            <td style="width:20%; font-weight:bold;" class="mainhead">
                &nbsp;
            <asp:Label ID="lblAuditType" 
                    runat="server"></asp:Label>
            </td>
            <td style="width:20%; text-align:center; font-weight:bold;" class="mainhead">                
                <img id="ViewReport" src="../Image/ViewReport.png" onclick="viewReport()" title="View Report" style="height:20px; width:20px; cursor:pointer;"/>
            </td>
        </tr>
      
        <tr>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%">
               Main Category</td>
            <td style="width:60%" colspan="3">
              &nbsp;<asp:DropDownList ID="cmbGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                </asp:DropDownList> </td>
        </tr>
        <tr>
            <td style="width:20%">
              </td>
            <td style="width:20%" >
                Sub Category 
            </td>
            <td style="width:60%" colspan="3">
                &nbsp;<asp:DropDownList ID="cmbSubGroup" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:20%">
               </td>
            <td style="width:20%" >
               Check List
            </td>
            <td style="width:60%" colspan="3">
                &nbsp;<asp:DropDownList ID="cmbCheckList" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
      
        <tr>
            <td style="width:20%;text-align:center;" colspan="5">
                <asp:Panel ID="pnDisplay" runat="server">
                </asp:Panel>
            </td>
            
        </tr>
      
          
        <br />
         <tr>
            <td style="width:20%;text-align:center;"colspan="5">
                <input id="chkCloseAudit" title="Close Audit" type="checkbox"  onclick="closeOnClick()" 
                    /> Close Audit</td>            
        </tr>
          <tr id="GenComm" style="display:none;">
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                General Comments</td>
            <td style="width:60%" colspan="3">
                <textarea id="txtGeneralComments" cols="20" name="S1" rows="2" maxlength="1000" onkeypress='return TextAreaCheck(event)'
                    style="width: 95%"></textarea></td>
        </tr>
          <tr id="SerComm" style="display:none;">
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                Serious Comments</td>
            <td style="width:60%" colspan="3">
                 <textarea id="txtSeriousComments" cols="20" name="S1" rows="2"  maxlength="1000" onkeypress='return TextAreaCheck(event)'
                    style="width: 95%"></textarea></td>
        </tr>
          <tr >
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:20%" >
                Management Note</td>
            <td style="width:60%" colspan="3">
                 <asp:TextBox ID="txtManagementNote" runat="server" Rows="5" 
                     TextMode="MultiLine" onkeypress='return TextAreaCheck(event)'  Width="95%" maxlength=5000></asp:TextBox>
              </td>
        </tr>
            <tr id="Decl1" style="display:none;">
            <td style="width:20%; text-align:center;" colspan="5" class="mainhead">
               DECLARATION</td>                    
        </tr>
        <tr id="Decl2" style="display:none;" >
            <td style="width:20%; text-align:left; margin-left:0px;"  colspan="5" >
                <asp:Panel ID="pnDeclaration" runat="server">
                </asp:Panel>              
                </td>        
        </tr>
      
         <tr>
            <td style="width:20%;text-align:center;"colspan="5">
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" /> &nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>            
        </tr>
        <tr>
            <td style="width:20%">
                <asp:HiddenField ID="hdnSangamVisitCnt" runat="server" />
            </td>
            <td style="width:80%" colspan="4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">
                <asp:HiddenField ID="hdnAuditID" runat="server" />
            </td>
            <td colspan="4" class="style2">
                <asp:HiddenField ID="hdnAuditDetails" runat="server" />
                </td>
        </tr>
        <tr>
            <td style="width:20%">
                <asp:HiddenField ID="hdnAuditTypeID" runat="server" />
            </td>
            <td style="width:80%" colspan="4">
                <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:HiddenField ID="hdnScheme" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:20%">
                <asp:HiddenField ID="hdnCenterCount" runat="server" />
                <asp:HiddenField ID="hdnItemType" runat="server" />
                </td>
            <td style="width:80%" colspan="4">
                <asp:HiddenField ID="hdnComments" runat="server" />
            </td>
        </tr>
    </table>
</head>
</html>
    </table>
    </table>
    </asp:Content>

