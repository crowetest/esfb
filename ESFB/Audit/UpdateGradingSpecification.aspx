﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateGradingSpecification.aspx.vb" Inherits="Audit_UpdateGradingSpecification" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
    </style>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:5%;text-align:center' >#</td>";    
            tab += "<td style='width:25%;text-align:left' >Grading Factor</td>";         
            tab += "<td style='width:40%;text-align:left' >Description</td>";
            tab += "<td style='width:10%;text-align:center'>Based On</td>";     
            tab += "<td style='width:10%;text-align:center'>Limit Type</td>";
            tab += "<td style='width:5%;text-align:center'>Limit</td>";  
                    
            tab += "<td style='width:5%;text-align:right'>Marks</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
            
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
          
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var TotalMarks=0;
                var Category="";
                var TotalFlag=0;
                for (n = 0; n <= row.length - 1; n++) {
                   col = row[n].split("µ");                 
                   if(col[7]==5 && TotalFlag==0)
                   {                     
                        tab += "<tr class='mainhead'; style='text-align:center; height:21px; padding-left:20px;'>";                 
                        tab += "<td style='width:5%;text-align:center' ></td>";
                        tab += "<td style='width:25%;text-align:left' >TOTAL</td>";
                        tab += "<td style='width:40%;text-align:left'></td>"; 
                        tab += "<td style='width:10%;text-align:left'></td>";  
                        tab += "<td style='width:5%;text-align:left'></td>"; 
                   //  var txtBox = "<textarea id='txtMarksTotal' name='txtMarksTotal' style='width:99%; text-align:right; font-weight:bold; color:black; resize: none;' rows=1 maxlength='3' disabled=true >"+TotalMarks+"</textarea>";                 
                        tab += "<td id='txtMarksTotal' style='width:5%;text-align:right; font-weight:bold'>"+ TotalMarks +"</td>";  
                        tab += "</tr>";
                        TotalFlag=1;
                   }
                    if(Category!=col[4])
                    {
                     
                         tab += "<tr class='sub_second'; style='text-align:center; min-height:30px; padding-left:20px;'>";
                         tab += "<td style='width:100%;text-align:left; font-size:12pt; font-family:cambria;' colspan=7 >" + col[4]  + "</td></tr>";
                    }
                    Category=col[4];
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; min-height:50px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_first'; style='text-align:center; min-height:50px; padding-left:20px;'>";
                    }
                    i = n + 1;                                      
                    tab += "<td style='width:5%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:25%;text-align:left'>" + col[1] + "</td>"; 
                    
                    if(col[0]==12)
                    {
                     tab += "<td style='width:40%;text-align:left' colspan='5'>" + col[2] + "</td>";
                    }
                    else
                    {
                    tab += "<td style='width:40%;text-align:left'>" + col[2] + "</td>"; 
                     var select1 = "<select id='cmbBasedOn" + col[0] + "' class='NormalText' name='cmbBasedOn" + col[0] + "' style='width:100%' >";
                    if(col[8]==1)
                    {
                        select1 += "<option value='1' selected=true>Observation</option>";
                        select1 += "<option value='2'>Sample</option>";
                        select1 += "<option value='3'>Sample+</option>";
                        select1 += "<option value='4'>Check List</option>";
                        LImitValue=col[5];
                    }
                    else
                    {
                        select1 += "<option value='1' >Observation</option>";
                        if(col[8]==2)
                        {
                        select1 += "<option value='2' selected=true>Sample</option>";
                        select1 += "<option value='3'>Sample+</option>";
                        select1 += "<option value='4'>Check List</option>";
                        }
                        else if (col[8]==3) {
                            select1 += "<option value='2'>Sample</option>";
                            select1 += "<option value='3' selected=true >Sample+</option>";
                        select1 += "<option value='4'>Check List</option>";
                        }
                        else {
                            select1 += "<option value='2'>Sample</option>";
                            select1 += "<option value='3'>Sample+</option>";
                        select1 += "<option value='4' selected=true>Check List</option>";
                        }
                        LImitValue=col[6];
                    }                                                          
                    tab += "<td style='width:10%;text-align:center'>" + select1 + "</td>";  
                    var LImitValue=0;
                    var select1 = "<select id='cmbLimitType" + col[0] + "' class='NormalText' name='cmbLimitType" + col[0] + "' style='width:100%' >";
                    if(col[5]>1)
                    {
                        select1 += "<option value='1' selected=true>PERCENTAGE</option>";
                        select1 += "<option value='2'>NUMBER</option>";
                        LImitValue=col[5];
                    }
                    else
                    {
                        select1 += "<option value='1' >Percentage</option>";
                        select1 += "<option value='2' selected=true>Number</option>";
                        LImitValue=col[6];
                    }  
                    tab += "<td style='width:10%;text-align:center'>" + select1 + "</td>";
                    var txtBox1 = "<textarea id='txtPerc" + col[0] + "' name='txtPerc" + col[0]  + "'  style='width:99%; text-align:right;resize: none; border:none;'  maxlength='3' onkeypress='NumericCheck(event)'  onkeyup='return LimitOnKeyUp(this.id,"+ col[0] +");'   >"+LImitValue+"</textarea>";                 
                    tab += "<td style='width:5%;text-align:center'>"+ txtBox1 +"</td>";
                                    
                    var txtBox = "<textarea id='txtMarks" + col[0] + "' name='txtMarks" + col[0]  + "'  style='width:99%; text-align:right;resize: none; border:none;'  maxlength='3' onkeypress='NumericCheck(event)' onkeyup='return CalcTotal(this.id);'  >"+col[3]+"</textarea>";                 
                    tab += "<td style='width:5%;text-align:center'>"+ txtBox +"</td>"; 
                    } 
                    tab += "</tr>";
                    if(col[7]<5)
                        TotalMarks+=parseFloat(col[3]);
                }               
                
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }

        function CalcTotal(ID)
        {
            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var TotalMarks=0;          
                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if(col[7]<5)
                    {
                        var Mark=0;
                        if(document.getElementById("txtMarks" + col[0]).value!="")
                            Mark=document.getElementById("txtMarks" + col[0]).value;
                    
                        TotalMarks+=parseFloat(Mark);
                    }
                    if(TotalMarks>100)
                    {
                        alert("Total Mark should not exceed 100");document.getElementById(ID).value="";document.getElementById(ID).focus();return false;
                    }
                }   
                document.getElementById("txtMarksTotal").innerHTML= TotalMarks;          
        }
         function LimitOnKeyUp(ID,RowNumber)
        {
            var row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            var TotalMarks=parseFloat(document.getElementById(ID).value);       
            var LimitType=document.getElementById("cmbLimitType"+RowNumber).value;
            if(TotalMarks>100 && LimitType==1)
            {
                alert("%Limit should not exceed 100");document.getElementById(ID).value="";document.getElementById(ID).focus();return false;
            }  
            if(TotalMarks==0)
            {
                alert("Limit should not be Zero");document.getElementById(ID).value="";document.getElementById(ID).focus();return false;
            }                
               
        }
       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btSave_onclick() {
             var TotalMarks=parseFloat(document.getElementById("txtMarksTotal").innerHTML);             
             if(TotalMarks!="100")
             {
                alert("Total Marks should be 100");return false;
             }
             document.getElementById("<%= hid_value.ClientID %>").value="";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
            col = row[n].split("µ");   
            if(col[0]==12){ 
                var LimitType= 1;
                var BasedOn= 1;
            }
            else{
                var LimitType= document.getElementById("cmbLimitType"+col[0]).value;    
                var BasedOn= document.getElementById("cmbBasedOn"+col[0]).value; 
            }
            var NumberLimit=0;
            var PercLimit=0; 
            
            if(LimitType==1)    
            {   if(col[0]!=12){ 
               
                    PercLimit= document.getElementById("txtPerc"+col[0]).value;
                    if(PercLimit==0)
                    {
                        alert("Enter Limit");document.getElementById("txtPerc"+col[0]).focus();return false;
                    }
                }
                
            }  
            else
            { 
              NumberLimit= document.getElementById("txtPerc"+col[0]).value;
                if(NumberLimit==0)
                {
                    alert("Enter Limit");document.getElementById("txtPerc"+col[0]).focus();return false;
                }
            }  
                         
            if(col[0]==12){
            document.getElementById("<%= hid_value.ClientID %>").value+="¥"+ col[0]+"µ"+ 0 +"µ"+ PercLimit +"µ"+ 0 +"µ"+BasedOn;
            }
            else{
            document.getElementById("<%= hid_value.ClientID %>").value+="¥"+ col[0]+"µ"+ document.getElementById("txtMarks"+col[0]).value+"µ"+ PercLimit + "µ" + NumberLimit+ "µ" +BasedOn;
            }
            
           
            }
          
         var ToData = "1Ø" + document.getElementById("<%= hid_value.ClientID %>").value;                  
         ToServer(ToData, 1);
        }

        function FromServer(arg, context) {
         switch (context) {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {
                    if(document.getElementById("<%= hid_RptID.ClientID %>").value==1)
                        window.open("UpdateGradingSpecification.aspx","_self");
                    else
                      window.open("AuditGrading.aspx","_self");
                    }
                    break;
                    }
               }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
                <asp:HiddenField ID="hid_RptID" runat="server" />
<br />

    <table  align="center" style="width: 90%;text-align:right; margin:0px auto;">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server" style="width:100%; text-align:center; margin:opx auto; ">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <br />               
                <input id="btSave" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="UPDATE"  onclick="return btSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
            
    </table>   
    <br />  
</asp:Content>

