﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AuditCheckList
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim AUD As New Audit
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub AuditCheckList_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 46) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            Me.Master.subtitle = "Audit Check List Preparation"
            DT = AUD.Get_AuditTypes()
            GF.ComboFill(cmbAuditType, DT, 0, 1)
            Me.cmbAuditType.Attributes.Add("onchange", "return RequestOnchange()")
            Me.cmbGroup.Attributes.Add("onchange", "return GroupOnchange()")
            Me.cmbSubGroup.Attributes.Add("onchange", "return SubGroupOnchange()")
            hid_tab.Value = CStr(1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))

        Dim UserID As Integer = CInt(Session("UserID"))

        If RequestID = 1 Then
            Dim UpdateID As Integer = CInt(Data(1))
            Dim Datas() As String = CStr(Data(2)).Split(CChar("~"))
            Dim TabID As Integer = CInt(Data(3))
            Dim AuditType As Integer = CInt(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@UpdateID", SqlDbType.Int)
                Params(0).Value = UpdateID
                Params(1) = New SqlParameter("@Data", SqlDbType.VarChar, 5000)
                Params(1).Value = Datas(0)
                Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                Params(2).Value = TabID
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@AuditTypeID", SqlDbType.Int)
                Params(5).Value = AuditType
                Params(6) = New SqlParameter("@SPM", SqlDbType.Int)
                Params(6).Value = Datas(1)
                DB.ExecuteNonQuery("SP_AUDIT_CHECKLIST", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = "1~" + ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 2 Then
            If CInt(Data(2)) = 2 Or CInt(Data(2)) = 3 Then
                Dim strVal1 As String = ""
                Dim strauditval As Array = Split(Data(1), ",", 1)
                DT = AUD.Get_CheckList_AuditGroup(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    strVal1 += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next
                CallBackReturn = "1~" + strVal1

            Else
                Dim StrData As String
                DT = AUD.Get_Exist_AuditGroup(CInt(Data(1)))
                StrData = AUD.Get_Exist_AuditGroupStringNew(DT)

                If DT.Rows.Count > 0 Then
                    CallBackReturn = "2~" + StrData
                Else
                    CallBackReturn = "2~"
                End If


            End If
        ElseIf RequestID = 3 Then
            If CInt(Data(2)) = 3 Then
                Dim strVal As String = ""
                DT = AUD.Get_CheckList_AuditSubGroup(CInt(Data(1)))
                For Each DR As DataRow In DT.Rows
                    strVal += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()

                Next
                CallBackReturn = "1~" + strVal

            Else
                Dim StrData As String
                DT = AUD.Get_Exist_AuditSubGroup(CInt(Data(1)))
                StrData = AUD.Get_Exist_AuditGroupString(DT)

                If DT.Rows.Count > 0 Then
                    CallBackReturn = "2~" + StrData
                Else
                    CallBackReturn = "2~"
                End If
            End If
        ElseIf RequestID = 4 Then
            Dim StrData As String
            DT = AUD.Get_Exist_AuditCheckList(CInt(Data(1)))
            StrData = AUD.Get_Exist_AuditGroupString(DT)

            If DT.Rows.Count > 0 Then
                CallBackReturn = "2~" + StrData
            Else
                CallBackReturn = "2~"
            End If
        End If



    End Sub
End Class
