﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditStaffInvolvementRefund.aspx.vb" Inherits="AuditStaffInvolvementRefund" EnableEventValidation="false"  %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
        function EmployeeOnchange(){
            var EmpCode = document.getElementById("<%= cmbEmployee.ClientID %>").value;
            if(EmpCode == -1)
            {
                alert("Select Employee");
                document.getElementById("<%= cmbEmployee.ClientID %>").focus();
                return false;
            }
            else
            {
                var ToData = "1Ø" + EmpCode;
	            ToServer(ToData, 1);
            }
        }

        function FromServer(arg, context) {
            switch (context) 
            {
                 case 1:   
                 {
                    document.getElementById("<%= hdnView.ClientID %>").value = arg;
                    FillData();
                    document.getElementById("rowRemark").style.display = "";
                    break;
                 } 
                 case 2:
                 {
                    var Data = arg.split("Ø");
                    alert(Data[1]);
                    if (Data[0] == 0) {
                        window.open("AuditStaffInvolvementRefund.aspx", "_self");
                    }
                    break;
                 }           
            }
        }

        function FillData()
        {
            document.getElementById("<%= pnlView.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:300px; overflow-y: scroll; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            //tab += "<div style='width:100%; overflow:scroll; height:275px; text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr style='height:30px;'>";           
            tab += "<td style='width:3%;text-align:center'>SINo</td>";
            tab += "<td style='width:10%;text-align:center'>Ref No</td>";
            tab += "<td style='width:20%;text-align:left'>Remarks</td>";     
            tab += "<td style='width:19%;text-align:left'>CheckList</td>"; 
            tab += "<td style='width:9%;text-align:center'>Leakage Amt</td>";          
            tab += "<td style='width:9%;text-align:center'>Collected Amt</td>";
            tab += "<td style='width:9%;text-align:center'>Balance</td>";
            tab += "<td style='width:9%;text-align:center'>Coll. Amt</td>";
            tab += "<td style='width:9%;text-align:center'>Coll. Type</td>";
            tab += "<td style='width:3%;text-align:center'><input type='checkbox' id='chkSelectAll' onclick='SelectOnClick(-1)' /></td>";
            tab += "</tr>";  
            var row;
            if (document.getElementById("<%= hdnView.ClientID %>").value != "") {
                row = document.getElementById("<%= hdnView.ClientID %>").value.split("¥");
                for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class='sub_first';>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second';>";
                    }
                    tab += "<td style='width:3%;text-align:center' >" + n + "</td>";
                    tab += "<td style='width:10%;text-align:center' >" + col[1] + "</td>";
                    tab += "<td style='width:20%;text-align:left'>" + col[2] + "</td>";                   
                    tab += "<td style='width:19%;text-align:left'>" + col[3] + "</td>"; 
                    tab += "<td style='width:9%;text-align:center'>" + col[4] + "</td>"; 
                    tab += "<td style='width:9%;text-align:center'>" + col[5] + "</td>"; 
                    var Balance = parseFloat(col[4])-parseFloat(col[5]);
                    tab += "<td style='width:9%;text-align:center'>" + Balance + "</td>"; 
                    tab += "<td style='width:9%;text-align:center'><input type='text' id = 'txtAmount" + n + "' value=" + Balance + " style='width:100%;border:0;background-color:transparent;text-align:center;' disabled=true ></input></td>"; 
                    var select1 = "<select id='cmbType" + n + "' class='NormalText' name='cmbType" + n + "' style='width:100%;text-align:left;border:0;background-color:transparent;' disabled=true>";
                    var Type = document.getElementById("<%= hdnType.ClientID %>").value.split("~");
                    for (j = 0; j <= Type.length - 1; j++) {
                        var Pcols = Type[j].split("!");
                        select1 += "<option value='" + Pcols[0] + "' >" + Pcols[1] + "</option>";
                    }
                    tab += "<td style='width:9%;text-align:center'>" + select1 + "</td>"; 
                    tab += "<td style='width:3%;text-align:center'><input id='chkSelect" + n + "' type='checkbox'  style='width:5%;' onclick='SelectOnClick(" + n + ")'></td>";
                    tab += "</tr>"; 
                }
                
            }
            tab += "</table></div>";            
            document.getElementById("<%= pnlView.ClientID %>").innerHTML = tab;
        }

        function SelectOnClick(ID) {
            if (ID < 0) {
                var Data = document.getElementById("<%= hdnView.ClientID %>").value;
                var row = Data.split("¥");
                var i = 0;
                for (n = 1; n <= row.length - 1; n++) {
                    i = n;
                    var col = row[n].split("µ");
                    if (document.getElementById("chkSelectAll").checked == true) {
                        document.getElementById("chkSelect" + i).checked = true;
                        document.getElementById("cmbType" + i).disabled = false;
                        document.getElementById("txtAmount" + i).disabled = false
                    }
                    else {
                        document.getElementById("chkSelect" + i).checked = false;
                        document.getElementById("cmbType" + i).disabled = true;
                        document.getElementById("txtAmount" + i).disabled = true;
                    }
                }
            }
            else
            {
                document.getElementById("chkSelectAll").checked = false;
                if (document.getElementById("chkSelect" + ID).checked == true) {
                    document.getElementById("cmbType" + ID).disabled = false;
                    document.getElementById("txtAmount" + ID).disabled = false;
                }
                else{
                    document.getElementById("cmbType" + ID).disabled = true;
                    document.getElementById("txtAmount" + ID).disabled = true;
                }    
            }
        }

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

       function btnSave_onclick() {    
            var EmpCode = document.getElementById("<%= cmbEmployee.ClientID %>").value;
            if(EmpCode == -1)
            {
                alert("Select Employee");
                document.getElementById("<%= cmbEmployee.ClientID %>").focus();
                return false;
            }
            var Data = document.getElementById("<%= hdnView.ClientID %>").value;
            var row = Data.split("¥");
            document.getElementById("<%= hdnData.ClientID %>").value = "";
            if (Data != "") {
                for (n = 1; n <= row.length - 1; n++) {
                    var col = row[n].split("µ");
                    if (document.getElementById("chkSelect" + n).checked == true) {
                        var Amount = document.getElementById("txtAmount" + n).value;
                        var Type   = document.getElementById("cmbType" + n).value;
                        var Balance = parseFloat(col[4])-parseFloat(col[5]);
                        if (Amount == 0 || Amount == "")
                        { alert("Enter Amount"); document.getElementById("txtAmount" + n).focus(); return false; }
                        if (parseFloat(Amount)>parseFloat(Balance))
                        { alert("Amount Must be Less than or Equal to " + Balance); document.getElementById("txtAmount" + n).focus(); return false; }
                        else
                        {
                            document.getElementById("<%= hdnData.ClientID %>").value += "¥" + col[0] + "µ" + Amount + "µ" + Type; 
                        }
                    }
                }
            }
            var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
            if (Remarks == "")
            {
                alert("Enter Remarks");
                document.getElementById("<%= txtRemarks.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= hdnData.ClientID %>").value == "")
            { alert("Select Details"); return false; }
            else {
                var ToData = "2Ø" + EmpCode + "Ø" + document.getElementById("<%= hdnData.ClientID %>").value  + "Ø" +  Remarks;
                ToServer(ToData, 2);
            }
       }

    </script>
</head>
</html>
<br />

    <table style="width:100%">
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Employee&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                <asp:DropDownList ID="cmbEmployee" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="61%" ForeColor="Black"> <asp:ListItem Value="-1"> ------Select------</asp:ListItem>
                </asp:DropDownList></td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                &nbsp;</td>
            <td style="text-align:left; width:50%">
                </td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
      
          <tr>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; " colspan="2">
                <asp:Panel ID="pnlView" runat="server">
                </asp:Panel>
              </td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
         <tr >
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                &nbsp;</td>
            <td style="text-align:left; width:50%">
                        &nbsp;</td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
      
         <tr >
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                &nbsp;</td>
            <td style="text-align:left; width:50%">
                        &nbsp;</td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
      
         <tr id="rowRemark" style="display:none;">
            <td style="text-align:left; width:15%">
                &nbsp;</td>
            <td style="text-align:right; width:20%">
                Remarks&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="text-align:left; width:50%">
                        <asp:TextBox ID="txtRemarks" runat="server" class="NormalText"  Width="62%" 
                            Font-Names="Cambria" Height="53px" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                </td>
            <td style="text-align:left; width:15%">
                &nbsp;</td>
        </tr>
      
        <tr>
            <td style="text-align:center;" colspan="4">
                <br />
                &nbsp;<input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="SAVE"   onclick="return btnSave_onclick()" 
                    />
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    <asp:HiddenField ID="hdnView" runat="server" />
                    <asp:HiddenField ID="hdnData" runat="server" />
                    <asp:HiddenField ID="hdnType" runat="server" />
            </td>
        </tr>
    </table>   

</asp:Content>

