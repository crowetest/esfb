﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditStart.aspx.vb" Inherits="Audit_AuditStart" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
<link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
<script src="../Script/Validations.js" type="text/javascript"></script>
<style type="text/css">
    #Button
    {
        width:80%;
        height:20px;
        font-weight:bold;
        line-height:20px;
        text-align:center;
        border-top-left-radius: 25px;
	    border-top-right-radius: 25px;
	    border-bottom-left-radius: 25px;
	    border-bottom-right-radius: 25px;
        cursor:pointer;
        background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
        color:#E0E0E0;
    }
        
    #Button:hover
    {
        background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        color:#036;
    }        
</style>
<script language="javascript" type="text/javascript">
function FromServer(arg, context) 
{
    switch (context) 
    {
        case 1:   
            var Dtl       = arg.split("Ø");
            var AuditID   = Dtl[0];
            var AuditType = Dtl[1];
            var TypeID    = Dtl[2];
            if (AuditType == 1) window.open("Audit.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");            /* Banking Audit*/
            if (AuditType == 2){
            if(TypeID==1)   /* Micro Finance Audit */                
                window.open("LoanWiseAudit.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");
             else          
                window.open("Audit_Micro.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");      
            }    
            if (AuditType == 3) window.open("GeneralAuditNew.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");  /* General Audit*/
            break;
        default:
            break;
    }
}
function ViewReport(AuditID,AuditType)
{
    window.open("Reports/viewAuditSamplingReport.aspx?AuditID="+ btoa(AuditID) +" &AuditTypeID="+ AuditType,"_self"); 
    return false;
}
function table_fill() 
{
    document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
    var row_bg = 0;
    var tab = "";
    tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
    tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
    tab += "<tr >";           
    tab += "<td style='width:20%;text-align:left' >Branch</td>";
    tab += "<td style='width:10%;text-align:left' >Month</td>";
    tab += "<td style='width:20%;text-align:left'>Type</td>";     
    tab += "<td style='width:10%;text-align:center'>Status</td>"; 
    //  tab += "<td style='width:10%;text-align:center'>Sample List</td>";          
     tab += "<td style='width:20%;text-align:center'></td>";
    tab += "<td style='width20%;text-align:center'></td>";
    tab += "</tr>";           
    if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") 
    {
        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
        for (n = 0; n <= row.length - 1; n++) 
        {
            col = row[n].split("µ");
            if (row_bg == 0) 
            {
                row_bg = 1;
                tab += "<tr class='sub_first'; style='height:25px;text-align:center; padding-left:20px;'>";
            }
            else 
            {
                row_bg = 0;
                tab += "<tr class='sub_second'; style='height:25px;text-align:center; padding-left:20px;'>";
            }
            i = n + 1;
            //EA.Emp_Code,EM.Emp_Name,EA.Tra_Dt,EA.M_Time,EA.E_time                    
            tab += "<td style='width:20%;text-align:left' >" + col[1] + "</td>";
            tab += "<td style='width:10%;text-align:left' >" + col[6] + "</td>";
            tab += "<td style='width:20%;text-align:left'>" + col[2] + "</td>";                   
            tab += "<td style='width:10%;text-align:center'>" + col[4] + "</td>"; 
            // tab += "<td style='width:10%;text-align:center'><img id='imgReport' src='../Image/report.png' Height='20px' Width='20px' onclick= 'ViewReport(" + col[0] + ","+ col[5]+")' style='cursor:pointer;'/></td>";       
            var LinkText="Start";
            if (col[3] == 1)  
            LinkText="Continue";  
              if(col[5]==2){
                var txtobs='Opt'+ col[0];
              var txtLoan='OptLoan'+ col[0];
              tab += "<td style='width:20%; text-align:center;' ><input id='" + txtLoan + "' name='" + txtobs + "' type='radio' checked='true'/> Loan Wise";
              tab += "&nbsp;&nbsp;&nbsp;&nbsp;<input id='" + txtobs + "' name='" + txtobs + "' type='radio' /> Observation Wise</td>";
             
              }
              else{
                tab += "<td style='width:20%; text-align:center;' ></td>";
              } 

            tab += "<td style='width:20%; text-align:center; padding-left:20px; ' onclick=StartAudit('" + col[0] + "','"+ col[5]+"','"+LinkText+"','"+col[1].toString().replace(" ","$" )+"','"+ col[6] +"')><div id='Button' >"+ LinkText +"</div></td>";
                
            tab += "</tr>";
        }
    }
    tab += "</table></div></div></div>";
    document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
    //--------------------- Clearing Data ------------------------//
}
function StartAudit(AuditID,AuditType,LinkText,BranchName,Month)
{  
    var TypeID = 2;
     if (AuditType == 2) 
        {
           if(document.getElementById("OptLoan"+AuditID).checked==true)
              TypeID=1;
       }      
    if (LinkText == "Start")
    {
        if (confirm("Are you sure to start the audit "+ BranchName.toString().replace("$"," ") +" "+ Month +"?") == 1)
        {
            var ToData = "1Ø" + AuditID+"Ø"+AuditType+"Ø"+TypeID;   
            ToServer(ToData, 1);  
        }    
    }
    else
    {
        if (AuditType == 1) window.open("Audit.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");            /* Banking Audit*/
        if (AuditType == 2) /* Micro Finance Audit */
        { if(TypeID==1)                  
                window.open("LoanWiseAudit.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");
             else          
                window.open("Audit_Micro.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");      
        }
        if (AuditType == 3) window.open("GeneralAuditNew.aspx?AuditID="+ btoa(AuditID) +" &AuditType="+AuditType,"_self");  /* General Audit*/
    }           
}
function btnExit_onclick() 
{
    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
}
</script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hdnAuditID" runat="server" />
                <asp:HiddenField ID="hdnAuditTypeID" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <br />
                &nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>   

</asp:Content>

