﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="BranchResponseApproval.aspx.vb" Inherits="Audit_BranchResponseApproval" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
 
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
             
     .bg
     {
         background-color:#FFF;
     }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link rel="stylesheet" href="../Style/bootstrap-3.1.2.minNew.css" type="text/css" />
		<link rel="stylesheet" href="../Style/bootstrap-multiselectNew.css" type="text/css" />		
        <script type="text/javascript" src="../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../Script/bootstrap-multiselectNew.js"></script>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#chkveg').multiselect();
              }); 0
</script>
      <style>
       .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblBody
        {
            border:9px;background-color:#EEB8A6; height:25px;font-size:10;color:#47476B;
        }
      
    </style>
    <br />
    <table align="center" style="width: 90%; margin:0px auto;" >
      <tr>
            <td style="width:30%; text-align:center;" colspan="2">
                <asp:HiddenField ID="hdnQueueID" runat="server" />
                <asp:HiddenField ID="hdnMaxLevelID" runat="server" />
                <asp:HiddenField ID="hdnMaxPostID" runat="server" />
                <asp:HiddenField ID="hdnPostID" runat="server" />
                <asp:HiddenField ID="hdnLoanNO" runat="server" />
                <asp:HiddenField ID="hdnLoanDtls" runat="server" />
                <asp:HiddenField ID="hdnDtl" runat="server" />
                <asp:HiddenField ID="hdnItemDtl" runat="server" />
              </td>
        
        </tr>
        <tr>
            <td style="width:30%; text-align:right;">
                Branch</td>
            <td style="width:70%; text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:30%; text-align:right;">
                Audit</td>
            <td style="width:70%; text-align:left;">
                 &nbsp;&nbsp;<asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                     <asp:ListItem Value="-1">----------Select----------</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width:30%; text-align:right;">
                Check List</td>
            <td style="width:70%; text-align:left;">
                &nbsp;&nbsp;<select id='chkveg'  multiple='multiple' style='width:90%;' onchange='FilterOnClick()'></select></td>
        </tr>
        <tr>
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnObservHead" runat="server">
                </asp:Panel>
               </td>
        </tr>
        <tr>
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnObservDetails" runat="server">
                </asp:Panel>
               </td>
        </tr>
          <tr style="height:25px;">
            <td id="lbltimeDisp" style="width:30%; text-align:center; color:red" colspan="2">
               
              </td>
        
        </tr>
          <tr id = "rowRemarks" style="display:none;">
            <td style="width:30%; text-align:right;">
                Remarks&nbsp; <asp:Label ID="lblNumber" runat="server" Text="Label"></asp:Label></td>
            <td style="width:70%; text-align:left;">
                &nbsp;&nbsp;<asp:TextBox ID="txtRemarks" runat="server" Rows="4" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' 
                    Width="90%"></asp:TextBox>
              </td>
        </tr>
          <tr>
            <td style="width:30%; text-align:left;">
                &nbsp;</td>
            <td style="width:70%; text-align:left;">
                &nbsp;</td>
        </tr>
        <tr>
             <td style="width:30%; text-align:center;" colspan="2">
                <input id="btnReject"  style="font-family: cambria; width: 18%; cursor: pointer;"  type="button" value="REJECT" onclick="return btnReject_onclick()" />&nbsp;
                <input id="btnRecommend" style="font-family: cambria; width: 18%; cursor: pointer;"  
                    type="button" value="RECOMMEND" onclick="return btnRecommend_onclick()" />&nbsp;
                <input id="btnApprove"  style="font-family: cambria; width: 18%; cursor: pointer;" type="button" value="APPROVE" onclick="return btnApprove_onclick()" />&nbsp;
                <input id="btnHigherApproval"  
                    style="font-family: cambria; width: 18%; cursor: pointer;" type="button" 
                    value="NEED HIGHER APPROVAL" onclick="return btnHigherApproval_onclick()" />&nbsp;
                 <input id="btnSatisfied"  
                    style="font-family: cambria; width: 18%; cursor: pointer; "  
                    type="button" value="SATISFIED"  onclick="return btnSatisfied_onclick()" />&nbsp;
                    <input id="btnClose"  
                    style="font-family: cambria; width: 18%; cursor: pointer; "  
                    type="button" value="CONFIRM"   onclick="return btnClose_onclick()" />
                 <input id="btnNotSatisfied"  
                    style="font-family: cambria; width: 18%; cursor: pointer; "  
                    type="button" value="NOT SATISFIED"  onclick="return btnNotSatisfied_onclick()" />&nbsp;
                      <input id="btnHOQueue"  
                    style="font-family: cambria; width: 18%; cursor: pointer;" type="button" 
                    value="MOVE TO HO QUEUE"  onclick="return btnHOQueue_onclick()" />&nbsp;<input id="btnPreviousLevel"  
                    style="font-family: cambria; width: 18%; cursor: pointer; height: 25px;" type="button" 
                    value="SEND TO PREVIOUS LEVEL"  
                     onclick="return btnPreviousLevel_onclick()" />&nbsp;
                 <input id="btnConfirmation" onclick="return btnConfirmation_onclick()" 
                     
                     style="font-family: cambria; width: 18%; cursor: pointer; visibility: hidden;" 
                     type="button" value="SEND TO CONFIRMATION" /></td>
         
        </tr>
        <tr>
             <td style="width:30%; text-align:center;" colspan="2">
                <input id="btn"  style="font-family: cambria; width: 10%; cursor: pointer;" 
                     type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
         
        </tr>
        <tr>
            <td style="width:30%; text-align:left;">
                &nbsp;</td>
            <td style="width:70%; text-align:left;">
                &nbsp;</td>
        </tr>
     
    </table>
 
      
    <script language="javascript" type="text/javascript">
// <![CDATA[
        var Remarks="";
         $(function() {
			          $('#chkveg').multiselect({                                            
			          includeSelectAllOption: true
                                        
			          });
			          $('#btnget').click(function() {
			          alert($('#chkveg').val());
			           })
			      });
        var QueueID=document.getElementById("<%= hdnQueueID.ClientID %>").value;
       function cursorwait(e) {
    document.body.className = 'wait';
}
   function cursordefault(e) {
    document.body.className = 'default';
}
        function btnExit_onclick() {
         window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function BranchOnchange() {    
         document.getElementById("<%= pnObservDetails.ClientID %>").style.display="none";   
         jQuery('#chkveg').multiselect('dataprovider', []);  
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            if (BranchID != -1) {
                var Data = "4Ø" + BranchID+"Ø"+QueueID;
                ToServer(Data, 4);
            }
           
        }
        function AuditOnChange() { 
         document.getElementById("<%= pnObservDetails.ClientID %>").style.display="none"; 
         jQuery('#chkveg').multiselect('dataprovider', []);
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            if (AuditID != -1) {
                var Data = "2Ø" + AuditID+"Ø"+QueueID;
                ToServer(Data, 2);
            }
           
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
             var option = document.createElement("OPTION");
             option.value = -1;
             option.text = "----------Select----------";
             document.getElementById(ddlName).add(option);
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                document.getElementById("<%= txtRemarks.ClientID %>").value="";               
                document.getElementById("rowRemarks").style.display='none';
                var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
                if (AuditID != -1) {
                var Data = "2Ø" + AuditID+"Ø"+QueueID;
                ToServer(Data, 2);
            }
            }
            else if (context == 2) {
                 
               document.getElementById("btnHigherApproval").style.display='none';
               document.getElementById("btnApprove").style.display='none';
               document.getElementById("btnReject").style.display='none';
               document.getElementById("btnClose").style.display='none';
               document.getElementById("btnRecommend").style.display='none';               
               document.getElementById("btnSatisfied").style.display='none';
               document.getElementById("btnNotSatisfied").style.display='none';
               document.getElementById("btnHOQueue").style.display='none';
               document.getElementById("btnPreviousLevel").style.display='none'; 
               document.getElementById("btnConfirmation").style.display='none';                
               document.getElementById("<%= hdnItemDtl.ClientID %>").value=arg;  
               var ArgDtl=arg.split("Ø");
               document.getElementById("<%= hdnDtl.ClientID %>").value=ArgDtl[0];    
               cursordefault();     
               document.getElementById("<%= pnObservDetails.ClientID %>").style.display="none";              
               UpdateItem(ArgDtl[1]); 

                     
              // FillObservationDetails(arg);
            }
             if (context == 4) {
                ComboFill(arg,"<%= cmbAudit.ClientID %>");
            }
            
        }
       function FilterOnClick()
        {       
          var SelectedItems =$('#chkveg').val();        
          if(SelectedItems!=null)
          {
          var Dtl=document.getElementById("<%= hdnItemDtl.ClientID %>").value.split("Ø");         
           var row = Dtl[0].split("ÿ");       
           document.getElementById("<%= hdnDtl.ClientID %>").value="";         
           for (n = 1; n <= row.length - 1; n++) {
                    col = row[n].split("~");
          if(SelectedItems.indexOf(col[7])>-1){
             document.getElementById("<%= hdnDtl.ClientID %>").value+= "ÿ"+  row[n];              
             }
         
          }
           document.getElementById("<%= hdnDtl.ClientID %>").value=document.getElementById("<%= hdnDtl.ClientID %>").value; 
         }
         else
         document.getElementById("<%= hdnDtl.ClientID %>").value="ÿ~~~~~~~~~~~~~~";
               
         FillObservationDetails(document.getElementById("<%= hdnItemDtl.ClientID %>").value);
        }
        function TableFill()
        {   var tab="";
           tab += "<div style='width:100%; height:auto; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
            tab += "<tr class='tblBody' height=30px;>"; 
            tab += "<td style='width:5%;  text-align:center' >#</td>";          
            tab += "<td style='width:8%;  text-align:center' >Ref No</td>";
            tab += "<td style='width:10%;  text-align:center'>Client Name</td>";
            tab += "<td style='width:10%; text-align:center'>Center Name</td>"; 
            tab += "<td style='width:6%; text-align:center'>Date</td>";   
            tab += "<td style='width:4%; text-align:center'>Amount</td>";      
            tab += "<td style='width:13%;  text-align:left'>Check List</td>";
            tab += "<td style='width:12%; text-align:center'>Audit Observation</td>";
            tab += "<td style='width:15%;  text-align:center'>Last Remarks</td>";
            tab += "<td style='width:4%;  text-align:center'>Type</td>";
            tab += "<td style='width:4%;  text-align:center'>Staffs Invld</td>";
            tab += "<td style='width:4%;  text-align:center'>Days Remain</td>";
            tab += "<td style='width:5%; text-align:center'>Respond&nbsp;<input id='chkAll' onclick='return SelectAllonClick()'  style='font-family: cambria; width: 45%; cursor: pointer;' type='checkbox' ></td>";  
            tab += "</tr>"; 
            tab += "</table></div>";
            document.getElementById("<%= pnObservHead.ClientID %>").innerHTML=tab;  
            document.getElementById("<%= pnObservHead.ClientID %>").style.display="";
        }

        function SelectAllonClick()
        {  var Dtl = document.getElementById("<%= hdnDtl.ClientID %>").value.split("ÿ");
        if(document.getElementById("chkAll").checked==true)
        {        
            if(document.getElementById("<%= hdnDtl.ClientID %>").value!="ÿ~~~~~~~~~~~~~~")    
            {   var ObserveDtl = Dtl[1].split("~");
                    var FirstLevel=1;
                    if(ObserveDtl[12]=="S")
                        FirstLevel=2;
                    else if (ObserveDtl[12]=="VS")
                        FirstLevel=3;
                CheckAllSame(1,FirstLevel);
            }
          }
          else
          {
            document.getElementById("btnHigherApproval").style.display='none';
            document.getElementById("btnApprove").style.display='none';
            document.getElementById("btnReject").style.display='none';
            document.getElementById("btnClose").style.display='none';
            document.getElementById("btnRecommend").style.display='none';
            document.getElementById("btnSatisfied").style.display='none';
            document.getElementById("btnNotSatisfied").style.display='none';
            document.getElementById("btnPreviousLevel").style.display='none';
            document.getElementById("rowRemarks").style.display='none';
            document.getElementById("btnPreviousLevel").style.display='none';
            document.getElementById("btnConfirmation").style.display='none';
             document.getElementById("<%= lblNumber.ClientID %>").innerHTML="";  
             var i=0;  
            for (a = 1; a < Dtl.length; a++) 
            {i+=1;               
                document.getElementById("chk"+i).checked=false;
            }
          }
        }

        function CheckAllSame(value,FirstLevel) {
            var LevelID=0;
            var LevelUpTo=0;  
            var obsQue=0  ; 
            var Dtl = document.getElementById("<%= hdnDtl.ClientID %>").value.split("ÿ");
            var MaxLevelID    = document.getElementById("<%= hdnMaxLevelID.ClientID %>").value;             
            var i=0;  
            var j=0;
            var ObserveDtl = Dtl[value].split("~");
            var CurrLevelID=0;
            var CurrLevelUpTo=0;  
            var CurrobsQue=0        
            CurrLevelID=ObserveDtl[2];
            CurrLevelUpTo=ObserveDtl[3];            
            if(CurrLevelID>=(FirstLevel+2) && CurrLevelID<CurrLevelUpTo)
            {
                CurrobsQue=3;
            }  
            else if(CurrLevelID>=(FirstLevel+2) && CurrLevelID==CurrLevelUpTo && CurrLevelID<MaxLevelID)
            {
                CurrobsQue=4;
            }       
          
            else if(CurrLevelID<CurrLevelUpTo)
            {
                CurrobsQue=1;
            }
            else if (CurrLevelID==MaxLevelID)
            {   
                CurrobsQue=2;
            }          
            else 
            {
                CurrobsQue=5;
            } 
            if (CurrobsQue==3 &&  (CurrLevelID==12 && CurrLevelUpTo>3))
            {
                 CurrobsQue=7;
            }  
            if (CurrobsQue==4 &&  (CurrLevelID==12 && CurrLevelUpTo>3))
            {
                 CurrobsQue=8;
            }  
     
            for (a = 1; a < Dtl.length; a++) 
            {
                ObserveDtl = Dtl[a].split("~");
                i+=1;                  
              
                LevelID=ObserveDtl[2];
                LevelUpTo=ObserveDtl[3];
                if(LevelID>=(FirstLevel+2) && LevelID<LevelUpTo)
                {
                     obsQue=3;
                }                 
                else if(LevelID>=(FirstLevel+2) && LevelID==LevelUpTo && LevelID<MaxLevelID)
                {
                     obsQue=4;
                }                     
                else if(LevelID<LevelUpTo)
                {
                      obsQue=1;
                }
                else if (LevelID==MaxLevelID)
                {
                      obsQue=2;
                }                    
                else 
                {
                    obsQue=5;
                }
                if (obsQue==3 &&  (LevelID==12 && LevelUpTo>3))
                {
                    obsQue=7;
                }  
                if (obsQue==4 &&  (LevelID==12 && LevelUpTo>3))
                {
                     obsQue=8;
                }                    
                
                if(LevelID>0 && CurrLevelID>0 && CurrobsQue==obsQue)
                {document.getElementById("chk"+i).checked=true;  j+=1;}
            
           }
        
        if(CurrLevelID==0 && CurrLevelUpTo==0)
        {CurrobsQue=obsQue;}      
    if(j>0)
    {  
    var MaxPostID     = document.getElementById("<%= hdnMaxPostID.ClientID %>").value;
    var PostID        = document.getElementById("<%= hdnPostID.ClientID %>").value; 
    document.getElementById("rowRemarks").style.display='';   
    var disp="Observations";
    if(j==1)
     disp="Observation";
    document.getElementById("<%= lblNumber.ClientID %>").innerHTML="(for " + j +" " + disp + " )";    
    if (QueueID == 1) {
            if(MaxPostID==PostID)
                {
                    document.getElementById("btnHigherApproval").style.display='none';
                    document.getElementById("btnApprove").style.display='none';
                    document.getElementById("btnRecommend").style.display='none';
                    document.getElementById("btnReject").style.display='none';
                    document.getElementById("btnSatisfied").style.display='none';
                    document.getElementById("btnNotSatisfied").style.display='none';
                    document.getElementById("btnClose").style.display='';
                    document.getElementById("btnPreviousLevel").style.display='';
                }
                else
                {
                    document.getElementById("btnHigherApproval").style.display='';
                    document.getElementById("btnApprove").style.display='';
                    document.getElementById("btnRecommend").style.display='';
                    document.getElementById("btnReject").style.display='';
                    document.getElementById("btnSatisfied").style.display='none';
                    document.getElementById("btnNotSatisfied").style.display='none';
                    document.getElementById("btnClose").style.display='none';
                    document.getElementById("btnPreviousLevel").style.display='none';
                }

            if(CurrobsQue==1)
            {
                document.getElementById("btnHigherApproval").style.display='none';
                document.getElementById("btnApprove").style.display='none';
                document.getElementById("btnRecommend").style.display='';
            }
            else if (CurrobsQue==2)
            {
                document.getElementById("btnHigherApproval").style.display='none';
                document.getElementById("btnApprove").style.display='none';
                document.getElementById("btnReject").style.display='none';
                document.getElementById("btnClose").style.display='';
                document.getElementById("btnRecommend").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='none';
            }
            else if (CurrobsQue==3)
            {
                document.getElementById("btnHigherApproval").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='';
                document.getElementById("btnApprove").style.display='none';
            }
            else if(CurrobsQue==4)
            {
                document.getElementById("btnHigherApproval").style.display='';
                document.getElementById("btnApprove").style.display='';
                document.getElementById("btnRecommend").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='';
            }
            else 
            {
                document.getElementById("btnHigherApproval").style.display='';
                document.getElementById("btnApprove").style.display='';
                document.getElementById("btnRecommend").style.display='none';
            }
        }
         else
        {
             if(MaxPostID==PostID)
             {
                document.getElementById("btnHOQueue").style.display='';
             }
             else{
                 document.getElementById("btnHigherApproval").style.display='';
             }          
            document.getElementById("btnApprove").style.display='none';
            document.getElementById("btnReject").style.display='none';
            document.getElementById("btnClose").style.display='none';
            document.getElementById("btnRecommend").style.display='none';
            document.getElementById("btnSatisfied").style.display='';
            if (CurrobsQue>6)
            {
                document.getElementById("btnNotSatisfied").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='none'; 
            }
            else
            {
                document.getElementById("btnNotSatisfied").style.display='';              
                document.getElementById("btnNotSatisfied").value ='NS-TO RESPONDENT';
                document.getElementById("btnPreviousLevel").value ='NS-TO APPROVER';
                document.getElementById("btnPreviousLevel").style.display='';
            }
                    
        }
         }
        else
        {
            document.getElementById("btnHigherApproval").style.display='none';
            document.getElementById("btnApprove").style.display='none';
            document.getElementById("btnReject").style.display='none';
            document.getElementById("btnClose").style.display='none';
            document.getElementById("btnRecommend").style.display='none';
            document.getElementById("btnSatisfied").style.display='none';
            document.getElementById("btnNotSatisfied").style.display='none';
            document.getElementById("btnPreviousLevel").style.display='none';
            document.getElementById("rowRemarks").style.display='none';
            document.getElementById("btnPreviousLevel").style.display='none';
            document.getElementById("btnConfirmation").style.display='none';
        }
        var PostID        = document.getElementById("<%= hdnPostID.ClientID %>").value; 
//        if(PostID==1 || PostID==2 || PostID==8 || PostID==9 || PostID==10 || PostID==11) 
//        {
//            document.getElementById("btnConfirmation").style.display='';
//        }
//        else
//        {
//            document.getElementById("btnConfirmation").style.display='none';
//        }
}



        function UpdateItem(arg){            
            jQuery('#chkveg').multiselect('dataprovider', []);
			    var dropdown2OptionList = [];			          
			    var Dtl = arg;
			    var rrr = Dtl.split("Ñ");
			     for (a = 1; a <= rrr.length - 1; a++) {
			            var cols = rrr[a].split("ÿ");
			            var ddd = cols[0].split("~");	                       	         
			                dropdown2OptionList.push({
			                    'label': cols[1],
			                    'value': cols[0]                             
			                });			            
			     
			       }
			        jQuery('#chkveg').multiselect('dataprovider', dropdown2OptionList);
                    jQuery('#chkveg').multiselect({
			        includeSelectAllOption: true
                    });
                   
        }

        function FillObservationDetails()
        {           
            var ArgDtl=document.getElementById("<%= hdnItemDtl.ClientID %>").value.split("Ø");         
            var Dtl = document.getElementById("<%= hdnDtl.ClientID %>").value.split("ÿ");
            var tab = "";        
            if(document.getElementById("<%= hdnDtl.ClientID %>").value!="ÿ~~~~~~~~~~~~~~")    
            {      
            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
            var row_bg = 0;
            var i=0;
            for (a = 1; a < Dtl.length; a++) 
            {
            var ObserveDtl = Dtl[a].split("~");
            i+=1;
            if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_first'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_second'>";
                    }  
            tab += "<td style='width:5%;  text-align:center'>"+ i +"</td>";
            tab += "<td style='width:8%;  text-align:left' ><a href=Reports/viewLoanDetails.aspx?LoanNo=" +  btoa(ObserveDtl[6]) + " target=_blank>"+ ObserveDtl[6] +"</a></td>";
            tab += "<td style='width:10%;  text-align:left'>"+ ObserveDtl[8] +"</td>";
            tab += "<td style='width:10%;  text-align:left'>"+ ObserveDtl[10] +"</td>";
            tab += "<td style='width:6%;  text-align:left'>"+ ObserveDtl[14] +"</td>";           
            if(ObserveDtl[15]!="")                 
                tab += "<td style='width:4%;  text-align:right'>"+parseFloat(ObserveDtl[15]).toFixed(0) +"</td>";
            else
                tab += "<td style='width:4%;  text-align:right'></td>";
            tab += "<td style='width:13%; text-align:left'>"+ ObserveDtl[9] +"</td>";
            tab += "<td style='width:12%;  text-align:left'>"+ ObserveDtl[4] +"</td>";
            tab += "<td style='width:15%;  text-align:left'>"+ ObserveDtl[11] + "&nbsp;<a href =Reports/viewPrevRemarksReport.aspx?AuditID=" + btoa(ObserveDtl[1]) + " target=_blank>Previous</a>" +"</td>";
            var msg;
            var MaxLevelID    = document.getElementById("<%= hdnMaxLevelID.ClientID %>").value;
            var QueueID       = document.getElementById("<%= hdnQueueID.ClientID %>").value;
            if(ObserveDtl[2] ==MaxLevelID || QueueID==2)
            {
                msg="";
            }
            else if(ObserveDtl[0] >2)
            {
                msg=ObserveDtl[0]  ;
            }
            else if(ObserveDtl[0]>0)
            {
                msg="<span style='color:orange; font-weight:bold;'>"+ ObserveDtl[0]  + "</span>";
            }
            else
            {
                msg="<span style='color:red; '>Time Expired</span>"
            }
            tab += "<td style='width:4%;  text-align:center'>"+ ObserveDtl[12] +"</td>";
            if(ObserveDtl[13]!="0")
                tab += "<td style='width:4%;  text-align:center'><a href='Reports/viewStaffInvolvement.aspx?SINo="+btoa(ObserveDtl[1]) +"' target=blank style='cursor:pointer;'>"+ ObserveDtl[13] +"</a></td>";
            else
                tab += "<td style='width:4%;  text-align:center'>-</td>";
            var FirstLevel=1;
            if(ObserveDtl[12]=="S")
                 FirstLevel=2;
            else if (ObserveDtl[12]=="VS")
                FirstLevel=3;
            tab += "<td style='width:4%;  text-align:center'>"+ msg +"</td>";
            tab += "<td style='width:5%;  text-align:center'><input id='chk"+ i +"' onclick='return btnRespond_onclick(" + i + ","+ FirstLevel +")'  style='font-family: cambria; width: 45%; cursor: pointer;' type='checkbox' ></td>";  
            tab += "</tr>"; 
            }
            tab += "</table></div>";
            }
            document.getElementById("<%= pnObservDetails.ClientID %>").innerHTML=tab; 
            document.getElementById("<%= pnObservDetails.ClientID %>").style.display="";          
            cursordefault();
        }
 
function ViewReport(LoanNo){
    window.open("Reports/viewLoanDetails.aspx?LoanNo=" + btoa(LoanNo));
}
        
function btnReject_onclick() {
    cursorwait();
     Confirmation(5);
     cursordefault();
}
function Confirmation(StatusID)
{        if(StatusID!=19)
    {  
       var Dtl = document.getElementById("<%= hdnDtl.ClientID %>").value.split("ÿ");
       var ObserveDtl;
       document.getElementById("<%= hdnLoanDtls.ClientID %>").value="";
      
        for (a = 1; a < Dtl.length; a++) 
        {           
            ObserveDtl = Dtl[a].split("~");           
            if(document.getElementById("chk"+a).checked==true)
            {
                document.getElementById("<%= hdnLoanDtls.ClientID %>").value+="ÿ"+ObserveDtl[1]+"~"+ObserveDtl[2];
            }
            
        }      
       if(document.getElementById("<%= hdnLoanDtls.ClientID %>").value=="")
       {alert("Select atleast one Observation");return false;}
       var Remarks= document.getElementById("<%= txtRemarks.ClientID %>").value;
       if(Remarks=="")
       {alert("Enter Remarks");document.getElementById(("<%= txtRemarks.ClientID %>")).focus();return false;}
    
       var Data = "1Ø" + document.getElementById("<%= hdnLoanDtls.ClientID %>").value+"Ø"+Remarks+"Ø"+StatusID;     
      
      ToServer(Data, 1);
         }  
    else
    {
        var Dtl = document.getElementById("<%= hdnDtl.ClientID %>").value.split("ÿ");
       var ObserveDtl;
       document.getElementById("<%= hdnLoanDtls.ClientID %>").value="";
      
        for (a = 1; a < Dtl.length; a++) 
        {           
            ObserveDtl = Dtl[a].split("~");           
            if(document.getElementById("chk"+a).checked==true)
            {
                if(document.getElementById("<%= hdnLoanDtls.ClientID %>").value!="")
                {
                    document.getElementById("<%= hdnLoanDtls.ClientID %>").value+=","
                }
                document.getElementById("<%= hdnLoanDtls.ClientID %>").value+=ObserveDtl[1];
            }
            
        }      
       if(document.getElementById("<%= hdnLoanDtls.ClientID %>").value=="")
       {alert("Select atleast one Observation");return false;}
       var Remarks= document.getElementById("<%= txtRemarks.ClientID %>").value;
       if(Remarks=="")
       {alert("Enter Remarks");document.getElementById(("<%= txtRemarks.ClientID %>")).focus();return false;}
        window.open("SendToDepartmentConfirmation.aspx?ConfDtl="+ document.getElementById("<%= hdnLoanDtls.ClientID %>").value+"Ø"+Remarks+"Ø"+StatusID,"_self");

        }
}
function btnRecommend_onclick() {
cursorwait();
 Confirmation(3);
 cursordefault();
}

function btnApprove_onclick() {
cursorwait();
 Confirmation(4);
 cursordefault();
}

function btnHigherApproval_onclick() {
cursorwait();
 Confirmation(6);
 cursordefault();
}

function window_onload() {
 
    document.getElementById("btn").style.display='';
    document.getElementById("btnClose").style.display='none';
    document.getElementById("btnHigherApproval").style.display='none';
    document.getElementById("btnApprove").style.display='none';
    document.getElementById("btnRecommend").style.display='none';
    document.getElementById("btnReject").style.display='none';
    document.getElementById("btnSatisfied").style.display='none';
    document.getElementById("btnNotSatisfied").style.display='none';
    document.getElementById("btnHOQueue").style.display='none';
    document.getElementById("btnPreviousLevel").style.display='none';
    document.getElementById("btnConfirmation").style.display='none'; 
    TableFill();
}

function btnRespond_onclick(value,FirstLevel) {
            var LevelID=0;
            var LevelUpTo=0;  
            var obsQue=0  ; 
            var Dtl = document.getElementById("<%= hdnDtl.ClientID %>").value.split("ÿ");
            var MaxLevelID    = document.getElementById("<%= hdnMaxLevelID.ClientID %>").value;             
            var i=0;  
            var j=0;
            var ObserveDtl = Dtl[value].split("~");
            var CurrLevelID=0;
            var CurrLevelUpTo=0;  
            var CurrobsQue=0           
           if(document.getElementById("chk"+value).checked==true)
           {            
              
            j+=1;           
            CurrLevelID=ObserveDtl[2];
            CurrLevelUpTo=ObserveDtl[3];            
            if(CurrLevelID>=(FirstLevel+2) && CurrLevelID<CurrLevelUpTo)
            {
                CurrobsQue=3;
            }  
            else if(CurrLevelID>=(FirstLevel+2) && CurrLevelID==CurrLevelUpTo && CurrLevelID<MaxLevelID)
            {
                CurrobsQue=4;
            }       
          
            else if(CurrLevelID<CurrLevelUpTo)
            {
                CurrobsQue=1;
            }
            else if (CurrLevelID==MaxLevelID)
            {   
                CurrobsQue=2;
            }          
            else 
            {
                CurrobsQue=5;
            } 
            if (CurrobsQue==3 &&  (CurrLevelID==12 && CurrLevelUpTo>3))
            {
                 CurrobsQue=7;
            }  
            if (CurrobsQue==4 &&  (CurrLevelID==12 && CurrLevelUpTo>3))
            {
                 CurrobsQue=8;
            }  
     }  
            for (a = 1; a < Dtl.length; a++) 
            {
                ObserveDtl = Dtl[a].split("~");
                i+=1; 
                  
                if(i!=value && document.getElementById("chk"+i).checked==true)
                {
                    j+=1;
                    if(LevelID==0 && LevelUpTo==0)
                    {
                       
                        LevelID=ObserveDtl[2];
                        LevelUpTo=ObserveDtl[3];
                        if(LevelID>=(FirstLevel+2) && LevelID<LevelUpTo)
                        {
                            obsQue=3;
                        }
                        else if(LevelID>=(FirstLevel+2) && LevelID==LevelUpTo && LevelID<MaxLevelID)
                        {
                            obsQue=4;
                        }
                    
                        else if(LevelID<LevelUpTo)
                        {
                            obsQue=1;
                        }
                        else if (LevelID==MaxLevelID)
                        {
                            obsQue=2;
                        }                    
                        else 
                        {
                            obsQue=5;
                        }
                        if (obsQue==3 &&  (LevelID==12 && LevelUpTo>3))
                        {
                            obsQue=7;
                        }  
                        if (obsQue==4 &&  (LevelID==12 && LevelUpTo>3))
                        {
                            obsQue=8;
                        }  
                    }
                }
              
                if(LevelID>0 && CurrLevelID>0 && CurrobsQue!=obsQue)
                {alert("You Can not select approval and recommendation category together");return false;document.getElementById("chk"+value).checked=false;}
            
           }
        
        if(CurrLevelID==0 && CurrLevelUpTo==0)
        {CurrobsQue=obsQue;}      
    if(j>0)
    {  
    var MaxPostID     = document.getElementById("<%= hdnMaxPostID.ClientID %>").value;
    var PostID        = document.getElementById("<%= hdnPostID.ClientID %>").value; 
    document.getElementById("rowRemarks").style.display='';   
    var disp="Observations";
    if(j==1)
     disp="Observation";
    document.getElementById("<%= lblNumber.ClientID %>").innerHTML="(for " + j +" " + disp + " )";    
    if (QueueID == 1) {
            if(MaxPostID==PostID)
                {
                    document.getElementById("btnHigherApproval").style.display='none';
                    document.getElementById("btnApprove").style.display='none';
                    document.getElementById("btnRecommend").style.display='none';
                    document.getElementById("btnReject").style.display='none';
                    document.getElementById("btnSatisfied").style.display='none';
                    document.getElementById("btnNotSatisfied").style.display='none';
                    document.getElementById("btnClose").style.display='';
                    document.getElementById("btnPreviousLevel").style.display='';
                }
                else
                {
                    document.getElementById("btnHigherApproval").style.display='';
                    document.getElementById("btnApprove").style.display='';
                    document.getElementById("btnRecommend").style.display='';
                    document.getElementById("btnReject").style.display='';
                    document.getElementById("btnSatisfied").style.display='none';
                    document.getElementById("btnNotSatisfied").style.display='none';
                    document.getElementById("btnClose").style.display='none';
                    document.getElementById("btnPreviousLevel").style.display='none';
                }

            if(CurrobsQue==1)
            {
                document.getElementById("btnHigherApproval").style.display='none';
                document.getElementById("btnApprove").style.display='none';
                document.getElementById("btnRecommend").style.display='';
            }
            else if (CurrobsQue==2)
            {
                document.getElementById("btnHigherApproval").style.display='none';
                document.getElementById("btnApprove").style.display='none';
                document.getElementById("btnReject").style.display='none';
                document.getElementById("btnClose").style.display='';
                document.getElementById("btnRecommend").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='none';
            }
            else if (CurrobsQue==3)
            {
                document.getElementById("btnHigherApproval").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='';
                document.getElementById("btnApprove").style.display='none';
            }
            else if(CurrobsQue==4)
            {
                document.getElementById("btnHigherApproval").style.display='';
                document.getElementById("btnApprove").style.display='';
                document.getElementById("btnRecommend").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='';
            }
            else 
            {
                document.getElementById("btnHigherApproval").style.display='';
                document.getElementById("btnApprove").style.display='';
                document.getElementById("btnRecommend").style.display='none';
            }
        }
         else
        {
             if(MaxPostID==PostID)
             {
                document.getElementById("btnHOQueue").style.display='';
             }
             else{
                 document.getElementById("btnHigherApproval").style.display='';
             }          
            document.getElementById("btnApprove").style.display='none';
            document.getElementById("btnReject").style.display='none';
            document.getElementById("btnClose").style.display='none';
            document.getElementById("btnRecommend").style.display='none';
            document.getElementById("btnSatisfied").style.display='';
            if (CurrobsQue>6)
            {
                document.getElementById("btnNotSatisfied").style.display='none';
                document.getElementById("btnPreviousLevel").style.display='none'; 
            }
            else
            {
                document.getElementById("btnNotSatisfied").style.display='';              
                document.getElementById("btnNotSatisfied").value ='NS-TO RESPONDENT';
                document.getElementById("btnPreviousLevel").value ='NS-TO APPROVER';
                document.getElementById("btnPreviousLevel").style.display='';
            }
                    
        }
         }
        else
        {
            document.getElementById("btnHigherApproval").style.display='none';
            document.getElementById("btnApprove").style.display='none';
            document.getElementById("btnReject").style.display='none';
            document.getElementById("btnClose").style.display='none';
            document.getElementById("btnRecommend").style.display='none';
            document.getElementById("btnSatisfied").style.display='none';
            document.getElementById("btnNotSatisfied").style.display='none';
            document.getElementById("btnPreviousLevel").style.display='none';
            document.getElementById("rowRemarks").style.display='none';
            document.getElementById("btnPreviousLevel").style.display='none';
            document.getElementById("btnConfirmation").style.display='none';
        }
        var PostID        = document.getElementById("<%= hdnPostID.ClientID %>").value; 
//        if(PostID==1 || PostID==2 || PostID==3  || PostID==8 || PostID==9 || PostID==10 || PostID==11) 
//        {
//            document.getElementById("btnConfirmation").style.display='';
//        }
//        else
//        {
//            document.getElementById("btnConfirmation").style.display='none';
//        }
         if(PostID==9) 
        {
            document.getElementById("btnPreviousLevel").style.display='';
        }
}
function btnSatisfied_onclick() {
cursorwait();
 Confirmation(8);
 cursordefault();
}

function btnNotSatisfied_onclick() {
cursorwait();
 Confirmation(9);
 cursordefault();
}

function btnClose_onclick() {
cursorwait();
Confirmation(14);
cursordefault();
}



function btnHOQueue_onclick() {
cursorwait();
Confirmation(16);
cursordefault();
}

function btnPreviousLevel_onclick() {
cursorwait();
Confirmation(18);
cursordefault();
}
function btnConfirmation_onclick() {
cursorwait();
Confirmation(19);
cursordefault();
}


// ]]>

    </script>
    </head>
<script language="javascript" type="text/javascript" for="window" event="onload">
// <![CDATA[
return window_onload()
// ]]>
</script>
</asp:Content>

