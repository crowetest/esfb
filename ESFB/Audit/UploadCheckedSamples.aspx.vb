﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class UploadCheckedSamples
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim ReportID As Integer = 1
    Dim DB As New MS_SQL.Connect
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""

        If CInt(Data(0)) = 1 Then
            DT = AD.GetAuditForReport(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 2 Then
            Dim Params(5) As SqlParameter
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Params(0) = New SqlParameter("@BRANCH_ID", SqlDbType.Int)
            Params(0).Value = CInt(Data(1))
            Params(1) = New SqlParameter("@AUDIT_ID", SqlDbType.Int)
            Params(1).Value = CInt(Data(2))
            Params(2) = New SqlParameter("@UPLOAD_DTL", SqlDbType.VarChar, 5000)
            Params(2).Value = CStr(Data(3))
            Params(3) = New SqlParameter("@User_ID", SqlDbType.Int)
            Params(3).Value = CInt(Session("UserID"))
            Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(5).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_AUDIT_UPLOAD_SAMPLES", Params)
            ErrorFlag = CInt(Params(4).Value)
            Message = CStr(Params(5).Value)
            Try
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 41) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            If Not IsNothing(Request.QueryString.Get("RptID")) Then
                ReportID = Request.QueryString.Get("RptID")
            End If
            If ReportID = 1 Or ReportID = 3 Then
                Me.Master.subtitle = "Audit Report"
            ElseIf ReportID = 2 Then
                Me.Master.subtitle = "Compliance Status Report"
            Else
                Me.Master.subtitle = ""
            End If
            Me.hdnReportID.Value = ReportID
            GN.ComboFill(cmbBranch, AD.GetBranchForAuditReport(CInt(Session("UserID")), Session("Post_ID")), 0, 1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "DisplaySettings();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
  
End Class

