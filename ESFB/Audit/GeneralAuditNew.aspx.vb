﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Audit_GeneralAuditNew
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DTComments As New DataTable
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 36) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "General Audit"
            Dim AuditID As Integer
            Dim AuditType As Integer
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            AuditID = CInt(GN.Decrypt(Request.QueryString.Get("AuditID")))
            AuditType = CInt(Request.QueryString.Get("AuditType"))
            DTComments = AD.GetAuditGeneralComments(AuditID)
            If DTComments.Rows.Count > 0 Then
                Me.hdnComments.Value = DTComments.Rows(0)(0).ToString() + "Ø" + DTComments.Rows(0)(1).ToString()
            End If
            hdnAuditID.Value = AuditID.ToString()
            hdnAuditTypeID.Value = AuditType.ToString()
            DT = AD.GetAuditDtl(AuditID)
            lblBranch.Text = DT.Rows(0)(1).ToString()
            lblStartDate.Text = DT.Rows(0)(2).ToString()
            lblAuditType.Text = DT.Rows(0)(3).ToString()
            DT = AD.GetObservations(AuditID)
            Me.hdnAuditDetails.Value = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                Me.hdnAuditDetails.Value += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() + "¥"
            Next
            GN.ComboFill(cmbGroup, AD.GetCheckGroup(AuditType), 0, 1)
            cmbGroup.Attributes.Add("onchange", "GroupOnChange()")
            cmbSubGroup.Attributes.Add("onchange", "SubGroupOnChange()")
            txtManagementNote.Text = AD.GetManagementNote(AuditID)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim AuditID As Integer = CInt(Data(1))
            Dim ObservationDtl As String = Data(2)
            If ObservationDtl <> "" Then
                ObservationDtl = ObservationDtl.Substring(1)
            End If
            Dim CloseFlag As Integer = CInt(Data(3))
            Dim GeneralComments As String = Data(4)
            Dim SeriousComments As String = Data(5)
            Dim ManagementNote As String = Data(6)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@AuditID", SqlDbType.Int)
                Params(0).Value = AuditID
                Params(1) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar)
                Params(1).Value = ObservationDtl
                Params(2) = New SqlParameter("@CloseFlag", SqlDbType.Int)
                Params(2).Value = CloseFlag
                Params(3) = New SqlParameter("@GeneralComments", SqlDbType.VarChar, 2000)
                Params(3).Value = GeneralComments
                Params(4) = New SqlParameter("@SeriousComments", SqlDbType.VarChar, 2000)
                Params(4).Value = SeriousComments
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@ManagementNote", SqlDbType.VarChar, 5000)
                Params(7).Value = ManagementNote
                DB.ExecuteNonQuery("SP_GeneralAuditObservation", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetCheckSubGroup(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetCheckListGeneral(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next

        ElseIf CInt(Data(0)) = 5 Then
            DT = AD.GetLoanDetails(Data(1), Data(2))
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(0).ToString() & "µ" & DT.Rows(0)(1).ToString() & "µ" & DT.Rows(0)(2).ToString() & "µ" & DT.Rows(0)(3).ToString() & "µ" & DT.Rows(0)(4).ToString() & "µ" & DT.Rows(0)(5).ToString()
            Else
                CallBackReturn = ""
            End If
        End If
    End Sub
End Class
