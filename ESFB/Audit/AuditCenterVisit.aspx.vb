﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AuditCenterVisit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim AD As New Audit
    Dim DB As New MS_SQL.Connect
    Dim AuditID As Integer
    Dim AuditType As Integer
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 310) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Update Visiting Center Details"
            Me.hdnEmpCode.Value = CStr(Session("UserID"))
            GN.ComboFill(cmbBranch, AD.GetBranchForVisitedReport(CInt(Session("UserID")), CInt(Session("Post_ID"))), 0, 1)
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbAudit.Attributes.Add("onchange", "AuditOnChange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim AuditID As Integer = CInt(Data(2))

            Dim SangamDtl As String = CStr(Data(1))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@AUDIT_GROUP_ID", SqlDbType.Int)
                Params(0).Value = AuditID
                Params(1) = New SqlParameter("@SangamDtl", SqlDbType.VarChar, 8000)
                Params(1).Value = SangamDtl.Substring(1)
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
               
                DB.ExecuteNonQuery("SP_AUDIT_SANGAM_VISIT", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString() + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim AuditID As Integer = CInt(Data(1))
            Dim StrAudit As String = ""
            DT = AD.GetSangamDetails(AuditID)
            If DT.Rows.Count > 0 Then
                For n As Integer = 0 To DT.Rows.Count - 1

                    StrAudit += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString


                    If n < DT.Rows.Count - 1 Then
                        StrAudit += "¥"
                    End If
                Next
                CallBackReturn = StrAudit
            Else
                CallBackReturn = "µµµ"
            End If
        ElseIf CInt(Data(0)) = 3 Then
            DT = AD.GetAuditDetails_Sangams(CInt(Data(1)))
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
       
        End If
    End Sub
End Class
