﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="RateABranch.aspx.vb" Inherits="Audit_RateABranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <table align="center" style="width: 60%; margin:0px auto;">
        <tr>
            <td style="font-size: medium">
                <asp:HiddenField ID="hdnAuditID" runat="server" />
                <asp:HiddenField ID="hdnAuditType" runat="server" />
                <asp:HiddenField ID="hdnTypeID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="font-size: medium">
                &nbsp;</td>
        </tr>
        <tr  class="mainhead">
            <td style="font-size: medium">
                How do you rate the general maintenance (House Keeping,Cleanliness,appearance) 
                of the Branch?</td>
        </tr>
     
        <tr class="sub_second">
            <td style="text-align:center;">
                <input id="rdVerypoor" type="radio" 
                    style="font-family: Cambria; font-size: medium" name="rr" /><span 
                    style="font-size: medium">&nbsp;&nbsp;Very Poor&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <input id="rdPoor" type="radio" 
                    style="font-family: Cambria; font-size: medium" name="rr"  /><span 
                    style="font-size: medium">&nbsp;&nbsp;Poor&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <input id="rdAverage" type="radio" 
                    style="font-family: Cambria; font-size: medium" name="rr" /><span 
                    style="font-size: medium">&nbsp;&nbsp;Average&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <input id="rdGood" type="radio" 
                    style="font-family: Cambria; font-size: medium" name="rr" /><span 
                    style="font-size: medium">&nbsp;&nbsp;Good&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <input id="rdVerygood" type="radio" 
                    style="font-family: Cambria; font-size: medium" name="rr" /><span 
                    style="font-size: medium">&nbsp;&nbsp;Very Good&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <input id="rdExcellent" type="radio" 
                    style="font-family: Cambria; font-size: medium" name="rr"  /><span 
                    style="font-size: medium">&nbsp;&nbsp;Excellent&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
        </tr>
        <tr class="mainhead">
            <td style="height: 18px;">
                </td>
        </tr>
        <tr>
            <td style="text-align:center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;">
                <input id="btnSave" style="font-family: Cambria; font-size: 10pt; width: 8%;" 
                    type="button" value="SAVE" onclick="return btnSave_onclick()" /></td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnSave_onclick() {
            var Rating = -1;
            if (document.getElementById("rdVerypoor").checked == true) {
                Rating = 0;
            }
            if (document.getElementById("rdPoor").checked == true) {
                Rating = 1;
            }
            if (document.getElementById("rdAverage").checked == true) {
                Rating = 2;
            }
            if (document.getElementById("rdGood").checked == true) {
                Rating = 3;
            }
            if (document.getElementById("rdVerygood").checked == true) {
                Rating = 4;
            }
            if (document.getElementById("rdExcellent").checked == true) {
                Rating = 5;
            }

            if (Rating < 0)
            {alert("Select Rating");return false;}
            var Data = "1Ø" + document.getElementById("<%= hdnAuditID.ClientID %>").value + "Ø" + Rating;
            ToServer(Data, 1);
         }

         function FromServer(arg, context) {

             switch (context) {
                 case 1:
                     var Data = arg.split("Ø");
                     alert(Data[1]);
                     if (document.getElementById("<%= hdnTypeID.ClientID %>").value == 1) {
                         window.open("AuditStart.aspx", "_self");
                     }
                     else {
                         window.open("AuditStaffInvolvement.aspx?AuditID=" + btoa(document.getElementById("<%= hdnAuditID.ClientID %>").value) + " &AuditType=" + document.getElementById("<%= hdnAuditType.ClientID %>").value, "_self");
                     }
                     break;
            
                 default:
                     break;

             }

         }


// ]]>
    </script>
</asp:Content>

