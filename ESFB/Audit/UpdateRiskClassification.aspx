﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="UpdateRiskClassification.aspx.vb" Inherits="Audit_UpdateRiskClassification" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }        
    </style>
    <script language="javascript" type="text/javascript">
        function table_fill() 
        {
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
              tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:5%;text-align:center' >#</td>"; 
            tab += "<td style='width:9%;text-align:center' >&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' id='chkSelectAll' onclick='SelectOnClick(-1)' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";    
            tab += "<td style='width:8%;text-align:center' >Branch ID</td>";
            tab += "<td style='width:36%;text-align:left'>Branch Name</td>";     
            tab += "<td style='width:14%;text-align:left'>Risk</td>";    
            tab += "<td style='width:14%;text-align:left'>Classification</td>"; 
            tab += "<td style='width:14%;text-align:left'>Interval (Months)</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:324px; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");

                for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_second'; style='text-align:center; height:21px; padding-left:20px;'>";
                    }
                    i = n + 1;                                      
                    tab += "<td style='width:5%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:9%;text-align:center' ><input type='checkbox' id='chkSelect" + col[0] + "' onchange=SelectOnClick(" + col[0] +   ") /></td>";
                    tab += "<td style='width:8%;text-align:center'>" + col[0] + "</td>";                   
                    tab += "<td style='width:36%;text-align:left'>" + col[1] + "</td>";    
                    var select1 = "<select id='cmbRisk" + col[0] + "' class='NormalText' name='cmbRisk" + col[0] + "' style='width:100%' disabled=true >";
                    if(col[4]==1)
                        select1 += "<option value='1' selected=true>Low</option>";
                    else
                        select1 += "<option value='1'>Low</option>";
                    if(col[4]==2)
                        select1 += "<option value='2' selected=true>Medium</option>";
                    else
                        select1 += "<option value='2'>Medium</option>";
                    if(col[4]==3)                    
                        select1 += "<option value='3' selected=true>High</option>";  
                    else
                       select1 += "<option value='3'>High</option>";                                   
                    tab += "<td style='width:14%;text-align:left'>" + select1 + "</td>";
                    var select2 = "<select id='cmbClass" + col[0] + "' class='NormalText' name='cmbClass" + col[0] + "' style='width:100%' disabled=true >";
                    if(col[3]==1)
                        select2 += "<option value='1' selected=true>Low</option>";
                    else
                        select2 += "<option value='1'>Low</option>";
                    if(col[3]==2)
                        select2 += "<option value='2' selected=true>Medium</option>";
                    else
                        select2 += "<option value='2'>Medium</option>";
                    if(col[3]==3)                    
                        select2 += "<option value='3' selected=true>High</option>";  
                    else
                       select2 += "<option value='3'>High</option>";                                     
                    tab += "<td style='width:14%;text-align:left'>" + select2 + "</td>";

                      var select1 = "<select id='cmbInterval" + col[0] + "' class='NormalText' name='cmbInterval" + col[0] + "' style='width:100%' disabled=true >";
                    if(col[4]==1)
                        select1 += "<option value='1' selected=true>1 Month</option>";
                    else
                        select1 += "<option value='1'>1 Month</option>";
                    if(col[4]==2)
                        select1 += "<option value='2' selected=true>2 Months</option>";
                    else
                        select1 += "<option value='2'>2 Months</option>";
                    if(col[4]==3)                    
                        select1 += "<option value='3' selected=true>3 Months</option>";  
                    else
                       select1 += "<option value='3'>3 Months</option>";      
                       
                    if(col[4]==4)                    
                        select1 += "<option value='4' selected=true>4 Months</option>";  
                    else
                       select1 += "<option value='4'>4 Months</option>";        
                       
                    if(col[4]==5)                    
                        select1 += "<option value='5' selected=true>5 Months</option>";  
                    else
                       select1 += "<option value='5'>5 Months</option>";  
                    if(col[4]==6)                    
                        select1 += "<option value='6' selected=true>6 Months</option>";  
                    else
                       select1 += "<option value='6'>6 Months</option>";                        
                    if(col[4]==7)                    
                        select1 += "<option value='7' selected=true>7 Months</option>";  
                    else
                       select1 += "<option value='7'>7 Months</option>";  
                    if(col[4]==8)                    
                        select1 += "<option value='8' selected=true>8 Months</option>";  
                    else
                       select1 += "<option value='8'>8 Months</option>";      
                    if(col[4]==9)                    
                        select1 += "<option value='9' selected=true>9 Months</option>";  
                    else
                       select1 += "<option value='9'>9 Months</option>";   
                    if(col[4]==10)                    
                        select1 += "<option value='10' selected=true>10 Months</option>";  
                    else
                       select1 += "<option value='10'>10 Months</option>";   
                     if(col[4]==11)                    
                        select1 += "<option value='11' selected=true>11 Months</option>";  
                    else
                       select1 += "<option value='11'>11 Months</option>";          
                    if(col[4]==12)                    
                        select1 += "<option value='12' selected=true>12 Months</option>";  
                    else
                       select1 += "<option value='12'>12 Months</option>";                                                                                                                                           
                    tab += "<td style='width:14%;text-align:left'>" + select1 + "</td>";   
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }

          function SelectOnClick(val)
            {    
                if(val<0)
                {                  
                        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                        var state=true;
                        if(document.getElementById("chkSelectAll").checked==true)
                           state=false;
                        for (n = 0; n <= row.length - 1; n++) {
                        col = row[n].split("µ");
                        document.getElementById("chkSelect"+col[0]).checked=document.getElementById("chkSelectAll").checked;
                        document.getElementById("cmbRisk"+col[0]).disabled =state; 
                       // document.getElementById("cmbClass"+col[0]).disabled =state;    
                         document.getElementById("cmbInterval"+col[0]).disabled =state;                       
                     
                     }
                    
                }
                else
                {
                document.getElementById("chkSelectAll").checked=false;
                if(document.getElementById("chkSelect"+val).checked==true)    
                {    
                    
                    document.getElementById("cmbRisk"+val).disabled =false; 
                   // document.getElementById("cmbClass"+val).disabled =false;
                    document.getElementById("cmbInterval"+val).disabled =false;   
                    document.getElementById("cmbRisk"+val).focus();  
                }
                 else
                {                                       
                    document.getElementById("cmbRisk"+val).disabled =true; 
                    document.getElementById("cmbClass"+val).disabled =true; 
                    document.getElementById("cmbInterval"+val).disabled =false;   
                }
                }
            }


      
       
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btSave_onclick() {
             document.getElementById("<%= hid_value.ClientID %>").value="";
            row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
            col = row[n].split("µ");
         if(document.getElementById("chkSelect"+col[0]).checked==true)
            {
                var RiskCategory=document.getElementById("cmbRisk"+col[0]).value;               
                if(RiskCategory==0)
                {alert("Select Risk Category");document.getElementById("cmbRisk"+col[0]).focus();return false;}  
                var Interval=document.getElementById("cmbInterval"+col[0]).value;               
                if(Interval==0)
                {alert("Select Interval");document.getElementById("cmbInterval"+col[0]).focus();return false;}                               
                document.getElementById("<%= hid_value.ClientID %>").value+="¥"+ col[0]+"µ"+ RiskCategory+"µ"+ Interval;
            }
        }
        if(document.getElementById("<%= hid_value.ClientID %>").value=="")
        {alert("No Branch Selected to Update");return false;}
         var ToData = "1Ø" + document.getElementById("<%= hid_value.ClientID %>").value;                  
         ToServer(ToData, 1);
        }

        function FromServer(arg, context) {
         switch (context) {
                case 1:
                    var Data=arg.split("Ø");
                    alert(Data[1]);
                    if(Data[0]==0) {window.open("UpdateRiskClassification.aspx","_self");}
                    break;
                    }
               }
    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_Value" runat="server" />
<br />

    <table  align="center" style="width: 60%;text-align:right; margin:0px auto;">
          <tr>
            <td style="text-align:center;">
                &nbsp;</td>
        </tr>
        <tr> 
            <td><asp:Panel ID="pnLeaveApproveDtl" runat="server" style="width:100%; text-align:center; margin:opx auto;">
            </asp:Panel></td></tr>
      
            
        <tr>
            <td style="text-align:center;">
                <br />               
                <input id="btSave" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="UPDATE"  onclick="return btSave_onclick()" />&nbsp;&nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width:10%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
            
    </table>   
    <br />  
</asp:Content>

