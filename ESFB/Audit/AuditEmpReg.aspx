﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditEmpReg.aspx.vb" Inherits="AuditEmpReg" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function RequestOnchange() {

            var Data = document.getElementById("<%= hid_data.ClientID %>").value.split("^");
            document.getElementById("<%= txtName.ClientID %>").value = Data[1];
            document.getElementById("<%= txtBranch.ClientID %>").value = Data[3];
            document.getElementById("<%= txtDepartment.ClientID %>").value = Data[5];
            document.getElementById("<%= txtDesignation.ClientID %>").value = Data[6];
            
            document.getElementById("<%= txtStartDt.ClientID %>").focus();
          }
          function RequestID() {
              var EmpID = document.getElementById("<%= txtEmpcode.ClientID %>").value;
              if (EmpID > 0) {
                  var ToData = "2Ø" + EmpID;
                  ToServer(ToData,2);
              }
          }

          function FromServer(arg, context) {
             
              if (context == 1) {
                var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("AuditEmpReg.aspx", "_self");
              }
              else if (context == 2) {
                  var Data = arg.split("~");
                 
                  if (Data[0] == 1) {
                      document.getElementById("<%= hid_data.ClientID %>").value = Data[1];
                      RequestOnchange();
                  }
                  else if (Data[0] == 2)
                  {
                      alert(Data[1]);
                      document.getElementById("<%= txtName.ClientID %>").value = "";
                      document.getElementById("<%= txtBranch.ClientID %>").value = "";
                      document.getElementById("<%= txtDepartment.ClientID %>").value = "";
                      document.getElementById("<%= txtDesignation.ClientID %>").value = "";
                      document.getElementById("<%= txtEmpcode.ClientID %>").value = "";

                      document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                  }
             
              }
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function btnConfirm_onclick() {
           
           
            if (document.getElementById("<%= txtEmpcode.ClientID %>").value == "") 
            {
                alert("Select Employee Code");
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
           
              if (document.getElementById("<%= txtStartDt.ClientID %>").value == "") 
            {
                alert("Select Joining Date");
                document.getElementById("<%= txtStartDt.ClientID %>").focus();
                return false;
            }
          
            if (document.getElementById("<%= txtName.ClientID %>").value == "-1") 
            {
                alert("Select Department");
                document.getElementById("<%= txtEmpcode.ClientID %>").focus();
                return false;
            }
            var EmpID	= document.getElementById("<%= txtEmpcode.ClientID %>").value;
	        var EffectiveDate	= document.getElementById("<%= txtStartDt.ClientID %>").value;
	        if(document.getElementById("<%= chkNeedApproval.ClientID %>").checked==1){
                var  ApprovalStatus=1;
            }
            else{
                var  ApprovalStatus=0;
            }

	        var ToData = "1Ø" + EmpID + "Ø" + EffectiveDate+ "Ø" + ApprovalStatus;
	       
            ToServer(ToData, 1);
        }

    </script>
   
</head>
</html>
<br />
    <asp:HiddenField ID="hid_data" runat="server" />
    <br />

 <table class="style1" style="width:80%;margin: 0px auto;">
    <tr> <td style="width:25%;"></td>
    <td style="width:12%; text-align:left;">Employee Code</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtEmpcode" class="NormalText" runat="server" Width="15%" 
                MaxLength="5"  ></asp:TextBox>
            </td>
    </tr>
        <tr>
        <td style="width:25%;"></td>
         <td style="width:12%; text-align:left;">Name</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Location</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Department</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDepartment" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       <tr> <td style="width:25%;"></td>
       <td style="width:12%; text-align:left;">Designation</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDesignation" class="ReadOnlyTextBox" runat="server" Width="60%" 
                MaxLength="100"></asp:TextBox>
            </td>


       </tr>
       
       <tr>
       <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;">Joining Date</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtStartDt" class="NormalText" runat="server" 
                    Width="15%" onkeypress="return false" ></asp:TextBox>
             <asp:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtStartDt" Format="dd MMM yyyy">
             </asp:CalendarExtender>
            </td>
       </tr>
       <tr>
       <td style="width:25%;"></td>
        <td style="width:12%; text-align:left;"></td>
            <td style="width:63%">
                &nbsp;&nbsp;<asp:CheckBox ID="chkNeedApproval" runat="server" 
                    Text=" Screening Required" Font-Bold="True" ForeColor="#CC0000"  />
            </td>
       </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

