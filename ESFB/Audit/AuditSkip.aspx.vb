﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AuditSkip
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim AUD As New Audit
    Dim CallBackReturn As String = Nothing
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 67) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Skip Audit"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            hid_Dtls.Value = Nothing
            hid_temp.Value = Nothing
            Dim StrAud As String = AUD.Audit_Skip
            If StrAud <> "" Then
                hid_Dtls.Value = StrAud
            Else
                hid_Dtls.Value = ""
            End If
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", " table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))

        Dim UserID As Integer = CInt(Session("UserID"))

        If RequestID = 1 Then

            Dim Datas As String = CStr(Data(1))

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try

                Dim Params(2) As SqlParameter
                
                Params(0) = New SqlParameter("@Data", SqlDbType.VarChar, 5000)
                Params(0).Value = Datas
                Params(1) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(1).Direction = ParameterDirection.Output
                Params(2) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(2).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_AUDITSKIP", Params)
                ErrorFlag = CInt(Params(1).Value)
                Message = CStr(Params(2).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        End If



    End Sub
#End Region
End Class
