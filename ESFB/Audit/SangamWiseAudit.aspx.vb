﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class SangamWiseAudit
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTComments As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            Dim AuditID As Integer = CInt(Data(1))
            Dim LoanNo As String = "0"
            If Data(2).ToString() <> "-1" Then
                DT = AD.GetOneLoanAccounts(Data(2), AuditID)
                LoanNo = DT.Rows(0).Item(0).ToString()
            
            End If
            Dim ObservationDtl As String = Data(3)
            If ObservationDtl <> "" Then
                ObservationDtl = ObservationDtl.Substring(1)
            End If
            Dim CloseFlag As Integer = CInt(Data(4))
            Dim GeneralComments As String = Data(5)
            Dim SeriousComments As String = Data(6)
            Dim ManagementNote As String = Data(7)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(8) As SqlParameter
                Params(0) = New SqlParameter("@AuditID", SqlDbType.Int)
                Params(0).Value = AuditID
                Params(1) = New SqlParameter("@LoanNo", SqlDbType.VarChar, 20)
                Params(1).Value = LoanNo
                Params(2) = New SqlParameter("@ObservationDtl", SqlDbType.VarChar)
                Params(2).Value = ObservationDtl
                Params(3) = New SqlParameter("@CloseFlag", SqlDbType.Int)
                Params(3).Value = CloseFlag
                Params(4) = New SqlParameter("@GeneralComments", SqlDbType.VarChar, 2000)
                Params(4).Value = GeneralComments
                Params(5) = New SqlParameter("@SeriousComments", SqlDbType.VarChar, 2000)
                Params(5).Value = SeriousComments
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@ManagementNote", SqlDbType.VarChar, 5000)
                Params(8).Value = ManagementNote
                DB.ExecuteNonQuery("SP_AuditObservation_LoanWise", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            DT = AD.GetAuditAccountsSangamWise(CInt(Data(1)), CInt(Data(2)), CInt(Data(3)))
            For n As Integer = 0 To DT.Rows.Count - 1
                CallBackReturn += DT.Rows(n)(0).ToString() & "µ" & DT.Rows(n)(1).ToString() & "µ" & DT.Rows(n)(2).ToString() & "µ" & DT.Rows(n)(3).ToString() & "µ" & DT.Rows(n)(4).ToString() & "µ" & DT.Rows(n)(5).ToString() & "µ" & DT.Rows(n)(6).ToString() & "µ" & DT.Rows(n)(7).ToString() & "µ" & DT.Rows(n)(8).ToString() & "µ" & DT.Rows(n)(9).ToString() & "µ" & DT.Rows(n)(10).ToString() & "µ" & DT.Rows(n)(11).ToString() & "µ" & DT.Rows(n)(12).ToString() & "µ" & DT.Rows(n)(13).ToString()
                If n < DT.Rows.Count - 1 Then
                    CallBackReturn += "¥"
                End If
            Next
            'ElseIf CInt(Data(0)) = 3 Then
            '    DT = AD.GetSangamAccounts(CInt(Data(2)))
            '    For Each DR As DataRow In DT.Rows
            '        CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            '    Next
        End If
    End Sub
    Protected Sub SangamWiseAudit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 36) = False Then
                Response.redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Audit Observations"
            Dim AuditID As Integer
            Dim AuditType As Integer
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            AuditID = CInt(GN.Decrypt(Request.QueryString.Get("AuditID")))
            hdnAuditID.Value = AuditID.ToString()
            AuditType = CInt(Request.QueryString.Get("AuditType"))
            hdnSangamVisitCnt.Value = "0"
            '----------------------
            If AuditType = 4 Then
                Dim DTCnt As New DataTable
                Dim DT_Group As New DataTable
                Dim GroupID As Integer = 0

                DT_Group = DB.ExecuteDataSet("select distinct group_id from AUDIT_DTL where audit_id=" & AuditID.ToString() & "").Tables(0)
                GroupID = CInt(DT_Group.Rows(0)(0))
                DTCnt = DB.ExecuteDataSet("select isnull(count(distinct b.CENTER_ID),0) from AUDIT_ACCOUNTS a,AUDIT_DTL E, " & _
                   " AUDIT_MASTER D ,  LOAN_MASTER b,CENTER_MASTER  c LEFT JOIN (SELECT GROUP_ID,J.EMP_NAME + '/'+ CONVERT(VARCHAR(20),G.EMP_CODE) + ' - '+ convert(varchar,g.visited_dt) AS EMP_name,g.emp_code FROM  AUDIT_VISITED_SANGAM_MASTER F  LEFT JOIN AUDIT_VISITED_SANGAM_DTL G ON  " & _
                   " F.VISITED_ID=G.VISITED_ID LEFT JOIN EMP_MASTER J ON G.EMP_CODE=J.EMP_CODE  where  AUDIT_GROUP_ID=" & GroupID.ToString() & ")s  ON C.CENTER_ID=S.GROUP_ID WHERE " & _
                   " A.AUDIT_ID=E.AUDIT_ID AND E.GROUP_ID=D.GROUP_ID AND a.LOAN_NO=b.LOAN_NO and b.CENTER_ID=c.CENTER_ID and e.audit_type=4 and s.emp_code is null and d.GROUP_ID=" & GroupID.ToString() & " ").Tables(0)
                hdnSangamVisitCnt.Value = DTCnt.Rows(0)(0).ToString()

            End If
            '----------------------
            DTComments = AD.GetAuditGeneralComments(AuditID)
            If DTComments.Rows.Count > 0 Then
                Me.hdnComments.Value = DTComments.Rows(0)(0).ToString() + "Ø" + DTComments.Rows(0)(1).ToString()
            End If
            hdnAuditTypeID.Value = AuditType.ToString()
            DT = AD.GetAuditDtl(AuditID)
            lblBranch.Text = DT.Rows(0)(1).ToString()
            lblStartDate.Text = DT.Rows(0)(2).ToString()
            lblAuditType.Text = DT.Rows(0)(3).ToString()
            GN.ComboFill(cmbSangam, AD.getCenters(AuditID), 0, 1)
            DT = AD.GetCheckGroup(AuditType)
            hid_dtls.Value = "Ø"
            For Each DR As DataRow In DT.Rows
                hid_dtls.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = AD.Get_Audit_Summary(AuditType, AuditID)
            If Not IsNothing(DT) Then
                hdnCenterCount.Value = DT.Rows(0)(0).ToString() + "~" + DT.Rows(0)(1).ToString() + "~" + DT.Rows(0)(2).ToString() + "~" + DT.Rows(0)(3).ToString()
            Else
                hdnCenterCount.Value = "0~0~0~0"
            End If
            cmd_viewReport.Attributes.Add("onclick", " return viewReport()")
            cmbSangam.Attributes.Add("onchange", "CenterOnChange()")

            txtManagementNote.Text = AD.GetManagementNote(AuditID)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
