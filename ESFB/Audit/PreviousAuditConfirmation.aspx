﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PreviousAuditConfirmation.aspx.vb" Inherits="PreviousAuditConfirmation" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">

<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
     <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>

   <link rel="stylesheet" href="../Style/bootstrap-3.1.2.minNew.css" type="text/css" />
		<link rel="stylesheet" href="../Style/bootstrap-multiselectNew.css" type="text/css" />
		<script type="text/javascript" src="../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../Script/bootstrap-multiselectNew.js"></script>
   

    <script type="text/javascript">
        $(document).ready(function () {
            $('#chkveg').multiselect();
            buttonWidth: '500px'
        }); 0
</script>

    <style type="text/css">
         .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#3F719A;
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #B0C4DE 90%, #F1F3F4 100%);
           
        }
        
        .Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #3F719A 0%, #B8C4C8 0%, #3F719A  100%);
            color:#FFF;
        }   
       
       
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>

    <script language="javascript" type="text/javascript">
             var Remarks="";
         $(function() {
			          $('#chkveg').multiselect({                                            
			          includeSelectAllOption: true
                                        
			          });
			          $('#btnget').click(function() {
			          alert($('#chkveg').val());
			           })
			      });

       function cursorwait(e) {
    document.body.className = 'wait';
}
   function cursordefault(e) {
    document.body.className = 'default';
}
        function FromServer(arg, context) {
        
        document.getElementById("btnSave").disabled =false;
             if (context == 1) { cursordefault();
                var Data = arg.split("Ø");
                alert(Data[1]);
                document.getElementById("chkCloseAudit").checked = false;
                if (Data[0] == 0) window.open("PreviousAuditConfirmation.aspx", "_self");              

            }  
            else if  (context == 2){
            
                if (arg == ""){
                    ClearCombo("<%= cmbAudit.ClientID %>");
                    alert ("No Pending Audits"); return false;
                }
                else{
                    ComboFill(arg,"<%= cmbAudit.ClientID %>");
                }
            }     
            else if  (context == 3){
               document.getElementById("<%= hdnItemDtl.ClientID %>").value=arg;  
               var ArgDtl=arg.split("Ø");
               document.getElementById("<%= hdnDtl.ClientID %>").value=ArgDtl[0];    
               cursordefault();     
               document.getElementById("<%= pnObservDetails.ClientID %>").style.display="none";  
               document.getElementById("<%= hdnTotalCnt.ClientID %>").value = ArgDtl[2];  
               document.getElementById("<%= hdnSavedCnt.ClientID %>").value = ArgDtl[3];              
               UpdateItem(ArgDtl[1]); 

               cursordefault();
            }
        }
        
        function FillObservationDetails(Datas)
        {           
                          
            document.getElementById("<%= hid_dtls.ClientID %>").value = Datas;
                           
            var Dtl = document.getElementById("<%= hid_dtls.ClientID %>").value.split("ÿ");
            var tab = "";        
            tab += "<div style='width:100%; height:auto; margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left' class=mainhead>";
            tab += "<tr class='tblBody' height=30px;>"; 
            tab += "<td style='width:5%;  text-align:center' >#</td>";          
            tab += "<td style='width:8%;  text-align:center' >Ref No</td>";
            tab += "<td style='width:10%;  text-align:center'>Client Name</td>";
            tab += "<td style='width:10%; text-align:center'>Center Name</td>"; 
            tab += "<td style='width:6%; text-align:center'>Date</td>";   
            tab += "<td style='width:4%; text-align:center'>Amount</td>";      
            tab += "<td style='width:10%;  text-align:left'>Check List</td>";
            tab += "<td style='width:10%; text-align:center'>Audit Observation</td>";
            tab += "<td style='width:10%;  text-align:center'>Last Remarks</td>";
            tab += "<td style='width:4%;  text-align:right'>Type</td>";
            tab += "<td style='width:4%;  text-align:right'>Staffs Invld</td>";
            tab += "<td style='width:14%;  text-align:center'>Remarks</td>";
            tab += "<td style='width:5%; text-align:center'>Respond&nbsp;</td>";  
            tab += "</tr>"; 
            
            if(document.getElementById("<%= hid_dtls.ClientID %>").value!="ÿ~~~~~~~~~~~~~~")    
            {    
//            tab += "<table style='width:100%;  margin:0px auto;font-family:'cambria';' align='left'>";
            var row_bg = 0;
            var i=0;
            
            for (a = 0; a < Dtl.length; a++) 
            {     
                    var ObserveDtl = Dtl[a].split("~");
                    i+=1;
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_first'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr  style='text-align:left; padding-left:20px; ' class='sub_second'>";
                    }
                     
                    tab += "<td style='width:5%;  text-align:center'>"+ i +"</td>";
                    tab += "<td style='width:8%;  text-align:left' ><a href=Reports/viewLoanDetails.aspx?LoanNo=" +  btoa(ObserveDtl[6]) + " target=_blank>"+ ObserveDtl[6] +"</a></td>";
                    tab += "<td style='width:10%;  text-align:left'>"+ ObserveDtl[8] +"</td>";
                    tab += "<td style='width:10%;  text-align:left'>"+ ObserveDtl[10] +"</td>";
                    tab += "<td style='width:6%;  text-align:left'>"+ ObserveDtl[14] +"</td>";  
                    var msg;
                        msg = ObserveDtl[0]  ;         
                    if(ObserveDtl[15]!="")                 
                        tab += "<td style='width:4%;  text-align:right'>"+parseFloat(ObserveDtl[15]).toFixed(0) +"</td>";
                    else
                        tab += "<td style='width:4%;  text-align:right'></td>";
                    tab += "<td style='width:13%; text-align:left'>"+ ObserveDtl[9] +"</td>";
                    tab += "<td style='width:12%;  text-align:left'>"+ ObserveDtl[4] +"</td>";
                    tab += "<td style='width:15%;  text-align:left'>"+ ObserveDtl[11] + "&nbsp;<a href =Reports/viewPrevRemarksReport.aspx?AuditID=" + btoa(ObserveDtl[1]) + " target=_blank>Previous</a>" +"</td>";

                    tab += "<td style='width:4%;  text-align:center'>"+ ObserveDtl[12] +"</td>";
                    
                    if(ObserveDtl[13]!="0")
                        tab += "<td style='width:4%;  text-align:center'><a href='Reports/viewStaffInvolvement.aspx?SINo="+ btoa(ObserveDtl[1]) +"' target=blank style='cursor:pointer;'>"+ ObserveDtl[13] +"</a></td>";
                    else
                        tab += "<td style='width:4%;  text-align:center'>-</td>";

                        
                    var FirstLevel=1;
                    if(ObserveDtl[12]=="S")
                            FirstLevel=2;
                    else if (ObserveDtl[12]=="VS")
                        FirstLevel=3;
                       
                    
                    var select = "<select id='cmb" + a + "' class='NormalText' name='cmb" + a + "' >";
                            select += "<option value='-1'>---- Select ----</option>";
                            select += "<option value='1'>Satisfied</option>";
                            select += "<option value='2'>Not Satisfied</option>";  
                    var txtBox1 = "<textarea id='txtRemarks" + a + "' name='txtRemarks" + a + "' type='Text' style='width:99%; height=200px;' class='NormalText'  maxlength='1000' onkeypress='return TextAreaCheck(event)'  ></textarea>";
                    tab += "<td style='width:14%;  text-align:left'>"+ txtBox1 +"</td>"; 
                    tab += "<td style='width:5%;  text-align:center'>"+ select +"</td>"; 
                    
                    tab += "</tr>"; 
                    
            }
            tab += "</table></div>";
            }
            
            document.getElementById("<%= pnObservDetails.ClientID %>").innerHTML=tab; 
            document.getElementById("<%= pnObservDetails.ClientID %>").style.display="";          
//            cursordefault();
        }
        function BranchOnchange() {     
         document.getElementById("<%= pnObservDetails.ClientID %>").style.display="none";   
         
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;
            if (BranchID != -1) {
                var Data = "2Ø" + BranchID;
                ToServer(Data, 2);
            }
           
        }
        function AuditOnChange() { 
        cursorwait();
         document.getElementById("<%= pnObservDetails.ClientID %>").style.display="none"; 
         document.getElementById("<%= hdnItemDtl.ClientID %>").value ="";
         UpdateItem("");
            var AuditID = document.getElementById("<%= cmbAudit.ClientID %>").value;
            if (AuditID != -1) {
                var Data = "3Ø" + AuditID;
                ToServer(Data, 3);
            }
           
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
             var option = document.createElement("OPTION");
             option.value = -1;
             option.text = "----------Select----------";
             document.getElementById(ddlName).add(option);
            var rows = data.split("Ñ");
            

            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
      function FilterOnClick()
        {       
          var SelectedItems =$('#chkveg').val(); 
                 
          if(SelectedItems!=null)
          {
            var Dtl=document.getElementById("<%= hdnItemDtl.ClientID %>").value.split("Ø");
                    
            var row = Dtl[0].split("ÿ");       
            document.getElementById("<%= hdnDtl.ClientID %>").value="";         
            for (n = 0; n <= row.length - 1; n++) {
                    col = row[n].split("~");
                    
            if(SelectedItems.indexOf(col[7])>-1){
                 document.getElementById("<%= hdnDtl.ClientID %>").value+= "ÿ"+  row[n];              
                }
         
          }
          
           document.getElementById("<%= hdnDtl.ClientID %>").value = document.getElementById("<%= hdnDtl.ClientID %>").value.substring(1); 
         }
         else
         {document.getElementById("<%= hdnDtl.ClientID %>").value="ÿ~~~~~~~~~~~~~~";}
               
         FillObservationDetails(document.getElementById("<%= hdnDtl.ClientID %>").value);
        }
        
          function UpdateItem(arg){  
                
            jQuery('#chkveg').multiselect('dataprovider', []);
			    var dropdown2OptionList = [];			          
			    var Dtl = arg;
			    var rrr = Dtl.split("Ñ");
			     for (a = 1; a <= rrr.length - 1; a++) {
			            var cols = rrr[a].split("ÿ");
			            var ddd = cols[0].split("~");	                       	         
			                dropdown2OptionList.push({
			                    'label': cols[1],
			                    'value': cols[0]                             
			                });			            
			     
			       }
			        jQuery('#chkveg').multiselect('dataprovider', dropdown2OptionList);
                    jQuery('#chkveg').multiselect({
			        includeSelectAllOption: true
                    });
                   
        }
   
         function FinalStringForSave() {
           
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            
            if(document.getElementById("<%= hid_dtls.ClientID %>").value != "ÿ~~~~~~~~~~~~~~")
            {
                        row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("ÿ");
            
                        var i=0;
                        for (n = 0; n <= row.length-1; n++) 
                        {
                
                                    col = row[n].split("~");               
                        
                                    if (document.getElementById("cmb" + n).value != -1) 
                                    {
                                               if (document.getElementById("txtRemarks" + n).value =="" && document.getElementById("cmb" + n).value == 2) {
                                                    document.getElementById("<%= hid_temp.ClientID %>").value ="2";
                                                    return false;
                                               }
                                                document.getElementById("<%= hid_temp.ClientID %>").value +=  col[1] + "~" + document.getElementById("cmb" + n).value + "~" + document.getElementById("txtRemarks" + n).value +"ÿ";
                                                i = i+1;
                                    }
                        }
                            
                        document.getElementById("<%= hdnSelectedCnt.ClientID %>").value = i;
               }
                return true;

        }    

          function closeOnClick()
        {   FinalStringForSave();
            if (document.getElementById("chkCloseAudit").checked == true) {
                var Savedcnt =Math.abs(document.getElementById("<%= hdnSavedCnt.ClientID %>").value);
                
               
                var Selectedcnt =Math.abs(document.getElementById("<%= hdnSelectedCnt.ClientID %>").value);
                
                if (document.getElementById("<%= hdnTotalCnt.ClientID %>").value > Math.abs(Savedcnt)+Math.abs(Selectedcnt)) {
                    var PendingCnt = Math.abs(document.getElementById("<%= hdnTotalCnt.ClientID %>").value) -(Math.abs(Savedcnt)+Math.abs(Selectedcnt));
                    alert("Confirmation checking is Pending for "+ PendingCnt +" observations.Closing is only allowed after the updation"); document.getElementById("chkCloseAudit").checked = false; return false;
                } 
           
            }
        }

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
             

    function btnSave_onclick() {
         if (document.getElementById("chkCloseAudit").checked == true) 
         {

                if(confirm("Are you sure to Close?")==0){ 
                
                   return false;
                }
                
                    FinalStringForSave();
                    
//                    if (document.getElementById("<%= hid_temp.ClientID %>").value =="" ) {
//                        alert("Select Atleast one ");
//                        return false;
//                    }
                if (document.getElementById("<%= hid_temp.ClientID %>").value == 2){
                    alert("Enter remarks ");
                    return false;
                }
        
	            var ToData = "1Ø" + document.getElementById("<%= hid_temp.ClientID %>").value +"Ø" + document.getElementById("<%= cmbBranch.ClientID %>").value + "Ø1Ø" + document.getElementById("<%= cmbAudit.ClientID %>").value ;
                    
	            document.getElementById("btnSave").disabled =true;
                cursorwait();
                ToServer(ToData, 1);
                
            }
            else
            { 
             FinalStringForSave();
             
                var Savedcnt =Math.abs(document.getElementById("<%= hdnSavedCnt.ClientID %>").value);
                var Selectedcnt =Math.abs(document.getElementById("<%= hdnSelectedCnt.ClientID %>").value);
                
                var Tot = Math.abs(Savedcnt)+Math.abs(Selectedcnt);
                var TotalCount =Math.abs(document.getElementById("<%= hdnTotalCnt.ClientID %>").value);
               

                if (TotalCount <= Tot)
                    {
                        if(confirm("Do you want to save?")==1) 
                        {    
                         if(confirm("Confirmation count exceeded.Are You sure to save with out closing?")==0) 
                          { 
                               return false;
                          } 
                       
                            if (document.getElementById("<%= hid_temp.ClientID %>").value =="" ) 
                            {
                                alert("Select Atleast one ");
                                return false;
                            }
                            
                            if (document.getElementById("<%= hid_temp.ClientID %>").value == 2)
                            {
                                alert("Enter remarks ");
                                return false;
                            }
                            var ClosedFlag = (document.getElementById("chkCloseAudit").checked == true)? 1 : 0;         
	                        var ToData = "1Ø" + document.getElementById("<%= hid_temp.ClientID %>").value +"Ø" +document.getElementById("<%= cmbBranch.ClientID %>").value + "Ø" + ClosedFlag + "Ø" +document.getElementById("<%= cmbAudit.ClientID %>").value;
                        
	                        document.getElementById("btnSave").disabled =true;
                            cursorwait();            
                            ToServer(ToData, 1);
                        }
                        else{
                        return false;
                        }
                                
                }
                else
                {
                        if (document.getElementById("<%= hid_temp.ClientID %>").value =="" ) 
                        {
                            alert("Select Atleast one ");
                            return false;
                        }
                        else if (document.getElementById("<%= hid_temp.ClientID %>").value == 2)
                        {
                            alert("Enter remarks ");
                            return false;
                        }
                            if(confirm("Do you want to save?")==1) 
                        { 
	                    var ToData = "1Ø" + document.getElementById("<%= hid_temp.ClientID %>").value +"Ø" +document.getElementById("<%= cmbBranch.ClientID %>").value+"Ø0Ø" +document.getElementById("<%= cmbAudit.ClientID %>").value;
	                    document.getElementById("btnSave").disabled =true;
                         cursorwait();           
                        ToServer(ToData, 1);
                        }
                        else{
                        return false;
                        }
                }
                        
            }
    }
    function ViewReport(){
    
        if(document.getElementById("<%= cmbAudit.ClientID %>").value != "-1")
        {
            window.open("Reports/viewMisleadObservationStatusReport.aspx?GroupID=" + btoa(document.getElementById("<%= cmbAudit.ClientID %>").value), "_blank");
        }
    }
    </script>
</head>
</html>
<asp:HiddenField ID="hid_Dtls" runat="server" />
<asp:HiddenField ID="hid_temp" runat="server" />
                <asp:HiddenField ID="hdnItemDtl" runat="server" />
                <asp:HiddenField ID="hdnDtl" runat="server" />
                <asp:HiddenField ID="hdnTotalCnt" runat="server" />
                <asp:HiddenField ID="hdnSavedCnt" runat="server" />

                <asp:HiddenField ID="hdnSelectedCnt" runat="server" />

 <table class="style1" style="width:100%; text-align:right; margin:0px auto;"> 
   
     <tr>
            <td style="width:30%; text-align:right;">
                Branch</td>
            <td style="width:70%; text-align:left;">
                &nbsp;&nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                </asp:DropDownList></td>
        </tr>
     <tr>
            <td style="width:30%; text-align:right;">
                Audit</td>
            <td style="width:70%; text-align:left;">
                 &nbsp;&nbsp;<asp:DropDownList ID="cmbAudit" class="NormalText" runat="server" Font-Names="Cambria" 
                    Width="90%" ForeColor="Black">
                     <asp:ListItem Value="-1">----------Select----------</asp:ListItem>
                </asp:DropDownList> <img id="cmd_Add"  src="../Image/viewReport.png" style="height:20px;width:20px;" title="View" onclick='ViewReport()' /></td>
        </tr>  
        <tr>
            <td style="width:30%; text-align:right;">
                Check List</td>
            <td style="width:70%; text-align:left;">
                &nbsp;&nbsp;<select id='chkveg'  multiple='multiple' style='width:90%;' onchange='FilterOnClick()'></select></td>
        </tr>
        <tr>
            <td style="width:100%; text-align:center;" colspan="4">
                <input id="chkCloseAudit" title="Completed" type="checkbox"  onclick="closeOnClick()" style="border-style: none" 
                    />&nbsp;&nbsp;Completed</td>
           
        </tr>
      <tr>
            <td style="text-align:left;" colspan="2">
                <asp:Panel ID="pnObservHead" runat="server">
                </asp:Panel>
               </td>
        </tr>   
     <tr id="RowNew"> 
            <td colspan="2" >
              <asp:Panel ID="pnObservDetails" runat="server" Width="100%" style="text-align:center; margin:0px auto;">                           
            </asp:Panel></td>
     </tr> 
         
     <tr>
        <td style="text-align:center;"  colspan="2" >
            <br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
    </tr>
     
 </table> 

 <br /> <br /> <br />
</asp:Content>