﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="AuditGrading.aspx.vb" Inherits="AuditGrading" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">

<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
         #Button
        {
            width:80%;
            height:20px;
            font-weight:bold;
            line-height:20px;
            text-align:center;
            border-top-left-radius: 25px;
	        border-top-right-radius: 25px;
	        border-bottom-left-radius: 25px;
	        border-bottom-right-radius: 25px;
            cursor:pointer;
           background: -moz-radial-gradient(center, ellipse cover, #801424 0%, #B21C32 0%, #801424 100%);
           color:#FFF;
        }
        
        #Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
        }  
       
     
     .bg
     {
         background-color:#FFF;
     }
           
    </style>
    <script language="javascript" type="text/javascript">
        function ViewReport(AuditID, AuditType) {
            window.open("UpdateGradingSpecification.aspx?RptID=" + btoa(2), "_self");
            return false;
        }
        function table_fill() 
        {
            document.getElementById("<%= pnDisplay.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=30px;>";      
            tab += "<td style='width:5%;text-align:center' >#</td>";    
            tab += "<td style='width:25%;text-align:left' >Grading Factor</td>";         
            tab += "<td style='width:50%;text-align:left' >Description</td>";
            tab += "<td style='width:10%;text-align:center'>Limit Type</td>";
            tab += "<td style='width:5%;text-align:right'>Limit</td>";              
            tab += "<td style='width:5%;text-align:right'>Marks</td>";   
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:80%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";  
            
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
          
                row = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var TotalMarks=0;
                var Category="";
                var TotalFlag=0;
                for (n = 0; n <= row.length - 1; n++) {
                   col = row[n].split("µ");                 
                   if(col[7]==5 && TotalFlag==0)
                   {                     
                        tab += "<tr class='mainhead'; style='text-align:center; height:21px; padding-left:20px;'>";                 
                        tab += "<td style='width:5%;text-align:center' ></td>";
                        tab += "<td style='width:25%;text-align:left' >TOTAL</td>";
                        tab += "<td style='width:50%;text-align:left'></td>"; 
                        tab += "<td style='width:10%;text-align:left'></td>";  
                        tab += "<td style='width:5%;text-align:left'></td>"; 
                   //  var txtBox = "<textarea id='txtMarksTotal' name='txtMarksTotal' style='width:99%; text-align:right; font-weight:bold; color:black; resize: none;' rows=1 maxlength='3' disabled=true >"+TotalMarks+"</textarea>";                 
                        tab += "<td id='txtMarksTotal' style='width:5%;text-align:right; font-weight:bold'>"+ TotalMarks +"</td>";  
                        tab += "</tr>";
                        TotalFlag=1;
                   }
                    if(Category!=col[4])
                    {
                     
                         tab += "<tr class='sub_second'; style='text-align:center; min-height:30px; padding-left:20px;'>";
                         tab += "<td style='width:100%;text-align:left; font-size:12pt; font-family:cambria;' colspan=6 >" + col[4]  + "</td></tr>";
                    }
                    Category=col[4];
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class='sub_first'; style='text-align:center; min-height:50px; padding-left:20px;'>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class='sub_first'; style='text-align:center; min-height:50px; padding-left:20px;'>";
                    }
                    i = n + 1;                                      
                    tab += "<td style='width:5%;text-align:center' >" + i  + "</td>";
                    tab += "<td style='width:25%;text-align:left'>" + col[1] + "</td>"; 
                    tab += "<td style='width:50%;text-align:left'>" + col[2] + "</td>"; 
                    var LImitValue=0;                  
                    var Type="Percentage";
                    if(col[5]>1)
                    {                      
                        LImitValue=col[5];
                        Type="Percentage";
                    }
                    else
                    {
  
                        LImitValue=col[6];
                        Type="Number";
                    }                                                  
                    tab += "<td style='width:10%;text-align:center'>" + Type + "</td>"; 
                    //var txtBox1 = "<textarea id='txtPerc" + col[0] + "' name='txtPerc" + col[0]  + "'  style='width:99%; text-align:right;resize: none; border:none;'  maxlength='3' onkeypress='NumericCheck(event)'  onkeyup='return LimitOnKeyUp(this.id,"+ col[0] +");'   >"+LImitValue+"</textarea>";                 
                    tab += "<td style='width:5%;text-align:center'>"+ LImitValue +"</td>";
                   // var txtBox = "<textarea id='txtMarks" + col[0] + "' name='txtMarks" + col[0]  + "'  style='width:99%; text-align:right;resize: none; border:none;'  maxlength='3' onkeypress='NumericCheck(event)' onkeyup='return CalcTotal(this.id);'  >"+col[3]+"</textarea>";                 
                    tab += "<td style='width:5%;text-align:center'>"+ col[3] +"</td>";  
                    tab += "</tr>";
                   
                }               
                
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnDisplay.ClientID %>").innerHTML = tab;
            
            //--------------------- Clearing Data ------------------------//


        }


        function ProcessOnClick() {
            if (confirm("Are you sure to process Grading?") == true) {
                document.getElementById("RowNew").style.display = "none";
                document.getElementById("<%= btnProcess.ClientID %>").style.display = "none";
                var FromDt=document.getElementById("<%= txtPeriodFrom.ClientID %>").value;
                var ToDt=document.getElementById("<%= txtPeriodTo.ClientID %>").value;
                document.getElementById("<%= hid_temp.ClientID %>").value=FromDt+"~"+ToDt;
                return true;
            }
            else {return false;
            }
        }

        function btnExit_onclick() {
               window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

    </script>
</head>
</html>
<asp:HiddenField ID="hid_Dtls" runat="server" />
<asp:HiddenField ID="hid_temp" runat="server" />
<br />


 <table class="style1" style="width:80%; text-align:right; margin:0px auto;">   
     <tr > 
            <td style="width:40%; text-align:right;" >
                &nbsp;</td>
            <td style="width:20%">
                &nbsp;</td>
            <td style="width:40%; text-align:left;">
                &nbsp;</td>
     </tr> 
     <tr > 
            <td style="width:40%; text-align:right;" >
                Grading Period From :</td>
            <td style="width:20%">
                <asp:TextBox ID="txtPeriodFrom" runat="server" style="width:90%;" 
                    ReadOnly="True" class="ReadOnlyTextBox"></asp:TextBox><asp:CalendarExtender ID="CEFromDate" runat="server" TargetControlID="txtPeriodFrom" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
            <td style="width:40%; text-align:left;">
                &nbsp;</td>
     </tr> 
     <tr > 
            <td style="width:40%; text-align:right;" >
                Last Process Date : </td>
            <td style="width:20%">
                <asp:TextBox ID="txtPeriodTo" runat="server" style="width:90%;" 
                    ReadOnly="True" class="ReadOnlyTextBox"></asp:TextBox><asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtPeriodTo" Format="dd MMM yyyy"></asp:CalendarExtender>
            </td>
            <td style="width:40%; text-align:left;">
                <asp:UpdatePanel ID="updPanel1" runat="server"  style="text-align:left; ">
                    <ContentTemplate>
                        <asp:Button ID="btnProcess" runat="server" Text="PROCESS" Font-Names="Cambria" 
                            Font-Size="11pt" Width="20%" />
                        &nbsp;
               </ContentTemplate>
                </asp:UpdatePanel></td>
     </tr> 
     <tr > 
            <td style="width:40%; text-align:right;" >
                &nbsp;</td>
            <td style="width:60%" colspan="2">
                &nbsp;</td>
     </tr> 
  
     <tr > 
            <td style="width:40%; text-align:right;" >
                &nbsp;</td>
            <td style="width:60%" colspan="2">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                    AssociatedUpdatePanelID="updPanel1">
                       <ProgressTemplate>           
            <img alt="progress" src="../image/Progress.gif" style="width:60px;height:60px"/>
               Processing...           
            </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
     </tr>         
         
         
     <tr id="RowNew"> 
            <td colspan="3" >
              <asp:Panel ID="pnDisplay" runat="server" Width="100%" style="text-align:center; margin:0px auto;">                           
            </asp:Panel></td>
     </tr>            
         
         
     <tr> 
            <td colspan="3" >
                &nbsp;</td>
     </tr>            
         
         
     <tr> 
            <td colspan="3"  style="text-align:center;">
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
     </tr>            
 </table> 

 <br /> <br /> <br />
</asp:Content>