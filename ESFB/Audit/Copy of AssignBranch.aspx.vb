﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class AssignBranch
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim AUD As New Audit
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Assign Branch"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            hid_dtls.Value = CStr(1)
            Me.txtEmpcode.Attributes.Add("onchange", "return RequestID()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "RequestOnchange();", True)
            txtEmpcode.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))

        Dim UserID As Integer = CInt(Session("UserID"))


        If CInt(Data(0)) = 1 Then
            'Dim statusID          INT,--0-update emp_master,otherwise-1
            Dim BranchData As String = CStr(Data(2))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@EmpID", SqlDbType.Int)
                Params(0).Value = EmpID
                Params(1) = New SqlParameter("@BranchData", SqlDbType.VarChar, 5000)
                Params(1).Value = BranchData
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                DB.ExecuteNonQuery("SP_AUDIT_ASSIGN_BRANCH", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            If CInt(Data(1)) > 0 Then
                DT_EMP = AUD.GET_EMP_DEATILS(CInt(Data(1)))

                Dim Strbranch As String
                DT = AUD.Getexist_branch(CInt(Data(1)))
                Strbranch = AUD.GetStr_branch(DT)

                If DT_EMP.Rows.Count > 0 Then
                    CallBackReturn = "1~" + CStr(DT_EMP.Rows(0).Item(0)) + "~" + Strbranch
                Else
                    CallBackReturn = "2~" + "Invalid Employee Code!"
                End If

            Else

            End If
       
        End If


    End Sub
#End Region
End Class
