﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" EnableEventValidation="false" CodeFile="UATSignOffActivity.aspx.vb" Inherits="UATSignOffActivity" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }

        .style1 {
            width: 10%;
            height: 17px;
        }

        .style2 {
            width: 30%;
            height: 17px;
        }

        .style3 {
            width: 50%;
            height: 17px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                document.getElementById("rwMail").style.display = "none";
                document.getElementById("rwMailHis").style.display = "none";
                document.getElementById("rwUATHis").style.display = "none";
                document.getElementById("rwMailSend").style.display = "none";
                document.getElementById("rwMailSendCC").style.display = "none";
                ToServer("1?" + document.getElementById("<%= hdnRequestID.ClientID %>").value, 1);
            }
            function FromServer(Arg, Context) {
                switch (Context) {

                    case 1:
                        {
                            if (Arg != "") {
                                var Data = Arg.split("£");
                                var Dtl = Data[0].split("µ");
                                document.getElementById("<%= txtReqNo.ClientID %>").value = Dtl[1];
                                document.getElementById("<%= txtEnvmnt.ClientID %>").value = Dtl[2];
                                document.getElementById("<%= txtApp.ClientID %>").value = Dtl[3];
                                document.getElementById("<%= txtType.ClientID %>").value = Dtl[4];
                                document.getElementById("<%= txtReqRemark.ClientID %>").value = Dtl[5];
                                document.getElementById("<%= txtPriority.ClientID %>").value = Dtl[6];
                                document.getElementById("<%= txtAssign.ClientID %>").value = Dtl[7];
                                document.getElementById("<%= txtStatus.ClientID %>").value = Dtl[8];

                                document.getElementById("<%= cmbActivity.ClientID %>").options.length = 0;
                                var Data1 = document.getElementById("<%= hid_dtls1.ClientID %>").value.split("¥");
                                var rows = Data1[1].split("Ř");

                                for (a = 1; a < rows.length; a++) {
                                    var cols = rows[a].split("Ĉ");
                                    var option1 = document.createElement("OPTION");
                                    option1.value = cols[0];
                                    option1.text = cols[1];
                                    document.getElementById("<%= cmbActivity.ClientID %>").add(option1);
                                }

                                var i = 0;
                                if (Data[1] != "") {
                                    var tab = "";
                                    var row_bg = 0;
                                    var subHead = "";
                                    if ((document.getElementById("<%= hid_lvlNo.ClientID %>").value) == '1')
                                        subHead = 'TEAM LEAD REVIEW HISTORY';
                                    else if ((document.getElementById("<%= hid_lvlNo.ClientID %>").value) == '2')
                                        subHead = 'REQUESTOR REVIEW HISTORY';
                                    else if ((document.getElementById("<%= hid_lvlNo.ClientID %>").value) == '3')
                                        subHead = 'RO REVIEW HISTORY'
                                    else if ((document.getElementById("<%= hid_lvlNo.ClientID %>").value) == '4')
                                        subHead = 'IT HEAD APPROVAL HISTORY'

                            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
                            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                    //tab += "<tr class=sub_second><td style='width:100%;text-align:center;' colspan=8>ACTIVITY HISTORY</td></tr>";
                            tab += "<tr class=sub_second><td style='width:100%;text-align:center;' colspan=8>" + subHead + "</td></tr>";
                            tab += "<tr>";
                            tab += "<td style='width:3%;text-align:center;'>#</td>";
                            tab += "<td style='width:10%;text-align:center' >Activity</td>";
                            tab += "<td style='width:10%;text-align:center'>Comments</td>";
                            //tab += "<td style='width:8%;text-align:center' >Attachments</td>";
                            tab += "<td style='width:8%;text-align:center' >Done Date</td>";
                            tab += "<td style='width:8%;text-align:center' >Done By</td>";
                                    //tab += "<td style='width:10%;text-align:center' >Mail Body</td>";
                            tab += "<td style='width:10%;text-align:center' >Mail Send To</td>";
                            tab += "<td style='width:8%;text-align:center' >Mail CC</td>";
                                    //tab += "<td style='width:10%;text-align:center' >Mail Send To</td>";
                            tab += "<td style='width:10%;text-align:center' >Mail Body</td>";


                            var row = Data[1].split("Ř");
                            for (n = 0; n < row.length - 1; n++) {
                                var Dtl = row[n].split("¥");
                                col = Dtl[0].split("Ĉ");

                                if (row_bg == 0) {
                                    row_bg = 1;
                                    tab += "<tr class=sub_first>";
                                }
                                else {
                                    row_bg = 0;
                                    tab += "<tr class=sub_second>";
                                }
                                i = n + 1;

                                tab += "<td style='width:3%;text-align:center;'>" + i + "</td>";
                                tab += "<td style='width:10%;text-align:left' >" + col[2] + "</td>";
                                tab += "<td style='width:15%;text-align:left;'>" + col[1] + "</td>";
                                //if (Dtl[1] != "") {
                                //    //-------------------------- Attachments
                                //    var subtab = "";
                                //    subtab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                //    subtab += "<tr class=sub_first>";
                                //    subtab += "<td style='width:5%;text-align:center;'>Sl No</td>";
                                //    subtab += "<td style='width:15%;text-align:left' >Document</td>";
                                //    subtab += "</tr>";

                                //    var docDtl = Dtl[1].split("Ø");
                                //    for (dx = 0; dx < docDtl.length - 1; dx++) {
                                //        var doc = docDtl[dx].split("Ĉ");
                                //        var sln = dx + 1;
                                //        subtab += "<tr class=sub_second>";
                                //        subtab += "<td style='width:5%;text-align:center;'>" + sln + "</td>";
                                //        subtab += "<td style='width:15%;text-align:left'><a href='ShowUATFormat.aspx?UatReqID=" + doc[1] + "&DocID=" + doc[0] + "&TestID=" + doc[2] + "&StatusID=2'>" + doc[3] + "</a></td>";
                                //        subtab += "</tr>";
                                //    }
                                //    subtab += "</table>";
                                //    tab += "<td style='width:20%;text-align:left' >" + subtab + "</td>";
                                //    //-----
                                //}
                                //else {
                                //    tab += "<td style='width:20%;text-align:left' ></td>";
                                //}
                                tab += "<td style='width:8%;text-align:left' >" + col[3] + "</td>";
                                tab += "<td style='width:8%;text-align:left' >" + col[4] + "</td>";
                                tab += "<td style='width:10%;text-align:left' >" + col[6] + "</td>";
                                tab += "<td style='width:8%;text-align:left' >" + col[7] + "</td>";
                                tab += "<td style='width:10%;text-align:left' >" + col[5] + "</td>";
                                tab += "</tr>";
                            }
                            tab += "</table></div></div></div>";

                                    /// New code
                            var row2 = Data[1].split("Ř");
                            var index = row2.length - 2;
                            var Dtl2 = row2[index].split("¥");
                            var col2 = Dtl2[0].split("Ĉ");
                            document.getElementById("<%= cmbActivity.ClientID %>").selectedIndex = col2[8]

                            document.getElementById("<%= pnlUATDtl.ClientID %>").innerHTML = tab;
                                    document.getElementById("rwUATHis").style.display = "";
                                }
                            }
                            break;
                        }
                    case 2:
                        {
                            if (Arg != "") {
                                var Data = Arg.split("Ø");
                                alert(Data[1]);
                                if (Data[0] == 0) window.open("UATSignOffAccess.aspx?Type=" + document.getElementById("<%= hid_lvlNo.ClientID %>").value, "_self");
                            }
                            break;
                        }
                }
            }

            function btnExit_onclick() {
                <%--window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");--%>
                window.open("UATSignOffAccess.aspx?Type=" + document.getElementById("<%= hid_lvlNo.ClientID %>").value, "_self");
            }

            function MailOnClick() {
                if (document.getElementById("<%= chkMail.ClientID %>").checked == true) {
                    document.getElementById("rwMail").style.display = "";
                    document.getElementById("rwMailSend").style.display = "";
                    document.getElementById("rwMailSendCC").style.display = "";
                }
                else {
                    document.getElementById("rwMail").style.display = "none";
                    document.getElementById("rwMailSend").style.display = "none";
                    document.getElementById("rwMailSendCC").style.display = "none";
                }
            }

            function btnSave_onclick() {
                if ((document.getElementById("<%= cmbActivity.ClientID %>").value != 1) && (document.getElementById("<%= txtComments.ClientID %>").value == "")) {
                    alert("Enter Reason");
                    document.getElementById("<%= txtComments.ClientID %>").focus();
                    return false;
                }
                else if (document.getElementById("<%= chkMail.ClientID %>").checked == true) {

                    if (document.getElementById("<%= txtSendto.ClientID %>").value == "") {
                        alert("Please enter send to");
                        document.getElementById("<%= txtSendto.ClientID %>").focus();
                        return false;
                    }

                    if (document.getElementById("<%= txtMailBody.ClientID %>").value == "") {
                        alert("Please enter Mail Body");
                        document.getElementById("<%= txtMailBody.ClientID %>").focus();
                        return false;
                    }
                    //                    if(document.getElementById("<%= txtCC.ClientID %>").value == "")
                    //                    {
                    //                        alert("Please enter CC");
                    //                        document.getElementById("<%= txtCC.ClientID %>").focus();
                    //                        return false;
                    //                    }  
                }
                var UatReqID = document.getElementById("<%= hdnRequestID.ClientID %>").value;
                var Status = document.getElementById("<%= cmbActivity.ClientID %>").value;
                var checkStatus = document.getElementById("<%= chkMail.ClientID %>").checked;
                var mailTo = document.getElementById("<%= txtSendto.ClientID %>").value;
                var mailCC = document.getElementById("<%= txtCC.ClientID %>").value;
                var mailBody = document.getElementById("<%= txtMailBody.ClientID %>").value;
                var Data = "2?" + UatReqID + "?" + document.getElementById("<%= txtComments.ClientID %>").value + "?" + Status + "?" + checkStatus + "?" + mailTo + "?" + mailCC + "?" + mailBody;
                ToServer(Data, 2);
            }


            ////            function btnReject_onclick() {         
            ////                if (document.getElementById("<%= txtComments.ClientID %>").value == "") {
            ////                    alert("Enter Rejected Reason");
            ////                    document.getElementById("<%= txtComments.ClientID %>").focus();
            ////                    return false;
            ////                }
            ////                 var UatReqID = document.getElementById("<%= hdnRequestID.ClientID %>").value;
            ////                var Status = 2;
            ////                var Data = "2?" + UatReqID + "?" + document.getElementById("<%= txtComments.ClientID %>").value + "?" + Status;                
            ////                ToServer(Data, 2);
            ////            }

            ////            function btnSignOff_onclick() {                        
            ////                var UatReqID = document.getElementById("<%= hdnRequestID.ClientID %>").value;
            ////                var Status = 1;
            ////                var Data = "2?" + UatReqID + "?" + document.getElementById("<%= txtComments.ClientID %>").value + "?" + Status;
            ////                
            ////                ToServer(Data, 2);
            ////            }

        </script>
        <script language="javascript" type="text/javascript" for="window" event="onready">
            // <![CDATA[
            window_onload()
            // ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />

            <asp:HiddenField ID="hid_dtls1" runat="server" />
            <asp:HiddenField ID="hid_lvlNo" runat="server" />

            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">UAT Request No</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReqNo" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td id="Td1" style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td id="tdRe" style="width: 10%"></td>
                    <td style="width: 30%; text-align: left;">Environment</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtEnvmnt" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td id="colDraft" style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Application</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtApp" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Type</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtType" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Description</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReqRemark" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" ReadOnly="true" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <%--<tr>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                    <td style="width: 30%">
                        UAT Documents 
                    </td>
                    <td style="width: 50%">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">
                        &nbsp;
                    </td>
                </tr>         --%>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Priority
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtPriority" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Assign To</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAssign" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Status</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStatus" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Activity</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbActivity" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="61%" ForeColor="Black">
                            <asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                            <%--<asp:ListItem Value="1">SIGN OFF</asp:ListItem>
                            <asp:ListItem Value="2">REJECT</asp:ListItem>
                            <asp:ListItem Value="3">RETURN</asp:ListItem>--%>
                            <%--<asp:ListItem Value="1">TL REVIEW</asp:ListItem>
                            <asp:ListItem Value="2">SEND FOR REQUESTOR REVIEW</asp:ListItem>
                            <asp:ListItem Value="3">CLOSE REQUEST</asp:ListItem>
                            <asp:ListItem Value="4">HOLD REQUEST</asp:ListItem>
                            <asp:ListItem Value="5">CANCEL REQUEST</asp:ListItem>
                            <asp:ListItem Value="6">RETURN REQUEST</asp:ListItem>--%>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr id="rwUATHis">
                    <td colspan="4">
                        <asp:Panel ID="pnlUATDtl" runat="server">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="text-align: left;" colspan="2">&nbsp;</td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMailHis">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="text-align: left;" colspan="2">
                        <asp:Panel ID="pnlMail" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 50%; text-align: left;">&nbsp;</td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        <%-- <Summary>Created by 40020 on 08/Oct/2020</Summary> --%>
                        <%-- <Comments>Rename 'Is Mail to be Send' to 'Send Mail'</Comments> --%>
                        <%--<asp:CheckBox ID="chkMail" runat="server" Text="Is Mail to be Send" />--%>
                        <asp:CheckBox ID="chkMail" runat="server" Text="Send Mail" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMail">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        <%--Mail Body--%>
                        Mail Send To
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSendto" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMailSend">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        <%--Mail Send To--%>
                         Mail Body
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtMailBody" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMailSendCC">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Mail CC
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtCC" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Remarks</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtComments" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style1"></td>
                    <td style="text-align: left;" class="style2"></td>
                    <td style="text-align: left;" class="style3"></td>
                    <td class="style1"></td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="SAVE" onclick="return btnSave_onclick()" />
                        <%--
                        <input id="btnSignOff" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="SIGNOFF" onclick="return btnSignOff_onclick()" />
                        &nbsp;
                        <input id="btnReject" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="REJECT" onclick="return btnReject_onclick()" />--%>
                        &nbsp;
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnTracker" runat="server" />
            <asp:HiddenField ID="hdnRequestID" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
