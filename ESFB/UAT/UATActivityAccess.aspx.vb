﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class UATActivityAccess
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT, DT2, DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DR As DataRow
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If GF.FormAccess(CInt(Session("UserID")), 1380) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            'DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'),a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, " +
            '    " case when a.uat_status is null then 'NOT STARTED' when a.uat_status=1 then 'IN PROGRESS' when a.uat_status=2 then 'COMPLETED' when a.uat_status=3 then 'HOLD'  when a.uat_status=4 then 'VENDER PENDING' when a.uat_status=5 then 'PENDING REQUESTEE' when a.uat_status=6 then 'RETURN' end Status from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and  isnull(a.uat_status,0)<>2 ").Tables(0)

            'DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status is null then 'NOT STARTED' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=1) end Status, a.serviceno, a.contactno, a.email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and isnull(a.uat_status,0)<>2").Tables(0)
            DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'NOT STARTED' Status, a.serviceno, a.contactno, a.email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status is null and a.uat_action is null " +
                                   "union " +
                                   "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=1) Status, a.serviceno, a.contactno, a.email from uat_request_master a, uat_status_master s where s.uat_status_id=a.uat_status and s.uat_action_flg=1 and a.approved_status=1 and a.hoapprove_status=1 and a.uat_status in (1,3,4,5) and uat_action = 1 " +
                                   "union " +
                                   "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'RETURN' Status, a.serviceno, a.contactno, a.email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=6 and a.uat_action = 2").Tables(0)

            '<Summary>Created by 40020 on 08/Oct/2020</Summary
            '<Comments>Title changed from 'Activity Updation' to 'Work Log Updation'</Comments>
            'Me.Master.subtitle = "Activity Updation"
            Me.Master.subtitle = "Work Log Updation"

            Dim StrDtl As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrDtl += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString
                StrDtl += "£"
                DT2 = GF.GetQueryResult("select uat_doc_id,uatfile_name from DMS_ESFB.dbo.app_uatrequest_document where uat_request_id=" & CInt(DT.Rows(n)(0)) & "")
                If DT2.Rows.Count > 0 Then
                    For Each DR In DT2.Rows
                        StrDtl += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ř"
                    Next
                End If
                If n < DT.Rows.Count - 1 Then
                    StrDtl += "¥"
                End If
            Next

            hid_dtls.Value = StrDtl
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim DR, DR1 As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                DT = GF.GetQueryResult("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status is null then 'Not Started' when a.uat_status=1 then 'In Progress' end Status, (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and (uat_status is null or uat_status=1) and uat_request_id=" & CInt(Data(1)) & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString & "µ" & DT.Rows(0)(1).ToString & "µ" & DT.Rows(0)(2).ToString & "µ" & DT.Rows(0)(3).ToString & "µ" & DT.Rows(0)(4).ToString & "µ" & DT.Rows(0)(5).ToString & "µ" & DT.Rows(0)(6).ToString & "µ" & DT.Rows(0)(7).ToString & "µ" & DT.Rows(0)(8).ToString & "µ" & DT.Rows(0)(9).ToString & "µ" & DT.Rows(0)(10).ToString
                End If
        End Select
    End Sub
#End Region
End Class
