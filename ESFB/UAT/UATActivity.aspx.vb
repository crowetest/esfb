﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class UATActivity
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT2, DT3, DT4, DT5 As New DataTable
    Dim DR As DataRow
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '<Summary>Created by 40020 on 08/Oct/2020</Summary
        '<Comments>Title changed from 'Activity Updation' to 'Work Log Updation'</Comments>
        'Me.Master.subtitle = "Activity Updation"
        Me.Master.subtitle = "Work Log Updation"

        If GF.FormAccess(CInt(Session("UserID")), 1380) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If

        RequestID = CInt(GF.Decrypt(Request.QueryString.Get("RequestID")))

        Me.hdnRequestID.Value = CStr(RequestID)

        Dim StrDtl1 As String = ""

        StrDtl1 += "¥"

        DT = DB.ExecuteDataSet("select -1 id, 'Select' value, 0 uat_custom_order union all select uat_status_id id,uat_status_name value,uat_custom_order from uat_status_master where uat_status_Flg=1 and uat_action_Flg=1 order by uat_custom_order").Tables(0)

        If DT.Rows.Count > 0 Then
            For Each DR In DT.Rows
                StrDtl1 += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
            Next
        End If

        StrDtl1 += "¥"

        hid_dtls1.Value = StrDtl1

        'cmbActivity.DataSource = DT
        'cmbActivity.DataTextField = "value"
        'cmbActivity.DataValueField = "id"
        'cmbActivity.DataBind()

        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Me.chkMail.Attributes.Add("onclick", "return MailOnClick()")
        Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")

    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim DR, DR1 As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                'DT = GF.GetQueryResult("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status is null then 'NOT STARTED' when a.uat_status=1 then 'IN PROGRESS' when a.uat_status=2 then 'COMPLETED' when a.uat_status=3 then 'HOLD'  when a.uat_status=4 then 'VENDER PENDING' when a.uat_status=5 then 'PENDING REQUESTEE' when a.uat_status = 6 then 'RETURN' end Status, (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and  isnull(a.uat_status,0)<>2 and a.uat_request_id=" & CInt(Data(1)) & "")
                'DT = GF.GetQueryResult("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status is null then 'NOT STARTED' when a.uat_status = 6 then 'RETURN' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=1) end Status, (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and isnull(a.uat_status,0)<>2 and a.uat_request_id=" & CInt(Data(1)) & "")
                DT = GF.GetQueryResult("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+ convert(varchar,a.assign_empcode) + ')' assignto, case when (a.uat_status is null and a.uat_action is null) then 'NOT STARTED' when (a.uat_status = 6 and a.uat_action=2) then 'RETURN' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=1) end Status, (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_request_id=" & CInt(Data(1)) & "")

                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString & "µ" & DT.Rows(0)(1).ToString & "µ" & DT.Rows(0)(2).ToString & "µ" & DT.Rows(0)(3).ToString & "µ" & DT.Rows(0)(4).ToString & "µ" & DT.Rows(0)(5).ToString & "µ" & DT.Rows(0)(6).ToString & "µ" & DT.Rows(0)(7).ToString & "µ" & DT.Rows(0)(8).ToString & "µ" & DT.Rows(0)(9).ToString & "µ" & DT.Rows(0)(10).ToString
                    CallBackReturn += "£"
                    If (CInt(DT.Rows(0)(10)) > 0) Then
                        'DT3 = GF.GetQueryResult("select B.test_dtl_id, test_comments,TEST,test_on, test_name,mail_body, mail_to, mail_cc  from uat_request_master A inner join " +
                        '" (select uat_request_id, test_dtl_id,test_comments, case when test_status = 1 then 'IN PROGRESS' when test_status = 2 then 'COMPLETED' when test_status = 3 then 'HOLD' when test_status = 4 then 'VENDER PENDING' when test_status = 5 then 'PENDING REQUESTEE'  when test_status = 6 then 'RETURN' END AS TEST,   test_on, test_name from uat_testdtl_master) B ON A.uat_request_id = B.uat_request_id left join  " +
                        '" (SELECT test_mail_id as test_dtl_id, mail_to, mail_body, mail_cc from uat_testmail_master ) C ON B.test_dtl_id =C.test_dtl_id " +
                        '" Where A.uat_request_id =" & CInt(DT.Rows(0)(0)) & "")
                        DT3 = GF.GetQueryResult("select B.test_dtl_id, test_comments, TEST, test_on, test_name, mail_to, mail_body, mail_cc, uat_custom_order from uat_request_master A inner join " +
                        " (select uat_request_id, test_dtl_id,test_comments, s.uat_custom_order, s.uat_status_name AS TEST, test_on, test_name from uat_testdtl_master, uat_status_master s" +
                        " where uat_action_flg=1 and test_status=s.uat_status_id) B ON A.uat_request_id = B.uat_request_id left join  " +
                        " (SELECT test_mail_id as test_dtl_id, mail_to, mail_body, mail_cc from uat_testmail_master ) C ON B.test_dtl_id =C.test_dtl_id " +
                        " Where A.uat_request_id =" & CInt(DT.Rows(0)(0)) & "")
                        If Not DT3 Is Nothing Then
                            For Each DR In DT3.Rows
                                CallBackReturn += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ĉ" + DR(2).ToString() + "Ĉ" + CDate(DR(3).ToString()).ToString("dd-MMM-yyyy") + "Ĉ" + DR(4).ToString() + "Ĉ" + DR(5).ToString() + "Ĉ" + DR(6).ToString() + "Ĉ" + DR(7).ToString() + "Ĉ" + DR(8).ToString()
                                CallBackReturn += "¥"
                                If (CInt(DR(0)) > 0) Then
                                    DT5 = GF.GetQueryResult("select uat_test_id,uat_request_id,test_dtl_id,testfile_name from DMS_ESFB.dbo.app_uatTest_document where uat_request_id=" & CInt(DT.Rows(0)(0)) & " and test_dtl_id=" & CInt(DR(0)) & "")
                                    If Not DT5 Is Nothing Then
                                        For Each DR1 In DT5.Rows
                                            CallBackReturn += DR1(0).ToString() + "Ĉ" + DR1(1).ToString() + "Ĉ" + DR1(2).ToString() + "Ĉ" + DR1(3).ToString() + "Ø"
                                        Next
                                    End If
                                End If
                                CallBackReturn += "Ř"
                            Next
                        End If
                    End If
                End If

      
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim ErrorFlagDoc As Integer = 0
        Dim MessageDoc As String = Nothing
        Dim ErrorFlagMail As Integer = 0
        Dim MessageMail As String = Nothing

        Dim UATRequestID As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim AppTrackerID As Integer = 0

        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim hfc As HttpFileCollection
     
        hfc = Request.Files

        For i = 0 To hfc.Count - 1
            Dim myFile As HttpPostedFile = hfc(i)
            Dim nFileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                If FileLength > 4000000 Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('Please Check the Size of attached file');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
                Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (FileExtension = ".png" Or FileExtension = ".odt" Or FileExtension = ".txt" Or FileExtension = ".xls" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP" Or FileExtension = ".7z") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If
        Next
        Try
            Dim Comments As String = CStr(Me.txtComments.Text)
            Dim UatReqID As Integer = CInt(Me.hdnRequestID.Value)
            Dim UatStatus As Integer = CInt(Me.hdnAction.Value)

            Dim TestID As Integer = 0
            Dim Params(6) As SqlParameter
            Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@Comments", SqlDbType.VarChar, 1000)
            Params(1).Value = Comments
            Params(2) = New SqlParameter("@UatReqID", SqlDbType.Int)
            Params(2).Value = UatReqID
            Params(3) = New SqlParameter("@UpdateID", SqlDbType.Int)
            Params(3).Value = UatStatus
            Params(4) = New SqlParameter("@TestID", SqlDbType.Int)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(5).Direction = ParameterDirection.Output
            Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 500)
            Params(6).Direction = ParameterDirection.Output
            DB.ExecuteNonQuery("SP_UAT_UPDATE_TEST", Params)
            TestID = CInt(Params(4).Value)
            ErrorFlag = CInt(Params(5).Value)
            Message = CStr(Params(6).Value)
            If TestID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Try
                            Dim Param(6) As SqlParameter
                            Param(0) = New SqlParameter("@UATRequestID", SqlDbType.Int)
                            Param(0).Value = UatReqID
                            Param(1) = New SqlParameter("@TestDtlID", SqlDbType.Int)
                            Param(1).Value = TestID
                            Param(2) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                            Param(2).Value = AttachImg
                            Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                            Param(3).Value = ContentType
                            Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                            Param(4).Value = FileName
                            Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Param(5).Direction = ParameterDirection.Output
                            Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Param(6).Direction = ParameterDirection.Output
                            DB.ExecuteNonQuery("SP_UAT_TEST_ATTACH", Param)
                            ErrorFlagDoc = CInt(Param(5).Value)
                            MessageDoc = CStr(Param(6).Value)
                        Catch ex As Exception
                            MessageDoc = ex.Message.ToString
                            ErrorFlagDoc = 1
                            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                        End Try
                    End If
                Next
            End If

            If TestID > 0 And ErrorFlag = 0 And chkMail.Checked = True Then
                Dim MailBody As String = CStr(Me.txtMailBody.Text)
                Dim MailTo As String = CStr(Me.txtSendto.Text)
                Dim CC As String = CStr(Me.txtCC.Text)

                Try

                    Dim Para(7) As SqlParameter
                    Para(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Para(0).Value = UserID
                    Para(1) = New SqlParameter("@MailBody", SqlDbType.VarChar)
                    Para(1).Value = MailBody
                    Para(2) = New SqlParameter("@MailTo", SqlDbType.VarChar)
                    Para(2).Value = MailTo
                    Para(3) = New SqlParameter("@UatReqID", SqlDbType.Int)
                    Para(3).Value = UatReqID
                    Para(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Para(4).Direction = ParameterDirection.Output
                    Para(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Para(5).Direction = ParameterDirection.Output
                    Para(6) = New SqlParameter("@TestDtlID", SqlDbType.Int)
                    Para(6).Value = TestID
                    Para(7) = New SqlParameter("@Mail_CC", SqlDbType.VarChar)
                    Para(7).Value = CC

                    DB.ExecuteNonQuery("SP_UAT_SEND_MAIL", Para)
                    ErrorFlagMail = CInt(Para(4).Value)
                    MessageMail = CStr(Para(5).Value)
                Catch ex As Exception
                    MessageMail = ex.Message.ToString
                    ErrorFlagMail = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try

            End If

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        If ErrorFlag = 0 Then
            Message = "Saved Successfully"
            If ErrorFlagDoc = 1 Then
                Message += IIf(Message <> "", vbCrLf, MessageDoc).ToString()
            End If
            If ErrorFlagMail = 1 Then
                Message += IIf(Message <> "", vbCrLf, MessageMail).ToString()
            End If
        End If

        Dim RequestID As String = CStr(Me.hdnRequestID.Value)
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()

        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('UATActivityAccess.aspx?', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    End Sub

#End Region

    
End Class
