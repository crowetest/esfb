﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" Async="true"
    CodeFile="UATApprovalIT.aspx.vb" Inherits="UATApprovalIT" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../Style/Style.css" type="text/css" rel="Stylesheet" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function table_fill() {
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
                tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                tab += "<tr>";
                tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
                tab += "<td style='width:4%;text-align:left' >RequestNo</td>";
                tab += "<td style='width:5%;text-align:left' >Environment</td>";
                tab += "<td style='width:5%;text-align:left'>Application</td>";
                tab += "<td style='width:6%;text-align:left'>Type</td>";
                <%-- <Summary>Created by 40020 on 07/Oct/2020</Summary> --%>
                <%-- <Comments>Rename 'Remark' to 'Description'</Comments> --%>
                //tab += "<td style='width:8%;text-align:left'>Remark</td>";
                tab += "<td style='width:10%;text-align:left'>Description</td>";
                tab += "<td style='width:4%;text-align:left'>SNOW Request/Incident No</td>";
                tab += "<td style='width:4%;text-align:left'>Requested On</td>";
                tab += "<td style='width:5%;text-align:left'>Requested By</td>";
                tab += "<td style='width:4%;text-align:left'>Branch</td>";
                tab += "<td style='width:5%;text-align:left'>Department</td>";
                tab += "<td style='width:4%;text-align:left'>Contact No</td>";
                tab += "<td style='width:5%;text-align:left'>Email</td>";
                tab += "<td style='width:7%;text-align:left'>Attachments</td>";
                tab += "<td style='width:10%;text-align:left'>RO Remarks</td>";
                tab += "<td style='width:5%;text-align:center'>Action</td>";
                tab += "<td style='width:6%;text-align:center'>Remarks</td>";
                tab += "<td style='width:5%;text-align:center'>Priority</td>";
                tab += "<td style='width:5%;text-align:center'>AssignTo</td>";
                tab += "</tr>";
                if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                    var datas = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                    var row = "";
                    var col = "";
                    for (n = 0; n <= datas.length - 1; n++) {
                        row = datas[n].split("£");
                        col = row[0].split("µ");

                        if (row_bg == 0) {
                            row_bg = 1;
                            tab += "<tr class=sub_first>";
                        }
                        else {
                            row_bg = 0;
                            tab += "<tr class=sub_second>";
                        }
                        i = n + 1;
                        //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch
                        tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                        tab += "<td style='width:4%;text-align:left;word-break: break-all;' >" + col[1] + "</td>";
                        tab += "<td style='width:4%;text-align:left;word-break: break-all;' >" + col[2] + "</td>";
                        tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[3] + "</td>";
                        tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + col[4] + "</td>";
                        tab += "<td style='width:10%;text-align:left; word-break: break-all;'>" + col[5] + "</td>";
                        tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[10] + "</td>";
                        tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[6] + "</td>";
                        tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[7] + "</td>";
                        tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[8] + "</td>";
                        tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[9] + "</td>";
                        tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[11] + "</td>";
                        tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[12] + "</td>";

                        //-------------------------- Attachments
                        var docDtl = row[1].split("Ř");
                        var subtab = "";

                        if (docDtl[0].length != 0) {
                            subtab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                            subtab += "<tr class=sub_first>";
                            subtab += "<td style='width:3%;text-align:center;'>Sl No</td>";
                            subtab += "<td style='width:11%;text-align:left' >Document</td>";
                            subtab += "</tr>";


                            for (dx = 0; dx < docDtl.length - 1; dx++) {
                                var doc = docDtl[dx].split("Ĉ");
                                var sln = dx + 1;
                                subtab += "<tr class=sub_second>";
                                subtab += "<td style='width:3%;text-align:center;'>" + sln + "</td>";
                                subtab += "<td style='width:11%;text-align:left'><a href='ShowUATFormat.aspx?UatReqID=" + col[0] + "&DocID=" + doc[0] + "&TestID=0&StatusID=1'>" + doc[1] + "</a></td>";
                                subtab += "</tr>";
                            }
                            subtab += "</table>";
                        }
                        else
                            subtab += 'NA';

                        tab += "<td style='width:7%;text-align:center;word-break: break-all;'>" + subtab + "</td>";
                        //---------------------------------

                        tab += "<td style='width:10%;text-align:left;word-break: break-all;'>" + col[13] + "</td>";

                        var Data = document.getElementById("<%= hid_dtls1.ClientID %>").value.split("¥");

                        var select = "<select id='cmb" + col[0] + "' class='NormalText' style='width:99%; float:left;' maxlength='300' name='cmb" + col[0] + "'  onchange='checkAction(this.value)'>";
                        //select += "<option value='-1'>Select</option>";
                        <%-- <Summary>Created by 40020 on 07/Oct/2020</Summary> --%>
                        <%-- <Comments>Rename 'Received' to 'Accepted'</Comments> --%>
                        //select += "<option value='1'>Received</option>";
                        //select += "<option value='1'>Accepted</option>";
                        //select += "<option value='2'>Rejected</option>";
                        //select += "<option value='3'>Resolved</option>";                        
                        var rows = Data[0].split("Ř");
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            select += "<option value='" + cols[0] + "'>" + cols[1] + "</option>"
                        }
                        tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + select + "</td>";

                        var txtBox = "<textarea id='txtRemarks" + col[0] + "' name='txtRemarks" + col[0] + "' style='width:99%; float:left;' maxlength='300' ></textarea>";
                        tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + txtBox + "</td>";


                        var select1 = "<select id='cmbPriority" + col[0] + "' class='NormalText' style='width:99%; float:left;' maxlength='300' name='cmbPriority" + col[0] + "'>";
                        //select1 += "<option value='-1' selected=true>Select</option>"; //Priority
                        //select1 += "<option value='1'>Critical</option>";
                        //select1 += "<option value='2'>High</option>";
                        //select1 += "<option value='3'>Medium</option>";
                        //select1 += "<option value='4'>Low</option>";
                        var rows = Data[1].split("Ř");
                        for (a = 1; a < rows.length; a++) {
                            var cols = rows[a].split("Ĉ");
                            select1 += "<option value='" + cols[0] + "'>" + cols[1] + "</option>"
                        }
                        tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + select1 + "</td>";

                        var select2 = "<select id='cmbEmployee" + col[0] + "' class='NormalText' name='cmbEmployee" + col[0] + "' style='width:100%'>";
                        var rowEmp = row[2].split("Ř");    // Employee Details
                        for (j = 0; j < rowEmp.length - 1; j++) {
                            var colEmp = rowEmp[j].split("Ĉ");
                            if (colEmp[0] == -1)
                                select2 += "<option value='" + colEmp[0] + "' selected=true>" + colEmp[1] + "</option>";
                            else
                                select2 += "<option value='" + colEmp[0] + "' >" + colEmp[1] + "</option>";
                        }
                        tab += "<td style='width:5%;text-align:center;word-break: break-all;'>" + select2 + "</td>";


                        tab += "</tr>";
                    }
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

                //--------------------- Clearing Data ------------------------//

            }

            function UpdateValue() {
                document.getElementById("<%= hid_temp.ClientID %>").value = "";
                if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                    alert("No pending Approval");
                    return false;
                }

                var datas = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                for (n = 0; n <= datas.length - 1; n++) {
                    row = datas[n].split("£");
                    col = row[0].split("µ");

                    if (document.getElementById("cmb" + col[0]).value != -1) {
                        if (document.getElementById("cmb" + col[0]).value == 2) // rejected
                        {
                            if (document.getElementById("txtRemarks" + col[0]).value == "") {
                                alert("Enter Rejected Reason");
                                document.getElementById("txtRemarks" + col[0]).focus();
                                return false;
                            }
                        }
                        else if (document.getElementById("cmb" + col[0]).value == 3) // resolved
                        {
                            if (document.getElementById("txtRemarks" + col[0]).value == "") {
                                alert("Enter Resolved Reason");
                                document.getElementById("txtRemarks" + col[0]).focus();
                                return false;
                            }
                        }
                        else if (document.getElementById("cmb" + col[0]).value == 1) // Received
                        {
                            if (document.getElementById("cmbPriority" + col[0]).value == -1)  // Priority 
                            {
                                alert("Please select Priority");
                                document.getElementById("cmbPriority" + col[0]).focus();
                                return false;
                            }
                            else if (document.getElementById("cmbEmployee" + col[0]).value == -1) // Assign to 
                            {
                                alert("Please select Assign To");
                                document.getElementById("cmbEmployee" + col[0]).focus();
                                return false;
                            }
                        }
                        var Priority = "";
                        if (document.getElementById("cmbPriority" + col[0]).value == 1)
                            Priority = 'Critical';
                        else if (document.getElementById("cmbPriority" + col[0]).value == 2)
                            Priority = 'High';
                        else if (document.getElementById("cmbPriority" + col[0]).value == 3)
                            Priority = 'Medium';
                        else if (document.getElementById("cmbPriority" + col[0]).value == 4)
                            Priority = 'Low';

                        document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmb" + col[0]).value + "µ" + document.getElementById("txtRemarks" + col[0]).value + "µ" + Priority + "µ" + document.getElementById("cmbEmployee" + col[0]).value;
                    }
                }
                return true;
            }

            function FromServer(arg, context) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) window.open("UATApprovalIT.aspx", "_self");
            }


            function btnExit_onclick() {
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }

            function btnApprove_onclick() {
                var ret = UpdateValue();
                if (ret == 0) return false;
                if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                    alert("Select Any Request for Approval");
                    return false;
                }

                var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
                var Data = "1Ø" + strempcode;
                ToServer(Data, 1);
            }

            function checkAction(id) {
                //var x = document.getElementById("cmbPriority");
                //x.style.visibility = "hidden";
                //var y = document.getElementById("cmbEmployee")
                //y.stylevisibility = "hidden";
                
                //document.addEventListener("DOMContentLoaded", function (event) {
                //    document.getElementById("cmbPriority").option[0].disabled = true;
                //    document.getElementById("cmbEmployee").options[0].disabled = true;
                //});
                //document.addEventListener("DOMContentLoaded", function (event) {                
                    //document.getElementById("cmbPriority").disabled = id != "1";
                    //document.getElementById("cmbEmployee").disabled = id != "1";
                //});
                //if (id != "1") {
                //    document.addEventListener("DOMContentLoaded", function (event) {
                //        document.getElementById("cmbPriority").disabled = true;
                //        document.getElementById("cmbEmployee").disabled = true;
                //    });
                //}
                //else {                    
                //    document.addEventListener("DOMContentLoaded", function (event) {
                //        document.getElementById("cmbPriority").disabled = false;
                //        document.getElementById("cmbEmployee").disabled = false;
                //    });
                //}
            }

        </script>
    </head>
    </html>
    <asp:HiddenField ID="hid_dtls" runat="server" />
    <asp:HiddenField ID="hid_dtls1" runat="server" />
    <br />

    <table class="style1" style="width: 100%">

        <tr>
            <td colspan="3">
                <asp:Panel ID="pnLeaveApproveDtl" runat="server">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;"
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 6%;"
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>
    <br />
    <br />
</asp:Content>

