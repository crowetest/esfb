﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  
CodeFile="UATSignOffAccess.aspx.vb" Inherits="UATActivityAccess" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {      
             
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:7%;text-align:left' >Request No</td>";
            tab += "<td style='width:6%;text-align:left' >Environment</td>";
            tab += "<td style='width:5%;text-align:left'>Application</td>";
            tab += "<td style='width:6%;text-align:left'>Type</td>";
            //tab += "<td style='width:8%;text-align:left'>Remark</td>";
            tab += "<td style='width:8%;text-align:left'>Description</td>";
            tab += "<td style='width:4%;text-align:left'>SNOW/Incident No</td>";
            tab += "<td style='width:4%;text-align:left'>Requested On</td>";
            tab += "<td style='width:4%;text-align:left'>Requested By</td>";
            tab += "<td style='width:6%;text-align:left'>Branch</td>";
            tab += "<td style='width:6%;text-align:left'>Department</td>";
            tab += "<td style='width:4%;text-align:left'>Contact No</td>";
            tab += "<td style='width:4%;text-align:left'>Email</td>";
            tab += "<td style='width:14%;text-align:left'>Attachments</td>";    
            tab += "<td style='width:4%;text-align:left'>Priority</td>";
            tab += "<td style='width:8%;text-align:left'>Assign To</td>";
            tab += "<td style='width:4%;text-align:left'>Status</td>";
            tab += "<td style='width:4%;text-align:center'>Action</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {

                var datas = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var row = "";
                var col = "";
                for (n = 0; n <= datas.length - 1; n++) {
                    row = datas[n].split("£");
                    col = row[0].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:7%;text-align:left;word-break: break-all;' >" + col[1] + "</td>";
                    tab += "<td style='width:6%;text-align:left;word-break: break-all;' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[3] + "</td>";
                    tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + col[4] + "</td>";
                    tab += "<td style='width:8%;text-align:left;word-break: break-all;'>" + col[5] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[13] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[6] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[7] + "</td>";
                    tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + col[8] + "</td>";
                    tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + col[9] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[14] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[15] + "</td>";
                     
                    //-------------------------- Attachments
                    var subtab = "";
                    subtab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    subtab += "<tr class=sub_first>";
                    subtab += "<td style='width:3%;text-align:center;'>Sl No</td>";
                    subtab += "<td style='width:11%;text-align:left' >Document</td>";
                    subtab += "</tr>";
                   
                    var docDtl = row[1].split("Ř");    
                    for (dx = 0; dx < docDtl.length - 1; dx++) {
                        var doc = docDtl[dx].split("Ĉ"); 
                        var sln = dx + 1;
                        subtab += "<tr class=sub_second>";  
                        subtab += "<td style='width:3%;text-align:center;'>" + sln + "</td>";
                        subtab += "<td style='width:11%;text-align:left'><a href='ShowUATFormat.aspx?UatReqID=" + col[0] + "&DocID=" + doc[0] + "&TestID=0&StatusID=1'>" + doc[1] + "</a></td>";   
                        subtab += "</tr>";
                    }                    
                    subtab += "</table>";

                    tab += "<td style='width:14%;text-align:left;word-break: break-all;'>" + subtab + "</td>";
                    //---------------------------------

                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[10] + "</td>";
                    tab += "<td style='width:8%;text-align:left;word-break: break-all;'>" + col[11] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[12] + "</td>";
                    tab += "<td  style='width:4%;text-align:center; ;word-break: break-all;'><img id='ViewReport' src='../Image/update.png' onclick='GetActivity(" + col[0] + ")' title='Get Activity'  style='height:30px; width:30px;  cursor:pointer;' >";
                    
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//
        }

        function GetActivity(ReqID)
        {
            window.open("UATSignOffActivity.aspx?RequestID=" + btoa(ReqID)+"&Type="+ document.getElementById("<%= hid_lvlNo.ClientID %>").value, "_self");
        }
                  
             
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        

    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
                <asp:HiddenField ID="hid_lvlNo" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

