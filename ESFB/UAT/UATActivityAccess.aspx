﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"  Async="true"  
CodeFile="UATActivityAccess.aspx.vb" Inherits="UATActivityAccess" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
     <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function table_fill() {   
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
            tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            tab += "<td style='width:2%;text-align:center;'>Sl No</td>";
            tab += "<td style='width:7%;text-align:left' >Request No</td>";
            tab += "<td style='width:6%;text-align:left' >Environment</td>";
            tab += "<td style='width:5%;text-align:left'>Application</td>";
            tab += "<td style='width:6%;text-align:left'>Type</td>";
            <%-- <Summary>Created by 40020 on 08/Oct/2020</Summary> --%>
            <%-- <Comments>Rename 'Remark' to 'Description'</Comments> --%>
            //tab += "<td style='width:15%;text-align:left'>Remark</td>";
            tab += "<td style='width:11%;text-align:left'>Description</td>";
            tab += "<td style='width:4%;text-align:left'>SNOW Request/Incident No</td>";
            tab += "<td style='width:5%;text-align:left'>Requested On</td>";
            tab += "<td style='width:6%;text-align:left'>Requested By</td>";
            tab += "<td style='width:6%;text-align:left'>Branch</td>";
            tab += "<td style='width:6%;text-align:left'>Department</td>";
            tab += "<td style='width:4%;text-align:left'>Contact No</td>";
            tab += "<td style='width:4%;text-align:left'>Email</td>";
            tab += "<td style='width:6%;text-align:left'>Attachments</td>";    
            tab += "<td style='width:3%;text-align:left'>Priority</td>";
            tab += "<td style='width:8%;text-align:left'>AssignTo</td>";
            tab += "<td style='width:8%;text-align:left'>Status</td>";
            tab += "<td style='width:3%;text-align:center'>Action</td>";
            tab += "</tr>";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                var datas = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
                var row = "";
                var col = "";
                for (n = 0; n <= datas.length - 1; n++) {
                    row = datas[n].split("£");
                    col = row[0].split("µ");

                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }
                    else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    i = n + 1;
                    //c.app_request_id,d.app_dtl_id,j.app_name,h.branch_name,d.emp_code, f.Emp_Name,e.App_Type_name,d.Remarks,g.App_role_name,i.branch_name as To_branch
                    tab += "<td style='width:2%;text-align:center;'>" + i + "</td>";
                    tab += "<td style='width:7%;text-align:left;word-break: break-all;' >" + col[1] + "</td>";
                    tab += "<td style='width:6%;text-align:left;word-break: break-all;' >" + col[2] + "</td>";
                    tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[3] + "</td>";
                    tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + col[4] + "</td>";
                    tab += "<td style='width:11%;text-align:left;word-break: break-all;'>" + col[5] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[13] + "</td>";
                    tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[6] + "</td>";
                    tab += "<td style='width:8%;text-align:left;word-break: break-all;'>" + col[7] + "</td>";
                    tab += "<td style='width:5%;text-align:left;word-break: break-all;'>" + col[8] + "</td>";
                    tab += "<td style='width:3%;text-align:left;word-break: break-all;'>" + col[9] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[14] + "</td>";
                    tab += "<td style='width:4%;text-align:left;word-break: break-all;'>" + col[15] + "</td>";
                     
                    //-------------------------- Attachments
                    var subtab = "";
                    subtab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                    subtab += "<tr class=sub_first>";
                    subtab += "<td style='width:3%;text-align:center;'>Sl No</td>";
                    subtab += "<td style='width:11%;text-align:left' >Document</td>";
                    subtab += "</tr>";
                   
                    var docDtl = row[1].split("Ř");    
                    for (dx = 0; dx < docDtl.length - 1; dx++) {
                        var doc = docDtl[dx].split("Ĉ"); 
                        var sln = dx + 1;
                        subtab += "<tr class=sub_second>";  
                        subtab += "<td style='width:3%;text-align:center;'>" + sln + "</td>";
                        subtab += "<td style='width:11%;text-align:left'><a href='ShowUATFormat.aspx?UatReqID=" + col[0] + "&DocID=" + doc[0] + "&TestID=0&StatusID=1'>" + doc[1] + "</a></td>";   
                        subtab += "</tr>";
                    }                    
                    subtab += "</table>";

                    tab += "<td style='width:6%;text-align:left;word-break: break-all;'>" + subtab + "</td>";
                    //---------------------------------
                    tab += "<td style='width:3%;text-align:left;word-break: break-all;'>" + col[10] + "</td>";
                    tab += "<td style='width:8%;text-align:left;word-break: break-all;'>" + col[11] + "</td>";
                    tab += "<td style='width:8%;text-align:left;word-break: break-all;'>" + col[12] + "</td>";
                    tab += "<td  style='width:8%;text-align:center; '><img id='ViewReport' src='../Image/update.png' onclick='GetActivity("+col[0]+")' title='Get Activity'  style='height:30px; width:30px;  cursor:pointer;' >";
                    tab += "</tr>";
                }
            }
            tab += "</table></div></div></div>";
            document.getElementById("<%= pnLeaveApproveDtl.ClientID %>").innerHTML = tab;

            //--------------------- Clearing Data ------------------------//

        }

        function GetActivity(ReqID)
        {
            window.open("UATActivity.aspx?RequestID="+btoa(ReqID),"_self");
           
        }

        function UpdateValue() {            
            document.getElementById("<%= hid_temp.ClientID %>").value = "";
            if (document.getElementById("<%= hid_dtls.ClientID %>").value == "") {
                alert("No pending Approval");
                return false;
            }

            var datas = document.getElementById("<%= hid_dtls.ClientID %>").value.split("¥");
            for (n = 0; n <= datas.length - 1; n++) {
                row = datas[n].split("£");
                col = row[0].split("µ");
                               
                if (document.getElementById("cmb" + col[0]).value != -1) {
                    if(document.getElementById("cmb" + col[0]).value == 2)
                    {
                        if (document.getElementById("txtRemarks" + col[0]).value == "") {
                            alert("Enter Rejected Reason");
                            document.getElementById("txtRemarks" + col[0]).focus();
                            return false;
                        }
                    }   
                    if(document.getElementById("cmbPriority" + col[0]).value == -1)
                    {
                        alert("Please select Priority");
                        document.getElementById("cmbPriority" + col[0]).focus();
                        return false;
                    }
                    else
                    {
                        var Priority = "";
                        if(document.getElementById("cmbPriority" + col[0]).value == 1)
                            Priority='Critical';
                        else if(document.getElementById("cmbPriority" + col[0]).value == 2)
                            Priority='High';
                        else if(document.getElementById("cmbPriority" + col[0]).value == 3)
                            Priority='Medium';
                        else if(document.getElementById("cmbPriority" + col[0]).value == 4)
                            Priority='Low';
                    }
                    if(document.getElementById("cmbEmployee" + col[0]).value == -1)
                    {
                        alert("Please select Assign To");
                        document.getElementById("cmbEmployee" + col[0]).focus();
                        return false;
                    }                 
                    document.getElementById("<%= hid_temp.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmb" + col[0]).value + "µ" + document.getElementById("txtRemarks" + col[0]).value + "µ" + Priority + "µ" + document.getElementById("cmbEmployee" + col[0]).value;
                }
            }
            return true;
        }

        function FromServer(arg, context) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) window.open("UATApprovalIT.aspx", "_self");
        }
           
             
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        function btnApprove_onclick() {
            var ret = UpdateValue();
            if (ret == 0) return false;
            if (document.getElementById("<%= hid_temp.ClientID %>").value == "" && document.getElementById("<%= hid_dtls.ClientID %>").value != "") {
                alert("Select Any Request for Approval");
                return false;
            }

            var strempcode = document.getElementById("<%= hid_temp.ClientID %>").value;
            var Data = "1Ø" + strempcode;
            ToServer(Data, 1);
        }

    </script>
</head>
</html>
                <asp:HiddenField ID="hid_dtls" runat="server" />
<br />

    <table class="style1" style="width:100%">
        
        <tr> 
            <td colspan="3"><asp:Panel ID="pnLeaveApproveDtl" runat="server">
            </asp:Panel></td></tr>
        <tr>
            <td style="text-align:center;" colspan="3">
                <asp:HiddenField ID="hid_temp" runat="server" />
                <br />
                <input id="btnApprove" style="font-family: cambria; cursor: pointer; width: 6%;" 
                    type="button" value="SUBMIT" onclick="return btnApprove_onclick()" />&nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" 
                    type="button" value="EXIT" onclick="return btnExit_onclick()" /></td>
        </tr>
    </table>    
<br /><br />
</asp:Content>

