﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class UATApprovalIT
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT, DT2, DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DR As DataRow
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If GF.FormAccess(CInt(Session("UserID")), 1379) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            'DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'),a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name from uat_request_master a where a.approved_status=1 and a.hoapprove_status is null").Tables(0)
            DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'),a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.ServiceNo,a.ContactNo, a.email,a.approved_remark from uat_request_master a where a.approved_status=1 and a.hoapprove_status is null").Tables(0)

            Me.Master.subtitle = "IT Approval"

            Dim StrDtl As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrDtl += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString
                StrDtl += "£"
                DT2 = GF.GetQueryResult("select uat_doc_id,uatfile_name from DMS_ESFB.dbo.app_uatrequest_document where uat_request_id=" & CInt(DT.Rows(n)(0)) & "")
                If DT2.Rows.Count > 0 Then
                    For Each DR In DT2.Rows
                        StrDtl += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ř"
                    Next
                End If
                StrDtl += "£"
                DT3 = GF.GetQueryResult("select -1, 'Select' union all select emp_code, emp_name + ' ('+convert(varchar,emp_code) + ')' from uat_team_master")
                If DT3.Rows.Count > 0 Then
                    For Each DR In DT3.Rows
                        StrDtl += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ř"
                    Next
                End If
                If n < DT.Rows.Count - 1 Then
                    StrDtl += "¥"
                End If
            Next

            hid_dtls.Value = StrDtl

            '<Summary>Created by 40020 on 12/Oct/2020</Summary
            '<Comments>Data for Action Combo</Comments>

            Dim StrDtl1 As String = ""

            DT = DB.ExecuteDataSet("select -1, 'Select' union all select uat_status_id,uat_status_name from uat_status_master where uat_status_Flg=1 and uat_action_Flg=7").Tables(0)

            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    StrDtl1 += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
            End If

            StrDtl1 += "¥"

            DT = DB.ExecuteDataSet("select -1, 'Select' union all select uat_status_id,uat_status_name from uat_status_master where uat_status_Flg=1 and uat_action_Flg=8").Tables(0)

            If DT.Rows.Count > 0 Then
                For Each DR In DT.Rows
                    StrDtl1 += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                Next
            End If

            hid_dtls1.Value = StrDtl1

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim dataval As String = CStr(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))

        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Try
            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(0).Value = UserID
            Params(1) = New SqlParameter("@AppDtl", SqlDbType.VarChar)
            Params(1).Value = dataval.Substring(1)
            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(3).Direction = ParameterDirection.Output

            DB.ExecuteNonQuery("SP_UAT_IT_APPROVAL", Params)
            ErrorFlag = CInt(Params(2).Value)
            Message = CStr(Params(3).Value)
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        CallBackReturn = ErrorFlag.ToString + "Ø" + Message

    End Sub
#End Region

End Class
