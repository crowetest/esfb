﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    Async="true" EnableEventValidation="false" CodeFile="UATActivity.aspx.vb" Inherits="UATActivity" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }

        .style1 {
            width: 10%;
            height: 17px;
        }

        .style2 {
            width: 30%;
            height: 17px;
        }

        .style3 {
            width: 50%;
            height: 17px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function window_onload() {
                <%--document.getElementById("<%= chkMail.ClientID %>").checked = false--%>
                document.getElementById("rwMail").style.display = "none";
                document.getElementById("rwMailHis").style.display = "none";
                document.getElementById("rwUATHis").style.display = "none";
                document.getElementById("rwMailSend").style.display = "none";
                document.getElementById("rwMailSendCC").style.display = "none";
                document.getElementById("rwBtnSend").style.display = "none";
                ToServer("1ʘ" + document.getElementById("<%= hdnRequestID.ClientID %>").value, 1);
                document.getElementById("<%= hdnAction.ClientID %>").value = 0;
            }
            function RemoveImage(i) {
                if (i == 0) {
                    document.getElementById("<%= fup1.ClientID %>").value = "";
                    document.getElementById("<%= fup1.ClientID %>").focus();
                    document.getElementById("div_" + i).innerHTML = '';
                } else {
                    var parentDiv = document.getElementById("div" + i);
                    var removeDiv = document.getElementById("div_" + i);
                    var fileUploadarea = document.getElementById("fileUploadarea");
                    fileUploadarea.removeChild(parentDiv);
                }
            }
            function onchangeimage(i) {
                var removeBtn = "";
                var fileName = document.getElementById("<%= fup1.ClientID %>").value;
                var file = document.querySelector('input[type=file]').files[0];
                var size = Math.abs(file.size);
                if (((size / 1024) > 4096)) {
                    alert("File size should be less than 4 mb");
                    document.getElementById("<%= fup1.ClientID %>").value = "";
                    document.getElementById("<%= fup1.ClientID %>").focus();
                    return false;
                }
                else {
                    removeBtn += "<img src='../Image/close1.png' style='height:16px; width:16px; float:center; z-index:1; cursor: pointer;  padding-right:10px;' onclick='RemoveImage(" + i + ")' title='Remove' />";
                    document.getElementById("div_" + i).innerHTML = removeBtn;
                    return true;
                }
            }
            function AddMoreImages() {
                if (!document.getElementById && !document.createElement)
                    return false;
                var fileUploadarea = document.getElementById("fileUploadarea");
                if (!fileUploadarea)
                    return false;
                var newLine = document.createElement("br");
                fileUploadarea.appendChild(newLine);
                var newFile = document.createElement("input");
                newFile.type = "file";
                newFile.setAttribute("class", "fileUpload");

                if (!AddMoreImages.lastAssignedId)
                    AddMoreImages.lastAssignedId = 100;
                newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
                newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
                newFile.setAttribute("onchange", "onchangeimage(" + AddMoreImages.lastAssignedId + ")");
                var div = document.createElement("div");
                div.appendChild(newFile);
                div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
                fileUploadarea.appendChild(div);
                var removeDiv = document.createElement("span");
                removeDiv.setAttribute("id", "div_" + AddMoreImages.lastAssignedId);
                var parentDiv = document.getElementById("div" + AddMoreImages.lastAssignedId);
                parentDiv.appendChild(removeDiv);
                AddMoreImages.lastAssignedId++;
            }


            function FromServer(Arg, Context) {
                switch (Context) {

                    case 1:
                        {
                            if (Arg != "") {
                                var Data = Arg.split("£");
                                var Dtl = Data[0].split("µ");
                                document.getElementById("<%= txtReqNo.ClientID %>").value = Dtl[1];
                                document.getElementById("<%= txtEnvmnt.ClientID %>").value = Dtl[2];
                                document.getElementById("<%= txtApp.ClientID %>").value = Dtl[3];
                                document.getElementById("<%= txtType.ClientID %>").value = Dtl[4];
                                document.getElementById("<%= txtReqRemark.ClientID %>").value = Dtl[5];
                                document.getElementById("<%= txtPriority.ClientID %>").value = Dtl[6];
                                document.getElementById("<%= txtAssign.ClientID %>").value = Dtl[7];
                                document.getElementById("<%= txtStatus.ClientID %>").value = Dtl[8];

                                document.getElementById("<%= cmbActivity.ClientID %>").options.length = 0;
                                var Data1 = document.getElementById("<%= hid_dtls1.ClientID %>").value.split("¥");
                                var rows = Data1[1].split("Ř");

                                for (a = 1; a < rows.length; a++) {
                                    var cols = rows[a].split("Ĉ");
                                    var option1 = document.createElement("OPTION");
                                    option1.value = cols[0];
                                    option1.text = cols[1];
                                    document.getElementById("<%= cmbActivity.ClientID %>").add(option1);
                                }

                                var i = 0;
                                if (Data[1] != "") {
                                    var tab = "";
                                    var row_bg = 0;
                                    tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=mainhead><div style='width:100%; height:auto; overflow:auto;'>";
                                    tab += "<div style='width:100%; overflow:auto;text-align:center;margin: 0px auto;' align='center'><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                    //tab += "<tr class=sub_second><td style='width:100%;text-align:center;' colspan=8>ACTIVITY HISTORY</td></tr>";
                                    tab += "<tr class=sub_second><td style='width:100%;text-align:center;' colspan=10>WORK LOG HISTORY</td></tr>";
                                    tab += "<tr>";
                                    tab += "<td style='width:4%;text-align:center;'>#</td>";
                                    tab += "<td style='width:10%;text-align:center' >Activity</td>";

                                    tab += "<td style='width:20%;text-align:center'>Comments</td>";
                                    tab += "<td style='width:20%;text-align:center' >Attachments</td>";
                                    tab += "<td style='width:8%;text-align:center' >Done Date</td>";
                                    tab += "<td style='width:8%;text-align:center' >Done By</td>";
                                    tab += "<td style='width:8%;text-align:center' >Mail Send To</td>";
                                    tab += "<td style='width:8%;text-align:center' >Mail CC</td>";
                                    tab += "<td style='width:14%;text-align:center' >Mail Body</td>";

                                    var row = Data[1].split("Ř");
                                    for (n = 0; n < row.length - 1; n++) {
                                        var Dtl = row[n].split("¥");
                                        col = Dtl[0].split("Ĉ");

                                        if (row_bg == 0) {
                                            row_bg = 1;
                                            tab += "<tr class=sub_first>";
                                        }
                                        else {
                                            row_bg = 0;
                                            tab += "<tr class=sub_second>";
                                        }
                                        i = n + 1;

                                        tab += "<td style='width:4%;text-align:center;'>" + i + "</td>";
                                        tab += "<td style='width:10%;text-align:left;word-break: break-all;' >" + col[2] + "</td>";
                                        tab += "<td style='width:20%;text-align:left;word-break: break-all;'>" + col[1] + "</td>";
                                        if (Dtl[1] != "") {
                                            //-------------------------- Attachments
                                            var subtab = "";
                                            subtab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
                                            subtab += "<tr class=sub_first>";
                                            subtab += "<td style='width:5%;text-align:center;'>Sl No</td>";
                                            subtab += "<td style='width:15%;text-align:left' >Document</td>";
                                            subtab += "</tr>";

                                            var docDtl = Dtl[1].split("Ø");
                                            for (dx = 0; dx < docDtl.length - 1; dx++) {
                                                var doc = docDtl[dx].split("Ĉ");
                                                var sln = dx + 1;
                                                subtab += "<tr class=sub_second>";
                                                subtab += "<td style='width:5%;text-align:center;'>" + sln + "</td>";
                                                subtab += "<td style='width:15%;text-align:left'><a href='ShowUATFormat.aspx?UatReqID=" + doc[1] + "&DocID=" + doc[0] + "&TestID=" + doc[2] + "&StatusID=2'>" + doc[3] + "</a></td>";
                                                subtab += "</tr>";
                                            }
                                            subtab += "</table>";
                                            tab += "<td style='width:20%;text-align:left;word-break: break-all;' >" + subtab + "</td>";
                                            //-----                                              
                                        }
                                        else {
                                            tab += "<td style='width:20%;text-align:left;word-break: break-all;' ></td>";
                                        }
                                        tab += "<td style='width:8%;text-align:left;word-break: break-all;' >" + col[3] + "</td>";
                                        tab += "<td style='width:8%;text-align:left;word-break: break-all;' >" + col[4] + "</td>";
                                        tab += "<td style='width:8%;text-align:left;word-break: break-all;' >" + col[5] + "</td>";
                                        tab += "<td style='width:8%;text-align:left;word-break: break-all;' >" + col[7] + "</td>";
                                        tab += "<td style='width:14%;text-align:left;word-break: break-all;' >" + col[6] + "</td>";
                                        tab += "</tr>";
                                    }
                                    tab += "</table></div></div></div>";

                                    /// New code
                                    var row2 = Data[1].split("Ř");
                                    var index = row2.length - 2;
                                    var Dtl2 = row2[index].split("¥");
                                    var col2 = Dtl2[0].split("Ĉ");
                                    document.getElementById("<%= cmbActivity.ClientID %>").selectedIndex = col2[8]

                                    document.getElementById("<%= pnlUATDtl.ClientID %>").innerHTML = tab;
                                    document.getElementById("rwUATHis").style.display = "";
                                }
                            }
                            break;
                        }

                }
            }

            function MailOnClick() {
                if (document.getElementById("<%= chkMail.ClientID %>").checked == true) {
                    document.getElementById("rwMail").style.display = "";
                    document.getElementById("rwMailSend").style.display = "";
                    document.getElementById("rwMailSendCC").style.display = "";
                }
                else {
                    document.getElementById("rwMail").style.display = "none";
                    document.getElementById("rwMailSend").style.display = "none";
                    document.getElementById("rwMailSendCC").style.display = "none";
                }
            }

            function btnExit_onclick() {
                window.open("UATActivityAccess.aspx", "_self");
            }


            function setStartDate(sender, args) {
                sender._startDate = new Date();
            }

            function btnSave_onclick() {
                if (document.getElementById("<%= cmbActivity.ClientID %>").value == -1) {
                    alert("Select Activity");
                    document.getElementById("<%= cmbActivity.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%= chkMail.ClientID %>").checked == true) {

                    if (document.getElementById("<%= txtSendto.ClientID %>").value == "") {
                    alert("Please enter send to");
                    document.getElementById("<%= txtSendto.ClientID %>").focus();
                    return false;
                }

                if (document.getElementById("<%= txtMailBody.ClientID %>").value == "") {
                    alert("Please enter Mail Body");
                    document.getElementById("<%= txtMailBody.ClientID %>").focus();
                    return false;
                }

                //                    if(document.getElementById("<%= txtCC.ClientID %>").value == "")
                //                    {
                //                        alert("Please enter CC");
                //                        document.getElementById("<%= txtCC.ClientID %>").focus();
                //                        return false;
                //                    }  
            }
                ////                ToServer("2ʘ" + document.getElementById("<%= txtMailBody.ClientID %>").value + "ʘ" + document.getElementById("<%= txtSendto.ClientID %>").value + "ʘ" + document.getElementById("<%= hdnRequestID.ClientID %>").value, 2);
                document.getElementById("<%= hdnAction.ClientID %>").value = document.getElementById("<%= cmbActivity.ClientID %>").value;
            }



        </script>
        <script language="javascript" type="text/javascript" for="window" event="onready">
            // <![CDATA[
            window_onload()
            // ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <br />

            <asp:HiddenField ID="hid_dtls1" runat="server" />
            <asp:HiddenField ID="hdnAction" runat="server" />

            <table align="center" style="width: 90%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">UAT Request No</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReqNo" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td id="Td1" style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td id="tdRe" style="width: 10%"></td>
                    <td style="width: 30%; text-align: left;">Environment</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtEnvmnt" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td id="colDraft" style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Application</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtApp" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Type</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtType" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Description</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtReqRemark" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" ReadOnly="true" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <%--<tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%">UAT Documents 
                    </td>
                    <td style="width: 50%">
                        <asp:Panel ID="pnlDoc" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>--%>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Priority
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtPriority" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Assign To</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtAssign" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Status</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtStatus" runat="server" class="ReadOnlyTextBox" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Activity</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbActivity" class="NormalText" runat="server" Font-Names="Cambria"
                            Width="61%" ForeColor="Black">
                            <%--<asp:ListItem Value="-1">-----SELECT-----</asp:ListItem>
                            <asp:ListItem Value="1">IN PROGRESS</asp:ListItem>
                            <asp:ListItem Value="3">HOLD</asp:ListItem>
                            <asp:ListItem Value="4">VENDER PENDING</asp:ListItem>--%>
                            <%-- <Summary>Created by 40020 on 08/Oct/2020</Summary> --%>
                            <%-- <Comments>Rename 'PENDING REQUESTEE' to 'PENDING WITH REQUESTOR'</Comments> --%>
                            <%--<asp:ListItem Value="5">PENDING REQUESTEE</asp:ListItem>--%>
                            <%--<asp:ListItem Value="5">PENDING WITH REQUESTOR</asp:ListItem>
                            <asp:ListItem Value="2">COMPLETED</asp:ListItem>--%>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr id="rwUATHis">
                    <td colspan="4">
                        <asp:Panel ID="pnlUATDtl" runat="server">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="text-align: left;" colspan="2">&nbsp;</td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMailHis">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="text-align: left;" colspan="2">
                        <asp:Panel ID="pnlMail" runat="server">
                        </asp:Panel>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        <%-- <Summary>Created by 40020 on 08/Oct/2020</Summary> --%>
                        <%-- <Comments>Rename 'Is Mail to be Send' to 'Send Mail'</Comments> --%>
                        <%--<asp:CheckBox ID="chkMail" runat="server" Text="Is Mail to be Send" />--%>
                        <asp:CheckBox ID="chkMail" runat="server" Text="Send Mail" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMail">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        <%--Mail Body--%>
                        Mail Send To
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtSendto" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMailSend">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">
                        <%--Mail Send To--%>
                         Mail Body
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtMailBody" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwMailSendCC">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Mail CC
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtCC" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr id="rwBtnSend">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 50%; text-align: left;">
                        <input id="btnSend"
                            style="font-family: cambria; cursor: pointer; width: 93px;" type="button"
                            value="SEND MAIL" onclick="return btnSend_onclick()" /></td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 50%; text-align: left;">&nbsp;</td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Upload Documents<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileUploadarea">
                            <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" onchange="onchangeimage(0)" />
                            <span id="div_0" style="width: 40%"></span>
                            <br />
                        </div>
                        <div>
                            <input id="btnDraftDoc" onclick="AddMoreImages();" style="display: block;"
                                type="button" value="Add More" />
                        </div>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Comments</td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtComments" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style1"></td>
                    <td style="text-align: left;" class="style2"></td>
                    <td style="text-align: left;" class="style3"></td>
                    <td class="style1"></td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <%--<asp:Button ID="btnSave" style="font-family: cambria; cursor: pointer;" runat="server" Text="SAVE" Width="95px" />--%>
                        <asp:Button ID="btnSave" Style="font-family: cambria; cursor: pointer;" runat="server" Text="UPDATE" Width="95px" />
                        &nbsp;
                       
                        <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                            value="EXIT" onclick="return btnExit_onclick()" />&nbsp;
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnTracker" runat="server" />
            <asp:HiddenField ID="hdnRequestID" runat="server" />
            <asp:HiddenField ID="hdnStatus" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
