﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class ShowUATFormat
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim UatReqID As Integer = Convert.ToInt32(Request.QueryString.[Get]("UatReqID"))
            Dim StatusID As Integer = Convert.ToInt32(Request.QueryString.[Get]("StatusID"))
            Dim DocID As Integer = Convert.ToInt32(Request.QueryString.[Get]("DocID"))
            Dim TestID As Integer = Convert.ToInt32(Request.QueryString.[Get]("TestID"))

            If StatusID = 1 Then
                Dim Params(1) As SqlParameter
                Params(0) = New SqlParameter("@UatReqID", SqlDbType.Int)
                Params(0).Value = UatReqID
                Params(1) = New SqlParameter("@DocID", SqlDbType.Int)
                Params(1).Value = DocID
                '<Summary>Created by 40020 on 07/Oct/2020</Summary
                '<Comments>Changed the database to DMS_ESFB</Comments>
                'DT = DB.ExecuteDataSet("select a.uat_attach,a.content_type,uatfile_name from dbo.app_uatrequest_document a where a.uat_request_id=" & UatReqID & " and a.uat_doc_id= " & DocID & "").Tables(0)
                DT = DB.ExecuteDataSet("select a.uat_attach,a.content_type,uatfile_name from [DMS_ESFB].[dbo].app_uatrequest_document a where a.uat_request_id= @UatReqID and a.uat_doc_id= @DocID", Params, 1).Tables(0)
            ElseIf StatusID = 2 Then
                '<Summary>Created by 40020 on 07/Oct/2020</Summary
                '<Comments>Changed the database to DMS_ESFB</Comments>
                'DT = DB.ExecuteDataSet("select a.test_attach,a.content_type,a.testfile_name from dbo.app_uatTest_document a where a.uat_request_id=" & UatReqID & " and a.uat_test_id=" & DocID & " and a.test_dtl_id= " & TestID & "").Tables(0)
                DT = DB.ExecuteDataSet("select a.test_attach,a.content_type,a.testfile_name from [DMS_ESFB].[dbo].app_uatTest_document a where a.uat_request_id=" & UatReqID & " and a.uat_test_id=" & DocID & " and a.test_dtl_id= " & TestID & "").Tables(0)
            End If

            If DT IsNot Nothing Then
                Dim bytes() As Byte = CType(DT.Rows(0)(0), Byte())
                Response.Buffer = True
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = DT.Rows(0)(1).ToString()
                Response.AddHeader("content-disposition", "attachment;filename=" + DT.Rows(0)(2).ToString().Replace(" ", ""))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class
