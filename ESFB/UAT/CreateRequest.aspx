﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    EnableEventValidation="false" CodeFile="CreateRequest.aspx.vb" Inherits="CreateRequest" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .fileUpload {
            width: 255px;
            font-size: 11px;
            color: #000000;
            border: solid;
            border-width: 1px;
            border-color: #7f9db9;
            height: 22px;
        }
    </style>
    <head>
        <title></title>
        <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function window_onload() {   
                document.getElementById("<%= hdnApplication.ClientID %>").value=0;  
                document.getElementById("<%= hdnEvnmt.ClientID %>").value=0;  
                document.getElementById("<%= hdnType.ClientID %>").value=0;           
                ToServer("1ʘ", 1);
            }
            
            function RemoveImage(i){
                if(i==0){
                    document.getElementById("<%= fup1.ClientID %>").value = "";
                    document.getElementById("<%= fup1.ClientID %>").focus();
                    document.getElementById("div_"+i).innerHTML = '';
                }else{
                    var parentDiv = document.getElementById("div"+i);
                    var removeDiv = document.getElementById("div_"+i);
                    var fileUploadarea = document.getElementById("fileUploadarea");
                    fileUploadarea.removeChild(parentDiv);
                }
            }
            function onchangeimage(i){
                var removeBtn ="";
                var fileName = document.getElementById("<%= fup1.ClientID %>").value;
                var file    = document.querySelector('input[type=file]').files[0];
                var size = Math.abs(file.size);
                if (((size/1024)>4096))
                {
                    alert("File size should be less than 4 mb");
                    document.getElementById("<%= fup1.ClientID %>").value = "";
                    document.getElementById("<%= fup1.ClientID %>").focus();
                    return false;
                }
                else
                {
                    removeBtn += "<img src='../Image/close1.png' style='height:16px; width:16px; float:center; z-index:1; cursor: pointer;  padding-right:10px;' onclick='RemoveImage("+i+")' title='Remove' />";
                    document.getElementById("div_"+i).innerHTML = removeBtn;
                    return true;  
                }
            }
            function AddMoreImages() {
                if (!document.getElementById && !document.createElement)
                    return false;
                var fileUploadarea = document.getElementById("fileUploadarea");
                if (!fileUploadarea)
                    return false;
                var newLine = document.createElement("br");
                fileUploadarea.appendChild(newLine);
                var newFile = document.createElement("input");
                newFile.type = "file";
                newFile.setAttribute("class", "fileUpload");
            
                if (!AddMoreImages.lastAssignedId)
                    AddMoreImages.lastAssignedId = 100;
                newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
                newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
                newFile.setAttribute("onchange", "onchangeimage("+AddMoreImages.lastAssignedId+")");
                var div = document.createElement("div");
                div.appendChild(newFile);
                div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
                fileUploadarea.appendChild(div);
                var removeDiv = document.createElement("span");
                removeDiv.setAttribute("id","div_"+AddMoreImages.lastAssignedId);
                var parentDiv = document.getElementById("div"+AddMoreImages.lastAssignedId);
                parentDiv.appendChild(removeDiv);
                AddMoreImages.lastAssignedId++;        
            }

            function SaveOnClick() {  
                //////                if (document.getElementById("<%= fup1.ClientID %>").value == "") {
                //////                    alert("Please Upload Documents");
                //////                    document.getElementById("<%= fup1.ClientID %>").focus();
                //////                    return false;
                //////                }
                if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                {
                    alert("Please enter Description of Test");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= cmbApplication.ClientID %>").value == -1)
                {
                    alert("Please Select Application");
                    document.getElementById("<%= cmbApplication.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= cmbEnvironment.ClientID %>").value == -1)
                {
                    alert("Please Select Environment");
                    document.getElementById("<%= cmbEnvironment.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= cmbType.ClientID %>").value == -1)
                {
                    alert("Please Select Type");
                    document.getElementById("<%= cmbType.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtRemark.ClientID %>").value == "")
                {
                    alert("Please enter Description of Test");
                    document.getElementById("<%= txtRemark.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtContactno.ClientID %>").value == "")
                {
                    alert("Please enter Point of Contact No");
                    document.getElementById("<%= txtContactno.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtEmail.ClientID %>").value == "")
                {
                    alert("Please enter Email");
                    document.getElementById("<%= txtEmail.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtContactno.ClientID %>").value!="")
                { 
                    var ret= checkPhone(document.getElementById("<%= txtContactno.ClientID %>")); 
                    if(ret==false)
                    {
                        return false;
                    }
                }
                if(document.getElementById("<%= txtEmail.ClientID %>").value!="")
                { 
                    var ret= checkEmail(document.getElementById("<%= txtEmail.ClientID %>")); 
                    if(ret==false)
                    {
                        return false;
                    }
                }
                document.getElementById("<%= hdnApplication.ClientID %>").value=document.getElementById("<%= cmbApplication.ClientID %>").value;  
                document.getElementById("<%= hdnEvnmt.ClientID %>").value=document.getElementById("<%= cmbEnvironment.ClientID %>").value;  
                document.getElementById("<%= hdnType.ClientID %>").value=document.getElementById("<%= cmbType.ClientID %>").value;                
            }
            function FromServer(Arg, Context) {
                switch (Context) {
                    case 1:
                        {
                            if(Arg != "")
                            {
                                var Data = Arg.split("¥");
                                ComboFill(Data[0], "<%= cmbEnvironment.ClientID %>");
                                ComboFill(Data[1], "<%= cmbApplication.ClientID %>");
                                ComboFill(Data[2], "<%= cmbType.ClientID %>");
                            }
                            break;
                        }
                }
            }

            function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
            function btnExit_onclick() {               
                window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
            }
        
        </script>
        <script language="javascript" type="text/javascript" for="window" event="onload">
            // <![CDATA[
            return window_onload()
            // ]]>
        </script>
    </head>
    <div style="width: 80%; background-color: #A34747; margin: 0px auto;">
        <br />
        <div style="width: 96%; background-color: white; margin: 0px auto; border-radius: 25px;">
            <br />
            <table align="center" style="width: 80%; margin: 0px auto;">
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Select Environment<span style="color: red"> *</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbEnvironment" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td id="colDraft" style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr id="rowApplication">
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Application/Module<span style="color: red"> *</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbApplication" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr id="rowType">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Type<span style="color: red"> *</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:DropDownList ID="cmbType" class="NormalText" Style="text-align: left;"
                            runat="server" Font-Names="Cambria" Width="100%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 50%; text-align: left;">&nbsp;</td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>

                <tr id="rowDoc">
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%; text-align: left;">Upload Documents<br />
                        <span style="color: #FF0000">(Maximum size is 4 MB)</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <div id="fileUploadarea">
                            <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" onchange="onchangeimage(0)" />
                            <span id="div_0" style="width: 40%"></span>
                            <br />
                        </div>
                        <div>
                            <input id="btnDraftDoc" onclick="AddMoreImages();" style="display: block;"
                                type="button" value="Add More" />
                        </div>
                    </td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>


                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">&nbsp;</td>
                    <td style="width: 100%; text-align: left;"></td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Description of the test to be conducted<span style="color: red"> *</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtRemark" runat="server" class="NormalText" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="100%" TextMode="MultiLine" Height="53px"
                            MaxLength="200" />
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Point of Contact No<span style="color: red"> *</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtContactno" runat="server" class="NormalText" MaxLength="50" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="41%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">Email<span style="color: red"> *</span>
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtEmail" runat="server" class="NormalText" MaxLength="50" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="72%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        <%-- <Summary>Created by 40020 on 07/Oct/2020</Summary> --%>
                        <%-- <Comments>Rename 'Service Request No/Incident' to 'SNOW Request/Incident Number'</Comments> --%>
                        <%--Service Request No/Incident--%>
                        SNOW Request/Incident Number
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtServiceNo" runat="server" class="NormalText" MaxLength="50" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="72%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <%-- <Summary>Created by 40020 on 07/Oct/2020</Summary> --%>
                <%-- <comments>Added field for 'On Behalf Of'</Comments> --%>
                <tr>
                    <td style="width: 10%">&nbsp;
                    </td>
                    <td style="width: 30%; text-align: left;">
                        On Behalf Of
                    </td>
                    <td style="width: 50%; text-align: left;">
                        <asp:TextBox ID="txtOnBehalfOf" runat="server" class="NormalText" MaxLength="50" Style="font-family: Cambria; font-size: 10pt; resize: none;"
                            Width="72%"></asp:TextBox>
                    </td>
                    <td style="width: 10%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="4" class="style2">
                        <asp:Button ID="btnSave" runat="server" Text="SUBMIT" Width="67px" />
                        &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;"
                            type="button" value="EXIT" onclick="return btnExit_onclick()" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnApplication" runat="server" />
            <asp:HiddenField ID="hdnEvnmt" runat="server" />
            <asp:HiddenField ID="hdnType" runat="server" />
        </div>
        <br />
    </div>
</asp:Content>
