﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class CreateRequest
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Dim RequestID As Integer
#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Create Request"

        'If GF.FormAccess(CInt(Session("UserID")), 1376) = False And CInt(Session("BranchID")) <> 0 Then
        '    Response.Redirect("~/AccessDenied.aspx", False)
        '    Return
        'End If
        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        Dim Params(0) As SqlParameter
        Params(0) = New SqlParameter("@EmpCode", SqlDbType.Int)
        Params(0).Value = CInt(Session("UserID"))
        DT = DB.ExecuteDataSet("select case when cug_no <>'' then cug_no else mobile end as ContactNo, official_mail_id from emp_profile where emp_code=@EmpCode", Params, 1).Tables(0)

        If DT.Rows.Count > 0 Then
            txtContactno.Text = DT.Rows(0).Item(0).ToString()
            txtEmail.Text = DT.Rows(0).Item(1).ToString()
            'txtServiceNo.Text = ""
        End If

        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        End If
        Me.btnSave.Attributes.Add("onclick", "return SaveOnClick()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim DR As DataRow
        Select Case CInt(Data(0))
            Case 1 'Fill Application
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select envmt_id,envmt_name from uat_environment_master where envmt_Flg=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "¥"
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select uat_app_id,uat_app_name from uat_application_master where uat_app_Flg=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
                CallBackReturn += "¥"
                DT = GF.GetQueryResult("select -1, '-----Select -----' union all select uat_type_id,uat_type_name from uat_type_master where uat_type_Flg=1")
                If DT.Rows.Count > 0 Then
                    For Each DR In DT.Rows
                        CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
                    Next
                End If
        End Select
    End Sub
#End Region

#Region "Confirm"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim UATRequestID As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        Dim AppTrackerID As Integer = 0

        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim hfc As HttpFileCollection
        hfc = Request.Files

        For i = 0 To hfc.Count - 1
            Dim myFile As HttpPostedFile = hfc(i)
            Dim nFileLen As Integer = myFile.ContentLength
            Dim FileName As String = ""
            If (nFileLen > 0) Then
                ContentType = myFile.ContentType
                FileName = myFile.FileName
                Dim FileLength As Integer = CInt(myFile.ContentLength) 'Convert into bytes
                If FileLength > 4000000 Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('Please Check the Size of attached file');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
                Dim FileExtension As String = Path.GetExtension(myFile.FileName)
                If Not (FileExtension = ".txt" Or FileExtension = ".png" Or FileExtension = ".xls" Or FileExtension = ".odt" Or FileExtension = ".xlsx" Or FileExtension = ".jpg" Or FileExtension = ".jpeg" Or FileExtension = ".doc" Or FileExtension = ".docx" Or FileExtension = ".zip" Or FileExtension = ".pdf" Or FileExtension = ".PDF" Or FileExtension = ".XLS" Or FileExtension = ".XLSX" Or FileExtension = ".JPG" Or FileExtension = ".JPEG" Or FileExtension = ".DOC" Or FileExtension = ".DOCX" Or FileExtension = ".ZIP" Or FileExtension = ".7z") Then
                    Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Exit Sub
                End If
            End If
        Next
        Try
            Dim Remark As String = CStr(Me.txtRemark.Text)
            Dim Envmnt As Integer = CInt(Me.hdnEvnmt.Value)
            Dim Application As Integer = CInt(Me.hdnApplication.Value)
            Dim Type As Integer = CInt(Me.hdnType.Value)
            Dim ContactNo As String = CStr(Me.txtContactno.Text)
            Dim Email As String = CStr(Me.txtEmail.Text)
            Dim ServiceNo As String = CStr(Me.txtServiceNo.Text)
            Dim OnBehalfOf As String = CStr(Me.txtOnBehalfOf.Text)

            '<Summary>Created by 40020 on 07/Oct/2020</Summary
            '<Comments>size of Params changed</Comments>
            'Dim Params(11) As SqlParameter
            Dim Params(12) As SqlParameter
            Params(0) = New SqlParameter("@BranchID", SqlDbType.Int)
            Params(0).Value = BranchID
            Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(1).Value = UserID
            Params(2) = New SqlParameter("@Remark", SqlDbType.VarChar, 500)
            Params(2).Value = Remark
            Params(3) = New SqlParameter("@Envmnt", SqlDbType.Int)
            Params(3).Value = Envmnt
            Params(4) = New SqlParameter("@Application", SqlDbType.Int)
            Params(4).Value = Application
            Params(5) = New SqlParameter("@Type", SqlDbType.Int)
            Params(5).Value = Type
            Params(6) = New SqlParameter("@UATReqID", SqlDbType.Int)
            Params(6).Direction = ParameterDirection.Output
            Params(7) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(7).Direction = ParameterDirection.Output
            Params(8) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(8).Direction = ParameterDirection.Output
            Params(9) = New SqlParameter("@ContactNo", SqlDbType.VarChar, 500)
            Params(9).Value = ContactNo
            Params(10) = New SqlParameter("@Email", SqlDbType.VarChar, 500)
            Params(10).Value = Email
            Params(11) = New SqlParameter("@ServiceNo", SqlDbType.VarChar, 500)
            Params(11).Value = ServiceNo
            '<Summary>Created by 40020 on 07/Oct/2020</Summary
            '<Comments>Added field for 'On Behalf Of'</Comments>
            Params(12) = New SqlParameter("@Onbehalfof", SqlDbType.VarChar, 500)
            Params(12).Value = OnBehalfOf

            DB.ExecuteNonQuery("SP_UAT_INIT_REQUEST", Params)
            ErrorFlag = CInt(Params(7).Value)
            Message = CStr(Params(8).Value)
            UATRequestID = CInt(Params(6).Value)
            If UATRequestID > 0 And ErrorFlag = 0 And hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)

                        Dim Param(5) As SqlParameter
                        Param(0) = New SqlParameter("@UATRequestID", SqlDbType.Int)
                        Param(0).Value = UATRequestID
                        Param(1) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                        Param(1).Value = AttachImg
                        Param(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                        Param(2).Value = ContentType
                        Param(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Param(3).Value = FileName
                        Param(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Param(4).Direction = ParameterDirection.Output
                        Param(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Param(5).Direction = ParameterDirection.Output
                        DB.ExecuteNonQuery("SP_UAT_REQUEST_ATTACH", Param)
                    End If
                Next
            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
        End Try
        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        cl_script1.Append("         alert('" + Message + "'); ")
        cl_script1.Append("        window.open('CreateRequest.aspx', '_self');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If ErrorFlag = 0 Then
            initializeControls()
        End If

    End Sub
    Private Sub initializeControls()
        txtRemark.Text = ""
    End Sub

#End Region

End Class
