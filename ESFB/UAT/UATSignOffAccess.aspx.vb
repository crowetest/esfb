﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class UATActivityAccess
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT, DT2, DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DR As DataRow
    Dim TypeID As Integer
    Dim CallBackReturn As String = Nothing
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim levelNo As Integer = CInt(Request.QueryString.Get("Type"))
            hid_lvlNo.Value = CStr(levelNo)

            If GF.FormAccess(CInt(Session("UserID")), 1381) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            'DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'),a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status=2 then 'Test Completed' end Status from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_verify_status is null").Tables(0)

            'DT = DB.ExecuteDataSet("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'),a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, " +
            '    " case when a.uat_status is null then 'NOT STARTED' when a.uat_status=1 then 'IN PROGRESS' when a.uat_status=2 then 'COMPLETED' when a.uat_status=3 then 'HOLD'  when a.uat_status=4 then 'VENDER PENDING' when a.uat_status=5 then 'PENDING REQUESTEE' end Status from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_verify_status is null ").Tables(0)

            Dim qry As String = ""

            If levelNo = 1 Then 'TL Review
                qry = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'NOT STARTED' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_action=1 " +
                      "union " +
                      "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, m.uat_status_name Status, serviceno,ContactNo,email from uat_request_master a,uat_status_master m where m.uat_action_flg=2 and m.uat_status_id=a.uat_status and a.approved_status=1 and a.hoapprove_status=1 and a.uat_status in (1,4) and a.uat_action=2 " +
                      "union " +
                      "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'RETURN' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=5 and a.uat_action=3"
            ElseIf levelNo = 2 Then 'Requestor Review
                qry = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'NOT STARTED' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_action=2" +
                      "union " +
                      "Select a.uat_request_id, a.uat_request_no, a.envmt_name, a.app_name, a.type_name, a.req_remarks, Replace(Convert(Char(11), a.created_on, 113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, m.uat_status_name Status, serviceno,ContactNo,email from uat_request_master a,uat_status_master m where m.uat_action_flg=3 and m.uat_status_id=a.uat_status and a.approved_status=1 and a.hoapprove_status=1 and a.uat_status = 1 and a.uat_action=3" +
                      "union " +
                      "Select a.uat_request_id, a.uat_request_no, a.envmt_name, a.app_name, a.type_name, a.req_remarks, Replace(Convert(Char(11), a.created_on, 113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'RETURN' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=4 and a.uat_action=4"
            ElseIf levelNo = 3 Then 'RO Review
                qry = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'NOT STARTED' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_action=3 " +
                      "union " +
                      "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, m.uat_status_name Status, serviceno,ContactNo,email from uat_request_master a,uat_status_master m where m.uat_action_flg=4 and m.uat_status_id=a.uat_status and a.approved_status=1 and a.hoapprove_status=1 and a.uat_status = 1 and a.uat_action=4 " +
                      "union " +
                      "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'RETURN' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=3 and a.uat_action=5"
            ElseIf levelNo = 4 Then 'IT Head Approval
                qry = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'NOT STARTED' Status, serviceno,ContactNo,email from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_action=4 "
                '+ "union " +
                '"select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, m.uat_action_name Status from uat_request_master a,uat_status_master m where m.uat_action_flg=2 and m.uat_status_id=a.uat_status and a.approved_status=1 and a.hoapprove_status=1 and a.uat_status in (1,4) and a.uat_action=5 " +
                '"union " +
                '"select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'), a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, 'RETURN' Status from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_action=3"

            End If

            DT = DB.ExecuteDataSet(qry).Tables(0)

            '<Summary>Created by 40020 on 08/Oct/2020</Summary
            '<Comments>Title changed from 'UAT SignOff' to 'Team Lead Review'</Comments>
            'Me.Master.subtitle = "UAT SignOff"
            'Me.Master.subtitle = "Team Lead Review"

            Select Case CInt(levelNo)
                Case 1
                    Me.Master.subtitle = "Team Lead Review"
                Case 2
                    Me.Master.subtitle = "Requestor Review"
                Case 3
                    Me.Master.subtitle = "RO Review"
                Case 4
                    Me.Master.subtitle = "IT Head Approval"
            End Select

            Dim StrDtl As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrDtl += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString
                StrDtl += "£"
                DT2 = GF.GetQueryResult("select uat_doc_id,uatfile_name from DMS_ESFB.dbo.app_uatrequest_document where uat_request_id=" & CInt(DT.Rows(n)(0)) & "")
                If DT2.Rows.Count > 0 Then
                    For Each DR In DT2.Rows
                        StrDtl += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ř"
                    Next
                End If
                If n < DT.Rows.Count - 1 Then
                    StrDtl += "¥"
                End If
            Next

            hid_dtls.Value = StrDtl
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("ʘ"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0

        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                DT = GF.GetQueryResult("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status is null then 'NOT STARTED' when a.uat_status=1 then 'IN PROGRESS' when a.uat_status=2 then 'COMPLETED' when a.uat_status=3 then 'HOLD'  when a.uat_status=4 then 'VENDER PENDING' when a.uat_status=5 then 'PENDING REQUESTEE' end Status , (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1  and a.uat_status=2 and a.uat_verify_status is null and uat_request_id=" & CInt(Data(1)) & "")
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString & "µ" & DT.Rows(0)(1).ToString & "µ" & DT.Rows(0)(2).ToString & "µ" & DT.Rows(0)(3).ToString & "µ" & DT.Rows(0)(4).ToString & "µ" & DT.Rows(0)(5).ToString & "µ" & DT.Rows(0)(6).ToString & "µ" & DT.Rows(0)(7).ToString & "µ" & DT.Rows(0)(8).ToString & "µ" & DT.Rows(0)(9).ToString & "µ" & DT.Rows(0)(10).ToString
                End If
        End Select
    End Sub
#End Region
End Class
