﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class UATSignOffActivity
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT, DT2, DT3, DT4, DT5 As New DataTable
    Dim DR As DataRow
    Dim LevelNo As Integer
    Dim RequestID As Integer

#Region "Page Load & Dispose"
    Protected Sub BRD_Request_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
        DT.Dispose()
    End Sub

    Protected Sub BRD_Request_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '<Summary>Created by 40020 on 08/Oct/2020</Summary
        '<Comments>Title changed from 'UAT SignOff' to 'Team Lead Review'</Comments>
        'Me.Master.subtitle = "UAT SignOff"
        'Me.Master.subtitle = "Team Lead Review"

        If GF.FormAccess(CInt(Session("UserID")), 1381) = False And CInt(Session("BranchID")) <> 0 Then
            Response.Redirect("~/AccessDenied.aspx", False)
            Return
        End If

        RequestID = CInt(GF.Decrypt(Request.QueryString.Get("RequestID")))
        Me.hdnRequestID.Value = CStr(RequestID)
        LevelNo = CInt(Request.QueryString.Get("Type").ToString)

        hid_lvlNo.Value = CStr(LevelNo)
        Dim actionFlag As Integer = LevelNo + 1

        Select Case CInt(LevelNo)
            Case 1
                Me.Master.subtitle = "Team Lead Review"
            Case 2
                Me.Master.subtitle = "Requestor Review"
            Case 3
                Me.Master.subtitle = "RO Review"
            Case 4
                Me.Master.subtitle = "IT Head Approval"
        End Select

        Dim StrDtl1 As String = ""

        StrDtl1 += "¥"

        DT = DB.ExecuteDataSet("select -1 id, 'Select' value union all select uat_status_id id,uat_status_name value from uat_status_master where uat_status_Flg=1 and uat_action_Flg=" & actionFlag & "").Tables(0)

        If DT.Rows.Count > 0 Then
            For Each DR In DT.Rows
                StrDtl1 += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()
            Next
        End If

        StrDtl1 += "¥"

        hid_dtls1.Value = StrDtl1

        '--//---------- Script Registrations -----------//--
        '/--- For Call Back ---//
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Me.chkMail.Attributes.Add("onclick", "return MailOnClick()")
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("?"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim Message As String = Nothing
        Dim ErrorFlag As Integer = 0
        Dim DR, DR1 As DataRow
        Dim ErrorFlagMail As Integer = 0
        Dim MessageMail As String = Nothing
        Dim qry1 As String = ""
        Dim qry2 As String = ""

        Select Case CInt(Data(0))
            Case 1 'Fill Tracker
                'DT = GF.GetQueryResult("select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status is null then 'NOT STARTED' when a.uat_status=1 then 'IN PROGRESS' when a.uat_status=2 then 'COMPLETED' when a.uat_status=3 then 'HOLD'  when a.uat_status=4 then 'VENDER PENDING' when a.uat_status=5 then 'PENDING REQUESTEE' when a.uat_status=6 then 'RETURN' end Status, (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and  isnull(a.uat_status,0)=2 and a.uat_request_id=" & CInt(Data(1)) & "")
                If hid_lvlNo.Value = "1" Then
                    qry1 = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when (a.uat_status = 2 and a.uat_action=1) then 'NOT STARTED' when (a.uat_status=5 and a.uat_action =3) then 'RETURN' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=2) end Status, (select count(*) from uat_reviewmail_master m where m.uat_request_id=a.uat_request_id and review_flag=1) MailCnt, (select count(*) from uat_reviewdtl_master t where t.uat_request_id=a.uat_request_id and review_flg=1) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_request_id= " & CInt(Data(1)) & ""
                    qry2 = "select B.review_dtl_id, review_comments,TEST,review_on, review_name,mail_body, mail_to, mail_cc,uat_status_id from uat_request_master A inner join (select uat_request_id, review_dtl_id,review_comments, s.uat_status_id, s.uat_status_name AS TEST, review_on, review_name from uat_reviewdtl_master, uat_status_master s where s.uat_action_flg = 2 and s.uat_status_id=review_status and review_flg=1) B ON A.uat_request_id = B.uat_request_id left join (SELECT review_mail_id as review_dtl_id, mail_body, mail_to, mail_cc from uat_reviewmail_master where review_flag = 1) C ON B.review_dtl_id =C.review_dtl_id Where A.uat_request_id = " & CInt(RequestID) & ""
                ElseIf hid_lvlNo.Value = "2" Then
                    qry1 = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when (a.uat_status = 2 and a.uat_action=2) then 'NOT STARTED' when (a.uat_status=4 and a.uat_action =4) then 'RETURN' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=3) end Status, (select count(*) from uat_reviewmail_master m where m.uat_request_id=a.uat_request_id and review_flag=2) MailCnt, (select count(*) from uat_reviewdtl_master t where t.uat_request_id=a.uat_request_id and review_flg=2) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_request_id= " & CInt(Data(1)) & ""
                    qry2 = "select B.review_dtl_id, review_comments,TEST,review_on, review_name,mail_body, mail_to, mail_cc,uat_status_id from uat_request_master A inner join (select uat_request_id, review_dtl_id,review_comments, s.uat_status_id, s.uat_status_name AS TEST, review_on, review_name from uat_reviewdtl_master, uat_status_master s where s.uat_action_flg = 3 and s.uat_status_id=review_status and review_flg=2) B ON A.uat_request_id = B.uat_request_id left join (SELECT review_mail_id as review_dtl_id, mail_body, mail_to, mail_cc from uat_reviewmail_master where review_flag = 2) C ON B.review_dtl_id =C.review_dtl_id Where A.uat_request_id = " & CInt(RequestID) & ""
                ElseIf hid_lvlNo.Value = "3" Then
                    qry1 = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when (a.uat_status = 2 and a.uat_action=3) then 'NOT STARTED' when (a.uat_status=3 and a.uat_action =5) then 'RETURN' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=4) end Status, (select count(*) from uat_reviewmail_master m where m.uat_request_id=a.uat_request_id and review_flag=3) MailCnt, (select count(*) from uat_reviewdtl_master t where t.uat_request_id=a.uat_request_id and review_flg=3) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_request_id= " & CInt(Data(1)) & ""
                    qry2 = "select B.review_dtl_id, review_comments,TEST,review_on, review_name,mail_body, mail_to, mail_cc,uat_status_id from uat_request_master A inner join (select uat_request_id, review_dtl_id,review_comments, s.uat_status_id, s.uat_status_name AS TEST, review_on, review_name from uat_reviewdtl_master, uat_status_master s where s.uat_action_flg = 4 and s.uat_status_id=review_status and review_flg=3) B ON A.uat_request_id = B.uat_request_id left join (SELECT review_mail_id as review_dtl_id, mail_body, mail_to, mail_cc from uat_reviewmail_master where review_flag = 3) C ON B.review_dtl_id =C.review_dtl_id Where A.uat_request_id = " & CInt(RequestID) & ""
                ElseIf hid_lvlNo.Value = "4" Then
                    qry1 = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when (a.uat_status = 2 and a.uat_action=4) then 'NOT STARTED' else (select uat_status_name from uat_status_master where uat_status_id=a.uat_status and uat_action_flg=5) end Status, (select count(*) from uat_reviewmail_master m where m.uat_request_id=a.uat_request_id and review_flag=4) MailCnt, (select count(*) from uat_reviewdtl_master t where t.uat_request_id=a.uat_request_id and review_flg=4) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_request_id= " & CInt(Data(1)) & ""
                    qry2 = "select B.review_dtl_id, review_comments,TEST,review_on, review_name,mail_body, mail_to, mail_cc,uat_status_id from uat_request_master A inner join (select uat_request_id, review_dtl_id,review_comments, s.uat_status_id, s.uat_status_name AS TEST, review_on, review_name from uat_reviewdtl_master, uat_status_master s where s.uat_action_flg = 5 and s.uat_status_id=review_status and review_flg=4) B ON A.uat_request_id = B.uat_request_id left join (SELECT review_mail_id as review_dtl_id, mail_body, mail_to, mail_cc from uat_reviewmail_master where review_flag = 4) C ON B.review_dtl_id =C.review_dtl_id Where A.uat_request_id = " & CInt(RequestID) & ""
                End If

                DT = GF.GetQueryResult(qry1)

                If DT.Rows.Count > 0 Then
                        CallBackReturn += DT.Rows(0)(0).ToString & "µ" & DT.Rows(0)(1).ToString & "µ" & DT.Rows(0)(2).ToString & "µ" & DT.Rows(0)(3).ToString & "µ" & DT.Rows(0)(4).ToString & "µ" & DT.Rows(0)(5).ToString & "µ" & DT.Rows(0)(6).ToString & "µ" & DT.Rows(0)(7).ToString & "µ" & DT.Rows(0)(8).ToString & "µ" & DT.Rows(0)(9).ToString & "µ" & DT.Rows(0)(10).ToString
                        CallBackReturn += "£"
                    If (CInt(DT.Rows(0)(10)) > 0) Then

                        'DT3 = GF.GetQueryResult("select B.test_dtl_id, test_comments,TEST,test_on, test_name,mail_body, mail_to  from uat_request_master A inner join " +
                        '" (select uat_request_id, test_dtl_id,test_comments, case when test_status = 1 then 'IN PROGRESS' when test_status = 2 then 'COMPLETED' when test_status = 3 then 'HOLD' when test_status = 4 then 'VENDER PENDING' when test_status = 5 then 'PENDING REQUESTEE' when test_status=6 then 'RETURN' END AS TEST,   test_on, test_name from uat_testdtl_master) B ON A.uat_request_id = B.uat_request_id left join  " +
                        '" (SELECT test_mail_id as test_dtl_id, mail_body, mail_to from uat_testmail_master ) C ON B.test_dtl_id =C.test_dtl_id " +
                        '" Where A.uat_request_id =" & CInt(DT.Rows(0)(0)) & "")

                        'DT3 = GF.GetQueryResult("select B.review_dtl_id, review_comments,TEST,review_on, review_name,mail_body, mail_to, mail_cc from uat_request_master A inner join (select uat_request_id, review_dtl_id,review_comments, s.uat_status_name AS TEST, review_on, review_name from uat_reviewdtl_master, uat_status_master s where s.uat_action_flg = 2 and s.uat_status_id=review_status and review_flg=1) B ON A.uat_request_id = B.uat_request_id left join (SELECT review_mail_id as review_dtl_id, mail_body, mail_to, mail_cc from uat_reviewmail_master where review_flag = 1) C ON B.review_dtl_id =C.review_dtl_id Where A.uat_request_id = " & CInt(DT.Rows(0)(0)) & "")
                        DT3 = GF.GetQueryResult(qry2)

                        If Not DT3 Is Nothing Then
                            For Each DR In DT3.Rows
                                CallBackReturn += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ĉ" + DR(2).ToString() + "Ĉ" + CDate(DR(3).ToString()).ToString("dd-MMM-yyyy") + "Ĉ" + DR(4).ToString() + "Ĉ" + DR(5).ToString() + "Ĉ" + DR(6).ToString() + "Ĉ" + DR(7).ToString() + "Ĉ" + DR(8).ToString()
                                CallBackReturn += "¥"
                                'If (CInt(DR(0)) > 0) Then
                                '    DT5 = GF.GetQueryResult("Select uat_review_id, uat_request_id, review_dtl_id, testfile_name from DMS_ESFB.dbo.app_uatReview_document where uat_request_id=" & CInt(DT.Rows(0)(0)) & " And review_dtl_id=" & CInt(DR(0)) & "")
                                '    If Not DT5 Is Nothing Then
                                '        For Each DR1 In DT5.Rows
                                '            CallBackReturn += DR1(0).ToString() + "Ĉ" + DR1(1).ToString() + "Ĉ" + DR1(2).ToString() + "Ĉ" + DR1(3).ToString() + "Ø"
                                '        Next
                                '    End If
                                'End If
                                CallBackReturn += "Ř"
                            Next
                        End If
                    End If
                End If
                'DT = GF.GetQueryResult("Select a.uat_request_id, a.uat_request_no, a.envmt_name, a.app_name, a.type_name, a.req_remarks, a.uat_priority, a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, case when a.uat_status=2 then 'Test Completed' end Status, (select count(*) from uat_testmail_master m where m.uat_request_id=a.uat_request_id) MailCnt, (select count(*) from uat_testdtl_master t where t.uat_request_id=a.uat_request_id) TestDtl from uat_request_master a where a.approved_status=1 and a.hoapprove_status=1 and a.uat_status=2 and a.uat_request_id=" & CInt(Data(1)) & "")
                'If DT.Rows.Count > 0 Then
                '    CallBackReturn += DT.Rows(0)(0).ToString & "µ" & DT.Rows(0)(1).ToString & "µ" & DT.Rows(0)(2).ToString & "µ" & DT.Rows(0)(3).ToString & "µ" & DT.Rows(0)(4).ToString & "µ" & DT.Rows(0)(5).ToString & "µ" & DT.Rows(0)(6).ToString & "µ" & DT.Rows(0)(7).ToString & "µ" & DT.Rows(0)(8).ToString & "µ" & DT.Rows(0)(9).ToString & "µ" & DT.Rows(0)(10).ToString
                'End If
                'CallBackReturn += "£"
                'DT2 = GF.GetQueryResult("select uat_doc_id,uatfile_name from DMS_ESFB.dbo.app_uatrequest_document where uat_request_id=" & CInt(DT.Rows(0)(0)) & "")
                'If DT2.Rows.Count > 0 Then
                '    For Each DR In DT2.Rows
                '        CallBackReturn += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ř"
                '    Next
                'End If
                'If (CInt(DT.Rows(0)(9)) > 0) Then
                '    CallBackReturn += "£"
                '    DT3 = GF.GetQueryResult("select test_mail_id, mail_body,mail_to,replace(convert(char(11),mailsend_on,113),' ','-'),mailsend_name + ' - ' + convert(varchar,mailsend_by) from uat_testmail_master where uat_request_id=" & CInt(DT.Rows(0)(0)) & "")
                '    For Each DR In DT3.Rows
                '        CallBackReturn += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ĉ" + DR(2).ToString() + "Ĉ" + DR(3).ToString() + "Ĉ" + DR(4).ToString() + "Ř"
                '    Next
                'End If
                'If (CInt(DT.Rows(0)(10)) > 0) Then
                '    CallBackReturn += "£"
                '    DT4 = GF.GetQueryResult("select a.test_dtl_id,a.test_comments,replace(convert(char(11),a.test_on,113),' ','-'),a.test_name + ' -' + convert(varchar,a.test_by), (select count(*) from DMS_ESFB.dbo.app_uatTest_document d where d.uat_request_id=a.uat_request_id and d.test_dtl_id=a.test_dtl_id) from uat_testdtl_master a where a.uat_request_id=" & CInt(DT.Rows(0)(0)) & "")
                '    For Each DR In DT4.Rows
                '        CallBackReturn += DR(0).ToString() + "Ĉ" + DR(1).ToString() + "Ĉ" + DR(2).ToString() + "Ĉ" + DR(3).ToString() + "Ĉ" + DR(4).ToString()
                '        CallBackReturn += "¥"
                '        If (CInt(DR(4)) > 0) Then
                '            DT5 = GF.GetQueryResult("select uat_test_id,uat_request_id,test_dtl_id,testfile_name from DMS_ESFB.dbo.app_uatTest_document where uat_request_id=" & CInt(DT.Rows(0)(0)) & " and test_dtl_id=" & CInt(DR(0)) & "")
                '            For Each DR1 In DT5.Rows
                '                CallBackReturn += DR1(0).ToString() + "Ĉ" + DR1(1).ToString() + "Ĉ" + DR1(2).ToString() + "Ĉ" + DR1(3).ToString() + "Ø"
                '            Next
                '        End If
                '        CallBackReturn += "Ř"
                '    Next
                'End If
                        Case 2
                Dim UatReqID As Integer = CInt(Data(1))
                Dim Remarks As String = CStr(Data(2))
                Dim Status As Integer = CInt(Data(3))
                Dim ReviewFlag As Integer = 1
                Try
                    Dim TestID As Integer = 0
                    Dim Params(7) As SqlParameter
                    Params(0) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(0).Value = UserID
                    Params(1) = New SqlParameter("@UatReqID", SqlDbType.Int)
                    Params(1).Value = UatReqID
                    Params(2) = New SqlParameter("@Remarks", SqlDbType.VarChar)
                    Params(2).Value = Remarks
                    Params(3) = New SqlParameter("@Status", SqlDbType.Int)
                    Params(3).Value = Status
                    Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(5).Direction = ParameterDirection.Output
                    Params(6) = New SqlParameter("@ReviewFlag", SqlDbType.Int)
                    Params(6).Value = LevelNo
                    Params(7) = New SqlParameter("@TestID", SqlDbType.Int)
                    Params(7).Direction = ParameterDirection.Output
                    DB.ExecuteNonQuery("SP_UAT_SIGNOFF_TEST", Params)
                    TestID = CInt(Params(7).Value)
                    ErrorFlag = CInt(Params(4).Value)
                    Message = CStr(Params(5).Value)

                    If TestID > 0 And ErrorFlag = 0 And CStr(Data(4)) = "true" Then
                        Dim MailBody As String = CStr(Data(7))
                        Dim MailTo As String = CStr(Data(5))
                        Dim CC As String = CStr(Data(6))

                        Try

                            Dim Para(8) As SqlParameter
                            Para(0) = New SqlParameter("@UserID", SqlDbType.Int)
                            Para(0).Value = UserID
                            Para(1) = New SqlParameter("@MailBody", SqlDbType.VarChar)
                            Para(1).Value = MailBody
                            Para(2) = New SqlParameter("@MailTo", SqlDbType.VarChar)
                            Para(2).Value = MailTo
                            Para(3) = New SqlParameter("@UatReqID", SqlDbType.Int)
                            Para(3).Value = UatReqID
                            Para(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                            Para(4).Direction = ParameterDirection.Output
                            Para(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                            Para(5).Direction = ParameterDirection.Output
                            Para(6) = New SqlParameter("@TestDtlID", SqlDbType.Int)
                            Para(6).Value = TestID
                            Para(7) = New SqlParameter("@Mail_CC", SqlDbType.VarChar)
                            Para(7).Value = CC
                            Para(8) = New SqlParameter("@Type", SqlDbType.Int)
                            Para(8).Value = LevelNo

                            DB.ExecuteNonQuery("SP_UAT_SEND_MAIL", Para)
                            ErrorFlagMail = CInt(Para(4).Value)
                            MessageMail = CStr(Para(5).Value)
                        Catch ex As Exception
                            MessageMail = ex.Message.ToString
                            ErrorFlagMail = 1
                            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlagMail.ToString())
                        End Try

                    End If

                Catch ex As Exception
                    Message = ex.Message.ToString
                    ErrorFlag = 1
                    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
                End Try
                CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        End Select
    End Sub
#End Region

End Class
