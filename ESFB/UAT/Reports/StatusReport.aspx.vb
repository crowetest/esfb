﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class StatusReport
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler

    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim RptStatus As Integer
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
          
            Me.Master.subtitle = "Request Status Report"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As Integer = CInt(Session("UserID"))

            DT = DB.ExecuteDataSet("select * from uat_team_master where emp_code = " + UserID.ToString() + "").Tables(0)
            If DT Is Nothing Then
                txtEmpCode.Text = CInt(Session("UserID")).ToString
                txtEmpCode.ReadOnly = True
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function


    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))

        If CInt(Data(0)) = 1 Then
            'If CInt(Data(1)) > 0 Then
            DT = DB.ExecuteDataSet("select '-1' as ID,'--------- Select ---------' as name union all select app_type_id,app_type_name from esfb.dbo.App_Type_master where app_id=" & Data(1).ToString & "").Tables(0)
            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            'End If
        End If
    End Sub

End Class
