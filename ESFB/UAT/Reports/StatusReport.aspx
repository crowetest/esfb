﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="StatusReport.aspx.vb" Inherits="StatusReport" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <link href="../../Style/Style.css" type="text/css" rel="Stylesheet" />
            <style type="text/css">
            #Button
            {
                width: 80%;
                height: 20px;
                font-weight: bold;
                line-height: 20px;
                text-align: center;
                border-top-left-radius: 25px;
                border-top-right-radius: 25px;
                border-bottom-left-radius: 25px;
                border-bottom-right-radius: 25px;
                cursor: pointer;
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 0%, #61c419 0%, #b4e391 90%);
            }            
            #Button:hover
            {
                background: -moz-radial-gradient(center, ellipse cover, #b4e391 20%, #61c419 90%, #b4e391 100%);
            }
            .sub_hdRow
            {
                background-color: #EBCCD6;
                height: 20px;
                font-family: Arial;
                color: #B84D4D;
                font-size: 8.5pt;
                font-weight: bold;
            }
            .style1
            {
                width: 100%;
            }
        </style>
       
        <script language="javascript" type="text/javascript">

        function btnShow_onclick() {
            alert("rr");
            var Empcode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var RequestNo = document.getElementById("<%= txtRequestNo.ClientID %>").value;
            alert("ss");
            window.open("ViewStatusRpt.aspx?Empcode=" + btoa(Empcode) + "&RequestNo=" + btoa(RequestNo), "_blank");
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }

        </script>
        
    </head>
    </html>
    <br />
    <br />
    <div style="width: 45%; margin: 0px auto;">
        <table class="style1" style="width: 99%; top: 350px auto;">
            <tr class="style1">
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Request No &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                    <asp:TextBox ID="txtRequestNo" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                        Width="90%" MaxLength="50" />
                </td>
              
            </tr>
            <tr class="style1">
                <td style="text-align: left; margin: 0px auto; width: 15%;">
                    Employee &nbsp; &nbsp;
                </td>
                <td style="text-align: left; margin: 0px auto; width: 25%;">
                    &nbsp;
                     <asp:TextBox ID="txtEmpCode" runat="server" Style="font-family: Cambria; font-size: 10pt;"
                        Width="90%" MaxLength="50" onkeypress='return NumericCheck(event)' />
                </td>
             
            </tr>            
       
            <tr>
                <td style="text-align: center;" colspan="5">
                    <br />
                    <br />
                    <input id="btnShow" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="SHOW" onclick="return btnShow_onclick()" />
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" type="button"
                        value="EXIT" onclick="return btnExit_onclick()" />
                    <asp:HiddenField ID="hid_USer" runat="server" />
                  
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
