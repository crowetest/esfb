﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class ViewStatusRpt
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim DT As New DataTable
    Dim WebTools As New WebApp.Tools
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            Dim DTT As New DataTable
            Dim Empcode As String = GF.Decrypt(Request.QueryString.Get("Empcode"))
            Dim RequestNo As String = GF.Decrypt(Request.QueryString.Get("RequestNo"))
       

            Dim Struser As String = Session("UserID").ToString()
           
            Dim StrQuery As String = " WHERE 1 =1 "

            If Empcode <> "" Then
                StrQuery += " And created_by = 4037 "
            End If
            If RequestNo <> "" Then
                StrQuery += " and uat_request_no like '%" + RequestNo + "%'"
            End If

            Dim SqlStr As String

            'SqlStr = "select a.uat_request_id,a.uat_request_no,a.envmt_name,a.app_name,a.type_name,a.req_remarks,replace(convert(char(11),a.created_on,113),' ','-'),a.created_name + ' - ' + convert(varchar,a.created_by),a.created_branch_name,a.created_dept_name,a.uat_priority,a.assign_name + ' ('+convert(varchar,a.assign_empcode) + ')' assignto, " +
            '    " case when a.uat_status is null then 'NOT STARTED' when a.uat_status=1 then 'IN PROGRESS' when a.uat_status=2 then 'COMPLETED' when a.uat_status=3 then 'HOLD'  when a.uat_status=4 then 'VENDER PENDING' when a.uat_status=5 then 'PENDING REQUESTEE' end Status from uat_request_master a " + StrQuery + ""


            SqlStr = "select uat_request_no, CONVERT(VARCHAR,Created_by) + ' | ' +  created_name, envmt_name, app_name , type_name, created_on, req_remarks, case " +
            " when uat_verify_status = 2 THEN 'UAT SIGN OFF LEVEL REJECT'  " +
            " when uat_verify_status = 1 THEN 'UAT SIGN OFF'  " +
            " when uat_status =2  then'UAT SIGN OFF PENDING' " +
            " when uat_status not in (2) then'UAT IN PROGRESS' " +
            " when hoapprove_status = 1 then 'UAT NOT STARTED' " +
            " when hoapprove_status is null then'PENDING IT APPROVAL' " +
            " when approved_status is null then'PENDING RO APPROVAL'  " +
            " End " +
            " from uat_request_master a " + StrQuery + ""



            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            Dim b As Integer = DT.Rows.Count
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            RH.Heading(Session("FirmName"), tb, "UAT REQUEST STATUS ", 150)

            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            '
            Dim TRSHead As New TableRow
            TRSHead.Width = "100"
            TRSHead.BorderWidth = "1"
            TRSHead.BorderStyle = BorderStyle.Solid
            TRSHead.Style.Add("background-color", "lightsteelblue")

            Dim TRSHead_00 As New TableCell
            TRSHead_00.BorderWidth = "1"
            TRSHead_00.BorderColor = Drawing.Color.Silver
            TRSHead_00.BorderStyle = BorderStyle.Solid
            'Dim StrUserNam As String = Nothing

            'DTT = DB.ExecuteDataSet("select emp_name from EMP_MASTER where emp_code=  " & Struser & "").Tables(0)
            'If DTT.Rows.Count > 0 Then
            '    StrUserNam = DTT.Rows(0)(0)
            'End If
            Dim TRHead_1 As New TableRow

            'RH.AddColumn(TRSHead, TRSHead_00, 150, 150, "c", "USER :" + StrUserNam)


            RH.BlankRow(tb, 4)
            tb.Controls.Add(TRSHead)

            'TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHeadS_1_00, TRHead_1_00, TRHead_1_01, TRHead_1_02, TRHead_1_03, TRHead_1_04, TRHead_1_05, TRHead_1_06, TRHead_1_07, TRHead_1_08, TRHead_1_09, TRHead_1_10, TRHead_1_11, TRHead_1_12, TRHead_1_13, TRHead_1_14, TRHead_1_15 As New TableCell

            TRHeadS_1_00.BorderWidth = "1"
            TRHead_1_00.BorderWidth = "1"
            TRHead_1_01.BorderWidth = "1"
            TRHead_1_02.BorderWidth = "1"
            TRHead_1_03.BorderWidth = "1"
            TRHead_1_04.BorderWidth = "1"
            TRHead_1_05.BorderWidth = "1"
            TRHead_1_06.BorderWidth = "1"
            TRHead_1_07.BorderWidth = "1"
            TRHead_1_08.BorderWidth = "1"
            TRHead_1_09.BorderWidth = "1"

            TRHeadS_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_00.BorderColor = Drawing.Color.Silver
            TRHead_1_01.BorderColor = Drawing.Color.Silver
            TRHead_1_02.BorderColor = Drawing.Color.Silver
            TRHead_1_03.BorderColor = Drawing.Color.Silver
            TRHead_1_04.BorderColor = Drawing.Color.Silver
            TRHead_1_05.BorderColor = Drawing.Color.Silver
            TRHead_1_06.BorderColor = Drawing.Color.Silver
            TRHead_1_07.BorderColor = Drawing.Color.Silver
            TRHead_1_08.BorderColor = Drawing.Color.Silver
            TRHead_1_09.BorderColor = Drawing.Color.Silver


            TRHeadS_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_00.BorderStyle = BorderStyle.Solid
            TRHead_1_01.BorderStyle = BorderStyle.Solid
            TRHead_1_02.BorderStyle = BorderStyle.Solid
            TRHead_1_03.BorderStyle = BorderStyle.Solid
            TRHead_1_04.BorderStyle = BorderStyle.Solid
            TRHead_1_05.BorderStyle = BorderStyle.Solid
            TRHead_1_06.BorderStyle = BorderStyle.Solid
            TRHead_1_07.BorderStyle = BorderStyle.Solid
            TRHead_1_08.BorderStyle = BorderStyle.Solid
            TRHead_1_09.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead_1, TRHeadS_1_00, 4, 4, "l", "Serial No")
            RH.AddColumn(TRHead_1, TRHead_1_00, 10, 10, "l", "Request No") '
            RH.AddColumn(TRHead_1, TRHead_1_01, 15, 15, "l", "Requestee")
            RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "l", "Environment")
            RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Application/Module")
            RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Type")
            RH.AddColumn(TRHead_1, TRHead_1_04, 5, 5, "l", "Request Date")
            RH.AddColumn(TRHead_1, TRHead_1_05, 20, 20, "l", "Remarks")
            RH.AddColumn(TRHead_1, TRHead_1_06, 20, 20, "l", "Current Status")
         

            tb.Controls.Add(TRHead_1)
            Dim I As Integer = 0

            For Each DR In DT.Rows
                I = I + 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid
                Dim TR3S_00, TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15 As New TableCell

                TR3S_00.BorderWidth = "1"
                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
               

                TR3S_00.BorderColor = Drawing.Color.Silver
                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver


                TR3S_00.BorderStyle = BorderStyle.Solid
                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3S_00, 4, 4, "c", I.ToString()) 'SlNO
                RH.AddColumn(TR3, TR3_00, 10, 10, "l", DR(0).ToString())

                RH.AddColumn(TR3, TR3_01, 15, 15, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_07, 5, 5, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_02, 5, 5, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_03, 5, 5, "c", CDate(DR(5)).ToString("dd/MMM/yyyy"))
                RH.AddColumn(TR3, TR3_04, 20, 20, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_05, 20, 20, "l", DR(7).ToString())

                RH.AddColumn(TRHead_1, TRHeadS_1_00, 4, 4, "l", "Serial No")
                RH.AddColumn(TRHead_1, TRHead_1_00, 10, 10, "l", "Request No") '
                RH.AddColumn(TRHead_1, TRHead_1_01, 15, 15, "l", "Requestee")
                RH.AddColumn(TRHead_1, TRHead_1_07, 5, 5, "l", "Environment")
                RH.AddColumn(TRHead_1, TRHead_1_02, 5, 5, "l", "Application/Module")
                RH.AddColumn(TRHead_1, TRHead_1_03, 5, 5, "l", "Type")
                RH.AddColumn(TRHead_1, TRHead_1_04, 5, 5, "l", "Request Date")
                RH.AddColumn(TRHead_1, TRHead_1_05, 20, 20, "l", "Remarks")
                RH.AddColumn(TRHead_1, TRHead_1_06, 20, 20, "l", "Current Status")


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)

        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
        End Try

    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub


    Protected Sub cmd_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Excel.Click
        'Try
        WebTools.ExporttoExcel(DT, "Request")
        'Catch ex As Exception
        '    Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
        '    Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        'End Try
    End Sub
End Class
