﻿
Imports System.Collections
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Partial Public Class ChangePassword
    Inherits System.Web.UI.Page
    Dim gf As New GeneralFunctions
    Private FormID As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        btnSave.Attributes.Add("onclick", "return isValid()")
        If Not IsNothing(Request.QueryString.Get("ID")) Then
            FormID = CInt(Request.QueryString.Get("ID"))
        End If
        If Not IsPostBack() Then
            InitialiseControls()
        End If
        If FormID = 3 Then
            Me.head.InnerHtml = "<span class='style7' style='color:red;'>Your password expired. Change your password.</span>"
        End If
        Me.txtCurrentPassword.Attributes.Add("autocomplete", "off")
        Me.txtNewPassword.Attributes.Add("autocomplete", "off")
        Me.txtConfirmPassword.Attributes.Add("autocomplete", "off")
        Me.txtCurrentPassword.Text = ""
        Me.txtNewPassword.Text = ""
        Me.txtConfirmPassword.Text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        hdnValue.Value = hdnValue.Value
        Dim result As String = gf.ChangePassword(hdnValue.Value)
        Dim cl_script1 As New System.Text.StringBuilder()
        cl_script1.Append("         alert('" & result & "');")
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        If result = "Password Changed Successfully" Then
            If FormID = 1 Then
                InitialiseControls()
            Else
                If Session("GuestModuleID") > 0 Then
                    Response.Redirect("Home.aspx?ModuleID=" & Session("GuestModuleID"))
                Else
                    Response.Redirect("Portal.aspx")
                End If

            End If
        End If
    End Sub
    Private Sub InitialiseControls()
        If FormID = 1 Then
            txtUserID.Text = ""
            txtUserID.[ReadOnly] = False
            txtCurrentPassword.Text = ""
            txtNewPassword.Text = ""
            txtConfirmPassword.Text = ""
            txtUserID.Focus()
        Else
            txtUserID.Text = Session("UserID").ToString()
            txtUserID.[ReadOnly] = True
            txtCurrentPassword.Text = ""
            txtNewPassword.Text = ""
            txtConfirmPassword.Text = ""
            txtCurrentPassword.Focus()
        End If
    End Sub
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click
        Response.Redirect("Default.aspx", False)
    End Sub
End Class
