﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewPunchingReport
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim SQL As String = ""
    Dim GF As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Dim ReqID As Integer
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            ReqID = CInt(Request.QueryString.Get("ReqID"))
            SQL = "SELECT A.REQUEST_ID,UPPER(C.EMP_NAME)+' ('+CAST(A.USER_ID AS VARCHAR)+')' AS USER_NAME,A.TRA_DATE,A.VALUE_DT,B.STATUS_NAME,A.REMARKS  FROM CARD_REQUEST_CYCLE A,CARD_REQUEST_STATUS B,EMP_MASTER C WHERE A.STATUS_ID=B.STATUS_ID AND A.USER_ID=C.EMP_CODE AND  A.REQUEST_ID=" + ReqID.ToString() + ""
            DT = DB.ExecuteDataSet(SQL).Tables(0)

            RH.Heading(Session("FirmName"), tb, "DEBIT CARD PIN REQUEST CYCLE FOR REQUEST ID - " + ReqID.ToString(), 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim TR0 As New TableRow
            TR0.BackColor = Drawing.Color.WhiteSmoke
            Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05 As New TableCell
            RH.AddColumn(TR0, TR0_00, 5, 5, "l", "SLNO")
            RH.AddColumn(TR0, TR0_01, 15, 15, "l", "USER")
            RH.AddColumn(TR0, TR0_02, 15, 15, "l", "TRA DT")
            RH.AddColumn(TR0, TR0_03, 15, 15, "l", "EFF DT")
            RH.AddColumn(TR0, TR0_04, 15, 15, "l", "ACTION")
            RH.AddColumn(TR0, TR0_05, 35, 35, "l", "REMARKS")
            tb.Controls.Add(TR0)
            RH.DrawLine(tb, 100)

            Dim I As Integer = 1
            For Each DR In DT.Rows
                Dim TR1 As New TableRow
                If RowBG = 0 Then
                    RowBG = 1
                    TR1.BackColor = Drawing.Color.WhiteSmoke
                Else
                    RowBG = 0
                End If
                Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05 As New TableCell
                RH.AddColumn(TR1, TR1_00, 5, 5, "c", I.ToString())
                RH.AddColumn(TR1, TR1_01, 15, 15, "l", DR(1))
                RH.AddColumn(TR1, TR1_02, 15, 15, "l", CDate(DR(2)).ToString("dd MMM yyyy hh:mm:ss tt"))
                RH.AddColumn(TR1, TR1_03, 15, 15, "l", CDate(DR(3)).ToString("dd MMM yyyy"))
                RH.AddColumn(TR1, TR1_04, 15, 15, "l", DR(4).ToString())
                RH.AddColumn(TR1, TR1_05, 35, 35, "l", DR(5).ToString())
                tb.Controls.Add(TR1)
                I = I + 1
            Next
            RH.DrawLine(tb, 100)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            SQL = "SELECT A.REQUEST_ID [REQUEST ID],UPPER(C.EMP_NAME)+' ('+CAST(A.USER_ID AS VARCHAR)+')' [USER NAME],A.TRA_DATE [TRANSACTION DATE],A.VALUE_DT [EFFECTIVE DATE],B.STATUS_NAME [STATUS],A.REMARKS [REMARKS]  FROM CARD_REQUEST_CYCLE A,CARD_REQUEST_STATUS B,EMP_MASTER C WHERE A.STATUS_ID=B.STATUS_ID AND A.USER_ID=C.EMP_CODE AND  A.REQUEST_ID=" + ReqID.ToString() + ""
            DT = DB.ExecuteDataSet(SQL).Tables(0)
            Dim HeaderText As String
            HeaderText = "REQUEST-DETAILS"
            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
End Class

