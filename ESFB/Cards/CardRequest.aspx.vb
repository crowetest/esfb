﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class ResetPassword
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Debit Card / PIN Request"
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT TYPE_ID,TYPE_NAME FROM CARD_REQUEST_TYPE WHERE STATUS_ID = 1").Tables(0)
                GF.ComboFill(ddlRequestType, DT, 0, 1)
            End If
            '--//---------- Script Registrations -----------//--
            Me.btnSave.Attributes.Add("onclick", "return btnSave_onclick()")
            txtAccountNo.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Save"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim ContentType As String = ""
        Dim AttachImg As Byte() = Nothing
        Dim myFile As HttpPostedFile = fupUpload.PostedFile
        Dim nFileLen As Integer = myFile.ContentLength
        If (nFileLen > 0) Then
            ContentType = myFile.ContentType
            AttachImg = New Byte(nFileLen - 1) {}
            myFile.InputStream.Read(AttachImg, 0, nFileLen)
        End If
        Dim FileName As String = Path.GetFileName(fupUpload.PostedFile.FileName)
        Dim FileExtension As String = Path.GetExtension(fupUpload.PostedFile.FileName).ToUpper()
        If FileExtension = ".PDF" Then
            Dim AccountNo As String = Me.txtAccountNo.Text.ToString()
            Dim CIF As String = Me.txtCIF.Text.ToString()
            Dim Name As String = Me.txtName.Text.ToString()
            Dim TypeID As Integer = CInt(Me.ddlRequestType.SelectedValue)
            Dim Remarks As String = Me.txtRemarks.Text.ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(11) As SqlParameter
                Params(0) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 14)
                Params(0).Value = AccountNo
                Params(1) = New SqlParameter("@CIF", SqlDbType.VarChar, 12)
                Params(1).Value = CIF
                Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(2).Value = CInt(Session("UserID"))
                Params(3) = New SqlParameter("@Attachment", SqlDbType.VarBinary)
                Params(3).Value = AttachImg
                Params(4) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
                Params(4).Value = ContentType
                Params(5) = New SqlParameter("@FileName", SqlDbType.VarChar, 100)
                Params(5).Value = FileName
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                Params(8) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(8).Value = CInt(Session("BranchID"))
                Params(9) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                Params(9).Value = Name
                Params(10) = New SqlParameter("@TypeID", SqlDbType.Int)
                Params(10).Value = TypeID
                Params(11) = New SqlParameter("@Remarks", SqlDbType.VarChar, 300)
                Params(11).Value = Remarks
                DB.ExecuteNonQuery("SP_CARD_REQUEST", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
                Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_script1.Append("         alert('" + Message + "');window.open('CardRequest.aspx','_self')")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
        Else
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Uploaded Only Pdf file');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        End If
    End Sub
#End Region
End Class
