﻿Imports System.Data
Partial Class ConsolidatedBtwnDateReport
    Inherits System.Web.UI.Page
    Dim DT As New DataTable
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Master.subtitle = "Card Status Report"
            txtFromDt.Attributes.Add("onchange", "return DateOnChange();")
            txtToDt.Attributes.Add("onchange", "return DateOnChange();")
            btnGenerate.Attributes.Add("onclick", "return GenerateOnClick()")

            txtFromDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            txttODt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            Response.Redirect("ViewReport.aspx?FromDt=" + txtFromDt.Text + " &ToDt=" + txtToDt.Text + "&StatusID=" + Me.ddlStatus.SelectedValue.ToString() + "", False)
        Catch ex As Exception
            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
