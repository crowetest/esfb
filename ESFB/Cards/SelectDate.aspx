﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="SelectDate.aspx.vb" Inherits="ConsolidatedBtwnDateReport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Calculations.js" type="text/javascript"></script>
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function DateOnChange() {
            var frmDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
            if (frmDt != "" && ToDt != "") {
                var days = getDateDiff(frmDt, ToDt, "days");
                if (days < 0) {
                    document.getElementById("<%= txtToDt.ClientID %>").value = "";
                    alert("Check Dates");
                    document.getElementById("<%= txtToDt.ClientID %>").focus();
                    return false;
                }
            }

        }
        function GenerateOnClick() {
            var frmDt = document.getElementById("<%= txtFromDt.ClientID %>").value;
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value;
            if (frmDt == "")
            { alert("Select From Date"); document.getElementById("<%= txtFromDt.ClientID %>").focus(); return false; }
            if (ToDt == "")
            { alert("Select To Date"); document.getElementById("<%= txtToDt.ClientID %>").focus(); return false; }
        }
    </script>
    <table style="width:80%;margin: 0px auto;" >
        <tr>
            <td style="width:25%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            </td>
            <td style="width:12%; text-align:left;">
                &nbsp;</td>
            <td style="width:63%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:25%;">
            </td>
            <td style="width:12%; text-align:left;">
                Date From</td>
            <td style="width:63%">
                &nbsp; &nbsp;<asp:TextBox ID="txtFromDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtFromDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtFromDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr >
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Date To</td>
            <td style="width:63%">
                 &nbsp; &nbsp;<asp:TextBox ID="txtToDt" class="NormalText" runat="server" 
                    Width="40%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txttODt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txttODt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr >
            <td style="width:25%;">
                &nbsp;</td>
            <td style="width:12%; text-align:left;">
                Status</td>
            <td style="width:63%">
                 &nbsp;&nbsp; &nbsp;<asp:DropDownList ID="ddlStatus" Width="40%" class="NormalText" runat="server">
                    <asp:ListItem Value="1">REQUESTED</asp:ListItem>
                     <asp:ListItem Value="2">IN PROGRESS</asp:ListItem>
                      <asp:ListItem Value="6">REJECTED</asp:ListItem>
                     <asp:ListItem Value="0">RECEIVED</asp:ListItem>
                     <asp:ListItem Value="-1">ALL</asp:ListItem>
                 </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:center;"  colspan="3">
                &nbsp; 
                <asp:Button ID="btnGenerate" runat="server" Text="GENERATE" Width="10%" 
                    style="cursor:pointer" Font-Names="Cambria"  />
                &nbsp;<input id="btnExit" style="font-family: cambria; cursor: pointer; width: 10%;" 
                type="button" value="EXIT" onclick="window.open('../Home.aspx','_self')"  /></td>
        </tr>
        <tr >
            <td style="text-align:center; " colspan="3">
                <br />
                &nbsp;&nbsp;
                <asp:HiddenField ID="hdnDate" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
