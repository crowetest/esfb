﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CBR_Entry.aspx.vb" Inherits="CBR_Entry" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {
            document.getElementById('<%= txtBranchCode.ClientID %>').value = $('#<%= hidBID.ClientID %>').val();
            document.getElementById('<%= txtBranchName.ClientID %>').value = $('#<%= hidBName.ClientID %>').val();
            $("#Table1").show();
            $("#EntryTr").hide();
            $("#Table2").hide();
            $("#Table3").hide();
            $("#Table4").hide();
            $("#Table5").hide();
            $("#Table7").hide();
            $("#btnSave").hide();
            $("#btnUpdate").hide();
            $("#btnDelete").hide();
            TableFill(1);
        }
        function TaskOnChange(){
            var task = $("#<%= cmbTask.ClientID %>").val();
            if(task==1){
                $("#Table2").show();
                $("#Table3").show();
                $("#Table4").show();
                $("#Table5").show();
                $("#Table7").show();
                $("#btnSave").show();
                $("#EntryTr").hide();
            }
            else if(task==2){
                $("#EntryTr").show();
                $("#Table2").hide();
                $("#Table3").hide();
                $("#Table4").hide();
                $("#Table5").hide();
                $("#Table7").hide();
                $("#btnSave").hide();
                var branchCode = $('#<%= txtBranchCode.ClientID %>').val();
                var atmID = $('#<%= cmbAtm.ClientID %>').val();
                var Data = "2Ø" + branchCode + "Ø" + atmID;
                ToServer(Data,2);
            }
            else if(task == -1){
                $("#EntryTr").hide();
                $("#Table2").hide();
                $("#Table3").hide();
                $("#Table4").hide();
                $("#Table5").hide();
                $("#Table7").hide();
                $("#btnSave").hide();
                $("#btnUpdate").hide();
                $("#btnDelete").hide();
            }
        }
        function EntryOnChange(){
            var branchCode = $('#<%= txtBranchCode.ClientID %>').val();
            var atmID = $('#<%= cmbAtm.ClientID %>').val();
            var entryID = $('#<%= cmbEntry.ClientID %>').val();
            var Data = "3Ø" + branchCode + "Ø" + atmID + "Ø" + entryID;
            ToServer(Data,3);
        }
        function TableFill(z){
            document.getElementById("<%= pnl1.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div>";
            tab += "<table style='width:100%;line-height:40%;'>";
            tab += "<tr>";
            tab += "<td colspan='2'>";
            tab += "<div class='mainhead' style='width:100%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
            tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:30px;background-color:#B21C32;' align='center'>";
            tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
            tab += "<td rowspan='2' style='width:10%;text-align:center;font-weight:bold;'>Denomination</td>";
            tab += "<td colspan='2' style='width:15%;text-align:center;font-weight:bold;'>Opening Balance</td>";
            tab += "<td colspan='2' style='width:15%;text-align:center;font-weight:bold;'>Cash Dispense</td>";
            tab += "<td colspan='2' style='width:15%;text-align:center;font-weight:bold;'>Cash In Purge Bin</td>";
            tab += "<td colspan='2' style='width:15%;text-align:center;font-weight:bold;'>Closing Before Replenishment</td>";
            tab += "<td colspan='2' style='width:15%;text-align:center;font-weight:bold;'>Cash Replenishment</td>";
            tab += "<td colspan='2' style='width:15%;text-align:center;font-weight:bold;'>Position After Replenishment</td>";
            tab += "</tr>";
            tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
            tab += "<td style='width:6%;text-align:center;'>Count</td>";
            tab += "<td style='width:9%;text-align:center;'>Total Amount</td>";
            tab += "<td style='width:6%;text-align:center;'>Count</td>";
            tab += "<td style='width:9%;text-align:center;'>Total Amount</td>";
            tab += "<td style='width:6%;text-align:center;'>Count</td>";
            tab += "<td style='width:9%;text-align:center;'>Total Amount</td>";
            tab += "<td style='width:6%;text-align:center;'>Count</td>";
            tab += "<td style='width:9%;text-align:center;'>Total Amount</td>";
            tab += "<td style='width:6%;text-align:center;'>Count</td>";
            tab += "<td style='width:9%;text-align:center;'>Total Amount</td>";
            tab += "<td style='width:6%;text-align:center;'>Count</td>";
            tab += "<td style='width:9%;text-align:center;'>Total Amount</td>";
            tab += "</tr>";
            var Denominations = document.getElementById("<%= hidDenominations.ClientID %>").value.split("Ñ");
            var count = Denominations.length - 1;
            for(var i = 0;i<count;i++){
                var deno = Denominations[i];
                tab += "<tr style='width:100%;font-family:cambria;background-color:white;' align='left' class='sec2'>";
                tab += "<td style='width:10%;text-align:center;background-color:#FFF0E6;' id='Denomination_"+i+"'>"+Denominations[i]+"</td>";
                tab += "<td style='width:6%;text-align:center;'><input id='OB_Count_"+i+"' type='text' style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' onchange='OB_Count_Change(this.value,"+deno+","+i+")' /></td>";
                tab += "<td style='width:9%;text-align:center;text-align:center;background-color:#FFF0E6;' id='OB_Tot_"+i+"'></td>";
                tab += "<td style='width:6%;text-align:center;'><input id='CD_Count_"+i+"' type='text' style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' onchange='CD_Count_Change(this.value,"+deno+","+i+")' /></td>";
                tab += "<td style='width:9%;text-align:center;text-align:center;background-color:#FFF0E6;' id='CD_Tot_"+i+"'></td>";
                tab += "<td style='width:6%;text-align:center;'><input id='CPB_Count_"+i+"' type='text' style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' onchange='CPB_Count_Change(this.value,"+deno+","+i+")' /></td>";
                tab += "<td style='width:9%;text-align:center;text-align:center;background-color:#FFF0E6;' id='CPB_Tot_"+i+"'></td>";
                tab += "<td style='width:6%;text-align:center;background-color:#FFF0E6;' id='CBR_Count_"+i+"' ></td>";
                tab += "<td style='width:9%;text-align:center;text-align:center;background-color:#FFF0E6;' id='CBR_Tot_"+i+"'></td>";
                tab += "<td style='width:6%;text-align:center;'><input id='CR_Count_"+i+"' type='text' style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' onchange='CR_Count_Change(this.value,"+deno+","+i+")' /></td>";
                tab += "<td style='width:9%;text-align:center;text-align:center;background-color:#FFF0E6;' id='CR_Tot_"+i+"'></td>";
                tab += "<td style='width:6%;text-align:center;background-color:#FFF0E6;' id='PAR_Count_"+i+"' ></td>";
                tab += "<td style='width:9%;text-align:center;text-align:center;background-color:#FFF0E6;' id='PAR_Tot_"+i+"'></td>";
                tab += "</tr>";
            } 
            tab += "<tr style='width:100%;font-family:cambria;background-color:#F2F1F0;' align='left' class='sec2'>";
            tab += "<td style='width:10%;text-align:center;'>Total</td>";
            tab += "<td style='width:6%;text-align:center;' id='obc'></td>";
            tab += "<td style='width:9%;text-align:center;' id='obt'></td>";
            tab += "<td style='width:6%;text-align:center;' id='cdc'></td>";
            tab += "<td style='width:9%;text-align:center;' id='cdt'></td>";
            tab += "<td style='width:6%;text-align:center;' id='cpbc'></td>";
            tab += "<td style='width:9%;text-align:center;' id='cpbt'></td>";
            tab += "<td style='width:6%;text-align:center;' id='cbrc'></td>";
            tab += "<td style='width:9%;text-align:center;' id='cbrt'></td>";
            tab += "<td style='width:6%;text-align:center;' id='crc'></td>";
            tab += "<td style='width:9%;text-align:center;' id='crt'></td>";
            tab += "<td style='width:6%;text-align:center;' id='parc'></td>";
            tab += "<td style='width:9%;text-align:center;' id='part'></td>";
            tab += "</tr>";
            tab += "</table>";
            tab += "</div>";
            tab += "<br /><br /><br /><br /><br />";
            tab += "</td>";                         
            tab += "</tr>";         
            tab += "<table style='width:100%; margin:0px auto;font-family:cambria;line-height:30px;' align='center'>";
            tab += "<tr>";
             tab+="<td style='width:8%'></td>";
             tab+="<td style='width:73%;'>";
                tab += "<table id = 'x' style='width:75%; margin:0px auto;font-family:cambria;line-height:30px;background-color:#B21C32;' align='center'>";
                tab += "<tr style='width:80%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
                tab += "<td rowspan='2' style='width:30%;text-align:center;font-weight:bold;'>Denomination</td>";
                var select = "<select id='cmb_SorO' class='NormalText' style='width:100%' onchange = 'return sOroChange()'>";
                select += "<option value='1' selected=true>Shortage</option>";
                select += "<option value='2'>Overage</option>";
                select += "<option value='3'>Tally</option>";
                tab += "<td colspan='2' style='width:70%;text-align:center;font-weight:bold;'>"+select+"</td>";
                tab += "</tr>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
                tab += "<td style='width:35%;text-align:center;'>Count</td>";
                tab += "<td style='width:35%;text-align:center;'>Total Amount</td>";
                tab += "</tr>";
                for(var i = 0;i<count;i++){
                    var deno = Denominations[i];
                    tab += "<tr style='width:100%;font-family:cambria;background-color:white;' align='left' class='sec2'>";
                    tab += "<td style='width:30%;text-align:center;background-color:#FFF0E6;' id='Denomination_"+i+"'>"+Denominations[i]+"</td>";
                    tab += "<td style='width:35%;text-align:center;'><input id='SORO_Count_"+i+"' type='text' style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' onchange='SORO_Count_Change(this.value,"+deno+","+i+")'/></td>";
                    tab += "<td style='width:35%;text-align:center;text-align:center;background-color:#FFF0E6;' id='SORO_Tot_"+i+"'></td>";
                    tab += "</tr>";
                } 
                tab += "<tr style='width:80%;font-family:cambria;background-color:#F2F1F0;' align='left' class='sec2'>";
                tab += "<td style='width:30%;text-align:center;font-weight:bold;'>Total</td>";
                tab += "<td style='width:35%;text-align:center;font-weight:bold;' id='soroc'></td>";
                tab += "<td style='width:35%;text-align:center;font-weight:bold;' id='sorot'></td>";
                tab += "</tr>";
                tab += "</table>";
            tab+="</td>";
            tab+="<td style='width:10%'></td>";
            tab += "</tr>";
            tab += "</table>";
            tab += "</div>";
            document.getElementById("<%= pnl1.ClientID %>").innerHTML = tab;
            if(z==2){
                TableFillData();
            }
        }
        function OB_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('OB_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('OB_Count_'+j).value);
                sum += Number(document.getElementById('OB_Tot_'+j).innerHTML);
            }
            document.getElementById('obt').innerHTML = sum;
            ChangeCBR(i,deno);
        }
        function CD_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('CD_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('CD_Count_'+j).value);
                sum += Number(document.getElementById('CD_Tot_'+j).innerHTML);
            }
            document.getElementById('cdt').innerHTML = sum;
            ChangeCBR(i,deno);
        }
        function CPB_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('CPB_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('CPB_Count_'+j).value);
                sum += Number(document.getElementById('CPB_Tot_'+j).innerHTML);
            }
            document.getElementById('cpbt').innerHTML = sum;
        }
        function CR_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('CR_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('CR_Count_'+j).value);
                sum += Number(document.getElementById('CR_Tot_'+j).innerHTML);
            }
            document.getElementById('crt').innerHTML = sum;
            ChangePAR(i,deno);
        }
        function SORO_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('SORO_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('SORO_Count_'+j).value);
                sum += Number(document.getElementById('SORO_Tot_'+j).innerHTML);
            }
            document.getElementById('sorot').innerHTML = sum;
        }
        function CBR_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('CBR_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('CBR_Count_'+j).innerHTML);
                sum += Number(document.getElementById('CBR_Tot_'+j).innerHTML);
            }
            document.getElementById('cbrt').innerHTML = sum;
        }
        function PAR_Count_Change(val,deno,i){
            var total = val * deno;
            document.getElementById('PAR_Tot_'+i).innerHTML = total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('PAR_Count_'+j).innerHTML);
                sum += Number(document.getElementById('PAR_Tot_'+j).innerHTML);
            }
            document.getElementById('part').innerHTML = sum;
        }
        function ChangeCBR(i,deno){
            var OB_Count = document.getElementById('OB_Count_'+i).value;
            var CD_Count = document.getElementById('CD_Count_'+i).value;
            var CBR_Count = Number(OB_Count) - Number(CD_Count);
            document.getElementById('CBR_Count_'+i).innerHTML = CBR_Count;
            var CBR_Total = CBR_Count * deno;
            document.getElementById('CBR_Tot_'+i).innerHTML = CBR_Total;
            var count = 0;
            var sum = 0;
            for(j=0;j<=i;j++){
                count += Number(document.getElementById('CBR_Count_'+j).innerHTML);
                sum += Number(document.getElementById('CBR_Tot_'+j).innerHTML);
            }
            document.getElementById('cbrt').innerHTML = sum;
            ChangePAR(i,deno);
        }
        function ChangePAR(i,deno){
            var CBR_Count = document.getElementById('CBR_Count_'+i).innerHTML;
            var CR_Count = document.getElementById('CR_Count_'+i).value;
            var PAR_Count = Number(CBR_Count) + Number(CR_Count);
            var PAR_Total = PAR_Count * deno;
            document.getElementById('PAR_Count_'+i).innerHTML = PAR_Count;
            document.getElementById('PAR_Tot_'+i).innerHTML = PAR_Total;
            var count = 0;
            var sum = 0;
            for(var j=0;j<=i;j++){
                count += Number(document.getElementById('PAR_Count_'+j).innerHTML);
                sum += Number(document.getElementById('PAR_Tot_'+j).innerHTML);
            }
            document.getElementById('part').innerHTML = sum;
        }
        function GLChange(i,amount){
            var GLBefore = document.getElementById('<%= txtBefore_GL.ClientID %>').value;
            var GLAfter = document.getElementById('<%= txtAfter_GL.ClientID %>').value;
            var Total = Number(GLBefore)+Number(GLAfter);
            FinalChange();
        }
        function PCBChange(i,amount){
            var PCBefore = document.getElementById('<%= PCB_Before.ClientID %>').value;
            var PCAfter = document.getElementById('<%= PCB_After.ClientID %>').value;
            var Total = Number(PCBefore)+Number(PCAfter);
            FinalChange();
        }
        function FinalChange(){
            var GL = document.getElementById('<%= txtAfter_GL.ClientID %>').value;
            var Cash = document.getElementById('<%= PCB_After.ClientID %>').value;
            var amount = Cash - GL;
            if(amount > 0){ //overage
                document.getElementById("txtShortOver").innerHTML = "Overage";
                document.getElementById("txtAmount").innerHTML = "+"+amount;
            }
            else if(amount < 0){ //shortage
                document.getElementById("txtShortOver").innerHTML = "Shortage";
                document.getElementById("txtAmount").innerHTML = amount;
            }
            else if(amount == 0){ //tally
                document.getElementById("txtShortOver").innerHTML = "Tally";
                document.getElementById("txtAmount").innerHTML = amount;
            }

        }
        function Saveonclick(mode){
            var EntryID = 0;
            if(mode == 2){
                EntryID = document.getElementById('<%= cmbEntry.ClientID %>').value;
            }
            var Branch_Code = document.getElementById('<%= txtBranchCode.ClientID %>').value;
            var Branch_Name = document.getElementById('<%= txtBranchName.ClientID %>').value;
            var ATM_ID = document.getElementById('<%= cmbAtm.ClientID %>').value;
            var Cash_GL = document.getElementById('<%= Cash_GL.ClientID %>').value;
            var PCB_Before = document.getElementById('<%= PCB_Before.ClientID %>').value;
            var PCB_After = document.getElementById('<%= PCB_After.ClientID %>').value;
            var Branch_Remarks = document.getElementById('<%= cmbRemarks.ClientID %>').value;
            var Branch_Contact = document.getElementById('<%= Contact_No.ClientID %>').value;
            var Created_By = document.getElementById('<%= hidUser.ClientID %>').value;
            var SorO = document.getElementById('cmb_SorO').value;
            var GL_Before = document.getElementById('<%= txtBefore_GL.ClientID %>').value;
            var GL_After = document.getElementById('<%= txtAfter_GL.ClientID %>').value;

            //dec17
            var termBal = document.getElementById('<%= Term_Bal.ClientID %>').value;
            var admBal = document.getElementById('<%= Admin_Bal.ClientID %>').value;
            var atmStatus = document.getElementById('txtShortOver').innerHTML;
            var atmAmount = document.getElementById('txtAmount').innerHTML;
            //dec17

            var Sub_Entry = "";
            var Denominations = document.getElementById("<%= hidDenominations.ClientID %>").value.split("Ñ");
            var count = Denominations.length - 1;
            
            for(var i = 0;i<count;i++){
            
               var Denomination = Number(document.getElementById("Denomination_"+i).innerHTML);
                var OB_Count = Number(document.getElementById("OB_Count_"+i).value);
                var OB_Total = Number(document.getElementById("OB_Tot_"+i).innerHTML);
                var CD_Count = Number(document.getElementById("CD_Count_"+i).value);
                var CD_Total = Number(document.getElementById("CD_Tot_"+i).innerHTML);
                var CPB_Count = Number(document.getElementById("CPB_Count_"+i).value);
                var CPB_Total = Number(document.getElementById("CPB_Tot_"+i).innerHTML);
                var CBR_Count = Number(document.getElementById("CBR_Count_"+i).innerHTML);
                var CBR_Total = Number(document.getElementById("CBR_Tot_"+i).innerHTML);
                var CR_Count = Number(document.getElementById("CR_Count_"+i).value);
                var CR_Total = Number(document.getElementById("CR_Tot_"+i).innerHTML);
                var PAR_Count = Number(document.getElementById("PAR_Count_"+i).innerHTML);
                var PAR_Total = Number(document.getElementById("PAR_Tot_"+i).innerHTML);
                var Short_Count = Number(document.getElementById("SORO_Count_"+i).value);
                var Short_Total = Number(document.getElementById("SORO_Tot_"+i).innerHTML);
                Sub_Entry += Denomination + "ÿ" + OB_Count + "ÿ" + OB_Total + "ÿ" + CD_Count + "ÿ" + CD_Total + "ÿ" + CPB_Count + "ÿ" + CPB_Total + "ÿ" + CBR_Count + "ÿ" + CBR_Total + "ÿ" + CR_Count + "ÿ" + CR_Total + "ÿ" + PAR_Count + "ÿ" + PAR_Total + "ÿ" + Short_Count + "ÿ" + Short_Total + "Ñ";       
            }
            
            var SaveData = "1Ø" + mode + "Ø" + Branch_Code + "Ø" + Branch_Name + "Ø" + ATM_ID + "Ø" + Cash_GL + "Ø" + PCB_Before + "Ø" + PCB_After + "Ø" + Branch_Remarks + "Ø" + Branch_Contact + "Ø" + Created_By + "Ø" + Sub_Entry + "Ø" + SorO + "Ø" + EntryID + "Ø" + GL_Before + "Ø" + GL_After + "Ø" + termBal + "Ø" + admBal + "Ø" + atmStatus + "Ø" + atmAmount;
            ToServer(SaveData ,1); 
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                if(Data[0] == 0){
                    alert(Data[1].toString());
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");  
                }
            }
            if(context == 2){
                ComboFill(arg, "<%= cmbEntry.ClientID %>") ;
            }
            if(context == 3){                
                var Data = arg.split("Ø");
                document.getElementById('<%= hidEntryData.ClientID %>').value = Data[0].toString();
                document.getElementById('<%= hidSubData.ClientID %>').value = Data[1].toString();
                $("#Table2").show();
                $("#Table3").show();
                $("#Table4").show(); 
                $("#Table5").show();
                $("#Table7").show();
                $("#btnSave").hide();
                $("#btnUpdate").show();
                $("#btnDelete").show(); 
                TableFill(2);
            }    
            if (context == 4) {
                var Data = arg.split("Ø");
                if(Data[0] == 0){
                    alert(Data[1].toString());
                    window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");  
                }
            }        
        }
        function DeleteOnClick(){
            var EntryID = document.getElementById('<%= cmbEntry.ClientID %>').value;
            var SaveData = "4Ø3Ø" +EntryID;
            ToServer(SaveData ,4); 
        }
        function TableFillData(){
            var entryData = document.getElementById('<%= hidEntryData.ClientID %>').value;
            var EntryRows = entryData.split("Ñ");
            for(var i = 0;i<EntryRows.length-1;i++){
                var EntryCols = EntryRows[i].split("ÿ");
                document.getElementById('<%= Cash_GL.ClientID %>').value = EntryCols[0];
                document.getElementById('<%= PCB_Before.ClientID %>').value = EntryCols[1];
                document.getElementById('<%= PCB_After.ClientID %>').value = EntryCols[2];
                document.getElementById('<%= cmbRemarks.ClientID %>').value = EntryCols[3];
                document.getElementById('<%= Contact_No.ClientID %>').value = EntryCols[4];
                document.getElementById('cmb_SorO').value = EntryCols[5];
                document.getElementById('<%= txtBefore_GL.ClientID %>').value = EntryCols[6];
                document.getElementById('<%= txtAfter_GL.ClientID %>').value = EntryCols[7];

                document.getElementById('<%= Term_Bal.ClientID %>').value = EntryCols[8];
                document.getElementById('<%= Admin_Bal.ClientID %>').value = EntryCols[9];
                document.getElementById('txtShortOver').innerHTML = EntryCols[10];
                document.getElementById('txtAmount').innerHTML = EntryCols[11];

            }
            var obt = 0;
            var cdt = 0;
            var cpbt = 0;
            var cbrt = 0;
            var crt = 0;
            var part = 0;
            var st = 0;
            var subData = document.getElementById('<%= hidSubData.ClientID %>').value;
            var subRows = subData.split("Ñ");
            for(var i = 0;i<subRows.length-1;i++){
                var subCols = subRows[i].split("ÿ");
                document.getElementById("OB_Count_"+i).value = subCols[0];
                document.getElementById("OB_Tot_"+i).innerHTML = subCols[1];
                obt += Number(subCols[1]);
                document.getElementById("CD_Count_"+i).value = subCols[2];
                document.getElementById("CD_Tot_"+i).innerHTML = subCols[3];
                cdt += Number(subCols[3]);
                document.getElementById("CPB_Count_"+i).value = subCols[4];
                document.getElementById("CPB_Tot_"+i).innerHTML = subCols[5];
                cpbt += Number(subCols[5]);
                document.getElementById("CBR_Count_"+i).innerHTML = subCols[6];
                document.getElementById("CBR_Tot_"+i).innerHTML = subCols[7];
                cbrt += Number(subCols[7]);
                document.getElementById("CR_Count_"+i).value = subCols[8];
                document.getElementById("CR_Tot_"+i).innerHTML = subCols[9];
                crt += Number(subCols[9]);
                document.getElementById("PAR_Count_"+i).innerHTML = subCols[10];
                document.getElementById("PAR_Tot_"+i).innerHTML = subCols[11];
                part += Number(subCols[11]);
                document.getElementById("SORO_Count_"+i).value = subCols[12];
                document.getElementById("SORO_Tot_"+i).innerHTML = subCols[13];
                st += Number(subCols[13]);
            }
            document.getElementById('obt').innerHTML = obt;
            document.getElementById('cdt').innerHTML = cdt;
            document.getElementById('cpbt').innerHTML = cpbt;
            document.getElementById('cbrt').innerHTML = cbrt;
            document.getElementById('crt').innerHTML = crt;
            document.getElementById('part').innerHTML = part;
            document.getElementById('sorot').innerHTML = st;
        }        
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 0; a < rows.length-1; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
        function sOroChange(){
            if( document.getElementById("cmb_SorO").value == 3){
                var Denominations = document.getElementById("<%= hidDenominations.ClientID %>").value.split("Ñ");
                var count = Denominations.length - 1;
                for(var i = 0;i<count;i++){
                    document.getElementById("SORO_Count_"+i).disabled = true;
                }
            }
            else{
                var Denominations = document.getElementById("<%= hidDenominations.ClientID %>").value.split("Ñ");
                var count = Denominations.length - 1;
                for(var i = 0;i<count;i++){
                    document.getElementById("SORO_Count_"+i).disabled = false;
                }
            }
        }
        function DecimalCheck(e,el)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
    </script>
</head>
</html>
<br />
<div  style="width:98%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:98%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />        
        <table id = "Table1" class="style1" style="width:80%;margin: 0px auto;line-height:30px;">
            <tr> 
                <td style="width:25%;">
                    <asp:HiddenField ID="hidData" runat="server" />
                    <asp:HiddenField ID="hidDenominations" runat="server" />
                    <asp:HiddenField ID="hidBID" runat="server" />
                    <asp:HiddenField ID="hidBName" runat="server" />
                    <asp:HiddenField ID="hidUser" runat="server" />
                    <asp:HiddenField ID="hidEntryData" runat="server" />
                    <asp:HiddenField ID="hidSubData" runat="server" />
                </td>
                <td style="width:20%; text-align:left;">
                </td>
                <td style="width:30%">
                </td>
                <td style="width:25%">
                </td>
            </tr>
            <tr> 
                <td style="width:25%;"></td>
                <td style="width:20%; text-align:left;">
                    Branch Code
                </td>
                <td style="width:30%">
                    <asp:TextBox ID="txtBranchCode" class="NormalText" ReadOnly="true" runat="server" Width="80%" ></asp:TextBox>
                </td>
                <td style="width:25%;"></td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:20%; text-align:left;">
                    Branch Name
                </td>
                <td style="width:30%">
                    <asp:TextBox ID="txtBranchName" class="NormalText" ReadOnly="true" runat="server" Width="80%" BackColor="#FFF0E6" ></asp:TextBox>
                </td>
                <td style="width:25%;"></td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:20%; text-align:left;">
                    ATM ID
                </td>
                <td style="width:30%">
                    <asp:DropDownList ID="cmbAtm" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                    </asp:DropDownList>
                </td>
                <td style="width:25%;"></td>
            </tr>
            <tr>
                <td style="width:25%">
                </td>
                <td style="width:20%; text-align:left;">
                    Select Task
                </td>
                <td style="width:30%">
                    <asp:DropDownList ID="cmbTask" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black" Onchange="return TaskOnChange()">
                        <asp:ListItem Text="--SELECT--" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="ADD NEW" Value="1"></asp:ListItem>
                        <asp:ListItem Text="UPDATE / REMOVE" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width:25%">
                </td>
            </tr>
            <tr id="EntryTr">
                <td style="width:25%">
                </td>
                <td style="width:20%; text-align:left;">
                    Select Entry
                </td>
                <td style="width:30%">
                    <asp:DropDownList ID="cmbEntry" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black" onchange="return EntryOnChange()">
                        
                    </asp:DropDownList>
                </td>
                <td style="width:25%">
                </td>
            </tr>
        </table>
        <br /><br />
        <table id="Table2" style="width:98%;height:95px;margin:0px auto;">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnl1" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
        </table>

        <br /><br />
        <table id="Table7" style='width:98%;' align='center'>
            <tr>
                <td style="width:30%;"></td>
                <td style="width:40%;">
                    <table style='width:75%; margin:0px auto;font-family:cambria;line-height:30px;background-color:#B21C32;' align='center'>                        
                        <tr style='width:100%;font-family:cambria;background-color:#EEB8A6;text-align:center;' align='center' class='sec2'>
                            <td style="text-align:center;width:50%;">
                                Terminal Balance
                            </td>
                            <td style="text-align:center;width:50%;">
                                Admin Balance
                            </td>
                        </tr>
                         <tr style='width:100%;font-family:cambria;background-color:white;text-align:center;' align='center' class='sec2'>
                            <td id="Td1" style="text-align:center;background-color:#FFF0E6;height:30px;">
                                <asp:TextBox ID="Term_Bal" class="NormalText" runat="server" style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                            <td id="Td2" style="text-align:center;background-color:#FFF0E6;height:30px;">
                                <asp:TextBox ID="Admin_Bal" class="NormalText" runat="server" style='width:95%;height:25px;' onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:30%;"></td>
            </tr>
        </table>    


        <br /><br />
        <table id="Table3" style='width:98%;' align='center'>
            <tr>
                <td style="width:10%;"></td>
                <td style="width:80%;">
                    <table style='width:75%; margin:0px auto;font-family:cambria;line-height:30px;background-color:#B21C32;' align='center'>                        
                        <tr style='width:100%;font-family:cambria;background-color:#EEB8A6;text-align:center;' align='center' class='sec2'>
                            <td rowspan='2' style="text-align:center;width:20%;font-weight:bold;">
                                Cash Loaded as per GL
                            </td>
                            <td colspan='2' style="text-align:center;width:40%;font-weight:bold;">
                                Physical Cash Balance
                            </td>
                            <td colspan='2' style="text-align:center;width:40%;font-weight:bold;">
                                GL Balance
                            </td>
                        </tr>
                        <tr style='width:100%;font-family:cambria;background-color:#EEB8A6;text-align:center;' align='center' class='sec2'>
                            <td style="text-align:center;">
                                Before
                            </td>
                            <td style="text-align:center;">
                                After
                            </td>
                            <td style="text-align:center;">
                                Before
                            </td>
                            <td style="text-align:center;">
                                After
                            </td>
                        </tr>
                        <tr style='width:100%;font-family:cambria;background-color:white;text-align:center;' align='center' class='sec2'>
                            <td style="text-align:center;background-color:#FFF0E6;height:30px;">
                                <asp:TextBox ID="Cash_GL" class="NormalText" runat="server" style='width:95%;height:25px;' onchange="return Tab3Change(1)" onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                            <td style="text-align:center;background-color:#FFF0E6;height:30px;">
                                <asp:TextBox ID="PCB_Before" class="NormalText" runat="server" style='width:95%;height:25px;' onchange="return PCBChange(1,this.value)" onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                            <td style="text-align:center;background-color:#FFF0E6;height:30px;" >
                                <asp:TextBox ID="PCB_After" class="NormalText" runat="server" style='width:95%;height:25px;' onchange="return PCBChange(2,this.value)" onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                            <td style="text-align:center;background-color:#FFF0E6;height:30px;">
                                <asp:TextBox ID="txtBefore_GL" class="NormalText" runat="server" style='width:95%;height:25px;' onchange="return GLChange(1,this.value)" onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                            <td style="text-align:center;background-color:#FFF0E6;height:30px;">
                                <asp:TextBox ID="txtAfter_GL" class="NormalText" runat="server" style='width:95%;height:25px;' onchange="return GLChange(2,this.value)" onkeypress='return NumericCheck(event)' ></asp:TextBox>    
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:10%;"></td>
            </tr>
        </table>    
        <br /><br />
        <table id="Table5" style='width:98%;' align='center'>
            <tr>
                <td style="width:30%;"></td>
                <td style="width:40%;">
                    <table style='width:75%; margin:0px auto;font-family:cambria;line-height:30px;background-color:#B21C32;' align='center'>                        
                        <tr style='width:100%;font-family:cambria;background-color:#EEB8A6;text-align:center;' align='center' class='sec2'>
                            <td style="text-align:center;width:50%;">
                                ATM Status
                            </td>
                            <td style="text-align:center;width:50%;">
                                Amount
                            </td>
                        </tr>
                         <tr style='width:100%;font-family:cambria;background-color:white;text-align:center;' align='center' class='sec2'>
                            <td id="txtShortOver" style="text-align:center;background-color:#FFF0E6;height:30px;">
                                
                            </td>
                            <td id="txtAmount" style="text-align:center;background-color:#FFF0E6;height:30px;">
                                
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:30%;"></td>
            </tr>
        </table>    
        <br /><br />
        <table id="Table4" class="style1" style="width:80%;margin: 0px auto;">
            <tr>
                <td style="width:25%;"></td>
                <td style="width:20%; text-align:left;">
                    Branch Remarks
                </td>
                <td style="width:30%">
                    <asp:DropDownList ID="cmbRemarks" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                    </asp:DropDownList>
                </td>
                <td style="width:25%;"></td>
            </tr>
            <tr>
                <td style="width:25%"></td>
                <td style="width:20%; text-align:left;">
                    Branch Contact No.
                </td>
                <td style="width:30%">
                    <asp:TextBox ID="Contact_No" class="NormalText" runat="server" Width="80%" ></asp:TextBox>
                </td>
                <td style="width:25%"></td>
            </tr>
            <tr>
                <td style="width:25%"></td>
                <td style="width:20%; text-align:left;">
                    Attachment
                </td>
                <td style="width:30%">
                    <div id="fileUploadarea">
                        <asp:FileUpload ID="upload" runat="server" CssClass="fileUpload"/><br />
                    </div>
                </td>
                <td style="width:25%"></td>
            </tr>
        </table>
        <br /><br />
    </div>
    <div  style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
        <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
            <div style="text-align:center; height: 63px;"><br />
                <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="SAVE" onclick="return Saveonclick(1)"/>       
                &nbsp;
                &nbsp;
                <input id="btnUpdate" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="UPDATE" onclick="return Saveonclick(2)"/>       
                &nbsp;
                &nbsp;
                <input id="btnDelete" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="DELETE" onclick="return DeleteOnClick()"/>       
                &nbsp;
                &nbsp;
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" onclick="return btnExit_onclick()" />
            </div>
        </div>
    </div>   
</div>
</asp:Content>

