﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CBR_Entry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1286) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = DB.ExecuteDataSet("SELECT Value FROM CBR_Denomination_Master WHERE Status = 1 ORDER BY Value ASC").Tables(0)
            Dim Denominations As String = ""
            Dim i As Integer = 0
            For Each dtt In DT.Rows
                Denominations += DT.Rows(i)(0).ToString() + "Ñ"
                i += 1
            Next
            Me.hidDenominations.Value = Denominations
            Me.hidUser.Value = CStr(Session("UserID"))
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
            Me.hidBID.Value = DT.Rows(0)(0).ToString()
            Me.hidBName.Value = DT.Rows(0)(1).ToString()
            DT1 = DB.ExecuteDataSet("SELECT -1 AS VALUE,'---SELECT---' AS ATM_ID UNION ALL SELECT MASTER_ID,ATM_ID FROM CBR_ATM_MASTER WHERE BRANCH_ID = " + Me.hidBID.Value).Tables(0)
            GF.ComboFill(cmbAtm, DT1, 0, 1)
            If DT1.Rows.Count <= 1 Then
                Me.cmbTask.Enabled = False
            End If
            DT2 = DB.ExecuteDataSet("SELECT -1 AS VALUE,'---SELECT---' AS REMARK UNION ALL SELECT VALUE,REMARK FROM CBR_BRANCH_REMARKS WHERE STATUS = 1").Tables(0)
            GF.ComboFill(cmbRemarks, DT2, 0, 1)
            Me.Master.subtitle = "CBR ENTRY"

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Try
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
                Dim ContentType As String = ""
                Dim AttachImg As Byte() = Nothing
                Dim hfc As HttpFileCollection
                hfc = Request.Files
                Dim Mode As Integer = CInt(Data(1).ToString())
                Dim Branch_Code As Integer = CInt(Data(2).ToString())
                Dim Branch_Name As String = Data(3)
                Dim ATM_ID As Integer = CInt(Data(4).ToString())
                Dim Cash_GL As Integer = CInt(Data(5).ToString())
                Dim PCB_Before As Integer = CInt(Data(6).ToString())
                Dim PCB_After As Integer = CInt(Data(7).ToString())
                Dim Branch_Remarks As Integer = CInt(Data(8).ToString())
                Dim Branch_Contact As String = Data(9)
                Dim Created_By As Integer = CInt(Data(10).ToString())
                Dim Sub_Entry As String = Data(11)
                Dim SorO As Integer = CInt(Data(12).ToString())
                Dim EntryID As Integer = CInt(Data(13).ToString())
                Dim GL_Before As Integer = CInt(Data(14).ToString())
                Dim GL_After As Integer = CInt(Data(15).ToString())
                Dim termBal As Integer = CInt(Data(16).ToString())
                Dim admBal As Integer = CInt(Data(17).ToString())
                Dim atmStatus As String = Data(18).ToString()
                Dim atmAmount As Integer = CInt(Data(19).ToString())
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Entry_ID As Integer = 0
                Dim Params(21) As SqlParameter
                Params(0) = New SqlParameter("@Mode", SqlDbType.Int)
                Params(0).Value = Mode
                Params(1) = New SqlParameter("@Branch_Code", SqlDbType.Int)
                Params(1).Value = Branch_Code
                Params(2) = New SqlParameter("@Branch_Name", SqlDbType.VarChar, 100)
                Params(2).Value = Branch_Name
                Params(3) = New SqlParameter("@ATM_ID", SqlDbType.Int)
                Params(3).Value = ATM_ID
                Params(4) = New SqlParameter("@Cash_GL", SqlDbType.Int)
                Params(4).Value = Cash_GL
                Params(5) = New SqlParameter("@PCB_Before", SqlDbType.Int)
                Params(5).Value = PCB_Before
                Params(6) = New SqlParameter("@PCB_After", SqlDbType.Int)
                Params(6).Value = PCB_After
                Params(7) = New SqlParameter("@Branch_Remarks", SqlDbType.Int)
                Params(7).Value = Branch_Remarks
                Params(8) = New SqlParameter("@Branch_Contact", SqlDbType.VarChar, 30)
                Params(8).Value = Branch_Contact
                Params(9) = New SqlParameter("@Created_By", SqlDbType.Int)
                Params(9).Value = Created_By
                Params(10) = New SqlParameter("@Sub_Entry", SqlDbType.VarChar, 1000)
                Params(10).Value = Sub_Entry
                Params(11) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(11).Direction = ParameterDirection.Output
                Params(12) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(12).Direction = ParameterDirection.Output
                Params(13) = New SqlParameter("@SorO", SqlDbType.Int)
                Params(13).Value = SorO
                Params(14) = New SqlParameter("@Entry_ID", SqlDbType.Int)
                Params(14).Direction = ParameterDirection.Output
                Params(15) = New SqlParameter("@MasterID", SqlDbType.Int)
                Params(15).Value = EntryID
                Params(16) = New SqlParameter("@GL_Before", SqlDbType.Int)
                Params(16).Value = GL_Before
                Params(17) = New SqlParameter("@GL_After", SqlDbType.Int)
                Params(17).Value = GL_After
                Params(18) = New SqlParameter("@termBal", SqlDbType.Int)
                Params(18).Value = termBal
                Params(19) = New SqlParameter("@admBal", SqlDbType.Int)
                Params(19).Value = admBal
                Params(20) = New SqlParameter("@atmStatus", SqlDbType.VarChar, 30)
                Params(20).Value = atmStatus
                Params(21) = New SqlParameter("@atmAmount", SqlDbType.Int)
                Params(21).Value = atmAmount
                DB.ExecuteNonQuery("SP_CBR_ENTRY_SAVE", Params)
                ErrorFlag = CInt(Params(11).Value)
                Message = CStr(Params(12).Value)
                Entry_ID = CInt(Params(14).Value)


                'If Entry_ID > 0 Then
                '    'For i = 0 To hfc.Count - 1
                '    Dim myFile As HttpPostedFile = hfc(0)
                '    Dim nFileLen As Integer = myFile.ContentLength
                '    Dim FileName As String = ""
                '    If (nFileLen > 0) Then
                '        ContentType = myFile.ContentType
                '        FileName = myFile.FileName
                '        AttachImg = New Byte(nFileLen - 1) {}
                '        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                '        Dim Param(3) As SqlParameter
                '        Param(0) = New SqlParameter("@Entry_ID", SqlDbType.Int)
                '        Param(0).Value = Entry_ID
                '        Param(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                '        Param(1).Value = AttachImg
                '        Param(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                '        Param(2).Value = ContentType
                '        Param(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                '        Param(3).Value = FileName
                '        DB.ExecuteNonQuery("SP_CBR_UPLOAD", Param)
                '    End If
                '    'Next


                'End If

                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            End If
            If CInt(Data(0)) = 2 Then
                Dim branchCode As Integer = CInt(Data(1).ToString())
                Dim atmID As Integer = CInt(Data(2).ToString())
                DT = DB.ExecuteDataSet("SELECT -1 AS VALUE,'---SELECT---' AS EntryDate UNION ALL SELECT Entry_ID as VALUE,convert(varchar, created_date, 0) As EntryDate FROM CBR_ENTRY_MASTER WHERE CONVERT(VARCHAR(10), Created_Date, 105) = CONVERT(VARCHAR(10), getdate(), 105) and branch_code = " + branchCode.ToString() + " and atm_id = " + atmID.ToString()).Tables(0)
                'GF.ComboFill(cmbEntry, DT, 0, 1)
                Dim comboData As String = ""
                'Dim i As Integer = 0
                For i As Integer = 0 To DT.Rows.Count - 1
                    comboData += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "Ñ"
                Next
                CallBackReturn += comboData
            End If
            If CInt(Data(0)) = 3 Then
                Dim branchCode As Integer = CInt(Data(1).ToString())
                Dim atmID As Integer = CInt(Data(2).ToString())
                Dim entryID As Integer = CInt(Data(3).ToString())
                Dim entryData As String = ""
                Dim subData As String = ""
                DT = DB.ExecuteDataSet("SELECT Cash_GL,PCB_Before,PCB_After,Branch_Remarks,Branch_Contact,ShortOrOverBit,GL_Before,GL_After,Terminal_Bal,Admin_Bal,Atm_Amount,Atm_Status FROM CBR_ENTRY_MASTER WHERE Entry_ID = " + entryID.ToString()).Tables(0)
                For i As Integer = 0 To DT.Rows.Count - 1
                    For j As Integer = 0 To DT.Columns.Count - 1
                        entryData += DT.Rows(i)(j).ToString() + "ÿ"
                    Next
                    entryData += "Ñ"
                Next
                'DT1 = DB.ExecuteDataSet("SELECT OB_Count,OB_Total,CD_Count,CD_Total,CPB_Count,CPB_Total,CBR_Count,CBR_Total,CR_Count,CR_Total,PAR_Count,PAR_Total,ShortageOrOverage_Count,ShortageOrOverage_Total,SB_Before_Count,SB_Before_Total,SB_After_Count,SB_After_Total,EJ_Before,EJ_After FROM CBR_ENTRY_SUB WHERE Master_ID = " + entryID.ToString()).Tables(0)
                DT1 = DB.ExecuteDataSet("SELECT OB_Count,OB_Total,CD_Count,CD_Total,CPB_Count,CPB_Total,CBR_Count,CBR_Total,CR_Count,CR_Total,PAR_Count,PAR_Total,ShortageOrOverage_Count,ShortageOrOverage_Total,SB_Before_Count,SB_Before_Total,SB_After_Count,SB_After_Total FROM CBR_ENTRY_SUB WHERE Master_ID = " + entryID.ToString()).Tables(0)
                For k As Integer = 0 To DT1.Rows.Count - 1
                    For l As Integer = 0 To DT1.Columns.Count - 1
                        subData += DT1.Rows(k)(l).ToString() + "ÿ"
                    Next
                    subData += "Ñ"
                Next
                CallBackReturn += entryData.ToString() + "Ø" + subData.ToString()
            End If
            If CInt(Data(0)) = 4 Then
                Dim Mode As Integer = CInt(Data(1).ToString())
                Dim MasterID As Integer = CInt(Data(2).ToString())
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Entry_ID As Integer = 0
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@Mode", SqlDbType.Int)
                Params(0).Value = Mode
                Params(1) = New SqlParameter("@MasterID", SqlDbType.Int)
                Params(1).Value = MasterID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@Entry_ID", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CBR_ENTRY_SAVE", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
                Entry_ID = CInt(Params(4).Value)
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            End If
        Catch ex As Exception
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try

    End Sub

    'Protected Sub btnUpload_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles .Click
    '    Try
    '        Dim ErrorFlag As Integer = 0
    '        Dim Message As String = Nothing
    '        Dim UserID As Integer = CInt(Session("UserID"))
    '        Dim Master_ID = CInt(Me..Value)
    '        Dim PkID As Integer = CInt(Me.hdnPkID.Value)
    '        Dim ContentType As String = ""
    '        Dim AttachImg As Byte() = Nothing
    '        Dim FileName As String = ""
    '        Dim myFile As HttpPostedFile = fupAttDraft.PostedFile
    '        Dim nFileLen As Integer = myFile.ContentLength
    '        If (nFileLen > 0) Then
    '            ContentType = myFile.ContentType
    '            FileName = myFile.FileName
    '            AttachImg = New Byte(nFileLen - 1) {}
    '            myFile.InputStream.Read(AttachImg, 0, nFileLen)
    '            Dim fileExtension As String = Path.GetExtension(myFile.FileName)
    '            If Not (fileExtension = ".xls" Or fileExtension = ".xlsx" Or fileExtension = ".jpg" Or fileExtension = ".jpeg" Or fileExtension = ".doc" Or fileExtension = ".docx" Or fileExtension = ".zip" Or fileExtension = ".pdf" Or fileExtension = ".PDF" Or fileExtension = ".XLS" Or fileExtension = ".XLSX" Or fileExtension = ".JPG" Or fileExtension = ".JPEG" Or fileExtension = ".DOC" Or fileExtension = ".DOCX" Or fileExtension = ".ZIP") Then
    '                Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '                cl_srpt1.Append("alert('This Attached file is Not Allowed Here');")
    '                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
    '                Exit Sub
    '            End If
    '        End If

    '        Try
    '            If RequestID > 0 Then
    '                Dim Param(6) As SqlParameter
    '                Param(0) = New SqlParameter("@PkID", SqlDbType.Int)
    '                Param(0).Value = PkID
    '                Param(1) = New SqlParameter("@RequestID", SqlDbType.Int)
    '                Param(1).Value = RequestID
    '                Param(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
    '                Param(2).Value = AttachImg
    '                Param(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
    '                Param(3).Value = ContentType
    '                Param(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
    '                Param(4).Value = FileName
    '                Param(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '                Param(5).Direction = ParameterDirection.Output
    '                Param(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '                Param(6).Direction = ParameterDirection.Output
    '                DB.ExecuteNonQuery("SP_VET_REDRAFT_COMMENTS", Param)
    '                ErrorFlag = CInt(Param(5).Value)
    '                Message = CStr(Param(6).Value)
    '            End If
    '        Catch ex As Exception
    '            Message = ex.Message.ToString
    '            ErrorFlag = 1
    '        End Try
    '        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '        cl_script1.Append("alert('" + Message + "');")
    '        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If
    '    End Try
    'End Sub




    'Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Upload.Click
    '    Dim ContentType As String = ""
    '    Dim AttachImg As Byte() = Nothing
    '    Dim myFile As HttpPostedFile
    '    Dim FileLen As Integer = 0
    '    Dim FileName As String = ""
    '    If fup1.Visible = True Then
    '        myFile = fup1.PostedFile
    '        FileLen = myFile.ContentLength
    '        If (FileLen > 0) Then
    '            ContentType = myFile.ContentType
    '            FileName = myFile.FileName
    '            AttachImg = New Byte(FileLen - 1) {}
    '            myFile.InputStream.Read(AttachImg, 0, FileLen)
    '        End If
    '    End If
    '    Dim ErrorFlag As Integer = 0
    '    Dim Message As String = Nothing
    '    'Dim BO_Master_Id As Integer = 11
    '    Try
    '        Dim Params(10) As SqlParameter
    '        Params(0) = New SqlParameter("@AttachImage", SqlDbType.Int)
    '        Params(0).Value = AttachImage
    '        Params(1) = New SqlParameter("@CaptionName", SqlDbType.VarChar, 100)
    '        Params(1).Value = "Form 6"
    '        Params(2) = New SqlParameter("@AttachImg", SqlDbType.VarBinary)
    '        Params(2).Value = AttachImg
    '        Params(3) = New SqlParameter("@ContentType", SqlDbType.VarChar, 500)
    '        Params(3).Value = ContentType
    '        Params(4) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
    '        Params(4).Value = FileName
    '        Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
    '        Params(5).Value = CInt(Session("UserID"))
    '        Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '        Params(6).Direction = ParameterDirection.Output
    '        Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '        Params(7).Direction = ParameterDirection.Output
    '        Params(8) = New SqlParameter("@BO_Master_Id", SqlDbType.Int)
    '        Params(8).Value = BO_Master_Id
    '        Params(9) = New SqlParameter("@BO_Section_Id", SqlDbType.Int)
    '        Params(9).Value = 1
    '        Params(10) = New SqlParameter("@BO_Premise_Id", SqlDbType.Int)
    '        Params(10).Value = 0
    '        DB.ExecuteNonQuery("SP_BO_BUSINESS_ATTACH", Params)
    '        Message = CStr(Params(7).Value)
    '        ErrorFlag = CInt(Params(6).Value)
    '    Catch ex As Exception
    '        Message = ex.Message.ToString
    '        ErrorFlag = 1
    '    End Try
    '    Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '    cl_script1.Append("         alert('" + Message + "');")
    '    cl_script1.Append("         window.open('BO_BusinessProcedures.aspx','_self');")
    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    'End Sub
End Class
