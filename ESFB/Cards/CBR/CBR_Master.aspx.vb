﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class CBR_Master
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim HD As New HelpDesk
    Dim GF As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1287) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("select b.branch_id,b.branch_name from emp_master a inner join branch_master b on a.branch_id = b.branch_id where a.emp_code = '" + UserID + "'").Tables(0)
            Me.Master.subtitle = "CBR MASTER ENTRY"

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Try
            Dim Data() As String = eventArgument.Split(CChar("Ø"))
            If CInt(Data(0)) = 1 Then
                'Dim editdata = Me.EditData.Value.ToString().Split(CChar("Ø"))
                'DT = DB.ExecuteDataSet("Select master_id from cbr_atm_master where atm_id = '" + editdata(0) + "' and branch_id = '" + editdata(1) + "' and location = '" + editdata(2) + "'").Tables(0)

                Dim Master_ID As Integer = CInt(Data(5).ToString())
                Dim Mode As Integer = CInt(Data(1).ToString())
                Dim ATM_ID As String = Data(2)
                Dim Location As String = Data(3)
                Dim BranchID As Integer = CInt(Data(4).ToString())
                Dim UserID As Integer = CInt(Session("UserID"))
                Dim Message As String = Nothing
                Dim ErrorFlag As Integer = 0
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@Mode", SqlDbType.Int)
                Params(0).Value = Mode
                Params(1) = New SqlParameter("@ATM_ID", SqlDbType.VarChar, 10)
                Params(1).Value = ATM_ID
                Params(2) = New SqlParameter("@Location", SqlDbType.VarChar, 100)
                Params(2).Value = Location
                Params(3) = New SqlParameter("@Branch_ID", SqlDbType.Int)
                Params(3).Value = BranchID
                Params(4) = New SqlParameter("@Created_By", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@Master_ID", SqlDbType.Int)
                Params(7).Value = Master_ID
                DB.ExecuteNonQuery("SP_CBR_MASTER", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
                CallBackReturn = ErrorFlag.ToString() + "Ø" + Message.ToString()
            End If
            If CInt(Data(0)) = 2 Then
                Dim BCode = CStr(Data(1))
                DT = DB.ExecuteDataSet("SELECT BRANCH_NAME FROM BRANCH_MASTER WHERE BRANCH_ID = " + BCode).Tables(0)
                DT1 = DB.ExecuteDataSet("SELECT a.Master_ID,a.ATM_ID,a.Location FROM CBR_ATM_MASTER a INNER JOIN BRANCH_MASTER b ON b.Branch_ID = a.Branch_Id WHERE a.Status = 1 and a.Branch_ID = " + BCode).Tables(0)
                Dim MasterData As String = ""
                Dim i As Integer = 0
                For Each dtr As DataRow In DT1.Rows
                    For k As Integer = 0 To DT1.Columns.Count - 1
                        MasterData += DT1.Rows(i)(k).ToString() + "ÿ"
                    Next
                    i += 1
                    MasterData += "Ñ"
                Next
                'Me.hidData.Value = MasterData.ToString()
                CallBackReturn = "2~" + DT.Rows(0)(0).ToString() + "~" + MasterData.ToString()
            End If
            If CInt(Data(0)) = 3 Then
                Dim BCode = CStr(Data(2))
                Dim ATMID = CStr(Data(1))
                DT = DB.ExecuteDataSet("SELECT Master_ID FROM CBR_ATM_MASTER WHERE BRANCH_ID = " + BCode + " AND ATM_ID = '" + ATMID + "' and status = 1").Tables(0)
                Me.hidMasterID.Value = DT.Rows(0)(0).ToString()
                CallBackReturn = "3~" + DT.Rows(0)(0).ToString()
            End If
        Catch ex As Exception
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try

    End Sub
End Class
