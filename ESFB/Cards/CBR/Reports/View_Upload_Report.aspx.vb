﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Data.SqlClient
Partial Class View_Upload_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DS As New DataTable
    Dim DTExcel As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date
    Dim DTDATE As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Branch As String = CStr(Request.QueryString.Get("Branch"))
            Dim ATM As String = CStr(Request.QueryString.Get("ATM"))
            Dim From_Dt As String = CStr(Request.QueryString.Get("From"))
            Dim To_Dt As String = CStr(Request.QueryString.Get("To"))
            Dim Status As String = CStr(Request.QueryString.Get("Status"))
            Dim StrWhere As String = "1=1 "
            Dim StrDate As String = ""
            If Branch <> -2 Then
                StrWhere += "and a.Branch_code = " + Branch
            End If
            If ATM <> -1 Then
                StrWhere += " and a.atm_ID = " + ATM
            End If
            Dim f_dt As String = CDate(From_Dt).ToString("yyyy-MM-dd")
            Dim t_dt As String = CDate(To_Dt).ToString("yyyy-MM-dd")
            If f_dt <> "" And t_dt <> "" Then
                If f_dt <> t_dt Then
                    StrDate += " x.Created_Date between '" + f_dt + "' and '" + t_dt + "'"
                Else
                    StrDate += " CAST(x.Created_Date AS date) = '" + f_dt + "'"
                End If
            End If


            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            
            RH.Heading(Session("FirmName"), tb, "ESAF", 100)
            tb.Attributes.Add("width", "100%")
            RH.BlankRow(tb, 3)


            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16 As New TableCell
            
            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"
            TRHead_16.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver
            TRHead_16.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid
            TRHead_16.BorderStyle = BorderStyle.Solid

            TRHead_00.RowSpan = 2
            TRHead_01.RowSpan = 2
            TRHead_02.RowSpan = 2
            TRHead_03.RowSpan = 2
            TRHead_04.RowSpan = 2
            TRHead_05.RowSpan = 2
            TRHead_06.RowSpan = 2
            TRHead_07.RowSpan = 2
            TRHead_08.RowSpan = 2
            TRHead_09.RowSpan = 2


            RH.AddColumn(TRHead, TRHead_00, 2, 2, "c", "SL NO")
            RH.AddColumn(TRHead, TRHead_01, 5, 5, "c", "DATE")
            RH.AddColumn(TRHead, TRHead_02, 4, 4, "c", "ATM ID")
            RH.AddColumn(TRHead, TRHead_03, 5, 5, "c", "GL Before")
            RH.AddColumn(TRHead, TRHead_04, 5, 5, "c", "GL After")
            RH.AddColumn(TRHead, TRHead_05, 5, 5, "c", "Terminal Balance")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "c", "Admin Balance")
            RH.AddColumn(TRHead, TRHead_07, 5, 5, "c", "ATM Status")
            RH.AddColumn(TRHead, TRHead_08, 5, 5, "c", "ATM Amount")
            RH.AddColumn(TRHead, TRHead_09, 3, 3, "c", "DENOMINATION")
            RH.AddColumn(TRHead, TRHead_10, 8, 8, "c", "OPENING BALANCE")
            RH.AddColumn(TRHead, TRHead_11, 8, 8, "c", "CASH DISPENSE")
            RH.AddColumn(TRHead, TRHead_12, 8, 8, "c", "CASH IN PURGE BIN")
            RH.AddColumn(TRHead, TRHead_13, 8, 8, "c", "CLOSING BEFORE REPLISHMENT")
            RH.AddColumn(TRHead, TRHead_14, 8, 8, "c", "CASH REPLISHMENT")
            RH.AddColumn(TRHead, TRHead_15, 8, 8, "c", "POSITION AFTER REPLISHMENT")
            RH.AddColumn(TRHead, TRHead_16, 8, 8, "c", "OVERAGE / SHORTAGE")
            'RH.AddColumn(TRHead, TRHead_11, 5, 5, "c", "GL Before")
            'RH.AddColumn(TRHead, TRHead_12, 5, 5, "c", "GL After")
            'RH.AddColumn(TRHead, TRHead_13, 5, 5, "c", "Terminal Balance")
            'RH.AddColumn(TRHead, TRHead_14, 5, 5, "c", "Admin Balance")
            'RH.AddColumn(TRHead, TRHead_15, 5, 5, "c", "ATM Status")
            'RH.AddColumn(TRHead, TRHead_16, 5, 5, "c", "ATM Amount")
            tb.Controls.Add(TRHead)

            Dim TRHead1 As New TableRow
            TRHead1.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead1_10, TRHead1_101, TRHead1_11, TRHead1_111, TRHead1_12, TRHead1_121, TRHead1_13, TRHead1_131, TRHead1_14, TRHead1_141, TRHead1_15, TRHead1_151, TRHead1_16, TRHead1_161 As New TableCell

            TRHead1_10.BorderWidth = "1"
            TRHead1_101.BorderWidth = "1"
            TRHead1_11.BorderWidth = "1"
            TRHead1_111.BorderWidth = "1"
            TRHead1_12.BorderWidth = "1"
            TRHead1_121.BorderWidth = "1"
            TRHead1_13.BorderWidth = "1"
            TRHead1_131.BorderWidth = "1"
            TRHead1_14.BorderWidth = "1"
            TRHead1_141.BorderWidth = "1"
            TRHead1_15.BorderWidth = "1"
            TRHead1_151.BorderWidth = "1"
            TRHead1_16.BorderWidth = "1"
            TRHead1_161.BorderWidth = "1"

            TRHead1_10.BorderColor = Drawing.Color.Silver
            TRHead1_101.BorderColor = Drawing.Color.Silver
            TRHead1_11.BorderColor = Drawing.Color.Silver
            TRHead1_111.BorderColor = Drawing.Color.Silver
            TRHead1_12.BorderColor = Drawing.Color.Silver
            TRHead1_121.BorderColor = Drawing.Color.Silver
            TRHead1_13.BorderColor = Drawing.Color.Silver
            TRHead1_131.BorderColor = Drawing.Color.Silver
            TRHead1_14.BorderColor = Drawing.Color.Silver
            TRHead1_141.BorderColor = Drawing.Color.Silver
            TRHead1_15.BorderColor = Drawing.Color.Silver
            TRHead1_151.BorderColor = Drawing.Color.Silver
            TRHead1_16.BorderColor = Drawing.Color.Silver
            TRHead1_161.BorderColor = Drawing.Color.Silver

            TRHead1_10.BorderStyle = BorderStyle.Solid
            TRHead1_101.BorderStyle = BorderStyle.Solid
            TRHead1_11.BorderStyle = BorderStyle.Solid
            TRHead1_111.BorderStyle = BorderStyle.Solid
            TRHead1_12.BorderStyle = BorderStyle.Solid
            TRHead1_121.BorderStyle = BorderStyle.Solid
            TRHead1_13.BorderStyle = BorderStyle.Solid
            TRHead1_131.BorderStyle = BorderStyle.Solid
            TRHead1_14.BorderStyle = BorderStyle.Solid
            TRHead1_141.BorderStyle = BorderStyle.Solid
            TRHead1_15.BorderStyle = BorderStyle.Solid
            TRHead1_151.BorderStyle = BorderStyle.Solid
            TRHead1_16.BorderStyle = BorderStyle.Solid
            TRHead1_161.BorderStyle = BorderStyle.Solid


            RH.AddColumn(TRHead1, TRHead1_10, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_101, 4, 4, "c", "TOTAL")
            RH.AddColumn(TRHead1, TRHead1_11, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_111, 4, 4, "c", "TOTAL")
            RH.AddColumn(TRHead1, TRHead1_12, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_121, 4, 4, "c", "TOTAL")
            RH.AddColumn(TRHead1, TRHead1_13, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_131, 4, 4, "c", "TOTAL")
            RH.AddColumn(TRHead1, TRHead1_14, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_141, 4, 4, "c", "TOTAL")
            RH.AddColumn(TRHead1, TRHead1_15, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_151, 4, 4, "c", "TOTAL")
            RH.AddColumn(TRHead1, TRHead1_16, 4, 4, "c", "COUNT")
            RH.AddColumn(TRHead1, TRHead1_161, 4, 4, "c", "TOTAL")
            tb.Controls.Add(TRHead1)

            RH.BlankRow(tb, 3)
            If StrDate <> "" Then
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x where " + StrDate).Tables(0)
            Else
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x").Tables(0)
            End If
            For n As Integer = 0 To DTDATE.Rows.Count - 1
                'Dim createdDate As String = DTDATE(n)(0).ToString()
                Dim createdDate As String = CDate(DTDATE(n)(0).ToString()).ToString("yyyy-MM-dd")
                DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id,a.ShortOrOverBit,a.GL_Before,a.GL_After,a.Terminal_Bal,a.Admin_Bal,a.Atm_Status,a.Atm_Amount from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and convert(date,a.created_date) = '" + createdDate + "'").Tables(0)
                Dim j As Integer = 1
                For m As Integer = 0 To DT2.Rows.Count - 1
                    Dim TR3 As New TableRow
                    TR3.BorderWidth = "1"
                    TR3.BorderStyle = BorderStyle.Solid

                    Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_041, TR3_05, TR3_051, TR3_06, TR3_061, TR3_07, TR3_071, TR3_08, TR3_081, TR3_09, TR3_091, TR3_10, TR3_101, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16 As New TableCell
                    TR3_00.BorderWidth = "1"
                    TR3_01.BorderWidth = "1"
                    TR3_02.BorderWidth = "1"
                    TR3_03.BorderWidth = "1"
                    TR3_04.BorderWidth = "1"
                    TR3_041.BorderWidth = "1"
                    TR3_05.BorderWidth = "1"
                    TR3_051.BorderWidth = "1"
                    TR3_06.BorderWidth = "1"
                    TR3_061.BorderWidth = "1"
                    TR3_07.BorderWidth = "1"
                    TR3_071.BorderWidth = "1"
                    TR3_08.BorderWidth = "1"
                    TR3_081.BorderWidth = "1"
                    TR3_09.BorderWidth = "1"
                    TR3_091.BorderWidth = "1"
                    TR3_10.BorderWidth = "1"
                    TR3_101.BorderWidth = "1"
                    TR3_11.BorderWidth = "1"
                    TR3_12.BorderWidth = "1"
                    TR3_13.BorderWidth = "1"
                    TR3_14.BorderWidth = "1"
                    TR3_15.BorderWidth = "1"
                    TR3_16.BorderWidth = "1"

                    TR3_00.BorderColor = Drawing.Color.Silver
                    TR3_01.BorderColor = Drawing.Color.Silver
                    TR3_02.BorderColor = Drawing.Color.Silver
                    TR3_03.BorderColor = Drawing.Color.Silver
                    TR3_04.BorderColor = Drawing.Color.Silver
                    TR3_041.BorderColor = Drawing.Color.Silver
                    TR3_05.BorderColor = Drawing.Color.Silver
                    TR3_051.BorderColor = Drawing.Color.Silver
                    TR3_06.BorderColor = Drawing.Color.Silver
                    TR3_061.BorderColor = Drawing.Color.Silver
                    TR3_07.BorderColor = Drawing.Color.Silver
                    TR3_071.BorderColor = Drawing.Color.Silver
                    TR3_08.BorderColor = Drawing.Color.Silver
                    TR3_081.BorderColor = Drawing.Color.Silver
                    TR3_09.BorderColor = Drawing.Color.Silver
                    TR3_091.BorderColor = Drawing.Color.Silver
                    TR3_10.BorderColor = Drawing.Color.Silver
                    TR3_101.BorderColor = Drawing.Color.Silver
                    TR3_11.BorderColor = Drawing.Color.Silver
                    TR3_12.BorderColor = Drawing.Color.Silver
                    TR3_13.BorderColor = Drawing.Color.Silver
                    TR3_14.BorderColor = Drawing.Color.Silver
                    TR3_15.BorderColor = Drawing.Color.Silver
                    TR3_16.BorderColor = Drawing.Color.Silver

                    TR3_00.BorderStyle = BorderStyle.Solid
                    TR3_01.BorderStyle = BorderStyle.Solid
                    TR3_02.BorderStyle = BorderStyle.Solid
                    TR3_03.BorderStyle = BorderStyle.Solid
                    TR3_04.BorderStyle = BorderStyle.Solid
                    TR3_041.BorderStyle = BorderStyle.Solid
                    TR3_05.BorderStyle = BorderStyle.Solid
                    TR3_051.BorderStyle = BorderStyle.Solid
                    TR3_06.BorderStyle = BorderStyle.Solid
                    TR3_061.BorderStyle = BorderStyle.Solid
                    TR3_07.BorderStyle = BorderStyle.Solid
                    TR3_071.BorderStyle = BorderStyle.Solid
                    TR3_08.BorderStyle = BorderStyle.Solid
                    TR3_081.BorderStyle = BorderStyle.Solid
                    TR3_09.BorderStyle = BorderStyle.Solid
                    TR3_091.BorderStyle = BorderStyle.Solid
                    TR3_10.BorderStyle = BorderStyle.Solid
                    TR3_101.BorderStyle = BorderStyle.Solid
                    TR3_11.BorderStyle = BorderStyle.Solid
                    TR3_12.BorderStyle = BorderStyle.Solid
                    TR3_13.BorderStyle = BorderStyle.Solid
                    TR3_14.BorderStyle = BorderStyle.Solid
                    TR3_15.BorderStyle = BorderStyle.Solid
                    TR3_16.BorderStyle = BorderStyle.Solid

                    TR3_00.RowSpan = 3
                    TR3_01.RowSpan = 3
                    TR3_02.RowSpan = 3
                    TR3_03.RowSpan = 3
                    TR3_04.RowSpan = 3
                    TR3_05.RowSpan = 3
                    TR3_06.RowSpan = 3
                    TR3_07.RowSpan = 3
                    TR3_08.RowSpan = 3



                    RH.AddColumn(TR3, TR3_00, 2, 2, "c", j)
                    RH.AddColumn(TR3, TR3_01, 5, 5, "c", DT2(m)(0).ToString())
                    RH.AddColumn(TR3, TR3_02, 4, 4, "c", DT2(m)(1).ToString())


                    RH.AddColumn(TR3, TR3_03, 5, 5, "c", DT2(m)(4).ToString())
                    RH.AddColumn(TR3, TR3_04, 5, 5, "c", DT2(m)(5).ToString())
                    RH.AddColumn(TR3, TR3_05, 5, 5, "c", DT2(m)(6).ToString())
                    RH.AddColumn(TR3, TR3_06, 5, 5, "c", DT2(m)(7).ToString())
                    RH.AddColumn(TR3, TR3_07, 5, 5, "c", DT2(m)(8).ToString())
                    RH.AddColumn(TR3, TR3_08, 5, 5, "c", DT2(m)(9).ToString())



                    tb.Controls.Add(TR3)
                    DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)
                    For Each DR In DT1.Rows
                        Dim TR31 As New TableRow
                        TR31.BorderWidth = "1"
                        TR31.BorderStyle = BorderStyle.Solid

                        Dim TR31_00, TR31_01, TR31_02, TR31_03, TR31_04, TR31_05, TR31_06, TR31_07, TR31_08, TR31_09, TR31_091, TR31_10, TR31_101, TR31_11, TR31_111, TR31_12, TR31_121, TR31_13, TR31_131, TR31_14, TR31_141, TR31_15, TR31_151, TR31_16, TR31_161 As New TableCell
                        TR31_00.BorderWidth = "1"
                        TR31_01.BorderWidth = "1"
                        TR31_02.BorderWidth = "1"
                        TR31_03.BorderWidth = "1"
                        TR31_04.BorderWidth = "1"
                        TR31_05.BorderWidth = "1"
                        TR31_06.BorderWidth = "1"
                        TR31_07.BorderWidth = "1"
                        TR31_08.BorderWidth = "1"
                        TR31_09.BorderWidth = "1"
                        TR31_091.BorderWidth = "1"
                        TR31_10.BorderWidth = "1"
                        TR31_101.BorderWidth = "1"
                        TR31_11.BorderWidth = "1"
                        TR31_111.BorderWidth = "1"
                        TR31_12.BorderWidth = "1"
                        TR31_121.BorderWidth = "1"
                        TR31_13.BorderWidth = "1"
                        TR31_131.BorderWidth = "1"
                        TR31_14.BorderWidth = "1"
                        TR31_141.BorderWidth = "1"
                        TR31_15.BorderWidth = "1"
                        TR31_151.BorderWidth = "1"
                        TR31_16.BorderWidth = "1"
                        TR31_161.BorderWidth = "1"

                        TR31_00.BorderColor = Drawing.Color.Silver
                        TR31_01.BorderColor = Drawing.Color.Silver
                        TR31_02.BorderColor = Drawing.Color.Silver
                        TR31_03.BorderColor = Drawing.Color.Silver
                        TR31_04.BorderColor = Drawing.Color.Silver
                        TR31_05.BorderColor = Drawing.Color.Silver
                        TR31_06.BorderColor = Drawing.Color.Silver
                        TR31_07.BorderColor = Drawing.Color.Silver
                        TR31_08.BorderColor = Drawing.Color.Silver
                        TR31_09.BorderColor = Drawing.Color.Silver
                        TR31_091.BorderColor = Drawing.Color.Silver
                        TR31_10.BorderColor = Drawing.Color.Silver
                        TR31_101.BorderColor = Drawing.Color.Silver
                        TR31_11.BorderColor = Drawing.Color.Silver
                        TR31_111.BorderColor = Drawing.Color.Silver
                        TR31_12.BorderColor = Drawing.Color.Silver
                        TR31_121.BorderColor = Drawing.Color.Silver
                        TR31_13.BorderColor = Drawing.Color.Silver
                        TR31_131.BorderColor = Drawing.Color.Silver
                        TR31_14.BorderColor = Drawing.Color.Silver
                        TR31_141.BorderColor = Drawing.Color.Silver
                        TR31_15.BorderColor = Drawing.Color.Silver
                        TR31_151.BorderColor = Drawing.Color.Silver
                        TR31_16.BorderColor = Drawing.Color.Silver
                        TR31_161.BorderColor = Drawing.Color.Silver

                        TR31_00.BorderStyle = BorderStyle.Solid
                        TR31_01.BorderStyle = BorderStyle.Solid
                        TR31_02.BorderStyle = BorderStyle.Solid
                        TR31_03.BorderStyle = BorderStyle.Solid
                        TR31_04.BorderStyle = BorderStyle.Solid
                        TR31_05.BorderStyle = BorderStyle.Solid
                        TR31_06.BorderStyle = BorderStyle.Solid
                        TR31_07.BorderStyle = BorderStyle.Solid
                        TR31_08.BorderStyle = BorderStyle.Solid
                        TR31_09.BorderStyle = BorderStyle.Solid
                        TR31_091.BorderStyle = BorderStyle.Solid
                        TR31_10.BorderStyle = BorderStyle.Solid
                        TR31_101.BorderStyle = BorderStyle.Solid
                        TR31_11.BorderStyle = BorderStyle.Solid
                        TR31_111.BorderStyle = BorderStyle.Solid
                        TR31_12.BorderStyle = BorderStyle.Solid
                        TR31_121.BorderStyle = BorderStyle.Solid
                        TR31_13.BorderStyle = BorderStyle.Solid
                        TR31_131.BorderStyle = BorderStyle.Solid
                        TR31_14.BorderStyle = BorderStyle.Solid
                        TR31_141.BorderStyle = BorderStyle.Solid
                        TR31_15.BorderStyle = BorderStyle.Solid
                        TR31_151.BorderStyle = BorderStyle.Solid
                        TR31_16.BorderStyle = BorderStyle.Solid
                        TR31_161.BorderStyle = BorderStyle.Solid

                        'TR3_00.RowSpan = 3
                        'TR3_01.RowSpan = 3
                        'TR3_02.RowSpan = 3

                        'RH.AddColumn(TR3, TR3_00, 3, 3, "c", j)
                        'RH.AddColumn(TR3, TR3_01, 7, 7, "c", DT2(m)(0).ToString())
                        'RH.AddColumn(TR3, TR3_02, 6, 6, "c", DT2(m)(1).ToString())
                        RH.AddColumn(TR31, TR31_09, 3, 3, "c", DR(0).ToString())
                        RH.AddColumn(TR31, TR31_10, 4, 4, "c", DR(1).ToString())
                        RH.AddColumn(TR31, TR31_101, 4, 4, "c", DR(2).ToString())
                        RH.AddColumn(TR31, TR31_11, 4, 4, "c", DR(3).ToString())
                        RH.AddColumn(TR31, TR31_111, 4, 4, "c", DR(4).ToString())
                        RH.AddColumn(TR31, TR31_12, 4, 4, "c", DR(5).ToString())
                        RH.AddColumn(TR31, TR31_121, 4, 4, "c", DR(6).ToString())
                        RH.AddColumn(TR31, TR31_13, 4, 4, "c", DR(7).ToString())
                        RH.AddColumn(TR31, TR31_131, 4, 4, "c", DR(8).ToString())
                        RH.AddColumn(TR31, TR31_14, 4, 4, "c", DR(9).ToString())
                        RH.AddColumn(TR31, TR31_141, 4, 4, "c", DR(10).ToString())
                        RH.AddColumn(TR31, TR31_15, 4, 4, "c", DR(11).ToString())
                        RH.AddColumn(TR31, TR31_151, 4, 4, "c", DR(12).ToString())
                        If DT2(m)(3).ToString() = "1" Then
                            RH.AddColumn(TR31, TR31_16, 4, 4, "c", "-" + DR(13).ToString())
                        ElseIf DT2(m)(3).ToString() = "2" Then
                            RH.AddColumn(TR31, TR31_16, 4, 4, "c", "+" + DR(13).ToString())
                        ElseIf DT2(m)(3).ToString() = "3" Then
                            RH.AddColumn(TR31, TR31_16, 4, 4, "c", DR(13).ToString())
                        End If
                        If DT2(m)(3).ToString() = "1" Then
                            RH.AddColumn(TR31, TR31_161, 4, 4, "c", "-" + DR(14).ToString())
                        ElseIf DT2(m)(3).ToString() = "2" Then
                            RH.AddColumn(TR31, TR31_161, 4, 4, "c", "+" + DR(14).ToString())
                        ElseIf DT2(m)(3).ToString() = "3" Then
                            RH.AddColumn(TR31, TR31_161, 4, 4, "c", DR(14).ToString())
                        End If




                        tb.Controls.Add(TR31)
                    Next
                    'RH.AddColumn(TR3, TR3_11, 5, 5, "c", DT2(m)(4).ToString())
                    'RH.AddColumn(TR3, TR3_12, 5, 5, "c", DT2(m)(5).ToString())
                    'RH.AddColumn(TR3, TR3_13, 5, 5, "c", DT2(m)(6).ToString())
                    'RH.AddColumn(TR3, TR3_14, 5, 5, "c", DT2(m)(7).ToString())
                    'RH.AddColumn(TR3, TR3_15, 5, 5, "c", DT2(m)(8).ToString())
                    'RH.AddColumn(TR3, TR3_16, 5, 5, "c", DT2(m)(9).ToString())



                    'tb.Controls.Add(TR3)
                    j += 1
                Next

            Next

            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

End Class
