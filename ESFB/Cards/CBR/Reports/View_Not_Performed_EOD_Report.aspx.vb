﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_Not_Performed_EOD_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim ViewType As Integer
            Dim FromDate, ToDate As Date
            Dim TDay As Date = CDate(Session("TraDt"))
            FromDate = CDate(Request.QueryString.Get("FDT"))
            Dim DateFrom As String
            DateFrom = FromDate.ToString("yyyy-MM-dd")
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            ToDate = CDate(Request.QueryString.Get("TDT"))
            'If ToDate = TDay Then
            '    ToDate = DateAdd(DateInterval.Day, -1, ToDate)
            'End If
            Dim DateTo As String
            DateTo = ToDate.ToString("yyyy-MM-dd")
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            RH.Heading(Session("FirmName"), tb, "Not Performed EOD Branches Report", 100)
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 10, 10, "c", "SI No")
            RH.AddColumn(TRHead, TRHead_01, 30, 30, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_02, 30, 30, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_03, 30, 30, "c", "Date")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer


            DT = DB.ExecuteDataSet("select a.branch_id,c.branch_name,convert(date,dates) from (select distinct branch_id, dates from cbr_atm_master , (SELECT DATEADD(dd,[number],'" + DateFrom.ToString() + "') dates  FROM Master.dbo.spt_values  WHERE [type] = 'P' AND [number] >= 0 AND [number] < 367 AND DATEADD(dd,[number],'" + DateFrom.ToString() + "') <= '" + DateTo.ToString() + "' ) b ) a left join cbr_entry_master b on a.branch_id = b.branch_code and convert(date,a.dates) = convert(date, b.created_date)  left join branch_master c on c.branch_id = a.branch_id  where (b.created_date Is null)  order by dates ").Tables(0)


            DTEXCEL = DB.ExecuteDataSet("select a.branch_id as [Branch ID],c.branch_name as [Branch Name],convert(date,dates) as [Date] from ( select distinct branch_id, dates from cbr_atm_master , (SELECT DATEADD(dd,[number],'" + DateFrom.ToString() + "') dates  FROM Master.dbo.spt_values  WHERE [type] = 'P' AND [number] >= 0 AND [number] < 367  AND DATEADD(dd,[number],'" + DateFrom.ToString() + "') <= '" + DateTo.ToString() + "' ) b ) a left join cbr_entry_master b on a.branch_id = b.branch_code and convert(date,a.dates) = convert(date, b.created_date)  left join branch_master c on c.branch_id = a.branch_id  where (b.created_date Is null)  order by dates ").Tables(0)

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid

                If createddate <> DR(2).ToString() Then
                    RH.BlankRow(tb, 3)
                    RH.SubHeading(tb, 100, "l", CDate(DR(2).ToString()))
                    tb.Attributes.Add("width", "100%")
                End If

                createddate = DR(2).ToString()

                RH.AddColumn(TR3, TR3_00, 10, 10, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 30, 30, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 30, 30, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 30, 30, "l", CDate(DR(2).ToString()))

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Not Performed EOD Branches Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            HeaderText = "Not_Performed_EOD_Branch_Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
