﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Data.SqlClient
Imports ClosedXML.Excel
Imports DocumentFormat.OpenXml

Partial Class View_Entry_Detail_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DTDATE As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim ViewType As Integer
            Dim BranchCode As String
            Dim ATM As String
            Dim From_Dt As String
            Dim To_Dt As String
            Dim Branch As String
            ViewType = CInt(Request.QueryString.Get("ViewType"))
            Branch = Request.QueryString.Get("Branch")
            hdnBranch.Value = Branch

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            If ViewType = -1 Then
                BranchCode = CStr(Request.QueryString.Get("BranchCode"))
                ATM = CStr(Request.QueryString.Get("ATM"))
                From_Dt = Request.QueryString.Get("From_Date")
                To_Dt = Request.QueryString.Get("To_Date")

                Dim StrWhere As String = " 1=1"
                If BranchCode <> -2 Then
                    StrWhere += " and a.Branch_code = " + BranchCode
                End If
                If ATM <> -1 Then
                    StrWhere += " and a.atm_ID = " + ATM
                End If
                Dim StrDate As String = ""
                If From_Dt <> To_Dt Then
                    StrDate += " convert(date,x.Created_Date) between convert(date,'" + From_Dt + "') and convert(date,'" + To_Dt + "')"
                Else
                    StrDate += " convert(date,x.Created_Date) = convert(date,'" + From_Dt + "')"
                End If

                cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
                cmd_Back.Attributes.Add("onclick", "return Exitform()")

                Dim RowBG As Integer = 0
                Dim DR As DataRow
                tb.Attributes.Add("width", "100%")


                RH.Heading(Session("FirmName"), tb, "Entry Report", 100)
                tb.Attributes.Add("width", "100%")
                RH.BlankRow(tb, 3)


                Dim TRHead As New TableRow
                TRHead.BackColor = System.Drawing.Color.WhiteSmoke
                Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16, TRHead_17 As New TableCell

                TRHead_00.BorderWidth = "1"
                TRHead_01.BorderWidth = "1"
                TRHead_02.BorderWidth = "1"
                TRHead_03.BorderWidth = "1"
                TRHead_04.BorderWidth = "1"
                TRHead_05.BorderWidth = "1"
                TRHead_06.BorderWidth = "1"
                TRHead_07.BorderWidth = "1"
                TRHead_08.BorderWidth = "1"
                TRHead_09.BorderWidth = "1"
                TRHead_10.BorderWidth = "1"
                TRHead_11.BorderWidth = "1"
                TRHead_12.BorderWidth = "1"
                TRHead_13.BorderWidth = "1"
                TRHead_14.BorderWidth = "1"
                TRHead_15.BorderWidth = "1"
                TRHead_16.BorderWidth = "1"
                TRHead_17.BorderWidth = "1"

                TRHead_00.BorderColor = System.Drawing.Color.Silver
                TRHead_01.BorderColor = System.Drawing.Color.Silver
                TRHead_02.BorderColor = System.Drawing.Color.Silver
                TRHead_03.BorderColor = System.Drawing.Color.Silver
                TRHead_04.BorderColor = System.Drawing.Color.Silver
                TRHead_05.BorderColor = System.Drawing.Color.Silver
                TRHead_06.BorderColor = System.Drawing.Color.Silver
                TRHead_07.BorderColor = System.Drawing.Color.Silver
                TRHead_08.BorderColor = System.Drawing.Color.Silver
                TRHead_09.BorderColor = System.Drawing.Color.Silver
                TRHead_10.BorderColor = System.Drawing.Color.Silver
                TRHead_11.BorderColor = System.Drawing.Color.Silver
                TRHead_12.BorderColor = System.Drawing.Color.Silver
                TRHead_13.BorderColor = System.Drawing.Color.Silver
                TRHead_14.BorderColor = System.Drawing.Color.Silver
                TRHead_15.BorderColor = System.Drawing.Color.Silver
                TRHead_16.BorderColor = System.Drawing.Color.Silver
                TRHead_17.BorderColor = System.Drawing.Color.Silver

                TRHead_00.BorderStyle = BorderStyle.Solid
                TRHead_01.BorderStyle = BorderStyle.Solid
                TRHead_02.BorderStyle = BorderStyle.Solid
                TRHead_03.BorderStyle = BorderStyle.Solid
                TRHead_04.BorderStyle = BorderStyle.Solid
                TRHead_05.BorderStyle = BorderStyle.Solid
                TRHead_06.BorderStyle = BorderStyle.Solid
                TRHead_07.BorderStyle = BorderStyle.Solid
                TRHead_08.BorderStyle = BorderStyle.Solid
                TRHead_09.BorderStyle = BorderStyle.Solid
                TRHead_10.BorderStyle = BorderStyle.Solid
                TRHead_11.BorderStyle = BorderStyle.Solid
                TRHead_12.BorderStyle = BorderStyle.Solid
                TRHead_13.BorderStyle = BorderStyle.Solid
                TRHead_14.BorderStyle = BorderStyle.Solid
                TRHead_15.BorderStyle = BorderStyle.Solid
                TRHead_16.BorderStyle = BorderStyle.Solid
                TRHead_17.BorderStyle = BorderStyle.Solid

                TRHead_00.RowSpan = 2
                TRHead_01.RowSpan = 2
                TRHead_02.RowSpan = 2
                TRHead_03.RowSpan = 2
                TRHead_11.RowSpan = 2
                TRHead_12.RowSpan = 2
                TRHead_13.RowSpan = 2
                TRHead_16.RowSpan = 2
                TRHead_17.RowSpan = 2

                RH.AddColumn(TRHead, TRHead_00, 2, 2, "l", "Sl.No")
                RH.AddColumn(TRHead, TRHead_01, 4, 4, "l", "Date")
                RH.AddColumn(TRHead, TRHead_02, 4, 4, "l", "ATM ID")
                RH.AddColumn(TRHead, TRHead_11, 5, 5, "l", "Terminal Balance")
                RH.AddColumn(TRHead, TRHead_12, 5, 5, "l", "Admin Balance")
                RH.AddColumn(TRHead, TRHead_13, 5, 5, "l", "Cash Loaded As Per GL")
                RH.AddColumn(TRHead, TRHead_16, 4, 4, "l", "ATM Status")
                RH.AddColumn(TRHead, TRHead_17, 4, 4, "l", "Amount")
                RH.AddColumn(TRHead, TRHead_03, 4, 4, "l", "Denomination")
                RH.AddColumn(TRHead, TRHead_04, 7, 7, "l", "Opening Balance")
                RH.AddColumn(TRHead, TRHead_05, 7, 7, "l", "Cash Dispense")
                RH.AddColumn(TRHead, TRHead_06, 7, 7, "l", "Cash In Purge Bin")
                RH.AddColumn(TRHead, TRHead_07, 7, 7, "l", "Closing Before Replenishment")
                RH.AddColumn(TRHead, TRHead_08, 7, 7, "l", "Cash Replenishment")
                RH.AddColumn(TRHead, TRHead_09, 7, 7, "l", "Position After Replenishment")
                RH.AddColumn(TRHead, TRHead_10, 7, 7, "l", "Overage / Shortage")
                RH.AddColumn(TRHead, TRHead_14, 7, 7, "l", "Physical Cash Balance")
                RH.AddColumn(TRHead, TRHead_15, 7, 7, "l", "GL Balance")

                tb.Controls.Add(TRHead)


                Dim TRHead1 As New TableRow
                Dim TRHead1_04, TRHead1_041, TRHead1_05, TRHead1_051, TRHead1_06, TRHead1_061, TRHead1_07, TRHead1_071, TRHead1_08, TRHead1_081, TRHead1_09, TRHead1_091, TRHead1_10, TRHead1_101, TRHead1_14, TRHead1_141, TRHead1_15, TRHead1_151 As New TableCell

                TRHead1_04.BorderWidth = "1"
                TRHead1_041.BorderWidth = "1"
                TRHead1_05.BorderWidth = "1"
                TRHead1_051.BorderWidth = "1"
                TRHead1_06.BorderWidth = "1"
                TRHead1_061.BorderWidth = "1"
                TRHead1_07.BorderWidth = "1"
                TRHead1_071.BorderWidth = "1"
                TRHead1_08.BorderWidth = "1"
                TRHead1_081.BorderWidth = "1"
                TRHead1_09.BorderWidth = "1"
                TRHead1_091.BorderWidth = "1"
                TRHead1_10.BorderWidth = "1"
                TRHead1_101.BorderWidth = "1"
                TRHead1_14.BorderWidth = "1"
                TRHead1_141.BorderWidth = "1"
                TRHead1_15.BorderWidth = "1"
                TRHead1_151.BorderWidth = "1"

                TRHead1_04.BorderColor = System.Drawing.Color.Silver
                TRHead1_041.BorderColor = System.Drawing.Color.Silver
                TRHead1_05.BorderColor = System.Drawing.Color.Silver
                TRHead1_051.BorderColor = System.Drawing.Color.Silver
                TRHead1_06.BorderColor = System.Drawing.Color.Silver
                TRHead1_061.BorderColor = System.Drawing.Color.Silver
                TRHead1_07.BorderColor = System.Drawing.Color.Silver
                TRHead1_071.BorderColor = System.Drawing.Color.Silver
                TRHead1_08.BorderColor = System.Drawing.Color.Silver
                TRHead1_081.BorderColor = System.Drawing.Color.Silver
                TRHead1_09.BorderColor = System.Drawing.Color.Silver
                TRHead1_091.BorderColor = System.Drawing.Color.Silver
                TRHead1_10.BorderColor = System.Drawing.Color.Silver
                TRHead1_101.BorderColor = System.Drawing.Color.Silver
                TRHead1_14.BorderColor = System.Drawing.Color.Silver
                TRHead1_141.BorderColor = System.Drawing.Color.Silver
                TRHead1_15.BorderColor = System.Drawing.Color.Silver
                TRHead1_151.BorderColor = System.Drawing.Color.Silver

                TRHead1_04.BorderStyle = BorderStyle.Solid
                TRHead1_041.BorderStyle = BorderStyle.Solid
                TRHead1_05.BorderStyle = BorderStyle.Solid
                TRHead1_051.BorderStyle = BorderStyle.Solid
                TRHead1_06.BorderStyle = BorderStyle.Solid
                TRHead1_061.BorderStyle = BorderStyle.Solid
                TRHead1_07.BorderStyle = BorderStyle.Solid
                TRHead1_071.BorderStyle = BorderStyle.Solid
                TRHead1_08.BorderStyle = BorderStyle.Solid
                TRHead1_081.BorderStyle = BorderStyle.Solid
                TRHead1_09.BorderStyle = BorderStyle.Solid
                TRHead1_091.BorderStyle = BorderStyle.Solid
                TRHead1_10.BorderStyle = BorderStyle.Solid
                TRHead1_101.BorderStyle = BorderStyle.Solid
                TRHead1_14.BorderStyle = BorderStyle.Solid
                TRHead1_141.BorderStyle = BorderStyle.Solid
                TRHead1_15.BorderStyle = BorderStyle.Solid
                TRHead1_151.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TRHead1, TRHead1_04, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_041, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_05, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_051, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_06, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_061, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_07, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_071, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_08, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_081, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_09, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_091, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_10, 3, 3, "l", "Count")
                RH.AddColumn(TRHead1, TRHead1_101, 4, 4, "l", "Total")
                RH.AddColumn(TRHead1, TRHead1_14, 3, 3, "l", "Before")
                RH.AddColumn(TRHead1, TRHead1_141, 4, 4, "l", "After")
                RH.AddColumn(TRHead1, TRHead1_15, 3, 3, "l", "Before")
                RH.AddColumn(TRHead1, TRHead1_151, 4, 4, "l", "After")
                tb.Controls.Add(TRHead1)

                RH.BlankRow(tb, 3)

                If StrDate <> "" Then
                    DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x where " + StrDate).Tables(0)
                Else
                    DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x").Tables(0)
                End If

                For n As Integer = 0 To DTDATE.Rows.Count - 1
                    'Dim createdDate As String = DTDATE(n)(0).ToString()
                    Dim createdDate As String = CDate(DTDATE(n)(0)).ToString("yyyy-MM-dd")
                    DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id,a.ShortOrOverBit,a.Terminal_Bal,a.Admin_Bal,a.Cash_GL,a.Atm_Status,a.Atm_Amount,a.PCB_Before,a.PCB_After,a.GL_Before,a.GL_After from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and convert(date,a.created_date) = convert(date,'" + createdDate + "')").Tables(0)
                    Dim j As Integer = 1
                    For m As Integer = 0 To DT2.Rows.Count - 1
                        Dim TR1 As New TableRow
                        TR1.BorderWidth = "1"
                        TR1.BorderStyle = BorderStyle.Solid

                        Dim TR1_00, TR1_01, TR1_02, TR1_11, TR1_12, TR1_13, TR1_16, TR1_17 As New TableCell

                        TR1_00.BorderWidth = "1"
                        TR1_01.BorderWidth = "1"
                        TR1_02.BorderWidth = "1"
                        TR1_11.BorderWidth = "1"
                        TR1_12.BorderWidth = "1"
                        TR1_13.BorderWidth = "1"
                        TR1_16.BorderWidth = "1"
                        TR1_17.BorderWidth = "1"

                        TR1_00.BorderStyle = BorderStyle.Solid
                        TR1_01.BorderStyle = BorderStyle.Solid
                        TR1_02.BorderStyle = BorderStyle.Solid
                        TR1_11.BorderStyle = BorderStyle.Solid
                        TR1_12.BorderStyle = BorderStyle.Solid
                        TR1_13.BorderStyle = BorderStyle.Solid
                        TR1_16.BorderStyle = BorderStyle.Solid
                        TR1_17.BorderStyle = BorderStyle.Solid

                        TR1_00.RowSpan = 4
                        TR1_01.RowSpan = 4
                        TR1_02.RowSpan = 4
                        TR1_11.RowSpan = 4
                        TR1_12.RowSpan = 4
                        TR1_13.RowSpan = 4
                        TR1_16.RowSpan = 4
                        TR1_17.RowSpan = 4

                        RH.AddColumn(TR1, TR1_00, 2, 2, "c", j)
                        RH.AddColumn(TR1, TR1_01, 4, 4, "c", DT2(m)(0).ToString())
                        RH.AddColumn(TR1, TR1_02, 4, 4, "c", DT2(m)(1).ToString())
                        RH.AddColumn(TR1, TR1_11, 5, 5, "c", DT2(m)(4).ToString())
                        RH.AddColumn(TR1, TR1_12, 5, 5, "c", DT2(m)(5).ToString())
                        RH.AddColumn(TR1, TR1_13, 5, 5, "c", DT2(m)(6).ToString())
                        RH.AddColumn(TR1, TR1_16, 4, 4, "c", DT2(m)(7).ToString())
                        'If DT2(m)(7).ToString() = "Shortage" Then
                        '    RH.AddColumn(TR1, TR1_17, 4, 4, "c", "-" + DT2(m)(8).ToString())
                        'ElseIf DT2(m)(7).ToString() = "Overage" Then
                        '    RH.AddColumn(TR1, TR1_17, 4, 4, "c", "+" + DT2(m)(8).ToString())
                        'ElseIf DT2(m)(7).ToString() = "Tally" Then
                        '    RH.AddColumn(TR1, TR1_17, 4, 4, "c", DT2(m)(8).ToString())
                        'End If
                        RH.AddColumn(TR1, TR1_17, 4, 4, "c", DT2(m)(8).ToString())

                        tb.Controls.Add(TR1)

                        DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)

                        For Each DR In DT1.Rows
                            Dim TR11 As New TableRow
                            TR11.BorderWidth = "1"
                            TR11.BorderStyle = BorderStyle.Solid

                            Dim TR11_00, TR11_01, TR11_02, TR11_03, TR11_04, TR11_041, TR11_05, TR11_051, TR11_06, TR11_061, TR11_07, TR11_071, TR11_08, TR11_081, TR11_09, TR11_091, TR11_10, TR11_101, TR11_11, TR11_111, TR11_12, TR11_121, TR11_13, TR11_131, TR11_14, TR11_141, TR11_15, TR11_151, TR11_16, TR11_161, TR11_17, TR11_171 As New TableCell

                            TR11_00.BorderWidth = "1"
                            TR11_01.BorderWidth = "1"
                            TR11_02.BorderWidth = "1"
                            TR11_03.BorderWidth = "1"
                            TR11_04.BorderWidth = "1"
                            TR11_041.BorderWidth = "1"
                            TR11_05.BorderWidth = "1"
                            TR11_051.BorderWidth = "1"
                            TR11_06.BorderWidth = "1"
                            TR11_061.BorderWidth = "1"
                            TR11_07.BorderWidth = "1"
                            TR11_071.BorderWidth = "1"
                            TR11_08.BorderWidth = "1"
                            TR11_081.BorderWidth = "1"
                            TR11_09.BorderWidth = "1"
                            TR11_091.BorderWidth = "1"
                            TR11_10.BorderWidth = "1"
                            TR11_101.BorderWidth = "1"
                            TR11_11.BorderWidth = "1"
                            TR11_111.BorderWidth = "1"
                            TR11_12.BorderWidth = "1"
                            TR11_121.BorderWidth = "1"
                            TR11_13.BorderWidth = "1"
                            TR11_131.BorderWidth = "1"
                            TR11_14.BorderWidth = "1"
                            TR11_141.BorderWidth = "1"
                            TR11_15.BorderWidth = "1"
                            TR11_151.BorderWidth = "1"
                            TR11_16.BorderWidth = "1"
                            TR11_161.BorderWidth = "1"
                            TR11_17.BorderWidth = "1"
                            TR11_171.BorderWidth = "1"

                            TR11_00.BorderStyle = BorderStyle.Solid
                            TR11_01.BorderStyle = BorderStyle.Solid
                            TR11_02.BorderStyle = BorderStyle.Solid
                            TR11_03.BorderStyle = BorderStyle.Solid
                            TR11_04.BorderStyle = BorderStyle.Solid
                            TR11_041.BorderStyle = BorderStyle.Solid
                            TR11_05.BorderStyle = BorderStyle.Solid
                            TR11_051.BorderStyle = BorderStyle.Solid
                            TR11_06.BorderStyle = BorderStyle.Solid
                            TR11_061.BorderStyle = BorderStyle.Solid
                            TR11_07.BorderStyle = BorderStyle.Solid
                            TR11_071.BorderStyle = BorderStyle.Solid
                            TR11_08.BorderStyle = BorderStyle.Solid
                            TR11_081.BorderStyle = BorderStyle.Solid
                            TR11_09.BorderStyle = BorderStyle.Solid
                            TR11_091.BorderStyle = BorderStyle.Solid
                            TR11_10.BorderStyle = BorderStyle.Solid
                            TR11_101.BorderStyle = BorderStyle.Solid
                            TR11_11.BorderStyle = BorderStyle.Solid
                            TR11_111.BorderStyle = BorderStyle.Solid
                            TR11_12.BorderStyle = BorderStyle.Solid
                            TR11_121.BorderStyle = BorderStyle.Solid
                            TR11_13.BorderStyle = BorderStyle.Solid
                            TR11_131.BorderStyle = BorderStyle.Solid
                            TR11_14.BorderStyle = BorderStyle.Solid
                            TR11_141.BorderStyle = BorderStyle.Solid
                            TR11_15.BorderStyle = BorderStyle.Solid
                            TR11_151.BorderStyle = BorderStyle.Solid
                            TR11_16.BorderStyle = BorderStyle.Solid
                            TR11_161.BorderStyle = BorderStyle.Solid
                            TR11_17.BorderStyle = BorderStyle.Solid
                            TR11_171.BorderStyle = BorderStyle.Solid

                            RH.AddColumn(TR11, TR11_03, 4, 4, "c", DR(0).ToString())
                            RH.AddColumn(TR11, TR11_04, 3, 3, "c", DR(1).ToString())
                            RH.AddColumn(TR11, TR11_041, 4, 4, "c", DR(2).ToString())
                            RH.AddColumn(TR11, TR11_05, 3, 3, "c", DR(3).ToString())
                            RH.AddColumn(TR11, TR11_051, 4, 4, "c", DR(4).ToString())
                            RH.AddColumn(TR11, TR11_06, 3, 3, "c", DR(5).ToString())
                            RH.AddColumn(TR11, TR11_061, 4, 4, "c", DR(6).ToString())
                            RH.AddColumn(TR11, TR11_07, 3, 3, "c", DR(7).ToString())
                            RH.AddColumn(TR11, TR11_071, 4, 4, "c", DR(8).ToString())
                            RH.AddColumn(TR11, TR11_08, 3, 3, "c", DR(9).ToString())
                            RH.AddColumn(TR11, TR11_081, 4, 4, "c", DR(10).ToString())
                            RH.AddColumn(TR11, TR11_09, 3, 3, "c", DR(11).ToString())
                            RH.AddColumn(TR11, TR11_091, 4, 4, "c", DR(12).ToString())
                            RH.AddColumn(TR11, TR11_10, 3, 3, "c", DR(13).ToString())
                            RH.AddColumn(TR11, TR11_101, 4, 4, "c", DR(14).ToString())
                            RH.AddColumn(TR11, TR11_14, 3, 3, "c", DT2(m)(9).ToString())
                            RH.AddColumn(TR11, TR11_141, 4, 4, "c", DT2(m)(10).ToString())
                            RH.AddColumn(TR11, TR11_15, 3, 3, "c", DT2(m)(11).ToString())
                            RH.AddColumn(TR11, TR11_151, 4, 7, "c", DT2(m)(12).ToString())



                            'If DT2(m)(3).ToString() = "1" Then
                            '    RH.AddColumn(TR11, TR11_10, 5, 5, "c", "-" + DR(13).ToString())
                            'ElseIf DT2(m)(3).ToString() = "2" Then
                            '    RH.AddColumn(TR11, TR11_10, 5, 5, "c", "+" + DR(13).ToString())
                            'ElseIf DT2(m)(3).ToString() = "3" Then
                            '    RH.AddColumn(TR11, TR11_10, 5, 5, "c", DR(13).ToString())
                            'End If
                            'If DT2(m)(3).ToString() = "1" Then
                            '    RH.AddColumn(TR11, TR11_101, 7, 7, "c", "-" + DR(14).ToString())
                            'ElseIf DT2(m)(3).ToString() = "2" Then
                            '    RH.AddColumn(TR11, TR11_101, 7, 7, "c", "+" + DR(14).ToString())
                            'ElseIf DT2(m)(3).ToString() = "3" Then
                            '    RH.AddColumn(TR11, TR11_101, 7, 7, "c", DR(14).ToString())
                            'End If

                            tb.Controls.Add(TR11)
                        Next
                        j += 1
                    Next
                    RH.BlankRow(tb, 20)
                Next

                RH.BlankRow(tb, 30)
                pnDisplay.Controls.Add(tb)
            End If
            
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub Export_Excel_Click()
        Try

            Dim WorkBook As XLWorkbook = New XLWorkbook
            Dim WorkSheet = WorkBook.Worksheets.Add("Sheet 1")

            Dim httpResponse = Response

            Dim i As Integer = 0

            Dim BranchCode As String
            Dim ATM As String
            Dim From_Dt As String
            Dim To_Dt As String
            BranchCode = CStr(Request.QueryString.Get("BranchCode"))
            ATM = CStr(Request.QueryString.Get("ATM"))
            From_Dt = Request.QueryString.Get("From_Date")
            To_Dt = Request.QueryString.Get("To_Date")

            Dim StrWhere As String = " 1=1"
            If BranchCode <> -2 Then
                StrWhere += " and a.Branch_code = " + BranchCode
            End If
            If ATM <> -1 Then
                StrWhere += " and a.atm_ID = " + ATM
            End If
            Dim StrDate As String = ""
            If From_Dt <> To_Dt Then
                StrDate += " convert(date,x.Created_Date) between convert(date,'" + From_Dt + "') and convert(date,'" + To_Dt + "')"
            Else
                StrDate += " convert(date,x.Created_Date) = convert(date,'" + From_Dt + "')"
            End If

            WorkSheet.Cell("A1").SetValue("Date")
            WorkSheet.Cell("B1").SetValue("ATM ID")
            WorkSheet.Cell("C1").SetValue("Denomination")
            WorkSheet.Cell("D1").SetValue("Opening Balance")
            WorkSheet.Cell("F1").SetValue("Cash Dispense")
            WorkSheet.Cell("H1").SetValue("Cash In Purge Bin")
            WorkSheet.Cell("J1").SetValue("Closing Before Replenishment")
            WorkSheet.Cell("L1").SetValue("Cash Replenishment")
            WorkSheet.Cell("N1").SetValue("Position After Replenishment")
            WorkSheet.Cell("P1").SetValue("Overage / Shortage")
            WorkSheet.Cell("R1").SetValue("Terminal Balance")
            WorkSheet.Cell("S1").SetValue("Admin Balance")
            WorkSheet.Cell("T1").SetValue("Cash Loaded As Per GL")
            WorkSheet.Cell("U1").SetValue("Physical Cash Balance")
            WorkSheet.Cell("W1").SetValue("GL Balance")
            WorkSheet.Cell("Y1").SetValue("ATM Status")
            WorkSheet.Cell("Z1").SetValue("Amount")


            WorkSheet.Range("D1:E1").Merge()
            WorkSheet.Range("F1:G1").Merge()
            WorkSheet.Range("H1:I1").Merge()
            WorkSheet.Range("J1:K1").Merge()
            WorkSheet.Range("L1:M1").Merge()
            WorkSheet.Range("N1:O1").Merge()
            WorkSheet.Range("P1:Q1").Merge()
            WorkSheet.Range("U1:V1").Merge()
            WorkSheet.Range("W1:X1").Merge()

            WorkSheet.Range("A1:A2").Merge()
            WorkSheet.Range("B1:B2").Merge()
            WorkSheet.Range("C1:C2").Merge()
            WorkSheet.Range("R1:R2").Merge()
            WorkSheet.Range("S1:S2").Merge()
            WorkSheet.Range("T1:T2").Merge()
            WorkSheet.Range("Y1:Y2").Merge()
            WorkSheet.Range("Z1:Z2").Merge()

            WorkSheet.Cell("D2").SetValue("Count")
            WorkSheet.Cell("E2").SetValue("Total")
            WorkSheet.Cell("F2").SetValue("Count")
            WorkSheet.Cell("G2").SetValue("Total")
            WorkSheet.Cell("H2").SetValue("Count")
            WorkSheet.Cell("I2").SetValue("Total")
            WorkSheet.Cell("J2").SetValue("Count")
            WorkSheet.Cell("K2").SetValue("Total")
            WorkSheet.Cell("L2").SetValue("Count")
            WorkSheet.Cell("M2").SetValue("Total")
            WorkSheet.Cell("N2").SetValue("Count")
            WorkSheet.Cell("O2").SetValue("Total")
            WorkSheet.Cell("P2").SetValue("Count")
            WorkSheet.Cell("Q2").SetValue("Total")
            WorkSheet.Cell("U2").SetValue("Before")
            WorkSheet.Cell("V2").SetValue("After")
            WorkSheet.Cell("W2").SetValue("Before")
            WorkSheet.Cell("X2").SetValue("After")

            WorkSheet.Range("A1:Z1").Style.Font.Bold = True
            WorkSheet.Range("A2:Z2").Style.Font.Bold = True

            WorkSheet.Range("A1:Z1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            WorkSheet.Range("A1:Z1").Style.Fill.BackgroundColor = XLColor.AshGrey
            WorkSheet.Range("A2:Z2").Style.Fill.BackgroundColor = XLColor.AshGrey

            WorkSheet.Range("A1:Z1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin
            WorkSheet.Range("A2:Z2").Style.Border.OutsideBorder = XLBorderStyleValues.Thin

            WorkSheet.Columns("A").Width = 15
            WorkSheet.Range("A1:Z1").Style.Alignment.WrapText = True
            WorkSheet.Range("A2:Z2").Style.Alignment.WrapText = True

            If StrDate <> "" Then
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x where " + StrDate).Tables(0)
            Else
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x").Tables(0)
            End If
            Dim j As Integer = 3
            For n As Integer = 0 To DTDATE.Rows.Count - 1
                'Dim createdDate As String = DTDATE(n)(0).ToString()
                System.Threading.Thread.Sleep(5000)


                Dim createdDate As String = CDate(DTDATE(n)(0)).ToString("yyyy-MM-dd")
                'DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and CAST(a.created_date AS date) = CAST('" + createdDate + "' AS date)").Tables(0)
                DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id,a.ShortOrOverBit,a.Terminal_Bal,a.Admin_Bal,a.Cash_GL,a.Atm_Status,a.Atm_Amount,a.PCB_Before,a.PCB_After,a.GL_Before,a.GL_After from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and convert(date,a.created_date) = convert(date,'" + createdDate + "')").Tables(0)
                For m As Integer = 0 To DT2.Rows.Count - 1
                    Dim k As Integer = j
                    WorkSheet.Cell("A" + k.ToString()).SetValue(CDate(DT2(m)(0).ToString()))
                    WorkSheet.Cell("B" + k.ToString()).SetValue(DT2(m)(1).ToString())
                    WorkSheet.Cell("R" + k.ToString()).SetValue(CInt(DT2(m)(4).ToString()))
                    WorkSheet.Cell("S" + k.ToString()).SetValue(CInt(DT2(m)(5).ToString()))
                    WorkSheet.Cell("T" + k.ToString()).SetValue(CInt(DT2(m)(6).ToString()))
                    WorkSheet.Cell("Y" + k.ToString()).SetValue(DT2(m)(7).ToString())

                    'If DT2(m)(7).ToString() = "Shortage" Then
                    '    WorkSheet.Cell("Z" + k.ToString()).SetValue("-" + DT2(m)(8).ToString())
                    'ElseIf DT2(m)(7).ToString() = "Overage" Then
                    '    WorkSheet.Cell("Z" + k.ToString()).SetValue("+" + DT2(m)(8).ToString())
                    'ElseIf DT2(m)(7).ToString() = "Tally" Then
                    '    WorkSheet.Cell("Z" + k.ToString()).SetValue(DT2(m)(8).ToString())
                    'End If

                    WorkSheet.Cell("Z" + k.ToString()).SetValue(CInt(DT2(m)(8).ToString()))

                    WorkSheet.Range("A" + k.ToString() + ":A" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("B" + k.ToString() + ":B" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("R" + k.ToString() + ":R" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("S" + k.ToString() + ":S" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("T" + k.ToString() + ":T" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("Y" + k.ToString() + ":Y" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("Z" + k.ToString() + ":Z" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("A" + k.ToString() + ":A" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("B" + k.ToString() + ":B" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("R" + k.ToString() + ":R" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("S" + k.ToString() + ":S" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("T" + k.ToString() + ":T" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("Y" + k.ToString() + ":Y" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("Z" + k.ToString() + ":Z" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("A" + k.ToString() + ":A" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("B" + k.ToString() + ":B" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("R" + k.ToString() + ":R" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("S" + k.ToString() + ":S" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("T" + k.ToString() + ":T" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("Y" + k.ToString() + ":Y" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("Z" + k.ToString() + ":Z" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    'DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)
                    DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)
                    For Each DR In DT1.Rows
                        WorkSheet.Cell("C" + j.ToString()).SetValue(CInt(DR(0).ToString()))
                        WorkSheet.Cell("D" + j.ToString()).SetValue(CInt(DR(1).ToString()))
                        WorkSheet.Cell("E" + j.ToString()).SetValue(CInt(DR(2).ToString()))
                        WorkSheet.Cell("F" + j.ToString()).SetValue(CInt(DR(3).ToString()))
                        WorkSheet.Cell("G" + j.ToString()).SetValue(CInt(DR(4).ToString()))
                        WorkSheet.Cell("H" + j.ToString()).SetValue(CInt(DR(5).ToString()))
                        WorkSheet.Cell("I" + j.ToString()).SetValue(CInt(DR(6).ToString()))
                        WorkSheet.Cell("J" + j.ToString()).SetValue(CInt(DR(7).ToString()))
                        WorkSheet.Cell("K" + j.ToString()).SetValue(CInt(DR(8).ToString()))
                        WorkSheet.Cell("L" + j.ToString()).SetValue(CInt(DR(9).ToString()))
                        WorkSheet.Cell("M" + j.ToString()).SetValue(CInt(DR(10).ToString()))
                        WorkSheet.Cell("N" + j.ToString()).SetValue(CInt(DR(11).ToString()))
                        WorkSheet.Cell("O" + j.ToString()).SetValue(CInt(DR(12).ToString()))
                        'If DT2(m)(7).ToString() = "Shortage" Then
                        '    WorkSheet.Cell("P" + j.ToString()).SetValue("-" + CInt(DR(13).ToString()))
                        '    WorkSheet.Cell("Q" + j.ToString()).SetValue("-" + CInt(DR(14).ToString()))
                        'ElseIf DT2(m)(7).ToString() = "Overage" Then
                        '    WorkSheet.Cell("P" + j.ToString()).SetValue("+" + CInt(DR(13).ToString()))
                        '    WorkSheet.Cell("Q" + j.ToString()).SetValue("+" + CInt(DR(14).ToString()))
                        'ElseIf DT2(m)(7).ToString() = "Tally" Then
                        '    WorkSheet.Cell("P" + j.ToString()).SetValue(CInt(DR(13).ToString()))
                        '    WorkSheet.Cell("Q" + j.ToString()).SetValue(CInt(DR(14).ToString()))
                        'End If
                        WorkSheet.Cell("P" + j.ToString()).SetValue(CInt(DR(13).ToString()))
                        WorkSheet.Cell("Q" + j.ToString()).SetValue(CInt(DR(14).ToString()))
                        WorkSheet.Cell("U" + j.ToString()).SetValue(CInt(DT2(m)(9).ToString()))
                        WorkSheet.Cell("V" + j.ToString()).SetValue(CInt(DT2(m)(10).ToString()))
                        WorkSheet.Cell("W" + j.ToString()).SetValue(CInt(DT2(m)(11).ToString()))
                        WorkSheet.Cell("X" + j.ToString()).SetValue(CInt(DT2(m)(12).ToString()))
                        WorkSheet.Range("C" + j.ToString() + ":X" + j.ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                        j += 1
                    Next
                Next
                System.Threading.Thread.Sleep(5000)
            Next
            Dim today As Date = Date.Today()
            Dim filename = "CBRReport" + today.ToString("ddMMMyyyy") + ".xls"

            httpResponse.Clear()
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            httpResponse.AddHeader("content-disposition", "attachment;filename=""" + filename + """")
            Using tmpMemoryStream As MemoryStream = New MemoryStream()
                WorkBook.SaveAs(tmpMemoryStream)
                tmpMemoryStream.WriteTo(httpResponse.OutputStream)
                tmpMemoryStream.Close()
            End Using
            httpResponse.End()

        Catch ex As Exception
        End Try
    End Sub
End Class
