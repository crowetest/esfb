﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Entry_FIS_Report
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As String
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1377) = False Then 'main
                'If GF.FormAccess(CInt(Session("UserID")), 1356) = False Then 'uat
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Entry Report"
            UserID = CInt(Session("UserID"))
            If Not IsPostBack Then
                Dim UserID As String = CStr(Session("UserID"))
                DT = DB.ExecuteDataSet("SELECT -2 as branch_id,'--ALL--' as branch_name union all select branch_id,branch_name from branch_master where status_id = 1").Tables(0)
                GF.ComboFill(cmbBranch, DT, 0, 1)
                DT1 = DB.ExecuteDataSet("SELECT -1 AS VALUE,'--ALL--' AS ATM_ID UNION ALL SELECT MASTER_ID,ATM_ID FROM CBR_ATM_MASTER").Tables(0)
                GF.ComboFill(cmbAtm, DT1, 0, 1)
                Me.txt_From_Dt.Text = Date.Today.ToString("dd MMM yyyy")
                Me.txt_To_Dt.Text = Date.Today.ToString("dd MMM yyyy")
            End If
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            DT1 = DB.ExecuteDataSet("SELECT -1 AS VALUE,'--ALL--' AS ATM_ID UNION ALL SELECT MASTER_ID,ATM_ID FROM CBR_ATM_MASTER WHERE BRANCH_ID = " + Data(1)).Tables(0)
            For Each DR As DataRow In DT1.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
#End Region
End Class

