﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports System.Data.SqlClient
Imports ClosedXML.Excel
Imports DocumentFormat.OpenXml

Partial Class View_Entry_FIS_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DTDATE As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim ViewType As Integer
            Dim BranchCode As String
            Dim ATM As String
            Dim From_Dt As String
            Dim To_Dt As String
            ViewType = CInt(Request.QueryString.Get("ViewType"))

            If ViewType = 1 Then
                Export_Excel_Click()
            End If

            BranchCode = CStr(Request.QueryString.Get("BranchCode"))
            ATM = CStr(Request.QueryString.Get("ATM"))
            'From_Dt = CDate(Request.QueryString.Get("From_Date")).ToString("yyyy-MM-dd")
            'To_Dt = CDate(Request.QueryString.Get("To_Date")).ToString("yyyy-MM-dd")
            From_Dt = CStr(Request.QueryString.Get("From_Date"))
            To_Dt = Request.QueryString.Get("To_Date")

            Dim StrWhere As String = " 1=1"
            If BranchCode <> -2 Then
                StrWhere += " and a.Branch_code = " + BranchCode
            End If
            If ATM <> -1 Then
                StrWhere += " and a.atm_ID = " + ATM
            End If
            Dim StrDate As String = ""
            If From_Dt <> To_Dt Then
                StrDate += " convert(date,x.Created_Date) between convert(date,'" + From_Dt + "') and convert(date,'" + To_Dt + "')"
            Else
                StrDate += " convert(date,x.Created_Date) = convert(date,'" + From_Dt + "')"
            End If

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")


            RH.Heading(Session("FirmName"), tb, "Entry Report", 100)
            tb.Attributes.Add("width", "100%")
            RH.BlankRow(tb, 3)


            Dim TRHead As New TableRow
            TRHead.BackColor = System.Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"

            TRHead_00.BorderColor = System.Drawing.Color.Silver
            TRHead_01.BorderColor = System.Drawing.Color.Silver
            TRHead_02.BorderColor = System.Drawing.Color.Silver
            TRHead_03.BorderColor = System.Drawing.Color.Silver
            TRHead_04.BorderColor = System.Drawing.Color.Silver
            TRHead_05.BorderColor = System.Drawing.Color.Silver
            TRHead_06.BorderColor = System.Drawing.Color.Silver
            TRHead_07.BorderColor = System.Drawing.Color.Silver
            TRHead_08.BorderColor = System.Drawing.Color.Silver
            TRHead_09.BorderColor = System.Drawing.Color.Silver
            TRHead_10.BorderColor = System.Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid

            TRHead_00.RowSpan = 2
            TRHead_01.RowSpan = 2
            TRHead_02.RowSpan = 2
            TRHead_03.RowSpan = 2

            RH.AddColumn(TRHead, TRHead_00, 2, 2, "l", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 5, 5, "l", "Date")
            RH.AddColumn(TRHead, TRHead_02, 4, 4, "l", "ATM ID")
            RH.AddColumn(TRHead, TRHead_03, 5, 5, "l", "Denomination")
            RH.AddColumn(TRHead, TRHead_04, 12, 12, "l", "Opening Balance")
            RH.AddColumn(TRHead, TRHead_05, 12, 12, "l", "Cash Dispense")
            RH.AddColumn(TRHead, TRHead_06, 12, 12, "l", "Cash In Purge Bin")
            RH.AddColumn(TRHead, TRHead_07, 12, 12, "l", "Closing Before Replenishment")
            RH.AddColumn(TRHead, TRHead_08, 12, 12, "l", "Cash Replenishment")
            RH.AddColumn(TRHead, TRHead_09, 12, 12, "l", "Position After Replenishment")
            RH.AddColumn(TRHead, TRHead_10, 12, 12, "l", "Overage / Shortage")
            tb.Controls.Add(TRHead)


            Dim TRHead1 As New TableRow
            Dim TRHead1_04, TRHead1_041, TRHead1_05, TRHead1_051, TRHead1_06, TRHead1_061, TRHead1_07, TRHead1_071, TRHead1_08, TRHead1_081, TRHead1_09, TRHead1_091, TRHead1_10, TRHead1_101 As New TableCell

            TRHead1_04.BorderWidth = "1"
            TRHead1_041.BorderWidth = "1"
            TRHead1_05.BorderWidth = "1"
            TRHead1_051.BorderWidth = "1"
            TRHead1_06.BorderWidth = "1"
            TRHead1_061.BorderWidth = "1"
            TRHead1_07.BorderWidth = "1"
            TRHead1_071.BorderWidth = "1"
            TRHead1_08.BorderWidth = "1"
            TRHead1_081.BorderWidth = "1"
            TRHead1_09.BorderWidth = "1"
            TRHead1_091.BorderWidth = "1"
            TRHead1_10.BorderWidth = "1"
            TRHead1_101.BorderWidth = "1"

            TRHead1_04.BorderColor = System.Drawing.Color.Silver
            TRHead1_041.BorderColor = System.Drawing.Color.Silver
            TRHead1_05.BorderColor = System.Drawing.Color.Silver
            TRHead1_051.BorderColor = System.Drawing.Color.Silver
            TRHead1_06.BorderColor = System.Drawing.Color.Silver
            TRHead1_061.BorderColor = System.Drawing.Color.Silver
            TRHead1_07.BorderColor = System.Drawing.Color.Silver
            TRHead1_071.BorderColor = System.Drawing.Color.Silver
            TRHead1_08.BorderColor = System.Drawing.Color.Silver
            TRHead1_081.BorderColor = System.Drawing.Color.Silver
            TRHead1_09.BorderColor = System.Drawing.Color.Silver
            TRHead1_091.BorderColor = System.Drawing.Color.Silver
            TRHead1_10.BorderColor = System.Drawing.Color.Silver
            TRHead1_101.BorderColor = System.Drawing.Color.Silver

            TRHead1_04.BorderStyle = BorderStyle.Solid
            TRHead1_041.BorderStyle = BorderStyle.Solid
            TRHead1_05.BorderStyle = BorderStyle.Solid
            TRHead1_051.BorderStyle = BorderStyle.Solid
            TRHead1_06.BorderStyle = BorderStyle.Solid
            TRHead1_061.BorderStyle = BorderStyle.Solid
            TRHead1_07.BorderStyle = BorderStyle.Solid
            TRHead1_071.BorderStyle = BorderStyle.Solid
            TRHead1_08.BorderStyle = BorderStyle.Solid
            TRHead1_081.BorderStyle = BorderStyle.Solid
            TRHead1_09.BorderStyle = BorderStyle.Solid
            TRHead1_091.BorderStyle = BorderStyle.Solid
            TRHead1_10.BorderStyle = BorderStyle.Solid
            TRHead1_101.BorderStyle = BorderStyle.Solid

            RH.AddColumn(TRHead1, TRHead1_04, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_041, 7, 7, "l", "Total")
            RH.AddColumn(TRHead1, TRHead1_05, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_051, 7, 7, "l", "Total")
            RH.AddColumn(TRHead1, TRHead1_06, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_061, 7, 7, "l", "Total")
            RH.AddColumn(TRHead1, TRHead1_07, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_071, 7, 7, "l", "Total")
            RH.AddColumn(TRHead1, TRHead1_08, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_081, 7, 7, "l", "Total")
            RH.AddColumn(TRHead1, TRHead1_09, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_091, 7, 7, "l", "Total")
            RH.AddColumn(TRHead1, TRHead1_10, 5, 5, "l", "Count")
            RH.AddColumn(TRHead1, TRHead1_101, 7, 7, "l", "Total")
            tb.Controls.Add(TRHead1)

            RH.BlankRow(tb, 3)

            If StrDate <> "" Then
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x where " + StrDate).Tables(0)
            Else
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x").Tables(0)
            End If

            For n As Integer = 0 To DTDATE.Rows.Count - 1
                Dim createdDate As String = CDate(DTDATE(n)(0)).ToString("yyyy-MM-dd")
                DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id,a.ShortOrOverBit,a.atm_status from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and convert(date,a.created_date) = convert(date,'" + createdDate + "')").Tables(0)
                Dim j As Integer = 1
                For m As Integer = 0 To DT2.Rows.Count - 1
                    Dim TR1 As New TableRow
                    TR1.BorderWidth = "1"
                    TR1.BorderStyle = BorderStyle.Solid

                    Dim TR1_00, TR1_01, TR1_02 As New TableCell

                    TR1_00.BorderWidth = "1"
                    TR1_01.BorderWidth = "1"
                    TR1_02.BorderWidth = "1"

                    TR1_00.BorderStyle = BorderStyle.Solid
                    TR1_01.BorderStyle = BorderStyle.Solid
                    TR1_02.BorderStyle = BorderStyle.Solid

                    TR1_00.RowSpan = 4
                    TR1_01.RowSpan = 4
                    TR1_02.RowSpan = 4

                    RH.AddColumn(TR1, TR1_00, 2, 2, "c", j)
                    RH.AddColumn(TR1, TR1_01, 5, 5, "c", DT2(m)(0).ToString())
                    RH.AddColumn(TR1, TR1_02, 4, 4, "c", DT2(m)(1).ToString())

                    tb.Controls.Add(TR1)

                    DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)

                    For Each DR In DT1.Rows
                        Dim TR11 As New TableRow
                        TR11.BorderWidth = "1"
                        TR11.BorderStyle = BorderStyle.Solid

                        Dim TR11_00, TR11_01, TR11_02, TR11_03, TR11_04, TR11_041, TR11_05, TR11_051, TR11_06, TR11_061, TR11_07, TR11_071, TR11_08, TR11_081, TR11_09, TR11_091, TR11_10, TR11_101 As New TableCell

                        TR11_00.BorderWidth = "1"
                        TR11_01.BorderWidth = "1"
                        TR11_02.BorderWidth = "1"
                        TR11_03.BorderWidth = "1"
                        TR11_04.BorderWidth = "1"
                        TR11_041.BorderWidth = "1"
                        TR11_05.BorderWidth = "1"
                        TR11_051.BorderWidth = "1"
                        TR11_06.BorderWidth = "1"
                        TR11_061.BorderWidth = "1"
                        TR11_07.BorderWidth = "1"
                        TR11_071.BorderWidth = "1"
                        TR11_08.BorderWidth = "1"
                        TR11_081.BorderWidth = "1"
                        TR11_09.BorderWidth = "1"
                        TR11_091.BorderWidth = "1"
                        TR11_10.BorderWidth = "1"
                        TR11_101.BorderWidth = "1"

                        TR11_00.BorderStyle = BorderStyle.Solid
                        TR11_01.BorderStyle = BorderStyle.Solid
                        TR11_02.BorderStyle = BorderStyle.Solid
                        TR11_03.BorderStyle = BorderStyle.Solid
                        TR11_04.BorderStyle = BorderStyle.Solid
                        TR11_041.BorderStyle = BorderStyle.Solid
                        TR11_05.BorderStyle = BorderStyle.Solid
                        TR11_051.BorderStyle = BorderStyle.Solid
                        TR11_06.BorderStyle = BorderStyle.Solid
                        TR11_061.BorderStyle = BorderStyle.Solid
                        TR11_07.BorderStyle = BorderStyle.Solid
                        TR11_071.BorderStyle = BorderStyle.Solid
                        TR11_08.BorderStyle = BorderStyle.Solid
                        TR11_081.BorderStyle = BorderStyle.Solid
                        TR11_09.BorderStyle = BorderStyle.Solid
                        TR11_091.BorderStyle = BorderStyle.Solid
                        TR11_10.BorderStyle = BorderStyle.Solid
                        TR11_101.BorderStyle = BorderStyle.Solid

                        RH.AddColumn(TR11, TR11_03, 5, 5, "c", DR(0).ToString())
                        RH.AddColumn(TR11, TR11_04, 5, 5, "c", DR(1).ToString())
                        RH.AddColumn(TR11, TR11_041, 7, 7, "c", DR(2).ToString())
                        RH.AddColumn(TR11, TR11_05, 5, 5, "c", DR(3).ToString())
                        RH.AddColumn(TR11, TR11_051, 7, 7, "c", DR(4).ToString())
                        RH.AddColumn(TR11, TR11_06, 5, 5, "c", DR(5).ToString())
                        RH.AddColumn(TR11, TR11_061, 7, 7, "c", DR(6).ToString())
                        RH.AddColumn(TR11, TR11_07, 5, 5, "c", DR(7).ToString())
                        RH.AddColumn(TR11, TR11_071, 7, 7, "c", DR(8).ToString())
                        RH.AddColumn(TR11, TR11_08, 5, 5, "c", DR(9).ToString())
                        RH.AddColumn(TR11, TR11_081, 7, 7, "c", DR(10).ToString())
                        RH.AddColumn(TR11, TR11_09, 5, 5, "c", DR(11).ToString())
                        RH.AddColumn(TR11, TR11_091, 7, 7, "c", DR(12).ToString())
                        'RH.AddColumn(TR11, TR11_10, 5, 5, "c", DR(13).ToString())
                        'RH.AddColumn(TR11, TR11_101, 7, 7, "c", DR(14).ToString())
                        If DT2(m)(4).ToString() = "Shortage" Then
                            RH.AddColumn(TR11, TR11_10, 5, 5, "c", "-" + DR(13).ToString())
                        ElseIf DT2(m)(4).ToString() = "Overage" Then
                            RH.AddColumn(TR11, TR11_10, 5, 5, "c", "+" + DR(13).ToString())
                        ElseIf DT2(m)(4).ToString() = "Tally" Then
                            RH.AddColumn(TR11, TR11_10, 5, 5, "c", DR(13).ToString())
                        End If
                        If DT2(m)(4).ToString() = "Shortage" Then
                            RH.AddColumn(TR11, TR11_101, 7, 7, "c", "-" + DR(14).ToString())
                        ElseIf DT2(m)(4).ToString() = "Overage" Then
                            RH.AddColumn(TR11, TR11_101, 7, 7, "c", "+" + DR(14).ToString())
                        ElseIf DT2(m)(4).ToString() = "Tally" Then
                            RH.AddColumn(TR11, TR11_101, 7, 7, "c", DR(14).ToString())
                        End If

                        tb.Controls.Add(TR11)
                    Next
                    j += 1
                Next
                RH.BlankRow(tb, 20)
            Next

            RH.BlankRow(tb, 30)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub Export_Excel_Click()
        Try

            Dim WorkBook As XLWorkbook = New XLWorkbook
            Dim WorkSheet = WorkBook.Worksheets.Add("Sheet 1")

            Dim httpResponse = Response

            Dim i As Integer = 0

            Dim BranchCode As String
            Dim ATM As String
            Dim From_Dt As String
            Dim To_Dt As String
            BranchCode = CStr(Request.QueryString.Get("BranchCode"))
            ATM = CStr(Request.QueryString.Get("ATM"))
            From_Dt = CStr(Request.QueryString.Get("From_Date"))
            To_Dt = CStr(Request.QueryString.Get("To_Date"))

            Dim StrWhere As String = " 1=1"
            If BranchCode <> -2 Then
                StrWhere += " and a.Branch_code = " + BranchCode
            End If
            If ATM <> -1 Then
                StrWhere += " and a.atm_ID = " + ATM
            End If
            Dim StrDate As String = ""
            If From_Dt <> To_Dt Then
                StrDate += " convert(date,x.Created_Date) between convert(date,'" + From_Dt + "') and convert(date,'" + To_Dt + "')"
            Else
                StrDate += " convert(date,x.Created_Date) = convert(date,'" + From_Dt + "')"
            End If

            WorkSheet.Cell("A1").SetValue("Date")
            WorkSheet.Cell("B1").SetValue("ATM ID")
            WorkSheet.Cell("C1").SetValue("Denomination")
            WorkSheet.Cell("D1").SetValue("Opening Balance")
            WorkSheet.Cell("F1").SetValue("Cash Dispense")
            WorkSheet.Cell("H1").SetValue("Cash In Purge Bin")
            WorkSheet.Cell("J1").SetValue("Closing Before Replenishment")
            WorkSheet.Cell("L1").SetValue("Cash Replenishment")
            WorkSheet.Cell("N1").SetValue("Position After Replenishment")
            WorkSheet.Cell("P1").SetValue("Overage / Shortage")

            WorkSheet.Range("D1:E1").Merge()
            WorkSheet.Range("F1:G1").Merge()
            WorkSheet.Range("H1:I1").Merge()
            WorkSheet.Range("J1:K1").Merge()
            WorkSheet.Range("L1:M1").Merge()
            WorkSheet.Range("N1:O1").Merge()
            WorkSheet.Range("P1:R1").Merge()

            WorkSheet.Range("A1:A2").Merge()
            WorkSheet.Range("B1:B2").Merge()
            WorkSheet.Range("C1:C2").Merge()

            WorkSheet.Cell("D2").SetValue("Count")
            WorkSheet.Cell("E2").SetValue("Total")
            WorkSheet.Cell("F2").SetValue("Count")
            WorkSheet.Cell("G2").SetValue("Total")
            WorkSheet.Cell("H2").SetValue("Count")
            WorkSheet.Cell("I2").SetValue("Total")
            WorkSheet.Cell("J2").SetValue("Count")
            WorkSheet.Cell("K2").SetValue("Total")
            WorkSheet.Cell("L2").SetValue("Count")
            WorkSheet.Cell("M2").SetValue("Total")
            WorkSheet.Cell("N2").SetValue("Count")
            WorkSheet.Cell("O2").SetValue("Total")
            WorkSheet.Cell("P2").SetValue("Status")
            WorkSheet.Cell("Q2").SetValue("Count")
            WorkSheet.Cell("R2").SetValue("Total")

            WorkSheet.Range("A1:R1").Style.Font.Bold = True
            WorkSheet.Range("A2:R2").Style.Font.Bold = True

            WorkSheet.Range("A1:R1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            WorkSheet.Range("A1:R1").Style.Fill.BackgroundColor = XLColor.AshGrey
            WorkSheet.Range("A2:R2").Style.Fill.BackgroundColor = XLColor.AshGrey

            WorkSheet.Range("A1:R1").Style.Border.OutsideBorder = XLBorderStyleValues.Thin
            WorkSheet.Range("A2:R2").Style.Border.OutsideBorder = XLBorderStyleValues.Thin

            WorkSheet.Columns("A").Width = 15

            If StrDate <> "" Then
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x where " + StrDate).Tables(0)
            Else
                DTDATE = DB.ExecuteDataSet("select distinct convert(date,x.created_date) from cbr_entry_master x").Tables(0)
            End If
            Dim j As Integer = 3
            For n As Integer = 0 To DTDATE.Rows.Count - 1
                Dim createdDate As String = CDate(DTDATE(n)(0)).ToString("yyyy-MM-dd")
                DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id,a.atm_status from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and convert(date,a.created_date) = convert(date,'" + createdDate + "')").Tables(0)
                For m As Integer = 0 To DT2.Rows.Count - 1
                    Dim k As Integer = j
                    WorkSheet.Cell("A" + k.ToString()).SetValue(CDate(DT2(m)(0).ToString()))
                    WorkSheet.Cell("B" + k.ToString()).SetValue(DT2(m)(1).ToString())
                    WorkSheet.Cell("P" + k.ToString()).SetValue(DT2(m)(3).ToString())

                    WorkSheet.Range("A" + k.ToString() + ":A" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("B" + k.ToString() + ":B" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("P" + k.ToString() + ":P" + (k + 1).ToString()).Merge()
                    WorkSheet.Range("A" + k.ToString() + ":A" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("B" + k.ToString() + ":B" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("P" + k.ToString() + ":P" + (k + 1).ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                    WorkSheet.Range("A" + k.ToString() + ":A" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("B" + k.ToString() + ":B" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    WorkSheet.Range("P" + k.ToString() + ":P" + (k + 1).ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                    DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)
                    For Each DR In DT1.Rows
                        WorkSheet.Cell("C" + j.ToString()).SetValue(DR(0).ToString())
                        WorkSheet.Cell("D" + j.ToString()).SetValue(DR(1).ToString())
                        WorkSheet.Cell("E" + j.ToString()).SetValue(DR(2).ToString())
                        WorkSheet.Cell("F" + j.ToString()).SetValue(DR(3).ToString())
                        WorkSheet.Cell("G" + j.ToString()).SetValue(DR(4).ToString())
                        WorkSheet.Cell("H" + j.ToString()).SetValue(DR(5).ToString())
                        WorkSheet.Cell("I" + j.ToString()).SetValue(DR(6).ToString())
                        WorkSheet.Cell("J" + j.ToString()).SetValue(DR(7).ToString())
                        WorkSheet.Cell("K" + j.ToString()).SetValue(DR(8).ToString())
                        WorkSheet.Cell("L" + j.ToString()).SetValue(DR(9).ToString())
                        WorkSheet.Cell("M" + j.ToString()).SetValue(DR(10).ToString())
                        WorkSheet.Cell("N" + j.ToString()).SetValue(DR(11).ToString())
                        WorkSheet.Cell("O" + j.ToString()).SetValue(DR(12).ToString())
                        'If DT2(m)(3).ToString() = "Shortage" Then
                        '    WorkSheet.Cell("P" + j.ToString()).SetValue("-" + CInt(DR(13).ToString()))
                        'ElseIf DT2(m)(3).ToString() = "Overage" Then
                        '    WorkSheet.Cell("P" + j.ToString()).SetValue("+" + CInt(DR(13).ToString()))
                        'ElseIf DT2(m)(3).ToString() = "Tally" Then
                        '    WorkSheet.Cell("P" + j.ToString()).SetValue(CInt(DR(13).ToString()))
                        'End If
                        'If DT2(m)(3).ToString() = "Shortage" Then
                        '    WorkSheet.Cell("Q" + j.ToString()).SetValue("-" + CInt(DR(14).ToString()))
                        'ElseIf DT2(m)(3).ToString() = "Overage" Then
                        '    WorkSheet.Cell("Q" + j.ToString()).SetValue("+" + CInt(DR(14).ToString()))
                        'ElseIf DT2(m)(3).ToString() = "Tally" Then
                        '    WorkSheet.Cell("Q" + j.ToString()).SetValue(CInt(DR(14).ToString()))
                        'End If
                        WorkSheet.Cell("Q" + j.ToString()).SetValue(DR(13).ToString())
                        WorkSheet.Cell("R" + j.ToString()).SetValue(DR(14).ToString())
                        WorkSheet.Range("C" + j.ToString() + ":R" + j.ToString()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin
                        j += 1
                    Next
                Next
            Next
            Dim today As Date = Date.Today()
            Dim filename = "CBRReport" + today.ToString("ddMMMyyyy") + ".xls"

            httpResponse.Clear()
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            httpResponse.AddHeader("content-disposition", "attachment;filename=""" + filename + """")
            Using tmpMemoryStream As MemoryStream = New MemoryStream()
                WorkBook.SaveAs(tmpMemoryStream)
                tmpMemoryStream.WriteTo(httpResponse.OutputStream)
                tmpMemoryStream.Close()
            End Using
            httpResponse.End()

        Catch ex As Exception
        End Try
    End Sub
End Class
