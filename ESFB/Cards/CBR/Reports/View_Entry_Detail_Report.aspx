﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="View_Entry_Detail_Report.aspx.vb" Inherits= "View_Entry_Detail_Report" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../../Style/ExportMenu.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
*                           {	padding:0; margin-left: 0px; margin-right: 0px;
}
    </style>
</head>
<body style="background-color:Black;">
 <script language="javascript" type="text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnDisplay.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=850,location=0,status=0');
         printWindow.document.write('<html><head><title>ESAF</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
     function Exitform() {
         var branch = document.getElementById("<%= hdnBranch.ClientID %>").value;
         if (branch == "0") {
             window.open("Entry_FIS_Report.aspx", "_self");
         } else {
             window.open("Entry_Report_Branch.aspx", "_self");
         }
         return false;
     }
     </script>
    <form id="form1" runat="server">    
    <div style="width:140%; background-color:white; min-height:750px; margin:0px auto;">
        <div style="text-align:right ;margin:0px auto; width:95%; background-color:white; ">          
            <asp:ImageButton ID="cmd_Back" style="text-align:left ;float:left; padding-top:12px;" runat="server" Height="35px" Width="35px" ImageAlign="AbsMiddle" ImageUrl="~/Image/back.png" ToolTip="Back"/>
            <asp:ImageButton ID="cmd_Print" style="text-align:right ; padding-top:8px;" runat="server" Height="40px" Width="40px" ImageAlign="AbsMiddle" ImageUrl="~/Image/print.png" ToolTip="Click to Print"/>
            <div id="MenuContainer" style="height:40px; width:40px; float:right;" >
                <ul class="menu">			               
			        <li><a href="#"><span><asp:ImageButton ID="ImageButton1" style="text-align:right ;" runat="server" Height="30px" Width="30px" ImageAlign="AbsMiddle" ImageUrl="~/Image/Export.png" ToolTip="Export"/></span></a>
				        <ul class="menu-hover">	
				        </ul>
			        </li>			            
		        </ul>
            </div>
        </div>
        <br style="background-color:white"/>
        <div style="text-align:center;margin:0px auto; width:95%; background-color:white; font-family:Cambria">   
            <asp:Panel ID="pnDisplay" runat="server" Width="100%">
            </asp:Panel>
        </div>
            <asp:HiddenField ID="hdnBranch" runat="server" />
    </div>
    </form>
</body>
</html>
</asp:Content>