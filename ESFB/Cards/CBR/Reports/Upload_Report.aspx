﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Upload_Report.aspx.vb" Inherits="Upload_Report"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <link href="../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
    
    <body>
    <script type="text/javascript" src="../../../Script/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
    
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }    
      
    function btnExcel_onclick(){        
        var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;
        var ATM = document.getElementById("<%= cmbAtm.ClientID %>").value;
        var From_Dt = document.getElementById("<%= txt_From_Dt.ClientID %>").value;
        var To_Dt = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
        var Data = "2Ø" + Branch + "Ø" + ATM + "Ø" + From_Dt + "Ø" + To_Dt;
        ToServer(Data ,2);
    }     
    function btnView_onclick() 
        {
           var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;
           var ATM = document.getElementById("<%= cmbAtm.ClientID %>").value;
           var From_Dt = document.getElementById("<%= txt_From_Dt.ClientID %>").value;
           var To_Dt = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
           var Status = -1;
           window.open("View_Upload_Report.aspx?Branch=" + Branch + "&ATM=" + ATM + "&From="+From_Dt+"&To="+To_Dt+"&Status=" + Status +" ", "_self"); 
                 
        }   
    function BranchOnChange(){
        var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;
        var SaveData = "1Ø" + Branch;
        ToServer(SaveData, 1); 
    } 
             
   function FromServer(arg, context) {
         switch (context) {
            case 1:
                {     
                    ComboFill(arg, "<%= cmbAtm.ClientID %>");
                    break;
                }           
            case 2:
               {                           
                alert(arg);
               }                            
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
    </script>

<br />
    <table align="center" style="width: 30%; text-align:center; margin:0px auto;">
       <tr>
            <td style="width:30%;">
                Branch</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black" >
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
       <td><br /></td>
       </tr>
        <tr>
            <td style="width:30%;">
                ATM</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbAtm" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
       <td><br /></td>
       </tr>
        <tr>
            <td style="width:30%;">
                Date From</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_From_Dt" class="NormalText" runat="server" 
                    Width="81%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_From_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_From_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
        <tr>
       <td><br /></td>
       </tr>
        <tr>
            <td style="width:30%;">
                Date To</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_To_Dt" class="NormalText" runat="server" 
                    Width="81%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_To_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_To_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
      <%--  <tr>
            <td style="width:30%;">
                Upload Status</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbStatus" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                    <asp:ListItem Text="--ALL--" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="YES" Value="1"></asp:ListItem>
                    <asp:ListItem Text="NO" Value="2"></asp:ListItem>
                </asp:DropDownList>               
            </td>           
        </tr>--%>
       <tr>
       <td><br /></td>
       </tr>
        <tr>
            <td style="width:35%; height: 18px; text-align:center;" colspan="2">             
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />&nbsp;&nbsp;
                <input id="Button1" style="font-family: Cambria; font-size: 10pt; width: 120px;display:none;" 
                type="button" value="DOWNLOAD EXCEL"  onclick="return btnExcel_onclick()" />&nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 67px;" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            </td>        
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                <asp:HiddenField ID="hdnReportID" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width:15%;">
                &nbsp;</td>
            <td  style="width:85%;">
                &nbsp;</td>
               
                 <asp:HiddenField ID="hdnValue" runat="server" />
                 <asp:HiddenField ID="hdnAdmin" runat="server" />
                 <asp:HiddenField ID="hdnUser" runat="server" />
                 <asp:HiddenField ID="hidBID" runat="server" />
        </tr>
    </table>
    </body>
    </html>
</asp:Content>

 