﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Entry_FIS_Report.aspx.vb" Inherits="Entry_FIS_Report"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
    <link href="../../../Style/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Branch</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black" onchange="return BranchOnChange()">
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                ATM</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbAtm" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Date From</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_From_Dt" class="NormalText" runat="server" 
                    Width="80%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_From_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_From_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Date To</td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:TextBox ID="txt_To_Dt" class="NormalText" runat="server" 
                    Width="80%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txt_To_Dt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txt_To_Dt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                Report Type
            </td>
            <td  style="width:70%;">
                &nbsp; &nbsp;
                <asp:DropDownList ID="cmbReportType" class="NormalText" runat="server" Font-Names="Cambria" Width="81%" ForeColor="Black">
                    <asp:ListItem Text="--SELECT--" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Detailed Report" Value="1"></asp:ListItem>
                    <asp:ListItem Text="FIS Report" Value="2"></asp:ListItem>
                </asp:DropDownList>               
            </td>           
        </tr>
        <tr>
            <td style="width:30%;">
                &nbsp;
            </td>
            <td  style="width:70%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width:100%; height: 18px; text-align:center;" colspan="2">             
                <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 67px;" 
                type="button" value="VIEW"   onclick="return btnView_onclick()" />
                &nbsp;&nbsp;
                <input id="cmd_Export_Excel" style="font-family: Cambria; cursor: pointer; width: 87px;" 
                type="button" value="VIEW EXCEL" onclick="return btnExcelView_onclick()" />
                &nbsp;&nbsp;
                <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
            </td>            
        </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;</td>
         <td  style="width:70%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;</td>
        <td  style="width:70%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;</td>
         <td  style="width:70%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="width:30%;">
            &nbsp;
        </td>
         <td  style="width:70%;">
            &nbsp;
        </td>
        <asp:HiddenField ID="hdnValue" runat="server" />
    </tr>
    </table>
    <script language="javascript" type="text/javascript">
     
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }            
    //ViewType = -1 for normal report view
    //ViewType = 1 for excel report view
    function btnView_onclick(){
        var BranchCode=document.getElementById("<%= cmbBranch.ClientID %>").value;
        var From_Date = document.getElementById("<%= txt_From_Dt.ClientID %>").value; 
        var To_Date = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
        var ATM = document.getElementById("<%= cmbAtm.ClientID %>").value;
        var Report = document.getElementById("<%= cmbReportType.ClientID %>").value;
        if(Report == 1){
            window.open("View_Entry_Detail_Report.aspx?ViewType=-1&Branch=0&BranchCode=" + BranchCode + "&From_Date=" + From_Date + " &To_Date=" + To_Date + " &ATM=" + ATM +  " ", "_self");                   
        }else if(Report == 2){
            window.open("View_Entry_FIS_Report.aspx?ViewType=-1&BranchCode=" + BranchCode + "&From_Date=" + From_Date + " &To_Date=" + To_Date + " &ATM=" + ATM +  " ", "_self");                   
        }else{
            alert("Select Report Type");
        }
    }    
   function btnExcelView_onclick(){
        var BranchCode=document.getElementById("<%= cmbBranch.ClientID %>").value;
        var From_Date = document.getElementById("<%= txt_From_Dt.ClientID %>").value; 
        var To_Date = document.getElementById("<%= txt_To_Dt.ClientID %>").value;
        var ATM = document.getElementById("<%= cmbAtm.ClientID %>").value;
        var Report = document.getElementById("<%= cmbReportType.ClientID %>").value;
        if(Report == 1){
            window.open("View_Entry_Detail_Report.aspx?ViewType=1&Branch=0&BranchCode=" + BranchCode + "&From_Date=" + From_Date + " &To_Date=" + To_Date + " &ATM=" + ATM +  " ", "_self");                   
        }else if(Report == 2){
            window.open("View_Entry_FIS_Report.aspx?ViewType=1&BranchCode=" + BranchCode + "&From_Date=" + From_Date + " &To_Date=" + To_Date + " &ATM=" + ATM +  " ", "_self");                   
        }else{
            alert("Select Report Type");
        }
    }      
    function BranchOnChange(){
        var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;
        var SaveData = "1Ø" + Branch;
        ToServer(SaveData ,1); 
    } 
   function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }       
    function FromServer(arg, context) {
        switch (context) {
            case 1:
                {                   
                       ComboFill(arg, "<%= cmbAtm.ClientID %>");
                    break;    
                }               
            case 2:
                {    
                    
                }  
        }
    }

    </script>
</asp:Content>

 