﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CBR_Master.aspx.vb" Inherits="CBR_Master" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">          
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#CF0D0D;          
            cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #F08080 90%, #F1F3F4 100%);          
        }        
        .Button:hover
        {           
             background: -moz-radial-gradient(center, ellipse cover #3CB82E 0%, #3CB82E 0%, #3CB82E 100%);            
            color:#801424;
        }                   
     .bg
     {
         background-color:#FFF;
     }
     .style1
        {
            width: 40%;
            height: 104px;
        }           
    </style>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">       
        window.onload = function () {
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                if(Data[0] == 0){
                    alert(Data[1].toString());
                    BranchCodeOnChange();
                }
            }
            if(context == 2){
                var Data = arg.split("~");
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[1];
                document.getElementById("<%= hidData.ClientID %>").value = Data[2];
                tablefill();
            }
            if(context == 3){
                var Data = arg.split("~");
                var MasterID = Data[1];      
                document.getElementById("<%= hidMasterID.ClientID %>").value = MasterID; 
                       
            }
        }
        function ComboFill(data, ddlName) 
        {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) 
            {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");                
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) 
        {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");       
        }
        function white_space(field)
        {
            if(field.value.length==1){
                field.value = (field.value).replace(' ','');
            }
        }
        function DecimalCheck(e,el)
        {
            var unicode = e.charCode ? e.charCode : e.keyCode;
            if (unicode != 8) {
                if (unicode < 9 || unicode > 9 && unicode < 46 || unicode > 57 || unicode == 47) {
                    if (unicode == 37 || unicode == 38) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (unicode == 46) {
                        var parts = el.value.split('.');
                        if (parts.length == 2)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            else {
                return true;
            }
        }
        function BranchCodeOnChange(){
            document.getElementById("<%= txtAtmID.ClientID %>").value = "";
            document.getElementById("<%= txtLocation.ClientID %>").value = "";
            var BCode = $('#<%= txtBranchCode.ClientID %>').val();
            var ToData = "2Ø" + BCode.toString();
            ToServer(ToData, 2);
        }
        function tablefill(){            
            var data = document.getElementById("<%= hidData.ClientID %>").value.split("Ñ");
            var count = data.length-1;
            if(count>0){
                document.getElementById("trAtm").style.display = "none";
                document.getElementById("trLoc").style.display = "none";
                document.getElementById("btnSave").style.display = "none";
                document.getElementById("<%= pnlAtm.ClientID %>").style.display = '';
                var row_bg = 0;
                var tab = "";
                tab += "<div>";
                tab += "<table style='width:100%'>";
                tab += "<tr>";
                tab += "<td>";
                tab += "<div class='mainhead' style='width:75%; height:auto; padding-top:0px;margin:0px auto;background-color:#EEB8A6;'>";
                tab += "<table id = 'x' style='width:100%; margin:0px auto;font-family:cambria;line-height:20px;background-color:#EEB8A6;' align='center'>";
                tab += "<tr style='width:100%;font-family:cambria;background-color:#EEB8A6;' align='left' class='sec2'>";
                tab += "<td style='text-align:center;width:15%;' class='NormalText'>Sl No</td>";
                tab += "<td style='text-align:center;width:25%;' class='NormalText'>ATM ID</td>";
                tab += "<td style='text-align:center;width:36%;' class='NormalText'>Location</td>";
                tab += "<td style='text-align:center;width:24%;' colspan='2' class='NormalText'>";
                tab += "<input id='btnAdd' style='font-family: cambria; cursor: pointer; width:60%;' type='button' value='ADD NEW' onclick='return btnAdd_onclick("+i+")' />";
                tab += "</td>";
                tab += "</tr>";
                for(var i=0;i<count;i++){
                    var rowData = data[i].toString().split("ÿ");
                    tab += "<tr style='width:100%;font-family:cambria;' class=sub_first align='left'>";
                    tab += "<td style='text-align:center;width:15%;' class='NormalText' id='slno_"+i+"'>"+(i+1)+"</td>";
                    tab += "<td style='text-align:center;width:25%;' class='NormalText' id='AtmID_"+i+"'>"+rowData[1]+"</td>";
                    tab += "<td style='text-align:center;width:36%;' class='NormalText' id='Location_"+i+"'>"+rowData[2]+"</td>";
                    tab += "<td style='text-align:center;width:12%;' class='NormalText' id='Save_"+i+"'>";
                    tab += "<img src='../../image/edit1.png' title='Edit'  style='height:18px;width:18px;cursor:pointer' onclick='return btnEdit_onclick("+i+")' />";
                    tab += "</td>";
                    tab += "<td style='text-align:center;width:12%;' class='NormalText'>";
                    tab += "<img src='../../image/close1.png' title='Remove'  style='height:18px;width:18px;cursor:pointer' onclick='return btnRemove_onclick("+i+")' />";
                    tab += "</td>";
                    tab += "</tr>";
                }
                tab += "</table>";
                tab += "</div>";
                tab += "</td>";
                tab += "</tr>";
                tab += "</table>";
                tab += "</div>";
                document.getElementById("<%= pnlAtm.ClientID %>").innerHTML = tab;
                document.getElementById("tblAtm").style.display = "";
            }
            else{
                document.getElementById("tblAtm").style.display = "none";
                document.getElementById("trAtm").style.display = "";
                document.getElementById("trLoc").style.display = "";
                document.getElementById("btnSave").style.display = "";
            }
        }
        function Saveonclick(){
            if(document.getElementById("<%= txtAtmID.ClientID %>").value == ""){
                alert("Enter ATM ID");
                return false;
            }
            else if(document.getElementById("<%= txtLocation.ClientID %>").value == ""){
                alert("Enter Location");
                return false;
            }
            else{
                var AtmID = document.getElementById("<%= txtAtmID.ClientID %>").value;
                var Location = document.getElementById("<%= txtLocation.ClientID %>").value;  
                var BranchID = document.getElementById("<%= txtBranchCode.ClientID %>").value;
                var data = "1Ø" + 1 + "Ø" + AtmID + "Ø" + Location + "Ø" + BranchID + "Ø" +-1; //1 , because its saving.
                ToServer(data ,1);             
            }
        }
        function btnEdit_onclick(i){
            var AtmID = document.getElementById("AtmID_"+i).innerHTML;
            var BranchID = document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var Location = document.getElementById("Location_"+i).innerHTML;
//            var prevData = AtmID + "" + BranchID + "" + Location;
            document.getElementById("AtmID_"+i).innerHTML = "<input type='text' value='"+AtmID+"' id='AtmIDName_"+i+"'/>";
            document.getElementById("Location_"+i).innerHTML = "<input type='text' value='"+Location+"' id='LocationName_"+i+"'/>";
            document.getElementById("Save_"+i).innerHTML = "<input id='btnSave_"+i+"' style='font-family: cambria; cursor: pointer; width:60%;' type='button' value='SAVE' onclick='return btnSaveAtm_onclick("+i+")' />";
            var data = "3Ø" + AtmID + "Ø" + BranchID; //for getting master id of the atm to be edited.
            ToServer(data ,3);  
        }
        function btnRemove_onclick(i){
            var AtmID = document.getElementById("AtmID_"+i).innerHTML;
            var BranchID = document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var Location = document.getElementById("Location_"+i).innerHTML;
            var data = "1Ø" + 3 + "Ø" + AtmID + "Ø" + Location + "Ø" + BranchID + "Ø" + -1; //3 , because its deleting.
            ToServer(data ,1);  
        }
        function btnAdd_onclick(i){
            document.getElementById("trAtm").style.display = "";
            document.getElementById("trLoc").style.display = "";
            document.getElementById("<%= txtAtmID.ClientID %>").value = "";
            document.getElementById("<%= txtLocation.ClientID %>").value = "";
            document.getElementById("btnSave").style.display = "";
        }
        function btnSaveAtm_onclick(i){
            var AtmID = document.getElementById("AtmIDName_"+i).value;
            var Location = document.getElementById("LocationName_"+i).value;
            var BranchID = document.getElementById("<%= txtBranchCode.ClientID %>").value;
            var MasterID = document.getElementById("<%= hidMasterID.ClientID %>").value;
            var data = "1Ø" + 2 + "Ø" + AtmID + "Ø" + Location + "Ø" + BranchID + "Ø" + MasterID; //2 , because its updating.
            ToServer(data ,1); 
        }
    </script>
</head>
</html>
<br />
<div  style="width:90%; height:auto; margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table class="style1" style="width:80%;margin: 0px auto;height:auto;line-height:35px;">
            <tr> 
                <td style="width:30%;">
                    <asp:HiddenField ID="hidData" runat="server" />
                    <asp:HiddenField ID="hidDenominations" runat="server" />
                    <asp:HiddenField ID="hidBID" runat="server" />
                    <asp:HiddenField ID="hidBName" runat="server" />
                    <asp:HiddenField ID="hidMasterID" runat="server" />
                    <asp:HiddenField ID="EditData" runat="server" />
                </td>
                <td style="width:20%; text-align:left;">
                </td>
                <td style="width:25%">
                </td>
                <td style="width:25%">
                </td>
            </tr>
            <tr> 
                <td style="width:30%;">
                </td>
                <td style="width:20%; text-align:left;">
                    Branch Code
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtBranchCode" class="NormalText" runat="server" Width="87%" onkeypress="NumericCheck(event)" onchange="BranchCodeOnChange()"></asp:TextBox>
                </td>
                <td style="width:25%">
                </td>
            </tr>
            <tr> 
                <td style="width:30%;">
                </td>
                <td style="width:20%; text-align:left;">
                    Branch Name
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtBranchName" class="NormalText" ReadOnly="true" runat="server" Width="87%" ></asp:TextBox>
                </td>
                <td style="width:25%">
                </td>
            </tr>
            <tr id="trAtm" style="display:none;"> 
                <td style="width:30%;">
                </td>
                <td style="width:20%; text-align:left;">
                    ATM ID
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtAtmID" class="NormalText" runat="server" Width="87%" ></asp:TextBox>
                </td>
                <td style="width:25%">
                </td>
            </tr>
            <tr id="trLoc" style="display:none;"> 
                <td style="width:30%;">
                </td>
                <td style="width:20%; text-align:left;">
                    Location
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtLocation" class="NormalText" runat="server" Width="87%" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' MaxLength="100"></asp:TextBox>
                </td>
                <td style="width:25%">
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table ID="tblAtm" style="width:90%;height:90px;margin:0px auto;display:none;">            
            <tr id="Tr1">
                <td style="text-align: center;">
                    <asp:Panel ID="pnlAtm" Style="width: 100%;  text-align: left; float: left;" runat="server">               

                    </asp:Panel>
                </td>
            </tr>
        </table> 
        <br />    
    </div>
    <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
        <div style="text-align:center; height: 63px;"><br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width:8%;display:none" type="button" value="SAVE" onclick="return Saveonclick()"/>       
            &nbsp;
            &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </div>
    </div>
</div>   
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

