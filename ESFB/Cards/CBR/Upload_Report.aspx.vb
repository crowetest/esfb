﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports Excel = Microsoft.Office.Interop.Excel


Partial Class Upload_Report
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DTDATE As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DT4 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As String
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1292) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim UserID As String = CStr(Session("UserID"))
            DT = DB.ExecuteDataSet("SELECT -2 AS branch_id,'--ALL--' AS branch_name UNION ALL SELECT branch_id,branch_name FROM BRANCH_MASTER WHERE Status_ID = 1").Tables(0)
            GF.ComboFill(cmbBranch, DT, 0, 1)
            DT1 = DB.ExecuteDataSet("SELECT -1 AS VALUE,'--ALL--' AS ATM_ID UNION ALL SELECT MASTER_ID,ATM_ID FROM CBR_ATM_MASTER").Tables(0)
            GF.ComboFill(cmbAtm, DT1, 0, 1)
            Me.Master.subtitle = "CBR Upload Report"


            If Not IsPostBack Then
            End If


            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "onload()", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then
            DT1 = DB.ExecuteDataSet("SELECT -1 AS VALUE,'--ALL--' AS ATM_ID UNION ALL SELECT MASTER_ID,ATM_ID FROM CBR_ATM_MASTER WHERE BRANCH_ID = " + Data(1)).Tables(0)
            For Each DR As DataRow In DT1.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
        If CInt(Data(0)) = 2 Then
            Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()
            If xlApp Is Nothing Then
                MsgBox("Excel is not properly installed!!")
                Return
            End If
            Dim xlWorkBook As Excel.Workbook
            Dim xlWorkSheet As Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("Sheet1")
            Dim i As Integer = 0

            Dim Branch As String = Data(1)
            Dim ATM As String = Data(2)
            Dim From_Dt As String = Data(3)
            Dim To_Dt As String = Data(4)
            Dim StrWhere As String = "1=1 "
            Dim StrDate As String = ""
            If Branch <> -2 Then
                StrWhere += "and a.Branch_code =" + Branch
            End If
            If ATM <> -1 Then
                StrWhere += " and a.atm_ID = " + ATM
            End If
            If From_Dt <> "" And To_Dt <> "" Then
                If From_Dt <> To_Dt Then
                    StrDate += " x.Created_Date between CAST('" + From_Dt + "' AS date) and CAST('" + To_Dt + "' AS date)"
                Else
                    StrDate += " CAST(x.Created_Date AS date) = CAST('" + From_Dt + "' AS date)"
                End If
            End If

            xlWorkSheet.Cells(1, 1) = "Date"
            xlWorkSheet.Cells(1, 2) = "ATM ID"
            xlWorkSheet.Cells(1, 3) = "Denomination"
            xlWorkSheet.Cells(1, 4) = "Opening Balance"
            xlWorkSheet.Cells(1, 6) = "Cash Dispense"
            xlWorkSheet.Cells(1, 8) = "Cash In Purge Bin"
            xlWorkSheet.Cells(1, 10) = "Closing Before Replenishment"
            xlWorkSheet.Cells(1, 12) = "Cash Replenishment"
            xlWorkSheet.Cells(1, 14) = "Position After Replenishment"
            xlWorkSheet.Cells(1, 16) = "Overage / Shortage"
            xlWorkSheet.Cells(1, 18) = "GL Before"
            xlWorkSheet.Cells(1, 19) = "GL After"
            xlWorkSheet.Cells(1, 20) = "Terminal Balance"
            xlWorkSheet.Cells(1, 21) = "Admin Balance"
            xlWorkSheet.Cells(1, 22) = "ATM Status"
            xlWorkSheet.Cells(1, 23) = "ATM Amount"

            xlWorkSheet.Range(xlWorkSheet.Cells(1, 4), xlWorkSheet.Cells(1, 5)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 6), xlWorkSheet.Cells(1, 7)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 8), xlWorkSheet.Cells(1, 9)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 10), xlWorkSheet.Cells(1, 11)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 12), xlWorkSheet.Cells(1, 13)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 14), xlWorkSheet.Cells(1, 15)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 16), xlWorkSheet.Cells(1, 17)).Merge()

            xlWorkSheet.Range(xlWorkSheet.Cells(1, 1), xlWorkSheet.Cells(2, 1)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 2), xlWorkSheet.Cells(2, 2)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 3), xlWorkSheet.Cells(2, 3)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 18), xlWorkSheet.Cells(2, 18)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 19), xlWorkSheet.Cells(2, 19)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 20), xlWorkSheet.Cells(2, 20)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 21), xlWorkSheet.Cells(2, 21)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 22), xlWorkSheet.Cells(2, 22)).Merge()
            xlWorkSheet.Range(xlWorkSheet.Cells(1, 23), xlWorkSheet.Cells(2, 23)).Merge()

            xlWorkSheet.Cells(2, 4) = "Count"
            xlWorkSheet.Cells(2, 5) = "Total"
            xlWorkSheet.Cells(2, 6) = "Count"
            xlWorkSheet.Cells(2, 7) = "Total"
            xlWorkSheet.Cells(2, 8) = "Count"
            xlWorkSheet.Cells(2, 9) = "Total"
            xlWorkSheet.Cells(2, 10) = "Count"
            xlWorkSheet.Cells(2, 11) = "Total"
            xlWorkSheet.Cells(2, 12) = "Count"
            xlWorkSheet.Cells(2, 13) = "Total"
            xlWorkSheet.Cells(2, 14) = "Count"
            xlWorkSheet.Cells(2, 15) = "Total"
            xlWorkSheet.Cells(2, 16) = "Count"
            xlWorkSheet.Cells(2, 17) = "Total"

            xlWorkSheet.Range("A1:W1").Font.Bold = True
            xlWorkSheet.Range("A2:W2").Font.Bold = True
            xlWorkSheet.Columns.WrapText = True
            xlWorkSheet.Cells.HorizontalAlignment = Excel.Constants.xlCenter
            xlWorkSheet.Range("A1:W1").Interior.ColorIndex = 15
            xlWorkSheet.Range("A2:W2").Interior.ColorIndex = 15
            xlWorkSheet.Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous
            xlWorkSheet.Columns("A").ColumnWidth = 15

            If StrDate <> "" Then
                DTDATE = DB.ExecuteDataSet("select distinct CAST(x.created_date AS date) from cbr_entry_master x where " + StrDate).Tables(0)
            Else
                DTDATE = DB.ExecuteDataSet("select distinct CAST(x.created_date AS date) from cbr_entry_master x").Tables(0)
            End If
            Dim j As Integer = 3
            For n As Integer = 0 To DTDATE.Rows.Count - 1
                Dim createdDate As String = DTDATE(n)(0).ToString()
                DT2 = DB.ExecuteDataSet("select a.Created_Date,c.ATM_ID,a.entry_id,a.ShortOrOverBit,a.GL_Before,a.GL_After,a.Terminal_Bal,a.Admin_Bal,a.Atm_Status,a.Atm_Amount from cbr_entry_master a inner join cbr_atm_master c on a.atm_id = c.master_id where " + StrWhere + " and CAST(a.created_date AS date) = CAST('" + createdDate + "' AS date)").Tables(0)
                For m As Integer = 0 To DT2.Rows.Count - 1
                    Dim k As Integer = j
                    xlWorkSheet.Cells(k, 1) = CDate(DT2(m)(0).ToString())
                    xlWorkSheet.Cells(k, 2) = DT2(m)(1).ToString()
                    xlWorkSheet.Cells(k, 18) = DT2(m)(4).ToString()
                    xlWorkSheet.Cells(k, 19) = DT2(m)(5).ToString()
                    xlWorkSheet.Cells(k, 20) = DT2(m)(6).ToString()
                    xlWorkSheet.Cells(k, 21) = DT2(m)(7).ToString()
                    xlWorkSheet.Cells(k, 22) = DT2(m)(8).ToString()
                    xlWorkSheet.Cells(k, 23) = DT2(m)(9).ToString()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 1), xlWorkSheet.Cells(k + 1, 1)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 2), xlWorkSheet.Cells(k + 1, 2)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 18), xlWorkSheet.Cells(k + 1, 18)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 19), xlWorkSheet.Cells(k + 1, 19)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 20), xlWorkSheet.Cells(k + 1, 20)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 21), xlWorkSheet.Cells(k + 1, 21)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 22), xlWorkSheet.Cells(k + 1, 22)).Merge()
                    xlWorkSheet.Range(xlWorkSheet.Cells(k, 23), xlWorkSheet.Cells(k + 1, 23)).Merge()
                    DT1 = DB.ExecuteDataSet("select a.denomination,a.ob_count,a.ob_total,a.cd_count,a.cd_total,a.cpb_count,a.cpb_total,a.cbr_count,a.cbr_total,a.cr_count,a.cr_total,a.par_count,a.par_total,a.ShortageOrOverage_Count,a.ShortageOrOverage_total from cbr_entry_sub a where a.master_id = " + DT2(m)(2).ToString()).Tables(0)
                    For Each DR In DT1.Rows
                        xlWorkSheet.Cells(j, 3) = DR(0).ToString()
                        xlWorkSheet.Cells(j, 4) = DR(1).ToString()
                        xlWorkSheet.Cells(j, 5) = DR(2).ToString()
                        xlWorkSheet.Cells(j, 6) = DR(3).ToString()
                        xlWorkSheet.Cells(j, 7) = DR(4).ToString()
                        xlWorkSheet.Cells(j, 8) = DR(5).ToString()
                        xlWorkSheet.Cells(j, 9) = DR(6).ToString()
                        xlWorkSheet.Cells(j, 10) = DR(7).ToString()
                        xlWorkSheet.Cells(j, 11) = DR(8).ToString()
                        xlWorkSheet.Cells(j, 12) = DR(9).ToString()
                        xlWorkSheet.Cells(j, 13) = DR(10).ToString()
                        xlWorkSheet.Cells(j, 14) = DR(11).ToString()
                        xlWorkSheet.Cells(j, 15) = DR(12).ToString()
                        xlWorkSheet.Cells(j, 16) = DR(13).ToString()
                        xlWorkSheet.Cells(j, 17) = DR(14).ToString()
                        j += 1
                    Next
                Next
            Next
            'Dim today = CDate(DT.Rows(0)(0).ToString())
            Dim today As Date = Date.Today()
            Dim filename = "C:\Users\user\Downloads\CBRReport" + today.ToString("ddMMMyyyy") + ".xls"

            xlWorkBook.SaveAs(filename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, _
             Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
            xlWorkBook.Close(True, misValue, misValue)
            xlApp.Quit()

            releaseObject(xlWorkSheet)
            releaseObject(xlWorkBook)
            releaseObject(xlApp)

            CallBackReturn = "Excel file created , you can find the file in downloads"
        End If
    End Sub
#End Region
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
End Class

