﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CardRequest.aspx.vb" Inherits="ResetPassword" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function btnSave_onclick() {
        if (document.getElementById("<%= txtAccountNo.ClientID %>").value == "" && document.getElementById("<%= txtCIF.ClientID %>").value == "") 
        {
            alert("Either Account No or CIF is Mandatory");
            document.getElementById("<%= txtAccountNo.ClientID %>").focus();
            return false;
        }
        if(document.getElementById("<%= txtAccountNo.ClientID %>").value != "")
        {
            var len = document.getElementById("<%= txtAccountNo.ClientID %>").value.length
            if (len!=14)
            {
                alert("Enter Correct Account No");
                document.getElementById("<%= txtAccountNo.ClientID %>").focus();
                return false;
            }
        }
        if(document.getElementById("<%= txtCIF.ClientID %>").value != "")
        {
            var len = document.getElementById("<%= txtCIF.ClientID %>").value.length
            if (len!=12)
            {
                alert("Enter Correct CIF");
                document.getElementById("<%= txtCIF.ClientID %>").focus();
                return false;
            }
        }
        if(document.getElementById("<%= txtName.ClientID %>").value == "")
        {
            alert("Enter Customer Name");
            document.getElementById("<%= txtName.ClientID %>").focus();
            return false;
        }
        if(document.getElementById("<%= fupUpload.ClientID %>").value == "")
        {
            alert("Attach Application Form");
            document.getElementById("<%= fupUpload.ClientID %>").focus();
            return false;
        }
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    </script>
   </head>
</html>
<br />
    <asp:HiddenField ID="hid_data" runat="server" />
    <br />

 <table  style="width:80%;margin: 0px auto;">
        <tr>
        <td style="width:30%;">&nbsp;</td>
         <td style="width:15%; text-align:left;">Request For</td>
            <td style="width:55%">
                &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlRequestType" class="NormalText" runat="server" Width="50%">
                </asp:DropDownList>
            </td>


       </tr>
        <tr>
        <td style="width:30%;"></td>
         <td style="width:15%; text-align:left;">Account No</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAccountNo" class="NormalText" onkeypress='return NumericCheck(event)' runat="server" Width="50%" 
                MaxLength="14"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:30%;"></td>
       <td style="width:15%; text-align:left;">CIF</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCIF" class="NormalText"  onkeypress='return NumericCheck(event)' runat="server" Width="50%" 
                MaxLength="12"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:30%;">&nbsp;</td>
       <td style="width:15%; text-align:left;">Customer Name</td>
            <td style="width:55%">
                &nbsp;&nbsp;
                <asp:TextBox ID="txtName" class="NormalText" runat="server"  onkeypress='return AlphaNumericCheck(event)' Width="50%" 
                MaxLength="50"></asp:TextBox>
                &nbsp;</td>


       </tr>
       <tr> 
       <td style="width:30%;"></td>
       <td style="width:15%; text-align:left;">Attachment</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:FileUpload ID="fupUpload" runat="server" Height="20px" 
                    Width="50%" />
            </td>


       </tr>
       <tr> 
       <td style="width:30%;">&nbsp;</td>
       <td style="width:15%; text-align:left;">Remarks if any</td>
            <td style="width:55%">
                &nbsp;&nbsp;
                <asp:TextBox ID="txtRemarks" class="NormalText" runat="server"  
                    onkeypress='return AlphaNumericCheck(event)' Width="50%" 
                MaxLength="300" TextMode="MultiLine"></asp:TextBox>
                &nbsp;</td>
           <a href="../Business/PDD/PDD_Team_Master.aspx">../Business/PDD/PDD_Team_Master.aspx</a>


       </tr>
       <tr> <td style="width:30%;"></td>
       <td style="width:15%; text-align:left;">&nbsp;</td>
            <td style="width:55%">
                &nbsp; &nbsp;</td>


       </tr>
       
       <tr>
            <td style="text-align:center;" colspan="3"><br />
                 <asp:Button ID="btnSave" runat="server" Text="SAVE" style="font-family: cambria; cursor: pointer; width: 67px;"/>
                &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

