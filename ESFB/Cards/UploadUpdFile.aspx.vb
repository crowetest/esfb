﻿Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Partial Class Cards_UploadUpdFile
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Upload CAF/UPD File"
        Me.btnSave.Attributes.Add("onclick", "return showLoading()")
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.ListBox1.Items.Clear()
        Dim FilePath As String
        Dim FileName As String
        If Me.FileUpload1.HasFile = False Then
            Dim cl_script0 As New System.Text.StringBuilder
            cl_script0.Append("         alert('Please Browse CAF File');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "clientscript", cl_script0.ToString, True)
            Exit Sub
        Else
            FileName = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName)
            FileUpload1.SaveAs(Server.MapPath("" + FileName))
            FilePath = Server.MapPath("" + FileName)
        End If
        ReadTextFile(FilePath) '--//-- Reading Data From Text File Saved in Mentioned Path
        SaveData(FileName) '--//-- Saving Data into DB from Datatable Generated from Text File

        File.Delete(FilePath)
    End Sub
    Private Sub ReadTextFile(ByVal FilePath As String)
        Dim Data As String = Nothing
        DT.Columns.Add("RowData", Type.GetType("System.String"))
        Using Reader As New Microsoft.VisualBasic.FileIO.TextFieldParser(FilePath)
            Reader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.FixedWidth
            Reader.SetFieldWidths(6, 4, 19, 2, 4, 125, 25, 25, 1, 35, 140, 12, 20, 35, 10, 10, 10, 3, 5, 1, 6, 14, 19, 1, 4, 10, 12, 10, 35, 35, 12, 1, -1)
            'CARD_BIN ~ BRANCH_CODE ~ ACCOUNT_NO ~ ACCOUNT_TYPE ~ ACCOUNT_BRANCH_CODE ~ BLANK DATA ~ EMB_NAME ~ CUST_NAME ~ GENDER ~ ADDRESS1 ~ BLANK ADDRESS ~ MOBILE_NO ~ RES_NO ~ EMAIL ~ REG_DT ~ APP_DT ~ CUST_ID ~ SUB_PROD_CODE ~ ATM_LIMIT ~ CARD_TYPE ~ APP_BRANCH_CODE ~ INDENT_NO ~ CARD_NO ~ ACTION_CODE ~ CARD_TYPE ~ CARD_EXPIRY_DT ~ ADHAAR ~ CARD_ISSUE_DT ~ FORUTH_DATA ~ BRANCH_NAME ~ CARD_PIN ~ PHOTO_CARD
            Dim currentRow As String()
            Dim RowID As Integer = 0
            While Not Reader.EndOfData
                Try
                    currentRow = Reader.ReadFields()
                    Dim DateField As Date
                    Dim RegDt As String = Nothing
                    Dim AppDt As String = Nothing
                    Dim IssueDt As String = Nothing
                    Dim ExpDt As String = Nothing
                    If currentRow(14).Length > 8 Then
                        DateField = currentRow(14).Substring(3, 3) + currentRow(14).Substring(0, 3) + currentRow(14).Substring(6, 4)
                        RegDt = Format(DateField, "dd/MMM/yyyy")
                    End If
                    If currentRow(15).Length > 8 Then
                        DateField = currentRow(15).Substring(3, 3) + currentRow(15).Substring(0, 3) + currentRow(15).Substring(6, 4)
                        AppDt = Format(DateField, "dd/MMM/yyyy")
                    End If
                    If currentRow(25).Length > 8 Then
                        DateField = currentRow(25).Substring(3, 3) + currentRow(25).Substring(0, 3) + currentRow(25).Substring(6, 4)
                        ExpDt = Format(DateField, "dd/MMM/yyyy")
                    End If
                    If currentRow(27).Length > 8 Then
                        DateField = currentRow(27).Substring(3, 3) + currentRow(27).Substring(0, 3) + currentRow(27).Substring(6, 4)
                        IssueDt = Format(DateField, "dd/MMM/yyyy")
                    End If
                    Data = currentRow(0) + "~" + currentRow(1) + "~" + currentRow(2) + "~" + currentRow(3) + "~" + currentRow(4) + "~" + currentRow(6) + "~" + currentRow(7) + "~" + currentRow(8) + "~" + currentRow(9) + "~" + currentRow(11) + "~" + currentRow(13) + "~" + RegDt + "~" + AppDt + "~" + currentRow(16) + "~" + currentRow(17) + "~" + currentRow(19) + "~" + currentRow(20) + "~" + currentRow(21) + "~" + currentRow(22) + "~" + IssueDt + "~" + ExpDt + "~" + currentRow(30) + "~" + currentRow(31)
                    Dim DR As DataRow = DT.NewRow
                    DR("RowData") = Data
                    DT.Rows.Add(DR)
                    'Me.ListBox1.Items.Add("Row " + (RowID + 1).ToString + " Fetching Completed...")
                    Me.ListBox1.SelectedIndex = Me.ListBox1.Items.Count - 1
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    Me.ListBox1.Items.Add("Line " & (RowID + 1).ToString & "is not valid and will be skipped.")

                End Try
                RowID += 1
            End While
        End Using
        'Me.ListBox1.Items.Add("Fetching Completed.")
        Me.ListBox1.SelectedIndex = Me.ListBox1.Items.Count - 1
    End Sub
    Private Sub SaveData(ByVal FileName As String)
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Try
            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@FileName", SqlDbType.VarChar, 30)
            Params(0).Value = FileName
            Params(1) = New SqlParameter("@Data", DT)
            Params(2) = New SqlParameter("@UserID", SqlDbType.Int)
            Params(2).Value = CInt(Session("UserID"))
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
            Params(4).Direction = ParameterDirection.Output
            DB.ExecuteDataSetLongTime("SP_CAF_FILE_UPLOAD", Params)
            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)

        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
        End Try
        If ErrorFlag = 0 Then
            Me.ListBox1.Items.Add(Message.ToString())
            Me.ListBox1.SelectedIndex = Me.ListBox1.Items.Count - 1
        Else
            Me.ListBox1.Items.Add("Error in Posting !!!")
            Me.ListBox1.Items.Add(Message)
            Me.ListBox1.ForeColor = Drawing.Color.Red
            Me.ListBox1.SelectedIndex = Me.ListBox1.Items.Count - 1
        End If
    End Sub
End Class
