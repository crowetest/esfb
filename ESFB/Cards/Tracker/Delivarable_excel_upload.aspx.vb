﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports System.Net
Imports System.Text
Imports ClosedXML.Excel
'Imports System.IDisposable
Partial Class DelivarableUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DTS As New DataTable


#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 102) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "Deliverables Upload"
            If Not IsPostBack Then
                Me.txtUploadDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            btnUpload.Attributes.Add("onclick", "return UploadOnClick()")

            DT = DB.ExecuteDataSet("select -1,'---Select---' item_id union all select  item_id,item_name from Cards_item_Master where status_id=1 order by item_id").Tables(0)
            GN.ComboFill(CmbType, DT, 0, 1)
            DTS = DB.ExecuteDataSet("select -1,'---Select---'Category_id union all select Category_id,Category_name  from Cards_deliverable_Master ").Tables(0)
            GN.ComboFill(cmbCategory, DTS, 0, 1)
            Me.CmbType.Attributes.Add("onchange", "return ItemOnChange()")
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnChange()")
            'DT = DB.ExecuteDataSet("select distinct Cat_id,Cat_name from Card_Type_Master").Tables(0)
            If DT.Rows.Count > 0 Then
                'cmbType0.DataSource = DT
                'cmbType0.DataTextField = "Cat_name"
                'cmbType0.DataValueField = "Cat_id"
                'cmbType0.DataBind()
                'cmbType0.Items.Insert(0, New ListItem("---Select---"))

                'cmbType0.SelectedIndex = 0
            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then 'category combofil
            'Dim Type_id = CInt(Data(1))
            'Dim DTS As New DataTable
            'DTS = DB.ExecuteDataSet("select -1,'---Select---'Category_id union all select Category_id,Category_name  from Cards_deliverable_Master where Type_id='" & Type_id & "'").Tables(0)
            'Dim StrData As String = Nothing
            ''    Dim Packingdate As String = Nothing
            ''    Dim courierdate As String = Nothing
            ''    DT = DB.ExecuteDataSet("select max(convert (varchar(10),Date_of_upload,111)) from card_courier_master where no_of_attachment=1").Tables(0)
            ''    StrData = CDate(CStr(DT.Rows(0)(0))).ToString("dd/MM/yyyy")
            ''    DT1 = DB.ExecuteDataSet("select max(convert (varchar(10),Date_of_upload,23)) from card_courier_master where no_of_attachment=2").Tables(0)
            ''    courierdate = CDate(CStr(DT1.Rows(0)(0))).ToString("dd/MM/yyyy")
            ''    'StrData = ("select max(CAST(Date_of_upload AS DATE)) from card_courieer_master where no_of_attachment=1")
            ''    'courierdate = CDate(("select max(CAST(Date_of_upload AS DATE)) from card_courieer_master where no_of_attachment=2"))
            'For Each DR As DataRow In DTS.Rows
            '    CallBackReturn += "Ř" + DR(0).ToString() + "Ĉ" + DR(1).ToString()

            'Next
            'StrData += CInt(DTS(0)(0)).ToString + "¥" + CStr(DTS(0)(1)).ToString
            'CallBackReturn = StrData.ToString
        End If
        If CInt(Data(0)) = 2 Then 'category combofil
            'Dim Type_ID As Integer = CInt(Data(1).ToString)
            'Dim Cat_ID As Integer = CInt(Data(2).ToString)
            'If fileUpload.FileName.ToString() <> String.Empty Then
            '    Dim filepath As String = Server.MapPath("~/") + "Cards/Tracker/TrackerFiles/" + fileUpload.FileName
            '    fileUpload.PostedFile.SaveAs(filepath)
            '    DT = GetExcelData(filepath)
            'End If
        End If
    End Sub
#End Region
#Region "upload"
    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Dim conStr As String = ""
        Dim myFile As HttpPostedFile = fileUpload.PostedFile
        Dim nFileLen As Integer = myFile.ContentLength
        Dim FileName As String = ""

        Dim Type_ID As String = CStr(hidType.Value)
        Dim Category_ID As String = CStr(hidCategory.Value)
        Dim Upload_Dt As String = CStr(hidUpload_date.Value)
        If (Type_ID = "-1" Or Type_ID = "") Then
            ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "alert('Select Type to continue..');", True)
        ElseIf (Category_ID = "-1" Or Category_ID = "") Then
            ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "alert('Select Category to continue..');", True)
        ElseIf (nFileLen = 0) Then
            ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "alert('Select File  to Upload..');", True)
        Else
            If fileUpload.FileName.ToString() <> String.Empty Then
                Dim filepath As String = Server.MapPath("~/") + "Cards/Tracker/TrackerFiles/" + fileUpload.FileName
                fileUpload.PostedFile.SaveAs(filepath)
                DT = GetExcelData(filepath)
            End If
        End If
        Dim ErrorFlag As Integer = 0
        Dim Message As String = Nothing
        Dim SearchID As Integer = 0
        Dim UserID As Integer = CInt(Session("UserID"))

        Try
           

            Dim Params(5) As SqlParameter
            Params(0) = New SqlParameter("@Type_ID", SqlDbType.VarChar, 20)
            Params(0).Value = Type_ID
            Params(1) = New SqlParameter("@Category_ID", SqlDbType.VarChar, 1000)
            Params(1).Value = Category_ID
            Params(2) = New SqlParameter("@Courier_dtl", SqlDbType.Structured)
            Params(2).Value = DT
            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 2000)
            Params(4).Direction = ParameterDirection.Output
            Params(5) = New SqlParameter("@Upload_Dt", SqlDbType.VarChar, 1000)
            Params(5).Value = Upload_Dt

            DB.ExecuteNonQuery("[SP_Cards_deliverables_upload]", Params)


            ErrorFlag = CInt(Params(3).Value)
            Message = CStr(Params(4).Value)
            If ErrorFlag = 0 Then
                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "alert('Data Uploaded Successfully..');", True)
            End If
        Catch ex As Exception
            Message = ex.Message.ToString
            ErrorFlag = 1
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString(), False)
        End Try

    End Sub
#End Region
    Private Function GetExcelData(ByVal filepath As String) As DataTable
        Dim oledbConnectionString As String = String.Empty
        Dim conn As OleDbConnection = Nothing
        oledbConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & filepath & "; Extended Properties='Excel 8.0;HDR=NO;IMEX=1';"
        conn = New OleDbConnection(oledbConnectionString)

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim command As OleDbCommand = New OleDbCommand("Select * from [Sheet1$]", conn)
        Dim objAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        objAdapter.SelectCommand = command
        Dim objDataset As DataSet = New DataSet()
        objAdapter.Fill(objDataset, "ExcelDataTable")
        conn.Close()
        Return objDataset.Tables(0)
    End Function


    '    Protected Sub ImportExcel(sender As Object, e As EventArgs)
    '        'Open the Excel file using ClosedXML.
    '        Using workBook As New XLWorkbook(fileUpload.PostedFile.InputStream)
    '            'Read the first Sheet from Excel file.
    '            Dim workSheet As IXLWorksheet = workBook.Worksheet(1)

    '            'Create a new DataTable.
    '            Dim dt As New DataTable()

    '            'Loop through the Worksheet rows.
    '            Dim firstRow As Boolean = True
    '            For Each row As IXLRow In workSheet.Rows()
    '                'Use the first row to add columns to DataTable.
    '                If firstRow Then
    '                    For Each cell As IXLCell In row.Cells()
    '                        dt.Columns.Add(cell.Value.ToString())
    '                    Next
    '                    firstRow = False
    '                Else
    '                    'Add rows to DataTable.
    '                    dt.Rows.Add()
    '                    Dim i As Integer = 0
    '                    For Each cell As IXLCell In row.Cells()
    '                        dt.Rows(dt.Rows.Count - 1)(i) = cell.Value.ToString()
    '                        i += 1
    '                    Next
    '                End If

    '                'GridView1.DataSource = dt
    '                'GridView1.DataBind()
    '            Next
    '        End Using
    '    End Sub

    '#End Region
    '#Region "Save"
    '    Private Sub SaveData(ByVal FileName As String)
    '        Dim ErrorFlag As Integer = 0
    '        Dim Message As String = Nothing
    '        Dim Data As String = hidData.Value
    '        Dim Deliverabl_Date As Date = Nothing
    '        'Dim year_id As Integer = cmbYear.SelectedIndex
    '        'Dim month_id As Integer = cmbMonth.SelectedIndex
    '        Try
    '            Dim Params(3) As SqlParameter
    '            Params(0) = New SqlParameter("@EmpDtl", SqlDbType.VarChar)
    '            Params(0).Value = Data
    '            Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
    '            Params(1).Value = CInt(Session("UserID"))
    '            Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '            Params(2).Direction = ParameterDirection.Output
    '            Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '            Params(3).Direction = ParameterDirection.Output
    '            'Params(4) = New SqlParameter("@YEAR_ID", SqlDbType.Int)
    '            'Params(4).Value = year_id
    '            'Params(5) = New SqlParameter("@MONTH_ID", SqlDbType.Int)
    '            'Params(5).Value = month_id

    '            'DB.ExecuteNonQuery("[SP_PAYSLIP_FILE_UPLOAD]", Params)
    '            ErrorFlag = CInt(Params(2).Value)
    '            Message = CStr(Params(3).Value)
    '        Catch ex As Exception
    '            Message = ex.Message.ToString
    '            ErrorFlag = 1
    '        End Try
    '        If ErrorFlag = 0 Then
    '            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '            cl_script1.Append("         alert('" + Message + "');")
    '            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '        Else
    '            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '            cl_script1.Append("         alert('" + Message + "');")
    '            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '        End If
    '    End Sub
    '#End Region
    'Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    '    Try
    '        hdnValue.Value += "Ø" + Session("UserID").ToString()

    '        Dim ContentType As String = ""
    '        Dim hfc As HttpFileCollection
    '        hfc = Request.Files
    '        Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
    '        'If nFileLen > 0 Then
    '        '    Dim Ftp As New FtpClass
    '        '    Ftp.DownloadFile()

    '        'End If
    '        Dim AttachImg As Byte() = Nothing
    '        Dim UploadDate As Date = CDate(Data(1))
    '        Dim SpeedPost As Integer = CInt(Data(2))
    '        Dim Courier As Integer = CInt(Data(3))
    '        Dim Category As Integer = CInt(Data(4))
    '        Dim SpeedPost1 As Integer = CInt(Data(2))
    '        Dim Courier1 As Integer = CInt(Data(3))

    '        Dim UserID As Integer = CInt(Data(5))
    '        Dim Message As String = Nothing
    '        Dim ErrorFlag As Integer = 0
    '        Dim NoofAttachments As Integer = 0
    '        Dim CourierID1 As Integer = 0
    '        Dim CourierID2 As Integer = 0
    '        Dim CourierID As Integer = 0
    '        Dim Type As Integer = 0
    '        Dim Typec As Integer = 0
    '        Dim Typep As Integer = 0
    '        ' Dim EditFlag As Integer
    '        'Dim EditFlag1 As Integer
    '        'Dim EditFlag2 As Integer
    '        If hfc.Count > 0 Then
    '            For i = 0 To hfc.Count - 1
    '                Dim myFile As HttpPostedFile = hfc(i)
    '                Dim nFileLen As Integer = myFile.ContentLength
    '                Dim FileName As String = ""

    '                NoofAttachments = 1
    '                If (nFileLen > 0) Then
    '                    NoofAttachments += 1
    '                End If
    '            Next
    '        End If
    '        If SpeedPost = 1 Then
    '            Typep = 2
    '            SpeedPost = 0
    '        End If
    '        If Courier = 1 Then
    '            Typec = 1
    '            Courier = 0
    '        End If
    '        If (NoofAttachments = 0) Then
    '            Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '            cl_srpt1.Append("alert('Please Attach the file');")
    '            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
    '            Exit Sub
    '        End If

    '        Try
    '            Dim Params(8) As SqlParameter
    '            Params(0) = New SqlParameter("@UploadDate", SqlDbType.DateTime)
    '            Params(0).Value = UploadDate
    '            Params(1) = New SqlParameter("@CourierId1", SqlDbType.Int)
    '            Params(1).Direction = ParameterDirection.Output
    '            Params(2) = New SqlParameter("@CourierId2", SqlDbType.Int)
    '            Params(2).Direction = ParameterDirection.Output
    '            Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
    '            Params(3).Direction = ParameterDirection.Output
    '            Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
    '            Params(4).Direction = ParameterDirection.Output
    '            Params(5) = New SqlParameter("@UserID", SqlDbType.Int)
    '            Params(5).Value = UserID
    '            'Params(6) = New SqlParameter("@EditFlag1", SqlDbType.Int)
    '            'Params(6).Direction = ParameterDirection.Output
    '            'Params(7) = New SqlParameter("@EditFlag2", SqlDbType.Int)
    '            'Params(7).Direction = ParameterDirection.Output
    '            Params(6) = New SqlParameter("@TypeFlagc", SqlDbType.Int)
    '            Params(6).Value = Typec
    '            Params(7) = New SqlParameter("@TypeFlagp", SqlDbType.Int)
    '            Params(7).Value = Typep
    '            Params(8) = New SqlParameter("@CatFlag", SqlDbType.Int)
    '            Params(8).Value = Category
    '            DB.ExecuteNonQuery("SP_Courier_Dispatch", Params)
    '            CourierID1 = CInt(Params(1).Value)
    '            CourierID2 = CInt(Params(2).Value)
    '            ErrorFlag = CInt(Params(3).Value)
    '            Message = CStr(Params(4).Value)
    '            'EditFlag1 = CInt(Params(6).Value)
    '            'EditFlag2 = CInt(Params(7).Value)
    '            If (CourierID1 > 0 Or CourierID2 > 0) And hfc.Count > 0 Then
    '                For i = 0 To hfc.Count - 1
    '                    Dim myFile As HttpPostedFile = hfc(i)
    '                    Dim nFileLen As Integer = myFile.ContentLength
    '                    Dim FileName As String = ""
    '                    If Courier1 = 1 Then
    '                        Type = 1
    '                        'EditFlag = EditFlag1
    '                        CourierID = CourierID1
    '                        Courier1 = 0
    '                    ElseIf SpeedPost1 = 1 Then
    '                        Type = 2
    '                        ' EditFlag = EditFlag2
    '                        CourierID = CourierID2
    '                        SpeedPost1 = 0
    '                    End If
    '                    If (nFileLen > 0) Then
    '                        ContentType = myFile.ContentType
    '                        FileName = myFile.FileName
    '                        AttachImg = New Byte(nFileLen - 1) {}
    '                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
    '                        Dim Params1(5) As SqlParameter
    '                        Params1(0) = New SqlParameter("@CourierId", SqlDbType.Int)
    '                        Params1(0).Value = CourierID
    '                        Params1(1) = New SqlParameter("@attachment", SqlDbType.VarBinary)
    '                        Params1(1).Value = AttachImg
    '                        Params1(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
    '                        Params1(2).Value = ContentType
    '                        Params1(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
    '                        Params1(3).Value = FileName
    '                        Params1(4) = New SqlParameter("@TypeFlag", SqlDbType.Int)
    '                        Params1(4).Value = Type
    '                        'Params1(5) = New SqlParameter("@EditFlag", SqlDbType.Int)
    '                        'Params1(5).Value = EditFlag
    '                        Params1(5) = New SqlParameter("@CatFlag", SqlDbType.Int)
    '                        Params1(5).Value = Category

    '                        DB.ExecuteNonQuery("[SP_courier_dispatch_ATTACH]", Params1)
    '                    End If
    '                Next
    '            End If

    '        Catch ex As Exception
    '            Message = ex.Message.ToString
    '            ErrorFlag = 1
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
    '        End Try

    '        Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
    '        cl_script1.Append("         alert('" + Message + "');")
    '        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
    '    Catch ex As Exception
    '        If Response.IsRequestBeingRedirected Then
    '            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
    '            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
    '        End If


    '    End Try



    'End Sub
    '#End Region


    'Protected Sub ChkbxCourier_CheckedChanged(sender As Object, e As System.EventArgs) Handles ChkbxCourier.CheckedChanged


    '        'Else
    '        '    cmbType0.SelectedIndex = 0
    '        '    cmbType0.Enabled = False
    '    If ChkbxCourier.Checked = True Then
    '        cmbType0.Enabled = True
    '    End If

    '    'cmbType0.SelectedIndex = 0
    'End Sub

    'Protected Sub ChkbxSpeed_CheckedChanged(sender As Object, e As System.EventArgs) Handles ChkbxSpeed.CheckedChanged
    '    If ChkbxSpeed.Checked = True Then
    '        cmbType0.Enabled = False
    '    Else
    '        cmbType0.Enabled = True
    '    End If
    'End Sub
End Class
