﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="TrackExcelUpload.aspx.vb" Inherits="TrackExcelUpload" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
  
        function UploadOnClick() {

            var UploadDate = document.getElementById("<%= txtUploadDt.ClientID %>").value;

             if (document.getElementById("<%= txtUploadDt.ClientID %>").value == "") {
                alert("Please select date ");
                return false;
            }

            if (document.getElementById("<%= ChkbxSpeed.ClientID %>").checked == false) {
                var SpeedPost = 0;
            }
            else {
                var SpeedPost = 1;
            }
            if (document.getElementById("<%= ChkbxCourier.ClientID %>").checked == false) {
                var Courier = 0;
            }
            else {
                var Courier = 1;
            }
            if (SpeedPost == 0 && Courier == 0)
            {
             alert("select any one  type of medium");
             return false;
         }
         
         var filespeed = document.getElementById("<%= fileSpeed.ClientID %>");
         if((filespeed.files.length == 0 ) && (SpeedPost==1))
         { 
               alert("No file selected for Packing Slip");
                return false;
           } 
         if((filespeed.files.length != 0 ) && (SpeedPost==0))
         { 
               alert("Please select Packing Slip");
                return false;
           } 


         var filecourier = document.getElementById("<%= FileCourier.ClientID %>");
         if((filecourier.files.length == 0 ) && (Courier==1))
         { 
               alert("No file selected for Courier");
                return false;
           } 
           if((filecourier.files.length != 0 ) && (Courier==0))
         { 
               alert("Please select courier");
                return false;
           }

            document.getElementById("<%= hdnValue.ClientID %>").value = "1Ø" + UploadDate + "Ø" + SpeedPost + "Ø" + Courier;       

 } 
 
    function FromServer(arg, context) 
    {             
        if (context == 1) 
        {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0)
            {
       
            }        
        }
        else if (context == 2)
        {    
            pnlDisplay(arg);
        }
        else if (context == 3)
        {    
         
//            alert("Deleted Successfully");
//            window.open("Catchment_Area_Pincode.aspx", "_self"); 
        }

    }  
 

        
         function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }


   
    </script>
   
</head>
</html>
<br />
    <asp:HiddenField ID="hdnValue" runat="server" />
    <br />

 <table class="style1" style="width:50%;text-align:center;margin: 0px auto;">
    <tr>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <td style="width:20%; text-align:center;Font-Size:11pt">Date of upload</td>
            <td style="width:20%">
                &nbsp; &nbsp;<asp:TextBox ID="txtUploadDt" class="NormalText" runat="server" 
                    Width="45%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtUploadDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtUploadDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
            </td>
    </tr>

        <tr>
         <td style="width:20%; text-align:left;">
         &nbsp; &nbsp;<asp:CheckBox ID="ChkbxCourier" style="text-align:center;padding-left:126px;" runat="server" Font-Names="Times New Roman"
                    Font-Size="11pt" Text="Courier" Width="21%" />
                     <div id="DivCourier" style="text-align:right;padding-left:90px;" >
                    <asp:FileUpload ID="FileCourier" runat="server" CssClass="fileUpload" /><br />
                </div></td>
            <td style="width:20%">
                &nbsp; &nbsp;<asp:CheckBox ID="ChkbxSpeed" style="text-align:center;display:initial;" runat="server" Font-Names="Times New Roman"
                    Font-Size="11pt" Text="Packing Slip" Width="35%" />
                     <div id="DivSpeed" style="text-align:left;">
                    <asp:FileUpload ID="fileSpeed" style="padding-left:6px;" runat="server" CssClass="fileUpload" /><br />
                </div>
            </td>

       </tr>

       
       <tr>
            <td style="text-align:center;" colspan="2"><br />
                 <asp:button ID="btnUpload" runat="server"  text="UPLOAD"/>&nbsp;&nbsp; &nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 </td>
        </tr>
 

        <asp:HiddenField ID="hid_OTP" runat="server" />
        <asp:HiddenField ID="hidStringData" runat="server" />

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

</asp:Content>

