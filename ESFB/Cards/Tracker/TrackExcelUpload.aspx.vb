﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Text
Partial Class TrackExcelUpload
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim GN As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable


#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 102) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "Card Tracking"
            If Not IsPostBack Then
                Me.txtUploadDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            btnUpload.Attributes.Add("onclick", "return UploadOnClick()")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        'If CInt(Data(0)) = 1 Then 'document display
        '    Dim StrData As String = Nothing
        '    Dim Packingdate As String = Nothing
        '    Dim courierdate As String = Nothing
        '    DT = DB.ExecuteDataSet("select max(convert (varchar(10),Date_of_upload,111)) from card_courier_master where no_of_attachment=1").Tables(0)
        '    StrData = CDate(CStr(DT.Rows(0)(0))).ToString("dd/MM/yyyy")
        '    DT1 = DB.ExecuteDataSet("select max(convert (varchar(10),Date_of_upload,23)) from card_courier_master where no_of_attachment=2").Tables(0)
        '    courierdate = CDate(CStr(DT1.Rows(0)(0))).ToString("dd/MM/yyyy")
        '    'StrData = ("select max(CAST(Date_of_upload AS DATE)) from card_courieer_master where no_of_attachment=1")
        '    'courierdate = CDate(("select max(CAST(Date_of_upload AS DATE)) from card_courieer_master where no_of_attachment=2"))
        '    StrData += CStr(Packingdate) + "¥" + CStr(courierdate)
        '    CallBackReturn = StrData.ToString
        'End If

    End Sub
#End Region
#Region "Save"
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            hdnValue.Value += "Ø" + Session("UserID").ToString()

            Dim ContentType As String = ""
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            'If nFileLen > 0 Then
            '    Dim Ftp As New FtpClass
            '    Ftp.DownloadFile()

            'End If
            Dim AttachImg As Byte() = Nothing
            Dim UploadDate As Date = CDate(Data(1))
            Dim SpeedPost As Integer = CInt(Data(2))
            Dim Courier As Integer = CInt(Data(3))
            Dim UserID As Integer = CInt(Data(4))
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            Dim CourierID As Integer = 0
            Dim Type As Integer = 0
            Dim EditFlag As Integer
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If

            If (NoofAttachments = 0) Then
                Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_srpt1.Append("alert('Please Attach the file');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                Exit Sub
            End If

            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@UploadDate", SqlDbType.DateTime)
                Params(0).Value = UploadDate
                Params(1) = New SqlParameter("@NOOFATTACHMENTS", SqlDbType.Int)
                Params(1).Value = NoofAttachments
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@CourierId", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@EditFlag", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_Courier_Dispatch", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
                CourierID = CInt(Params(5).Value)
                EditFlag = CInt(Params(6).Value)
                If CourierID > 0 And hfc.Count > 0 Then
                    For i = 0 To hfc.Count - 1
                        Dim myFile As HttpPostedFile = hfc(i)
                        Dim nFileLen As Integer = myFile.ContentLength
                        Dim FileName As String = ""
                        If Courier = 1 Then
                            Type = 1
                            Courier = 0
                        ElseIf SpeedPost = 1 Then
                            Type = 2
                            SpeedPost = 0
                        End If
                        If (nFileLen > 0) Then
                            ContentType = myFile.ContentType
                            FileName = myFile.FileName
                            AttachImg = New Byte(nFileLen - 1) {}
                            myFile.InputStream.Read(AttachImg, 0, nFileLen)
                            Dim Params1(5) As SqlParameter
                            Params1(0) = New SqlParameter("@CourierId", SqlDbType.Int)
                            Params1(0).Value = CourierID
                            Params1(1) = New SqlParameter("@attachment", SqlDbType.VarBinary)
                            Params1(1).Value = AttachImg
                            Params1(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                            Params1(2).Value = ContentType
                            Params1(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                            Params1(3).Value = FileName
                            Params1(4) = New SqlParameter("@TypeFlag", SqlDbType.Int)
                            Params1(4).Value = Type
                            Params1(5) = New SqlParameter("@EditFlag", SqlDbType.Int)
                            Params1(5).Value = EditFlag
                            DB.ExecuteNonQuery("[SP_courier_dispatch_ATTACH]", Params1)
                        End If
                    Next
                End If

            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())

            End Try

            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region


End Class
