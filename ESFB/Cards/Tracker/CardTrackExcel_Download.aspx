﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="CardTrackExcel_Download.aspx.vb" Inherits="CardTrackExcel_Download" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
    <table align="center" style="width: 40%; text-align:center; margin:0px auto;">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
         <tr>
            <td style="width:50%; text-align:center;">
                Date</td>
             
           <td  style="width:50%;">
              <asp:TextBox ID="txtUploadDt" class="NormalText" runat="server" 
                    Width="45%" onkeypress="return false" ></asp:TextBox>
                <asp:CalendarExtender ID="txtUploadDt_CalendarExtender" runat="server" 
                 Enabled="True" TargetControlID="txtUploadDt" Format="dd MMM yyyy">
                </asp:CalendarExtender>
              
                </td>
           
        </tr>
    <tr>
            <td style="width:50%; text-align:center;">
                Type</td>
           <td  style="width:50%;">
              
               <asp:DropDownList ID="cmbType" class="NormalText" runat="server" Font-Names="Cambria" 
         Width="46%" ForeColor="Black" AppendDataBoundItems="True">
         <asp:ListItem Value="-1">---Select---</asp:ListItem>
          <asp:ListItem Value="1">Courier</asp:ListItem>
           <asp:ListItem Value="2">Packing Slip</asp:ListItem>
              
                </asp:DropDownList>
                </td>
           
        </tr>
        <tr>
        <td style="width:50%; text-align:center;">
        </td>
        <td style="width:50%; text-align:center;">
         <br />
        </td>
        </tr>
         </table>
    <div style="text-align:center;">
    <input id="BtnDownload" runat="server" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="DOWNLOAD"  onclick="return DownloadFile()" />
               <input id="btnExit" runat="server" style="font-family: Cambria; font-size: 10pt; width: 10%" 
                type="button" value="EXIT"  onclick="return btnExit_onclick()" />
    </div>           <asp:Panel ID="pnlDtls" runat="server"></asp:Panel>
            <asp:HiddenField ID="hidStringData" runat="server" />

    <script language="javascript" type="text/javascript">
         window.onload = function()
    {
     
         ScrollText();
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

     function DownloadFile() {
         var typeID = document.getElementById("<%= cmbType.ClientID %>").value;
          var Date=document.getElementById("<%= txtUploadDt.ClientID %>").value;
            if(typeID==-1)
            {
               alert("Please select Type");
               return false;
               }

            window.open("Report/ShowAttachment.aspx?TypeID=" + btoa(typeID) + "&Date=" + btoa(Date) +"","_self");
            return false;
        }

        function ScrollText()
         {
            var ToData = "1Ø";
            ToServer(ToData,2);

         }
   function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
       
     function FromServer(arg, context) {

         if (context == 1) 
        {
//            var Data = arg.split("Ø");
//            alert(Data[1]);
//            if (Data[0] == 0)
//            {
//       
//            }        
        }
        else if (context == 2)
        {    
            pnlDisplay(arg);
        }
        else if (context == 3)
        {    
         
//            alert("Deleted Successfully");
//            window.open("Catchment_Area_Pincode.aspx", "_self"); 
        }
            }
 function pnlDisplay(arg) {
        document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
        document.getElementById("<%= hidStringData.ClientID %>").value=arg;
         var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class=''>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
        if (document.getElementById("<%= hidStringData.ClientID %>").value != "") {
            row1 = document.getElementById("<%= hidStringData.ClientID %>").value.split("¥");
            var packing_date=row1[0].split("00");
            var ShowQuali = 0;
            tab += "<tr>"
            tab += "<tr><td style='width:100%;height:20px; text-align:left;' ><marquee style='color: #F53C3C; background-color:blanchedalmond;' behavior='scroll' direction='right' scrollamount='3'>Package slip has been uploaded for the latest date :-'"+packing_date[0]+"' . . . .Courrier has been uploaded for the latest date :-'"+row1[1]+"'</marquee></td></tr>";
            tab += "</tr>";


        }
        tab += "</table></div>";
         document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
    }

    </script>
</asp:Content>

