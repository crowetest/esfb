﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Deliverables_tracking
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim GN As New GeneralFunctions
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 1466) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If

            Me.Master.subtitle = "Deliverables tracking"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            
            If Not IsPostBack Then
                Me.txtFromDT.Text = Date.Today.ToString("dd MMM yyyy")
                Me.txtToDT.Text = Date.Today.ToString("dd MMM yyyy")
                'Me.CalFromDate.SelectedDate = Start_Dt
                'Me.CalToDate.SelectedDate = Start_Dt
                DT = DB.ExecuteDataSet(" select  item_id,item_name from Cards_item_Master where status_id=1 order by item_id").Tables(0)
                GN.ComboFill(CmbType, DT, 0, 1)
                DT = DB.ExecuteDataSet("select -1,'----------Select Category----------'Category_id union all select Category_id,Category_name  from Cards_deliverable_Master").Tables(0)
                GN.ComboFill(cmbCategory, DT, 0, 1)
                Dim BranchID As Integer = CInt(Session("BranchID"))
                If BranchID = 0 Then
                    DT1 = DB.ExecuteDataSet("select -1,'----------ALL----------'[Branch Code] union all select distinct [Branch Code],[Branch Name]  from cards_dispatch_dtl").Tables(0)
                Else
                    DT1 = DB.ExecuteDataSet(" select distinct [Branch Code],[Branch Name]  from cards_dispatch_dtl where [Branch Code]='" & BranchID & "' ").Tables(0)
                    cmbBranch.Enabled = False
                End If
                GN.ComboFill(cmbBranch, DT1, 0, 1)
            End If
            

            Me.cmbCategory.Attributes.Add("onchange", "return categoryOnChange();")
            'Me.btnView.Attributes.Add("onclick", "return ViewOnClick();")

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString(), False)
            End If
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
    End Sub

#Region "CallBack"

    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        
            Dim DT2 As New DataTable
            Dim Type_id = CStr(Data(1))
            Dim Category_ID = CStr(Data(2))
            Dim Branch_ID = CStr(Data(3))
            Dim Search_value = CStr(Data(4))
            Dim From_DT = CStr(Data(5))
        Dim To_DT = CStr(Data(6))
        Me.hidTypeFlag.Value = Type_id
            Dim DTS As New DataTable
            If (Search_value <> 1 And Branch_ID <> "-1") Then
                If (Category_ID = 1 Or Category_ID = 3 Or Category_ID = 4 Or Category_ID = 5) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Account no]='" & Search_value & "' and [Branch Code]='" & Branch_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'  ").Tables(0)
                End If
                If (Category_ID = 2) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Request ID]='" & Search_value & "' and [Branch Code]='" & Branch_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                End If
                If (Category_ID = 6) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Refference number]='" & Search_value & "' and [Branch Code]='" & Branch_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                End If
            ElseIf (Search_value = 1 And Branch_ID <> "-1") Then
                If (Category_ID = 1 Or Category_ID = 3 Or Category_ID = 4 Or Category_ID = 5) Then
                    If (Search_value <> 1) Then
                    DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [Category ID]='" & Category_ID & "' and [Account no]='" & Search_value & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                    Else
                    DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [Category ID]='" & Category_ID & "' and [Branch Code]='" & Branch_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)

                    End If
                End If
                If (Category_ID = 2) Then
                    If (Search_value <> 1) Then
                    DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Request ID]='" & Search_value & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                    Else
                    DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Branch Code]='" & Branch_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)

                    End If
                End If
                If (Category_ID = 6) Then
                    If (Search_value <> 1) Then
                    DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Refference number]='" & Search_value & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                    Else
                    DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Branch Code]='" & Branch_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)

                    End If
                End If
            ElseIf (Search_value = 1 And Branch_ID = "-1") Then
                If (Category_ID = 1 Or Category_ID = 3 Or Category_ID = 4 Or Category_ID = 5) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and  [category ID]='" & Category_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                End If
                If (Category_ID = 2) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and  [category ID]='" & Category_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                End If
                If (Category_ID = 6) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no] from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
                End If
        ElseIf (Search_value <> 1 And Branch_ID = "-1") Then
            If (Category_ID = 1 Or Category_ID = 3 Or Category_ID = 4 Or Category_ID = 5) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Account no]='" & Search_value & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
            End If
            If (Category_ID = 2) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Request ID]='" & Search_value & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
            End If
            If (Category_ID = 6) Then
                DT2 = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_id & "' and [category ID]='" & Category_ID & "' and [Refference number]='" & Search_value & "' and [upload date] between '" + From_DT + "' and '" + To_DT + "'").Tables(0)
            End If
        End If

        For Each DR As DataRow In DT2.Rows
            CallBackReturn += "Ř" + DR(0).ToString().Trim() + "Ĉ" + DR(1).ToString().Trim() + "Ĉ" + DR(2).ToString().Trim() + "Ĉ" + DR(3).ToString().Trim() + "Ĉ" + DR(4).ToString().Trim() + "Ĉ" + DR(5).ToString().Trim() + "Ĉ" + DR(6).ToString().Trim() + "Ĉ" + DR(7).ToString().Trim()
        Next

    End Sub

#End Region

End Class
