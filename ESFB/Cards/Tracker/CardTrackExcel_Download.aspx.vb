﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CardTrackExcel_Download
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As Date
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                Me.txtUploadDt_CalendarExtender.SelectedDate = CDate(Session("TraDt"))
            End If
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim Errmsg As Integer = CInt(Request.QueryString.Get("ErrMsg"))
            If Not Errmsg = Nothing Then
                Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                cl_srpt1.Append("alert('No file has uploaded');")
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)

            End If

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try

    End Sub

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then 'document display
            Dim StrData As String = Nothing
            Dim Packingdate As String = Nothing
            Dim courierdate As String = Nothing
            DT = DB.ExecuteDataSet("select max(convert (varchar(10),Date_of_upload,111)) from card_courier_master a inner join DMS_ESFB.dbo.card_courier_attachment b on a.courier_id=b.courier_id  where b.type_flag=2").Tables(0)
            StrData = CDate(CStr(DT.Rows(0)(0))).ToString("dd/MM/yyyy")
            DT1 = DB.ExecuteDataSet("select max(convert (varchar(10),Date_of_upload,111)) from card_courier_master a inner join DMS_ESFB.dbo.card_courier_attachment b on a.courier_id=b.courier_id  where b.type_flag=1").Tables(0)
            courierdate = CDate(CStr(DT1.Rows(0)(0))).ToString("dd/MM/yyyy")
            'StrData = ("select max(CAST(Date_of_upload AS DATE)) from card_courieer_master where no_of_attachment=1")
            'courierdate = CDate(("select max(CAST(Date_of_upload AS DATE)) from card_courieer_master where no_of_attachment=2"))
            StrData += CStr(Packingdate) + "¥" + CStr(courierdate)
            CallBackReturn = StrData.ToString
        End If

    End Sub
#End Region

End Class

