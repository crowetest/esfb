﻿Imports System.Data
Partial Class ShowAttachment
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim TraDt As Date
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            TraDt = CDate(Session("TraDt")).ToString("yyyy MMM dd")
            'Dim UploadDate As Date = CStr(GF.Decrypt(Request.QueryString.[Get]("Date")))
            Dim UploadDate As String = CDate(GF.Decrypt(Request.QueryString.[Get]("Date"))).ToString("yyyy MMM dd")
            Dim TypeID As Integer = CInt(GF.Decrypt(Request.QueryString.[Get]("TypeID")))
            Dim dt As DataTable
            '            If TypeID = 1 Then
            '                dt = DB.ExecuteDataSet("select attachment,content_Type,isnull(case when Atch_fileName='' then 'Attachment' else Atch_fileName end,'Attachment')" +
            '"	from DMS_ESFB.dbo.card_courier_attachment  a " +
            '"	inner join esfb.dbo.Card_Courier_Master b on a.courier_id=b.courier_id " +
            '"	where date_of_upload='" & UploadDate & "' and type_flag=1").Tables(0)

            '                '                dt = DB.ExecuteDataSet("select attachment,content_Type,attach_file_name" +
            '                '"	from DMS_ESFB.dbo.dedupe_attach   where pkid = 55").Tables(0)
            '            Else
            '                dt = DB.ExecuteDataSet("select attachment,content_Type,isnull(case when Atch_fileName='' then 'Attachment' else Atch_fileName end,'Attachment')" +
            '"	from DMS_ESFB.dbo.card_courier_attachment  a " +
            '"	inner join esfb.dbo.Card_Courier_Master b on a.courier_id=b.courier_id " +
            '"	where date_of_upload='" & UploadDate & "' and type_flag=2").Tables(0)
            '            End If
            Dim Parameters1 As System.Data.SqlClient.SqlParameter() = {New System.Data.SqlClient.SqlParameter("@TypeID", TypeID),
                                                                       New System.Data.SqlClient.SqlParameter("@Uploaded_Dt", UploadDate)}
            dt = DB.ExecuteDataSet("SP_CARDS_GET_UPLOADED_DATA", Parameters1).Tables(0)

            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    Dim bytes() As Byte = CType(dt.Rows(0)(0), Byte())
                    Response.Buffer = True
                    Response.Charset = ""
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.ContentType = dt.Rows(0)(1).ToString()
                    Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows(0)(2).ToString().Replace(" ", ""))
                    'Response.AddHeader("content-disposition", dt.Rows(0)(2).ToString()) ' ----------  OPenwith Dialogbox 
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                Else

                    'Dim cl_srpt1 As System.Text.StringBuilder = New System.Text.StringBuilder()
                    'cl_srpt1.Append("alert('No file has uploaded today');")
                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_srpt1.ToString(), True)
                    Response.Redirect("~/Cards/tracker/CardTrackExcel_Download.aspx?ErrMsg=1", False)

                End If
            End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected() Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub
End Class
