﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false"
    CodeFile="Deliverable_tracking.aspx.vb" Inherits="Deliverables_tracking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
    <style type="text/css">
        .NormalText
        {
            font-family: Cambria;
            font-size: 10pt;
        }
    </style>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <br />
    <div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    </div>
    <div>
        <table align="center" style="width: 50%; text-align: center; margin: 0px auto;">
            <tr>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px;" width="50%" class="style5">
                    Delivarable Type
                </td>
                <td>
                    <asp:DropDownList ID="CmbType" Style="text-align: center;" runat="server" Height="19px"
                        Width="100%">
                    </asp:DropDownList>
                </td>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px;" width="50%" class="style5">
                    Category
                </td>
                <td>
                    <asp:DropDownList ID="cmbCategory" Style="text-align: center;" runat="server" Height="19px"
                        Width="100%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px;" class="style5">
                    Branch
                </td>
                <td>
                    <asp:DropDownList ID="cmbBranch" Style="text-align: center;" runat="server" Height="20px"
                        Width="100%">
                    </asp:DropDownList>
                </td>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px;" id="lblaccount"
                    class="style5">
                    Account No
                </td>
                <td id="accountno">
                    <asp:TextBox ID="txtAccountNo" class="NormalText" runat="server" autocomplete="off"
                        Width="99%" MaxLength="200"></asp:TextBox>
                </td>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px; display: none;"
                    id="lblrequest" class="style5">
                    Request Id
                </td>
                <td id="request" style="display: none;">
                    <asp:TextBox ID="txtRequestNo" class="NormalText" runat="server" autocomplete="off"
                        Width="99%" MaxLength="200"></asp:TextBox>
                </td>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px; display: none;"
                    id="lblrefference" class="style5">
                    Refference No
                </td>
                <td id="refference" style="display: none;">
                    <asp:TextBox ID="txtRefferenceNo" class="NormalText" runat="server" autocomplete="off"
                        Width="99%" MaxLength="200"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px;" class="style5">
                    <asp:Label ID="lblFromDt" class="NormalText" runat="server">From Date<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:HiddenField ID="hidFromDt" runat="server" />
                    <asp:TextBox ID="txtFromDT" class="NormalText" runat="server" autocomplete="off"
                        ReadOnly="true" Width="197px"></asp:TextBox>
                    <asp:CalendarExtender ID="CalFromDate" runat="server" Enabled="True" EnabledOnClient="true"
                        OnClientShowing="setStartDate" TargetControlID="txtFromDT" Format="dd MMM yyyy">
                    </asp:CalendarExtender>
                </td>
                <td style="text-align: center; font-size: 11pt; padding-left: 0px;" class="style5">
                    <asp:Label ID="lblToDt" class="NormalText" runat="server">To Date<span style="color:red"> *</span></asp:Label>
                </td>
                <td>
                    <asp:HiddenField ID="hidToDt" runat="server" />
                    <asp:TextBox ID="txtToDT" class="NormalText" runat="server" autocomplete="off" ReadOnly="true"
                        Width="235px"></asp:TextBox>
                    <asp:CalendarExtender ID="CalToDate" runat="server" Enabled="True" EnabledOnClient="true"
                        OnClientShowing="setStartDate" TargetControlID="txtToDT" Format="dd MMM yyyy">
                    </asp:CalendarExtender>
                </td>
            </tr>
            <asp:HiddenField ID="hidType" runat="server" />
            <asp:HiddenField ID="hidCategory" runat="server" />
            <asp:HiddenField ID="hidTypeFlag" runat="server" />
            <asp:HiddenField ID="hid" runat="server" />
        </table>
        <div align="center" style="padding-top: 20px">
            <%--<asp:Button ID="btnView" runat="server" Style="font-family: cambria; cursor: pointer;
                width: 67px;" type="button" Text="VIEW" />&nbsp;&nbsp;--%>
            <input id="btnView"   style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="VIEW"   onclick="return ViewOnClick()"  />&nbsp;&nbsp;&nbsp;
             <input id="Button1"   style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="CLEAR"   onclick="return RefreshOnClick()"  />&nbsp;&nbsp;&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                value="EXIT" onclick="return btnExit_onclick()" />
        </div>
        <br /><br />
        <div>
      
            <tr>
                <td style="width:10%;"></td>
                <td style="width:80%;">
                <asp:Panel ID="pnlDispatch" runat="server">
                </asp:Panel>
                </td>
            </tr>  
    </div>
    <script language="javascript" type="text/javascript">

    function categoryOnChange(){
        if (document.getElementById("<%= cmbCategory.ClientID %>").value == 1 || document.getElementById("<%= cmbCategory.ClientID %>").value == 3 || document.getElementById("<%= cmbCategory.ClientID %>").value == 4 || document.getElementById("<%= cmbCategory.ClientID %>").value == 5)
        {
            document.getElementById("lblaccount").style.display='';
            document.getElementById("accountno").style.display='';
            document.getElementById("lblrequest").style.display='none';
            document.getElementById("request").style.display='none';
             document.getElementById("lblrefference").style.display='none';
            document.getElementById("refference").style.display='none';
   
        }
        if(document.getElementById("<%= cmbCategory.ClientID %>").value ==2 )
        {
            document.getElementById("lblaccount").style.display='none';
            document.getElementById("accountno").style.display='none';
             document.getElementById("lblrequest").style.display='';
            document.getElementById("request").style.display='';
             document.getElementById("lblrefference").style.display='none';
            document.getElementById("refference").style.display='none';
        }
        if (document.getElementById("<%= cmbCategory.ClientID %>").value == 6)
        {
             document.getElementById("lblaccount").style.display='none';
            document.getElementById("accountno").style.display='none';
             document.getElementById("lblrefference").style.display='';
            document.getElementById("refference").style.display='';
              document.getElementById("lblrequest").style.display='none';
            document.getElementById("request").style.display='none';
        }
          
    }

     function FromServer(arg, context)   
    {  
        var data=arg.split("Ř");
       
      TableFill(arg);
    }  


     function ComboFill(data, ddlName) {
                document.getElementById(ddlName).options.length = 0;
                var rows = data.split("Ř");
                for (a = 1; a < rows.length; a++) {
                    var cols = rows[a].split("Ĉ");
                    var option1 = document.createElement("OPTION");
                    option1.value = cols[0];
                    option1.text = cols[1];
                    document.getElementById(ddlName).add(option1);
                }
            }
        function btnExit_onclick() {

            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
 function RefreshOnClick() {

            window.open("Deliverable_tracking.aspx", "_self");
        }
        function setStartDate(sender, args) {                    
            var Today = new Date();
            sender._endDate = Today;            
        }


        function ViewOnClick() { 
                if(document.getElementById("<%= cmbCategory.ClientID %>").value == "" || document.getElementById("<%= cmbCategory.ClientID %>").value == "-1")
                {
                    alert("Please Select Category");
                    document.getElementById("<%= cmbCategory.ClientID %>").focus();
                    return false;
                } 
     if (document.getElementById("<%= cmbCategory.ClientID %>").value == 1 || document.getElementById("<%= cmbCategory.ClientID %>").value == 3 || document.getElementById("<%= cmbCategory.ClientID %>").value == 4 || document.getElementById("<%= cmbCategory.ClientID %>").value == 5)
    {
            if(document.getElementById("<%= txtAccountNo.ClientID %>").value == "")
                {
                    var Search_value=1;
                }
            else 
                {
                var Search_value=document.getElementById("<%= txtAccountNo.ClientID %>").value;
                }
    }
    if(document.getElementById("<%= cmbCategory.ClientID %>").value ==2 )
    {
            if(document.getElementById("<%= txtRequestNo.ClientID %>").value == "")
                {
                    var Search_value=1;
                }
            else
                {   
                var Search_value=document.getElementById("<%= txtRequestNo.ClientID %>").value;

                }
    }
    if (document.getElementById("<%= cmbCategory.ClientID %>").value == 6)
    {
    if(document.getElementById("<%= txtRefferenceNo.ClientID %>").value == "")
                {
                   var Search_value=1;
                }
                else
                {
                var Search_value=document.getElementById("<%= txtRefferenceNo.ClientID %>").value;
                 }
    }


               
                 if(document.getElementById("<%= cmbBranch.ClientID %>").value == "")
                {
                    alert("Please select Branch");
                    document.getElementById("<%= cmbBranch.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtFromDT.ClientID %>").value == "")
                {
                    alert("Please select From Date");
                    document.getElementById("<%= txtFromDT.ClientID %>").focus();
                    return false;
                }
                if(document.getElementById("<%= txtToDT.ClientID %>").value == "")
                {
                    alert("Please Select To Date");
                    document.getElementById("<%= txtToDT.ClientID %>").focus();
                    return false;
                }
               
               var Type_ID=document.getElementById("<%= CmbType.ClientID %>").value;
               var Category_ID=document.getElementById("<%= cmbCategory.ClientID %>").value;         
               var Branch_ID=document.getElementById("<%= cmbBranch.ClientID %>").value;
               var From_DT=document.getElementById("<%= txtFromDT.ClientID %>").value;
               var To_DT=document.getElementById("<%= txtToDT.ClientID %>").value;
               var Data = "1Ø" + Type_ID + "Ø" +Category_ID +"Ø" +Branch_ID +"Ø" + Search_value +"Ø" + From_DT + "Ø" + To_DT;
               
               ToServer(Data, 1);

        }
function TableFill(arg)
        {
            var type_ID= document.getElementById("<%= cmbCategory.ClientID %>").value;
            
            document.getElementById("<%= pnlDispatch.ClientID %>").style.display = '';
            var tab = "";            
            tab += "<div style='width:85%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='ClosureTable'>";
            tab += "<tr class=mainhead style='height:35px;'>";
            tab += "<td style='width:5%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:10%;text-align:center'>Branch Name</td>";
            tab += "<td style='width:10%;text-align:center'>Branch Code</td>";
            tab += "<td style='width:10%;text-align:center'>AWB No</td>";
            if (type_ID == 1 || type_ID == 3 || type_ID == 4 || type_ID == 5){
            tab += "<td style='width:13%;text-align:center' >Customer Name</td>";
            tab += "<td style='width:16%;text-align:center'>Customer Account Number</td>";
            }
            else if(type_ID ==2){
             tab += "<td style='width:13%;text-align:center' >Request ID</td>";
            tab += "<td style='width:16%;text-align:center'>Quantity</td>";
            }
            else if(type_ID ==6){
             tab += "<td style='width:13%;text-align:center' >Refference Number</td>";
            tab += "<td style='width:16%;text-align:center'>Number Of Books</td>";
            }
            tab += "<td style='width:13%;text-align:center'>Date Of Dispatch</td>";
            tab += "<td style='width:10%;text-align:center' >Courier</td>";
            tab += "<td style='width:13%;text-align:center' >Status</td>";
            tab += "</tr>";
            var data=arg.split("Ř");
            var j = data.length;
                for (var k=1;k<j;k++)
                {
                            var DispatchDtl=data[k].split("Ĉ");
                            tab += "<tr class=sub_first style ='height:30px;'>";
                            tab += "<td style='width:5%;text-align:center'>" + k + "</td>";
                            tab += "<td style='width:10%;text-align:center'>" + DispatchDtl[0] + "</td>";
                            tab += "<td style='width:10%;text-align:center'> "+ DispatchDtl[1] + "</td>";
                            tab += "<td style='width:10%;text-align:center'> "+ DispatchDtl[7] + "</td>";
                            tab += "<td style='width:13%;text-align:center'>" + DispatchDtl[2] + "</td>";
                            tab += "<td style='width:16%;text-align:center'>" + DispatchDtl[3] + "</td>";
                            tab += "<td style='width:13%;text-align:center'>" + DispatchDtl[4] + "</td>";
                            tab += "<td style='width:10%;text-align:center'>" + DispatchDtl[5] + "</td>";
                            tab += "<td style='width:13%;text-align:center'>" + DispatchDtl[6] + "</td>";
                            tab += "</tr>";
                }
            
        tab += "</table></div>";
        document.getElementById("<%= pnlDispatch.ClientID %>").innerHTML = tab;  
    }
      
    </script>
</asp:Content>
