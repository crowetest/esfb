﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class Reports_viewPunchingReport
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim SQL As String = ""
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim DT As New DataTable
    Dim FromDt As Date
    Dim ToDt As Date
    Dim StatusID As Integer
    Dim RepBranchID As Integer
    Dim Selection As String = ""
    Dim BranchFilter As String = ""
    Dim StatusFilter As String = ""
    Dim TypeFilter As String = ""
    Dim DateFilter As String = ""
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RepBranchID = CInt(Session("BranchID"))
        If Not IsPostBack Then
            If RepBranchID = 0 Then
                DT = GN.GetQueryResult("SELECT ' ALL',-1 UNION ALL select UPPER(BRANCH_NAME),BRANCH_ID from ESFB.DBO.BRANCH_MASTER  order by 1 ")
                GN.ComboFill(cmbBranch, DT, 1, 0)
                DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(DISP_NAME),STATUS_ID from ESFB.DBO.CARD_REQUEST_STATUS").Tables(0)
                GN.ComboFill(cmbStatus, DT, 1, 0)
            Else
                DT = GN.GetQueryResult("select UPPER(BRANCH_NAME),BRANCH_ID from ESFB.DBO.BRANCH_MASTER WHERE STATUS_ID = 1  AND BRANCH_ID = " & RepBranchID & "")
                GN.ComboFill(cmbBranch, DT, 1, 0)
                DT = DB.ExecuteDataSet("select DISTINCT UPPER(GROUP_NAME),GROUP_ID from ESFB.DBO.CARD_REQUEST_STATUS  ORDER BY 1 DESC").Tables(0)
                GN.ComboFill(cmbStatus, DT, 1, 0)
            End If
            DT = DB.ExecuteDataSet("SELECT ' ALL',-1 UNION ALL select UPPER(TYPE_NAME),TYPE_ID from ESFB.DBO.CARD_REQUEST_TYPE").Tables(0)
            GN.ComboFill(cmbType, DT, 1, 0)
        End If
        Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
        Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "OnLoad();", True)
        Me.btnGenerate.Attributes.Add("onclick", "return GenerateOnclick()")
        Me.cmbDate.Attributes.Add("onchange", "return DateOnchange()")
        GenerateReport()
    End Sub
    Public Sub GenerateReport()
        Try
            Dim tb As New Table
            pnDisplay.Controls.Clear()
            Dim BranchID As Integer = CInt(Me.cmbBranch.SelectedValue)
            Dim StatusID As Integer = CInt(Me.cmbStatus.SelectedValue)
            Dim TypeID As Integer = CInt(Me.cmbType.SelectedValue)
            Dim DateSelection As Integer = CInt(Me.cmbDate.SelectedValue)

            If RepBranchID > 0 Then
                Selection = "SELECT A.BRANCH_ID,B.BRANCH_NAME,A.ACCOUNT_NO,A.CIF,A.CUSTOMER_NAME,A.TRA_DT REQ_DT,A.REQUEST_ID,C.TYPE_NAME,A.STATUS_ID,D.DISP_NAME FROM CARD_REQUEST A,BRANCH_MASTER B,CARD_REQUEST_TYPE C,CARD_REQUEST_STATUS D WHERE A.BRANCH_ID = B.BRANCH_ID AND A.REQUEST_TYPE = C.TYPE_ID AND A.STATUS_ID = D.STATUS_ID "
                If BranchID <> -1 Then
                    BranchFilter = " AND A.BRANCH_ID = " & BranchID & ""
                End If
                If StatusID <> -1 Then
                    StatusFilter = " AND D.GROUP_ID = " & StatusID & ""
                End If
                If TypeID <> -1 Then
                    TypeFilter = " AND A.REQUEST_TYPE = " & TypeID & ""
                End If
                If DateSelection = 1 Then
                    FromDt = CDate(Me.txtFrom.Text)
                    ToDt = CDate(Me.txtTo.Text)
                    DateFilter = " and DATEADD(day,DATEDIFF(day, 0,A.TRA_DT),0)  >= '" & CDate(FromDt).ToString("MM/dd/yyyy") & "'  and DATEADD(day,DATEDIFF(day, 0,A.TRA_DT),0) <= '" & CDate(ToDt).ToString("MM/dd/yyyy") & "'"
                End If
            Else
                Selection = "SELECT A.BRANCH_ID,B.BRANCH_NAME,A.ACCOUNT_NO,A.CIF,A.CUSTOMER_NAME,A.TRA_DT REQ_DT,A.REQUEST_ID,C.TYPE_NAME,A.STATUS_ID,D.DISP_NAME FROM CARD_REQUEST A,BRANCH_MASTER B,CARD_REQUEST_TYPE C,CARD_REQUEST_STATUS D WHERE A.BRANCH_ID = B.BRANCH_ID AND A.REQUEST_TYPE = C.TYPE_ID AND A.STATUS_ID = D.STATUS_ID "
                If BranchID <> -1 Then
                    BranchFilter = " AND A.BRANCH_ID = " & BranchID & ""
                End If
                If StatusID <> -1 Then
                    StatusFilter = " AND A.STATUS_ID = " & StatusID & ""
                End If
                If TypeID <> -1 Then
                    TypeFilter = " AND A.REQUEST_TYPE = " & TypeID & ""
                End If
                If DateSelection = 1 Then
                    FromDt = CDate(Me.txtFrom.Text)
                    ToDt = CDate(Me.txtTo.Text)
                    DateFilter = " and DATEADD(day,DATEDIFF(day, 0,A.TRA_DT),0)  >= '" & CDate(FromDt).ToString("MM/dd/yyyy") & "'  and DATEADD(day,DATEDIFF(day, 0,A.TRA_DT),0) <= '" & CDate(ToDt).ToString("MM/dd/yyyy") & "'"
                End If
            End If
            
            SQL = Selection + BranchFilter + StatusFilter + TypeFilter + DateFilter + " ORDER BY C.TYPE_NAME,TRA_DT"
            DT = DB.ExecuteDataSet(SQL).Tables(0)

            RH.Heading(Session("FirmName"), tb, "DEBIT CARD PIN REQUEST REPORT", 100)
            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")
            Dim TR0 As New TableRow
            TR0.BackColor = Drawing.Color.WhiteSmoke
            Dim TR0_00, TR0_01, TR0_02, TR0_03, TR0_04, TR0_05, TR0_06, TR0_07, TR0_08, TR0_09 As New TableCell
            RH.AddColumn(TR0, TR0_00, 5, 5, "l", "SLNO")
            'RH.AddColumn(TR0, TR0_01, 5, 5, "l", "BRANCH&nbsp;CODE")
            RH.AddColumn(TR0, TR0_02, 15, 15, "l", "BRANCH")
            RH.AddColumn(TR0, TR0_03, 10, 10, "l", "ACCOUNT NO")
            RH.AddColumn(TR0, TR0_04, 10, 10, "l", "CIF")
            RH.AddColumn(TR0, TR0_05, 10, 10, "l", "CUSTOMER NAME")
            RH.AddColumn(TR0, TR0_06, 15, 15, "l", "REQUEST DT")
            RH.AddColumn(TR0, TR0_07, 15, 15, "l", "STATUS")
            RH.AddColumn(TR0, TR0_08, 10, 10, "c", "VIEW")
            RH.AddColumn(TR0, TR0_09, 5, 5, "c", "REJECT")
            tb.Controls.Add(TR0)
            RH.DrawLine(tb, 100)

            Dim TypeName As String = ""
            Dim I As Integer = 1
            For Each DR In DT.Rows
                If TypeName <> DR(7).ToString() Then
                    RH.SubHeading(tb, 100, "l", DR(7).ToString())
                    RH.BlankRow(tb, 2)
                End If
                TypeName = DR(7).ToString()
                Dim TR1 As New TableRow
                If RowBG = 0 Then
                    RowBG = 1
                    TR1.BackColor = Drawing.Color.WhiteSmoke
                Else
                    RowBG = 0
                End If
                Dim TR1_00, TR1_01, TR1_02, TR1_03, TR1_04, TR1_05, TR1_06, TR1_07, TR1_08, TR1_09 As New TableCell
                RH.AddColumn(TR1, TR1_00, 5, 5, "c", "<a href='ViewDetails.aspx?ReqID=" + DR(6).ToString() + "' target='_blank' title='View Details'> " + I.ToString() + "</a>")
                ' RH.AddColumn(TR1, TR1_01, 5, 5, "l", DR(0))
                RH.AddColumn(TR1, TR1_02, 15, 15, "l", DR(1).ToString() + "(" + DR(0).ToString() + ")")
                RH.AddColumn(TR1, TR1_03, 10, 10, "l", DR(2))
                RH.AddColumn(TR1, TR1_04, 10, 10, "l", DR(3))
                If Not IsDBNull(DR(4)) Then
                    RH.AddColumn(TR1, TR1_05, 10, 10, "l", DR(4).ToString())
                Else
                    RH.AddColumn(TR1, TR1_05, 10, 10, "l", "")
                End If
                RH.AddColumn(TR1, TR1_06, 15, 15, "l", CDate(DR(5)).ToString("dd MMM yyyy hh:mm:ss tt"))
                RH.AddColumn(TR1, TR1_07, 15, 15, "l", DR(9).ToString)
                RH.AddColumn(TR1, TR1_08, 10, 10, "c", "<a href ='ViewAttachment.aspx?ReqID=" + DR(6).ToString() + "' target='_blank'>VIEW</a>")
                If RepBranchID > 0 Then
                    RH.AddColumn(TR1, TR1_09, 5, 5, "c", "")
                Else
                    If (CInt(DR(8)) = 1 Or CInt(DR(8)) = 2) Then
                        RH.AddColumn(TR1, TR1_09, 5, 5, "c", "<img src='../Image/Reject.png' style='width:30%;cursor:pointer;' title='Reject'  onclick='RejectOnclick(" + DR(6).ToString() + ")' />")
                    Else
                        RH.AddColumn(TR1, TR1_09, 5, 5, "c", "")
                    End If
                End If
                tb.Controls.Add(TR1)
                I = I + 1
            Next
            RH.DrawLine(tb, 100)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim Message As String = ""
            Dim ErrorStatus As Integer = 0
            Try
                Dim RequestID As Integer = CInt(Data(1))
                Dim Remarks As String = CStr(Data(2))
                Dim Params(4) As SqlParameter
                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = CInt(Session("UserID"))
                Params(2) = New SqlParameter("@Remarks", SqlDbType.VarChar, 300)
                Params(2).Value = Remarks
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CARD_REQUEST_REJECT", Params)
                ErrorStatus = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message()
                ErrorStatus = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorStatus.ToString())
            End Try
            CallBackReturn = ErrorStatus.ToString() + "Ø" + Message.ToString()
        End If
    End Sub
#End Region
    Protected Sub btnExport_Click(sender As Object, e As System.EventArgs) Handles btnExport.Click
        Try
            Selection = "SELECT A.BRANCH_ID [BRANCH CODE],B.BRANCH_NAME [BRANCH NAME],A.REQUEST_ID [REQUEST ID],A.TRA_DT [REQUEST DATE],A.ACCOUNT_NO [ACCOUNT NUMBER],A.CIF,A.CUSTOMER_NAME[CUSTOMER NAME],C.TYPE_NAME [TYPE],D.DISP_NAME [STATUS] FROM CARD_REQUEST A,BRANCH_MASTER B,CARD_REQUEST_TYPE C,CARD_REQUEST_STATUS D WHERE A.BRANCH_ID = B.BRANCH_ID AND A.REQUEST_TYPE = C.TYPE_ID AND A.STATUS_ID = D.STATUS_ID "
            SQL = Selection + BranchFilter + StatusFilter + TypeFilter + DateFilter + " ORDER BY C.TYPE_NAME,TRA_DT"
            DT = DB.ExecuteDataSet(SQL).Tables(0)

            Dim HeaderText As String
            HeaderText = "DEBITCARD PIN REQUEST REPORT"
            WebTools.ExporttoExcel(DT, HeaderText)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
End Class

