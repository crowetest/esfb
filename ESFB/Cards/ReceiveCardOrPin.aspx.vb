﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class ReceiveCardOrPin
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)


            Me.Master.subtitle = "Debit Card PIN Receival"
            If Not IsPostBack Then
                DT = DB.ExecuteDataSet("SELECT '-1' AS ID,'SELECT' AS NAME UNION ALL  select CAST(REQUEST_ID AS VARCHAR)+'~'+CAST(REQUEST_TYPE AS VARCHAR)+'~'+ACCOUNT_NO+'~'+CIF+'~'+CUSTOMER_NAME+'~'+CAST(STATUS_ID AS VARCHAR),CUSTOMER_NAME  from CARD_REQUEST WHERE STATUS_ID IN(2,3,4) and branch_id=" + Session("BranchID").ToString()).Tables(0)
                GF.ComboFill(ddlRequest, DT, 0, 1)
                txtDate_CalendarExtender.EndDate = CDate(Session("TraDt"))
            End If
            '--//---------- Script Registrations -----------//--

            Me.ddlRequest.Attributes.Add("onchange", "RequestOnChange()")
            txtAccountNo.Focus()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region


    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
            Dim RequestID As Integer = CInt(Data(1))
            Dim StatusID As Integer = CInt(Data(2))
            Dim ReceiveDt As Date = CDate(Data(3))
            Dim Remarks As String = Data(4)
            Dim Message As String = ""
            Dim ErrorStatus As Integer = 0
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@RequestID", SqlDbType.Int)
                Params(0).Value = RequestID
                Params(1) = New SqlParameter("@StatusID", SqlDbType.Int)
                Params(1).Value = StatusID
                Params(2) = New SqlParameter("@ReceiveDt", SqlDbType.Date)
                Params(2).Value = ReceiveDt
                Params(3) = New SqlParameter("@Remarks", SqlDbType.VarChar, 50)
                Params(3).Value = Remarks
                Params(4) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(4).Value = CInt(Session("UserID"))
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_CARD_RECEIVAL", Params)
                ErrorStatus = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message()
                ErrorStatus = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorStatus.ToString())
            End Try
            CallBackReturn = ErrorStatus.ToString() + "Ø" + Message.ToString()
        End If
    End Sub

End Class
