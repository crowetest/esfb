﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="ReceiveCardOrPin.aspx.vb" Inherits="ReceiveCardOrPin" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function btnSave_onclick() {
       if(document.getElementById("<%= ddlRequest.ClientID %>").value=="-1")
       {
        alert("Select Request");document.getElementById("<%= ddlRequest.ClientID %>").focus();return false;
       }
       if(document.getElementById("chkPin").checked==false  && document.getElementById("chkCard").checked==false )
       {alert("Check the Received Item(Card/Pin))");document.getElementById("chkCard").focus();return false;}
       if(document.getElementById("<%= txtDate.ClientID %>").value=="")
       {alert("Select Received Date");document.getElementById("<%= txtDate.ClientID %>").focus();return false;}
        var RequestDtl=document.getElementById("<%= ddlRequest.ClientID %>").value.split("~");
        var RequestID=RequestDtl[0];
        var RequestType=RequestDtl[1];
        var AccountNo=RequestDtl[2];
        var CIF      =RequestDtl[3];
        var CustName =RequestDtl[4];
        var StatusID =RequestDtl[5];
        if(RequestType==1)
        {
            if(document.getElementById("chkCard").checked==true && document.getElementById("chkPin").checked==true)
                StatusID=0;
            else if(document.getElementById("chkCard").checked==true)
                StatusID=4;
            else
                StatusID=3;
        }
        else
        {
               StatusID=0;
        }
        ToServer("1Ø" + RequestID + "Ø" + StatusID +"Ø"+document.getElementById("<%= txtDate.ClientID %>").value+"Ø"+document.getElementById("<%= txtRemarks.ClientID %>").value,1);
    }
    function btnExit_onclick() 
    {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }
    function RequestOnChange()
    {
        if(document.getElementById("<%= ddlRequest.ClientID %>").value=="-1")
        {
            document.getElementById("<%= txtAccountNo.ClientID %>").value="";
            document.getElementById("<%= txtCIF.ClientID %>").value="";
            document.getElementById("<%= txtName.ClientID %>").value="";
            document.getElementById("chkCard").checked=false;
            document.getElementById("chkPin").checked=false;
        }
        else{
            var RequestDtl=document.getElementById("<%= ddlRequest.ClientID %>").value.split("~");
            var RequestType=RequestDtl[1];
            var AccountNo=RequestDtl[2];
            var CIF      =RequestDtl[3];
            var CustName =RequestDtl[4];
            var StatusID =RequestDtl[5];
            document.getElementById("<%= txtAccountNo.ClientID %>").value=AccountNo;
            document.getElementById("<%= txtCIF.ClientID %>").value=CIF;
            document.getElementById("<%= txtName.ClientID %>").value=CustName;
            document.getElementById("chkCard").checked=false;
            document.getElementById("chkPin").checked=false;
            if(RequestType==1)
            {
                if(StatusID==3)
                {
                      document.getElementById("chkPin").checked=true;
                      document.getElementById("chkPin").disabled=true;
                }
                else if(StatusID==4)
                {
                      document.getElementById("chkCard").checked=true;
                      document.getElementById("chkCard").disabled=true;
                }
            }
            else
            {
                 document.getElementById("chkCard").checked=false;
                 document.getElementById("chkCard").disabled=true;
                 document.getElementById("chkPin").checked=true;
            }

        }
    }
  function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0)  window.open("ReceiveCardOrPin.aspx","_self");
            }
        }
    </script>
   </head>
</html>
<br />
    <asp:HiddenField ID="hid_data" runat="server" />
    <br />

 <table  style="width:80%;margin: 0px auto;">
        <tr>
        <td style="width:30%;">  <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager></td>
         <td style="width:15%; text-align:left;">Request</td>
            <td style="width:55%">
                &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlRequest" class="NormalText" 
                    runat="server" Width="50%">
                </asp:DropDownList>
            </td>


       </tr>
        <tr>
        <td style="width:30%;"></td>
         <td style="width:15%; text-align:left;">Account No</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:TextBox ID="txtAccountNo" class="ReadOnlyTextBox" 
                    onkeypress='return NumericCheck(event)' runat="server" Width="50%" 
                MaxLength="14" ReadOnly="True"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:30%;"></td>
       <td style="width:15%; text-align:left;">CIF</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:TextBox ID="txtCIF" class="ReadOnlyTextBox"  
                    onkeypress='return NumericCheck(event)' runat="server" Width="50%" 
                MaxLength="12" ReadOnly="True"></asp:TextBox>
            </td>


       </tr>
       <tr> 
       <td style="width:30%; height: 24px;"></td>
       <td style="width:15%; text-align:left; height: 24px;">Customer Name</td>
            <td style="width:55%; height: 24px;">
                &nbsp;&nbsp;
                <asp:TextBox ID="txtName" class="ReadOnlyTextBox" runat="server"  
                    onkeypress='return AlphaNumericCheck(event)' Width="50%" 
                MaxLength="50" ReadOnly="True"></asp:TextBox>
                &nbsp;</td>


       </tr>
     
       <tr> <td style="width:30%; height: 17px;">&nbsp;</td>
       <td style="width:15%; text-align:left; height: 17px;">&nbsp;</td>
            <td style="width:55%; height: 17px;">
                &nbsp;</td>


       </tr>
     
       <tr> <td style="width:30%; height: 17px;"></td>
       <td style="width:15%; text-align:left; height: 17px;">Received</td>
            <td style="width:55%; height: 17px;">
                &nbsp; &nbsp;<input id="chkCard" type="checkbox" />&nbsp; &nbsp;Card &nbsp; &nbsp;<input id="chkPin" 
                    type="checkbox" />&nbsp; &nbsp;Pin</td>


       </tr>
         <tr> 
       <td style="width:30%;"></td>
       <td style="width:15%; text-align:left;">Received Date</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDate">
                </asp:CalendarExtender>
           </td>


       </tr>
         <tr> 
       <td style="width:30%;">&nbsp;</td>
       <td style="width:15%; text-align:left;">Remarks&nbsp; (if any)</td>
            <td style="width:55%">
                &nbsp; &nbsp;<asp:TextBox ID="txtRemarks" class="NormalText" runat="server"  
                    onkeypress='return AlphaNumericCheck(event)' Width="50%" 
                MaxLength="50"></asp:TextBox>
           </td>


       </tr>
       <tr>
            <td style="text-align:center;" colspan="3"><br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="SAVE" onclick="return btnSave_onclick()" />&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" />
                 </td>
        </tr>

</table>    

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

