﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_dispatch_Status_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTExcel As New DataTable
    Dim DT1 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim DT As New DataTable
            
            Dim Branch_ID As Integer = CInt(Request.QueryString.Get("Branch_ID"))
            Dim From_Dt As Date = CDate(Request.QueryString.Get("From"))
            Dim To_Dt As Date = CDate(Request.QueryString.Get("To"))
            Dim Type_ID As Integer = CInt(Request.QueryString.Get("Type_ID"))
            Dim Category_ID As Integer = CInt(Request.QueryString.Get("Category_ID"))

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")

            Dim RowBG As Integer = 0
            Dim DR As DataRow
           
            DT = DB.ExecuteDataSet("select Category_name  from cards_deliverable_master where category_id = " + Category_ID.ToString() + "").Tables(0)
                For Each dtr In DT.Rows
                    RH.Heading(Session("FirmName"), tb, dtr(0).ToString() + " Report", 100)
                Next


            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"


            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid

            If Category_ID = 2 Then
                RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No")
                RH.AddColumn(TRHead, TRHead_01, 8, 8, "l", "Branch Code")
                RH.AddColumn(TRHead, TRHead_02, 12, 12, "l", "Branch Name")
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "AWB Number")
                RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Request ID")
                RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Quantity")
                RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Date Of Dispatch")
                RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Courier")
                RH.AddColumn(TRHead, TRHead_08, 10, 10, "l", "Status")

                tb.Controls.Add(TRHead)
            ElseIf Category_ID = 6 Then
                RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No")
                RH.AddColumn(TRHead, TRHead_01, 8, 8, "l", "Branch Code")
                RH.AddColumn(TRHead, TRHead_02, 12, 12, "l", "Branch Name")
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "AWB Number")
                RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Refference Number")
                RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "No Of Books")
                RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Date Of Dispatch")
                RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Courier")
                RH.AddColumn(TRHead, TRHead_08, 10, 10, "l", "Status")
                tb.Controls.Add(TRHead)
            Else
                RH.AddColumn(TRHead, TRHead_00, 5, 5, "l", "Sl No")
                RH.AddColumn(TRHead, TRHead_01, 8, 8, "l", "Branch Code")
                RH.AddColumn(TRHead, TRHead_02, 12, 12, "l", "Branch Name")
                RH.AddColumn(TRHead, TRHead_03, 10, 10, "l", "AWB Number")
                RH.AddColumn(TRHead, TRHead_04, 15, 15, "l", "Customer Name")
                RH.AddColumn(TRHead, TRHead_05, 10, 10, "l", "Customer  Account No")
                RH.AddColumn(TRHead, TRHead_06, 10, 10, "l", "Date Of Dispatch")
                RH.AddColumn(TRHead, TRHead_07, 10, 10, "l", "Courier")
                RH.AddColumn(TRHead, TRHead_08, 10, 10, "l", "Status")
                tb.Controls.Add(TRHead)
            End If


            RH.BlankRow(tb, 3)
            Dim i As Integer
            If Category_ID = 1 Or Category_ID = 3 Or Category_ID = 4 Or Category_ID = 5 Then
                If Branch_ID = -1 Then
                    DT = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_ID & "' and [Category ID]='" & Category_ID & "' and  [upload date] between '" + From_Dt + "' and '" + To_Dt + "'").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("select [branch name],[branch code],[name],[Account No],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_ID & "' and [Category ID]='" & Category_ID & "' and [branch code]='" & Branch_ID & "' and [upload date] between '" + From_Dt + "' and '" + To_Dt + "'").Tables(0)
                    'DT = DB.ExecuteDataSet("select a.emp_code,b.emp_name,a.request_no,c.tra_date from vc_request_master a inner join emp_master b on a.emp_code = b.emp_code inner join vc_request_cycle c on c.request_no = a.request_no where convert(varchar,cast(c.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(c.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) and c.status_id = " + Status.ToString() + " and c.cycle_id in (select max(cycle_id) from vc_request_cycle where status_id = " + Status.ToString() + " group by emp_code)").Tables(0)
                End If
            ElseIf Category_ID = 2 Then
                If Branch_ID = -1 Then
                    DT = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_ID & "' and [category ID]='" & Category_ID & "' and  [upload date] between '" + From_Dt + "' and '" + To_Dt + "'").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("select [branch name],[branch code],[Request ID],[Quantity],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_ID & "' and [category ID]='" & Category_ID & "' and [branch code]='" & Branch_ID & "' and  [upload date] between '" + From_Dt + "' and '" + To_Dt + "'").Tables(0)
                    'DT = DB.ExecuteDataSet("select a.emp_code,b.emp_name,a.request_no,c.tra_date from vc_request_master a inner join emp_master b on a.emp_code = b.emp_code inner join vc_request_cycle c on c.request_no = a.request_no where convert(varchar,cast(c.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(c.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) and c.status_id = " + Status.ToString() + " and c.cycle_id in (select max(cycle_id) from vc_request_cycle where status_id = " + Status.ToString() + " group by emp_code)").Tables(0)
                End If
            Else
                If Branch_ID = -1 Then
                    DT = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_ID & "' and [category ID]='" & Category_ID & "' and  [upload date] between '" + From_Dt + "' and '" + To_Dt + "'").Tables(0)
                Else
                    DT = DB.ExecuteDataSet("select [branch name],[branch code],[Refference number],[No Of Books],convert(varchar,[Date Of Dispatch],103),[Courier],[Status],[AWB no]  from cards_dispatch_dtl where [Type id]='" & Type_ID & "' and [category ID]='" & Category_ID & "' and [branch code]='" & Branch_ID & "' and  [upload date] between '" + From_Dt + "' and '" + To_Dt + "'").Tables(0)
                    'DT = DB.ExecuteDataSet("select a.emp_code,b.emp_name,a.request_no,c.tra_date from vc_request_master a inner join emp_master b on a.emp_code = b.emp_code inner join vc_request_cycle c on c.request_no = a.request_no where convert(varchar,cast(c.tra_date as date)) >= convert(varchar,cast('" + From_Dt + "' as date)) and convert(varchar,cast(c.tra_date as date))<=convert(varchar,cast('" + To_Dt + "' as date)) and c.status_id = " + Status.ToString() + " and c.cycle_id in (select max(cycle_id) from vc_request_cycle where status_id = " + Status.ToString() + " group by emp_code)").Tables(0)
                End If
            End If
            
            For Each DR In DT.Rows
                i += 1
                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid

                'If Type_ID = -1 Then
                RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                RH.AddColumn(TR3, TR3_01, 8, 8, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 12, 12, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_04, 15, 15, "l", DR(2)).ToString()
                RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_06, 10, 10, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_07, 10, 10, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_08, 10, 10, "l", DR(6).ToString())
                'Else
                '    RH.AddColumn(TR3, TR3_00, 5, 5, "l", i.ToString())
                '    RH.AddColumn(TR3, TR3_01, 5, 5, "l", DR(0).ToString())
                '    RH.AddColumn(TR3, TR3_02, 15, 15, "l", DR(1).ToString())
                '    RH.AddColumn(TR3, TR3_03, 10, 10, "l", DR(2).ToString())
                '    RH.AddColumn(TR3, TR3_04, 15, 15, "l", CDate(DR(3)).ToString("dd/MM/yyyy"))
                '    RH.AddColumn(TR3, TR3_05, 10, 10, "l", DR(4).ToString())
                '    RH.AddColumn(TR3, TR3_06, 10, 10, "l", DR(5).ToString())
                '    RH.AddColumn(TR3, TR3_07, 10, 10, "l", DR(6).ToString())
                '    RH.AddColumn(TR3, TR3_08, 10, 10, "l", DR(7).ToString())
                'End If

                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

End Class
