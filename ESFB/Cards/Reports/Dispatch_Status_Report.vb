﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Dispatch_Status_Report
    Inherits System.Web.UI.Page

    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim AD As New Audit
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim Meeting_Dt As Date
    Dim UserID As Integer
    Dim TraDt As String
    Dim BranchID As Integer
    Dim PostID As Integer
    Dim DB As New MS_SQL.Connect
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            'If GN.FormAccess(CInt(Session("UserID")), 108) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If

            Me.Master.subtitle = "Dispatch Status Report"
            UserID = CInt(Session("UserID"))
            Me.hdnUser.Value = UserID.ToString()
            If Not IsPostBack Then
                Me.txt_From_Dt.Text = Date.Today.ToString("dd MMM yyyy")
                Me.txt_To_Dt.Text = Date.Today.ToString("dd MMM yyyy")

            End If
            Dim BranchID As Integer = CInt(Session("BranchID"))
            Me.hidBranch.Value = BranchID.ToString()
            If BranchID = 0 Then
                DT1 = DB.ExecuteDataSet("select -1,'----------ALL----------'[Branch Code] union all select distinct [Branch Code],[Branch Name]  from cards_dispatch_dtl").Tables(0)
            Else
                DT1 = DB.ExecuteDataSet(" select distinct [Branch Code],[Branch Name]  from cards_dispatch_dtl where [Branch Code]='" & BranchID & "' ").Tables(0)
                cmbBranch.Enabled = False
            End If
            GF.ComboFill(cmbBranch, DT1, 0, 1)
            DT = DB.ExecuteDataSet(" select -1,'----------ALL----------'item_id union all select  item_id,item_name from Cards_item_Master where status_id=1 order by item_id").Tables(0)
            GF.ComboFill(cmbType, DT, 0, 1)

            DT2 = DB.ExecuteDataSet("select -1,'----------Select----------'Category_id union all select Category_id,Category_name  from Cards_deliverable_Master where Type_id=1 ").Tables(0)
            GF.ComboFill(cmbCategory, DT2, 0, 1)
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
        DB.dispose()
    End Sub

#End Region
#Region "Events"

#End Region

#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

        End If
    End Sub
#End Region
End Class

