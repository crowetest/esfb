﻿Option Strict On
Option Explicit On
Imports System.Data
Partial Class ESFB
    Inherits System.Web.UI.MasterPage
    Dim DB As New MS_SQL.Connect
    Public WriteOnly Property subtitle() As String
        Set(ByVal value As String)
            Me.lblSubHead.Text = value
        End Set
    End Property
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        Session.Clear()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("UserName") Is Nothing Then
            Me.lblUserName.Text = CStr(Session("UserName"))
            Me.lblFirmName.Text = CStr(Session("FirmName"))
            Me.lblPackageName.Text = CStr(Session("ModuleName"))
            Me.lblBranchName.Text = CStr(Session("BranchName"))
            Me.hdnGuest.Value = CStr(Session("GuestModuleID"))
            ' Me.lblNews.Text = "News Room : " + CStr(Session("News"))
        Else
            Response.Redirect("~/ErrorPage.aspx", False)
        End If

        Dim br_date As DataTable = DB.ExecuteDataSet("select getdate()").Tables(0)
        Me.lblDate.Text = Format(br_date.Rows(0)(0), "dd/MMM/yyyy")
        Me.lblTime.Text = Format(br_date.Rows(0)(0), "hh:mm:ss")
    End Sub
End Class

