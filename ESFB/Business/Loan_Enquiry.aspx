﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Loan_Enquiry.aspx.vb" Inherits="Loan_Enquiry"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
     
   
    <script language="javascript" type="text/javascript">
    
          
   
     function cursorwait(e) {
            document.body.className = 'wait';}
     function cursordefault(e) {
            document.body.className = 'default';}
      
    function btnExit_onclick() 
    {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

    function btnSave_onclick() {
    
    var newstr="";
    row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
   
    var NewStr=""
    for (n = 1; n <= row.length-1 ; n++) {     
    var Qualcol=row[n].split("µ");
   
            if(Qualcol[0]=="")
           {
           alert("Please Fill Name Field");
           return false;
           }
            else if(Qualcol[2]=="")
           {
           alert("Please Fill Mobile Number Field");
           return false;
           }
           else if(Qualcol[4]=="-1")
           {
           alert("Please Select Loan Product ");
           return false;
           }
           else if(Qualcol[5]=="")
           {
           alert("Please Fill Loan Amount Field");
           return false;
           }  
           else if (Qualcol[5]!="")
           {

           var value=checkvalue(Qualcol[5],n);
               if(value=="false")
               {
               
               return false;
               }

           }
           else if(Qualcol[2]!="")
           {
                  var value=checkPhone(Qualcol[2],n);
                  if(value=="false")
                  {

                    alert("Please provide a valid phone number");
                    return false;
                  }
                 
                   
           } 
           
           
             if((Qualcol[0]!="" && Qualcol[2]!="" &&  Qualcol[4]!="-1" && Qualcol[5]!="" )&& (Qualcol[1]!="" || Qualcol[3]!="" || Qualcol[6]!="" || Qualcol[7]!="" || Qualcol[8]!="" || Qualcol[9]!="" || Qualcol[10]!=""||Qualcol[11]!=""))
            {                         
                    NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+ "µ" +Qualcol[3]+ "µ" +Qualcol[4]+ "µ" +Qualcol[5]+ "µ" +Qualcol[6]+ "µ" +Qualcol[7]+ "µ" + Qualcol[8]+ "µ" + Qualcol[9]+ "µ" + Qualcol[10]+ "µ" + Qualcol[11];
            } 
            else if(Qualcol[0]!="" && Qualcol[2]!="" &&  Qualcol[4]!="-1" && Qualcol[5]!="" )
            {

                NewStr+="¥"+Qualcol[0]+"µ"+Qualcol[1]+"µ"+Qualcol[2]+ "µ" +Qualcol[3]+ "µ" +Qualcol[4]+ "µ" +Qualcol[5]+ "µ" +Qualcol[6]+ "µ" +Qualcol[7]+ "µ" + Qualcol[8]+ "µ" + Qualcol[9]+ "µ" + Qualcol[10]+ "µ" + Qualcol[11];

            }
          
           else
           {
           alert("Please Fill the mandatory filed");
           return false;
           }  
               
    }
               
        var Data = "1Ø"+ NewStr ;
        cursorwait();
        document.getElementById("btnSave").disabled=true;
    
        ToServer(Data, 1);
            
    }
    
    function FromServer(Arg, Context) 
    {
          switch (Context)
           {
            
               case 1:
               {
                    var Data = Arg.split("Ø");
                    cursordefault();
                    alert(Data[1]);
                    document.getElementById("btnSave").disabled=false;
                   if (Data[0] == 0) 
                    window.open('Loan_Enquiry.aspx','_self');
                    break;

               }       
               case 2:
               {
         
                    var Data = Arg.split("Ø");
                    for (n = 1; n <= row.length - 1; n++)
                    {     
               
                         document.getElementById("txtREmp_Name"+n).value=Data;      
               
                   }
                break;
                   
                  

               }   
             }
          } 

     
           function Fill(){
                table_fill();
               
                }


        function updateValue(id)
        {       
            row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
          
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                   if(id==n)
                   {
                        var Name=document.getElementById("txtName"+n).value;
                   
                            var Address=document.getElementById("txtAddress"+n).value;
                        
                            var Mobile=document.getElementById("txtMobile"+n).value;
                    
                            var AlterMobile=document.getElementById("txtAlterMobile"+n).value;
                    
                            //////var LoanProduct=document.getElementById("cmbLoanproduct"+n).value;
                            var LoanProduct = document.getElementById("cmbLoanproduct"+n).options[document.getElementById("cmbLoanproduct"+n).selectedIndex].text;
                            var LoanAmount=document.getElementById("txtLoanAmount"+n).value;
                            
                            var Remarks=document.getElementById("txtRemarks"+n).value;
                        
                            var RefEmp_code=document.getElementById("txtREmp_Code"+n).value;
                            var RefEmp_Name=document.getElementById("txtREmp_Name"+n).value;
                            var RefContact=document.getElementById("txtRefContact"+n).value;
                            var Sangam_ID=document.getElementById("txtsangamID"+n).value;
                          
                            var Sangam_Name=document.getElementById("txtsangamName"+n).value;

                            NewStr+="¥"+Name+"µ"+Address+"µ"+Mobile+"µ"+AlterMobile+"µ"+document.getElementById("cmbLoanproduct"+n).value+"µ"+LoanAmount+"µ"+Remarks+"µ"+RefEmp_code+"µ"+RefEmp_Name+"µ"+RefContact+"µ"+Sangam_ID+"µ"+Sangam_Name+"µ"+LoanProduct;
                          
                   }
                   else
                   {
                        NewStr+="¥"+row[n];
                   }
                }
                document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value=NewStr;
              
        }
        
        function AddNewRow() {          
                      
                if (document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value != "") {      
                      
                    row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
                  
                    var Len = row.length - 1; 
                     
                    col = row[Len].split("µ");
                  
                  
                         
                    if (col[0] != "" && col[2] != "" && col[4] != "-1" && col[5] != "") {
                    
                        document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value += "¥µµµµ-1µµµµµµµ";                         
                       
                    }
                }
               else    
               {  
                document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value = "¥µµµµ-1µµµµµµµ";
                
                }
                table_fill();
            }
       
       
        function DeleteRow(id)
        {
           
                row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
                var NewStr=""
                for (n = 1; n <= row.length-1 ; n++) {
                    if(id!=n)                  
                        NewStr+="¥"+row[n];                   
                
                }
                document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value=NewStr;
                if(document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value=="")
                document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value= "¥µµµµµµµµµµµ";
                table_fill();
        }
        
        function DisableColumn(value_,row_no){
            if(value_==1){
                document.getElementById("cmbSeniorCitizen"+row_no).disabled=true;
                document.getElementById("cmbWomen"+row_no).disabled=true;
                document.getElementById("cmbSeniorCitizen"+row_no).value="-1";
                document.getElementById("cmbWomen"+row_no).value ="-1";             }
            else
            {
               document.getElementById("cmbSeniorCitizen"+row_no).disabled =false;
               document.getElementById("cmbWomen"+row_no).disabled=false;
            }
       }
       function setFocus() {
      
    document.getElementById("txtMobile"+n).Focus();
}

 function checkvalue(value,n) {
   
   
   if(value.toString().length<5)
   {

  
   alert("Please enter minimum 5 digit number");        
    document.getElementById("txtLoanAmount"+col[5]).focus();      
    return false;
   }

}
    
function checkEmployee(value,n) {
   
        var Data = "2Ø"+ value ;
      
       
    
        ToServer(Data, 2);
            
   

}


     function checkPhone(phone,n) {
   
        var filter = /^[0-9]{10}$/;
   
       if(phone.match(filter))
       {
       return true;
       }
       else
       {
        alert('Please provide a valid phone number');
       document.getElementById("txtMobile"+col[2]).focus();   
        
        return false;

       }
    }
       function table_fill() 
       {
         
         if (document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnLoanEnquiry.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:4%;text-align:center'>#</td>";
             
            tab += "<td style='width:8%;text-align:center'><b>Name*</b></td>"; 
            tab += "<td style='width:8%;text-align:center'><b>Place Name</b></td>";   
            tab += "<td style='width:8%;text-align:center'><b>Mobile No*</b></td>";
            tab += "<td style='width:8%;text-align:center'><b>Alternative Phone Number </b></td>";    
            tab += "<td style='width:9%;text-align:center'><b>Loan Product*</b></td>";  
            tab += "<td style='width:8%;text-align:center'><b>Loan Amount*</b></td>";   
            tab += "<td style='width:8%;text-align:center'><b>Remarks </b></td>";   
            tab += "<td style='width:6%;text-align:center'><b>Referred by Emp_Code </b></td>"; 
            tab += "<td style='width:8%;text-align:center'><b>Referred by Emp_Name </b></td>"; 
            tab += "<td style='width:8%;text-align:center'><b>Referred by Contact No </b></td>"; 
            tab += "<td style='width:6%;text-align:center'><b>Sangam ID </b></td>"; 
            tab += "<td style='width:8%;text-align:center'><b>Sangam Name </b></td>";           
            tab +="<td style='width:3%;text-align:left'></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       

            row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
          
            for (n = 1; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ");
                
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  else 
                  {
                        row_bg = 0;
                        tab += "<tr class='tblQalBody'; style='text-align:center; height:21px; padding-left:20px;'>";
                  }
                  tab += "<td style='width:4%;text-align:center' >" + n  + "</td>";  
                 
                  var Name="";
                  var addr;
                  
                    var mobile;
                    var Altermobile;
                    var LPur;
                    var LAmt;
                    var Remar;
                    var Leading;
                    var Sangam_ID;
                    var REmp_Code;
                    var REmp_Name;
                    var Sangam_Name;
                    var RefContact;
                    if(col[0]!="")
                    {
                    //  tab += "<td style='width:10%;text-align:left'>" + col[0] + "</td>";
                      Name = col[0];
                      
                       var txtName = "<input id='txtName" + n + "' name='txtName" + n + "' value='" + Name + "'    type='Text'  'maxlength='30' style='width:99%;' class='NormalText' />";
               
                    tab += "<td style='width:8%;text-align:left' >"+ txtName +"</td>";
                    }
                    else
                    {
                    var txtName = "<input id='txtName" + n + "' name='txtName" + n + "' type='Text' maxlength='30' style='width:99%;' class='NormalText' onchange='updateValue("+n+")'/>";
                    tab += "<td style='width:8%;text-align:left' >"+ txtName +"</td>";          
           
                       
                    }
                    if(col[1]!="")
                    {
                 //  tab += "<td style='width:15%;text-align:left'>" + col[1] + "</td>";
                      addr = col[1];
                       var txtAddress = "<input id='txtAddress" + n + "' name='txtAddress" + n + "' value='" + addr + "'  type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='60'  class='NormalText' onkeypress='return TextAreaCheck(event)'  onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAddress +"</td>";
                      
                    }
                    else
                    {
                    var txtAddress = "<input id='txtAddress" + n + "' name='txtAddress" + n + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='60' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAddress +"</td>";
                       
                    }
                    if(col[2]!="")
                    {
                   //   tab += "<td style='width:10%;text-align:left'>" + col[2] + "</td>";
                      mobile = col[2];
                      var txtMobile = "<input id='txtMobile" + n + "' name='txtMobile" + n + "'  value='" + mobile + "' type='Text'  style='width:99%;' class='NormalText' maxlength='10' onkeypress='return NumericCheck(event)'  onchange='updateValue("+n+")'    />";
           
                   
                    tab += "<td style='width:8%;text-align:left' >"+ txtMobile +"</td>";
                    }
                    else
                    {
                    var txtMobile = "<input id='txtMobile" + n + "' name='txtMobile" + n + "'  type='Text'  style='width:99%;' class='NormalText' maxlength='10' onkeypress='return NumericCheck(event)'  onchange='updateValue("+n+")'  onblur='return checkPhone(this.value,"+ n +")' />";
           
                   
                    tab += "<td style='width:8%;text-align:left' >"+ txtMobile +"</td>";
                       
                    }
                     if(col[3]!="")
                    {
                  // tab += "<td style='width:10%;text-align:left'>" + col[3] + "</td>";
                      Altermobile   = col[3];
                    var txtAlterMobile = "<input id='txtAlterMobile" + n + "' name='txtAlterMobile" + n + "' value='" + Altermobile + "' type='Text'  style='width:99%;' class='NormalText' maxlength='10' onkeypress='return NumericCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAlterMobile +"</td>";
                       
                    }
                    else
                    {
                    var txtAlterMobile = "<input id='txtAlterMobile" + n + "' name='txtAlterMobile" + n + "' type='Text'  style='width:99%;' class='NormalText' maxlength='12' onkeypress='return NumericCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtAlterMobile +"</td>";
                       
                    }
                    
                    var select = "<select id='cmbLoanproduct" + n + "' class='NormalText' name='cmbLoanproduct" + n + "' style='width:100%'  onchange='updateValue(" + n +")'  >";
                    
                    if(col[4]==-1)
                        select += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                         select += "<option value='-1'>-----Select-----</option>";
                    if(col[4]==1)
                        select += "<option value='1' selected=true>Micro Housing Loan</option>";
                    else
                        select += "<option value='1'>Micro Housing Loan</option>";
                    if(col[4]==2)
                        select += "<option value='2' selected=true>Agri Loan</option>";
                    else
                        select += "<option value='2'>Agri Loan</option>";
                    if(col[4]==3)
                        select += "<option value='3' selected=true>Business Loan</option>";
                    else
                        select += "<option value='3'>Business Loan</option>";
                    if(col[4]==4)
                        select += "<option value='4' selected=true>LAP</option>";
                    else
                        select += "<option value='4'>LAP</option>";
                    if(col[4]==5)
                        select += "<option value='5' selected=true>2 wheeler loan</option>";
                    else
                        select += "<option value='5'>2 wheeler loan</option>";
                    if(col[4]==6)
                        select += "<option value='5' selected=true>3&4 wheeler loan</option>";
                    else
                        select += "<option value='5'>3&4 wheeler loan</option>";
                    if(col[4]==7)
                        select += "<option value='7' selected=true>Gold Loan</option>";
                    else
                        select += "<option value='7'>Gold Loan</option>";
                    if(col[4]==8)
                        select += "<option value='8' selected=true>MSME Loan</option>";
                    else
                        select += "<option value='8'>MSME Loan</option>";
                    if(col[4]==9)
                        select += "<option value='9' selected=true>Dream Housing Loan</option>";
                    else
                        select += "<option value='9'>Dream Housing Loan</option>";
                    if(col[4]==10)
                        select += "<option value='10' selected=true>Personal Loan</option>";
                    else
                        select += "<option value='10'>Personal Loan</option>";
                    if(col[4]==11)
                        select += "<option value='11' selected=true>Global Career Education</option>";
                    else
                        select += "<option value='11'>Global Career Education</option>";
                    
                    if(col[4]==12)
                        select += "<option value='12' selected=true>Light Commercial Vehicle Loan</option>";
                    else
                        select += "<option value='12'>Light Commercial Vehicle Loan</option>";
                     if(col[4]==13)
                        select += "<option value='13' selected=true>LRD</option>";
                    else
                        select += "<option value='13'>LRD</option>";
                      if(col[4]==14)
                        select += "<option value='14' selected=true>Solar/Clean Energy</option>";
                    else
                        select += "<option value='14'>Solar/Clean Energy</option>";
                    tab += "<td style='width:9%;text-align:left'>" + select + "</td>";
//////                    if(col[4]!="")
//////                    {
//////                 // tab += "<td style='width:15%;text-align:left'>" + col[4] + "</td>";
//////                      LPur = col[4];
//////                       var txtLoanPurpose = "<input id='txtLoanPurpose" + n + "' name='txtLoanPurpose" + n + "' value='" + LPur + "' type='Text'  style='width:99%;' word-wrap:'break-word;' maxlength='50' style='width:99%;' class='NormalText'  onchange='updateValue("+n+")' />";
//////                    tab += "<td style='width:9%;text-align:left' >"+ txtLoanPurpose +"</td>";
//////                     
//////                    }
//////                    else
//////                    {
//////////                    var txtLoanPurpose = "<input id='txtLoanPurpose" + n + "' name='txtLoanPurpose" + n + "' type='Text'  style='width:99%;' word-wrap:'break-word;' maxlength='50' style='width:99%;' class='NormalText'  onchange='updateValue("+n+")' />";
//////////                    tab += "<td style='width:9%;text-align:left' >"+ txtLoanPurpose +"</td>";
//////                    var select = "<select id='cmbLoanproduct" + col[4] + "' class='NormalText' name='cmbLoanproduct" + col[4] + "' `style='width:100%' onchange='updateValue("+n+")'  >";
//////                    select += "<option value='-1'>-----Select-----</option>";
//////                    select += "<option value='1'>Housing Loan</option>";
//////                    select += "<option value='2'>Agri Loan</option>";
//////                    select += "<option value='3'>Business Loan</option>";
//////                    select += "<option value='4'>LAP</option>";
//////                    select += "<option value='5'>2 wheeler loan</option>";
//////                    select += "<option value='6'>3&4 wheeler loan</option>";
//////                    select += "<option value='7'>Gold Loan</option>";
//////                    select += "<option value='8'>MSME Loan</option>";
//////                    tab += "<td style='width:9%;text-align:center'>" + select + "</td>"; 
//////                    }
                     if(col[5]!="")
                    {
               // tab += "<td style='width:5%;text-align:left'>" + col[5] + "</td>";
                      LAmt = col[5];
                       var txtLoanAmount = "<input id='txtLoanAmount" + n + "' name='txtLoanAmount" + n + "' value='" + LAmt + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='10' onkeypress='return NumericCheck(event)'  onblur='return checkvalue(this.value,"+ n +")'  onchange='updateValue("+n+")'   />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtLoanAmount +"</td>";
                      
                    }
                    else
                    {
                    var txtLoanAmount = "<input id='txtLoanAmount" + n + "' name='txtLoanAmount" + n + "' type='Text' style='width:99%;'  class='NormalText'  maxlength='10' onkeypress='return NumericCheck(event)' onchange='updateValue("+n+")'  onblur='return checkvalue(this.value,"+ n +")'  />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtLoanAmount +"</td>";
                       
                    }
                      if(col[6]!="")
                    {
                  //tab += "<td style='width:15%;text-align:left'>" + col[6] + "</td>";
                      Remar =col[6] ;
                        var txtRemarks = "<input id='txtRemarks" + n + "' name='txtRemarks" + n + "'  value='" + Remar + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;'  maxlength='60' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtRemarks +"</td>";
                       
                    }
                    else
                    {
                    var txtRemarks = "<input id='txtRemarks" + n + "' name='txtRemarks" + n + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;'  maxlength='60' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtRemarks +"</td>";
                       
                    }
                    if(col[7]!="")
                    {
                      //tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                       REmp_Code = col[7];
                       var txtREmp_Code = "<input id='txtREmp_Code" + n + "' name='txtREmp_Code" + n + "'  value='" + REmp_Code + "' type='Text' onkeypress='return NumericCheck(event)' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='6' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:6%;text-align:left' >"+ txtREmp_Code +"</td>";
                       

                    }
                    else
                    {
                    var txtREmp_Code = "<input id='txtREmp_Code" + n + "' name='txtREmp_Code" + n + "' type='Text'  onkeypress='return NumericCheck(event)' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;'  maxlength='6' style='width:99%;' class='NormalText'  onblur='checkEmployee(this.value,"+ n +")' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:6%;text-align:left' >"+ txtREmp_Code +"</td>";
                       
                    }
                    if(col[8]!="")
                    {
                      //tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                       REmp_Name = col[8];
                       var txtREmp_Name = "<input id='txtREmp_Name" + n + "' name='txtREmp_Name" + n + "'  value='" + REmp_Name + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='30' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtREmp_Name +"</td>";
                       

                    }
                    else
                    {
                    var txtREmp_Name = "<input id='txtREmp_Name" + n + "' name='txtREmp_Name" + n + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;'  maxlength='30' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")'  />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtREmp_Name +"</td>";
                       
                    }
                    if(col[9]!="")
                    {
                  
                      RefContact   = col[9];
                    var txtRefContact = "<input id='txtRefContact" + n + "' name='txtRefContact" + n + "' value='" + RefContact + "'  type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' class='NormalText' maxlength='12' onkeypress='return NumericCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtRefContact +"</td>";
                       
                    }
                    else
                    {
                    var txtRefContact = "<input id='txtRefContact" + n + "' name='txtRefContact" + n + "' type='Text'  style='width:99%;' class='NormalText' maxlength='12' onkeypress='return NumericCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtRefContact +"</td>";
                       
                    }
                    if(col[10]!="")
                    {
                      //tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                       Sangam_ID = col[10];
                       var txtsangamID = "<input id='txtsangamID" + n + "' name='txtsangamID" + n + "'  value='" + Sangam_ID + "' type='Text' onkeypress='return NumericCheck(event)' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='8' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:6%;text-align:left' >"+ txtsangamID +"</td>";
                       

                    }
                    else
                    {
                    var txtsangamID = "<input id='txtsangamID" + n + "' name='txtsangamID" + n + "' type='Text' onkeypress='return NumericCheck(event)' TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;'  maxlength='8' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:6%;text-align:left' >"+ txtsangamID +"</td>";
                       
                    }
                    if(col[11]!="")
                    {
                      //tab += "<td style='width:10%;text-align:left'>" + col[7] + "</td>";
                       Sangam_Name = col[11];
                       var txtsangamName = "<input id='txtsangamName" + n + "' name='txtsangamName" + n + "'  value='" + Sangam_Name + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;' maxlength='30' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtsangamName +"</td>";
                       

                    }
                    else
                    {
                    var txtsangamName = "<input id='txtsangamName" + n + "' name='txtsangamName" + n + "' type='Text'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%;' word-wrap:'break-word;'  maxlength='30' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)' onchange='updateValue("+n+")' />";
                    tab += "<td style='width:8%;text-align:left' >"+ txtsangamName +"</td>";
                       
                    }
                    if(col[0] == "" && col[1] == "" && col[2] == "" && col[3] == "" && col[4] ==" -1 " && col[5] == ""  && col[6] == ""  && col[7] == "" && col[8] == "" && col[9] == "" && col[10] == "" && col[11] == "") 
                    {
                    
                    //tab +="<td style='width:3%;text-align:left'></td>";
                    tab += "<td style='width:3%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        
                  
                    
                    }
                    else
                    {
                       tab += "<td style='width:3%;text-align:center' onclick=DeleteRow('" + n + "')><img  src='../Image/cross.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='Delete'/></td>";
                        
               
                    }
                    tab += "</tr>";
                }
                tab += "</table></div></div></div>";
                document.getElementById("<%= pnLoanEnquiry.ClientID %>").innerHTML = tab;
                setTimeout(function() {
                document.getElementById("txtAttendee"+(n-1)).focus().select();
                }, 4);
                }
                else
                document.getElementById("<%= pnLoanEnquiry.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
        }
       
       function CreateNewRow(e, val) {
      
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(val);
                AddNewRow();                          
            }
         }
         


    </script>
    <br />
    <div>
    <table align="center" style="width:100%; margin: 0px auto;">
              
            <tr>
                    <td style="width:90% ;text-align:center;" colspan="4" class="mainhead" >
                    <strong>Enquiry</strong>
                    <img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New"/>
                   
                    </td>
            </tr> 
            <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                    <asp:Panel ID="pnLoanEnquiry" runat="server">
                    </asp:Panel>
                    </td>
           </tr>
         <tr style="text-align:center;">
              <td  colspan="4" style="text-align:center;">
              <asp:Panel ID="pnDisscussionDtl" runat="server">
              </asp:Panel>
             </td>
        </tr>
        <tr style="text-align:center;">
                <td  colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;
                <input id="btnExit" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                
                <asp:HiddenField 
                    ID="hdnStaffCustomer" runat="server" />
                <asp:HiddenField 
                    ID="hdnSeniorCitizen" runat="server" />
                <asp:HiddenField 
                    ID="hdnWomen" runat="server" />
                <asp:HiddenField 
                    ID="hdnLoanEnquiry" runat="server" />
               <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:HiddenField 
                    ID="hdnResponse" runat="server" />
                
               </td>
         </tr>
    </table>
    </div> 
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </asp:Content>
