﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="HO_Response.aspx.vb" Inherits="HO_Response"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
     
   
    <script language="javascript" type="text/javascript">
    
      
   
     function cursorwait(e) {
            document.body.className = 'wait';}
     function cursordefault(e) {
            document.body.className = 'default';}
      
    function btnExit_onclick() 
    {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

    function btnSave_onclick() {
     var ret = UpdateValue();
     if (ret == 0) return false;

            var Data = "1Ø" + document.getElementById("<%= hdnResponse.ClientID %>").value;
           
            ToServer(Data, 1);

    }

    function DisableColumn(value_,row_no){
 
            if(value_!=4)
            {
                document.getElementById("cmbAssigned"+row_no).disabled=true;
              
                document.getElementById("cmbAssigned"+row_no).value="-1";
            }
            else
            {
               document.getElementById("cmbAssigned"+row_no).disabled =false;
             
            }
       }
     function UpdateValue() {
            document.getElementById("<%= hdnResponse.ClientID %>").value = "";
            
            row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
         
            for (n = 0; n <= row.length - 1; n++) 
            {
              col = row[n].split("µ");

                if ((document.getElementById("cmbstatus" + n).value != -1) && (document.getElementById("txtExplanation" + n).value == ""))
                 {
                    
                        alert("Enter Reason");
                        document.getElementById("txtExplanation" + n).focus();
                        return false;
                }
                    
                if ((document.getElementById("cmbstatus" + n).value == 4) && (document.getElementById("cmbAssigned" + n).value == -1) )
                {
                   
                        alert("Choose Any Credit Officer");
                        document.getElementById("cmbAssigned" + n).focus();
                        return false;
                }        
                if((col[15]!=document.getElementById("cmbstatus" + n).value)||(col[16]!=document.getElementById("txtExplanation" + n).value)||(col[17]!=document.getElementById("cmbAssigned" + n).value))
                {
                            document.getElementById("<%= hdnResponse.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmbstatus" + n).value + "µ" + document.getElementById("txtExplanation" + n).value+ "µ" +document.getElementById("cmbAssigned" + n).value;
                }    
                            
                          
            }
           
  
      }   



    function FromServer(Arg, Context) 
    {
          switch (Context)
           {
            
               case 1:
               {
                    var Data = Arg.split("Ø");
                    cursordefault();
                    alert(Data[1]);
                    document.getElementById("btnSave").disabled=false;
                     if (Data[0] == 0) 
                    window.open('HO_Response.aspx','_self');
                    break;
               }       
              
             }
          } 
        function CreateSelect(n,value) {   
         
            if (value == -1)
                var select1 = "<select id='cmbAssigned" + n + "' class='NormalText' name='cmbAssigned" + n + "' style='width:100%' onchange='updateValue("+n+")' disabled=true  >"; 
            else
                var select1 = "<select id='cmbAssigned" + n + "' class='NormalText' name='cmbAssigned" + n + "' style='width:100%'  onchange='updateValue("+n+")' disabled=true >"; 
               
           

            var rows =document.getElementById("<%= hdnQualification.ClientID %>") .value.split("Ñ");  
          
            for (a = 1; a < rows.length; a++) {
             var cols = rows[a].split("ÿ");
         
          
             if(cols[0]==value)
               select1 += "<option value='"+ cols[0] +"' selected=true>"+ cols[1] +"</option>";
            else
                select1 += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                
            }  
           select1+="</select>"
          return select1;
            }
      function table_fill() 
       {
     
         if (document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnLoanEnquiry.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:1%;text-align:center'>#</td>";    
            tab += "<td style='width:7%;text-align:center'><b>Name</b></td>"; 
            tab += "<td style='width:6%;text-align:center'><b>Place Name</b></td>";   
            tab += "<td style='width:6%;text-align:center'><b>Mobile No</b></td>";
            tab += "<td style='width:6%;text-align:center'><b>Alternative Phone Number </b></td>";    
            tab += "<td style='width:5%;text-align:center'><b>Loan Product</b></td>";  
            tab += "<td style='width:5%;text-align:center'><b>Loan Amount</b></td>";   
            tab += "<td style='width:8%;text-align:center'><b>Remarks </b></td>";    
            tab += "<td style='width:4%;text-align:center'><b>Sangam ID  </b></td>";
            tab += "<td style='width:4%;text-align:center'><b>Sangam Name </b></td>"; 
            tab += "<td style='width:5%;text-align:center'><b>Enquiry Date </b></td>";   
            tab += "<td style='width:4%;text-align:center'><b>Branch ID </b></td>"; 
             tab += "<td style='width:7%;text-align:center'><b>Branch Name </b></td>";   
            tab += "<td style='width:5%;text-align:center'><b>Entered by Emp_code </b></td>";   
            tab += "<td style='width:7%;text-align:center'><b>Entered by Emp_name </b></td>";      
            tab += "<td style='width:5%;text-align:center'><b>Status</b></td>";   
            tab += "<td style='width:7%;text-align:center'><b>Explanation </b></td>";
             tab += "<td style='width:9%;text-align:center'><b>Assigned To</b></td>";         
           // tab +="<td style='width:1%;text-align:left'></td>";
            tab += "</tr>";     
            tab += "</table></div>";  
            tab += "<div id='ScrollDiv' style='width:100%; height:auto; overflow-x: scroll;margin: 0px auto;' class='mainhead' >";
            tab += "<table style='table-layout:fixed;width:100%;margin:0px auto;font-family:'cambria';' align='center'>";       
        
            row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
          
             var Explain; 
            for (n = 0; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ"); 
             
               
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        //tab += "<tr class='tblQalBody'; style='text-align:center; height:20px; padding-left:20px;'>";
                       tab += "<tr height=20px;  class='tblQal'>";  
                        
                  }
                  else 
                  {
                        row_bg = 0;
                        //tab += "<tr class='tblQalBody'; style='text-align:center; height:20px; padding-left:20px;'>";
                         tab += "<tr height=20px;  class='tblQal'>";  
                  }
                 i=n+1;
             
                    tab += "<td style='width:1%;text-align:center'>" + i + "</td>";  
                    tab += "<td style='width:7%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[11] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[13] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[14] + "</td>";
                    
                   

                     var select = "<select id='cmbstatus" + n + "' class='NormalText' name='cmbstatus" + n + "' `style='width:100%' onchange='DisableColumn(this.options[this.selectedIndex].value," + n +")' >";
             
                    if(col[15]==-1)
                        select += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                         select += "<option value='-1'>-----Select-----</option>";
                    if(col[15]==1)
                        select += "<option value='1' selected=true>Non contactable</option>";
                    else
                        select += "<option value='1'>Non contactable</option>";
                    if(col[15]==2)
                        select += "<option value='2' selected=true>On Hold</option>";
                    else
                        select += "<option value='2'>On Hold</option>";
                    if(col[15]==3)
                        select += "<option value='3' selected=true>Need more information</option>";
                    else
                        select += "<option value='3'>Need more information</option>";
                    if(col[15]==4)
                        select += "<option value='4' selected=true>Approved for processing</option>";
                    else
                        select += "<option value='4'>Approved for processing</option>";
                     if(col[15]==5)
                        select += "<option value='5' selected=true>Rejected by HQ</option>";
                    else
                        select += "<option value='5'>Rejected by HQ</option>";
                     if(col[15]==6)
                        select += "<option value='6' selected=true>Sanctioned by HQ</option>";
                    else
                        select += "<option value='6'>Sanctioned by HQ</option>";
                    if(col[15]==7)
                        select += "<option value='7' selected=true>Return by HQ</option>";
                    else
                        select += "<option value='7'>Return by HQ</option>";
                    tab += "<td style='width:5%;text-align:center'>" + select + "</td>";
                   
                  
                     if(col[16]!="")
                    {

                       Explain =col[16];
                   
                       var txtExplanation = "<input id='txtExplanation" + n + "' name='txtExplanation" + n + "'  type='Text'   value='" + Explain + "'  TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;'maxlength='50' style='width:99%;' class='NormalText' onkeypress='return TextAreaCheck(event)'   />";
                        tab += "<td style='width:7%;text-align:left' >"+ txtExplanation +"</td>";
                    }
                    else
                     {
                        var txtExplanation = "<input id='txtExplanation" + n + "'  name='txtExplanation" + n + "'  type='Text'    TextMode='MultiLine' Wrap='True' Rows='1' style='width:99%; word-wrap:'break-word;'maxlength='50' style='width:99%;' class='NormalText'  onkeypress='return TextAreaCheck(event)'  />";
           
                        tab += "<td style='width:7%;text-align:left' >"+ txtExplanation +"</td>";
               
                     }  
                      if((col[14]!=4) || (col[17]!=-1))  
                    {           
                    var select1 = CreateSelect(n,col[17])                                                
                    tab += "<td style='width:9%;text-align:left'>" + select1 + "</td>";
                    }
                    else
                    {
                     var select1 = CreateSelect(n,col[17])
                   tab += "<td style='width:9%;text-align:left'>" + select1 + "</td>";
                    }              

                    tab += "</tr>";
                }
                tab += "</table></div></div>";
                document.getElementById("<%= pnLoanEnquiry.ClientID %>").innerHTML = tab;
               
                }
                else
                document.getElementById("<%= pnLoanEnquiry.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
        }
       
       function CreateNewRow(e, val) {
      
            var n = (window.Event) ? e.which : e.keyCode;
            if (n == 13 || n == 9) {
                updateValue(val);
                AddNewRow();                          
            }
         }
         


    </script>
    <br />
    <div>
    <table align="center" style="width:100%; margin: 0px auto;">
              
            <tr>
                    <td style="width:100% ;text-align:center;" colspan="4" class="mainhead" >
                    <strong>Response</strong>
                    <img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New"/>
                   
                    </td>
            </tr> 
            <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                    <asp:Panel ID="pnLoanEnquiry" runat="server">
                    </asp:Panel>
                    </td>
           </tr>
         <tr style="text-align:center;">
              <td  colspan="4" style="text-align:center;">
              <asp:Panel ID="pnDisscussionDtl" runat="server">
              </asp:Panel>
             </td>
        </tr>
        <tr style="text-align:center;">
                <td  colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;
                <input id="btnExit" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
                <asp:HiddenField 
                    ID="hdnQualification" runat="server" />
                <asp:HiddenField 
                    ID="hdnStaffCustomer" runat="server" />
                <asp:HiddenField 
                    ID="hdnSeniorCitizen" runat="server" />
                <asp:HiddenField 
                    ID="hdnWomen" runat="server" />
                <asp:HiddenField 
                    ID="hdnLoanEnquiry" runat="server" />
                <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:HiddenField 
                    ID="hdnResponse" runat="server" />
                
               </td>
         </tr>
    </table>
    </div> 
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </asp:Content>
