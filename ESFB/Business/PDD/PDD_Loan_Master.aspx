﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PDD_Loan_Master.aspx.vb" Inherits="PDD_Loan_Master" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
  <html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
          .Button
        {
            width:100%;
            height:36px;
            font-weight:bold;
            line-height:40px;
            text-align:center;
            color:#3F719A;
             cursor:pointer;
            background: -moz-radial-gradient(center, ellipse cover, #F1F3F4 0%, #EEB8A6 90%, #F1F3F4 100%);
        }
        .Button:hover
        {
            background: -moz-radial-gradient(center, ellipse cover, #A34747 0%, #A34747 0%, #A34747  100%);
            color:#FFF;
        }   
     .bg
     {
         background-color:#FFF;
     }
     </style>
    <script language="javascript" type="text/javascript">
    function delete_pdd_row(pdd_id){
         if (confirm("Are You sure to Delete?")==1){
            var ToData = "4Ø3Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value + "Ø" + pdd_id ;
            ToServer(ToData, 4);
         }
    }
    function delete_product_row(prod_id){
         if (confirm("Are You sure to Delete?")==1){
            var ToData = "4Ø2Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value + "Ø" + prod_id ;
            ToServer(ToData, 4);
         }
    }
    function edit_pdd_row(pdd_id){
        var ToData = "5Ø3Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value + "Ø" + pdd_id ;
        ToServer(ToData, 5);
    }
    function edit_product_row(prod_id){
        var ToData = "5Ø2Ø" + document.getElementById("<%= cmbCategory.ClientID %>").value + "Ø" + prod_id ;
        ToServer(ToData, 5);
    }
    function table_fill() {   
        document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
        var row_bg = 0;
        var tab = "";
        tab += "<br/><div class=mainhead style='width:85%; height:25px; padding-top:0px;margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;font-family:'cambria';' align='center'>";
        tab += "<tr>";
        var rowData;
        if (document.getElementById("<%= hid_tab.ClientID %>").value==1){
            tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
            tab += "<td style='width:75%;text-align:left' class=NormalText>Category Name</td>";
            rowData = document.getElementById("<%= hid_category.ClientID %>").value;
        }else if (document.getElementById("<%= hid_tab.ClientID %>").value==2){
            tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
            tab += "<td style='width:75%;text-align:left' class=NormalText>Product Name</td>";
            tab += "<td style='width:5%;'/></td>";
            var categoryCombo = document.getElementById("<%= hidCmbCate.ClientID %>").value;
            if(categoryCombo != -1){
                document.getElementById("<%= cmbCategory.ClientID %>").value = categoryCombo;
            }
            rowData = document.getElementById("<%= hid_product.ClientID %>").value;
        }else if (document.getElementById("<%= hid_tab.ClientID %>").value==3){
            tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
            tab += "<td style='width:75%;text-align:left' class=NormalText>PDD Name</td>";
            tab += "<td style='width:5%;'/></td>";
            var categoryCombo = document.getElementById("<%= hidCmbCate.ClientID %>").value;
            if(categoryCombo != -1){
                document.getElementById("<%= cmbCategory.ClientID %>").value = categoryCombo;
            }
            rowData = document.getElementById("<%= hid_pdd.ClientID %>").value;
        }
        tab += "</tr></table></div><div style='width:85%; height:128px; overflow:auto; margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
        if(rowData != ""){
            row = rowData.split("¥");
            for (n = 0; n <= row.length - 1; n++) {
                col = row[n].split("µ");
                if (row_bg == 0) {
                    row_bg = 1;
                    tab += "<tr class=sub_first>";
                }else {
                    row_bg = 0;
                    tab += "<tr class=sub_second>";
                }
                tab += "<td style='width:15%;text-align:center;' >" + col[0] + "</td>";
                tab += "<td style='width:75%;text-align:left;' >" + col[1] + "</td>";
                if (document.getElementById("<%= hid_tab.ClientID %>").value==3){
                    tab += "<td style='width:5%;text-align:center' onclick='edit_pdd_row(" + col[2] + ")'><img  src='../../Image/edit1.png' style='align:middle;cursor:pointer;height:60%;width:60%;' /></td>";
                    tab += "<td style='width:5%;text-align:center' onclick='delete_pdd_row(" + col[2] + ")'><img  src='../../Image/cross.png' style='align:middle;cursor:pointer;' /></td>";
                }
                if (document.getElementById("<%= hid_tab.ClientID %>").value==2){
                    tab += "<td style='width:5%;text-align:center' onclick='edit_product_row(" + col[2] + ")'><img  src='../../Image/edit1.png' style='align:middle;cursor:pointer;height:60%;width:60%;' /></td>";
                    tab += "<td style='width:5%;text-align:center' onclick='delete_product_row(" + col[2] + ")'><img  src='../../Image/cross.png' style='align:middle;cursor:pointer;' /></td>";
                }
                tab += "</tr>";
            }
        }       
        tab += "</table></div>";
        document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
            //--------------------- Clearing Data ------------------------//
    }
    function CategoryTableFill() { //vidya
        var ToData = "1Ø" + document.getElementById("<%= hid_Tab.ClientID %>").value;
        ToServer(ToData, 1);
    }
    function FromServer(arg, context) {
        if (context == 1) {
            document.getElementById("<%= hid_category.ClientID %>").value =arg;
            table_fill();
        }else if (context == 2) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) {
                ComboFill(Data[2],"<%= cmbCategory.ClientID %>");
                if (document.getElementById("<%= hid_Tab.ClientID %>").value ==1){
                    document.getElementById("<%= txtCateName.ClientID %>").value = "";
                    CategoryTableFill();    
                }
                else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==2){
                    document.getElementById("<%= txtProductName.ClientID %>").value = "";
                    CategoryOnchange();
                }
                else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==3){
                    document.getElementById("<%= txtPddName.ClientID %>").value = "";
                    CategoryOnchange();
                }
            }
        }else if (context == 3) {
            var Data = arg.split("~");
            var tabid = document.getElementById("<%= hid_Tab.ClientID %>").value;
            if(tabid == 2){
                document.getElementById("<%= hid_product.ClientID %>").value = Data[0];
            }else if(tabid == 3){
                document.getElementById("<%= hid_pdd.ClientID %>").value = Data[0];
            }
            table_fill();
            return false;
        }else if (context == 4) {
            var Data = arg.split("Ø");
            alert(Data[1]);
            if (Data[0] == 0) {
                if (document.getElementById("<%= hid_Tab.ClientID %>").value ==2){
                    document.getElementById("<%= txtProductName.ClientID %>").value = "";
                    CategoryOnchange();
                }
                else if (document.getElementById("<%= hid_Tab.ClientID %>").value ==3){
                    document.getElementById("<%= txtPddName.ClientID %>").value = "";
                    CategoryOnchange();
                }
            }
        }else if (context == 5) {
            var Data = arg.split("Ø");
            var hidid = Data[1];
            if(hidid == 2){
                document.getElementById("<%= txtProductName.ClientID %>").value = Data[0];
            }else if(hidid == 3){
                document.getElementById("<%= txtPddName.ClientID %>").value = Data[0];
            }
            document.getElementById("<%= hidEditFlag.ClientID %>").value = "3";
            document.getElementById("<%= hidEditId.ClientID %>").value = Data[2];
        }else if (context == 6) {
               
        }
    }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }        
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function ClearCombo(control) {
        document.getElementById(control).options.length = 0;
        var option1 = document.createElement("OPTION");
        option1.value = -1;
        option1.text = " -----Select-----";
        document.getElementById(control).add(option1);
    }
    function CategoryClick() {
        document.getElementById("CategoryName").style.display = '';
        document.getElementById("CategoryCombo").style.display = 'none';
        document.getElementById("ProductName").style.display = 'none';
        document.getElementById("PDDName").style.display = 'none';   
         
        document.getElementById("<%= cmbCategory.ClientID %>").value=-1;
        document.getElementById("<%= txtCateName.ClientID %>").value="";
        document.getElementById("<%= txtProductName.ClientID %>").value="";
        document.getElementById("<%= txtPDDName.ClientID %>").value="";
        document.getElementById("<%= hidCmbCate.ClientID %>").value = -1;
            
        document.getElementById("<%= hid_tab.ClientID %>").value = 1;
        document.getElementById("<%= hid_category.ClientID %>").value = "";
        document.getElementById("<%= hid_product.ClientID %>").value = "";
        document.getElementById("<%= hid_pdd.ClientID %>").value = "";
        document.getElementById("<%= hidEditFlag.ClientID %>").value = 1;

        document.getElementById("<%= pnlDtls.ClientID %>").style.display='';
        document.getElementById("<%= pnlForms.ClientID %>").style.display='none';

        document.getElementById("1").style.background='-moz-radial-gradient(center, ellipse cover,  #A34747 0%, #A34747 0%, #A34747 100%)';
        document.getElementById("1").style.color='#FFF';
        document.getElementById("2").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
        document.getElementById("2").style.color='#3F719A';
        document.getElementById("3").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
        document.getElementById("3").style.color='#3F719A';    
        
        CategoryTableFill(); 
    }
    function ProductClick() {        
        document.getElementById("CategoryName").style.display = 'none';
        document.getElementById("CategoryCombo").style.display = '';
        document.getElementById("ProductName").style.display = '';
        document.getElementById("PDDName").style.display = 'none';   
         
        document.getElementById("<%= cmbCategory.ClientID %>").value=-1;
        document.getElementById("<%= txtCateName.ClientID %>").value="";
        document.getElementById("<%= txtProductName.ClientID %>").value="";
        document.getElementById("<%= txtPDDName.ClientID %>").value="";
        document.getElementById("<%= hidCmbCate.ClientID %>").value = -1;
            
        document.getElementById("<%= hid_tab.ClientID %>").value = 2;
        document.getElementById("<%= hid_category.ClientID %>").value = "";
        document.getElementById("<%= hid_product.ClientID %>").value = "";
        document.getElementById("<%= hid_pdd.ClientID %>").value = "";
        document.getElementById("<%= hidEditFlag.ClientID %>").value = 1;

        document.getElementById("<%= pnlDtls.ClientID %>").style.display='';
        document.getElementById("<%= pnlForms.ClientID %>").style.display='none';
           
        document.getElementById("1").style.background='-moz-radial-gradient(center, ellipse cover, #F1F3F4 20%, #EEB8A6 90%, #F1F3F4  100%)';
        document.getElementById("1").style.color='#3F719A';
        document.getElementById("2").style.background='-moz-radial-gradient(center, ellipse cover,#A34747 0%, #A34747 0%, #A34747 100%)';
        document.getElementById("2").style.color='#FFF';
        document.getElementById("3").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
        document.getElementById("3").style.color='#3F719A';

        table_fill();
    }
    function PDDClick() {
        document.getElementById("CategoryName").style.display = 'none';
        document.getElementById("CategoryCombo").style.display = '';
        document.getElementById("ProductName").style.display = 'none';
        document.getElementById("PDDName").style.display = '';   
         
        document.getElementById("<%= cmbCategory.ClientID %>").value=-1;
        document.getElementById("<%= txtCateName.ClientID %>").value="";
        document.getElementById("<%= txtProductName.ClientID %>").value="";
        document.getElementById("<%= txtPDDName.ClientID %>").value="";
        document.getElementById("<%= hidCmbCate.ClientID %>").value = -1;
            
        document.getElementById("<%= hid_tab.ClientID %>").value = 3;
        document.getElementById("<%= hid_category.ClientID %>").value = "";
        document.getElementById("<%= hid_product.ClientID %>").value = "";
        document.getElementById("<%= hid_pdd.ClientID %>").value = "";
        document.getElementById("<%= hidEditFlag.ClientID %>").value = 1;

        document.getElementById("<%= pnlDtls.ClientID %>").style.display='';
        document.getElementById("<%= pnlForms.ClientID %>").style.display='none';
           
        document.getElementById("1").style.background='-moz-radial-gradient(center, ellipse cover, #F1F3F4 20%, #EEB8A6 90%, #F1F3F4  100%)';
        document.getElementById("1").style.color='#3F719A';
        document.getElementById("2").style.background='-moz-radial-gradient(center, ellipse cover,#F1F3F4 20%, #EEB8A6 90%, #F1F3F4 100%)';
        document.getElementById("2").style.color='#3F719A';
        document.getElementById("3").style.background='-moz-radial-gradient(center, ellipse cover,#A34747 0%, #A34747 0%, #A34747 100%)';
        document.getElementById("3").style.color='#FFF';

        table_fill();
    }
    function btnSave_onclick(){
        var tabid = document.getElementById("<%= hid_tab.ClientID %>").value;
        var flag = document.getElementById("<%= hidEditFlag.ClientID %>").value;
        var ToData;
        if(tabid == 1){
            var category = document.getElementById("<%= txtCateName.ClientID %>").value;
            ToData = "2Ø1Ø" + category +"Ø1Ø1";
        }else if(tabid == 2){
            var categoryCombo = document.getElementById("<%= cmbCategory.ClientID %>").value;
            document.getElementById("<%= hidCmbCate.ClientID %>").value = categoryCombo;
            var product = document.getElementById("<%= txtProductName.ClientID %>").value;
            if(flag == 1){
                ToData = "2Ø2Ø" + categoryCombo + "Ø" + product + "Ø" + flag;
            }else if(flag == 3){
                var prodId = document.getElementById("<%= hidEditId.ClientID %>").value;
                ToData = "2Ø2Ø" + categoryCombo + "Ø" + product + "Ø" + flag + "Ø" + prodId;
            }
        }else if(tabid == 3){
            var categoryCombo = document.getElementById("<%= cmbCategory.ClientID %>").value;
            document.getElementById("<%= hidCmbCate.ClientID %>").value = categoryCombo;
            var pdd = document.getElementById("<%= txtPDDName.ClientID %>").value;
            if(flag == 1){
                ToData = "2Ø3Ø" + categoryCombo + "Ø" + pdd + "Ø" + flag;
            }else if(flag == 3){
                var pddId = document.getElementById("<%= hidEditId.ClientID %>").value;
                ToData = "2Ø3Ø" + categoryCombo + "Ø" + pdd + "Ø" + flag + "Ø" + pddId;
            }
        }
        ToServer(ToData, 2);
    }
    function CategoryOnchange(){
        var Data = document.getElementById("<%= cmbCategory.ClientID %>").value;
        if(document.getElementById("<%= hidCmbCate.ClientID %>").value != -1 && document.getElementById("<%= hidCmbCate.ClientID %>").value != ''){
            Data = document.getElementById("<%= hidCmbCate.ClientID %>").value;
        }
        if(document.getElementById("<%= cmbCategory.ClientID %>").value != -1 && document.getElementById("<%= cmbCategory.ClientID %>").value != ''){
            Data = document.getElementById("<%= cmbCategory.ClientID %>").value;
            document.getElementById("<%= hidCmbCate.ClientID %>").value = Data;
        }
        var tabid = document.getElementById("<%= hid_tab.ClientID %>").value;
        var ToData;
        if(tabid == 2){
            ToData = "3Ø2Ø" + Data;
        }else if(tabid == 3){
            ToData = "3Ø3Ø" + Data;
        }
        ToServer(ToData, 3);
    }
    </script>
</head>
</html>
 <br />
     <div style="width:60%;background-color:#A34747;margin:0px auto;height:430px;">
        <div style="width:80%;padding-left:10px;margin:0px auto;">
            <div id='1' class="Button" style="width:30%; float:left;background: -moz-radial-gradient(center, ellipse cover, #A34747 0%, #A34747 0%, #A34747  100%);color:#FFF;" onclick="return CategoryClick()">Loan Category</div>
            <div id='2' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return ProductClick()">Loan Product</div>
            <div id='3' class="Button" style="border-left:thick double #FFF;width:30%;float:left;" onclick="return PDDClick()">PDD</div>
            <br /> <br />
        </div>
        <div style="width:93%;background-color:white;margin:0px auto; height:330px;border-radius:25px;">
            <br /><br />
            <table style="width:90%;height:90px;margin:0px auto;">
                <tr id="CategoryName" >
                    <td style="width:12%;"></td>
                    <td style="width:23%; align:left;">
                        Loan Category :
                    </td> 
                    <td style="width:55%;">
                        <asp:TextBox ID="txtCateName" runat="server" class="NormalText" Width="61%"  Rows="3"  MaxLength="500" />
                    </td>
                </tr>
                <tr id="CategoryCombo" style="display:none;"> 
                    <td style="width:12%;"></td>
                    <td style="width:23%; align:left;">
                        Loan Category : 
                    </td> 
                    <td style="width:55%;">
                        <asp:DropDownList ID="cmbCategory" class="NormalText" runat="server" Font-Names="Cambria" Width="61%" ForeColor="Black">
                            <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="ProductName" style="display:none;">
                    <td style="width:12%;"></td>
                    <td style="width:23%; align:left;">
                        Loan Product :
                    </td> 
                    <td style="width:55%;">
                        <asp:TextBox ID="txtProductName" runat="server" class="NormalText" Width="61%"  Rows="3"  MaxLength="500"/>
                    </td>
                </tr>
                <tr id="PDDName" style="display:none;">
                    <td style="width:12%;"></td>
                    <td style="width:23%; align:left;">
                        PDD :
                    </td> 
                    <td style="width:55%;">
                        <asp:TextBox ID="txtPDDName" runat="server" class="NormalText" Width="61%"  Rows="3"  MaxLength="500"/>
                    </td>
                </tr>
                <tr id="Forms" style="display:none;">                                     
                    <td style="width:55%;" colspan="3">
                        <asp:Panel ID="pnlForms" style="width:100%; text-align:right;float:right;" runat="server">
                        </asp:Panel>
                    </td>
                </tr>                        
                <tr id="List">  
                    <td style="text-align:center;" colspan="3">
                        <br />
                        <asp:Panel ID="pnlDtls" style="width:100%; text-align:right;float:right;" runat="server">
                        </asp:Panel>
                        <asp:HiddenField ID="hid_tab" runat="server" /> 
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hid_form_dtls" runat="server" />
                        <asp:HiddenField ID="hid_temp" runat="server" />
                        <asp:HiddenField ID="hid_category" runat="server" />
                        <asp:HiddenField ID="hid_product" runat="server" />
                        <asp:HiddenField ID="hid_pdd" runat="server" />
                        <asp:HiddenField ID="hidCmbCate" runat="server" />
                        <asp:HiddenField ID="hidEditFlag" runat="server" />
                        <asp:HiddenField ID="hidEditId" runat="server" />
                    </td>
                </tr>
            </table>             
        </div>
        <div style="text-align:center;">
            <br />
            <input id="btnSave" style="font-family: cambria; cursor: pointer; width: 6%;" type="button" value="SAVE" onclick="return btnSave_onclick()" />
            &nbsp;&nbsp;
            <input id="btnExit" style="font-family: cambria; cursor: pointer; width:6%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
        </div>
    </div>   
<br /><br />
</asp:Content>

