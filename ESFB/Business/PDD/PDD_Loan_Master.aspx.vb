﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PDD_Loan_Master
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim MAST As New Master
    Dim GF As New GeneralFunctions
    Dim CallBackReturn As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1418) = False Then 'uat
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1433) = False Then 'main
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If

            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Me.Master.subtitle = "Loan Master"
            Me.cmbCategory.Attributes.Add("onchange", "return CategoryOnchange()")
            DT = DB.ExecuteDataSet("SELECT '-1' AS CATEGORY_ID,'-----SELECT-----' AS CATEGORY_NAME UNION ALL SELECT CATEGORY_ID,CATEGORY_NAME FROM PDD_CATEGORY_MASTER WHERE STATUS = 1 ORDER BY CATEGORY_ID").Tables(0)
            GF.ComboFill(cmbCategory, DT, 0, 1)
            hid_tab.Value = CStr(1)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "CategoryTableFill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim RequestID As Integer = CInt(Data(0))
        Dim UserID As Integer = CInt(Session("UserID"))
        If RequestID = 1 Then 'Display category table
            DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY Category_Name) AS Row,upper(Category_Name) from dbo.PDD_CATEGORY_MASTER where Status=1 order by CATEGORY_Name").Tables(0)
            Dim StrExistCategory As String
            Dim StrForm As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrForm += DT.Rows(n)(0).ToString() + "µ" + DT.Rows(n)(1).ToString()
                If n < DT.Rows.Count - 1 Then
                    StrForm += "¥"
                End If
            Next
            StrExistCategory = StrForm
            If DT.Rows.Count > 0 Then
                CallBackReturn = StrExistCategory
            End If
        ElseIf RequestID = 2 Then
            Dim tabid As Integer = CInt(Data(1).ToString())
            Dim category As String
            Dim categoryCombo As Integer
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim Flag As Integer = CInt(Data(4).ToString())
            Try
                If tabid = 1 Then
                    category = Data(2).ToString()
                    Dim Params(5) As SqlParameter
                    Params(0) = New SqlParameter("@Data", SqlDbType.VarChar, 500)
                    Params(0).Value = category
                    Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                    Params(1).Value = UserID
                    Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                    Params(2).Value = tabid
                    Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                    Params(3).Direction = ParameterDirection.Output
                    Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                    Params(4).Direction = ParameterDirection.Output
                    Params(5) = New SqlParameter("@Flag", SqlDbType.Int)
                    Params(5).Value = 1
                    DB.ExecuteNonQuery("SP_PDD_LOAN_MASTER", Params)
                    ErrorFlag = CInt(Params(3).Value)
                    Message = CStr(Params(4).Value)
                ElseIf tabid = 2 Or tabid = 3 Then
                    categoryCombo = CInt(Data(2).ToString())
                    If Flag = 1 Then
                        Dim nameData As String = Data(3).ToString()
                        Dim Params(6) As SqlParameter
                        Params(0) = New SqlParameter("@Data", SqlDbType.VarChar, 500)
                        Params(0).Value = nameData
                        Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(1).Value = UserID
                        Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                        Params(2).Value = tabid
                        Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(3).Direction = ParameterDirection.Output
                        Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(4).Direction = ParameterDirection.Output
                        Params(5) = New SqlParameter("@Category", SqlDbType.Int)
                        Params(5).Value = categoryCombo
                        Params(6) = New SqlParameter("@Flag", SqlDbType.Int)
                        Params(6).Value = 1
                        DB.ExecuteNonQuery("SP_PDD_LOAN_MASTER", Params)
                        ErrorFlag = CInt(Params(3).Value)
                        Message = CStr(Params(4).Value)
                    End If
                    If Flag = 3 Then
                        Dim comboData As Integer = CInt(Data(5).ToString())
                        Dim nameData As String = Data(3).ToString()
                        Dim Params(7) As SqlParameter
                        Params(0) = New SqlParameter("@Item", SqlDbType.Int)
                        Params(0).Value = comboData
                        Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                        Params(1).Value = UserID
                        Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                        Params(2).Value = tabid
                        Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                        Params(3).Direction = ParameterDirection.Output
                        Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                        Params(4).Direction = ParameterDirection.Output
                        Params(5) = New SqlParameter("@Category", SqlDbType.Int)
                        Params(5).Value = categoryCombo
                        Params(6) = New SqlParameter("@Flag", SqlDbType.Int)
                        Params(6).Value = 3
                        Params(7) = New SqlParameter("@Data", SqlDbType.VarChar, 500)
                        Params(7).Value = nameData
                        DB.ExecuteNonQuery("SP_PDD_LOAN_MASTER", Params)
                        ErrorFlag = CInt(Params(3).Value)
                        Message = CStr(Params(4).Value)
                    End If
                End If
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            DT = DB.ExecuteDataSet("SELECT '-1' AS CATEGORY_ID,'-----SELECT-----' AS CATEGORY_NAME UNION ALL SELECT CATEGORY_ID,CATEGORY_NAME FROM PDD_CATEGORY_MASTER WHERE STATUS = 1 ORDER BY CATEGORY_ID").Tables(0)
            Dim ComboDataCategory As String = ""
            For Each DR As DataRow In DT.Rows
                ComboDataCategory += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message + "Ø" + ComboDataCategory
        ElseIf RequestID = 3 Then
            Dim tabid As Integer = CInt(Data(1).ToString())
            Dim category As Integer = CInt(Data(2).ToString())
            Dim StrExistData As String = ""
            If tabid = 2 Then
                DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY Product_Name) AS Row,upper(Product_Name),Product_ID from dbo.PDD_PRODUCT_MASTER where Status=1 AND CATEGORY_ID = " + category.ToString() + " order by PRODUCT_Name").Tables(0)
                Dim StrForm As String = ""
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrForm += DT.Rows(n)(0).ToString() + "µ" + DT.Rows(n)(1).ToString() + "µ" + DT.Rows(n)(2).ToString()
                    If n < DT.Rows.Count - 1 Then
                        StrForm += "¥"
                    End If
                Next
                StrExistData = StrForm
                If DT.Rows.Count > 0 Then
                    CallBackReturn = StrExistData
                End If
            ElseIf tabid = 3 Then
                DT = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY PDD_Name) AS Row,upper(PDD_Name),PDD_Id from dbo.PDD_MASTER where Status=1 AND CATEGORY_ID = " + category.ToString() + " order by PDD_Name").Tables(0)
                Dim StrForm As String = ""
                For n As Integer = 0 To DT.Rows.Count - 1
                    StrForm += DT.Rows(n)(0).ToString() + "µ" + DT.Rows(n)(1).ToString() + "µ" + DT.Rows(n)(2).ToString()
                    If n < DT.Rows.Count - 1 Then
                        StrForm += "¥"
                    End If
                Next
                StrExistData = StrForm
                If DT.Rows.Count > 0 Then
                    CallBackReturn = StrExistData
                End If
            End If
        ElseIf RequestID = 4 Then
            Dim tabid As Integer = CInt(Data(1).ToString())
            Dim categoryCombo As Integer = CInt(Data(2).ToString())
            Dim delItem As Integer = CInt(Data(3).ToString())
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim nameData As String = Data(3).ToString()
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@Item", SqlDbType.Int)
                Params(0).Value = delItem
                Params(1) = New SqlParameter("@UserID", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@TabID", SqlDbType.Int)
                Params(2).Value = tabid
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@Category", SqlDbType.Int)
                Params(5).Value = categoryCombo
                Params(6) = New SqlParameter("@Flag", SqlDbType.Int)
                Params(6).Value = 2 'delete
                DB.ExecuteNonQuery("SP_PDD_LOAN_MASTER", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf RequestID = 5 Then
            Dim hidid As Integer = CInt(Data(1).ToString())
            Dim categoryId As String = Data(2).ToString()
            If hidid = 2 Then
                Dim productid As String = Data(3).ToString()
                DT = DB.ExecuteDataSet("SELECT PRODUCT_NAME,PRODUCT_ID FROM PDD_PRODUCT_MASTER WHERE PRODUCT_ID = " + productid + " AND CATEGORY_ID = " + categoryId + " AND STATUS = 1").Tables(0)
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "Ø" + hidid.ToString() + "Ø" + DT.Rows(0)(1).ToString()
                End If
            ElseIf hidid = 3 Then
                Dim pddid As String = Data(3).ToString()
                DT = DB.ExecuteDataSet("SELECT PDD_NAME,PDD_ID FROM PDD_MASTER WHERE PDD_ID = " + pddid + " AND CATEGORY_ID = " + categoryId + " AND STATUS = 1").Tables(0)
                If DT.Rows.Count > 0 Then
                    CallBackReturn += DT.Rows(0)(0).ToString() + "Ø" + hidid.ToString() + "Ø" + DT.Rows(0)(1).ToString()
                End If
            End If
            Me.hidEditFlag.Value = CStr(3)
        ElseIf RequestID = 6 Then

        End If
    End Sub
End Class
