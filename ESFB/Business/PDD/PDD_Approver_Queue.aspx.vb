﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PDD_Approver_Queue
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1431) = False Then 'uat
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1436) = False Then 'main
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "PDD - Approval Queue"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            If Not IsPostBack Then
                'DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1").Tables(0)
                'Dim i As Integer = 0
                'For Each dtr In DT.Rows
                '    Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "£"
                '    DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR(10),A.DUE_DATE,103) ELSE NULL END AS DUE_DATE, A.SUB_NO,A.SO_REMARKS,CONVERT(VARCHAR(10),A.SO_ENTRY_ON,103) FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID WHERE ((A.APPROVED_STATUS = 2 AND A.SO_ENTRY_STATUS = 3) OR (isnull(A.APPROVED_STATUS,0) NOT IN (1,2) AND A.SO_ENTRY_STATUS = 1))  AND (isnull(A.APPROVED_STATUS,0) <> 3) AND A.WI_NO = " + DT.Rows(i)(0).ToString()).Tables(0)
                '    Dim j As Integer = 0
                '    For Each dtr1 In DT1.Rows
                '        Dim dueDate As String = ""
                '        If DT1.Rows(j)(1).ToString() = "" Then
                '            dueDate = "To Be Updated"
                '        Else
                '            dueDate = DT1.Rows(j)(1).ToString()
                '        End If
                '        Me.hidQueueData.Value += DT1.Rows(j)(0).ToString() + "€" + dueDate + "€" + DT1.Rows(j)(2).ToString() + "€" + DT1.Rows(j)(3).ToString() + "€" + DT1.Rows(j)(4).ToString() + "¢"
                '        j += 1
                '    Next
                '    Me.hidQueueData.Value += "Ñ"
                '    i += 1
                'Next
            End If
            DT = DB.ExecuteDataSet("SELECT '-1' AS CATEGORY_ID,'-----SELECT-----' AS CATEGORY_NAME UNION ALL SELECT CATEGORY_ID,CATEGORY_NAME FROM PDD_CATEGORY_MASTER WHERE STATUS = 1 ORDER BY CATEGORY_ID").Tables(0)
            GF.ComboFill(cmbLoanCategory, DT, 0, 1)
            Me.cmbLoanCategory.Attributes.Add("onchange", "return CategoryOnchange()")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim EmpID As Integer = CInt(Data(1))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim Sub_Id As Integer = CInt(Data(1))
            Dim Wi_No As String = Data(2).ToString()
            Dim Status As Integer = CInt(Data(3))
            Dim Remarks As String = Data(4).ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(6) As SqlParameter
                Params(0) = New SqlParameter("@subID", SqlDbType.Int)
                Params(0).Value = Sub_Id
                Params(1) = New SqlParameter("@wiNum", SqlDbType.VarChar, 50)
                Params(1).Value = Wi_No
                Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                Params(2).Value = Status
                Params(3) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                Params(3).Value = Remarks
                Params(4) = New SqlParameter("@UserID", SqlDbType.VarChar, 25)
                Params(4).Value = UserID
                Params(5) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(5).Direction = ParameterDirection.Output
                Params(6) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(6).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_PDD_FINAL_APPROVE", Params)
                ErrorFlag = CInt(Params(5).Value)
                Message = CStr(Params(6).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim category = Data(1)
            DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1 and A.CATEGORY=" + category).Tables(0)
            Dim i As Integer = 0
            For Each dtr In DT.Rows
                Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "£"
                DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR(10),A.DUE_DATE,103) ELSE NULL END AS DUE_DATE, A.SUB_NO,A.SO_REMARKS,CONVERT(VARCHAR(10),A.SO_ENTRY_ON,103) FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID WHERE ((A.APPROVED_STATUS = 2 AND A.SO_ENTRY_STATUS = 3) OR (isnull(A.APPROVED_STATUS,0) NOT IN (1,2) AND A.SO_ENTRY_STATUS = 1))  AND (isnull(A.APPROVED_STATUS,0) <> 3) AND A.STATUS <> 7 AND A.WI_NO = '" + DT.Rows(i)(0).ToString() + "'").Tables(0)
                Dim j As Integer = 0
                For Each dtr1 In DT1.Rows
                    Dim dueDate As String = ""
                    If DT1.Rows(j)(1).ToString() = "" Then
                        dueDate = "To Be Updated"
                    Else
                        dueDate = DT1.Rows(j)(1).ToString()
                    End If
                    Me.hidQueueData.Value += DT1.Rows(j)(0).ToString() + "€" + dueDate + "€" + DT1.Rows(j)(2).ToString() + "€" + DT1.Rows(j)(3).ToString() + "€" + DT1.Rows(j)(4).ToString() + "¢"
                    j += 1
                Next
                Me.hidQueueData.Value += "Ñ"
                i += 1
            Next
            CallBackReturn = Me.hidQueueData.Value
        ElseIf CInt(Data(0)) = 3 Then

        ElseIf CInt(Data(0)) = 4 Then

        End If
    End Sub
#End Region
End Class
