﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PDD_SO_Queue.aspx.vb" Inherits="PDD_SO_Queue" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () { 
            var QueueData = document.getElementById("<%= hidQueueData.ClientID %>").value;  
            document.getElementById("new").innerHTML = "&nbsp;&nbsp;(" + document.getElementById("<%= hidNew.ClientID %>").value + ")";
            document.getElementById("return").innerHTML = "&nbsp;&nbsp;(" + document.getElementById("<%= hidReturn.ClientID %>").value + ")";
            TableFill();     
        }
        function FromServer(arg, context) {             
            if (context == 1) {
                document.getElementById("<%= hidQueueData.ClientID %>").value = arg;
                TableFill();
            }
        }
        function ReqUpdateOnClick(sub_id,wi_no){
            window.open("PDD_SO_Entry.aspx?WI_No=" + wi_no + "&Sub_No=" + sub_id + "", "_self");
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function TableFill(){
            var QueueData = "";            
            document.getElementById("<%= QueuePanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var orgData = document.getElementById("<%= hidQueueData.ClientID %>").value.split("Ø");
            var chk = orgData[1];
            var tab = "";            
            tab += "<div style='width:95%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='QueueTable'>";
            tab += "<tr class=mainhead style='height:50px;'>";
            tab += "<td style='width:5%;text-align:center;' >WI.No</td>";
            tab += "<td style='width:9%;text-align:center;' >Branch Name</td>";
            tab += "<td style='width:10%;text-align:center;' >Customer Name</td>";
            tab += "<td style='width:11%;text-align:center;' >Loan Account No</td>";
            tab += "<td style='width:9%;text-align:center;' >Loan Product</td>";
            tab += "<td style='width:7%;text-align:center;' >Disbursal Date</td>";
            tab += "<td style='width:9%;text-align:center;' >PDD Item</td>";
            tab += "<td style='width:7%;text-align:center;' >Due Date</td>";
            if (chk == 2) {
                tab += "<td style='width:7%;text-align:center;' >Returned Date</td>";
                tab += "<td style='width:10%;text-align:center;' >Returned By</td>";
                tab += "<td style='width:12%;text-align:center;' >Remarks</td>";
            } else {
                tab += "<td style='width:7%;text-align:center;display:none;' >Returned Date</td>";
                tab += "<td style='width:10%;text-align:center;display:none;' >Returned By</td>";
                tab += "<td style='width:12%;text-align:center;display:none;' >Remarks</td>";
            }
            if (chk == 1 || chk == 2) {
                tab += "<td style='width:4%;text-align:center;' >Next</td>";
            }           
            tab += "</tr>";
            QueueData = orgData[0].split("Ñ");
            var QueueLen = QueueData.length-1;
            for(var k=0;k<QueueLen;k++){
                var QueueRow = QueueData[k].split("£");
                var mainRow = QueueRow[0].split("ÿ");
                var subRow = QueueRow[1].split("¢");
                var subLen = subRow.length;
                if(subLen-1 != 0){
                    tab += "<tr class=sub_first style ='height:auto;' id='row_"+k+"'>";
                    tab += "<td style='width:5%;text-align:center'>"+mainRow[0]+"</td>";
                    tab += "<td style='width:9%;text-align:center'>"+mainRow[2]+"</td>";
                    tab += "<td style='width:10%;text-align:center'>"+mainRow[3]+"</td>";
                    tab += "<td style='width:11%;text-align:center'>"+mainRow[4]+"</td>";
                    tab += "<td style='width:9%;text-align:center'>" + mainRow[6] + "</td>";
                    tab += "<td style='width:7%;text-align:center'>"+mainRow[7]+"</td>";
                    tab += "<td style='width:8.1%;text-align:center' colspan='6'>"
                    tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px 0px;' align='center'>";
                    tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='left'>";  
                    var wi_no = '';
                    for(var l = 0;l<subLen-1;l++){
                        var subData = subRow[l].split("€");                     
                        tab += "<tr class=sub_first style ='height:30px;'>";
                        tab += "<td style='width:18.4%;text-align:center'>" + subData[0] + "</td>";
                        var date = '';
                        if(subData[1] != 'To be updated'){
                            date = GetFormattedDate(subData[1]);
                        }else{
                            date = subData[1];
                        }
                        tab += "<td style='width:14.2%;text-align:center'>" + date + "</td>";
                        if (chk == 2) {
                            tab += "<td style='width:14.2%;text-align:center'>" + subData[3] + "</td>";
                            tab += "<td style='width:20.4%;text-align:center'>" + subData[4] + "</td>";
                            tab += "<td style='width:24.4%;text-align:center'>" + subData[5] + "</td>";
                        } else {
                            tab += "<td style='width:14.2%;text-align:center;display:none;'>" + subData[3] + "</td>";
                            tab += "<td style='width:20.4%;text-align:center;display:none;'>" + subData[4] + "</td>";
                            tab += "<td style='width:24.4%;text-align:center;display:none;'>" + subData[5] + "</td>";
                        }
                        wi_no = mainRow[0];
                        if (chk == 1 || chk == 2) {
                            if (subData[1] != "To be updated") {
                                var g1 = new Date();
                                var g2 = new Date(subData[1]);
                                if (g1.getTime() > g2.getTime()) {
                                    tab += "<td style='width:8.16%;text-align:center'><img src='../../image/update.png' title='Update' style='height:28px; width:28px; cursor:pointer;' onclick='ReqUpdateOnClick(" + subData[2].toString() + ",\"" + wi_no.toString() + "\")' /></td>";
                                }else {
                                    tab += "<td style='width:8.16%;text-align:center'><img src='../../image/update2.png' title='Update' style='height:22px; width:22px; cursor:pointer;' onclick='ReqUpdateOnClick(" + subData[2].toString() + ",\"" + wi_no.toString() + "\")' /></td>";
                                } 
                            }else{
                                tab += "<td style='width:8.16%;text-align:center'><img src='../../image/update2.png' title='Update' style='height:22px; width:22px; cursor:pointer;' onclick='ReqUpdateOnClick(" + subData[2].toString() + ",\"" + wi_no.toString() + "\")' /></td>";
                            }
                        }                        
                        tab += "</tr>";
                    }
                    tab += "</table>";
                    tab += "</div>";
                    tab += "</td>";                
                    tab += "</tr>";    
                }            
            }
            tab += "</table></div>";
            document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = tab;    
            if(QueueData.toString() == ""){
                document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = "<table style='width:100%'><tr><td style='width:30%'></td><td style='width:40%;text-align:center;font-size:16px;'><b>No requests pending.</b></td><td style='width:30%'></td>";  
            }   
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 0; a < rows.length-1; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function statOnClick(e) {
            var ToData = "1Ø" + e;
            ToServer(ToData, 1);
        }
        function GetFormattedDate(dates) {
            var date = new Date(dates);
            return ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear();
        }
    </script>   
</head>
</html>
<br />
<div  style="width:95%; height:auto; margin:0px auto; background-color:#EEB8A6;">
<br />
    <div id = "divSection1" class = "sec1" style="width:95%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <div style="width:100%; text-align:center;" align="center">
            <div style="width:100%;" align="center">
                <table class="style1" style="width:100%;margin: 0px auto;">
                    <tr>
                        <td style="width:30%">
                        </td>
                        <td style="width:40%;text-align:center">
                            <input id="stat_1" type="radio" name="stat" value="1" checked="checked" onclick='statOnClick(1);'/>
                            <label for="stat_1">New</label><span id="new"></span>
                            &nbsp;&nbsp;&nbsp
                            <input id="stat_2" type="radio" name="stat" value="2"  onclick='statOnClick(2);'/>
                            <label for="stat_2">Returned</label><span id="return"></span>
                        </td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </div>
        </div>
        <table class="style1" id="UserTable" style="width:100%;margin: 0px auto;">
            <tr>
                <td>
                    <asp:HiddenField ID="hidQueueData" runat="server" />
                    <asp:HiddenField ID="hidNew" runat="server" />
                    <asp:HiddenField ID="hidReturn" runat="server" />
                    <%--<asp:HiddenField ID="hidSOType" runat="server" />--%>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width:95%;">
                    <asp:Panel ID="QueuePanel" runat="server">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
            <div style="text-align:center; height: 63px;"><br />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </div>
        </div>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

