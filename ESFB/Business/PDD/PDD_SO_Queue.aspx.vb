﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PDD_SO_Queue
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1435) = False Then 'main
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "PDD - SO Queue"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "", True)
            'If Not IsPostBack Then

            '<Summary>Change done by 40020 on 30-Jan-2021</Summary>
            '<Comments>ASMs & RSM/CHs can view all the PDDs under their mapped branches</Comments>
            'Dim SO As Integer = 0
            'Dim ASM As Integer = 0
            'DT = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE = " + CStr(Session("UserID"))).Tables(0)
            'If DT.Rows.Count > 0 Then
            '    If CInt(DT.Rows(0)(0).ToString()) = 2 Then
            '        SO = 1
            '    End If
            'End If
            'Dim k As Integer = 0
            'DT1 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + CStr(Session("UserID")) + ")").Tables(0)
            'If DT1.Rows.Count > 0 Then
            '    For Each dr1 In DT1.Rows
            '        If CInt(DT1.Rows(k)(0)) = 2 Then
            '            ASM = 1
            '        End If
            '    Next
            'End If

            'If SO = 1 Then
            '    DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1 AND A.BRANCH_CODE IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE = " + EmpCode.ToString() + ")").Tables(0)
            'ElseIf ASM = 1 Then
            '    DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1 AND A.BRANCH_CODE IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE  IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + EmpCode.ToString() + "))").Tables(0)
            'End If
            Dim Params(2) As SqlParameter
            Params(0) = New SqlParameter("@Emp_Code", SqlDbType.VarChar, 20)
            Params(0).Value = EmpCode.ToString()
            Params(1) = New SqlParameter("@New_Cnt", SqlDbType.VarChar, 20)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@Return_Cnt", SqlDbType.VarChar, 20)
            Params(2).Direction = ParameterDirection.Output
            DT = DB.ExecuteDataSet("SP_PDD_GET_SO_DATA", Params).Tables(0)

            Me.hidNew.Value = Params(1).Value.ToString()
            Me.hidReturn.Value = Params(2).Value.ToString()

            Dim i As Integer = 0
            For Each dtr In DT.Rows
                Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "£"
                DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR,CONVERT(DATE,A.DUE_DATE)) ELSE 'To be updated' END AS DUE_DATE, A.SUB_NO FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID WHERE ((isnull(A.SO_ENTRY_STATUS,0) NOT IN (1,3)) or (A.SO_ENTRY_STATUS = 2 and A.DISB_STATUS = 2 AND A.STATUS = 4) ) AND A.STATUS <> 3 AND A.WI_NO = '" + DT.Rows(i)(0).ToString() + "'").Tables(0)
                Dim j As Integer = 0
                For Each dtr1 In DT1.Rows
                    Dim dueDate As String = ""
                    If DT1.Rows(j)(1).ToString() = "" Then
                        dueDate = "To Be Updated"
                    Else
                        dueDate = DT1.Rows(j)(1).ToString()
                    End If
                    Me.hidQueueData.Value += DT1.Rows(j)(0).ToString() + "€" + dueDate + "€" + DT1.Rows(j)(2).ToString() + "¢"
                    j += 1
                Next

                Me.hidQueueData.Value += "Ñ"
                i += 1
            Next
            Me.hidQueueData.Value += "Ø1"


            'End If
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim chk As Integer = CInt(Data(1).ToString())
            Dim strData As String = ""

            '<Summary>Change done by 40020 on 30-Jan-2021</Summary>
            '<Comments>ROs,ASMs & RSM/CHs can view all the PDDs under their mapped branches</Comments>
            'DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1 AND A.BRANCH_CODE IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE = " + UserID.ToString() + ")").Tables(0)
            Dim Params(2) As SqlParameter
            Params(0) = New SqlParameter("@Emp_Code", SqlDbType.VarChar, 20)
            Params(0).Value = UserID.ToString()
            Params(1) = New SqlParameter("@New_Cnt", SqlDbType.VarChar, 20)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@Return_Cnt", SqlDbType.VarChar, 20)
            Params(2).Direction = ParameterDirection.Output
            DT = DB.ExecuteDataSet("SP_PDD_GET_SO_DATA", Params).Tables(0)

            Dim i As Integer = 0
            For Each dtr In DT.Rows
                strData += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "£"
                If chk = 1 Then
                    '<Summary>Change done by 40020 on 02-Feb-2021</Summary>
                    '<Comments>ROs,ASMs & RSM/CHs can view all the PDDs under their mapped branches</Comments>
                    'DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR,CONVERT(DATE,A.DUE_DATE)) ELSE 'To be updated' END AS DUE_DATE, A.SUB_NO,A.APPROVED_ON,A.APPROVED_BY, A.APPROVED_REMARKS FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID WHERE ((isnull(A.SO_ENTRY_STATUS,0) NOT IN (1,3)) or (A.SO_ENTRY_STATUS = 2 and A.DISB_STATUS = 2 AND A.STATUS = 4))  AND A.WI_NO = '" + DT.Rows(i)(0).ToString() + "'").Tables(0)
                    DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR,CONVERT(DATE,A.DUE_DATE)) ELSE 'To be updated' END AS DUE_DATE, A.SUB_NO,A.APPROVED_ON,A.APPROVED_BY, A.APPROVED_REMARKS FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID WHERE ((isnull(A.SO_ENTRY_STATUS,0) NOT IN (1,3)) or (A.SO_ENTRY_STATUS = 2 and A.DISB_STATUS = 2 AND A.STATUS = 4)) AND A.STATUS <> 3 AND A.WI_NO = '" + DT.Rows(i)(0).ToString() + "'").Tables(0)
                End If
                If chk = 2 Then
                    DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR,CONVERT(DATE,A.DUE_DATE)) ELSE 'To be updated' END AS DUE_DATE, A.SUB_NO,A.APPROVED_ON,A.APPROVED_BY, A.APPROVED_REMARKS FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID WHERE A.SO_ENTRY_STATUS IN (1,3) AND A.APPROVED_STATUS = 2 AND A.STATUS = 7  AND A.WI_NO = '" + DT.Rows(i)(0).ToString() + "'").Tables(0)
                End If
                Dim j As Integer = 0
                For Each dtr1 In DT1.Rows
                    Dim dueDate As String = ""
                    If DT1.Rows(j)(1).ToString() = "" Then
                        dueDate = "To Be Updated"
                    Else
                        dueDate = DT1.Rows(j)(1).ToString()
                    End If
                    strData += DT1.Rows(j)(0).ToString() + "€" + dueDate + "€" + DT1.Rows(j)(2).ToString() + "€" + DT1.Rows(j)(3).ToString() + "€" + DT1.Rows(j)(4).ToString() + "€" + DT1.Rows(j)(5).ToString() + "¢"
                    j += 1
                Next
                strData += "Ñ"
                i += 1
            Next
            CallBackReturn += strData + "Ø" + chk.ToString()
        ElseIf CInt(Data(0)) = 2 Then

        ElseIf CInt(Data(0)) = 3 Then

        ElseIf CInt(Data(0)) = 4 Then

        End If
    End Sub
#End Region
End Class
