﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PDD_Disbursement_Entry.aspx.vb" Inherits="PDD_Disbursement_Entry" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">   </asp:ToolkitScriptManager>
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var rowLen;
        var status = 0;
        var txtPddCount = 1;
        window.onload = function () {
            var today = new Date();
            today = today.toShortFormat();
            document.getElementById("<%= txtDisbDate.ClientID %>").value = today;
            AddRow(0);
            $('.QueueData').hide();
        }
        Date.prototype.toShortFormat = function () {
            var month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = this.getDate();
            var month_index = this.getMonth();
            var year = this.getFullYear();
            return "" + day + " " + month_names[month_index] + " " + year;
        }
        function AlphaSpaceCheck(e) {
            var inputValue = e.which;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0 && inputValue != 121)) {
                e.preventDefault();
            }
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function AddRow(i) {
            if( i == 15){
                alert("Maximum number of PDDs that can be uploaded is 15.");
                return false;
            }
            if (i != 0) {
                var dueDate = document.getElementById("dueDate_" + i).value;
                var chkDueDate = document.getElementById("chkDueDate_" + i).checked;
                if(chkDueDate == true){
                    chkDueDate = 1;
                }else{
                    chkDueDate = 0;
                }
                
                if ($("#cmbPdd_" + i).val() != -1 && (dueDate != "" || chkDueDate != 0 )) {
                    updateValue(i);
                    TableFill(i + 1);
                }
                else {
                    if($("#cmbPdd_" + i).val() == -1){
                        alert("Select PDD");
                        return false;
                    }else if (dueDate == "" && chkDueDate == 0) {
                        alert("Kindly update or due date or select 'To Be Updated' option");
                        return false;
                    }
                }
            }
            else {
                updateValue(i+1);
                TableFill(i + 1);
            }
        }

        function TableFill(k) {
            document.getElementById("<%= pnlPDD.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:left;margin: 0px auto;' align='left'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:10%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:51%;text-align:center' >PDD Item</td>";
            tab += "<td style='width:25%;text-align:center;'>Due Date</td>";
            tab += "<td class='addCol' style='width:7%;text-align:center;' colspan='2'></td>";
            tab += "</tr>";

            rowLen = k;
            for (var i = 1; i <= k; i++) {
                tab += "<tr class=sub_first style ='height:auto;'>";
                tab += "<td style='width:10%;text-align:center'>" + i + "</td>";
                var select = "<table style='width:100%'>";
                select += "<tr>";
                select += "<td style='width:100%'>";
                select += "<select id='cmbPdd_" + i + "' class='NormalText' name='cmbPdd_" + i + "' style='width:97.5%;text-align:center;' onchange='return PDDOnChange(" + i + ")'>";
                select += "</td></tr>";
                select += "<tr></tr>";
                select += "<tr>";
                select += "<td style='width:100%'>";
                select += "<input type='text' class='NormalText PddText' name='txtPdd_" + i + "' id='txtPdd_" + i + "' style='width:97%;text-align:center;display:none;' />";
                select += "</td></tr></table>";
                tab += "<td style='width:51%;text-align:center'>" + select + "</td>";
                var dateTab = "<table style='width:100%'>";
                dateTab += "<tr style='text-align:center'>";
                dateTab += "<td style='width:100%'>";
                dateTab += "<input type='date' name='dueDate_" + i + "' id='dueDate_" + i + "' onkeydown='return false' enabled='true' onchange='return DueDateOnChange("+ i +")'>";
                dateTab += "</td></tr>";
                dateTab += "<tr></tr>";
                dateTab += "<tr>";
                dateTab += "<td style='width:100%'>";
                dateTab += "<input type='checkbox' class='NormalText' id='chkDueDate_" + i + "' name='chkDueDate_" + i + "' style='width:10%;text-align:center;' enabled='true' onchange='return chkDateOnChange("+ i +")'/>&nbsp;&nbsp;<label for='chkDueDate_" + i + "'>To be updated</label><br>";
                dateTab += "</td></tr></table>";
                tab += "<td style='width:25%;text-align:center;'>" + dateTab + "</td>";
                tab += "<td class='addCol' style='width:7%;text-align:center' onclick=AddRow(" + i + ")><img id='Add_" + i + "' src='../../Image/addd1.png'; style='align:middle;cursor:pointer; height:17px; width:17px ;' title='ADD'/></td>"
                tab += "<td class='addCol' style='width:7%;text-align:center' onclick=DeleteRow(" + i + ")><img id='Delete_" + i + "' src='../../Image/delete.png'; style='align:middle;cursor:pointer; height:15px; width:15px ;' title='DELETE'/></td>"
                tab += "</tr>";
            }
            tab += "</table>";
            tab += "</div>";
            document.getElementById("<%= pnlPDD.ClientID %>").innerHTML = tab;
            var comboData = document.getElementById("<%= hidPDD.ClientID %>").value;
            for (var i = 1; i <= k; i++) {
                ComboFill(comboData, "cmbPdd_" + i);
                if (k == 1) {
                    $("#Delete_" + i).hide();
                } else {
                    $("#Delete_" + i).show();
                }
                if (i == k) {
                    $("#Add_" + i).show();
                } else {
                    $("#Add_" + i).hide();
                }
            }
            try {
                var rowData = document.getElementById("<%= hidRowData.ClientID %>").value.split("¥");
                for (var i = 1; i <= rowData.length; i++) {
                    var data = rowData[i-1].toString();
                    var rData = data.split("µ");
                    for (var j = 1; j <= rData.length; j++) {
                        if(rData[0] == 1000){
                            document.getElementById("cmbPdd_" + i).value = rData[0];
//                            if(txtPddCount > 8){
//                                document.getElementById("txtPdd_" + i).style.display = 'none';
//                            }else{
//                                document.getElementById("txtPdd_" + i).style.display = '';
//                            }
                            document.getElementById("txtPdd_" + i).style.display = '';
                            document.getElementById("txtPdd_" + i).value = rData[1];
                        }else{
                            if(rData[0] == ""){
                                document.getElementById("cmbPdd_" + i).value = -1;
                            }else{
                                document.getElementById("cmbPdd_" + i).value = rData[0];
                            }
                            document.getElementById("txtPdd_" + i).style.display = 'none';
                        }
                        if(!!(rData[2])){
                            document.getElementById("dueDate_" + i).value = rData[2];
                            document.getElementById("chkDueDate_" + i).disabled = true;
                            document.getElementById("dueDate_" + i).disabled = false;
                        }else if(rData[3] == 1 || rData[3] == "true"){
                            document.getElementById("chkDueDate_" + i).checked = true;
                            document.getElementById("dueDate_" + i).disabled = true;
                            document.getElementById("chkDueDate_" + i).disabled = false;
                        }else{                            
                            document.getElementById("dueDate_" + i).disabled = false;
                            document.getElementById("chkDueDate_" + i).disabled = false;
                        }
                    }
                }
            } catch (e) { }
        }
        function updateValue(k) {
            row = document.getElementById("<%= hidRowData.ClientID %>").value.split("¥");
            var NewStr = ""
            for (i = 1; i <= k; i++) {
                if (k == i) {
                    if(row != ""){
                        var pdd = document.getElementById("cmbPdd_" + i).value;
                        var txtPdd = document.getElementById("txtPdd_" + i).value;
                        var dueDate = document.getElementById("dueDate_" + i).value;
                        var chkDueDate = document.getElementById("chkDueDate_" + i).checked;
                        NewStr += pdd + "µ" + txtPdd + "µ" + dueDate + "µ" + chkDueDate + "¥";
                    }else{
                        NewStr += "µµµ¥";
                    }
                }
                else {
                    NewStr += row[i-1] + "¥";
                }
            }
            document.getElementById("<%= hidRowData.ClientID %>").value = NewStr;
        }
        function DeleteRow(k) {
            updateValue(rowLen);
            row = document.getElementById("<%= hidRowData.ClientID %>").value.split("¥");
            var NewStr = ""
            for (n = 1; n <= row.length-1; n++) {
                if (k != n) {
                    try {
                        var pdd = document.getElementById("cmbPdd_" + n).value;
                        var txtPdd = document.getElementById("txtPdd_" + n).value;
                        var dueDate = document.getElementById("dueDate_" + n).value;
                        var chkDueDate = document.getElementById("chkDueDate_" + n).checked;
                        NewStr += pdd + "µ" + txtPdd + "µ" + dueDate + "µ" + chkDueDate + "¥";
                    }
                    catch (e) { }
                }
            }
            document.getElementById("<%= hidRowData.ClientID %>").value = NewStr;
            TableFill(row.length - 2);
        }
        function BranchCodeOnchange() {
            var branch_id = document.getElementById("<%= cmbBranchCode.ClientID %>").value;
            document.getElementById("<%= cmbBranchName.ClientID %>").value = branch_id;
        }
        function BranchNameOnchange() {
            var branch_name = document.getElementById("<%= cmbBranchName.ClientID %>").value;
            document.getElementById("<%= cmbBranchCode.ClientID %>").value = branch_name;
        }
        function CategoryOnchange() {
            var cate_id = document.getElementById("<%= cmbLoanCategory.ClientID %>").value;
            var ToData = "1Ø" + cate_id.toString();
            ToServer(ToData, 1);
        }
        function PDDOnChange(i){
            var cmbPdd = document.getElementById("cmbPdd_"+i).value;
            if(cmbPdd == 1000){
                if(txtPddCount > 8){
                    alert("Maximum other PDDs that can be added is 8.");
                    document.getElementById("cmbPdd_"+i).value == -1;
                    document.getElementById("txtPdd_"+i).style.display = 'none';
                    return false;
                }else{
                    txtPddCount += 1;
                    document.getElementById("txtPdd_"+i).style.display = '';
                }
            }else{
                document.getElementById("txtPdd_"+i).style.display = 'none';
            }
        }
        function DueDateOnChange(i){
            var date = document.getElementById("dueDate_"+i).value;
            if(date != ""){
                document.getElementById("chkDueDate_"+i).disabled = true;
            }else{
                document.getElementById("chkDueDate_"+i).disabled = false;
            }
        }
        function chkDateOnChange(i){
            var chk = document.getElementById("chkDueDate_"+i).checked;
            if(chk == true){
                document.getElementById("dueDate_"+i).disabled = true;
            }else{
                document.getElementById("dueDate_"+i).disabled = false;
            }
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                var comboDataProduct = Data[0].toString();
                ComboFill(comboDataProduct, "<%= cmbLoanProduct.ClientID %>");
                var comboDataPdd = Data[1].toString();
                ComboFill(comboDataPdd, "cmbPdd_1");
                document.getElementById("<%= hidPDD.ClientID %>").value = comboDataPdd;
            } else if (context == 2) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) {
                    window.open("<%= Page.ResolveUrl("~")%>Business/PDD/PDD_Disbursement_Entry.aspx", "_self");
                }
            }else if (context == 3) {
                var Data = arg.split("Ø");
                ComboFill(Data[7], "<%= cmbLoanProduct.ClientID %>"); 
                document.getElementById("<%= cmbBranchCode.ClientID %>").value = Data[0];
                document.getElementById("<%= cmbBranchName.ClientID %>").value = Data[0];
                document.getElementById("<%= txtCustomerName.ClientID %>").value = Data[2];
                document.getElementById("<%= txtAccountNo.ClientID %>").value = Data[3];
                document.getElementById("<%= cmbLoanCategory.ClientID %>").value = Data[4];
                document.getElementById("<%= cmbLoanProduct.ClientID %>").value = Data[5]; 
                document.getElementById("<%= txtDisbDate.ClientID %>").value = Data[6];
                document.getElementById("<%= hidHisRowData.ClientID %>").value = Data[8].toString();
                document.getElementById("<%= hidPDD.ClientID %>").value = Data[9].toString();
                var rLen = Data[8].toString().split("¥");
                //TableFill(rLen.length-1);
                TableFill(1);
                HisTableFill(rLen.length - 1);
            } else if (context == 4) {
                $('.Queue').show();
                $('.QueueData').hide();
                $('.newItem').hide();
                document.getElementById("<%= hidQueueData.ClientID %>").value = arg;
                QueueTableFill();
            } else if (context == 5) {
                $('.Queue').hide();
                $('.DateQueue').hide();
                $('.QueueData').show();
                $('.newItem').show();
                var Data = arg.split("Ø");
                document.getElementById("<%= txtWINo.ClientID %>").value = Data[12];
                document.getElementById("<%= cmbBranchCode.ClientID %>").value = Data[0];
                document.getElementById("<%= cmbBranchName.ClientID %>").value = Data[0];
                document.getElementById("<%= txtCustomerName.ClientID %>").value = Data[2];
                document.getElementById("<%= txtAccountNo.ClientID %>").value = Data[3];
                document.getElementById("<%= cmbLoanCategory.ClientID %>").value = Data[13];
                ComboFill(Data[16], "<%= cmbLoanProduct.ClientID %>"); 
                ComboFill(Data[17], "cmbPdd_1"); 
                document.getElementById("<%= cmbLoanProduct.ClientID %>").value = Data[14]; 
                document.getElementById("<%= txtDisbDate.ClientID %>").value = Data[6];
                document.getElementById("<%= hidHisRowData.ClientID %>").value = Data[8].toString();
                if(Data[9]){
                    document.getElementById("<%= hidPDD.ClientID %>").value = Data[9].toString();
                    document.getElementById("<%= txtSOOn.ClientID %>").value = Data[9].toString();
                }
                if(Data[10]){
                    document.getElementById("<%= txtSOBy.ClientID %>").value = Data[10].toString();
                }
                if(Data[11]){
                    document.getElementById("<%= txtSORemark.ClientID %>").value = Data[11].toString();
                }
                document.getElementById("cmbPdd_1").value = Data[15];
                if(Data[15] == 1000){
                    document.getElementById("txtPdd_1").style.display = "";
                    document.getElementById("txtPdd_1").value = Data[7];
                }
                if (Data[8].toString() == '1900-01-01') {
                    document.getElementById("dueDate_1").disabled = true;
                    document.getElementById("chkDueDate_1").checked = true;
                }else if (Data[8].toString()==""){
                    document.getElementById("dueDate_1").disabled = true;
                    document.getElementById("chkDueDate_1").checked = true;
                } else {
                    document.getElementById("dueDate_1").value = Data[8].toString("yyyy/mm/dd");
                    document.getElementById("chkDueDate_1").disabled = true;
                }
                $('.addCol').hide();
            }else if (context == 6){
                $('.DateQueue').show();
                $('.Queue').hide();
                $('.QueueData').hide();
                $('.newItem').hide();
                document.getElementById("<%= hidQueueData.ClientID %>").value = arg;
                document.getElementById("<%= pnlHis.ClientID %>").style.display = 'none';
                DateTableFill();
            }
        }
        function btnConfirm_onclick() {
            var wiNum = document.getElementById("<%= txtWINo.ClientID %>").value;
            if (wiNum == "") {
                alert("Enter work item number");
                return false;
            }
            var branchCode = document.getElementById("<%= cmbBranchCode.ClientID %>").value;
            if (branchCode == -1) {
                alert("Select branch code");
                return false;
            }
            var branchName = document.getElementById("<%= cmbBranchName.ClientID %>").value;
            if (branchName == -1) {
                alert("Select branch name");
                return false;
            }
            var customerName = document.getElementById("<%= txtCustomerName.ClientID %>").value;
            if (customerName == "") {
//                alert("Enter customer name");
//                return false;
            }
            var loanNum = document.getElementById("<%= txtAccountNo.ClientID %>").value;
            if(loanNum == ""){
//                alert("Enter loan account number");
//                return false;
            }else{
            var isnum = /^\d+$/.test(loanNum);
            if(isnum == 0){
                alert("Enter Valid Loan Account Number");
                return false;
            }}
            var category = document.getElementById("<%= cmbLoanCategory.ClientID %>").value;
            if (category == -1) {
                alert("Select loan category");
                return false;
            }
            var product = document.getElementById("<%= cmbLoanProduct.ClientID %>").value;
            if (product == -1) {
                alert("Select loan product");
                return false;
            }
            var disbDate = document.getElementById("<%= txtDisbDate.ClientID %>").value;
            if (disbDate == "") {
                alert("Enter disbursal date");
                return false;
            }
            var NewStr = '';
            for (var n=1; n<=rowLen; n++) {
                var pdd = document.getElementById("cmbPdd_" + n).value;
                var txtPdd = document.getElementById("txtPdd_" + n).value;
                if (pdd == -1) {
                    alert("Select PDD Item");
                    return false;
                }else if(pdd == 1000){
                    if(txtPdd == ""){
                        alert("Enter PDD Item");
                        return false;
                    }
                }
                var dueDate = document.getElementById("dueDate_" + n).value;
                var chkDueDate = document.getElementById("chkDueDate_" + n).checked;
                if(chkDueDate == true){
                    chkDueDate = 1;
                }else{
                    chkDueDate = 0;
                }
                if (dueDate == "" && chkDueDate == 0) {
                    alert("Kindly update or due date or select 'To Be Updated' option");
                    return false;
                }
                NewStr += pdd + "µ" + txtPdd + "µ" + dueDate + "µ" + chkDueDate + "¥";
            }
            var pddData = NewStr;
            var ToData = "2Ø" + wiNum + "Ø" + branchCode + "Ø" + customerName + "Ø" + loanNum + "Ø" + category + "Ø" + product + "Ø" + disbDate + "Ø" + pddData + "Ø" + rowLen + "Ø" + status;
            ToServer(ToData, 2);
        }
        function WIOnChange(){
            var WINo = document.getElementById("<%= txtWINo.ClientID %>").value;
            var ToData = "3Ø" + WINo;
            ToServer(ToData, 3);
        }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
    }       
    function LoanNumChange(){
        var loanNum = document.getElementById("<%= txtAccountNo.ClientID %>").value;
        var isnum = /^\d+$/.test(loanNum);
        if(isnum == 0){
            alert("Enter Valid Loan Account Number");
            return false;
        }
    }
        function HisTableFill(k) {
            document.getElementById("<%= pnlHis.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:left;margin: 0px auto;' align='left'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center'>";
            tab += "<tr class=mainhead>";
            tab += "<td style='width:10%;text-align:center'>Sl.No</td>";
            tab += "<td style='width:35%;text-align:center' >PDD Item</td>";
            tab += "<td style='width:25%;text-align:center;'>Due Date</td>";
            tab += "<td style='width:30%;text-align:center;'>Status</td>";
            tab += "</tr>";

            for (var i = 1; i <= k; i++) {
                tab += "<tr class=sub_first style ='height:auto;'>";
                tab += "<td style='width:10%;text-align:center'>" + i + "</td>";
                tab += "<td style='width:35%;text-align:center' id='pdd_" + i + "'></td>";
                tab += "<td style='width:25%;text-align:center' id='dueDt_" + i + "'></td>";
                tab += "<td style='width:30%;text-align:center' id='status_" + i + "'></td>";
                tab += "</tr>";
            }
            tab += "</table>";
            tab += "</div>";
            document.getElementById("<%= pnlHis.ClientID %>").innerHTML = tab;
            try {
                var rowData = document.getElementById("<%= hidHisRowData.ClientID %>").value.split("¥");
                for (var i = 1; i <= rowData.length; i++) {
                    var data = rowData[i-1].toString();
                    var rData = data.split("µ");
                        if (rData[0] == 1000) {
                            document.getElementById('pdd_' + i).innerHTML = rData[2];
                        } else {
                            document.getElementById('pdd_' + i).innerHTML = rData[1];
                        }
                        document.getElementById('dueDt_' + i).innerHTML = rData[3];
                        document.getElementById('status_' + i).innerHTML = rData[4];
                }
            } catch (e) { }
        }
        function statOnClick(n) {
            if (n == 1) {
                document.location.reload(true);
                $('.Queue').hide();
                $('.DateQueue').hide();
                $('.QueueData').hide();
                $('.newItem').show();
            } if(n == 3){
                var ToData = "6Ø";
                ToServer(ToData, 6);
            }else {
                var ToData = "4Ø";
                ToServer(ToData, 4);
            }
        }
        function QueueTableFill() {
            var QueueData = "";            
            document.getElementById("<%= QueuePanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var orgData = document.getElementById("<%= hidQueueData.ClientID %>").value.split("Ø");
            var chk = orgData[1];
            var tab = "";            
            tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='QueueTable'>";
            tab += "<tr class=mainhead style='height:50px;'>";
            tab += "<td style='width:5%;text-align:center;' >WI.No</td>";
            tab += "<td style='width:9%;text-align:center;' >Branch Name</td>";
            tab += "<td style='width:10%;text-align:center;' >Customer Name</td>";
            tab += "<td style='width:11%;text-align:center;' >Loan Account No</td>";
            tab += "<td style='width:9%;text-align:center;' >Loan Product</td>";
            tab += "<td style='width:7%;text-align:center;' >Disbursal Date</td>";
            tab += "<td style='width:9%;text-align:center;' >PDD Item</td>";
            tab += "<td style='width:7%;text-align:center;' >Due Date</td>";
            tab += "<td style='width:7%;text-align:center;' >Returned Date</td>";
            tab += "<td style='width:10%;text-align:center;' >Returned By</td>";
            tab += "<td style='width:12%;text-align:center;' >Returned Remarks</td>";
            tab += "<td style='width:4%;text-align:center;' >Next</td>";
            tab += "</tr>";
            QueueData = orgData[0].split("Ñ");
            var QueueLen = QueueData.length-1;
            for(var k=0;k<QueueLen;k++){
                var QueueRow = QueueData[k].split("£");
                var mainRow = QueueRow[0].split("ÿ");
                var subRow = QueueRow[1].split("¢");
                var subLen = subRow.length;
                if(subLen-1 != 0){
                    tab += "<tr class=sub_first style ='height:auto;' id='row_"+k+"'>";
                    tab += "<td style='width:5%;text-align:center'>"+mainRow[1]+"</td>";
                    tab += "<td style='width:9%;text-align:center'>"+mainRow[2]+"</td>";
                    tab += "<td style='width:10%;text-align:center'>"+mainRow[3]+"</td>";
                    tab += "<td style='width:11%;text-align:center'>"+mainRow[4]+"</td>";
                    tab += "<td style='width:9%;text-align:center'>"+mainRow[6]+"</td>";
                    tab += "<td style='width:7%;text-align:center'>"+mainRow[7]+"</td>";
                    tab += "<td style='width:8.1%;text-align:center' colspan='6'>"
                    tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px 0px;' align='center'>";
                    tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='left'>";  
                    var wi_no = '';
                    for(var l = 0;l<subLen-1;l++){
                        var subData = subRow[l].split("€");                     
                        tab += "<tr class=sub_first style ='height:30px;'>";
                        tab += "<td style='width:18.3%;text-align:center'>" + subData[0] + "</td>";
                        tab += "<td style='width:14.2%;text-align:center'>" + subData[1] + "</td>";
                        tab += "<td style='width:14.2%;text-align:center'>" + subData[3] + "</td>";
                        tab += "<td style='width:20.4%;text-align:center'>" + subData[4] + "</td>";
                        tab += "<td style='width:24.4%;text-align:center'>" + subData[5] + "</td>";
                        wi_no = mainRow[0];
                        tab += "<td style='width:8.16%;text-align:center'><img src='../../image/update2.png' title='Update' style='height:22px; width:22px; cursor:pointer;' onclick='ReqUpdateOnClick(1," + subData[2].toString() + ",\"" + wi_no.toString() + "\")' /></td>";
                        tab += "</tr>";
                    }
                    tab += "</table>";
                    tab += "</div>";
                    tab += "</td>";                
                    tab += "</tr>";    
                }            
            }
            tab += "</table></div>";
            document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = tab;    
            if(QueueData.toString() == ""){
                document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = "<table style='width:100%'><tr><td style='width:30%'></td><td style='width:40%;text-align:center;font-size:16px;'><b>No requests pending.</b></td><td style='width:30%'></td>";  
            }   
        }
        function DateTableFill() {
            var QueueData = "";            
            document.getElementById("<%= DatePanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var orgData = document.getElementById("<%= hidQueueData.ClientID %>").value.split("Ø");
            var chk = orgData[1];
            var tab = "";            
            tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='QueueTable'>";
            tab += "<tr class=mainhead style='height:50px;'>";
            tab += "<td style='width:5%;text-align:center;' >WI.No</td>";
            tab += "<td style='width:9%;text-align:center;' >Branch Name</td>";
            tab += "<td style='width:10%;text-align:center;' >Customer Name</td>";
            tab += "<td style='width:11%;text-align:center;' >Loan Account No</td>";
            tab += "<td style='width:9%;text-align:center;' >Loan Product</td>";
            tab += "<td style='width:7%;text-align:center;' >Disbursal Date</td>";
            tab += "<td style='width:9%;text-align:center;' >PDD Item</td>";
            tab += "<td style='width:7%;text-align:center;' >Due Date</td>";
            tab += "<td style='width:7%;text-align:center;' >Returned Date</td>";
            tab += "<td style='width:10%;text-align:center;' >Returned By</td>";
            tab += "<td style='width:12%;text-align:center;' >Returned Remarks</td>";
            tab += "<td style='width:4%;text-align:center;' >Next</td>";
            tab += "</tr>";
            QueueData = orgData[0].split("Ñ");
            var QueueLen = QueueData.length-1;
            for(var k=0;k<QueueLen;k++){
                var QueueRow = QueueData[k].split("£");
                var mainRow = QueueRow[0].split("ÿ");
                var subRow = QueueRow[1].split("¢");
                var subLen = subRow.length;
                if(subLen-1 != 0){
                    tab += "<tr class=sub_first style ='height:auto;' id='row_"+k+"'>";
                    tab += "<td style='width:5%;text-align:center'>"+mainRow[0]+"</td>";
                    tab += "<td style='width:9%;text-align:center'>"+mainRow[2]+"</td>";
                    tab += "<td style='width:10%;text-align:center'>"+mainRow[3]+"</td>";
                    tab += "<td style='width:11%;text-align:center'>"+mainRow[4]+"</td>";
                    tab += "<td style='width:9%;text-align:center'>"+mainRow[6]+"</td>";
                    tab += "<td style='width:7%;text-align:center'>"+mainRow[7]+"</td>";
                    tab += "<td style='width:8.1%;text-align:center' colspan='6'>"
                    tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px 0px;' align='center'>";
                    tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='left'>";  
                    var wi_no = '';
                    for(var l = 0;l<subLen-1;l++){
                        var subData = subRow[l].split("€");     
                        tab += "<tr class=sub_first style ='height:30px;'>";
                        tab += "<td style='width:18.3%;text-align:center'>" + subData[0] + "</td>";
                        tab += "<td style='width:14.2%;text-align:center'>" + subData[1] + "</td>";
                        tab += "<td style='width:14.2%;text-align:center'>" + subData[3] + "</td>";
                        tab += "<td style='width:20.4%;text-align:center'>" + subData[4] + "</td>";
                        tab += "<td style='width:24.4%;text-align:center'>" + subData[5] + "</td>";
                        wi_no = mainRow[0];
                        tab += "<td style='width:8.16%;text-align:center'><img src='../../image/update2.png' title='Update' style='height:22px; width:22px; cursor:pointer;' onclick='ReqUpdateOnClick(2," + subData[2].toString() + ",\"" + wi_no.toString() + "\")' /></td>";
                        tab += "</tr>";
                    }
                    tab += "</table>";
                    tab += "</div>";
                    tab += "</td>";                
                    tab += "</tr>";    
                }            
            }
            tab += "</table></div>";
            document.getElementById("<%= DatePanel.ClientID %>").innerHTML = tab;    
            if(QueueData.toString() == ""){
                document.getElementById("<%= DatePanel.ClientID %>").innerHTML = "<table style='width:100%'><tr><td style='width:30%'></td><td style='width:40%;text-align:center;font-size:16px;'><b>No requests pending.</b></td><td style='width:30%'></td>";  
            }   
        }
        function ReqUpdateOnClick(stat,sub_id, wi_no) {
            var ToData = "5Ø" + sub_id + "Ø" + wi_no + "Ø" + stat;
            status = stat;
            ToServer(ToData, 5);
        }
    </script>   
</head>
</html>
<br />
<div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">
            <tr> 
                <td style="width:25%;">
                    <asp:HiddenField ID="hidPDD" runat="server" />
                    <asp:HiddenField ID="hidRowData" runat="server" />
                    <asp:HiddenField ID="hidHisRowData" runat="server" />
                    <asp:HiddenField ID="hidQueueData" runat="server" />
                </td>
                <td style="width:25%; text-align:left;"></td>
                <td style="width:25%"></td>
                <td style="width:25%"></td>
            </tr>
            <tr>
                <td style="width: 25%"></td>
                <td style="width: 25%; text-align: center" colspan="2">
                    <input id="stat_1" type="radio" name="stat" value="1" checked="checked" onclick='statOnClick(1);' />
                    <label for="stat_1">New</label>
                    &nbsp;&nbsp;&nbsp
                    <input id="stat_2" type="radio" name="stat" value="2" onclick='statOnClick(2);' />
                    <label for="stat_2">Returned</label>
                    &nbsp;&nbsp;&nbsp
                    <input id="stat_3" type="radio" name="stat" value="3" onclick='statOnClick(3);' />
                    <label for="stat_3">To Be Updated</label>
                </td>
                <td style="width: 25%"></td>
            </tr>
            <tr><td><br /><br />&nbsp;&nbsp;</td></tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Work Item Number 
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtWINo" class="NormalText" runat="server" Width="99.3%" onkeypress="NumericCheck(event)" onchange="return WIOnChange()"></asp:TextBox>
                </td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch Code
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:DropDownList ID="cmbBranchCode" runat="server" class="NormalText" onkeypress="NumericCheck(event)" Width="100%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch Name
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:DropDownList ID="cmbBranchName" runat="server" class="NormalText" Width="100%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Customer Name
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtCustomerName" class="NormalText" runat="server" Width="99.3%" onkeypress="AlphaSpaceCheck(event)"></asp:TextBox>
                </td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Account Number
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtAccountNo" class="NormalText" runat="server" Width="99.3%" onchange="LoanNumChange()" MaxLength="14" onkeypress="NumericCheck(event)" ></asp:TextBox>
                </td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Category
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:DropDownList ID="cmbLoanCategory" runat="server" class="NormalText" Width="100%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Product
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:DropDownList ID="cmbLoanProduct" runat="server" class="NormalText" Width="100%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="newItem">
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Disbursal Date
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtDisbDate" class="NormalText sec9" ReadOnly="true" runat="server" Width="50%" onkeypress="return false" >
                    </asp:TextBox>
                    <asp:CalendarExtender ID="txtDisbDate_CalendarExtender"  runat="server" Enabled="True" TargetControlID="txtDisbDate" Format="dd MMM yyyy">
                    </asp:CalendarExtender>
                </td>
            </tr>
            <tr class="QueueData">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   SO Returned By
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtSOBy" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr class="QueueData">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   SO Returned Date
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtSOOn" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr class="QueueData">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   So Returned Remarks
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtSORemark" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr class="newItem">
                <td style="width:25%;"><br /></td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%"></td>
                <td style="width:25%;text-align:left;" colspan="2">
                    <asp:Panel ID="pnlPDD" runat="server" align="left" style="text-align:left !important;">
                    </asp:Panel>
                </td>
            </tr>
            <tr class="newItem">
                <td style="width:25%;"><br /></td>
            </tr>
            <tr class="newItem">    
                <td style="width:25%"></td>
                <td style="width:25%;text-align:left;" colspan="2">
                    <asp:Panel ID="pnlHis" runat="server" align="left" style="text-align:left !important;">
                    </asp:Panel>
                </td>
            </tr>
            <tr class="Queue">    
                <td style="width:25%;text-align:left;" colspan="4">
                    <asp:Panel ID="QueuePanel" runat="server" align="left" style="text-align:left !important;">
                    </asp:Panel>
                </td>
            </tr>
            <tr class="DateQueue">    
                <td style="width:25%;text-align:left;" colspan="4">
                    <asp:Panel ID="DatePanel" runat="server" align="left" style="text-align:left !important;">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="text-align:center;" colspan="2"><br />
                    <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
                </td>
            </tr>
        </table>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

