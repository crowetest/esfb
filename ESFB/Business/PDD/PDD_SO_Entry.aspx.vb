﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PDD_SO_Entry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Me.Master.subtitle = "PDD SO Entry"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            Dim WI_No As String = CStr(Request.QueryString.Get("WI_No"))
            Dim Sub_No As Integer = CInt(Request.QueryString.Get("Sub_No"))
            Me.hidSubID.Value = Sub_No.ToString()
            Me.txtWINo.Text = WI_No
            Me.btnSave.Attributes.Add("onclick", "return btnConfirm_onclick()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window.onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim SubId As String = Data(1)
            Dim WiNo As String = Data(2)
            DT = DB.ExecuteDataSet("SELECT A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, D.CATEGORY_NAME, E.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) AS [DISB_DATE], CASE WHEN B.PDD_ITEM = 1000 THEN B.PDD_OTHER ELSE C.PDD_NAME END AS PDD, CONVERT(VARCHAR(10),B.DUE_DATE) AS [DUE_DATE] FROM PDD_ENTRY_MASTER A INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO LEFT JOIN PDD_MASTER C ON C.PDD_ID = B.PDD_ITEM INNER JOIN PDD_CATEGORY_MASTER D ON A.CATEGORY = D.CATEGORY_ID INNER JOIN PDD_PRODUCT_MASTER E ON E.PRODUCT_ID = A.PRODUCT WHERE A.WI_NO = '" + WiNo + "' AND B.SUB_NO = " + SubId).Tables(0)
            CallBackReturn += DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString()
        End If
    End Sub
#End Region
    Private Sub initializeControls()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContentType As String = ""
            Dim AttachImg As Byte() = Nothing
            Dim hfc As HttpFileCollection
            hfc = Request.Files
            Dim Data As String() = hdnValue.Value.Split(CChar("Ø"))
            Dim wiNum As String = CStr(Data(1))
            Dim subID As Integer = CInt(Data(2))
            Dim Status As Integer = CInt(Data(3))
            Dim Remarks As String = Data(4)
            Dim SubDate As Date = CDate(Data(5))
            Dim UserID As String = Session("UserID").ToString()
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Dim NoofAttachments As Integer = 0
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        NoofAttachments += 1
                    End If
                Next
            End If
            Try
                Dim Params(7) As SqlParameter
                Params(0) = New SqlParameter("@wiNum", SqlDbType.VarChar, 50)
                Params(0).Value = wiNum
                Params(1) = New SqlParameter("@subID", SqlDbType.Int)
                Params(1).Value = subID
                Params(2) = New SqlParameter("@Status", SqlDbType.Int)
                Params(2).Value = Status
                Params(3) = New SqlParameter("@Remarks", SqlDbType.VarChar, 500)
                Params(3).Value = Remarks
                Params(4) = New SqlParameter("@SubDate", SqlDbType.DateTime)
                Params(4).Value = SubDate
                Params(5) = New SqlParameter("@UserID", SqlDbType.VarChar, 25)
                Params(5).Value = UserID
                Params(6) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(6).Direction = ParameterDirection.Output
                Params(7) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(7).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_PDD_SO_APPROVE", Params)
                ErrorFlag = CInt(Params(6).Value)
                Message = CStr(Params(7).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            If ErrorFlag = 0 Then
                initializeControls()
            End If
            If hfc.Count > 0 Then
                For i = 0 To hfc.Count - 1
                    Dim myFile As HttpPostedFile = hfc(i)
                    Dim nFileLen As Integer = myFile.ContentLength
                    Dim FileName As String = ""
                    If (nFileLen > 0) Then
                        ContentType = myFile.ContentType
                        FileName = myFile.FileName
                        AttachImg = New Byte(nFileLen - 1) {}
                        myFile.InputStream.Read(AttachImg, 0, nFileLen)
                        Dim Params(4) As SqlParameter
                        Params(0) = New SqlParameter("@wiNum", SqlDbType.VarChar, 50)
                        Params(0).Value = wiNum
                        Params(1) = New SqlParameter("@ATTACHIMG", SqlDbType.VarBinary)
                        Params(1).Value = AttachImg
                        Params(2) = New SqlParameter("@CONTENTTYPE", SqlDbType.VarChar, 500)
                        Params(2).Value = ContentType
                        Params(3) = New SqlParameter("@FileName", SqlDbType.VarChar, 500)
                        Params(3).Value = FileName
                        Params(4) = New SqlParameter("@subID", SqlDbType.Int)
                        Params(4).Value = subID
                        DB.ExecuteNonQuery("SP_PDD_ATTACH", Params)
                    End If

                Next
            End If


            Dim cl_script1 As System.Text.StringBuilder = New System.Text.StringBuilder()
            cl_script1.Append("         alert('" + Message + "');")
            cl_script1.Append("        window.open('PDD_SO_Queue.aspx', '_self');")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "client script", cl_script1.ToString(), True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If

        End Try
    End Sub

End Class
