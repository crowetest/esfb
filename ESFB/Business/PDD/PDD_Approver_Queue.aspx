﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PDD_Approver_Queue.aspx.vb" Inherits="PDD_Approver_Queue" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () { 
            var QueueData = document.getElementById("<%= hidQueueData.ClientID %>").value;                
            TableFill();     
        }
        function FromServer(arg, context) {             
            if (context == 1) {         
                 var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) {
//                    window.open("<%= Page.ResolveUrl("~")%>Business/PDD/PDD_Approver_Queue.aspx", "_self");
                    var cate = document.getElementById("<%= cmbLoanCategory.ClientID %>").value;
                    var ToData = "2Ø" + cate;
                    ToServer(ToData, 2);
                }
            }
            if (context == 2) {
                document.getElementById("<%= hidQueueData.ClientID %>").value = arg;
                var QueueData = document.getElementById("<%= hidQueueData.ClientID %>").value;                
                TableFill(); 
            }
        }
        function SaveOnClick(sub_id,wi_no,l){
            var status = document.getElementById("cmbStatus_"+l).value;
            var remarks = document.getElementById("txtRemarks_"+l).value;
            var ToData = "1Ø" + sub_id.toString() + "Ø" + wi_no + "Ø" + status + "Ø" + remarks;
            ToServer(ToData, 1);
        }
        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function TableFill(){
            var QueueData = "";            
            document.getElementById("<%= QueuePanel.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";            
            tab += "<div style='width:98%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px auto;' align='center'>";
            tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='center' id='QueueTable'>";
            tab += "<tr class=mainhead style='height:50px;'>";
            tab += "<td style='width:4%;text-align:center;' >WI.No</td>";
            tab += "<td style='width:4%;text-align:center;display:none;' >Branch Code</td>";
            tab += "<td style='width:7%;text-align:center;' >Branch Name</td>";
            tab += "<td style='width:7%;text-align:center;' >Customer Name</td>";
            tab += "<td style='width:7%;text-align:center;' >Loan Account No</td>";
            tab += "<td style='width:8%;text-align:center;display:none;' >Category</td>";
            tab += "<td style='width:8%;text-align:center;' >Loan Product</td>";
            tab += "<td style='width:6%;text-align:center;' >Disbursal Date</td>";
            tab += "<td style='width:8%;text-align:center;' >PDD Item</td>";
            tab += "<td style='width:6%;text-align:center;' >Due Date</td>";
            tab += "<td style='width:7%;text-align:center;' >Remark By SO</td>";
            tab += "<td style='width:6%;text-align:center;' >Date Of Submission</td>";
            tab += "<td style='width:2%;text-align:center;' ></td>";
            tab += "<td style='width:8%;text-align:center;' >Approve Status</td>";
            tab += "<td style='width:8%;text-align:center;' >Remarks</td>";
            tab += "<td style='width:4%;text-align:center;' ></td>";
            tab += "</tr>";
            QueueData = document.getElementById("<%= hidQueueData.ClientID %>").value.split("Ñ");
            var QueueLen = QueueData.length-1;
            var x = 0;
            for(var k=0;k<QueueLen;k++){
                var QueueRow = QueueData[k].split("£");
                var mainRow = QueueRow[0].split("ÿ");
                var subRow = QueueRow[1].split("¢");
                var subLen = subRow.length;
                if(subLen-1 != 0){
                    tab += "<tr id='row_"+k+"' class=sub_first style ='height:auto;'>";
                    tab += "<td style='width:4%;text-align:center'>"+mainRow[0]+"</td>";
                    tab += "<td style='width:4%;text-align:center;display:none;'>" + mainRow[1] + "</td>";
                    tab += "<td style='width:7%;text-align:center'>"+mainRow[2]+"</td>";
                    tab += "<td style='width:7%;text-align:center'>"+mainRow[3]+"</td>";
                    tab += "<td style='width:7%;text-align:center'>"+mainRow[4]+"</td>";
                    tab += "<td style='width:8%;text-align:center;display:none;'>" + mainRow[5] + "</td>";
                    tab += "<td style='width:8%;text-align:center'>"+mainRow[6]+"</td>";
                    tab += "<td style='width:6%;text-align:center'>"+mainRow[7]+"</td>";
                    tab += "<td style='width:49%;text-align:center' colspan='8'>"
                    tab += "<div style='width:100%; height:auto;background-color:#A34747; overflow:auto;text-align:center;margin: 0px 0px;' align='center'>";
                    tab += "<table style='width:100%;font-family:'cambria';background-color:#C0D0E5;' align='left'>";  
                    var wi_no = '';
                    for(var l = 0;l<subLen-1;l++){
                        var subData = subRow[l].split("€");                     
                        tab += "<tr class=sub_first style ='height:40px;'>";
                        tab += "<td style='width:16.3%;text-align:center'>"+subData[0]+"</td>";
                        tab += "<td style='width:12.2%;text-align:center'>"+subData[1]+"</td>";
                        tab += "<td style='width:14.2%;text-align:center'>"+subData[3]+"</td>";
                        tab += "<td style='width:12.2%;text-align:center'>"+subData[4]+"</td>";
                        tab += "<td style='width:4%;text-align:center'>";
                        tab += "<img id='ViewReport' src='../../Image/attchment2.png' onclick='viewReport("+subData[2]+")' title='View attachment'  style='height:20px; width:20px;  cursor:pointer;' >";			                       
                        tab += "</td>";
                        var select = "<select id = 'cmbStatus_"+x+"' >";
                        select += "<option value='-1'>--SELECT--</option>";
                        select += "<option value='1'>Approved</option>";
                        select += "<option value='2'>Returned</option>";
                        tab += "<td style='width:16.3%;text-align:center'>"+select+"</td>";
                        tab += "<td style='width:16.3%;text-align:center'><textarea id='txtRemarks_"+x+"' style='width:90%;' onkeypress='return TextAreaCheck(event)'></textarea></td>";
                        wi_no = mainRow[0];
                        tab += "<td style='width:8.1%;text-align:center'><input type='button' style='width:95%' value='SAVE' onclick='SaveOnClick("+ subData[2].toString() +",\""+wi_no.toString()+"\","+x+")' /></td>";
                        tab += "</tr>";
                        x= x+1;
                    }
                    tab += "</table>";
                    tab += "</div>";
                    tab += "</td>";                
                    tab += "</tr>";        
                }
            }
            tab += "</table></div>";
            document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = tab;    
            if(QueueData.toString() == ""){
                document.getElementById("<%= QueuePanel.ClientID %>").innerHTML = "<table style='width:100%'><tr><td style='width:30%'></td><td style='width:40%;text-align:center;font-size:16px;'><b>No requests pending.</b></td><td style='width:30%'></td>";  
            }   
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 0; a < rows.length-1; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function viewReport(ID){
            window.open("<%= Page.ResolveUrl("~")%>Business/Reports/ShowMultipleAttachments.aspx?SubID=" + btoa(ID),"_blank");
        }
        function CategoryOnchange() {
            var cate = document.getElementById("<%= cmbLoanCategory.ClientID %>").value;
            var ToData = "2Ø" + cate;
            ToServer(ToData, 2);
        }
    </script>   
</head>
</html>
<br />
<div  style="width:98%; height:auto; margin:0px auto; background-color:#EEB8A6;">
<br />
    <div id = "divSection1" class = "sec1" style="width:98%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
    <br />   <br />
        <table>
        <tr class="newItem">    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Category
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:DropDownList ID="cmbLoanCategory" runat="server" class="NormalText" Width="100%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            </table>
        <table class="style1" id="UserTable" style="width:100%;margin: 0px auto;">
            
            <tr>
                <td>
                    <asp:HiddenField ID="hidQueueData" runat="server" />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width:98%;">
                    <asp:Panel ID="QueuePanel" runat="server">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <div style="width:80%;height:auto; padding-left:10px;margin:0px auto;">
            <div style="text-align:center; height: 63px;"><br />
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width:8%;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
            </div>
        </div>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

