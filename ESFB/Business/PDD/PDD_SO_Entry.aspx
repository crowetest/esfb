﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PDD_SO_Entry.aspx.vb" Inherits="PDD_SO_Entry" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">   </asp:ToolkitScriptManager>
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
            var today = new Date();
            today = today.toShortFormat();
            document.getElementById("<%= txtSubDate.ClientID %>").value = today;
            var subID = document.getElementById("<%= hidSubID.ClientID %>").value;
            var WINo = document.getElementById("<%= txtWINo.ClientID %>").value;
            var ToData = "1Ø" + subID.toString() + "Ø" + WINo;
            ToServer(ToData, 1);
        }
        Date.prototype.toShortFormat = function () {
            var month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var day = this.getDate();
            var month_index = this.getMonth();
            var year = this.getFullYear();
            return "" + day + " " + month_names[month_index] + " " + year;
        }
        function AlphaSpaceCheck(e) {
            var inputValue = e.which;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0 && inputValue != 121)) {
                e.preventDefault();
            }
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                document.getElementById("<%= txtBranchCode.ClientID %>").value = Data[0];
                document.getElementById("<%= txtBranchName.ClientID %>").value = Data[1];
                document.getElementById("<%= txtCustomerName.ClientID %>").value = Data[2];
                document.getElementById("<%= txtAccountNo.ClientID %>").value = Data[3];
                document.getElementById("<%= txtLoanCategory.ClientID %>").value = Data[4];
                document.getElementById("<%= txtLoanProduct.ClientID %>").value = Data[5];
                document.getElementById("<%= txtDisbDate.ClientID %>").value = Data[6];
                document.getElementById("<%= txtItem.ClientID %>").value = Data[7];
                if (Data[8] = '1900-01-01') {
                    document.getElementById("<%= txtDueDate.ClientID %>").value = 'To be updated';
                } else {
                    document.getElementById("<%= txtDueDate.ClientID %>").value = Data[8];
                }
            } else if (context == 2) {
                var Data = arg.split("Ø");
//                alert(Data[1]);
                if (Data[0] == 0) {
                    window.open("<%= Page.ResolveUrl("~")%>Business/PDD/PDD_SO_Queue.aspx", "_self");
                }
            }
        }
        function btnConfirm_onclick() {
            var wiNum = document.getElementById("<%= txtWINo.ClientID %>").value;
            var subID = document.getElementById("<%= hidSubID.ClientID %>").value;
            var Status = document.getElementById("<%= cmbStatus.ClientID %>").value;
            if(Status == -1){
                alert("Select status");
                return false;
            }
            var Remarks = document.getElementById("<%= txtRemarks.ClientID %>").value;
            var SubDate = document.getElementById("<%= txtSubDate.ClientID %>").value;
            var ToData = "2Ø" + wiNum + "Ø" + subID + "Ø" + Status + "Ø" + Remarks + "Ø" + SubDate;
            document.getElementById("<%= hdnValue.ClientID %>").value = ToData;
        }
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Business/PDD/PDD_SO_Queue.aspx", "_self");
    }
        function AddMoreImages() {
            if (!document.getElementById && !document.createElement)
                return false;
            var fileUploadarea = document.getElementById("fileUploadarea");
            if (!fileUploadarea)
                return false;
            var newLine = document.createElement("br");
            fileUploadarea.appendChild(newLine);
            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("class", "fileUpload");

            if (!AddMoreImages.lastAssignedId)
                AddMoreImages.lastAssignedId = 100;
            newFile.setAttribute("id", "FileUpload" + AddMoreImages.lastAssignedId);
            newFile.setAttribute("name", "FileUpload" + AddMoreImages.lastAssignedId);
            var div = document.createElement("div");
            div.appendChild(newFile);
            div.setAttribute("id", "div" + AddMoreImages.lastAssignedId);
            fileUploadarea.appendChild(div);
            AddMoreImages.lastAssignedId++;
        }
    </script>   
</head>
</html>
<br />
<div  style="width:90%;margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">
            <tr> 
                <td style="width:25%;">
                    <asp:HiddenField ID="hidPDD" runat="server" />
                    <asp:HiddenField ID="hidSubID" runat="server" />
                    <asp:HiddenField ID="hdnValue" runat="server" />
                </td>
                <td style="width:25%; text-align:left;"></td>
                <td style="width:25%"></td>
                <td style="width:25%"></td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Work Item Number 
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtWINo" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch Code
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtBranchCode" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch Name
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtBranchName" class="ReadOnlyTextBox" runat="server" ReadOnly="true" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Customer Name
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtCustomerName" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Account Number
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtAccountNo" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Category
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtLoanCategory" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Loan Product
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtLoanProduct" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Disbursal Date
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtDisbDate" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    PDD Item
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtItem" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Due Date
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtDueDate" class="ReadOnlyTextBox" runat="server" Width="99.3%" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Status
                </td>
                <td style="width:25%">
                    <asp:DropDownList ID="cmbStatus" runat="server" class="NormalText" Width="100.5%">
                       <asp:ListItem Value="-1"> -----SELECT-----</asp:ListItem>
                       <asp:ListItem Value="1">Submit</asp:ListItem>
                       <asp:ListItem Value="2">Return</asp:ListItem>
                    </asp:DropDownList>
                </td>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Remarks
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtRemarks" class="NormalText" runat="server" Width="99.7%" Rows="3" MaxLength="500" TextMode="MultiLine" onkeypress='return TextAreaCheck(event)' ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Date Of Submission
                </td>
                <td style="width:25%">
                    <asp:TextBox ID="txtSubDate" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.7%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%">
                    Upload Document
                </td>
                <td style="width:25%">
                    <div id="fileUploadarea">
                        <asp:FileUpload ID="fup1" runat="server" CssClass="fileUpload" /><br />
                    </div>
                    <div>
                        <img src="../../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddMoreImages()" title="Add New"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width:25%;"><br /></td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="text-align:center;" colspan="2"><br />
                    <asp:Button ID="btnSave" runat="server" Text="SAVE" Font-Names="Cambria" Style="cursor: pointer;width: 67px;" 
                    Width="6%" />&nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
                </td>
            </tr>
        </table>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

