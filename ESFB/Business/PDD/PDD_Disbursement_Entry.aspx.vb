﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PDD_Disbursement_Entry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1420) = False Then 'uat
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return

            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1434) = False Then 'main
                Response.Redirect("~/AccessDenied.aspx", False)
                Return

            End If

            Dim EmpCode = CInt(Session("UserID"))
            'DT = DB.ExecuteDataSet("SELECT * FROM PDD_TEAM_MASTER WHERE TEAM_ID = 1 AND EMP_CODE = " + EmpCode.ToString()).Tables(0)
            'If DT.Rows.Count = 0 Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "PDD Entry"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----SELECT-----' AS BRANCH_CODE UNION ALL SELECT BRANCH_ID,CONVERT(VARCHAR,BRANCH_ID ) FROM BRANCH_MASTER WHERE STATUS_ID = 1 ORDER BY BRANCH_ID").Tables(0)
            GF.ComboFill(cmbBranchCode, DT, 0, 1)
            DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----SELECT-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1 ORDER BY BRANCH_NAME").Tables(0)
            GF.ComboFill(cmbBranchName, DT, 0, 1)
            DT = DB.ExecuteDataSet("SELECT '-1' AS CATEGORY_ID,'-----SELECT-----' AS CATEGORY_NAME UNION ALL SELECT CATEGORY_ID,CATEGORY_NAME FROM PDD_CATEGORY_MASTER WHERE STATUS = 1 ORDER BY CATEGORY_ID").Tables(0)
            GF.ComboFill(cmbLoanCategory, DT, 0, 1)
            Me.cmbBranchCode.Attributes.Add("onchange", "return BranchCodeOnchange()")
            Me.cmbBranchName.Attributes.Add("onchange", "return BranchNameOnchange()")
            Me.cmbLoanCategory.Attributes.Add("onchange", "return CategoryOnchange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window.onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim Category_Id As String = Data(1)
            DT = DB.ExecuteDataSet("SELECT '-1' AS PRODUCT_ID,'-----SELECT-----' AS PRODUCT_NAME UNION ALL SELECT PRODUCT_ID,PRODUCT_NAME FROM PDD_PRODUCT_MASTER WHERE STATUS = 1 AND CATEGORY_ID = " + Category_Id + " ORDER BY PRODUCT_ID").Tables(0)
            Dim ComboDataProduct As String = ""
            For Each DR As DataRow In DT.Rows
                ComboDataProduct += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            DT = DB.ExecuteDataSet("SELECT '-1' AS PDD_ID,'-----SELECT-----' AS PDD_NAME UNION ALL SELECT '1000' AS PDD_ID,'Other' AS PDD_NAME UNION ALL SELECT PDD_ID,PDD_NAME FROM PDD_MASTER WHERE STATUS = 1 AND CATEGORY_ID = " + Category_Id + " ORDER BY PDD_ID").Tables(0)
            Dim ComboDataPdd As String = ""
            For Each DR As DataRow In DT.Rows
                ComboDataPdd += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            CallBackReturn += ComboDataProduct + "Ø" + ComboDataPdd
        ElseIf CInt(Data(0)) = 2 Then
            Dim WI_No As String = Data(1)
            Dim Branch_Code As Integer = CInt(Data(2))
            Dim Customer_Name As String = Data(3)
            Dim Loan_Number As String = Data(4)
            Dim Category As Integer = CInt(Data(5))
            Dim Product As Integer = CInt(Data(6))
            Dim Disbursal_Date As String = Data(7)
            Dim PDD_Item_Count As Integer = CInt(Data(9))
            Dim dateStat As Integer = CInt(Data(10))
            Dim Created_By As Integer = CInt(Session("UserID"))
            Dim PDD_Data As String = Data(8)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(12) As SqlParameter
                Params(0) = New SqlParameter("@WI_No", SqlDbType.VarChar, 500)
                Params(0).Value = WI_No
                Params(1) = New SqlParameter("@Branch_Code", SqlDbType.Int)
                Params(1).Value = Branch_Code
                Params(2) = New SqlParameter("@Customer_Name", SqlDbType.VarChar, 500)
                Params(2).Value = Customer_Name
                Params(3) = New SqlParameter("@Loan_Number", SqlDbType.VarChar, 50)
                Params(3).Value = Loan_Number
                Params(4) = New SqlParameter("@Category", SqlDbType.Int)
                Params(4).Value = Category
                Params(5) = New SqlParameter("@Product", SqlDbType.Int)
                Params(5).Value = Product
                Params(6) = New SqlParameter("@Disbursal_Date", SqlDbType.VarChar, 50)
                Params(6).Value = CDate(Disbursal_Date).ToString("yyyy-MM-dd")
                Params(7) = New SqlParameter("@PDD_Item_Count", SqlDbType.Int)
                Params(7).Value = PDD_Item_Count
                Params(8) = New SqlParameter("@Created_By", SqlDbType.Int)
                Params(8).Value = Created_By
                Params(9) = New SqlParameter("@PDD_Data", SqlDbType.VarChar, 1000)
                Params(9).Value = PDD_Data
                Params(10) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(10).Direction = ParameterDirection.Output
                Params(11) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(11).Direction = ParameterDirection.Output
                Params(12) = New SqlParameter("@datestat", SqlDbType.Int)
                Params(12).Value = dateStat
                DB.ExecuteNonQuery("SP_PDD_ENTRY_SAVE", Params)
                ErrorFlag = CInt(Params(10).Value)
                Message = CStr(Params(11).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 3 Then
            Dim WI_No As String = Data(1)
            DT = DB.ExecuteDataSet("SELECT BRANCH_CODE,BRANCH_NAME,CUSTOMER_NAME,LOAN_NUMBER,CATEGORY,PRODUCT,convert(varchar(10),DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER WHERE WI_NO = '" + WI_No + "'").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn += DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString()
                Dim Category_Id As String = DT.Rows(0)(4).ToString()
                DT1 = DB.ExecuteDataSet("SELECT '-1' AS PRODUCT_ID,'-----SELECT-----' AS PRODUCT_NAME UNION ALL SELECT PRODUCT_ID,PRODUCT_NAME FROM PDD_PRODUCT_MASTER WHERE STATUS = 1 AND CATEGORY_ID = " + Category_Id + " ORDER BY PRODUCT_ID").Tables(0)
                Dim ComboDataProduct As String = ""
                For Each DR As DataRow In DT1.Rows
                    ComboDataProduct += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø" + ComboDataProduct
                DT2 = DB.ExecuteDataSet("SELECT A.PDD_ITEM,B.PDD_NAME,A.PDD_OTHER,case when A.TO_BE_UPDATED = 0 then convert(varchar(10),A.DUE_DATE,103) else 'To Be Updated' end as due_date, case when A.status = 1 then 'Not Attended' when a.status = 2 then 'SO Approved' when a.status = 3 then 'SO Returned' when a.status = 5 then 'Approved' when a.status = 6 then 'Approver Returned' end as status FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON B.PDD_ID = A.PDD_ITEM WHERE WI_NO = '" + WI_No + "'").Tables(0)
                Dim pdd_Data As String = ""
                For Each DR As DataRow In DT2.Rows
                    pdd_Data += DR(0).ToString() + "µ" + DR(1).ToString() + "µ" + DR(2).ToString() + "µ" + DR(3).ToString() + "µ" + DR(4).ToString() + "¥"
                Next
                CallBackReturn += "Ø" + pdd_Data
                DT3 = DB.ExecuteDataSet("SELECT '-1' AS PDD_ID,'-----SELECT-----' AS PDD_NAME UNION ALL SELECT '1000' AS PDD_ID,'Other' AS PDD_NAME UNION ALL SELECT PDD_ID,PDD_NAME FROM PDD_MASTER WHERE STATUS = 1 AND CATEGORY_ID = " + Category_Id + " ORDER BY PDD_ID").Tables(0)
                Dim ComboDataPdd As String = ""
                For Each DR As DataRow In DT3.Rows
                    ComboDataPdd += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                Next
                CallBackReturn += "Ø" + ComboDataPdd
            End If
        ElseIf CInt(Data(0)) = 4 Then
            DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1").Tables(0)
            Dim i As Integer = 0
            For Each dtr In DT.Rows
                Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "£"
                DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR(10),A.DUE_DATE,103) ELSE 'To be updated' END AS DUE_DATE, A.SUB_NO, CONVERT(VARCHAR(10),A.SO_ENTRY_ON,103), C.EMP_NAME, A.SO_REMARKS FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID INNER JOIN EMP_MASTER C ON C.EMP_CODE = A.SO_ENTRY_BY WHERE (isnull(A.SO_ENTRY_STATUS,0) = 2) AND A.STATUS <> 4 AND (A.DISB_STATUS <> 2 OR A.SO_Entry_Status NOT IN (1,3))  AND A.WI_NO = " + DT.Rows(i)(0).ToString()).Tables(0)
                Dim j As Integer = 0
                For Each dtr1 In DT1.Rows
                    Dim dueDate As String = ""
                    If DT1.Rows(j)(1).ToString() = "" Then
                        dueDate = "To Be Updated"
                    Else
                        dueDate = DT1.Rows(j)(1).ToString()
                    End If
                    Me.hidQueueData.Value += DT1.Rows(j)(0).ToString() + "€" + dueDate + "€" + DT1.Rows(j)(2).ToString() + "€" + DT1.Rows(j)(3).ToString() + "€" + DT1.Rows(j)(4).ToString() + "€" + DT1.Rows(j)(5).ToString() + "¢"
                    j += 1
                Next
                Me.hidQueueData.Value += "Ñ"
                i += 1
            Next
            Me.hidQueueData.Value += "Ø1"
            CallBackReturn += Me.hidQueueData.Value
        ElseIf CInt(Data(0)) = 5 Then
            Dim SubId As String = Data(1)
            Dim WiNo As String = Data(2)
            DT = DB.ExecuteDataSet("SELECT A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, D.CATEGORY_NAME, E.PRODUCT_NAME, FORMAT(A.DISBURSAL_DATE,'dd MMM yyyy') AS [DISB_DATE], CASE WHEN B.PDD_ITEM = 1000 THEN B.PDD_OTHER ELSE C.PDD_NAME END AS PDD, CONVERT(VARCHAR(10),B.DUE_DATE) AS [DUE_DATE], CONVERT(VARCHAR(10),B.SO_ENTRY_ON,103), F.EMP_NAME, B.SO_REMARKS, A.CATEGORY, A.PRODUCT, B.PDD_ITEM FROM PDD_ENTRY_MASTER A INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO LEFT JOIN PDD_MASTER C ON C.PDD_ID = B.PDD_ITEM INNER JOIN PDD_CATEGORY_MASTER D ON A.CATEGORY = D.CATEGORY_ID INNER JOIN PDD_PRODUCT_MASTER E ON E.PRODUCT_ID = A.PRODUCT LEFT JOIN EMP_MASTER F ON F.EMP_CODE = B.SO_ENTRY_BY WHERE A.WI_NO = '" + WiNo + "' AND B.SUB_NO = " + SubId).Tables(0)

            CallBackReturn += DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString() + "Ø" + DT.Rows(0)(4).ToString() + "Ø" + DT.Rows(0)(5).ToString() + "Ø" + DT.Rows(0)(6).ToString() + "Ø" + DT.Rows(0)(7).ToString() + "Ø" + DT.Rows(0)(8).ToString() + "Ø" + DT.Rows(0)(9).ToString() + "Ø" + DT.Rows(0)(10).ToString() + "Ø" + DT.Rows(0)(11).ToString() + "Ø" + WiNo.ToString() + "Ø" + DT.Rows(0)(12).ToString() + "Ø" + DT.Rows(0)(13).ToString() + "Ø" + DT.Rows(0)(14).ToString()
            DT1 = DB.ExecuteDataSet("SELECT '-1' AS PRODUCT_ID,'-----SELECT-----' AS PRODUCT_NAME UNION ALL SELECT PRODUCT_ID,PRODUCT_NAME FROM PDD_PRODUCT_MASTER WHERE STATUS = 1 AND CATEGORY_ID = " + DT.Rows(0)(12).ToString() + " ORDER BY PRODUCT_ID").Tables(0)
            Dim ComboDataProduct As String = ""
            For Each DR As DataRow In DT1.Rows
                ComboDataProduct += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            CallBackReturn += "Ø" + ComboDataProduct
            DT3 = DB.ExecuteDataSet("SELECT '-1' AS PDD_ID,'-----SELECT-----' AS PDD_NAME UNION ALL SELECT '1000' AS PDD_ID,'Other' AS PDD_NAME UNION ALL SELECT PDD_ID,PDD_NAME FROM PDD_MASTER WHERE STATUS = 1 AND CATEGORY_ID = " + DT.Rows(0)(12).ToString() + " ORDER BY PDD_ID").Tables(0)
            Dim ComboDataPdd As String = ""
            For Each DR As DataRow In DT3.Rows
                ComboDataPdd += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
            CallBackReturn += "Ø" + ComboDataPdd
        ElseIf CInt(Data(0)) = 6 Then
            DT = DB.ExecuteDataSet("SELECT A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, B.CATEGORY_NAME, C.PRODUCT_NAME, CONVERT(VARCHAR(10),A.DISBURSAL_DATE,103) FROM PDD_ENTRY_MASTER A INNER JOIN PDD_CATEGORY_MASTER B ON B.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER C ON C.PRODUCT_ID = A.PRODUCT WHERE A.STATUS = 1").Tables(0)
            Dim i As Integer = 0
            For Each dtr In DT.Rows
                Me.hidQueueData.Value += DT.Rows(i)(0).ToString() + "ÿ" + DT.Rows(i)(1).ToString() + "ÿ" + DT.Rows(i)(2).ToString() + "ÿ" + DT.Rows(i)(3).ToString() + "ÿ" + DT.Rows(i)(4).ToString() + "ÿ" + DT.Rows(i)(5).ToString() + "ÿ" + DT.Rows(i)(6).ToString() + "ÿ" + DT.Rows(i)(7).ToString() + "£"
                DT1 = DB.ExecuteDataSet("SELECT CASE WHEN A.PDD_ITEM <> 1000 THEN B.PDD_NAME ELSE A.PDD_OTHER END AS PDD_ITEM, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR(10),A.DUE_DATE,103) ELSE 'To be updated' END AS DUE_DATE, A.SUB_NO, CONVERT(VARCHAR(10),A.SO_ENTRY_ON,103), C.EMP_NAME, A.SO_REMARKS FROM PDD_ENTRY_SUB A LEFT JOIN PDD_MASTER B ON A.PDD_ITEM = B.PDD_ID LEFT JOIN EMP_MASTER C ON C.EMP_CODE = A.SO_ENTRY_BY WHERE A.TO_BE_UPDATED = 1 and A.DUE_DATE is null  AND A.WI_NO = '" + DT.Rows(i)(0).ToString() + "'").Tables(0)
                Dim j As Integer = 0
                For Each dtr1 In DT1.Rows
                    Dim dueDate As String = ""
                    If DT1.Rows(j)(1).ToString() = "" Then
                        dueDate = "To Be Updated"
                    Else
                        dueDate = DT1.Rows(j)(1).ToString()
                    End If
                    Me.hidQueueData.Value += DT1.Rows(j)(0).ToString() + "€" + dueDate + "€" + DT1.Rows(j)(2).ToString() + "€" + DT1.Rows(j)(3).ToString() + "€" + DT1.Rows(j)(4).ToString() + "€" + DT1.Rows(j)(5).ToString() + "¢"
                    j += 1
                Next
                Me.hidQueueData.Value += "Ñ"
                i += 1
            Next
            Me.hidQueueData.Value += "Ø1"
            CallBackReturn += Me.hidQueueData.Value
        End If
    End Sub
#End Region
End Class
