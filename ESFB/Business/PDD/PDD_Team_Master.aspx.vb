﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class PDD_Team_Master
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DTTS As New DataTable
    Dim DT_EMP As New DataTable
    Dim DTTRA As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim EN As New Enrollment
    Dim CallBackReturn As String = Nothing
    Dim SM As New SMTP
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1433) = False Then 'uat
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return

            'End If
            If GF.FormAccess(CInt(Session("UserID")), 1437) = False Then 'main
                Response.Redirect("~/AccessDenied.aspx", False)
                Return

            End If

            Me.Master.subtitle = "PDD Team Master"
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            Dim EmpCode = CInt(Session("UserID"))
            DT = DB.ExecuteDataSet("SELECT B.TEAM_ID, A.TEAM_NAME FROM PDD_TEAM_MASTER A INNER JOIN PDD_TEAM_DTL B ON B.TEAM_ID = A.TEAM_ID WHERE A.STATUS_ID = 1 AND B.EMP_CODE = " + EmpCode.ToString()).Tables(0)
            Me.hidTeam.Value = DT.Rows(0)(0).ToString()
            
            Dim creditAdmin As Integer = 0
            For n As Integer = 0 To DT.Rows.Count - 1
                If CInt(DT.Rows(n)(0).ToString) = 0 Then
                    Me.cmbTeam.Enabled = True
                End If
                If CInt(DT.Rows(n)(0).ToString) = 4 Then
                    creditAdmin = 1
                    Me.cmbTeam.Enabled = True
                End If
            Next
            If creditAdmin = 1 Then
                DT1 = DB.ExecuteDataSet("SELECT '-1' AS TEAM_ID,'-----SELECT-----' AS TEAM_NAME UNION ALL SELECT TEAM_ID,TEAM_NAME FROM PDD_TEAM_MASTER WHERE STATUS_ID = 1 AND TEAM_ID IN (1,3) ORDER BY TEAM_ID").Tables(0)
                GF.ComboFill(cmbTeam, DT1, 0, 1)
                Me.cmbTeam.SelectedValue = CStr(1)
            Else
                DT1 = DB.ExecuteDataSet("SELECT '-1' AS TEAM_ID,'-----SELECT-----' AS TEAM_NAME UNION ALL SELECT TEAM_ID,TEAM_NAME FROM PDD_TEAM_MASTER WHERE STATUS_ID = 1 ORDER BY TEAM_ID").Tables(0)
                GF.ComboFill(cmbTeam, DT1, 0, 1)
                Me.cmbTeam.SelectedValue = DT.Rows(0)(0).ToString()
            End If
            'Me.txtTeam.Text = DT.Rows(0)(1).ToString()
            Me.hidTeamName.Value = DT.Rows(0)(1).ToString()
            If DT.Rows(0)(0).ToString() = "4" Then
                DT1 = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY B.EMP_NAME) AS Row,upper(B.EMP_NAME)+' ('+CONVERT(VARCHAR,A.EMP_CODE)+') ',A.EMP_CODE FROM PDD_TEAM_DTL A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE WHERE A.TEAM_ID = 1 ORDER BY B.EMP_NAME").Tables(0)
            ElseIf DT.Rows(0)(0).ToString() = "2" Then
                DT1 = DB.ExecuteDataSet("select DISTINCT ROW_NUMBER() OVER (ORDER BY B.EMP_NAME) AS Row,upper(B.EMP_NAME)+' ('+CONVERT(VARCHAR,A.EMP_CODE)+') '+' ('+D.BRANCH_NAME+') ',A.EMP_CODE FROM PDD_TEAM_DTL A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE INNER JOIN PDD_SO_MAPPING C ON C.EMP_CODE = A.EMP_CODE INNER JOIN BRANCH_MASTER D ON D.BRANCH_ID = C.BRANCH_ID WHERE A.TEAM_ID = " + DT.Rows(0)(0).ToString() + " GROUP BY A.EMP_CODE,B.EMP_NAME,D.BRANCH_NAME ORDER BY B.EMP_NAME").Tables(0)
            Else
                DT1 = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY B.EMP_NAME) AS Row,upper(B.EMP_NAME)+' ('+CONVERT(VARCHAR,A.EMP_CODE)+') ',A.EMP_CODE FROM PDD_TEAM_DTL A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE WHERE A.TEAM_ID = " + DT.Rows(0)(0).ToString() + " ORDER BY B.EMP_NAME").Tables(0)
            End If

            If DT1.Rows.Count > 0 Then
                For i As Integer = 0 To DT1.Rows.Count - 1
                    Me.hidRowData.Value += DT1.Rows(i)(0).ToString() + "µ" + DT1.Rows(i)(1).ToString() + "µ" + DT1.Rows(i)(2).ToString() + "¥"
                Next
            End If
            Me.cmbTeam.Attributes.Add("onchange", "return TeamOnChange()")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()
        DT_EMP.Dispose()
        DTTS.Dispose()
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        If CInt(Data(0)) = 1 Then
            Dim empCode As String = Data(1)
            DT = DB.ExecuteDataSet("SELECT A.EMP_NAME, B.BRANCH_NAME, C.DEPARTMENT_NAME, D.DESIGNATION_NAME FROM EMP_MASTER A INNER JOIN BRANCH_MASTER B ON A.BRANCH_ID = B.BRANCH_ID INNER JOIN DEPARTMENT_MASTER C ON C.DEPARTMENT_ID = A.DEPARTMENT_ID INNER JOIN DESIGNATION_MASTER D ON D.DESIGNATION_ID = A.DESIGNATION_ID WHERE EMP_CODE =" + empCode).Tables(0)
            CallBackReturn += DT.Rows(0)(0).ToString() + "Ø" + DT.Rows(0)(1).ToString() + "Ø" + DT.Rows(0)(2).ToString() + "Ø" + DT.Rows(0)(3).ToString()
        ElseIf CInt(Data(0)) = 2 Then
            Dim EmpCode As Integer = CInt(Data(1))
            Dim TeamID As Integer = CInt(Data(2))
            Dim AddOrDel As Integer = CInt(Data(3))
            Dim assignedBranches As String = Data(4)
            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter
                Params(0) = New SqlParameter("@Emp_Code", SqlDbType.Int)
                Params(0).Value = EmpCode
                Params(1) = New SqlParameter("@Team_ID", SqlDbType.Int)
                Params(1).Value = TeamID
                Params(2) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(2).Direction = ParameterDirection.Output
                Params(3) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 5000)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@AddOrDel", SqlDbType.Int)
                Params(4).Value = AddOrDel
                Params(5) = New SqlParameter("@AssignedBranches", SqlDbType.VarChar, 1000)
                Params(5).Value = assignedBranches
                DB.ExecuteNonQuery("SP_PDD_TEAM_MASTER", Params)
                ErrorFlag = CInt(Params(2).Value)
                Message = CStr(Params(3).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 3 Then
            Dim TeamID As String = Data(1)
            Dim RowData As String = ""
            Dim Branch As String = ""

            If TeamID = "2" Then
                DT1 = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY B.EMP_NAME) AS Row,upper(B.EMP_NAME)+' ('+CONVERT(VARCHAR,A.EMP_CODE)+') '+' ('+D.BRANCH_NAME+') ',A.EMP_CODE FROM PDD_TEAM_DTL A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE INNER JOIN PDD_SO_MAPPING C ON C.EMP_CODE = A.EMP_CODE INNER JOIN BRANCH_MASTER D ON D.BRANCH_ID = C.BRANCH_ID WHERE A.TEAM_ID = " + TeamID + " GROUP BY A.EMP_CODE,B.EMP_NAME,D.BRANCH_NAME ORDER BY B.EMP_NAME").Tables(0)
            Else
                DT1 = DB.ExecuteDataSet("select ROW_NUMBER() OVER (ORDER BY B.EMP_NAME) AS Row,upper(B.EMP_NAME)+' ('+CONVERT(VARCHAR,A.EMP_CODE)+') ',A.EMP_CODE FROM PDD_TEAM_DTL A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE WHERE A.TEAM_ID = " + TeamID + " ORDER BY B.EMP_NAME").Tables(0)
            End If
            If DT1.Rows.Count > 0 Then
                For i As Integer = 0 To DT1.Rows.Count - 1
                    RowData += DT1.Rows(i)(0).ToString() + "µ" + DT1.Rows(i)(1).ToString() + "µ" + DT1.Rows(i)(2).ToString() + "¥"
                Next
            End If
            DT = DB.ExecuteDataSet("SELECT A.TEAM_ID, A.TEAM_NAME FROM PDD_TEAM_MASTER A WHERE A.STATUS_ID = 1 AND A.TEAM_ID = " + TeamID.ToString()).Tables(0)
            CallBackReturn += RowData + "Ø" + DT.Rows(0)(1).ToString()
            If CInt(TeamID) = 2 Then
                DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----SELECT-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1 ORDER BY BRANCH_NAME").Tables(0)
                If DT.Rows.Count > 0 Then
                    For i As Integer = 0 To DT.Rows.Count - 1
                        Branch += DT.Rows(i)(0).ToString() + "µ" + DT.Rows(i)(1).ToString() + "¥"
                    Next
                End If
            End If
            CallBackReturn += "Ø" + TeamID + "Ø" + Branch
            End If
    End Sub
#End Region
End Class
