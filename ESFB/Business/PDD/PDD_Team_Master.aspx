﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="PDD_Team_Master.aspx.vb" Inherits="PDD_Team_Master" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">
 <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">   </asp:ToolkitScriptManager>
<style type="text/css">
        .tblQal
        {
            border:9px;background-color:#A34747; height:25px;font-size:10;color:#FFF;
        }
       .tblQalBody
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
    
   
        .style1
        {
            width: 40%;
            height: 104px;
        }
        </style>
 <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style3
     {
         width: 25%;
         height: 21px;
     }
        .style4
     {
         width: 12%;
         height: 25px;
     }
     .style5
     {
         width: 13%;
         height: 25px;
     }
        </style>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <title></title>
    <link href="../../Style/Style.css" type="text/css" rel="Stylesheet"/>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script src="../../Script/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var appLength = 1;
        window.onload = function () {
            document.getElementById("branchAssign").style.display = 'none';
        }
        function ComboFill(data, ddlName) {
            document.getElementById(ddlName).options.length = 0;
            var rows = data.split("Ñ");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("ÿ");
                var option1 = document.createElement("OPTION");
                option1.value = cols[0];
                option1.text = cols[1];
                document.getElementById(ddlName).add(option1);
            }
        }
        function ClearCombo(control) {
            document.getElementById(control).options.length = 0;
            var option1 = document.createElement("OPTION");
            option1.value = -1;
            option1.text = " -----Select-----";
            document.getElementById(control).add(option1);
        }
        function EmpCodeOnchange() {
            var empCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            var ToData = "1Ø" + empCode.toString();
            ToServer(ToData, 1);
        }
        function RemoveMember(id){
            var team_id = document.getElementById("<%= hidTeam.ClientID %>").value;
            var ToData = "2Ø" + id.toString() + "Ø" + team_id.toString() + "Ø0Ø0";
            ToServer(ToData, 2);
        }
        function FromServer(arg, context) {
            if (context == 1) {
                var Data = arg.split("Ø");
                document.getElementById("<%= txtEmpName.ClientID %>").value = Data[0];
                document.getElementById("<%= txtBranch.ClientID %>").value = Data[1];
                document.getElementById("<%= txtDept.ClientID %>").value = Data[2];
                document.getElementById("<%= txtDes.ClientID %>").value = Data[3];
            } else if (context == 2) {
                var Data = arg.split("Ø");
                alert(Data[1]);
                if (Data[0] == 0) {
                    var team_id = document.getElementById("<%= hidTeam.ClientID %>").value;
                    var ToData = "3Ø" + team_id.toString();
                    ToServer(ToData, 3);
                }
            } else if (context == 3) {
                document.getElementById("branchAssign").style.display = '';
                document.getElementById("branchTab").innerHTML = '';
                var Data = arg.split("Ø");
                document.getElementById("<%= hidRowData.ClientID %>").value = Data[0];
                document.getElementById("<%= hidTeamName.ClientID %>").value = Data[1];
                document.getElementById("<%= txtEmpCode.ClientID %>").value = '';
                document.getElementById("<%= txtEmpName.ClientID %>").value = ''; 
                document.getElementById("<%= txtBranch.ClientID %>").value = '';
                document.getElementById("<%= txtDept.ClientID %>").value = '';
                document.getElementById("<%= txtDes.ClientID %>").value = '';
                var teamID = Data[2];
                var branchCombo = Data[3];
                document.getElementById("<%= hdnBranchCombo.ClientID %>").value = Data[3];
                if(teamID == 2){
                    var tab = "";
                    tab += "<tr id='metrics_1' style='height: 20px !important;'>";
                    tab += "<td colspan='4'>";
                    var select = "<select id='cmbBranch_1' class='NormalText' name='cmbBranch_1' style='width:80%' onchange = 'return SelectOption(1)'>";
                    var rows = branchCombo.split("¥");
                    for (a = 1; a < rows.length; a++) {
                        var cols = rows[a].split("µ");
                        select += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
                    }
                    select += "</select>&nbsp;&nbsp;";
                    var add = "&nbsp;&nbsp;<img src='../../Image/Addd1.gif' id='add_1' style='height:18px; width:18px;  z-index:1; cursor: pointer;  padding-right:10px;position:fixed;' onclick='AddNewBranch(2)' title='Add New' />";
                    tab += select  + add + "</td>";
                    tab += "</tr>";
                    document.getElementById("branchTab").innerHTML += tab;
                    document.getElementById("branchTab").style.display = '';
                } else {
                    document.getElementById("branchAssign").style.display = 'none';
                    document.getElementById("branchTab").style.display = 'none';
                }
                table_fill();
            }
        }
        
        function AddNewBranch(i) {
            appLength = i;
            var tab = "";
            tab += "<tr id='metrics_"+i+"' style='height: 20px !important;'>";
            tab += "<td colspan='4'>";
            var select = "<select id='cmbBranch_" + i + "' class='NormalText' name='cmbBranch_" + i + "' style='width:80%' onchange = 'return SelectOption(" + i + ")' >";
            var rows = document.getElementById("<%= hdnBranchCombo.ClientID %>").value.split("¥");
            for (a = 1; a < rows.length; a++) {
                var cols = rows[a].split("µ");
                select += "<option value='"+ cols[0] +"' >"+ cols[1] +"</option>";               
            }
            select += "</select>&nbsp;&nbsp;";
            var add = "&nbsp;&nbsp;<img src='../../Image/Addd1.gif' id='add_"+i+"' style='height:18px; width:18px;  z-index:1; cursor: pointer;  padding-right:10px;position:fixed;' onclick='AddNewBranch("+(i+1)+")' title='Add New' />";
            tab += select+add+"</td>";
            tab += "</tr>";
            document.getElementById("branchTab").innerHTML += tab;
            for(k=1;k<i;k++){
                document.getElementById('add_'+k).style.display = 'none';
            }
        }

        function SelectOption(i) {
                var appr = document.getElementById("cmbBranch_" + i).value;
                $('#cmbBranch_' + i + ' option[value=' + appr + ']').attr('selected', 'selected');
        }
        function btnConfirm_onclick() {
            var empCode = document.getElementById("<%= txtEmpCode.ClientID %>").value;
            if (empCode == "") {
                alert("Enter Employee Code");
                return false;
            }
            var teamId = document.getElementById("<%= hidTeam.ClientID %>").value;
            var assignedBranches = '';
            if(teamId == 2){
                for (var i = 1; i <= appLength; i++) {
                    assignedBranches += empCode + "µ" + document.getElementById('cmbBranch_' + i).value + "¥";
                }
            }
            var ToData = "2Ø" + empCode + "Ø" + teamId + "Ø1Ø" + assignedBranches;
            ToServer(ToData, 2);
        }
        function btnExit_onclick() {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }   
        function table_fill() {      
            document.getElementById("TeamHeading").innerHTML = document.getElementById("<%= hidTeamName.ClientID %>").value;
            document.getElementById("<%= pnlTeamMem.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            tab += "<br /><div class=mainhead style='width:60%; height:25px; padding-top:0px;margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;font-family:'cambria';' align='center'>";
            tab += "<tr>";
            var rowData = document.getElementById("<%= hidRowData.ClientID %>").value;
            tab += "<td style='width:15%;text-align:center;' class=NormalText>#</td>";
            tab += "<td style='width:75%;text-align:left' class=NormalText>Team Member</td>";
            tab += "<td style='width:10%;'/></td>";
            tab += "</tr></table></div><div style='width:60%; height:128px; overflow:auto; margin:0px auto;background-color:#ADC2D6;' ><table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            if(rowData != ""){
                row = rowData.split("¥");
                for (n = 0; n < row.length - 1; n++) {
                    col = row[n].split("µ");
                    if (row_bg == 0) {
                        row_bg = 1;
                        tab += "<tr class=sub_first>";
                    }else {
                        row_bg = 0;
                        tab += "<tr class=sub_second>";
                    }
                    tab += "<td style='width:15%;text-align:center;' >" + col[0] + "</td>";
                    tab += "<td style='width:75%;text-align:left;' >" + col[1] + "</td>";
                    tab += "<td style='width:10%;text-align:center' onclick='RemoveMember(" + col[2] + ")'><img  src='../../Image/cross.png' style='align:middle;cursor:pointer;' /></td>";
                    tab += "</tr>";
                }
            }       
            tab += "</table></div>";
            document.getElementById("<%= pnlTeamMem.ClientID %>").innerHTML = tab;
        }
        function TeamOnChange(){
            document.getElementById("<%= hidTeam.ClientID %>").value = document.getElementById("<%= cmbTeam.ClientID %>").value;
            var team_id = document.getElementById("<%= cmbTeam.ClientID %>").value;
            var ToData = "3Ø" + team_id;
            ToServer(ToData, 3);
        }
    </script>   
</head>
</html>
<br />
<div  style="width:60%;margin:0px auto; background-color:#EEB8A6;">
    <br />
    <div id = "divSection1" class = "sec1" style="width:97%;background-color:white;margin:0px auto; height:auto; border-radius:25px;">
        <br />
        <table style="width:90%;height:90px;margin:0px auto;">
            <tr> 
                <td style="width:25%;">
                    <asp:HiddenField ID="hidTeam" runat="server" />
                    <asp:HiddenField ID="hidTeamName" runat="server" />
                    <asp:HiddenField ID="hidRowData" runat="server" />
                    <asp:HiddenField ID="hdnBranchCombo" runat="server" />
                </td>
                <td style="width:25%; text-align:left;"></td>
                <td style="width:25%"></td>
                <td style="width:25%"></td>
            </tr>
            <tr>
                <td style="width:25%;"></td>
                <td style="width:25%;font-weight:bold;font-size:17px;text-align:center;text-decoration:underline;" colspan='2' id="TeamHeading"></td>
                <td style="width:25%;"></td>
            </tr>
            <tr>
                <td><br /></td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Team
                </td>
                <td style="width:25%; text-align:left;">
                    <%--<asp:TextBox ID="txtTeam" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>--%>
                    <asp:DropDownList ID="cmbTeam" class="NormalText" runat="server" Enabled="false" Font-Names="Cambria" Width="61%" ForeColor="Black">
                        <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Employee Code 
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtEmpCode" class="NormalText" runat="server" Width="99.3%" onkeypress="NumericCheck(event)" onchange="return EmpCodeOnchange()"></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Employee Name 
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtEmpName" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Branch 
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtBranch" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Department
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtDept" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr>    
                <td style="width:25%;"></td>
                <td style="width:25%;">
                   Designation
                </td>
                <td style="width:25%; text-align:left;">
                    <asp:TextBox ID="txtDes" class="ReadOnlyTextBox" ReadOnly="true" runat="server" Width="99.3%" ></asp:TextBox>
                </td>
            </tr>
            <tr id="branchAssign">
                <td style="width:25%"></td>
                <td style="width:25%">
                    Assigned Branches
                </td>
                <td style="width:25%;text-align:left" >                    
                    <table id="branchTab" style="width:90%;height:auto;margin:0px auto;display:none;">
                    </table>
                </td>
            </tr>
            <tr id="Panel">                                     
                <td style="width:30%;" colspan="4">
                    <asp:Panel ID="pnlTeamMem" style="width:100%; text-align:right;float:right;" runat="server">
                    </asp:Panel>
                </td>
            </tr>     
            <tr>
                <td style="width:25%;"></td>
                <td style="text-align:center;" colspan="2"><br />
                    <input id="btnConfirm" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="SAVE" onclick="return btnConfirm_onclick()"/>
                    &nbsp;
                    <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button" value="EXIT" onclick="return btnExit_onclick()" />
                </td>
            </tr>
        </table>
    </div>
    <br />
</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</asp:Content>

