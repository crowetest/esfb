﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Credit_Officer_Response.aspx.vb" Inherits="Credit_Officer_Response"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
 <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
    <script src="../Script/Validations.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../Style/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="../../Style/bootstrap-multiselect.css" type="text/css" />
		<script type="text/javascript" src="../../Script/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-2.3.2.min.js"></script>
		<script type="text/javascript" src="../../Script/bootstrap-multiselect_EMP.js"></script>
    <style>
        .tblQal
        {
            border:9px;background-color:#FBEFFB; height:25px;font-size:10;color:#476C91;
        }
       .tblQalBody
        {
            border:9px;background-color:#FFF; height:25px;font-size:10;color:#476C91;
        }
    </style>
     
   
    <script language="javascript" type="text/javascript">
    
      
   
     function cursorwait(e) {
            document.body.className = 'wait';}
     function cursordefault(e) {
            document.body.className = 'default';}
      
    function btnExit_onclick() 
    {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }

    function btnSave_onclick() {
    UpdateValue();
    
   
      var strempcode = document.getElementById("<%= hdnResponse.ClientID %>").value;
     var ToData = "1Ø" + strempcode;
      
     ToServer(ToData, 1);
     
         
               

    }

     function UpdateValue() 
     {

            document.getElementById("<%= hdnResponse.ClientID %>").value = "";
            
            row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
       
            for (n = 0; n <= row.length - 1; n++) 
            {
                    col = row[n].split("µ");
           
                  if ((document.getElementById("cmbstatus" + n).value != -1) && (document.getElementById("txtRemarks" + n).value == ""))
                   {
                   
                        alert("Enter Remarks");
                        document.getElementById("txtRemarks" + n).focus();
                        return false;
                    }
                     if((col[16]!=document.getElementById("cmbstatus" + n).value)||(col[17]!=document.getElementById("txtRemarks" + n).value))
                      {
                      
                            document.getElementById("<%= hdnResponse.ClientID %>").value += "¥" + col[0] + "µ" + document.getElementById("cmbstatus" + n).value + "µ" + document.getElementById("txtRemarks" + n).value;
                      } 
            }

                 
    }
     
          
    function FromServer(Arg, Context) 
    {

    
          switch (Context)
           {
           
               case 1:
               {
                    var Data = Arg.split("Ø");
                    cursordefault();
                    alert(Data[1]);
                    document.getElementById("btnSave").disabled=false;
                     if (Data[0] == 0) 
                    window.open('Credit_Officer_Response.aspx','_self');
                    break;
                   
                   
     
               }       
              
             }
          } 
       

      function table_fill() 
       {
     
         if (document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value != "") 
         {
            document.getElementById("<%= pnLoanEnquiry.ClientID %>").style.display = '';
            var row_bg = 0;
            var tab = "";
            
            tab += "<div style='width:100%; height:auto; overflow:auto;margin: 0px auto;' class='mainhead'>";
            tab += "<table style='width:100%;margin:0px auto;font-family:'cambria';' align='center'>";
            tab += "<tr height=20px;  class='tblQal'>";      
            tab += "<td style='width:3%;text-align:center'>#</td>";    
            tab += "<td style='width:5%;text-align:center'><b>Name</b></td>"; 
            tab += "<td style='width:6%;text-align:center'><b>Place Name</b></td>";   
            tab += "<td style='width:6%;text-align:center'><b>Mobile No</b></td>";
            tab += "<td style='width:6%;text-align:center'><b>Alternative Phone Number </b></td>";    
            tab += "<td style='width:5%;text-align:center'><b>Loan Product</b></td>";  
            tab += "<td style='width:5%;text-align:center'><b>Loan Amount</b></td>";   
            tab += "<td style='width:8%;text-align:center'><b>Remarks </b></td>";    
            tab += "<td style='width:4%;text-align:center'><b>Sangam ID  </b></td>";
            tab += "<td style='width:6%;text-align:center'><b>Sangam Name </b></td>"; 
            tab += "<td style='width:5%;text-align:center'><b>Enquiry Date </b></td>";   
            tab += "<td style='width:4%;text-align:center'><b>Branch ID </b></td>"; 
            tab += "<td style='width:7%;text-align:center'><b>Branch Name </b></td>";   
            tab += "<td style='width:5%;text-align:center'><b>Entered by Emp_code </b></td>";   
            tab += "<td style='width:7%;text-align:center'><b>Entered by Emp_name </b></td>"; 
            tab += "<td style='width:5%;text-align:center'><b>Explanation </b></td>";     
            tab += "<td style='width:3%;text-align:center'><b>Status</b></td>";   
            tab += "<td style='width:10%;text-align:center'><b>Remarks</b></td>";         
           // tab +="<td style='width:1%;text-align:left'></td>";
            tab += "</tr>";     
          
            row = document.getElementById("<%= hdnLoanEnquiry.ClientID %>").value.split("¥");
          
             var Explain; 
            for (n = 0; n <= row.length - 1; n++) 
            {
                 col = row[n].split("µ"); 
             
               
                 if (row_bg == 0) 
                 {
                        row_bg = 1;
                        //tab += "<tr class='tblQalBody'; style='text-align:center; height:20px; padding-left:20px;'>";
                       tab += "<tr height=20px;  class='tblQal'>";  
                        
                  }
                  else 
                  {
                        row_bg = 0;
                        //tab += "<tr class='tblQalBody'; style='text-align:center; height:20px; padding-left:20px;'>";
                         tab += "<tr height=20px;  class='tblQal'>";  
                  }
                 i=n+1;
             
                    tab += "<td style='width:3%;text-align:center'>" + i + "</td>";  
                    tab += "<td style='width:5%;text-align:left' >" + col[1] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[2] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[3] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[4] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[5] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[6] + "</td>";
                    tab += "<td style='width:8%;text-align:left'>" + col[7] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[8] + "</td>";
                    tab += "<td style='width:6%;text-align:left'>" + col[9] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[10] + "</td>";
                    tab += "<td style='width:4%;text-align:left'>" + col[11] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[12] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[13] + "</td>";
                    tab += "<td style='width:7%;text-align:left'>" + col[14] + "</td>";
                    tab += "<td style='width:5%;text-align:left'>" + col[15] + "</td>";

                     var select = "<select id='cmbstatus" + n + "' class='NormalText' name='cmbstatus" + n + "' style='width:150px; height:25px;' >";
             
                    if(col[16]==-1)
                        select += "<option value='-1' selected=true>-----Select-----</option>";
                    else
                         select += "<option value='-1'>-----Select-----</option>";
                    if(col[16]==1)
                        select += "<option value='1' selected=true>Televerification Done</option>";
//                    else
//                        select += "<option value='1'>Televerification Done</option>";
                    if(col[16]==2)
                        select += "<option value='2' selected=true>Branch visit by Customer</option>";
//                    else
//                        select += "<option value='2'>Branch visit by Customer</option>";
                    if(col[16]==3)
                        select += "<option value='3' selected=true>Customer place visit done</option>";
//                    else
//                        select += "<option value='3'>Customer place visit done</option>";
                    if(col[16]==4)
                        select += "<option value='4' selected=true>Document pending from Customer</option>";
//                    else
//                        select += "<option value='4'>Document pending from Customer</option>";
                    if(col[16]==5)
                        select += "<option value='5' selected=true>Submitted for Sanction</option>";
//                    else
//                        select += "<option value='5'>Submitted for Sanction</option>";
                     if(col[16]==6)
                        select += "<option value='6' selected=true>Sanctioned</option>";
//                    else
//                        select += "<option value='6'>Sanctioned</option>";
                    if(col[16]==7)
                        select += "<option value='7' selected=true>Rejected by CO</option>";
//                    else
//                        select += "<option value='7'>Rejected by CO</option>";
                    if(col[16]==8)
                        select += "<option value='8' selected=true>Not interested</option>";
                    else
                        select += "<option value='8'>Not interested</option>";
                        if(col[16]==9)
                        select += "<option value='9' selected=true>Not contactable</option>";
                    else
                        select += "<option value='9'>Not contactable</option>";
                        if(col[16]==10)
                        select += "<option value='10' selected=true>Not Eligible</option>";
                    else
                        select += "<option value='10'>Not Eligible</option>";
                        if(col[16]==11)
                        select += "<option value='11' selected=true>In Process</option>";
                    else
                        select += "<option value='11'>In Process</option>";
                        if(col[16]==12)
                        select += "<option value='12' selected=true>Document Pending</option>";
                    else
                        select += "<option value='12'>Document Pending</option>";
                        if(col[16]==13)
                        select += "<option value='13' selected=true>Sanctioned</option>";
                    else
                        select += "<option value='13'>Sanctioned</option>";
                        if(col[16]==14)
                        select += "<option value='14' selected=true>Disbursed</option>";
                    else
                        select += "<option value='14'>Disbursed</option>";
                        if(col[16]==15)
                        select += "<option value='15' selected=true>Legal / Technical rejected</option>";
                    else
                        select += "<option value='15'>Legal / Technical rejected</option>";

                         if(col[16]==16)
                        select += "<option value='16' selected=true>Follow Up</option>";
                    else
                        select += "<option value='16'>Follow Up</option>";
                    tab += "<td style6='width:3%;text-align:center'>" + select + "</td>";
                   
                  
                   
                    
                     if(col[17]!="")
                    {

                        Remarks =col[17];
                        var txtRemarks ="<textarea class='NormalText' id='txtRemarks"+ n +"' name='txtRemarks" + n + "' onkeypress='return TextAreaCheck(event)'  style='width:100%;text-align:left;' rows=1;  >"+Remarks+"</textarea>";
                        tab += "<td style='width:10%;text-align:left' >"+ txtRemarks +"</td>";
                   
                     
                    }
                    else
                     {

                         var txtRemarks ="<textarea class='NormalText' id='txtRemarks"+ n +"' name='txtRemarks" + n + "'  onkeypress='return TextAreaCheck(event)' style='width:100%;text-align:left;' rows=1;  ></textarea>";
                        tab += "<td style='width:10%;text-align:left' >"+ txtRemarks +"</td>";
                   
                          
           
                       
               
                     }  

                    tab += "</tr>";
                }
                tab += "</table></div></div>";
                document.getElementById("<%= pnLoanEnquiry.ClientID %>").innerHTML = tab;
               
                }
                else
                document.getElementById("<%= pnLoanEnquiry.ClientID %>").style.display = 'none';
                //--------------------- Clearing Data ------------------------//
        }
       
       
         


    </script>
    <br />
    <div>
    <table align="center" style="width:100%; margin: 0px auto;">
              
            <tr>
                    <td style="width:100% ;text-align:center;" colspan="4" class="mainhead" >
                    <strong>Response</strong>
                    <img src="../Image/Addd1.gif" style="height:18px; width:18px; float:right; z-index:1; cursor: pointer;  padding-right:10px;" onclick="AddNewRow()" title="Add New"/>
                   
                    </td>
            </tr> 
            <tr style="text-align:center;">
                    <td  colspan="4" style="text-align:center;">
                    <asp:Panel ID="pnLoanEnquiry" runat="server">
                    </asp:Panel>
                    </td>
           </tr>
         <tr style="text-align:center;">
              <td  colspan="4" style="text-align:center;">
              <asp:Panel ID="pnDisscussionDtl" runat="server">
              </asp:Panel>
             </td>
        </tr>
        <tr style="text-align:center;">
                <td  colspan="4" style="text-align:center;">
                <input id="btnSave" type="button" value="SAVE" onclick="return btnSave_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />&nbsp;
                <input id="btnExit" 
                type="button" value="EXIT" onclick="return btnExit_onclick()" style="font-family: cambria; width: 5%; cursor: pointer;" />
              
                <asp:HiddenField 
                    ID="hdnLoanEnquiry" runat="server" />
                <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:HiddenField 
                    ID="hdnResponse" runat="server" />
                
               </td>
         </tr>
    </table>
    </div> 
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </asp:Content>
