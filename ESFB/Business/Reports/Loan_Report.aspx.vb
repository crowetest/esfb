﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Loan_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable  
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim BranchID As Integer
    Dim State_ID As Integer

    Dim UserID As Integer
    Dim GMASTER As New Master
    Dim CallBackReturn As String = Nothing
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
    Dim AD As New Audit
    Dim PostID As Integer

   

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1170) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim strWhere As String = ""
            Dim strWhere1 As String = ""
            BranchID = CInt(Session("BranchID"))
            UserID = CInt(Session("UserID"))
            Me.Master.subtitle = "Loan Lead Report"
            DT = GMASTER.GetEmpRoleList(UserID)
            For Each DR As DataRow In DT.Rows
                If DR(2).ToString() = "40" Then
                    AdminFlag = True
                    Exit For
                End If

            Next

           

            If Not IsPostBack Then
                strWhere = " And bm.branch_id = " & BranchID & ""
                'strWhere1 = " And bm.State_id = " & State_ID & ""
                If AdminFlag = False Then
                    'strWhere = " And branch_id = " & BranchID & ""

                    DT = DB.ExecuteDataSet("select  Branch_ID  as Branch_ID ,upper(Branch_Name) as Branch_Name from BRANCH_MASTER bm where status_id=1  " & strWhere & "  order by 2").Tables(0)
                Else

                    DT = DB.ExecuteDataSet("select '-11' as Branch_ID,' ------ALL------' as Branch_Name Union All select  Branch_ID  as Branch_ID ,upper(Branch_Name) as Branch_Name from BRANCH_MASTER where status_id=1 order by 2").Tables(0)
                End If

                GF.ComboFill(cmbBranch, DT, 0, 1)
                If AdminFlag = False Then


                    DT = DB.ExecuteDataSet("select sm.state_id,sm.state_name  from state_master sm inner join branch_master bm on sm.State_ID=bm.State_ID where sm.State_ID=1  " & strWhere & " ").Tables(0)
                Else

                    DT = DB.ExecuteDataSet("select '-1' as State_id,' ------ALL------' as State Union All select  State_ID  as State_id ,upper(State_name) as State from STATE_MASTER where status_id=1 order by 2").Tables(0)
                End If

                GF.ComboFill(cmbState, DT, 0, 1)
                If AdminFlag = False Then
                    DT = DB.ExecuteDataSet("select  dm.District_ID  as District_ID ,upper(District_Name) as District_Name from DISTRICT_MASTER  dm inner join branch_master bm  on dm.District_ID=bm.District_ID and bm.status_id=1  " & strWhere & "   order by 2").Tables(0)
                Else

                    DT = DB.ExecuteDataSet("select '-1' as District_ID,' ------ALL------' as District_Name Union All select  District_ID  as District_ID ,upper(District_Name) as District_Name from DISTRICT_MASTER where status_id=1 order by 2").Tables(0)
                End If

                GF.ComboFill(cmbDistrict, DT, 0, 1)
                If AdminFlag = False Then
                    DT = DB.ExecuteDataSet("select '-1' as Staff_id,' ------ALL------' as Staff Union All select distinct Enquiry_userid  as Staff_id ,upper(emp_name) as Staff from LOAN_ENQUIRY LE inner join emp_master on LE.Enquiry_UserID=emp_master.Emp_Code inner join branch_master bm on LE.Branch_ID=bm.Branch_ID " & strWhere & " order by 2").Tables(0)
                Else

                    DT = DB.ExecuteDataSet("select '-1' as Staff_id,' ------ALL------' as Staff Union All select distinct Enquiry_userid  as Staff_id ,upper(emp_name) as Staff from LOAN_ENQUIRY inner join emp_master on LOAN_ENQUIRY.Enquiry_UserID=emp_master.Emp_Code order by 2").Tables(0)
                End If

                GF.ComboFill(cmbStaff, DT, 0, 1)
            End If
            '
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            'GF.ComboFill(cmbState, MAST.GetState(), 0, 1)


            Me.cmbState.Attributes.Add("onchange", "StateOnChange()")
            Me.cmbDistrict.Attributes.Add("onchange", "DistrictOnChange()")
            Me.cmbBranch.Attributes.Add("onchange", "BranchOnChange()")
            Me.cmbStatus.Attributes.Add("onchange", "StatusOnChange()")
            btnView.Attributes.Add("onclick", "return ViewOnClick()")
         
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()  
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
          

        ElseIf CInt(Data(0)) = 2 Then
            Dim StateID As Integer = CInt(Data(1))
            Dim StrWhrState As String
            StrWhrState = " 1 = 1 "
            If StateID <> -1 Then
                StrWhrState = "State_ID = " & StateID

            End If
            DT = DB.ExecuteDataSet("select -1 as District_id,' -----ALL-----' as District_Name union all  select District_ID,District_Name from dbo.DISTRICT_MASTER where " & StrWhrState & " order by District_Name").Tables(0)


            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 3 Then
            Dim StateID As Integer = CInt(Data(1))
            Dim DistrictID As Integer = CInt(Data(2))
            Dim StrWhrDistrict As String
            StrWhrDistrict = "1 = 1 "
            If DistrictID <> -1 Then
                StrWhrDistrict = "District_ID = " & DistrictID

            End If
            '   DT = DB.ExecuteDataSet("select -11 as Branch_id,' -----ALL-----' as Branch_Name union all  select Branch_id,Branch_Name from dbo.Branch_master where State_ID=" & StateID & " or " & StrWhrDistrict & " order by Branch_Name").Tables(0)
            DT = DB.ExecuteDataSet("select -11 as Branch_id,' -----ALL-----' as Branch_Name union all  select Branch_id,Branch_Name from dbo.Branch_master where  " & StrWhrDistrict & " order by Branch_Name").Tables(0)

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        ElseIf CInt(Data(0)) = 4 Then
            Dim StateID As Integer = CInt(Data(1))
            Dim DistrictID As Integer = CInt(Data(2))
            Dim BranchID As Integer = CInt(Data(3))
            Dim StrWhrBranch As String
            StrWhrBranch = "1 = 1 "
            If BranchID <> -11 Then
                StrWhrBranch = "bm.branch_id = " & BranchID

            End If
            DT = DB.ExecuteDataSet("select '-1' as Staff_id,' ------ALL------' as Staff Union All select distinct Enquiry_userid  as Staff_id ,upper(emp_name) as Staff from LOAN_ENQUIRY Ln inner join emp_master em on Ln.Enquiry_UserID=em.Emp_Code inner join Branch_master bm on em.branch_id=bm.Branch_ID and  " & StrWhrBranch & "  order by 2").Tables(0)

            For Each DR As DataRow In DT.Rows
                CallBackReturn += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
            Next
        End If
    End Sub
#End Region
#Region "Events"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        Try
            Dim Arr() As String = hidData.Value.Split(CChar("ÿ"))
            Dim StateID As Integer = CInt(Arr(0))
            Dim DistrictID As Integer = CInt(Arr(1))
            Dim BranchID As Integer = CInt(Arr(2))
            Dim StaffID As Integer = CInt(Arr(3))
            Dim StatusID As Integer = CInt(Arr(4))
            Dim OfficerStatusID = CInt(Arr(5))
            Dim FromDt As String = CStr(Arr(6))
            Dim ToDt As String = CStr(Arr(7))
            Dim SqlStr As String = ""
            'Dim StateID As Integer = CInt(Me.cmbState.SelectedValue)
            'Dim DistrictID As Integer = CInt(Me.cmbDistrict.SelectedValue)
            'Dim BranchID As Integer = CInt(Me.cmbBranch.SelectedValue)
            'Dim StaffID As Integer = CInt(Me.cmbStaff.SelectedValue)


            'Dim ToDt As Date = CDate(Me.CALFromDt.SelectedDate.ToShortDateString())
            '  Dim FromDt As String = CInt(Me.txtFromDt.Text).ToString
            'Dim StatusID As Integer = CInt(cmbStatus.SelectedValue)
            'Dim OfficerStatusID As Integer = CInt(cmbOfficerResponse.SelectedValue)
           


            Dim StrWhrState As String
            Dim StrWhrDistrict As String
            Dim StrWhrBranch As String
            Dim StrWhrStaff As String
            Dim StrWhrStatus As String
            Dim StrWhrOfficerStatus As String
            Dim StrDateWhere As String

            StrWhrState = " 1 = 1 "
            StrWhrDistrict = " and  1 = 1 "
            StrWhrBranch = " and  1 = 1 "
            StrWhrStaff = " and  1 = 1 "
            StrWhrStatus = " and  1 = 1 "
            StrWhrOfficerStatus = " and  1 = 1 "
            StrDateWhere = " and  1 = 1 "

            If StateID <> -1 Then
                StrWhrState = " bm.State_ID = " & StateID

            End If
            If DistrictID <> -1 Then
                StrWhrDistrict = " And bm.District_ID = " & DistrictID

            End If
            If BranchID <> -11 Then
                StrWhrBranch = " And bm.branch_id = " & BranchID

            End If
            If StaffID <> -1 Then
                StrWhrStaff = " And em.emp_code = " & StaffID

            End If
            If StatusID <> -1 Then
                StrWhrStatus = " And ln.status_ID = " & StatusID

            End If
            If OfficerStatusID <> -1 Then
                StrWhrOfficerStatus = " And ln.Officers_Status = " & OfficerStatusID

            End If
            If FromDt <> "" And ToDt <> "" Then
                StrDateWhere += " and  cast(ln.Enquiry_Date as date)>=cast('" & FromDt & "' as date) and  cast(ln.Enquiry_Date as date)<=Cast('" & ToDt & "' as date)"

            End If

            SqlStr = "select ROW_NUMBER() OVER (ORDER BY ln.Enquiry_ID) AS Sl_No,bm.Branch_Name as Branch_Name,bm.branch_id as  " & _
                             " Branch_ID,  convert(varchar,ln.Branch_ID)+'/'+  RIGHT('00' + CAST(DATEPART(mm,ln.enquiry_date) AS varchar(2)), 2)+'-'+RIGHT(CONVERT(VARCHAR(8),ln.enquiry_date, 1),2) " & _
                             " +'/'  +convert(varchar,ln.Enquiry_ID)  as Lead_Reference_No,ln.name as Name, ln.address as Place_Name,ln.mobile  as Mobile_No,ln.Altmobile as Alternative_Phone_Number, " & _
                             " case when ln.Loan_Purpose='1' then  'Micro Housing Loan'  when ln.Loan_Purpose='2' then 'Agri Loan' when ln.Loan_Purpose='3' then 'Business Loan' when ln.Loan_Purpose='4'   " & _
                             " then 'LAP' when ln.Loan_Purpose='5'  then '2 wheeler loan' when ln.Loan_Purpose='6'  then '3&4 wheeler loan' when ln.Loan_Purpose='7' then 'Gold Loan' when  " & _
                             " ln.Loan_Purpose='8' then 'MSME Loan' when ln.Loan_Purpose='9' then 'Dream Housing Loan' when ln.Loan_Purpose='10' then 'Personal Loan' when ln.Loan_Purpose='11' then 'Global Career Education' when ln.Loan_Purpose='12' then 'Light Commercial Vehicle Loan' when ln.Loan_Purpose='13' then 'LRD' when ln.Loan_Purpose='14' then 'Solar/Clean Energy'  end as Loan_Product, ln.Loan_Amount as  Loan_Amount,isnull(ln.remarks,'') as Remarks,case ln.Sangam_ID when 0 then null " & _
                             " else ln.Sangam_ID end  as Sangam_ID,ln.sangam_name  as Sangam_Name,ln.Ref_Emp_Name  as Referred_By_Emp_Name, case ln.ref_emp_code when 0 then null else ln.ref_emp_code  " & _
                             " end  as Referred_By_Emp_code,ln.Ref_Contact  as  Referred_By_Contact_NO ,em.Emp_Name as  Entered_by_Emp_Name,ln.Enquiry_userId  as Entered_by_Emp_code, dm.department_name as [Department of entered employee],  " & _
                             "  CONVERT(VARCHAR(10),ln.enquiry_date,105) as Entered_by_Date,isnull(CONVERT(VARCHAR(8), ln.Enquiry_Date, 108),'')  [Entered_by_Time] , isnull( emm.emp_name,'') as  " & _
                             " Verified_by_Emp_Name,case when ln.response_userID=0 then null else ln.response_userID end as Verified_by_Emp_Code,case  when ln.status_id is null or ln.status_id=-1 then 'Status Not Updated' when " & _
                             " ln.status_id=1  then 'Non contactable' when   ln.status_id=2 then  'On Hold' when ln. status_id=3  then 'Need more information'   when  ln.status_id=4 then  " & _
                             " 'Approved for Processing' when  ln.status_id=5 then 'Rejected by HQ ' when  ln.status_id=6 then 'Sanctioned by HQ ' when  ln.status_id=7 then 'Return by HQ ' end as HO_Status ,isnull(ln.explanation,'')  as Explanation, isnull(CONVERT(VARCHAR(10),ln.Response_Date,105),'') " & _
                             "  as HO_Response_Date,isnull( CONVERT(VARCHAR(8), ln.Response_Date, 108),'') [HO_Response_Time] ,DATEDIFF (d,ln.enquiry_date, ln.Response_Date) as HO_TAT, case when Assigned_Credit_officer=-1 then null else  Assigned_Credit_officer end " & _
                             " as Assigned_CO_Emp_code,isnull(emmm.emp_name,'')as Assigned_CO_Emp_Name, case  Officers_Status when 1 then 'Televerification Done'  when 2 then 'Branch visit by Customer' when 3 then  'Customer place visit done' when 4 then  " & _
                             " 'Document pending from Customer' when 5 then'Submitted for Sanction' when 6 then 'Sanctioned' when 7 then 'Rejected by CO'  when 8 then 'Not interested'  when 9 then 'Not contactable'  when 10 then 'Not Eligible'  when 11 then 'In Process'  when 12 then 'Document Pending'  when 13 then 'Sanctioned' when 14 then 'Disbursed' when 15 then 'Legal / Technical rejected' when 16 then 'Follow Up' else '' end  as CO_Status, " & _
                          " isnull(Officers_Remarks,'') as CO_Remarks, isnull(CONVERT(VARCHAR(10), ln.Officers_Response_Date,105),'') as Last_Updated_Date,isnull(CONVERT(VARCHAR(8),  " & _
                          " ln.Officers_Response_Date, 108),'') [Last_Updated_Time], isnull(DATEDIFF (d,ln.enquiry_date, ln.Officers_Response_Date),0)   as CO_TAT from  loan_enquiry ln  inner join  " & _
                          " branch_master bm  on ln.branch_id=bm.branch_id inner join emp_master em on ln.Enquiry_UserID=em.Emp_Code  left join department_master dm on dm.department_id=em.department_id left join emp_master emmm on  " & _
                          "ln.Assigned_Credit_Officer=emmm.Emp_Code left join emp_master emm on  ln.Response_UserID = emm.Emp_Code   " & _
                             " where  " + StrWhrState + StrWhrDistrict + StrWhrBranch + StrWhrStaff + StrWhrStatus + StrWhrOfficerStatus + StrDateWhere

            'SqlStr = "select ROW_NUMBER() OVER (ORDER BY ln.Enquiry_ID) AS Sl_No,sm.State_Name as State,dm.District_Name as District, bm.Branch_Name as Branch_Name,bm.branch_id as  " & _
            '        " Branch_ID,  convert(varchar,ln.Branch_ID)+'/'+  RIGHT('00' + CAST(DATEPART(mm,ln.enquiry_date) AS varchar(2)), 2)+'-'+RIGHT(CONVERT(VARCHAR(8),ln.enquiry_date, 1),2) " & _
            '        " +'/'  +convert(varchar,ln.Enquiry_ID)  as Lead_Reference_No,ln.name as Name, ln.address as Place_Name,ln.mobile  as Mobile_No,ln.Altmobile as Alternative_Phone_Number, " & _
            '        " case when ln.Loan_Purpose='1' then  'Micro Housing Loan'  when ln.Loan_Purpose='2' then 'Agri Loan' when ln.Loan_Purpose='3' then 'Business Loan' when ln.Loan_Purpose='4'   " & _
            '        " then 'LAP' when ln.Loan_Purpose='5'  then '2 wheeler loan' when ln.Loan_Purpose='6'  then '3&4 wheeler loan' when ln.Loan_Purpose='7' then 'Gold Loan' when  " & _
            '        " ln.Loan_Purpose='8' then 'MSME Loan' when ln.Loan_Purpose='9' then 'Dream Housing Loan' end as Loan_Product, ln.Loan_Amount as  Loan_Amount,isnull(ln.remarks,'') as Remarks,case ln.Sangam_ID when 0 then null " & _
            '        " else ln.Sangam_ID end  as Sangam_ID,ln.sangam_name  as Sangam_Name,ln.Ref_Emp_Name  as Referred_By_Emp_Name, case ln.ref_emp_code when 0 then null else ln.ref_emp_code  " & _
            '        " end  as Referred_By_Emp_code,ln.Ref_Contact  as  Referred_By_Contact_NO ,em.Emp_Name as  Entered_by_Emp_Name,ln.Enquiry_userId  as Entered_by_Emp_code,  " & _
            '        "  CONVERT(VARCHAR(10),ln.enquiry_date,105) as Entered_by_Date,isnull(CONVERT(VARCHAR(8), ln.Enquiry_Date, 108),'')  [Entered_by_Time] , isnull( emm.emp_name,'') as  " & _
            '        " Verified_by_Emp_Name,case when ln.response_userID=0 then null else ln.response_userID end as Verified_by_Emp_Code,case  when ln.status_id is null or ln.status_id=-1 then 'Status Not Updated' when " & _
            '        " ln.status_id=1  then 'Non contactable' when   ln.status_id=2 then  'On Hold' when ln. status_id=3  then 'Need more information'   when  ln.status_id=4 then  " & _
            '        " 'Approved for Processing' when  ln.status_id=5 then 'Rejected by HQ ' when  ln.status_id=6 then 'Sanctioned by HQ ' when  ln.status_id=7 then 'Return by HQ ' end as HO_Status ,isnull(ln.explanation,'')  as Explanation, isnull(CONVERT(VARCHAR(10),ln.Response_Date,105),'') " & _
            '        "  as HO_Response_Date,isnull( CONVERT(VARCHAR(8), ln.Response_Date, 108),'') [HO_Response_Time] ,DATEDIFF (d,ln.enquiry_date, ln.Response_Date) as HO_TAT, case when Assigned_Credit_officer=-1 then null else  Assigned_Credit_officer end " & _
            '        " as Assigned_CO_Emp_code,isnull(emmm.emp_name,'')as Assigned_CO_Emp_Name, case  Officers_Status when 1 then 'Televerification Done'  when 2 then 'Branch visit by Customer' when 3 then  'Customer place visit done' when 4 then  " & _
            '        " 'Document pending from Customer' when 5 then'Submitted for Sanction' when 6 then 'Sanctioned' when 7 then 'Rejected by CO' else '' end  as CO_Status, " & _
            '     " isnull(Officers_Remarks,'') as CO_Remarks, isnull(CONVERT(VARCHAR(10), ln.Officers_Response_Date,105),'') as Last_Updated_Date,isnull(CONVERT(VARCHAR(8),  " & _
            '     " ln.Officers_Response_Date, 108),'') [Last_Updated_Time], isnull(DATEDIFF (d,ln.enquiry_date, ln.Officers_Response_Date),0)   as CO_TAT from  loan_enquiry ln  inner join  " & _
            '     " branch_master bm  on ln.branch_id=bm.branch_id  inner join state_master sm on sm.State_ID=bm.State_ID  inner join  " & _
            '     " DISTRICT_MASTER dm on dm.District_ID=bm.District_ID  inner join emp_master em on ln.Enquiry_UserID=em.Emp_Code left join emp_master emmm on  " & _
            '     "ln.Assigned_Credit_Officer=emmm.Emp_Code left join emp_master emm on  ln.Response_UserID = emm.Emp_Code   " & _
            '        " where  " + StrWhrState + StrWhrDistrict + StrWhrBranch + StrWhrStaff + StrWhrStatus + StrWhrOfficerStatus + StrDateWhere


            DT = DB.ExecuteDataSet(SqlStr).Tables(0)



            WebTools.ExporttoExcel(DT, "Loan_Lead_Report")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

    

End Class
