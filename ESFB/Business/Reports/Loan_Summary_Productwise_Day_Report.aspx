﻿

<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Loan_Summary_Productwise_Day_Report.aspx.vb" Inherits="Loan_Report_Product_Day" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
        .style2
        {
            width: 23%;
        }
    </style>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
          function window_onload() {

           }
          function StatusOnChange() {
        }
         function FromServer(arg, context) {

         switch (context) {
         
            case 1:
              
                 var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("Loan_Report.aspx", "_self");
                break;
             }
            }
        function ComboFill(data, ddlName) {
             document.getElementById(ddlName).options.length = 0;
             var rows = data.split("Ñ");
           
             for (a = 1; a < rows.length; a++) {
                 var cols = rows[a].split("ÿ");
                 var option1 = document.createElement("OPTION");
                 option1.value = cols[0];
                 option1.text = cols[1];
                
                 document.getElementById(ddlName).add(option1);
             }
         }
        

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function ViewOnClick() {           
            var FromDt = document.getElementById("<%= txtFromDt.ClientID %>").value;  
            var ToDt = document.getElementById("<%= txtToDt.ClientID %>").value; 
            document.getElementById("<%= hidData.ClientID %>").value = FromDt + "ÿ" + ToDt;          
        }

        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 65%; margin: 0px auto;">
       <tr>
       <td style="width: 10%;">
                </td>
             <td style="text-align: right;" class="style2">
             From Date
            </td>
            
            
             <td style="width: 58%">
            &nbsp;
                <asp:TextBox ID="txtFromDt" runat="server" class="NormalText" Width="40%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CALFromDt" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtFromDt"></asp:CalendarExtender>

                </td>
                
        </tr>
       <tr>
       <td style="width: 10%;">
                </td>
             <td style="text-align: right;" class="style2">
             To Date
            </td>
            
            
             <td style="width: 58%">
      &nbsp;
                <asp:TextBox ID="txtToDt" runat="server" class="NormalText" Width="40%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender1" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtToDt"></asp:CalendarExtender>

                </td>
                
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
               <asp:Button ID="btnView" runat="server" Text="VIEW" Font-Names="Cambria" 
                    style="margin-bottom: 0px" Width="8%" />
                    
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
            <asp:HiddenField ID="hidData" runat="server" />
    </table>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
