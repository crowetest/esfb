﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class PDD_Status_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim CallBackReturn As String = Nothing
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim GF As New GeneralFunctions
    Dim GMASTER As New Master
    Dim TraDt As Date
    Dim DB As New MS_SQL.Connect
    Dim WebTools As New WebApp.Tools
#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strWhere As String = ""
        Try
            'If GF.FormAccess(CInt(Session("UserID")), 1383) = False Then
            '    Response.Redirect("~/AccessDenied.aspx", False)
            '    Return
            'End If
            Me.Master.subtitle = "PDD Status Report"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            '<Summary>Change done by 40020 on 01-Feb-2021</Summary>
            '<Comments>ASMs & RSM/CHs can view all the PDDs under their mapped branches</Comments>
            'Dim SO As Integer = 0
            'Dim ASM As Integer = 0
            'Dim CH As Integer = 0
            'DT = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE = " + Session("UserID")).Tables(0)
            'If DT.Rows.Count > 0 Then
            '    If CInt(DT.Rows(0)(0).ToString()) = 2 Then
            '        SO = 1
            '    End If
            'End If
            'Dim i As Integer = 0
            'DT1 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + ")").Tables(0)
            'If DT1.Rows.Count > 0 Then
            '    For Each dr In DT1.Rows
            '        If CInt(DT1.Rows(i)(0)) = 2 Then
            '            ASM = 1
            '        End If
            '    Next
            'End If
            'DT2 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + "))").Tables(0)
            'If DT2.Rows.Count > 0 Then
            '    For Each dr In DT2.Rows
            '        If CInt(DT2.Rows(i)(0)) = 2 Then
            '            CH = 1
            '        End If
            '    Next
            'End If
            'If SO = 1 Then
            '    DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----ALL-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1 AND BRANCH_ID IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE = " + Session("UserID") + ") ORDER BY BRANCH_NAME").Tables(0)
            'ElseIf ASM = 1 Then
            '    DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----ALL-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1 AND BRANCH_ID IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + ")) ORDER BY BRANCH_NAME").Tables(0)
            'ElseIf CH = 1 Then
            '    DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----ALL-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1 AND BRANCH_ID IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + "))) ORDER BY BRANCH_NAME").Tables(0)
            'Else
            '    DT = DB.ExecuteDataSet("SELECT '-1' AS BRANCH_ID,'-----ALL-----' AS BRANCH_NAME UNION ALL SELECT BRANCH_ID,BRANCH_NAME FROM BRANCH_MASTER WHERE STATUS_ID = 1 ORDER BY BRANCH_NAME").Tables(0)
            'End If
            Dim Params(3) As SqlParameter
            Params(0) = New SqlParameter("@Emp_Code", SqlDbType.VarChar, 20)
            Params(0).Value = Session("UserID").ToString()
            Params(1) = New SqlParameter("@New_Cnt", SqlDbType.VarChar, 20)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@Return_Cnt", SqlDbType.VarChar, 20)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@Level", SqlDbType.Int)
            Params(3).Value = 1
            DT = DB.ExecuteDataSet("SP_PDD_GET_SO_DATA", Params).Tables(0)

            GF.ComboFill(Me.cmbBranch, DT, 0, 1)
            DT = DB.ExecuteDataSet("SELECT '-1' AS CATEGORY_ID,'-----ALL-----' AS CATEGORY_NAME UNION ALL SELECT CATEGORY_ID,CATEGORY_NAME FROM PDD_CATEGORY_MASTER WHERE STATUS = 1 ORDER BY CATEGORY_NAME").Tables(0)
            GF.ComboFill(Me.cmbLoanCategory, DT, 0, 1)
            DT = DB.ExecuteDataSet("SELECT '-1' AS STATUS_ID,'-----ALL-----' AS STATUS_NAME UNION ALL SELECT STATUS_ID,STATUS_NAME FROM PDD_STATUS_MASTER ORDER BY STATUS_NAME").Tables(0)
            GF.ComboFill(Me.cmbStatus, DT, 0, 1)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        GC.Collect()
        DT.Dispose()
        DB.dispose()
    End Sub
#End Region
#Region "Events"
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then

        End If
    End Sub
#End Region
End Class

