﻿

<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" AutoEventWireup="false" CodeFile="Loan_Report.aspx.vb" Inherits="Loan_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 40%;
            height: 104px;
        }
    </style>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <link href="../../Style/Style.css" rel="stylesheet" type="text/css" />
        <script src="../../Script/Validations.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
          function window_onload() {
     
              document.getElementById("<%= cmbOfficerResponse.ClientID%>").disabled = true; 
           }
          function StatusOnChange() {

            if (document.getElementById("<%= cmbStatus.ClientID %>").value == "4" ) {
           document.getElementById("<%= cmbOfficerResponse.ClientID%>").disabled = false; 
             
            }
            else {
                document.getElementById("<%= cmbOfficerResponse.ClientID%>").disabled = true; 
            }
        }
          function StateOnChange() {
            var StateID = document.getElementById("<%= cmbState.ClientID %>").value;           
            var ToData = "2Ø" + StateID;    
     
            ToServer(ToData, 2);
          } 
          
                 
           function DistrictOnChange() {
             var StateID = document.getElementById("<%= cmbState.ClientID %>").value;    
            var DistrictID = document.getElementById("<%= cmbDistrict.ClientID %>").value;
                    
            var ToData = "3Ø" + StateID+"Ø"+DistrictID;    
        
            ToServer(ToData, 3);
          } 

          function BranchOnChange() {
           var StateID = document.getElementById("<%= cmbState.ClientID %>").value;  
            var DistrictID = document.getElementById("<%= cmbDistrict.ClientID %>").value;
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value;              
            var ToData = "4Ø" + StateID+"Ø"+DistrictID+"Ø"+BranchID;    
        
            ToServer(ToData, 4);
          } 
          

         function FromServer(arg, context) {

         switch (context) {
         
            case 1:
              
                 var Data = arg.split("Ø");
                  alert(Data[1]);
                  if (Data[0] == 0) window.open("Loan_Report.aspx", "_self");
                break;
            case 2:
              
                ComboFill(arg, "<%= cmbDistrict.ClientID %>");
                break;
          
            case 3:
            
              ComboFill(arg, "<%= cmbBranch.ClientID %>");
                break;
            case 4:
            
              ComboFill(arg, "<%= cmbStaff.ClientID %>");
                break;
 
             }
            }
        function ComboFill(data, ddlName) {
             document.getElementById(ddlName).options.length = 0;
             var rows = data.split("Ñ");
           
             for (a = 1; a < rows.length; a++) {
                 var cols = rows[a].split("ÿ");
                 var option1 = document.createElement("OPTION");
                 option1.value = cols[0];
                 option1.text = cols[1];
                
                 document.getElementById(ddlName).add(option1);
             }
         }
        

        function btnExit_onclick() 
        {
            window.open("<%= Page.ResolveUrl("~")%>Home.aspx", "_self");
        }
        function ViewOnClick() {           
            var StateID = document.getElementById("<%= cmbState.ClientID %>").value;  
          
            var DistrictID = document.getElementById("<%= cmbDistrict.ClientID %>").value;
            var BranchID = document.getElementById("<%= cmbBranch.ClientID %>").value; 
            var StaffID = document.getElementById("<%= cmbStaff.ClientID %>").value; 
            var StatusID = document.getElementById("<%= cmbStatus.ClientID %>").value; 
            var OfficerStatusID = document.getElementById("<%= cmbOfficerResponse.ClientID %>").value; 
            var FromDt    = document.getElementById("<%= txtFromDt.ClientID %>").value;  
            var ToDt    = document.getElementById("<%= txtToDt.ClientID %>").value; 
       if(FromDt=="" && ToDt != "")
        {
            alert("Select From  Date First");
            document.getElementById("<%= txtFromDt.ClientID %>").focus();
            return false;
        }
        else if (FromDt!="" && ToDt == "")
        {
            alert("Select To Date Also");
            document.getElementById("<%= txtToDt.ClientID %>").focus();
            return false;
        }
        document.getElementById("<%= hidData.ClientID %>").value = StateID + "ÿ" + DistrictID + "ÿ" + BranchID+ "ÿ" + StaffID+ "ÿ" +  StatusID + "ÿ" + OfficerStatusID + "ÿ" + FromDt + "ÿ" + ToDt ;
        
       
                  
        }

        </script>
    </head>
    </html>
    <br />
    <br />
    <table class="style1" style="width: 80%; margin: 0px auto;">
        
      
        
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                State
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbState" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------ALL---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                District
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbDistrict" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------ALL---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Branch
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbBranch" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------ALL---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
          <tr>
            <td style="width: 25%;">
            </td>
            <td style="width: 17%; text-align: left;">
                Staff
            </td>
            <td style="width: 58%">
                &nbsp; &nbsp;<asp:DropDownList ID="cmbStaff" class="NormalText" Style="text-align: left;"
                    runat="server" Font-Names="Cambria" Width="40%" ForeColor="Black">
                    <asp:ListItem Value="-1"> ---------ALL---------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
      
         <tr>
             <td style="width: 25%;">
                </td>
             <td style="width: 17%; text-align: left;">
               HO Response Status
            </td>
            <td style="width: 58%">
            &nbsp; &nbsp;<asp:DropDownList ID="cmbStatus" class="NormalText" 
                runat="server" Font-Names="Cambria" 
                    Width="40%" ForeColor="Black" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">------ALL------</asp:ListItem>
                    <asp:ListItem Value="1">Non contactable</asp:ListItem>
                    <asp:ListItem Value="2">On Hold</asp:ListItem>
                    <asp:ListItem Value="3">Need more information</asp:ListItem>
                    <asp:ListItem Value="4">Approved for processing</asp:ListItem>
                    <asp:ListItem Value="5">Rejected by HQ</asp:ListItem>
                    <asp:ListItem Value="5">Sanctioned by HQ</asp:ListItem>
                    <asp:ListItem Value="5">Return by HQ</asp:ListItem>
                </asp:DropDownList></td>
                 
           
        </tr>
         <tr>
             <td style="width: 25%;">
                </td>
             <td style="width: 17%; text-align: left;">
               CO Response Status
            </td>
            <td style="width: 58%">
            &nbsp; &nbsp;<asp:DropDownList ID="cmbOfficerResponse" class="NormalText" 
                runat="server" Font-Names="Cambria" 
                    Width="40%" ForeColor="Black" AppendDataBoundItems="True" >
                    <asp:ListItem Value="-1">------ALL------</asp:ListItem>
                   <%-- <asp:ListItem Value="1">Televerification Done</asp:ListItem>
                    <asp:ListItem Value="2">Branch visit by Customer</asp:ListItem>
                    <asp:ListItem Value="3">Customer place visit done</asp:ListItem>
                    <asp:ListItem Value="4">Document pending from Customer</asp:ListItem>
                    <asp:ListItem Value="5">Submitted for Sanction</asp:ListItem>
                    <asp:ListItem Value="6">Sanctioned</asp:ListItem>
                    <asp:ListItem Value="7">Rejected by CO</asp:ListItem>--%>
                    <asp:ListItem Value="8">Not interested</asp:ListItem>
                    <asp:ListItem Value="9">Not contactable</asp:ListItem>
                    <asp:ListItem Value="10">Not Eligible</asp:ListItem>
                    <asp:ListItem Value="11">In Process</asp:ListItem>
                    <asp:ListItem Value="12">Document Pending</asp:ListItem>
                    <asp:ListItem Value="13">Sanctioned</asp:ListItem>
                    <asp:ListItem Value="14">Disbursed</asp:ListItem>
                    <asp:ListItem Value="15">Legal / Technical rejected</asp:ListItem>
                    <asp:ListItem Value="16">Follow Up</asp:ListItem>

                </asp:DropDownList></td>
                 
           
        </tr>
       <tr>
       <td style="width: 25%;">
                </td>
             <td style="width: 17%; text-align: left;">
             From Date
            </td>
            
            
             <td style="width: 58%">
            &nbsp;
                <asp:TextBox ID="txtFromDt" runat="server" class="NormalText" Width="40%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CALFromDt" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtFromDt"></asp:CalendarExtender>

                </td>
                
        </tr>
        <tr>
       <td style="width: 25%;">
                </td>
             <td style="width: 17%; text-align: left;">
             To Date
            </td>
            
            
             <td style="width: 58%">
      &nbsp;
                <asp:TextBox ID="txtToDt" runat="server" class="NormalText" Width="40%"  
                    ReadOnly="true"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender1" Format="dd MMM yyyy"  runat="server" Enabled="True" TargetControlID="txtToDt"></asp:CalendarExtender>

                </td>
                
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">
                <br />
               <asp:Button ID="btnView" runat="server" Text="VIEW" Font-Names="Cambria" 
                    style="margin-bottom: 0px" Width="8%" />
                    
                <input id="btnExit" style="font-family: cambria; cursor: pointer; width: 67px;" type="button"
                    value="EXIT" onclick="return btnExit_onclick()" />
            </td>
        </tr>
            <asp:HiddenField ID="hidData" runat="server" />
    </table>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
