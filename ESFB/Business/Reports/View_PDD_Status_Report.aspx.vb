﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_PDD_Status_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim Branch As String = GN.Decrypt(Request.QueryString.Get("Branch"))
            Dim Category As String = GN.Decrypt(Request.QueryString.Get("Category"))
            Dim Status As String = GN.Decrypt(Request.QueryString.Get("Status"))
            Dim FromDt As String = CStr(Request.QueryString.Get("FromDt"))
            Dim ToDt As String = CStr(Request.QueryString.Get("ToDt"))
            Dim wino As String = GN.Decrypt(Request.QueryString.Get("wino"))
            Dim strWhere As String = " WHERE 1 = 1"

            '<Summary>Change done by 40020 on 01-Feb-2021</Summary>
            '<Comments>ASMs & RSM/CHs can view all the PDDs under their mapped branches</Comments>
            'Dim Admin As Integer = 0
            'Dim SO As Integer = 0
            'Dim ASM As Integer = 0
            'Dim CH As Integer = 0
            'DT = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE = " + Session("UserID")).Tables(0)
            'If DT.Rows.Count > 0 Then
            '    If CInt(DT.Rows(0)(0).ToString()) = 2 Then
            '        SO = 1
            '    End If
            '    If CInt(DT.Rows(0)(0).ToString()) = 0 Then
            '        Admin = 1
            '    End If
            'End If
            'Dim j As Integer = 0
            'DT1 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + ")").Tables(0)
            'If DT1.Rows.Count > 0 Then
            '    For Each dr1 In DT1.Rows
            '        If CInt(DT1.Rows(j)(0)) = 2 Then
            '            ASM = 1
            '        End If
            '    Next
            'End If
            'DT2 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + "))").Tables(0)
            'If DT2.Rows.Count > 0 Then
            '    For Each dr1 In DT2.Rows
            '        If CInt(DT2.Rows(j)(0)) = 2 Then
            '            CH = 1
            '        End If
            '    Next
            'End If
            'If wino <> "" Then
            '    strWhere += " AND A.WI_NO = '" + wino + "'"
            'End If

            'If CInt(Branch) <> -1 Then
            '    strWhere += " AND A.BRANCH_CODE = " + Branch
            'ElseIf Admin = 0 Then
            '    If SO = 1 Then
            '        strWhere += " AND A.BRANCH_CODE IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE =" + Session("UserID") + ")"
            '    ElseIf ASM = 1 Then
            '        strWhere += " AND A.BRANCH_CODE IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + "))"
            '    ElseIf CH = 1 Then
            '        strWhere += " AND A.BRANCH_CODE IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + ")))"
            '    End If
            'End If

            'If CInt(Category) <> -1 Then
            '    strWhere += " And A.CATEGORY = " + Category
            'End If

            'If CInt(Status) <> -1 Then
            '    strWhere += " AND B.STATUS = " + Status
            'End If


            'If FromDt <> "" And ToDt <> "" Then
            '    strWhere += " AND CONVERT(DATE,B.DUE_DATE) BETWEEN CONVERT(DATE,'" + FromDt.ToString() + "') AND CONVERT(DATE,'" + ToDt.ToString() + "') "
            'Else

            'End If

            ''DT = DB.ExecuteDataSet("Select A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, convert(varchar(10),A.DISBURSAL_DATE,103), C.CATEGORY_NAME, D.PRODUCT_NAME, E.PDD_NAME, Case When B.TO_BE_UPDATED = 1 Then 'TO BE UPDATED' ELSE CONVERT(VARCHAR(10),B.DUE_DATE,103) END AS DUE_DATE, CASE WHEN B.STATUS IN (2,5) THEN 'APPROVER LEVEL COMPLETED' WHEN B.STATUS = 3 THEN 'PDD ENTRY TEAM LEVEL PENDING' WHEN B.STATUS IN (1,4,7) THEN 'SO LEVEL PENDING ( '+STUFF((SELECT DISTINCT ', '+EM.EMP_NAME FROM PDD_SO_MAPPING PSM INNER JOIN EMP_MASTER EM ON EM.EMP_CODE = PSM.EMP_CODE WHERE PSM.BRANCH_ID = A.BRANCH_CODE FOR XML PATH('')),1,1,'')+' )' WHEN B.STATUS = 6 THEN 'APPROVER LEVEL COMPLETED' END AS [CURRENT STATUS], B.SO_REMARKS, CONVERT(VARCHAR(10),B.SO_ENTRY_ON,103) AS SO_ENTRY_DATE, CASE WHEN B.APPROVED_STATUS = 1 THEN 'APPROVED' WHEN B.APPROVED_STATUS = 2 THEN 'RETURNED' WHEN B.APPROVED_STATUS = 3 THEN 'REJECTED' ELSE 'NOT APPROVED' END AS APPROVED_STATUS, B.APPROVED_REMARKS, CONVERT(VARCHAR(10),B.APPROVED_ON,103) AS APPROVED_DATE,B.PDD_ITEM, CONVERT(VARCHAR(10),A.CREATED_DATE,103),CASE WHEN A.STATUS = 7 THEN 'RETURNED' ELSE 'NEW' END AS [RETURNED / NEW]  FROM PDD_ENTRY_MASTER A INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = A.PRODUCT INNER JOIN PDD_MASTER E ON E.PDD_ID = B.PDD_ITEM LEFT JOIN EMP_MASTER F ON F.EMP_CODE = B.SO_ENTRY_BY " + strWhere).Tables(0)
            'DT = DB.ExecuteDataSet("Select A.WI_NO, A.BRANCH_CODE, A.BRANCH_NAME, A.CUSTOMER_NAME, A.LOAN_NUMBER, convert(varchar(10),A.DISBURSAL_DATE,103), C.CATEGORY_NAME, D.PRODUCT_NAME,CASE WHEN B.PDD_ITEM = 1000 THEN B.PDD_OTHER ELSE E.PDD_NAME END AS PDD_NAME, Case When B.TO_BE_UPDATED = 1 Then 'TO BE UPDATED' ELSE CONVERT(VARCHAR(10),B.DUE_DATE,103) END AS DUE_DATE, CASE WHEN B.STATUS IN (2,5) THEN 'APPROVER LEVEL PENDING' WHEN B.STATUS = 3 THEN 'PDD ENTRY TEAM LEVEL PENDING' WHEN B.STATUS IN (1,4,7) THEN 'SO LEVEL PENDING' WHEN B.STATUS = 6 THEN 'APPROVER LEVEL COMPLETED' END AS [CURRENT STATUS], B.SO_REMARKS, CONVERT(VARCHAR(10),B.SO_ENTRY_ON,103) AS SO_ENTRY_DATE, B.APPROVED_REMARKS, CONVERT(VARCHAR(10),B.APPROVED_ON,103) AS APPROVED_DATE,B.PDD_ITEM, CONVERT(VARCHAR(10),A.CREATED_DATE,103), CASE WHEN (SELECT COUNT(*) FROM PDD_ENTRY_SUB_CYCLE WHERE SUB_NO = B.SUB_NO AND STATUS IN (3,7)) > 0 THEN 'RETURNED' ELSE 'NEW' END AS [RETURNED / NEW], STUFF((SELECT DISTINCT ', '+EM.EMP_NAME FROM PDD_SO_MAPPING PSM INNER JOIN EMP_MASTER EM ON EM.EMP_CODE = PSM.EMP_CODE WHERE PSM.BRANCH_ID = A.BRANCH_CODE FOR XML PATH('')),1,1,'')+'' AS SO FROM PDD_ENTRY_MASTER A INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = A.PRODUCT INNER JOIN PDD_MASTER E ON E.PDD_ID = B.PDD_ITEM LEFT JOIN EMP_MASTER F ON F.EMP_CODE = B.SO_ENTRY_BY " + strWhere).Tables(0)
            ''DTEXCEL = DB.ExecuteDataSet("Select A.WI_NO AS [WI NO], A.BRANCH_CODE AS [BRANCH CODE], A.BRANCH_NAME AS [BRANCH NAME], A.CUSTOMER_NAME AS [CUSTOMER NAME], A.LOAN_NUMBER AS [LOAN NUMBER], convert(varchar(10),A.DISBURSAL_DATE,103) AS [DISBURSAL DATE],CONVERT(VARCHAR(10),A.CREATED_DATE,103) AS [PDD ENTRY DATE], C.CATEGORY_NAME AS [CATEGORY], D.PRODUCT_NAME AS [PRODUCT], E.PDD_NAME AS [PDD], Case When B.TO_BE_UPDATED = 1 Then 'TO BE UPDATED' ELSE CONVERT(VARCHAR(10),B.DUE_DATE,103) END AS [DUE DATE], CASE WHEN B.SO_ENTRY_STATUS = 1 THEN 'UPLOADED' WHEN B.SO_ENTRY_STATUS = 3 THEN 'RE-UPLOADED' WHEN B.SO_ENTRY_STATUS IS NULL THEN 'NOT UPLOADED' END AS [SO STATUS], B.SO_REMARKS AS [SO REMARKS], CONVERT(VARCHAR(10),B.SO_ENTRY_ON,103) AS [SO ENTRY DATE], CASE WHEN B.APPROVED_STATUS = 1 THEN 'APPROVED' WHEN B.APPROVED_STATUS = 2 THEN 'RETURNED' WHEN B.APPROVED_STATUS = 3 THEN 'REJECTED' ELSE 'NOT APPROVED' END AS [APPROVED STATUS], B.APPROVED_REMARKS AS [APPROVED REMARKS], CONVERT(VARCHAR(10),B.APPROVED_ON,103) AS [APPROVED DATE] FROM PDD_ENTRY_MASTER A INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = A.CATEGORY INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = A.PRODUCT INNER JOIN PDD_MASTER E ON E.PDD_ID = B.PDD_ITEM LEFT JOIN EMP_MASTER F ON F.EMP_CODE = B.SO_ENTRY_BY " + strWhere).Tables(0)
            ''DTEXCEL = DB.ExecuteDataSet("Select ROW_NUMBER() OVER (ORDER BY A.CREATED_DATE) AS [SL NO],A.WI_NO AS [WI NO], A.BRANCH_CODE AS [BRANCH CODE], A.BRANCH_NAME AS [BRANCH NAME], A.CUSTOMER_NAME AS [CUSTOMER NAME], A.LOAN_NUMBER AS [LOAN NUMBER], convert(varchar(10),A.DISBURSAL_DATE,103) AS [DISBURSAL DATE],CONVERT(VARCHAR(10),A.CREATED_DATE,103) AS [PDD ENTRY DATE], C.CATEGORY_NAME AS [CATEGORY], D.PRODUCT_NAME AS [PRODUCT], E.PDD_NAME AS [PDD], Case When B.TO_BE_UPDATED = 1 Then 'TO BE UPDATED' ELSE CONVERT(VARCHAR(10),B.DUE_DATE,103) END AS [DUE DATE],CASE WHEN B.STATUS IN (2,5) THEN 'APPROVER LEVEL COMPLETED' WHEN B.STATUS = 3 THEN 'PDD ENTRY TEAM LEVEL PENDING' WHEN B.STATUS IN (1,4,7) THEN 'SO LEVEL PENDING ( '+STUFF((SELECT DISTINCT ', '+EM.EMP_NAME FROM PDD_SO_MAPPING PSM INNER JOIN EMP_MASTER EM ON EM.EMP_CODE = PSM.EMP_CODE WHERE PSM.BRANCH_ID = A.BRANCH_CODE FOR XML PATH('')),1,1,'')+' )' WHEN B.STATUS = 6 THEN 'APPROVER LEVEL COMPLETED' END AS [CURRENT STATUS],CASE WHEN A.STATUS = 7 THEN 'RETURNED' ELSE 'NEW' END AS [RETURNED / NEW],B.STATUS,B.SO_REMARKS AS [SO REMARKS], CONVERT(VARCHAR(10),B.SO_ENTRY_ON,103) AS [SO ENTRY DATE], CASE WHEN B.APPROVED_STATUS = 1 THEN 'APPROVED' WHEN B.APPROVED_STATUS = 2 THEN 'RETURNED' WHEN B.APPROVED_STATUS = 3 THEN 'REJECTED' ELSE 'NOT APPROVED' END AS [APPROVER STATUS],  B.APPROVED_REMARKS AS [APPROVER REMARKS], CONVERT(VARCHAR(10),B.APPROVED_ON,103) AS [APPROVER DATE]  FROM PDD_ENTRY_MASTER A  INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO  INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = A.CATEGORY  INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = A.PRODUCT  INNER JOIN PDD_MASTER E ON E.PDD_ID = B.PDD_ITEM  LEFT JOIN EMP_MASTER F ON F.EMP_CODE = B.SO_ENTRY_BY" + strWhere).Tables(0)
            'DTEXCEL = DB.ExecuteDataSet("Select ROW_NUMBER() OVER (ORDER BY A.CREATED_DATE) AS [SL NO],A.WI_NO AS [WI NO], A.BRANCH_CODE AS [BRANCH CODE], A.BRANCH_NAME AS [BRANCH NAME], A.CUSTOMER_NAME AS [CUSTOMER NAME], A.LOAN_NUMBER AS [LOAN NUMBER], convert(varchar(10),A.DISBURSAL_DATE,103) AS [DISBURSAL DATE],CONVERT(VARCHAR(10),A.CREATED_DATE,103) AS [PDD ENTRY DATE], C.CATEGORY_NAME AS [CATEGORY], D.PRODUCT_NAME AS [PRODUCT], CASE WHEN B.PDD_ITEM = 1000 THEN B.PDD_OTHER ELSE E.PDD_NAME END AS [PDD], Case When B.TO_BE_UPDATED = 1 Then 'TO BE UPDATED' ELSE CONVERT(VARCHAR(10),B.DUE_DATE,103) END AS [DUE DATE],CASE WHEN B.STATUS IN (2,5) THEN 'APPROVER LEVEL PENDING' WHEN B.STATUS = 3 THEN 'PDD ENTRY TEAM LEVEL PENDING' WHEN B.STATUS IN (1,4,7) THEN 'SO LEVEL PENDING' WHEN B.STATUS = 6 THEN 'APPROVER LEVEL COMPLETED' END AS [CURRENT STATUS],CASE WHEN (SELECT COUNT(*) FROM PDD_ENTRY_SUB_CYCLE WHERE SUB_NO = B.SUB_NO AND STATUS IN (3,7)) > 0 THEN 'RETURNED' ELSE 'NEW' END AS [RETURNED / NEW], STUFF((SELECT DISTINCT ', '+EM.EMP_NAME FROM PDD_SO_MAPPING PSM INNER JOIN EMP_MASTER EM ON EM.EMP_CODE = PSM.EMP_CODE WHERE PSM.BRANCH_ID = A.BRANCH_CODE FOR XML PATH('')),1,1,'')+'' AS SO,B.SO_REMARKS AS [SO REMARKS], CONVERT(VARCHAR(10),B.SO_ENTRY_ON,103) AS [SO ENTRY DATE], B.APPROVED_REMARKS AS [APPROVER REMARKS], CONVERT(VARCHAR(10),B.APPROVED_ON,103) AS [APPROVER DATE]  FROM PDD_ENTRY_MASTER A  INNER JOIN PDD_ENTRY_SUB B ON A.WI_NO = B.WI_NO  INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = A.CATEGORY  INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = A.PRODUCT  INNER JOIN PDD_MASTER E ON E.PDD_ID = B.PDD_ITEM  LEFT JOIN EMP_MASTER F ON F.EMP_CODE = B.SO_ENTRY_BY" + strWhere).Tables(0)
            Dim Params(4) As SqlParameter
            Params(0) = New SqlParameter("@Emp_Code", SqlDbType.VarChar, 20)
            Params(0).Value = Session("UserID").ToString()
            Params(1) = New SqlParameter("@New_Cnt", SqlDbType.VarChar, 20)
            Params(1).Direction = ParameterDirection.Output
            Params(2) = New SqlParameter("@Return_Cnt", SqlDbType.VarChar, 20)
            Params(2).Direction = ParameterDirection.Output
            Params(3) = New SqlParameter("@Level", SqlDbType.Int)
            Params(3).Value = 2
            Params(4) = New SqlParameter("@Condition", SqlDbType.VarChar, 2000)
            Params(4).Value = wino + "|" + Branch + "|" + Category + "|" + Status + "|" + FromDt + "|" + ToDt
            Dim DS As DataSet = DB.ExecuteDataSet("SP_PDD_GET_SO_DATA", Params)
            DT = DS.Tables(0)
            DTEXCEL = DS.Tables(1)

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16, TRHead_17, TRHead_18, TRHead_19 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"
            TRHead_16.BorderWidth = "1"
            TRHead_17.BorderWidth = "1"
            TRHead_18.BorderWidth = "1"
            TRHead_19.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver
            TRHead_16.BorderColor = Drawing.Color.Silver
            TRHead_17.BorderColor = Drawing.Color.Silver
            TRHead_18.BorderColor = Drawing.Color.Silver
            TRHead_19.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid
            TRHead_16.BorderStyle = BorderStyle.Solid
            TRHead_17.BorderStyle = BorderStyle.Solid
            TRHead_18.BorderStyle = BorderStyle.Solid
            TRHead_19.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True
            TRHead_06.Font.Bold = True
            TRHead_07.Font.Bold = True
            TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True
            TRHead_10.Font.Bold = True
            TRHead_11.Font.Bold = True
            TRHead_12.Font.Bold = True
            TRHead_13.Font.Bold = True
            TRHead_14.Font.Bold = True
            TRHead_15.Font.Bold = True
            TRHead_16.Font.Bold = True
            TRHead_17.Font.Bold = True
            TRHead_18.Font.Bold = True
            TRHead_19.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 2, 2, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 3, 3, "c", "Work Item")
            RH.AddColumn(TRHead, TRHead_02, 3, 3, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_03, 5, 5, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_04, 5, 5, "c", "Customer Name")
            RH.AddColumn(TRHead, TRHead_05, 6, 6, "c", "Loan Number")
            RH.AddColumn(TRHead, TRHead_06, 4, 4, "c", "Disbursal Date")
            RH.AddColumn(TRHead, TRHead_07, 6, 6, "c", "Category")
            RH.AddColumn(TRHead, TRHead_08, 6, 6, "c", "Product")
            RH.AddColumn(TRHead, TRHead_09, 6, 6, "c", "PDD Item")
            RH.AddColumn(TRHead, TRHead_10, 4, 4, "c", "Due Date")
            RH.AddColumn(TRHead, TRHead_11, 5, 5, "c", "PDD Entry Date")
            RH.AddColumn(TRHead, TRHead_12, 6, 6, "c", "Current Status")
            RH.AddColumn(TRHead, TRHead_13, 6, 6, "c", "Returned/New")
            RH.AddColumn(TRHead, TRHead_14, 6, 6, "c", "SO")
            RH.AddColumn(TRHead, TRHead_15, 7, 7, "c", "SO Remarks")
            RH.AddColumn(TRHead, TRHead_16, 5, 5, "c", "SO Entry Date")
            RH.AddColumn(TRHead, TRHead_17, 7, 7, "c", "Approver Remarks")
            RH.AddColumn(TRHead, TRHead_18, 4, 4, "c", "Approver Date")
            RH.AddColumn(TRHead, TRHead_19, 4, 4, "c", "View Detail")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17, TR3_18, TR3_19 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"
                TR3_18.BorderWidth = "1"
                TR3_19.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver
                TR3_18.BorderColor = Drawing.Color.Silver
                TR3_19.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid
                TR3_18.BorderStyle = BorderStyle.Solid
                TR3_19.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 2, 2, "c", i)
                RH.AddColumn(TR3, TR3_01, 3, 3, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 3, 3, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 5, 5, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 5, 5, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 6, 6, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 4, 4, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_07, 6, 6, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_08, 6, 6, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_09, 6, 6, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_10, 4, 4, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_11, 5, 5, "l", DR(16).ToString())
                RH.AddColumn(TR3, TR3_12, 6, 6, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_13, 6, 6, "l", DR(17).ToString())
                RH.AddColumn(TR3, TR3_14, 6, 6, "l", DR(18).ToString())
                RH.AddColumn(TR3, TR3_15, 7, 7, "l", DR(11).ToString())
                RH.AddColumn(TR3, TR3_16, 5, 5, "l", DR(12).ToString())
                RH.AddColumn(TR3, TR3_17, 7, 7, "l", DR(13).ToString())
                RH.AddColumn(TR3, TR3_18, 4, 4, "l", DR(14).ToString())
                RH.AddColumn(TR3, TR3_19, 4, 4, "l", "<a href='viewWIDetails.aspx?WI=" + GN.Encrypt(DR(0).ToString()) + "&pdd=" + GN.Encrypt(DR(15).ToString()) + "' target='_blank'>" + "View Detail")


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Status Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            Dim report = Me.hdnReportID.Value
            HeaderText = "Status Report"
            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
