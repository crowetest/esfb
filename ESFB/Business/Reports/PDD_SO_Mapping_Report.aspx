﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ESFB.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="PDD_SO_Mapping_Report.aspx.vb" Inherits="PDD_SO_Mapping_Report"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/ESFB.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" Runat="Server">

<br />
<table align="center" style="width: 40%; text-align:center; margin:0px auto;">
    <tr>
        <td style="width:25%;">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        </td>
        <td  style="width:25%;">
        <br /><br />
        </td>
        <td  style="width:25%;"></td>
        <td  style="width:25%;"></td>
    </tr>      
    <tr>           
        <td style="width:25%;text-align:right;">
            Branch :
        </td>
        <td style="width:25%;text-align:left;" colspan="3">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="cmbBranch" class="NormalText" runat="server" Font-Names="Cambria" Width="61%" ForeColor="Black">
                <asp:ListItem Value="-1"> ---------Select---------</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>   
    <tr>
        <td style="width:25%;text-align:left;"></td>
        <td style="width:25%;text-align:center;" colspan="2">
            <br /><br />        
            <input id="btnView" style="font-family: Cambria; cursor: pointer; width: 20%" type="button" value="VIEW"   onclick="return btnView_onclick()" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input id="btnExit" style="font-family: Cambria; font-size: 10pt; width: 20%" type="button" value="EXIT"  onclick="return btnExit_onclick()" />
        </td>
        <td style="width:100%;height:18px;text-align:center;"></td>
    </tr>       
    <tr>
        <td style="width:25%;">&nbsp;</td>
        <td style="width:25%;">
            <asp:HiddenField ID="hdnReportID" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="width:25%;">&nbsp;</td>
        <td  style="width:25%;">               
            <asp:HiddenField ID="hdnValue" runat="server" />
        </td>               
    </tr>
</table>
<script language="javascript" type="text/javascript">
    function FromServer(arg, context) {
                
    }
    function ComboFill(data, ddlName) {
        document.getElementById(ddlName).options.length = 0;
        var rows = data.split("Ñ");
        for (a = 1; a < rows.length; a++) {
            var cols = rows[a].split("ÿ");
            var option1 = document.createElement("OPTION");
            option1.value = cols[0];
            option1.text = cols[1];
            document.getElementById(ddlName).add(option1);
        }
    }
    function btnView_onclick() {
        var Branch = document.getElementById("<%= cmbBranch.ClientID %>").value;
        window.open("View_PDD_SO_Mapping_Report.aspx?Branch=" + btoa(Branch) , "_blank");
    }   
    function btnExit_onclick() {
        window.open("<%= Page.ResolveUrl("~")%>Home.aspx","_self");
    }  
</script>
</asp:Content>

