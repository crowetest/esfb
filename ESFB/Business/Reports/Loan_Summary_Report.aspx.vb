﻿Option Strict On
Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Loan_Report
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim DT As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim DT3 As New DataTable
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim BranchID As Integer
    Dim State_ID As Integer

    Dim UserID As Integer
    Dim GMASTER As New Master
    Dim CallBackReturn As String = Nothing
    Dim AdminFlag As Boolean = False
    Dim WebTools As New WebApp.Tools
    Dim AD As New Audit
    Dim PostID As Integer

   

#Region "Page Load & Dispose"
    Private _dataRow As Object

    Private Property DataRow(p1 As Object) As Object
        Get
            Return _dataRow
        End Get
        Set(value As Object)
            _dataRow = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GF.FormAccess(CInt(Session("UserID")), 1170) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Dim strWhere As String = ""
            Dim strWhere1 As String = ""
            BranchID = CInt(Session("BranchID"))
            UserID = CInt(Session("UserID"))
            Me.Master.subtitle = "Loan Summary Report"
            DT = GMASTER.GetEmpRoleList(UserID)
            For Each DR As DataRow In DT.Rows
                If DR(2).ToString() = "40" Then
                    AdminFlag = True
                    Exit For
                End If

            Next



            If Not IsPostBack Then
            End If
            '
            '--//---------- Script Registrations -----------//--
            '/--- For Call Back ---//
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "FromServer", "context", True)
            Dim cbscript As String = "function ToServer (arg,context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            'GF.ComboFill(cmbState, MAST.GetState(), 0, 1)

            'Me.cmbStatus.Attributes.Add("onchange", "StatusOnChange()")
            btnView.Attributes.Add("onclick", "return ViewOnClick()")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        DT.Dispose()  
        DB.dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function
    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        If CInt(Data(0)) = 1 Then
        End If
    End Sub
#End Region
#Region "Events"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        Try
            Dim Arr() As String = hidData.Value.Split(CChar("ÿ"))
            Dim FromDt As DateTime = Convert.ToDateTime("2019-04-01 00:00:00")
            Dim ToDt As DateTime = Date.Today()
            If Arr(0) <> "" Then
                FromDt = CDate((Arr(0)))
            End If
            If Arr(1) <> "" Then
                ToDt = CDate(Arr(1))
            End If
            Dim SqlStr As String = ""
            Dim SqlStr1 As String = ""
            Dim DateVal As String = ToDt.Day().ToString()
            Dim MonthVal As String = ToDt.Month.ToString()
            Dim YearVal As String = ToDt.Year.ToString()
            Dim StrDateWhere As String

            StrDateWhere = " 1 = 1 "
            Dim FromDateDt As String = FromDt.ToString("yyyy-MM-dd")
            Dim ToDateDt As String = ToDt.ToString("yyyy-MM-dd")
            If FromDateDt <> "" And ToDateDt <> "" Then
                StrDateWhere += " and  cast(Enquiry_Date as date) >='" & FromDateDt & "' and  cast(Enquiry_Date as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt <> "" Then
                StrDateWhere += " and cast(Enquiry_Date as date) <='" & ToDateDt & "' "
            End If
            If FromDateDt <> "" And ToDateDt = "" Then
                StrDateWhere += " and  cast(Enquiry_Date as date) >='" & FromDateDt & "' "
            End If
            If FromDateDt = "" And ToDateDt = "" Then
                StrDateWhere += ""
            End If
            'SqlStr = "select ROW_NUMBER() OVER (ORDER BY ln.Enquiry_ID) AS Sl_No,sm.State_Name as State,dm.District_Name as District, bm.Branch_Name as Branch_Name,bm.branch_id as  " & _
            '        " Branch_ID,  convert(varchar,ln.Branch_ID)+'/'+  RIGHT('00' + CAST(DATEPART(mm,ln.enquiry_date) AS varchar(2)), 2)+'-'+RIGHT(CONVERT(VARCHAR(8),ln.enquiry_date, 1),2) " & _
            '        " +'/'  +convert(varchar,ln.Enquiry_ID)  as Lead_Reference_No,ln.name as Name, ln.address as Place_Name,ln.mobile  as Mobile_No,ln.Altmobile as Alternative_Phone_Number, " & _
            '        " case when ln.Loan_Purpose='1' then  'Housing Loan'  when ln.Loan_Purpose='2' then 'Agri Loan' when ln.Loan_Purpose='3' then 'Business Loan' when ln.Loan_Purpose='4'   " & _
            '        " then 'LAP' when ln.Loan_Purpose='5'  then '2 wheeler loan' when ln.Loan_Purpose='6'  then '3&4 wheeler loan' when ln.Loan_Purpose='7' then 'Gold Loan' when  " & _
            '        " ln.Loan_Purpose='8' then 'MSME Loan'  end as Loan_Product, ln.Loan_Amount as  Loan_Amount,isnull(ln.remarks,'') as Remarks,case ln.Sangam_ID when 0 then null " & _
            '        " else ln.Sangam_ID end  as Sangam_ID,ln.sangam_name  as Sangam_Name,ln.Ref_Emp_Name  as Referred_By_Emp_Name, case ln.ref_emp_code when 0 then null else ln.ref_emp_code  " & _
            '        " end  as Referred_By_Emp_code,ln.Ref_Contact  as  Referred_By_Contact_NO ,em.Emp_Name as  Entered_by_Emp_Name,ln.Enquiry_userId  as Entered_by_Emp_code,  " & _
            '        "  CONVERT(VARCHAR(10),ln.enquiry_date,105) as Entered_by_Date,isnull(CONVERT(VARCHAR(8), ln.Enquiry_Date, 108),'')  [Entered_by_Time] , isnull( emm.emp_name,'') as  " & _
            '        " Verified_by_Emp_Name,case when ln.response_userID=0 then null else ln.response_userID end as Verified_by_Emp_Code,case  when ln.status_id is null  then '' when " & _
            '        " ln.status_id=1  then 'Non contactable' when   ln.status_id=2 then  'On Hold' when ln. status_id=3  then 'Need more information'   when  ln.status_id=4 then  " & _
            '        " 'Approved for Processing' when  ln.status_id=5 then 'Rejected by HQ ' when  ln.status_id=6 then 'Sanctioned by HQ ' when  ln.status_id=7 then 'Return by HQ ' end as HO_Status ,isnull(ln.explanation,'')  as Explanation, isnull(CONVERT(VARCHAR(10),ln.Response_Date,105),'') " & _
            '        "  as HO_Response_Date,isnull( CONVERT(VARCHAR(8), ln.Response_Date, 108),'') [HO_Response_Time] ,DATEDIFF (d,ln.enquiry_date, ln.Response_Date) as HO_TAT, case when Assigned_Credit_officer=-1 then null else  Assigned_Credit_officer end " & _
            '        " as Assigned_CO_Emp_code,isnull(emmm.emp_name,'')as Assigned_CO_Emp_Name, case  Officers_Status when 1 then 'Televerification Done'  when 2 then 'Branch visit by Customer' when 3 then  'Customer place visit done' when 4 then  " & _
            '        " 'Document pending from Customer' when 5 then'Submitted for Sanction' when 6 then 'Sanctioned' when 7 then 'Rejected by CO' else '' end  as CO_Status, " & _
            '     " isnull(Officers_Remarks,'') as CO_Remarks, isnull(CONVERT(VARCHAR(10), ln.Officers_Response_Date,105),'') as Last_Updated_Date,isnull(CONVERT(VARCHAR(8),  " & _
            '     " ln.Officers_Response_Date, 108),'') [Last_Updated_Time], isnull(DATEDIFF (d,ln.enquiry_date, ln.Officers_Response_Date),0)   as CO_TAT from  loan_enquiry ln  inner join  " & _
            '     " branch_master bm  on ln.branch_id=bm.branch_id  inner join state_master sm on sm.State_ID=bm.State_ID  inner join  " & _
            '     " DISTRICT_MASTER dm on dm.District_ID=bm.District_ID  inner join emp_master em on ln.Enquiry_UserID=em.Emp_Code left join emp_master emmm on  " & _
            '     "ln.Assigned_Credit_Officer=emmm.Emp_Code left join emp_master emm on  ln.Response_UserID = emm.Emp_Code   " & _
            '        " where  " + StrDateWhere

            'SqlStr = "select convert(varchar,ROW_NUMBER() OVER (order BY (A.CallsMadeBy))) as SLNO,A.CallsMadeBy,convert(varchar,sum(A.day)) as Day ,convert(varchar,sum(A.month)) as Month,convert(varchar,sum(A.cumulative)) as Cumulative from " & _
            '         "(select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '         "as Sl_No,case  when status_id is null or status_id=-1  then 'Status Not Updated' when " & _
            '         "status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '         " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ ' " & _
            '         " end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,convert(varchar,'') as cumulative from loan_enquiry group by status_id " & _
            '         " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '        " as Sl_No,case  when status_id is null or status_id=-1  then 'Status Not Updated' when " & _
            '        " status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '        " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ ' " & _
            '        " end as CallsMadeBy, count(enquiry_id) as Day,convert(varchar,'') as month,convert(varchar,'') as cumulative from loan_enquiry " & _
            '        " where year(Enquiry_Date) =" + YearVal + "And Month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) = " + DateVal & _
            '        " group by status_id " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '        " as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when " & _
            '        " status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '        " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ ' " & _
            '        " end as CallsMadeBy, convert(varchar,'') as day,count(enquiry_id) as month,convert(varchar,'') as cumulative from loan_enquiry " & _
            '        " where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <= " + DateVal & _
            '        " group by status_id " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '        " as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when " & _
            '        " status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '        " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ' " & _
            '        " end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,count(enquiry_id) as cumulative from loan_enquiry " & _
            '        " where" + StrDateWhere & _
            '        " group by status_id " & _
            '        " )A " & _
            '        " group by A.CallsMadeBy union all " & _
            '        " select '' as SLNO,'Total' as CallsMadeBy,convert(varchar,sum(B.day)) as day,convert(varchar,sum(B.Month)) as month,convert(varchar,sum(B.cumulative)) as cumulative from " &
            '        " (select convert(varchar,ROW_NUMBER() OVER (order BY (A.CallsMadeBy))) as SLNo,A.CallsMadeBy,sum(A.day) as day ,sum(A.month) as month,sum(A.cumulative) as cumulative from " & _
            '         "(select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '         "as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when " & _
            '         "status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '         " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ ' " & _
            '         " end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,convert(varchar,'') as cumulative from loan_enquiry group by status_id " & _
            '         " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '        " as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when " & _
            '        " status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '        " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ ' " & _
            '        " end as CallsMadeBy,count(enquiry_id) as Day,convert(varchar,'') as month,convert(varchar,'') as cumulative from loan_enquiry " & _
            '        " where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "and day(enquiry_date)=" + DateVal & _
            '        " group by status_id " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '        " as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when " & _
            '        " status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '        " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ ' " & _
            '        " end as CallsMadeBy, convert(varchar,'') as day,count(enquiry_id) as month,convert(varchar,'') as cumulative from loan_enquiry " & _
            '        " where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <= " + DateVal & _
            '        " group by status_id " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) " & _
            '        " as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when " & _
            '        " status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then " & _
            '        " 'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ' " & _
            '        " end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,count(enquiry_id) as cumulative from loan_enquiry " & _
            '        " where" + StrDateWhere & _
            '        " group by status_id " & _
            '        " )A " & _
            '        " group by A.CallsMadeBy)B " & _
            '        " union all " & _
            '        " (select '' as Sl_No,'' as CallsMadeBy,'' as Day,'' as Month, " & _
            '        " '' as cumulative " & _
            '        " ) " & _
            '        " union all " & _
            '        " (select 'SLNO' as Sl_No,'Product Name' as CallsMadeBy,'Day' as Day,'Month' as Month, " & _
            '        " 'Cumulative' as cumulative " & _
            '        " ) " & _
            '        " union all " & _
            '        " (select convert(varchar,ROW_NUMBER() OVER (order BY (A.Product_Name))) as SLNo,A.Product_Name, " & _
            '        " convert(varchar,sum(A.day)) as day ,convert(varchar,sum(A.month)) as month,convert(varchar,sum(A.cumulative)) as cumulative " & _
            '        " from " & _
            '        " (select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Day,convert(varchar,'') as Month, " & _
            '        " convert(varchar,'') as cumulative from loan_enquiry group by Loan_Purpose " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'others' end as Product_Name,count(Loan_Purpose) as Day,convert(varchar,'') as Month, " & _
            '        " convert(varchar,'') as cumulative from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "and day(enquiry_date)=" + DateVal & _
            '        " group by Loan_Purpose " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan' when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Day,count(Loan_Purpose) as Month, " & _
            '        " '' as cumulative from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <= " + DateVal & _
            '        " group by Loan_Purpose " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Day,convert(varchar,'') as Month, " & _
            '        " count(Loan_Purpose) as cumulative from loan_enquiry where " + StrDateWhere & _
            '        " group by Loan_Purpose)A " & _
            '        " group by A.Product_Name) " & _
            '        " union all " & _
            '        " select '' as SLNO,'Total' as CallsMadeBy,convert(varchar,sum(D.dayy)) as day,convert(varchar,sum(D.Monthh)) as month,convert(varchar,sum(D.cumulativee)) as cumulative from " & _
            '        " (select convert(varchar,ROW_NUMBER() OVER (order BY (C.Product_Name))) as SLNo,C.Product_Name, " & _
            '        " sum(C.dayy) as dayy ,sum(C.monthh) as monthh,sum(C.cumulativee) as cumulativee " & _
            '        " from " & _
            '        " (select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Dayy,convert(varchar,'') as Monthh, " & _
            '        " convert(varchar,'') as cumulativee from loan_enquiry group by Loan_Purpose " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,count(Loan_Purpose) as dayy,convert(varchar,'') as Monthh, " & _
            '        " convert(varchar,'') as cumulativee from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "and day(enquiry_date)=" + DateVal & _
            '        " group by Loan_Purpose " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Dayy,count(Loan_Purpose) as Monthh, " & _
            '        " convert(varchar,'') as cumulativee from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <= " + DateVal & _
            '        " group by Loan_Purpose " & _
            '        " union all " & _
            '        " select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " & _
            '        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' " & _
            '        " when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' " & _
            '        " when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Dayy,convert(varchar,'') as Monthh, " & _
            '        " count(Loan_Purpose) as cumulativee from loan_enquiry where " + StrDateWhere & _
            '        " group by Loan_Purpose)C " & _
            '        " group by C.Product_Name)D " & _
            '        " union all " & _
            '        " (select '' as Sl_No,'' as CallsMadeBy,convert(varchar,'') as Day,convert(varchar,'') as Month,  convert(varchar,'') as cumulative) " & _
            '        " union all " & _
            '        " (select 'SLNO' as Sl_No,'Entered By Emp Name' as CallsMadeBy,convert(varchar,'Entered By Emp Code') as Day,convert(varchar,'Branch Name') as Month,  convert(varchar,'Total') as cumulative " & _
            '        " ) " & _
            '        " union all " & _
            '        " (select top 25 convert(varchar,ROW_NUMBER() OVER (order BY (count(Enquiry_UserID))desc)) as Sl_No, " & _
            '        " em.emp_Name as CallsMadeBy,convert(varchar,le.Enquiry_userid) as day, " & _
            '        " bm.branch_name as Month,convert(varchar,count(Enquiry_UserID)) as Cumulative from loan_enquiry le " & _
            '        " inner join emp_master em on em.emp_code=le.Enquiry_userid " & _
            '        " inner join branch_master bm on bm.branch_id=le.branch_id " & _
            '        " where year(enquiry_date)=" + YearVal + "and month(enquiry_date)=" + MonthVal + "And Day(enquiry_date) <= " + DateVal & _
            '        " group by Enquiry_UserID,em.emp_Name,le.Enquiry_userid,bm.branch_name) "
            'DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            'WebTools.ExporttoExcel(DT, "Loan_Lead_Report")
            SqlStr1 = " With Pivoted  AS " +
                        " (select * from " +
                        " (select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No, " +
                        " case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan'  when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP'  when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'   when Loan_Purpose='9' then  'Dream Housing Loan' else 'others' end as product_name, " +
                        " case  when status_id is null or status_id=-1  then 'Status Not Updated'   when status_id=1  then 'Non contactable' when status_id=2 then  'On Hold'  when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing'  when status_id=5 then 'Rejected by HQ' when status_id=6 then 'Sanctioned by HQ'  when  status_id=7 then 'Return by HQ'  end as CallsMadeBy, " +
                        " count(status_id) as countvalue " +
                        " from loan_enquiry where" + StrDateWhere + "group by Loan_Purpose,status_id)x " +
                        " pivot(  max(countvalue)  for CallsMadeBy in ([Status Not Updated],[Non contactable],[On Hold],[Need more information],[Approved for Processing], " +
                        " [Rejected by HQ],[Sanctioned by HQ],[Return by HQ]))p) " +
                        " SELECT  convert(varchar,ROW_NUMBER() OVER (order BY (product_name))) as Sl_No, " +
                        " product_name, " +
                        " isnull(convert(varchar,MAX([Status Not Updated])),0) AS [Status Not Updated], " +
                        " isnull(convert(varchar,MAX([Non contactable])),0) AS [Non contactable], " +
                        " isnull(convert(varchar,MAX([On Hold])),0) AS [On Hold], " +
                        " isnull(convert(varchar,MAX([Need more information])),0) AS [Need more information], " +
                        " isnull(convert(varchar,MAX([Approved for Processing])),0) AS [Approved for Processing], " +
                        " isnull(convert(varchar,MAX([Rejected by HQ])),0) AS [Rejected by HQ], " +
                        " isnull(convert(varchar,MAX([Sanctioned by HQ])),0) AS [Sanctioned by HQ], " +
                        " isnull(convert(varchar,MAX([Return by HQ])),0) AS [Return by HQ], " +
                        " isnull(convert(varchar,sum(isnull([Non contactable],0))+sum(isnull([On Hold],0))+sum(isnull([Need more information],0))+sum(isnull([Approved for Processing],0))+sum(isnull([Rejected by HQ],0))+sum(isnull([Sanctioned by HQ],0))+sum(isnull([Return by HQ],0)),0),0) as [Total] " +
                        " FROM Pivoted " +
                        " GROUP BY product_name " +
                        " union all  select '' as Sl_no,'Total' as product_name, " +
                        " isnull(convert(varchar,sum([Status Not Updated])),0) as [Status Not Updated], " +
                        " isnull(convert(varchar,sum([Non contactable])),0) as [Non contactable], " +
                        " isnull(convert(varchar,sum([On Hold])),0) as [On Hold], " +
                        " isnull(convert(varchar,sum([Need more information])),0) as [Need more information], " +
                        " isnull(convert(varchar,sum([Approved for Processing])),0) as [Approved for Processing], " +
                        " isnull(convert(varchar,sum([Rejected by HQ])),0) as [Rejected by HQ], " +
                        " isnull(convert(varchar,sum([Sanctioned by HQ])),0) as [Sanctioned by HQ], " +
                        " isnull(convert(varchar,sum([Return by HQ])),0) as [Return by HQ], " +
                        " isnull(convert(varchar,sum(isnull([Non contactable],0))+sum(isnull([On Hold],0))+sum(isnull([Need more information],0))+sum(isnull([Approved for Processing],0))+sum(isnull([Rejected by HQ],0))+sum(isnull([Sanctioned by HQ],0))+sum(isnull([Return by HQ],0)),0),0) as [Total] " +
                        " from Pivoted " +
                        " union all " +
                        " (select '' as Sl_No,'' as CallsMadeBy,'' as Day,'' as Month,  '' as cumulative,'' as [Need more information],'' as [Approved for processing], " +
                        " '' as [Rejected by HQ],'' as [Sanctioned by HQ],'' as [Return by HQ], " +
                        " convert(varchar,'') as [Total]) " +
                        " union all " +
                        " select 'Sl_No' as SLNO, 'CallsMadeBy' as CallsMadeBy,'Day' as Day,'Month' as Month,'Cumulative' as Cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] " +
                        " union all " +
                        " select convert(varchar,ROW_NUMBER() OVER (order BY (A.CallsMadeBy))) as SLNO,A.CallsMadeBy,convert(varchar,sum(A.day)) as Day ,convert(varchar,sum(A.month)) as Month,convert(varchar,sum(A.cumulative)) as Cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] " +
                        " from (select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) as Sl_No,case  when status_id is null or status_id=-1  then 'Status Not Updated' when status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ '  end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry group by status_id " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (status_id)))  as Sl_No,case  when status_id is null or status_id=-1  then 'Status Not Updated' when  status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ '  end as CallsMadeBy, count(enquiry_id) as Day,convert(varchar,'') as month,convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry  where year(Enquiry_Date) =" + YearVal + "And Month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) =" + DateVal + "group by status_id " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (status_id)))  as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when  status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ '  end as CallsMadeBy, convert(varchar,'') as day,count(enquiry_id) as month,convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry  where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <=" + DateVal + "group by status_id " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (status_id)))  as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when  status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ'  end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,count(enquiry_id) as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry  where" + StrDateWhere + "group by status_id  )A  group by A.CallsMadeBy " +
                        " union all  select '' as SLNO,'Total' as CallsMadeBy,convert(varchar,sum(B.day)) as day,convert(varchar,sum(B.Month)) as month,convert(varchar,sum(B.cumulative)) as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from  (select convert(varchar,ROW_NUMBER() OVER (order BY (A.CallsMadeBy))) as SLNo,A.CallsMadeBy,sum(A.day) as day ,sum(A.month) as month,sum(A.cumulative) as cumulative,'' as [Need more information],'' as [Approved for processing],'' as [Rejected by HQ],'' as 'Sanctioned by HQ','' as 'Return by HQ' from (select convert(varchar,ROW_NUMBER() OVER (order BY (status_id))) as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ '  end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,convert(varchar,'') as cumulative, " +
                        " '' as [Need more information],'' as [Approved for Processing],'' as [Rejected by HQ],'' as [Sanctioned by HQ],'' as [Return by HQ],convert(varchar,'') as [Total] from loan_enquiry group by status_id " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (status_id)))  as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when  status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ '  end as CallsMadeBy,count(enquiry_id) as Day,convert(varchar,'') as month,convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry  where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "and day(enquiry_date)=" + DateVal + "group by status_id " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (status_id)))  as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when  status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ '  end as CallsMadeBy, convert(varchar,'') as day,count(enquiry_id) as month,convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry  where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <=" + DateVal + "group by status_id " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (status_id)))  as Sl_No,case  when status_id is null or status_id=-1 then 'Status Not Updated' when  status_id=1  then 'Non contactable' when status_id=2 then  'On Hold' when status_id=3  then 'Need more information'   when status_id=4 then  'Approved for Processing' when status_id=5 then 'Rejected by HQ ' when status_id=6 then 'Sanctioned by HQ ' when  status_id=7 then 'Return by HQ'  end as CallsMadeBy, convert(varchar,'') as day,convert(varchar,'') as month,count(enquiry_id) as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry  where" + StrDateWhere + "group by status_id  )A  group by A.CallsMadeBy)B " +
                        " union all  (select '' as Sl_No,'' as CallsMadeBy,'' as Day,'' as Month,  '' as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total]  ) " +
                        " union all  (select 'SLNO' as Sl_No,'Product Name' as CallsMadeBy,'Day' as Day,'Month' as Month,  'Cumulative' as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total]  ) " +
                        " union all  (select convert(varchar,ROW_NUMBER() OVER (order BY (A.Product_Name))) as SLNo,A.Product_Name,  convert(varchar,sum(A.day)) as day ,convert(varchar,sum(A.month)) as month,convert(varchar,sum(A.cumulative)) as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total]  from  (select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Day,convert(varchar,'') as Month,  convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry group by Loan_Purpose " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'others' end as Product_Name,count(Loan_Purpose) as Day,convert(varchar,'') as Month,  convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "and day(enquiry_date)=" + DateVal + "group by Loan_Purpose " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan' when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Day,count(Loan_Purpose) as Month,  '' as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <=" + DateVal + "group by Loan_Purpose " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Day,convert(varchar,'') as Month,  count(Loan_Purpose) as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry where" + StrDateWhere + "group by Loan_Purpose)A  group by A.Product_Name) " +
                        " union all  select '' as SLNO,'Total' as CallsMadeBy,convert(varchar,sum(D.dayy)) as day,convert(varchar,sum(D.Monthh)) as month,convert(varchar,sum(D.cumulativee)) as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total] from  (select convert(varchar,ROW_NUMBER() OVER (order BY (C.Product_Name))) as SLNo,C.Product_Name,  sum(C.dayy) as dayy ,sum(C.monthh) as monthh,sum(C.cumulativee) as cumulativee,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total] from  (select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,convert(varchar,'') as Dayy,convert(varchar,'') as Monthh,  convert(varchar,'') as cumulativee,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ',convert(varchar,'') as [Total] from loan_enquiry group by Loan_Purpose " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,count(Loan_Purpose) as dayy,convert(varchar,'') as Monthh,  convert(varchar,'') as cumulativee,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total] from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "and day(enquiry_date)=" + DateVal + "group by Loan_Purpose " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan'  when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP'  when Loan_Purpose='5'  then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,  convert(varchar,'') as Dayy,count(Loan_Purpose) as Monthh,  convert(varchar,'') as cumulativee,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total] from loan_enquiry where year(Enquiry_Date) =" + YearVal + "and month(Enquiry_Date) =" + MonthVal + "And Day(enquiry_date) <=" + DateVal + "group by Loan_Purpose " +
                        " union all  select convert(varchar,ROW_NUMBER() OVER (order BY (Loan_Purpose))) as Sl_No,  case when Loan_Purpose='1' then  'Micro Housing Loan'  when Loan_Purpose='2' then 'Agri Loan'  when Loan_Purpose='3' then 'Business Loan'  when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5'  then '2 wheeler loan'  when Loan_Purpose='6' then '3&4 wheeler loan'  when Loan_Purpose='7' then 'Gold Loan'  when Loan_Purpose='8' then 'MSME Loan'  when Loan_Purpose='9' then  'Dream Housing Loan' else 'Others' end as Product_Name,  convert(varchar,'') as Dayy,convert(varchar,'') as Monthh,  count(Loan_Purpose) as cumulativee,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total] from loan_enquiry where" + StrDateWhere + "group by Loan_Purpose)C  group by C.Product_Name)D " +
                        " union all  (select '' as Sl_No,'' as CallsMadeBy,convert(varchar,'') as Day,convert(varchar,'') as Month,  convert(varchar,'') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total]) " +
                        " union all  (select 'SLNO' as Sl_No,'Entered By Emp Name' as CallsMadeBy,convert(varchar,'Entered By Emp Code') as Day,  convert(varchar,'Branch Name') as Month,  convert(varchar,'Total') as cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total]  ) " +
                        " union all  (select top 25 convert(varchar,ROW_NUMBER() OVER (order BY (count(Enquiry_UserID))desc)) as Sl_No,  em.emp_Name as CallsMadeBy,convert(varchar,le.Enquiry_userid) as day,  bm.branch_name as Month,  convert(varchar,count(Enquiry_UserID)) as Cumulative,'' as 'Need more information','' as 'Approved for processing','' as 'Rejected by HQ','' as 'Sanctioned by HQ','' as 'Return by HQ' ,convert(varchar,'') as [Total] from loan_enquiry le  inner join emp_master em on em.emp_code=le.Enquiry_userid  inner join branch_master bm on bm.branch_id=le.branch_id  where(Year(enquiry_date) =" + YearVal + "And Month(enquiry_date) =" + MonthVal + "And Day(enquiry_date) <=" + DateVal + ")  group by Enquiry_UserID,em.emp_Name,le.Enquiry_userid,bm.branch_name)"
            DT1 = DB.ExecuteDataSet(SqlStr1).Tables(0)
            DT1.Merge(DT2)
            WebTools.ExporttoExcel(DT1, "Loan_Lead_Summary_Report")
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GF.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
#End Region

    

End Class
