﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class View_PDD_SO_Mapping_Report
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim DT As New DataTable
    Dim DTEXCEL As New DataTable
    Dim DT1 As New DataTable
    Dim DT2 As New DataTable
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim PostID As Integer
    Dim GN As New GeneralFunctions
    Dim WebTools As New WebApp.Tools
    Dim TraDt As Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim DT As New DataTable
            Dim Branch As String = GN.Decrypt(Request.QueryString.Get("Branch"))
            Dim strWhere As String = " WHERE 1 = 1 AND A.STATUS = 1 "


            Dim SO As Integer = 0
            Dim ASM As Integer = 0
            Dim CH As Integer = 0
            DT = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE = " + Session("UserID")).Tables(0)
            If DT.Rows.Count > 0 Then
                If CInt(DT.Rows(0)(0).ToString()) = 2 Then
                    SO = 1
                End If
            End If
            Dim j As Integer = 0
            DT1 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + ")").Tables(0)
            If DT1.Rows.Count > 0 Then
                For Each dr1 In DT1.Rows
                    If CInt(DT1.Rows(j)(0)) = 2 Then
                        ASM = 1
                    End If
                Next
            End If
            DT2 = DB.ExecuteDataSet("SELECT TEAM_ID FROM PDD_TEAM_DTL WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + "))").Tables(0)
            If DT2.Rows.Count > 0 Then
                For Each dr1 In DT2.Rows
                    If CInt(DT2.Rows(j)(0)) = 2 Then
                        CH = 1
                    End If
                Next
            End If


            If CInt(Branch) <> -1 Then
                strWhere += " AND A.BRANCH_ID = " + Branch
            Else
                If SO = 1 Then
                    strWhere += " AND A.BRANCH_ID IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE =" + Session("UserID") + ")"
                ElseIf ASM = 1 Then
                    strWhere += " AND A.BRANCH_ID IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + "))"
                ElseIf CH = 1 Then
                    strWhere += " AND A.BRANCH_ID IN (SELECT BRANCH_ID FROM PDD_SO_MAPPING WHERE EMP_CODE IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO IN (SELECT EMP_CODE FROM EMP_MASTER WHERE REPORTING_TO = " + Session("UserID") + ")))"
                End If
            End If



            DT = DB.ExecuteDataSet("SELECT A.EMP_CODE,B.EMP_NAME,A.BRANCH_ID,C.BRANCH_NAME FROM PDD_SO_MAPPING A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE INNER JOIN BRANCH_MASTER C ON C.BRANCH_ID = A.BRANCH_ID " + strWhere).Tables(0)
            DTEXCEL = DB.ExecuteDataSet("SELECT A.EMP_CODE AS [EMP CODE],B.EMP_NAME AS [EMP NAME],A.BRANCH_ID AS [BRANCH CODE],C.BRANCH_NAME AS [BRANCH NAME] FROM PDD_SO_MAPPING A INNER JOIN EMP_MASTER B ON A.EMP_CODE = B.EMP_CODE INNER JOIN BRANCH_MASTER C ON C.BRANCH_ID = A.BRANCH_ID " + strWhere).Tables(0)

            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16, TRHead_17 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"
            TRHead_16.BorderWidth = "1"
            TRHead_17.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver
            TRHead_16.BorderColor = Drawing.Color.Silver
            TRHead_17.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid
            TRHead_16.BorderStyle = BorderStyle.Solid
            TRHead_17.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True
            TRHead_06.Font.Bold = True
            TRHead_07.Font.Bold = True
            TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True
            TRHead_10.Font.Bold = True
            TRHead_11.Font.Bold = True
            TRHead_12.Font.Bold = True
            TRHead_13.Font.Bold = True
            TRHead_14.Font.Bold = True
            TRHead_15.Font.Bold = True
            TRHead_16.Font.Bold = True
            TRHead_17.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 10, 10, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 15, 15, "c", "Emp Code")
            RH.AddColumn(TRHead, TRHead_02, 30, 30, "c", "Employee Name")
            RH.AddColumn(TRHead, TRHead_03, 15, 15, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_04, 30, 30, "c", "Branch Name")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 10, 10, "c", i)
                RH.AddColumn(TR3, TR3_01, 15, 15, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 30, 30, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 15, 15, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 30, 30, "l", DR(3).ToString())


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=ACReasonsReport.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub cmd_Export_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export_Excel.Click
        Try
            Dim HeaderText As String
            'Dim DT As New DataTable
            HeaderText = "Status Report"

            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
    Protected Sub Export_Excel_Click()
        Try
            Dim HeaderText As String
            Dim report = Me.hdnReportID.Value
            HeaderText = "Status Report"
            WebTools.ExporttoExcel(DTEXCEL, HeaderText)
        Catch ex As Exception
            Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
            Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
        End Try
    End Sub
End Class
