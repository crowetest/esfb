﻿Imports System.Data
Imports System.IO
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Partial Class viewWIDetails
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim RH As New WholeHelper.ClsRepCtrl
    Dim tb As New Table
    Dim WI As String
    Dim PDD As String
    Dim AD As New Audit
    Dim GN As New GeneralFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            cmd_Print.Attributes.Add("onclick", "return PrintPanel()")
            cmd_Back.Attributes.Add("onclick", "return Exitform()")
            WI = GN.Decrypt(Request.QueryString.Get("WI"))
            PDD = GN.Decrypt(Request.QueryString.Get("pdd"))
            Dim DT As New DataTable
            Dim SqlStr As String
            'SqlStr = "SELECT A.WI_NO,F.BRANCH_CODE, F.BRANCH_NAME, F.CUSTOMER_NAME, F.LOAN_NUMBER, F.DISBURSAL_DATE, C.CATEGORY_NAME, D.PRODUCT_NAME, CASE WHEN A.PDD_ITEM = 1000 THEN A.PDD_OTHER ELSE B.PDD_NAME END AS PDD, CASE WHEN A.TO_BE_UPDATED = 0 THEN CONVERT(VARCHAR(10),A.DUE_DATE,103) ELSE 'TO BE UPDATED' END AS DUE_DATE,CONVERT(VARCHAR(10),A.DISB_ON,103) AS [DISB DATE], CASE WHEN B.STATUS IN (2,5) THEN 'APPROVER LEVEL PENDING' WHEN B.STATUS = 3 THEN 'PDD ENTRY TEAM LEVEL PENDING' WHEN B.STATUS IN (1,4,7) THEN 'SO LEVEL PENDING' WHEN B.STATUS = 6 THEN 'APPROVER LEVEL COMPLETED' END AS [CURRENT STATUS], A.SO_REMARKS, CONVERT(VARCHAR(10),SO_ENTRY_ON,103) AS [SO ENTRY DATE],CASE WHEN A.APPROVED_STATUS = 1 THEN 'APPROVED' WHEN A.APPROVED_STATUS = 2 THEN 'RETURNED' WHEN A.APPROVED_STATUS = 3 THEN 'REJECTED' ELSE 'NOT APPROVED' END AS APPROVED_STATUS, A.APPROVED_REMARKS, CONVERT(VARCHAR(10),A.APPROVED_ON,103) AS APPROVED_DATE FROM PDD_ENTRY_SUB_CYCLE A LEFT JOIN PDD_MASTER B ON B.PDD_ID = A.PDD_ITEM INNER JOIN PDD_ENTRY_MASTER F ON F.WI_NO = A.WI_NO INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = F.CATEGORY INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = F.PRODUCT WHERE A.WI_NO =" + WI + " AND PDD_ITEM = " + PDD
            SqlStr = "SELECT A.WI_NO,F.BRANCH_CODE, F.BRANCH_NAME, F.CUSTOMER_NAME, F.LOAN_NUMBER, convert(varchar,FORMAT(F.DISBURSAL_DATE, 'dd/MM/yyyy')), C.CATEGORY_NAME, D.PRODUCT_NAME, CASE WHEN A.PDD_ITEM = 1000 THEN A.PDD_OTHER ELSE B.PDD_NAME END AS PDD, CASE WHEN A.TO_BE_UPDATED = 0 THEN convert(varchar,FORMAT(A.DUE_DATE, 'dd/MM/yyyy')) ELSE 'TO BE UPDATED' END AS DUE_DATE, CASE WHEN A.STATUS IN (2,5) THEN 'APPROVER LEVEL PENDING' WHEN A.STATUS = 3 THEN 'PDD ENTRY TEAM LEVEL PENDING' WHEN A.STATUS IN (1,4,7) THEN 'SO LEVEL PENDING' WHEN A.STATUS = 6 THEN 'APPROVER LEVEL COMPLETED' END AS [CURRENT STATUS], A.SO_REMARKS, convert(varchar,FORMAT(E.SO_ENTRY_ON, 'dd/MM/yyyy')) AS [SO ENTRY DATE],CASE WHEN A.APPROVED_STATUS = 1 THEN 'APPROVED' WHEN A.APPROVED_STATUS = 2 THEN 'RETURNED' WHEN A.APPROVED_STATUS = 3 THEN 'REJECTED' ELSE 'NOT APPROVED' END AS APPROVED_STATUS, A.APPROVED_REMARKS, convert(varchar,FORMAT(A.APPROVED_ON, 'dd/MM/yyyy')) AS APPROVED_DATE FROM PDD_ENTRY_SUB_CYCLE A LEFT JOIN PDD_MASTER B ON B.PDD_ID = A.PDD_ITEM INNER JOIN PDD_ENTRY_MASTER F ON F.WI_NO = A.WI_NO INNER JOIN PDD_CATEGORY_MASTER C ON C.CATEGORY_ID = F.CATEGORY inner join pdd_entry_sub E ON E.Sub_No = A.Sub_No INNER JOIN PDD_PRODUCT_MASTER D ON D.PRODUCT_ID = F.PRODUCT WHERE A.WI_NO = '" + WI + "' AND A.PDD_ITEM = " + PDD

            DT = DB.ExecuteDataSet(SqlStr).Tables(0)
            RH.Heading(Session("FirmName"), tb, "Details Of PDD ITEM ", 100)

            Dim RowBG As Integer = 0
            Dim DR As DataRow
            tb.Attributes.Add("width", "100%")

            Dim TRHead As New TableRow
            TRHead.BackColor = Drawing.Color.WhiteSmoke
            Dim TRHead_00, TRHead_01, TRHead_02, TRHead_03, TRHead_04, TRHead_05, TRHead_06, TRHead_07, TRHead_08, TRHead_09, TRHead_10, TRHead_11, TRHead_12, TRHead_13, TRHead_14, TRHead_15, TRHead_16, TRHead_17 As New TableCell

            TRHead_00.BorderWidth = "1"
            TRHead_01.BorderWidth = "1"
            TRHead_02.BorderWidth = "1"
            TRHead_03.BorderWidth = "1"
            TRHead_04.BorderWidth = "1"
            TRHead_05.BorderWidth = "1"
            TRHead_06.BorderWidth = "1"
            TRHead_07.BorderWidth = "1"
            TRHead_08.BorderWidth = "1"
            TRHead_09.BorderWidth = "1"
            TRHead_10.BorderWidth = "1"
            TRHead_11.BorderWidth = "1"
            TRHead_12.BorderWidth = "1"
            TRHead_13.BorderWidth = "1"
            TRHead_14.BorderWidth = "1"
            TRHead_15.BorderWidth = "1"
            TRHead_16.BorderWidth = "1"
            TRHead_17.BorderWidth = "1"

            TRHead_00.BorderColor = Drawing.Color.Silver
            TRHead_01.BorderColor = Drawing.Color.Silver
            TRHead_02.BorderColor = Drawing.Color.Silver
            TRHead_03.BorderColor = Drawing.Color.Silver
            TRHead_04.BorderColor = Drawing.Color.Silver
            TRHead_05.BorderColor = Drawing.Color.Silver
            TRHead_06.BorderColor = Drawing.Color.Silver
            TRHead_07.BorderColor = Drawing.Color.Silver
            TRHead_08.BorderColor = Drawing.Color.Silver
            TRHead_09.BorderColor = Drawing.Color.Silver
            TRHead_10.BorderColor = Drawing.Color.Silver
            TRHead_11.BorderColor = Drawing.Color.Silver
            TRHead_12.BorderColor = Drawing.Color.Silver
            TRHead_13.BorderColor = Drawing.Color.Silver
            TRHead_14.BorderColor = Drawing.Color.Silver
            TRHead_15.BorderColor = Drawing.Color.Silver
            TRHead_16.BorderColor = Drawing.Color.Silver
            TRHead_17.BorderColor = Drawing.Color.Silver

            TRHead_00.BorderStyle = BorderStyle.Solid
            TRHead_01.BorderStyle = BorderStyle.Solid
            TRHead_02.BorderStyle = BorderStyle.Solid
            TRHead_03.BorderStyle = BorderStyle.Solid
            TRHead_04.BorderStyle = BorderStyle.Solid
            TRHead_05.BorderStyle = BorderStyle.Solid
            TRHead_06.BorderStyle = BorderStyle.Solid
            TRHead_07.BorderStyle = BorderStyle.Solid
            TRHead_08.BorderStyle = BorderStyle.Solid
            TRHead_09.BorderStyle = BorderStyle.Solid
            TRHead_10.BorderStyle = BorderStyle.Solid
            TRHead_11.BorderStyle = BorderStyle.Solid
            TRHead_12.BorderStyle = BorderStyle.Solid
            TRHead_13.BorderStyle = BorderStyle.Solid
            TRHead_14.BorderStyle = BorderStyle.Solid
            TRHead_15.BorderStyle = BorderStyle.Solid
            TRHead_16.BorderStyle = BorderStyle.Solid
            TRHead_17.BorderStyle = BorderStyle.Solid

            TRHead_00.Font.Bold = True
            TRHead_01.Font.Bold = True
            TRHead_02.Font.Bold = True
            TRHead_03.Font.Bold = True
            TRHead_04.Font.Bold = True
            TRHead_05.Font.Bold = True
            TRHead_06.Font.Bold = True
            TRHead_07.Font.Bold = True
            TRHead_08.Font.Bold = True
            TRHead_09.Font.Bold = True
            TRHead_10.Font.Bold = True
            TRHead_11.Font.Bold = True
            TRHead_12.Font.Bold = True
            TRHead_13.Font.Bold = True
            TRHead_14.Font.Bold = True
            TRHead_15.Font.Bold = True
            TRHead_16.Font.Bold = True
            TRHead_17.Font.Bold = True

            RH.AddColumn(TRHead, TRHead_00, 3, 3, "c", "Sl.No")
            RH.AddColumn(TRHead, TRHead_01, 4, 4, "c", "Work Item")
            RH.AddColumn(TRHead, TRHead_02, 4, 4, "c", "Branch Code")
            RH.AddColumn(TRHead, TRHead_03, 7, 7, "c", "Branch Name")
            RH.AddColumn(TRHead, TRHead_04, 7, 7, "c", "Customer Name")
            RH.AddColumn(TRHead, TRHead_05, 6, 6, "c", "Loan Number")
            RH.AddColumn(TRHead, TRHead_06, 5, 5, "c", "Disbursal Date")
            RH.AddColumn(TRHead, TRHead_07, 6, 6, "c", "Category")
            RH.AddColumn(TRHead, TRHead_08, 6, 6, "c", "Product")
            RH.AddColumn(TRHead, TRHead_09, 7, 7, "c", "PDD Item")
            RH.AddColumn(TRHead, TRHead_10, 5, 5, "c", "Due Date")
            RH.AddColumn(TRHead, TRHead_11, 6, 6, "c", "Current Status")
            RH.AddColumn(TRHead, TRHead_12, 8, 8, "c", "SO Remarks")
            RH.AddColumn(TRHead, TRHead_13, 5, 5, "c", "SO Entry Date")
            RH.AddColumn(TRHead, TRHead_14, 6, 6, "c", "Approver Status")
            RH.AddColumn(TRHead, TRHead_15, 8, 8, "c", "Approver Remarks")
            RH.AddColumn(TRHead, TRHead_16, 5, 5, "c", "Approver Date")
            'RH.AddColumn(TRHead, TRHead_17, 4, 4, "c", "View Detail")

            tb.Controls.Add(TRHead)

            RH.BlankRow(tb, 3)
            Dim i As Integer

            Dim createddate As String = ""

            For Each DR In DT.Rows
                i += 1

                Dim TR3 As New TableRow
                TR3.BorderWidth = "1"
                TR3.BorderStyle = BorderStyle.Solid

                Dim TR3_00, TR3_01, TR3_02, TR3_03, TR3_04, TR3_05, TR3_06, TR3_07, TR3_08, TR3_09, TR3_10, TR3_11, TR3_12, TR3_13, TR3_14, TR3_15, TR3_16, TR3_17 As New TableCell

                TR3_00.BorderWidth = "1"
                TR3_01.BorderWidth = "1"
                TR3_02.BorderWidth = "1"
                TR3_03.BorderWidth = "1"
                TR3_04.BorderWidth = "1"
                TR3_05.BorderWidth = "1"
                TR3_06.BorderWidth = "1"
                TR3_07.BorderWidth = "1"
                TR3_08.BorderWidth = "1"
                TR3_09.BorderWidth = "1"
                TR3_10.BorderWidth = "1"
                TR3_11.BorderWidth = "1"
                TR3_12.BorderWidth = "1"
                TR3_13.BorderWidth = "1"
                TR3_14.BorderWidth = "1"
                TR3_15.BorderWidth = "1"
                TR3_16.BorderWidth = "1"
                TR3_17.BorderWidth = "1"

                TR3_00.BorderColor = Drawing.Color.Silver
                TR3_01.BorderColor = Drawing.Color.Silver
                TR3_02.BorderColor = Drawing.Color.Silver
                TR3_03.BorderColor = Drawing.Color.Silver
                TR3_04.BorderColor = Drawing.Color.Silver
                TR3_05.BorderColor = Drawing.Color.Silver
                TR3_06.BorderColor = Drawing.Color.Silver
                TR3_07.BorderColor = Drawing.Color.Silver
                TR3_08.BorderColor = Drawing.Color.Silver
                TR3_09.BorderColor = Drawing.Color.Silver
                TR3_10.BorderColor = Drawing.Color.Silver
                TR3_11.BorderColor = Drawing.Color.Silver
                TR3_12.BorderColor = Drawing.Color.Silver
                TR3_13.BorderColor = Drawing.Color.Silver
                TR3_14.BorderColor = Drawing.Color.Silver
                TR3_15.BorderColor = Drawing.Color.Silver
                TR3_16.BorderColor = Drawing.Color.Silver
                TR3_17.BorderColor = Drawing.Color.Silver

                TR3_00.BorderStyle = BorderStyle.Solid
                TR3_01.BorderStyle = BorderStyle.Solid
                TR3_02.BorderStyle = BorderStyle.Solid
                TR3_03.BorderStyle = BorderStyle.Solid
                TR3_04.BorderStyle = BorderStyle.Solid
                TR3_05.BorderStyle = BorderStyle.Solid
                TR3_06.BorderStyle = BorderStyle.Solid
                TR3_07.BorderStyle = BorderStyle.Solid
                TR3_08.BorderStyle = BorderStyle.Solid
                TR3_09.BorderStyle = BorderStyle.Solid
                TR3_10.BorderStyle = BorderStyle.Solid
                TR3_11.BorderStyle = BorderStyle.Solid
                TR3_12.BorderStyle = BorderStyle.Solid
                TR3_13.BorderStyle = BorderStyle.Solid
                TR3_14.BorderStyle = BorderStyle.Solid
                TR3_15.BorderStyle = BorderStyle.Solid
                TR3_16.BorderStyle = BorderStyle.Solid
                TR3_17.BorderStyle = BorderStyle.Solid

                RH.AddColumn(TR3, TR3_00, 3, 3, "c", i)
                RH.AddColumn(TR3, TR3_01, 4, 4, "l", DR(0).ToString())
                RH.AddColumn(TR3, TR3_02, 4, 4, "l", DR(1).ToString())
                RH.AddColumn(TR3, TR3_03, 7, 7, "l", DR(2).ToString())
                RH.AddColumn(TR3, TR3_04, 7, 7, "l", DR(3).ToString())
                RH.AddColumn(TR3, TR3_05, 6, 6, "l", DR(4).ToString())
                RH.AddColumn(TR3, TR3_06, 5, 5, "l", DR(5).ToString())
                RH.AddColumn(TR3, TR3_07, 6, 6, "l", DR(6).ToString())
                RH.AddColumn(TR3, TR3_08, 6, 6, "l", DR(7).ToString())
                RH.AddColumn(TR3, TR3_09, 7, 7, "l", DR(8).ToString())
                RH.AddColumn(TR3, TR3_10, 5, 5, "l", DR(9).ToString())
                RH.AddColumn(TR3, TR3_11, 6, 6, "l", DR(10).ToString())
                RH.AddColumn(TR3, TR3_12, 8, 8, "l", DR(11).ToString())
                RH.AddColumn(TR3, TR3_13, 5, 5, "l", DR(12).ToString())
                RH.AddColumn(TR3, TR3_14, 6, 6, "l", DR(13).ToString())
                RH.AddColumn(TR3, TR3_15, 8, 8, "l", DR(14).ToString())
                RH.AddColumn(TR3, TR3_16, 5, 5, "l", DR(15).ToString())
                'RH.AddColumn(TR3, TR3_17, 4, 4, "l", "<a href='viewWIDetails.aspx?WI=" + GN.Encrypt(DR(0).ToString()) + "&pdd=" + GN.Encrypt(DR(8).ToString()) + "' target='_blank'>" + "View Detail")


                tb.Controls.Add(TR3)

            Next
            RH.BlankRow(tb, 20)
            pnDisplay.Controls.Add(tb)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

    Protected Sub cmd_Print_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Print.Click

    End Sub

    Protected Sub cmd_Back_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Back.Click

    End Sub

    Protected Sub cmd_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Export.Click
        Try
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=Report.pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            pnDisplay.RenderControl(hw)

            Dim sr As New StringReader(sw.ToString())
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.[End]()
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub

End Class
