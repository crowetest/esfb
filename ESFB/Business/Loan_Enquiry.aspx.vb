﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Loan_Enquiry
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Dim SINo As Integer

    Dim UserID As Integer
    Dim BranchID As Integer

    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1166) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Loan Enquiry"
            UserID = CInt(Session("UserID"))
            BranchID = CInt(Session("BranchID"))

            hdnLoanEnquiry.Value = "¥µµµµ-1µµµµµµµ"
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)
            hdnDate.Value = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            '/--- Register Client Side Functions ---//
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "Fill();", True)
        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try


    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent


        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim TRDate As DateTime = CDate(Session("TraDt"))
        Dim BranchID As Integer = CInt(Session("BranchID"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            Dim MeetingDtl As String = Data(1)
            If MeetingDtl <> "" Then
                MeetingDtl = MeetingDtl.Substring(1)
            End If

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(5) As SqlParameter

                Params(0) = New SqlParameter("@MeetingDtl", SqlDbType.VarChar, -1)
                Params(0).Value = MeetingDtl
                Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@TraDt", SqlDbType.DateTime)
                Params(2).Value = TRDate
                Params(3) = New SqlParameter("@BranchID", SqlDbType.Int)
                Params(3).Value = BranchID
                Params(4) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(4).Direction = ParameterDirection.Output
                Params(5) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 50)
                Params(5).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_LOAN_ENQUIRY", Params)
                ErrorFlag = CInt(Params(4).Value)
                Message = CStr(Params(5).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message
        ElseIf CInt(Data(0)) = 2 Then
            Dim Emp_code As String = Data(1)
            DT = DB.ExecuteDataSet(" Select emp_name from  Emp_List where EMp_Code =" + Emp_code.ToString() + "").Tables(0)
            If DT.Rows.Count > 0 Then
                CallBackReturn = DT.Rows(0)(0).ToString()
            Else
                CallBackReturn = "Ø"
            End If
        End If
    End Sub

#End Region


End Class

