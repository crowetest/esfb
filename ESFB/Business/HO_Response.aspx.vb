﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class HO_Response
    Inherits System.Web.UI.Page
    Implements Web.UI.ICallbackEventHandler
    Dim EN As New Enrollment
    Dim GN As New GeneralFunctions
    Dim DT As New DataTable
    Dim DTT As New DataTable
    Dim SINo As Integer

    Dim UserID As Integer
    Dim BranchID As Integer

    Dim DB As New MS_SQL.Connect
    Dim CallBackReturn As String = Nothing

#Region "Page Load & Dispose"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GN.FormAccess(CInt(Session("UserID")), 1167) = False Then
                Response.Redirect("~/AccessDenied.aspx", False)
                Return
            End If
            Me.Master.subtitle = "Loan Response"
            UserID = CInt(Session("UserID"))
            hdnDate.Value = CDate(Session("TraDt")).ToString("dd MMM yyyy")
            DTT = DB.ExecuteDataSet("select -1 as Emp_code, ' -----Select-----' as Emp_name union all select RA.Emp_code,cast(RA.Emp_code as varchar(5))+ '   |   '  + EM.emp_name from ROLES_ASSIGNED RA inner join emp_master EM on  EM.emp_code=RA.emp_code and RA.Role_id=41 and EM.status_id=1 ").Tables(0)
            hdnQualification.Value = ""
            Dim str As String = ""
            For Each DR As DataRow In DTT.Rows
                hdnQualification.Value += "Ñ" + DR(0).ToString() + "ÿ" + DR(1).ToString()
                'str += "µ" + DR(0).ToString() + "µ" + DR(1).ToString()
            Next
            Dim DT As New DataTable
            'DT = DB.ExecuteDataSet(" select Enquiry_ID,upper(Name) as Name,Address,Mobile,AltMobile ,case when Loan_Purpose='1' then  'Housing Loan' when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5' then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan' end as Loan_Product,Loan_Amount,Remarks,Ref_Emp_code,Ref_Emp_Name,Ref_Contact,Sangam_ID,Sangam_Name ,CONVERT(VARCHAR(10),Enquiry_Date,105) as Enquiry_Date,loan_enquiry.branch_id as Branch_ID,Enquiry_UserID,emp_master.emp_name,Loan_enquiry.Status_id,Explanation from LOAN_ENQUIRY inner join emp_master  on LOAN_ENQUIRY.Enquiry_UserID=emp_master.Emp_Code  where (loan_enquiry.status_id is null or   loan_enquiry.status_id in (1,2,3))  and DATEDIFF(day,Enquiry_Date,getdate())  < 90 ").Tables(0)
            DT = DB.ExecuteDataSet("select Enquiry_ID,upper(Name) as Name,loan_enquiry.Address,Mobile,AltMobile ,case when Loan_Purpose='1' then 'Housing Loan' when Loan_Purpose='2' then 'Agri Loan' when Loan_Purpose='3' then 'Business Loan' when Loan_Purpose='4' then 'LAP' when Loan_Purpose='5' then '2 wheeler loan' when Loan_Purpose='6' then '3&4 wheeler loan' when Loan_Purpose='7' then 'Gold Loan' when Loan_Purpose='8' then 'MSME Loan' when Loan_Purpose='9' then 'Dream Home Loan'  when Loan_Purpose='10' then 'Personal Loan'  when Loan_Purpose='11' then 'Global Career Education' when Loan_Purpose='12' then 'Light Commercial Vehicle Loan' when Loan_Purpose='13' then 'LRD' when Loan_Purpose='14' then 'Solar/Clean Energy'  end as Loan_Product,Loan_Amount,Remarks,case when Sangam_ID=0 then null else Sangam_ID end,Sangam_Name ,CONVERT(VARCHAR(10),Enquiry_Date,105) as Enquiry_Date,loan_enquiry.branch_id  as Branch_ID,bm.branch_name as Branch_Name,Enquiry_UserID,emp_master.emp_name,Loan_enquiry.Status_id,Explanation, case when Assigned_Credit_Officer   is  null then -1  else Assigned_Credit_Officer  end   as Assigned_Credit_Officer from LOAN_ENQUIRY inner join emp_master  on LOAN_ENQUIRY.Enquiry_UserID=emp_master.Emp_Code inner join branch_master bm on emp_master.branch_id=bm.branch_id where (loan_enquiry.status_id is null or   loan_enquiry.status_id not in (4,5,6,7)) and DATEDIFF(day,Enquiry_Date,getdate())  < 90  ").Tables(0)
            Dim StrAttendance As String = ""
            For n As Integer = 0 To DT.Rows.Count - 1
                StrAttendance += DT.Rows(n)(0).ToString & "µ" & DT.Rows(n)(1).ToString & "µ" & DT.Rows(n)(2).ToString & "µ" & DT.Rows(n)(3).ToString & "µ" & DT.Rows(n)(4).ToString & "µ" & DT.Rows(n)(5).ToString & "µ" & DT.Rows(n)(6).ToString & "µ" & DT.Rows(n)(7).ToString & "µ" & DT.Rows(n)(8).ToString & "µ" & DT.Rows(n)(9).ToString & "µ" & DT.Rows(n)(10).ToString & "µ" & DT.Rows(n)(11).ToString & "µ" & DT.Rows(n)(12).ToString & "µ" & DT.Rows(n)(13).ToString & "µ" & DT.Rows(n)(14).ToString & "µ" & DT.Rows(n)(15).ToString & "µ" & DT.Rows(n)(16).ToString & "µ" & DT.Rows(n)(17).ToString
                If n < DT.Rows.Count - 1 Then
                    StrAttendance += "¥"
                End If
            Next


            Me.hdnLoanEnquiry.Value = StrAttendance
           
            Dim cbref As String = Page.ClientScript.GetCallbackEventReference(Me, "Arg", "FromServer", "Context", True)
            Dim cbscript As String = "function ToServer (Arg,Context) {" & cbref & ";}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ToServer", cbscript, True)

            '/--- Register Client Side Functions ---//
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "table_fill();", True)
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "ScriptRegistration", "window_onload();", True)

        Catch ex As Exception
            If Response.IsRequestBeingRedirected Then
                Dim ErrorNo As Integer = GN.InsertError(ex.Message, System.IO.Path.GetFileName(Request.Path))
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorNo.ToString())
            End If
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        DT.Dispose()
        GC.Collect()
    End Sub
#End Region
#Region "Call Back"
    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return CallBackReturn
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        Dim Data() As String = eventArgument.Split(CChar("Ø"))
        Dim UserID As Integer = CInt(Session("UserID"))
        Dim TRDate As Date = CDate(Session("TraDt"))
        CallBackReturn = ""
        If CInt(Data(0)) = 1 Then

            Dim MeetingDtl As String = Data(1)
            If MeetingDtl <> "" Then
                MeetingDtl = MeetingDtl.Substring(1)
            End If

            Dim Message As String = Nothing
            Dim ErrorFlag As Integer = 0
            Try
                Dim Params(4) As SqlParameter

                Params(0) = New SqlParameter("@MeetingDtl", SqlDbType.VarChar, -1)
                Params(0).Value = MeetingDtl
                Params(1) = New SqlParameter("@EmpCode", SqlDbType.Int)
                Params(1).Value = UserID
                Params(2) = New SqlParameter("@TraDt", SqlDbType.Date)
                Params(2).Value = TRDate
                Params(3) = New SqlParameter("@ErrorStatus", SqlDbType.Int)
                Params(3).Direction = ParameterDirection.Output
                Params(4) = New SqlParameter("@OutputMessage", SqlDbType.VarChar, 50)
                Params(4).Direction = ParameterDirection.Output
                DB.ExecuteNonQuery("SP_LOAN_HO_RESPONSE", Params)
                ErrorFlag = CInt(Params(3).Value)
                Message = CStr(Params(4).Value)
            Catch ex As Exception
                Message = ex.Message.ToString
                ErrorFlag = 1
                Response.Redirect("~/CatchException.aspx?ErrorNo=" + ErrorFlag.ToString())
            End Try
            CallBackReturn = ErrorFlag.ToString + "Ø" + Message

        End If
    End Sub

#End Region


End Class

