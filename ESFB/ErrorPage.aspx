﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ErrorPage.aspx.vb" Inherits="ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            color: #E31E24;
            text-align:center;
            height : auto;
           
        }
         .style2
        {
            color: #0000FF;
            background-color:#E31E24;
        }
    </style>
</head>
<body onload="pageInit(); width:100%; height:100%"  class="style2">
    <form id="login_form" runat="server">

        <div style="height:200px; padding-top:0px; padding-top:10px; padding-bottom:10px;"><img src="Image/Logo.png" style="width:435px; height:100px;"/></div>
        <div id="MainContents" style="background-color:White; height:250px; float:none; text-align:center;"><br />
        <b class="style1">
            Session Expired ! 
        </b>
        <div class="style1"><strong style="padding:0px;">
        <b>Sorry,but the page you are looking for has not been found.Click here to login again</b></strong>&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="Image/refresh1.png" style="width:65px; height:50px; padding-top: 60px;" onclick='window.open("<%= Page.ResolveUrl("~")%>default.aspx", "_self")'/></div>
        </div>
    </form>
</body>
</html>
