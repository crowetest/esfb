﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Verification
    Inherits System.Web.UI.Page
    Dim DB As New MS_SQL.Connect
    Dim GF As New GeneralFunctions
    Dim DT As New DataTable
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.Master.subtitle = "Welcome Kit Status Updation"
        '--//---------- Script Registrations -----------//--
        Me.btnConfirm.Attributes.Add("onclick", "return btnConfirmClick()")
    End Sub

    Protected Sub btnConfirm_Click(sender As Object, e As System.EventArgs) Handles btnConfirm.Click
        Try
            Dim Status As Integer = Me.hdnStatus.Value
            Dim Accno As String = Me.txtAccNo.Text
            Dim CustID As String = Me.txtCustID.Text
            Dim UserID As Integer = CInt(Session("UserID"))
            Dim SQL As String = ""
            DT = DB.ExecuteDataSet("SELECT * FROM EMFIL.DBO.SFB_ACCOUNT_STATUS WHERE EMP_CODE = " & UserID & " AND STATUS_ID = 1").Tables(0)
            If DT.Rows.Count = 0 Then
                SQL = "INSERT INTO EMFIL.DBO.SFB_ACCOUNT_STATUS  VALUES(" & UserID & "," & Status & ",'" & Accno & "','" & CustID & "',GETDATE())"
            Else
                SQL = "UPDATE EMFIL.DBO.SFB_ACCOUNT_STATUS SET STATUS_ID = " & Status & ",ACCOUNT_NO = '" & Accno & "',CUSTOMER_ID='" & CustID & "',TRA_DT = GETDATE() WHERE EMP_CODE = " & UserID & ""
            End If
            DB.ExecuteNonQuery(SQL)
            Response.Redirect("Portal.aspx")
        Catch ex As Exception
            Response.Redirect("Portal.aspx")
            Response.Redirect("~/CatchException.aspx?ErrorNo=1")
        End Try
    End Sub
End Class
