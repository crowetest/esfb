﻿<%@ Page Title="" Language="VB" MasterPageFile="~/REPORT.master" AutoEventWireup="false" CodeFile="Downloads_Risk.aspx.vb" Inherits="Downloads" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/REPORT.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPH" runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ESAF</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="Style/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="Style/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="Style/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="Style/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
     <script src="../../Script/Calculations.js" type="text/javascript"></script>
    <script src="../../Script/Validations.js" type="text/javascript"></script>
    <style type="text/css">
        .pn1
        {
            margin-top :-350px;
        }
    </style>
  <script language="javascript" type="text/javascript">
      function window_onload() {

          var tab = "<div><br/><br/></div>";
          document.getElementById("<%= pnlImg.ClientID %>").innerHTML = tab;
          //document.getElementById("lblName").innerHTML = document.getElementById("<%= hid_User.ClientID %>").value;
          
         // table_fill_HR();
      }

      

      

      function table_fill_HR() {
          
          
          document.getElementById("lblCaption").innerHTML = 'HR Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          
          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>HR Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_HR.ClientID %>").value != "") {            
              row1 = document.getElementById("<%= hid_HR.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {
                      
                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";
                      
                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m+1) + "</td>";
                    if(col2[4]  == 1)
                        tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                    else
                        tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";
                   

                  
                  tab += "<td style='width:70%;height:10px; text-align:left;' >" + col2[5] + "</td>";
                  
              }
              
              tab += "<tr><td colspan='3'></td></tr>";

          }
        


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Risk() {


          document.getElementById("lblCaption").innerHTML = 'Risk Management';
          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;


          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>Risk Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Risk.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Risk.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";



                  tab += "<td style='width:70%;height:10px; text-align:left;' >" + col2[5] + "</td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }



          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
          Risk()

          //--------------------- Clearing Data ------------------------//


      }
      function table_fill_Audit() {
          
          document.getElementById("lblCaption").innerHTML = 'Audit Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>Audit Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Audit.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Audit.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";



                  tab += "<td style='width:70%;height:10px; text-align:left;' >" + col2[5] + "</td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }



          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      
      function table_fill_HD() {
      
        document.getElementById("lblCaption").innerHTML = 'HelpDesk Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>HelpDesk Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Hd.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Hd.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";



                  tab += "<td style='width:70%;height:10px; text-align:left;' >" + col2[5] + "</td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }



          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

      }
      
      function table_fill_Compliance() {
         
        document.getElementById("lblCaption").innerHTML = 'Compliance Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>Compliance Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Com.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Com.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";



                  tab += "<td style='width:70%;height:10px; text-align:left;' >" + col2[5] + "</td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }



          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

      }
     
      function table_fill_OP() {
         document.getElementById("lblCaption").innerHTML = 'Operation Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%; font-size:10pt;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'><h5 style='color:#E31E24;'><u>OPeration Management Policies</u></h5></td></tr>";
          if (document.getElementById("<%= hid_Op.ClientID %>").value != "") {
              row1 = document.getElementById("<%= hid_Op.ClientID %>").value.split("¥");
              var ShowQuali = 0;
              for (m = 0; m <= row1.length - 1; m++) {
                  col2 = row1[m].split("µ");
                  if (ShowQuali == 0) {

                      tab += "<tr><td style='width:14%;height:10px; text-align:left;font-weight:bold;'>Sl No</td>";
                      tab += "<td style='width:20%;height:10px; text-align:left;font-weight:bold;' >Caption</td>";
                      tab += "<td style='width:70%;height:10px; text-align:left;font-weight:bold;' >Description</td></tr>";

                      ShowQuali = 1;

                  }
                  tab += "<tr><td style='width:10%;height:10px; text-align:left;'>" + (m + 1) + "</td>";
                  if (col2[4] == 1)
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/viewAttachment.aspx?RequestID=" + col2[0] + " &RptID=1  ' style='cursor:pointer; font-size:10pt;' target='_blank'>" + col2[3] + "</a></td>";
                  else
                      tab += "<td style='width:20%;height:10px; text-align:left;' ><a href='Help/ShowAttachment.aspx?RequestID=" + col2[0] + " &RptID=2 ' style='cursor:pointer; font-size:10pt;'>" + col2[3] + "</a></td>";



                  tab += "<td style='width:70%;height:10px; text-align:left;' >" + col2[5] + "</td>";

              }

              tab += "<tr><td colspan='3'></td></tr>";

          }



          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;

      }
      function Foriegn() {

          document.getElementById("lblCaption").innerHTML = 'Foreign Exchange';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";

          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Foreign Exchange  Purchase Application [SFB_05_27_09_17].pdf' target='_blank'><b>Application for purchase of Foreign Exchange</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/currency features.zip' target='_blank'><b>Currency Features</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Cash Memo.pdf' target='_blank'><b>Cash Memo</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Encashment Certificate new.pdf' target='_blank'><b>Encashment Certificate new</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/Form_A2.pdf' target='_blank'><b>Form_A2</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/LRS Application cum declaration.pdf' target='_blank'><b>LRS Application cum declaration</b></a></td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_NPS() {
          document.getElementById("lblCaption").innerHTML = 'NPS Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'>No files uploaded in NPS Management</td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_Election() {
          document.getElementById("lblCaption").innerHTML = 'Election Management';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          //tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/HRHelp.pptx' target='_blank'><b>Help</b></a></td></tr>";
          tab += "<tr><td colspan='3'>No files uploaded in Election Management</td></tr>";


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_OFMA() {
          document.getElementById("lblCaption").innerHTML = 'OFMA';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/OFMA.pdf' target='_blank'><b>User Manual</b></a></td></tr>";
        


          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;


      }
      function table_fill_CompanyPolicies() {
          document.getElementById("lblCaption").innerHTML = 'Company Policies';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=1' target='_blank'><b>Microfinance and Credit Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=2' target='_blank'><b>Loan Rescheduling Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=3' target='_blank'><b>Client Privacy Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=4' target='_blank'><b>Client Protection Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=5' target='_blank'><b>Grievance  Redressal Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=6' target='_blank'><b>I T Policy</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=7' target='_blank'><b>Product Guidelines</b></a></td></tr>";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/EmbedPolicies.aspx?RptID=8' target='_blank'><b>Operational Manual</b></a></td></tr>";

          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function table_fill_LivelihoodSurvey() {
          document.getElementById("lblCaption").innerHTML = 'Livelihood Services';

          document.getElementById("<%= pnlDtls.ClientID %>").style.display = '';
          var row_bg = 0;

          var tab = "<div style='width:100%;'><table style='width:100%;' >";
          tab += "<tr><td colspan='3'><img src='Image/Emergency.png' height='20px' width='20px' >&nbsp;&nbsp;<a href='Help/LivelihoodSurveyFormat.pdf' target='_blank'><b>Identification Survey Form </b></a></td></tr>";
          tab += "</table></div>";
          document.getElementById("<%= pnlDtls.ClientID %>").innerHTML = tab;
      }
      function FromServer(arg, context) {

          switch (context) {
              case 1:
                  {
                      document.getElementById("<%= hid_HR.ClientID %>").value = arg; 
                      table_fill_HR();
                      break;
                  }
              case 2:
                  {
                      document.getElementById("<%= hid_Audit.ClientID %>").value = arg;
                      table_fill_Audit();
                      break;
                  }
              case 3:
                  {
                      document.getElementById("<%= hid_Hd.ClientID %>").value = arg;
                      table_fill_HD();
                      break;
                  }
             case 4:
                  {
                      document.getElementById("<%= hid_Com.ClientID %>").value = arg;
                      table_fill_Compliance();
                      break;
                  }
             case 5:
                  {
                      document.getElementById("<%= hid_Op.ClientID %>").value = arg;
                      table_fill_OP();
                      break;
                  }
              case 11:
                  {
                      document.getElementById("<%= hid_Risk.ClientID %>").value = arg;
                      table_fill_Risk();
                      break;
                  }
             
          }

      }
      function HR() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 1;
          ToServer("1Ø" , 1);
      }
      function Audit() {
          
          
          document.getElementById("<%= hid_Audit.ClientID %>").value = 2;
          ToServer("2Ø", 2);
      }
      function HelpDesk() {
          
          
          document.getElementById("<%= hid_Hd.ClientID %>").value = 3;
          ToServer("3Ø", 3);
      }
      function Compliance() {
          
          document.getElementById("<%= hid_Com.ClientID %>").value = 4;
          ToServer("4Ø", 4);
      }
      function Operations() {
          
          document.getElementById("<%= hid_Op.ClientID %>").value = 5;
          ToServer("5Ø", 5);
      }
      function NPS() {
          
          
          document.getElementById("<%= hid_Type.ClientID %>").value = 6;


          table_fill_NPS();
      }
      function OFMA() {
          
          
          document.getElementById("<%= hid_Type.ClientID %>").value = 6;


          table_fill_OFMA();
      }
     

      function Election() {
          

          document.getElementById("<%= hid_Type.ClientID %>").value = 7;


          table_fill_Election();
      }

      function CompanyPolicies() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 8;
          table_fill_CompanyPolicies();
      }
      function LivelihoodSurvey() {
          document.getElementById("<%= hid_Type.ClientID %>").value = 9;
          table_fill_LivelihoodSurvey();
      }
      function Risk() {
          document.getElementById("<%= hid_Risk.ClientID %>").value = 11;
          ToServer("11Ø", 11);
      }

    </script>
    </head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            
  <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">&nbsp; <a href="home.aspx" class="btn btn-danger square-btn-adjust">Back</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                <asp:Panel ID="pnlImg" runat="server"></asp:Panel>
                    
					</li>
                    <li  onclick ="Risk()" >
                        <a   href="#"> Risk Management <span style="float:right;"> &raquo;</span></a>
                    </li>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                    <h3 style='width:100%;color:#E31E24;font-size:larger;'><b>Document Downloads</h3></b><br/><br/><h5>Choose the download that you want </h5>
                     <h4><label id="lblCaption" style="font-size:12pt;font-weight:normal;color:#E31E24;"></label><asp:HiddenField ID="hid_image" runat="server" />
                        
                        <asp:HiddenField ID="hid_User" runat="server" />
                        <asp:HiddenField ID="hid_EmpCode" runat="server" />
                        <asp:HiddenField ID="hid_Leave" runat="server" />
                        <asp:HiddenField ID="hid_Promotion" runat="server" />
                        <asp:HiddenField ID="hid_HR" runat="server" />
                        <asp:HiddenField ID="hid_dtls" runat="server" />
                        <asp:HiddenField ID="hdnSubID" runat="server" />
                        <asp:HiddenField ID="hdnFromDt" runat="server" />
                        <asp:HiddenField ID="hdnToDt" runat="server" />
                        
                        <asp:HiddenField ID="hid_Transfer" runat="server" />
                        <asp:HiddenField ID="hid_Type" runat="server" />
                        <asp:HiddenField ID="hid_Job" runat="server" />
                        <asp:HiddenField ID="hid_Compliant" runat="server" />
                        <asp:HiddenField ID="hid_Risk" runat="server" />
                        <asp:HiddenField ID="hid_Audit" runat="server" />
                        <asp:HiddenField ID="hid_Hd" runat="server" />
                        <asp:HiddenField ID="hid_Com" runat="server" />
                        <asp:HiddenField ID="hid_Op" runat="server" />
                        <asp:HiddenField ID="hid_for" runat="server" />
                        </h4>   
                        
                                               
                <br /><asp:Panel ID="pnlDtls" runat="server"></asp:Panel>
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                              
                           
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <!-- METISMENU SCRIPTS -->
    
    
   
    </form>
    
   
</body>
</html>
</asp:Content>
